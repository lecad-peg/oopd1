##
##	oopd1 Makefile 
##
# compile w/o MPI (no) or w/ MPI (yes)
MPI=no
# compile mode: optimized; if not set, it is debug mode
# MODE=optimized
#
UNAME_S := $(shell uname -s)
UNAME_N := $(shell uname -n)
# Tcl/Tk version, use if only tkX.x.a libs exist:
# TCLTK=8.4
TCLTK=8.6
#TKDIR=-L/usr/local/lib
TKDIR=-L/usr/lib64
# MAL/JG had this as target dir for xgrafix; might be necessary on MacOS
#XGPATH = /usr/local/xgrafix
##
XGPATH = /usr/local
EXTRA_LIBDIR=
##
##
DEBUGPREFIX = v
##
DEPFILE = .depend
##
COPT= -O3
CDEBUG= -Wall -g
##
ifeq ($(MPI),no)
   CC = g++
   EXEC = pd1
   ifeq ($(UNAME_S),Darwin)
      TCLTK=
      EXTRA_LIBDIR=-L/opt/local/lib
   endif
else
   EXEC = mpipd1
   ifeq ($(UNAME_S),Darwin)
      CC = mpic++-mpich-devel-mp
   else
      CC = mpiCC
      ifeq ($(UNAME_N),dev-intel16)
         # HPCC @ MSU
         XGPATH = $(HOME)/xgrafix
      endif
   endif
endif
##
CFLAGS=-I$(XGPATH)/include
##
##
ifeq ($(MODE),optimized)
override CFLAGS += $(COPT)
else
override CFLAGS += $(CDEBUG)
endif
##
##
## Flags used for compiling.  -O for optimization, which is optional.
## If the X11 include files are not located in /usr/include/X11, you
## must specify the directory as a flag prefixed by -I.  For instance, 
## if the include files for X11 are located in /usr/local/include, the 
## flag -I/usr/local/include must also be passed.
##

#LIBS  = -L/usr/local/lib -L/usr/X11R6/lib $(EXTRA_LIBDIR) -L$(XGPATH)/lib -lXGC250 -ltk$(TCLTK) -ltcl$(TCLTK) -lXpm -lX11 -lm -ldl
LIBS  = -L/usr/local/lib $(EXTRA_LIBDIR) -L$(XGPATH)/lib -lXGC250 -ltk$(TCLTK) -ltcl$(TCLTK) -lXpm -lX11 -lm -ldl
##
##
## Libraries and their directories used in loading.  
## Normal is -lm and -lX11 for the Math and X11 libraries, which is 
## NOT optional.  On Unicos, -lnet is also used.  If the X11 
## libraries are not located in /usr/lib/X11, you
## must also specify the directory as a flag prefixed by -L.  
## For instance, if the include files for X11 are located in 
## /usr/local/lib, the flag -L/usr/local/lib must also be passed.  
## On Sun, use -Bstatic to avoid problems with differing versions of OS.  
## Man cc for more information on flags.
##
##// chlorine_mcc.o added below 20130604,Huang 
PD1OBJ= pd1.o sptlrgn.o grid.o fields.o poissonfield.o circuit.o \
	trimatrix.o particlegroup.o particlegrouparray.o species.o \
	DataArray.o VariableArray.o TimeArray.o \
	RandomScalarGenerator.o Array2d.o \
	FastFourierTransformer.o  VectorArray.o load.o plasmasource.o \
	boundary.o conductor.o dielectric.o plsmdev.o drive.o \
	reactiontype.o reaction.o reactionParse.o reactionSubParse.o \
	mcc.o hydrocarbons_mcc.o oxygen_mcc.o chlorine_mcc.o\
	fparser.o sptlrgngrp.o directfield.o \
	secondary.o fluid.o boltz.o diffusion_fluid.o radiation.o\
	dist_xv.o dist_binned.o uniformmesh.o dist_x.o dist_v.o \
	dist_function.o dist_gaussian.o dist_uniform.o dist_arbitrary.o \
	message.o \
	emitter.o beamemitter.o fieldemitter.o secondaryemitter.o \
	Base_Class_Direct_Field.o equation.o timing.o \
	diagnosticcontrol.o diagnostic_phase.o diagnostic2d.o \
	diagnostic3d.o eqarray.o \
	diagnostic_time.o diagnostic_parser.o diagnostic.o\
	periodic.o diagnostic_eedf.o diagnostic_reaction.o

all:	 $(PD1OBJ) $(EXEC)

.cpp.o:	argon_gas.hpp
	$(CC) -c $(CFLAGS) $(DEFINES) $*.cpp
	
$(EXEC):	$(PD1OBJ) 
		$(CC) -o $(EXEC) $(PD1OBJ) $(LIBS)

# fcalc -- function calculator (tests Equation class)
fcalc:		fparser.o function.o equation.o
	$(CC) -o fcalc function.o fparser.o equation.o $(LIBS)

# ndarray -- demonstrates ndarray and binned template
ndarray: $(DEPFILE) $(PD1OBJ) ndarray.cpp
	$(CC) $(CFLAGS) $(DEFINES) -o ndarray ndarray.cpp uniformmesh.o DataArray.o equation.o fparser.o

# load -- demonstrates distribution loading
LOAD_SOURCE= dist_load.cpp dist_xv.cpp dist_x.cpp dist_v.cpp dist_function.cpp \
	dist_uniform.cpp dist_gaussian.cpp dist_arbitrary.cpp message.cpp
dist_load: $(LOAD_SOURCE)
	$(CC) $(CFLAGS) -D_STANDALONE_LOADLIB -o dist_load $(LOAD_SOURCE)

# mindgame: for case that the file $(DEPFILE) is wanted to be updated.
dep:
	$(MAKE) $(DEPFILE)

$(DEPFILE): 
# mindgame: makedepend is a legacy util.
	$(CC) -MM $(CFLAGS) *.cpp 1> $(DEPFILE)
#	touch $(DEPFILE)
#	makedepend -f$(DEPFILE) *.cpp 2> /dev/null

clean:
	@rm -f *.o *~ $(PD1OBJ) $(DEPFILE) > /dev/null

distclean: 
	@make clean
	@rm -f $(EXEC)
	
# DO NOT DELETE
# mindgame: check whether it exists
ifneq ($(wildcard $(DEPFILE)),)
include $(DEPFILE)
endif
