
oopd1 -- Object Oriented Plasma Device 1-d

Input file is 'inp/argon_16mTorr_13MHz_50Am2.inp'
Running without X
Dumping every 550000 steps
Running for 5500000 steps
Periodic dumping in overwrite mode.


* Resultant Reactions regarding Argon
  e_elastic
   : E-(E-) + Argon(Ar) -> Argon(Ar) + E-(E-)
  e_excitation
   : E-(E-) + Argon(Ar) -> E-(E-) + Arr(Arr)
  e_excitation
   : E-(E-) + Argon(Ar) -> E-(E-) + Arm(Arm)
  e_excitation
   : E-(E-) + Argon(Ar) -> E-(E-) + Ar4p(Ar4p)
  e_excitation
   : E-(E-) + Argon(Ar) -> E-(E-) + (Empty)
  e_excitation
   : E-(E-) + Argon(Ar) -> E-(E-) + (Empty)
  e_excitation
   : E-(E-) + Argon(Ar) -> E-(E-) + (Empty)
  e_ionization
   : E-(E-) + Argon(Ar) -> 2 E-(E-) + Ar_ions(Ar+)
  samemass_elastic
   : Ar_ions(Ar+) + Argon(Ar) -> Argon(Ar) + Ar_ions(Ar+)
  b_samemass_elastic
   : Ar_ions(Ar+) + Argon(Ar) -> Argon(Ar) + Ar_ions(Ar+)
  samemass_elastic
   : Argon(Ar) + Argon(Ar) -> 2 Argon(Ar)
  samemass_elastic
   : Arm(Arm) + Argon(Ar) -> Argon(Ar) + Arm(Arm)
  samemass_excitation
   : Arm(Arm) + Argon(Ar) -> Arm(Arm) + Arr(Arr)
  samemass_elastic
   : Arr(Arr) + Argon(Ar) -> Argon(Ar) + Arr(Arr)
  samemass_excitation
   : Arr(Arr) + Argon(Ar) -> Arr(Arr) + Arm(Arm)
  samemass_elastic
   : Ar4p(Ar4p) + Argon(Ar) -> Argon(Ar) + Ar4p(Ar4p)
  e_elastic
   : E-(E-) + Arm(Arm) -> Arm(Arm) + E-(E-)
  e_excitation
   : E-(E-) + Arm(Arm) -> E-(E-) + Argon(Ar)
  e_ionization
   : E-(E-) + Arm(Arm) -> 2 E-(E-) + Ar_ions(Ar+)
  e_excitation
   : E-(E-) + Arm(Arm) -> E-(E-) + Arr(Arr)
  e_excitation
   : E-(E-) + Arm(Arm) -> E-(E-) + Ar4p(Ar4p)
  Penning_ionization
   : Arm(Arm) + Arm(Arm) -> E-(E-) + Ar_ions(Ar+) + Argon(Ar)
  Penning_ionization
   : Arr(Arr) + Arm(Arm) -> E-(E-) + Ar_ions(Ar+) + Argon(Ar)
  e_elastic
   : E-(E-) + Ar4p(Ar4p) -> Ar4p(Ar4p) + E-(E-)
  e_excitation
   : E-(E-) + Ar4p(Ar4p) -> E-(E-) + Argon(Ar)
  e_ionization
   : E-(E-) + Ar4p(Ar4p) -> 2 E-(E-) + Ar_ions(Ar+)
  e_excitation
   : E-(E-) + Ar4p(Ar4p) -> E-(E-) + Arr(Arr)
  e_excitation
   : E-(E-) + Ar4p(Ar4p) -> E-(E-) + Arm(Arm)
  Penning_ionization
   : Ar4p(Ar4p) + Ar4p(Ar4p) -> E-(E-) + Ar_ions(Ar+) + Argon(Ar)
  e_elastic
   : E-(E-) + Arr(Arr) -> Arr(Arr) + E-(E-)
  e_excitation
   : E-(E-) + Arr(Arr) -> E-(E-) + Argon(Ar)
  e_excitation
   : E-(E-) + Arr(Arr) -> E-(E-) + Arm(Arm)
  e_excitation
   : E-(E-) + Arr(Arr) -> E-(E-) + Ar4p(Ar4p)
  Penning_ionization
   : Arr(Arr) + Arr(Arr) -> E-(E-) + Ar_ions(Ar+) + Argon(Ar)
  photon_emission
   : Ar4p(Ar4p) + Unit(Unit) -> Arm(Arm)
  photon_emission
   : Ar4p(Ar4p) + Unit(Unit) -> Arr(Arr)
  photon_emission
   : Arr(Arr) + Unit(Unit) -> Argon(Ar)
  samemass_elastic
   : Ar_ions(Ar+) + Ar_ions(Ar+) -> 2 Ar_ions(Ar+)

Initializing diagnostics, SpatialRegion ...
*adding diagnostic: Boundary(0,0)_PwrAve(t)
*adding diagnostic: Boundary(0,0)_Pwr(t)
*adding diagnostic: Boundary(0,0)_Ickt(t)
*adding diagnostic: Boundary(0,0)_phi(t)
*adding diagnostic: Boundary(0,0)_Iconv(t)
*adding diagnostic: Boundary(500,500)_phi(t)
*adding diagnostic: Boundary(500,500)_Iconv(t)
*adding diagnostic: Boundary(880,880)_phi(t)
*adding diagnostic: Boundary(880,880)_Iconv(t)
*adding diagnostic: Boundary(1000,1000)_PwrAve(t)
*adding diagnostic: Boundary(1000,1000)_Pwr(t)
*adding diagnostic: Boundary(1000,1000)_Ickt(t)
*adding diagnostic: Boundary(1000,1000)_phi(t)
*adding diagnostic: Boundary(1000,1000)_Iconv(t)
*adding diagnostic: Ar4p phase space
*adding diagnostic: Arr phase space
*adding diagnostic: Arm phase space
*adding diagnostic: Unit phase space
*adding diagnostic: Argon phase space
*adding diagnostic: Ar_ions phase space
*adding diagnostic: E- phase space
*adding diagnostic: n(x)
*adding diagnostic: Flux1(x)
*adding diagnostic: J1dotE1(x)
*adding diagnostic: Enden1(x)
*adding diagnostic: Vel1(x)
*adding diagnostic: T1(x)
*adding diagnostic: Flux2(x)
*adding diagnostic: J2dotE2(x)
*adding diagnostic: Enden2(x)
*adding diagnostic: Vel2(x)
*adding diagnostic: T2(x)
*adding diagnostic: Flux3(x)
*adding diagnostic: J3dotE3(x)
*adding diagnostic: Enden3(x)
*adding diagnostic: Vel3(x)
*adding diagnostic: T3(x)
*adding diagnostic: JdotE(x)
*adding diagnostic: Enden(x)
*adding diagnostic: Tperp(x)
*adding diagnostic: Temp(x)
*adding diagnostic: nAve(x)
*adding diagnostic: Flux1Ave(x)
*adding diagnostic: J1dotE1Ave(x)
*adding diagnostic: Enden1Ave(x)
*adding diagnostic: Vel1Ave(x)
*adding diagnostic: T1Ave(x)
*adding diagnostic: Flux2Ave(x)
*adding diagnostic: J2dotE2Ave(x)
*adding diagnostic: Enden2Ave(x)
*adding diagnostic: Vel2Ave(x)
*adding diagnostic: T2Ave(x)
*adding diagnostic: Flux3Ave(x)
*adding diagnostic: J3dotE3Ave(x)
*adding diagnostic: Enden3Ave(x)
*adding diagnostic: Vel3Ave(x)
*adding diagnostic: T3Ave(x)
*adding diagnostic: JdotEAve(x)
*adding diagnostic: EndenAve(x)
*adding diagnostic: TperpAve(x)
*adding diagnostic: TempAve(x)
*adding diagnostic: JdotEPwrAve(t)
*adding diagnostic: J3dotE3PwrAve(t)
*adding diagnostic: J2dotE2PwrAve(t)
*adding diagnostic: J1dotE1PwrAve(t)
*adding diagnostic: Sheathwidths(t)
*adding diagnostic: Temp(t)
*adding diagnostic: Tperp(t)
*adding diagnostic: T3(t)
*adding diagnostic: T2(t)
*adding diagnostic: T1(t)
*adding diagnostic: PhysicalNumber(t)
*adding diagnostic: Number(t)
*adding diagnostic: E(x)
*adding diagnostic: phi(x)
*adding diagnostic: rho(x)
*adding diagnostic: rhoAve(x)
*adding diagnostic: phiAve(x)
*adding diagnostic: Argon DiffusionFluid(x)
*adding diagnostic: Argon Source(x)
*adding diagnostic: Unit DiffusionFluid(x)
*adding diagnostic: Unit Source(x)
*adding diagnostic: Argon(1000,1000)_f(energy)
*adding diagnostic: Argon(1000,1000)_f(angle)
*adding diagnostic: Ar_ions(1000,1000)_f(energy)
*adding diagnostic: Ar_ions(1000,1000)_f(angle)
*adding diagnostic: E-(880,880)_f(energy)
*adding diagnostic: E-(500,500)_f(energy)
*adding diagnostic: Argon(0,0)_f(energy)
*adding diagnostic: Argon(0,0)_f(angle)
*adding diagnostic: Ar_ions(0,0)_f(energy)
*adding diagnostic: Ar_ions(0,0)_f(angle)
PGA::accum_density, timestep=0, species=E-, get_n=40850, get_w0=1e+07
PGA::accum_density, timestep=0, species=Ar_ions, get_n=40850, get_w0=1e+07

Circuit::currentsource(), Didn't do 1st timestep
MCC::init_radiationTrapping(): particlespecies=Arr, fluidspecies=Unit
MCC::setRadiationEscapeFactor(): lambda10=1.049e-07, nu10=5.1e+08, sigma00=3.4e-17, g1overg0=3, secspecies=Ar, L=0.025, V=0.00020425
  T=0.026, n=5.12e+20, v0=354.22, k0=57306, a=0.0121828, gd=0.000307182, gc=0.00309764, gcd=0.00302488, gi=0.00269784
  overall escape factor g = 0.00269784, nu10eff=g*nu10=1.3759e+06
PGA::accum_density, timestep=10000, species=E-, get_n=15, get_w0=5e+07
PGA::accum_density, timestep=10000, species=E-, get_n=32684, get_w0=1e+07
PGA::accum_density, timestep=10000, species=Ar_ions, get_n=10, get_w0=5e+07
PGA::accum_density, timestep=10000, species=Ar_ions, get_n=38931, get_w0=1e+07
PGA::accum_density, timestep=10000, species=Argon, get_n=1589, get_w0=5e+07
PGA::accum_density, timestep=10000, species=Arm, get_n=3, get_w0=5e+07
PGA::accum_density, timestep=10000, species=Arm, get_n=609, get_w0=1e+07
PGA::accum_density, timestep=10000, species=Arr, get_n=1, get_w0=5e+07
PGA::accum_density, timestep=10000, species=Arr, get_n=784, get_w0=1e+07
PGA::accum_density, timestep=10000, species=Ar4p, get_n=0, get_w0=5e+07
PGA::accum_density, timestep=10000, species=Ar4p, get_n=53, get_w0=1e+07
PGA::accum_density, timestep=20000, species=E-, get_n=36, get_w0=5e+07
PGA::accum_density, timestep=20000, species=E-, get_n=27384, get_w0=1e+07
PGA::accum_density, timestep=20000, species=Ar_ions, get_n=21, get_w0=5e+07
PGA::accum_density, timestep=20000, species=Ar_ions, get_n=33626, get_w0=1e+07
PGA::accum_density, timestep=20000, species=Argon, get_n=3551, get_w0=5e+07
PGA::accum_density, timestep=20000, species=Arm, get_n=6, get_w0=5e+07
PGA::accum_density, timestep=20000, species=Arm, get_n=1232, get_w0=1e+07
PGA::accum_density, timestep=20000, species=Arr, get_n=4, get_w0=5e+07
PGA::accum_density, timestep=20000, species=Arr, get_n=1287, get_w0=1e+07
PGA::accum_density, timestep=20000, species=Ar4p, get_n=1, get_w0=5e+07
PGA::accum_density, timestep=20000, species=Ar4p, get_n=25, get_w0=1e+07
PGA::accum_density, timestep=30000, species=E-, get_n=52, get_w0=5e+07
PGA::accum_density, timestep=30000, species=E-, get_n=23209, get_w0=1e+07
PGA::accum_density, timestep=30000, species=Ar_ions, get_n=48, get_w0=5e+07
PGA::accum_density, timestep=30000, species=Ar_ions, get_n=29387, get_w0=1e+07
PGA::accum_density, timestep=30000, species=Argon, get_n=5871, get_w0=5e+07
PGA::accum_density, timestep=30000, species=Arm, get_n=12, get_w0=5e+07
PGA::accum_density, timestep=30000, species=Arm, get_n=1737, get_w0=1e+07
PGA::accum_density, timestep=30000, species=Arr, get_n=12, get_w0=5e+07
PGA::accum_density, timestep=30000, species=Arr, get_n=1513, get_w0=1e+07
PGA::accum_density, timestep=30000, species=Ar4p, get_n=0, get_w0=5e+07
PGA::accum_density, timestep=30000, species=Ar4p, get_n=24, get_w0=1e+07
PGA::accum_density, timestep=40000, species=E-, get_n=61, get_w0=5e+07
PGA::accum_density, timestep=40000, species=E-, get_n=19258, get_w0=1e+07
PGA::accum_density, timestep=40000, species=Ar_ions, get_n=60, get_w0=5e+07
PGA::accum_density, timestep=40000, species=Ar_ions, get_n=25400, get_w0=1e+07
PGA::accum_density, timestep=40000, species=Argon, get_n=8466, get_w0=5e+07
PGA::accum_density, timestep=40000, species=Arm, get_n=14, get_w0=5e+07
PGA::accum_density, timestep=40000, species=Arm, get_n=2190, get_w0=1e+07
PGA::accum_density, timestep=40000, species=Arr, get_n=14, get_w0=5e+07
PGA::accum_density, timestep=40000, species=Arr, get_n=1580, get_w0=1e+07
PGA::accum_density, timestep=40000, species=Ar4p, get_n=0, get_w0=5e+07
PGA::accum_density, timestep=40000, species=Ar4p, get_n=27, get_w0=1e+07
PGA::accum_density, timestep=50000, species=E-, get_n=75, get_w0=5e+07
PGA::accum_density, timestep=50000, species=E-, get_n=15549, get_w0=1e+07
PGA::accum_density, timestep=50000, species=Ar_ions, get_n=68, get_w0=5e+07
PGA::accum_density, timestep=50000, species=Ar_ions, get_n=21720, get_w0=1e+07
PGA::accum_density, timestep=50000, species=Argon, get_n=11053, get_w0=5e+07
PGA::accum_density, timestep=50000, species=Arm, get_n=17, get_w0=5e+07
PGA::accum_density, timestep=50000, species=Arm, get_n=2617, get_w0=1e+07
PGA::accum_density, timestep=50000, species=Arr, get_n=18, get_w0=5e+07
PGA::accum_density, timestep=50000, species=Arr, get_n=1552, get_w0=1e+07
PGA::accum_density, timestep=50000, species=Ar4p, get_n=0, get_w0=5e+07
PGA::accum_density, timestep=50000, species=Ar4p, get_n=31, get_w0=1e+07
PGA::accum_density, timestep=60000, species=E-, get_n=76, get_w0=5e+07
PGA::accum_density, timestep=60000, species=E-, get_n=11926, get_w0=1e+07
PGA::accum_density, timestep=60000, species=Ar_ions, get_n=72, get_w0=5e+07
PGA::accum_density, timestep=60000, species=Ar_ions, get_n=18072, get_w0=1e+07
PGA::accum_density, timestep=60000, species=Argon, get_n=13798, get_w0=5e+07
PGA::accum_density, timestep=60000, species=Arm, get_n=19, get_w0=5e+07
PGA::accum_density, timestep=60000, species=Arm, get_n=3005, get_w0=1e+07
PGA::accum_density, timestep=60000, species=Arr, get_n=20, get_w0=5e+07
PGA::accum_density, timestep=60000, species=Arr, get_n=1523, get_w0=1e+07
PGA::accum_density, timestep=60000, species=Ar4p, get_n=0, get_w0=5e+07
PGA::accum_density, timestep=60000, species=Ar4p, get_n=23, get_w0=1e+07
PGA::accum_density, timestep=70000, species=E-, get_n=82, get_w0=5e+07
PGA::accum_density, timestep=70000, species=E-, get_n=8482, get_w0=1e+07
PGA::accum_density, timestep=70000, species=Ar_ions, get_n=63, get_w0=5e+07
PGA::accum_density, timestep=70000, species=Ar_ions, get_n=14655, get_w0=1e+07
PGA::accum_density, timestep=70000, species=Argon, get_n=16408, get_w0=5e+07
PGA::accum_density, timestep=70000, species=Arm, get_n=20, get_w0=5e+07
PGA::accum_density, timestep=70000, species=Arm, get_n=3360, get_w0=1e+07
PGA::accum_density, timestep=70000, species=Arr, get_n=17, get_w0=5e+07
PGA::accum_density, timestep=70000, species=Arr, get_n=1409, get_w0=1e+07
PGA::accum_density, timestep=70000, species=Ar4p, get_n=1, get_w0=5e+07
PGA::accum_density, timestep=70000, species=Ar4p, get_n=21, get_w0=1e+07
PGA::accum_density, timestep