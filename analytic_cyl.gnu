A = 1.
B = 11.
V = 1000

set xrange[A:B]
set yrange[0.:V]
set multiplot
plot "phi.dat"
plot V*(log(x/B)/log(A/B));
set nomultiplot
