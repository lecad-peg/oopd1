﻿NOTES:
======
This distribution can be compiled using the g++ compiler with debug flag -g, or with optimization flags -O1, -O2, or -O3. 
This distribution MUST BE USED with xgrafix-2.70.7 or later (see item 65 below).  If you have an earlier version of xgrafix, 
you must reinstall the latest version of xgrafix.

NOTES: 
  (a) np2c MUST be defined in the Species input file section for all species.
  (b) For the calculation of the IAD (angular distribution), the keyword Force3V should appear in the Species input file section.
  (c) Current IAD (angular distribution) is solid angle distribution function times sin(angle), i.e., f(angle) * sin(theta), to avoid 
      "unphysical result as theta -> 0 due to particle discreteness, linear weighting, or other round-off errors.".

Latest revisions of oopd1, MAL (lieber@berkeley.edu), 1/17/20
---------------------------------------------------------------------------

(66) Fix bug in initializing reactant_for_sigmaV and maxSigmaV for particle_particle() and fluid_particle() collisions, 
     to always use the first reactant (reactant[0]) and its mass; see ReactionGroup::update() in reaction.cpp, and 
     NOTE TO DEVELOPERS in xxx_gas.hpp files;
     Add #define debugReactionGroup in reaction.hpp to print ReactionGroup information to the terminal on oopd1 startup. 
     Fix minor bugs to oxygen_gas.hpp and nobelgasxterms_gas.hpp (MAL, 1/17/20); files modified are: reaction.hpp, reaction.cpp, mcc.hpp, mcc.cpp, and all xxx_gas.hpp (except air_gas.hpp).

Latest revisions of oopd1, JK (krek@msu.edu), 1/2/20 and 1/12/20
---------------------------------------------------------------------------

(65) Fix bug in xgrafix due to buffer overflow, in cases of long diagnostics names; see XGSetupWindow() in file xgsetup.c (JK, 1/2/20). 
	 New xgrafix version is 2.70.7. 
	 Fix bug in sortflag initialization for particle_particle() collisions; see MCC::init_mcc() in mcc.cpp. (JK, 1/12/20). 
	 Files modified are: mcc.cpp, and xgsetup.c.

Latest revisions of oopd1, JK (krek@msu.edu), 12/06/19
---------------------------------------------------------------------------

(64) reaction (collision) diagnostics now collects real particle count (not computer particles) at each location; 
     particle weight is taken into account; the colliding number of particles are lineary weighted to the
     left and the right grid node according to the position of projectile inside the cell


Latest revisions of oopd1, JK (krek@msu.edu), 11/27/19
---------------------------------------------------------------------------

(63) if selected in input file inside DiagnosticControl block: reaction_diagnostics 
     then diagnostics window also includes list of all reactions in the system; the plot shows accumulated number
     of collisions; numbers (values) can be saved into TXT/PS file using Xgrafix output method available in diagnostics window

(62) new diagnostics: reactions (diagnostics_reaction.cpp)
     the diagnostics shows spatial distribution of MCC (collisions), sorted by reactions; the MCC module is responsible for 
     counting the collisions. The fluid_particle and particle_particle collisions are taken into account (see mcc.cpp)


Latest revisions of oopd1, JK (krek@msu.edu), 01/19/19
---------------------------------------------------------------------------

(61) xgrafix 2.70.6 is required to support species name exporting

(60) when exporting to ASCII files, header line for each data block (next to time simulation time of the export) now includes label - usually that is species name. This makes identifying data blocks much easier.


Latest revisions of oopd1, JK (krek@msu.edu), 10/23/17
---------------------------------------------------------------------------

(59) Makefiles for normal and MPI compiling are now combined into one make with setting on top of file to select compiling. To compile with MPI, change the line
MPI=no
to
MPI=yes
That should do. The resulting executable file is named "mpipd1"

Latest revisions of oopd1Gudmund160426, MAL, 5/14/16
-----------------------------------------------------------------------------
(58) Add characteristic values to Hep1 -> He (resonance photon emission with radiation trapping) reaction in helium_gas.hpp; correct sigma00 for Arr -> Ar (resonance photon emission with radiation trapping) reaction in argon_gas.hpp; minor change to setRadiationEscapeFactor in mcc.cpp; run TestHem.inp and TestArm.inp to see results;  correct format error %p -> %li in pd1.cpp, line 555; MAL 5/14/16

(57) Do complex arithmetic explicitly in Fields::update_icp_fields(), lines 544-593, and remove the "#include <complex>" and "using namespace std" statements at the beginning of fields.cpp; for compilation and linking on new apple clang compiler in OSX 10.11.  MAL 5/10/16

(56) Properly allocate and delete variable-sized sgdarray for grid color diagnostics in diagnosticcontrol.cpp, lines 317-332;  delete multiple ;; from files pd1.hpp line 54, drive_inline.hpp lines 21, 26 and 35;  fparser.cpp lines 181, 324 and 487; for compilation and linking on new apple clang compiler in OSX 10.11, MAL  5/10/16

This distribution can be compiled using the g++ compiler with optimization flags: -O1, or -O2, or -O3.  oopd1 will run a factor of 3.6 times faster when compiled with optimization flag -O1 than with the debug flag -g.  This distribution MUST BE USED with xgrafix-2.70 or later.  If you have an earlier version of xgrafix, you must reinstall the latest version of xgrafix.


(55) Jon Gudmundsson added chlorine_mcc.cpp, chlorine_mcc.hpp and chlorine_gas.hpp; modified secondary emission to use energyThrToAddAsPIC. Made changes to nobelgasxterms cross sections and oxygen cross sections. Modified fields,cpp, nobelgasxterms_gas.hpp, oxygen_gas.hpp, reaction.cpp, reactionParse.cpp, reactionParse.hpp, reactionSubParse.cpp, reactiontype.cpp, reactiontype.hpp, secondary.cpp, makefile.  

(54) Fix bug in calculating gas density; fix bug in JdotE, fluxes and energy densities with subcycling; turn off diagnostics in -nox mode; add maxSigmaVEnergy as Species input parameter; disable mcc subcycling; choose mcc particle-particle target from within 1 grid unit; changes to oxygen reaction dynamics, reactions, and cross sections; xenon and nobel gas reactions; fix bugs in temperature diagnostics; modify nonresonant charge transfer method; main files modified are diffusion_fluid.hpp, diffusion_fluid.cpp, fields.hpp, fields.cpp, fluid.hpp, fluid.cpp, species.hpp, species.cpp, diagnostic_parser.hpp, mcc.hpp, mcc.cpp, oxygen_mcc.hpp, oxygen_mcc.cpp, oxygen_gas.hpp, xenon_gas.hpp, nobelgasxterms_gas.hpp, and hydrocarbons_mcc.cpp

Latest revisions of oopd1, MAL, 11/25/11
---------------------------------------------------------------------------

(53) Add nonresonant charge exchange (Ar+ + Xe -> Ar + Xe+) and ion-ion Coulomb collisions (Ar+ + Ar+; Xe+ + Xe+; Ar+ + Xe+) for argon-xenon system.  Uses momentum transfer Coulomb cross section for ion-ion collisions. Tests: Increase the nonresonant cross section by 100 and run TestArXeChExchange to see the nonresonant charge exchange collisions.  Increase the Coulomb cross section by 1E5 and run the input files TestArCoulomb, TestXeCoulomb, and TestArXeCoulomb, to see the ion-ion collisions.  Files modified: reactionParse.cpp, mcc.hpp, mcc.cpp, reactiontype.hpp, reactiontype.cpp, argon_gas.hpp, xenon_gas.hpp and nobelgasxterms_gas.hpp.

(52) Replace find_sheathwidth in fields.cpp with xpdp1 version; the xpdp1 algorithm gives much more accurate results

(51) oopd1_110505 bug fix in mcc.cpp: in particle_particle and fluid_particle, delete neutral projectile and target particles with energies less than energyThrToAddAsPIC.

(50) oopd1_110111: Added planar inductive heating model (also used by E. Kawamura in xpdp1). Specify surface current [A/m], frequency [Hz], phase [degrees], optional wavespeed [m/s], and nupdateE (default=5000 timesteps) in the PoissonSolver section of the input file.  See the function Fields::update_icp_fields() for model details.  Dumpfile save, restore, and print modified to save the mcc variable aveNcoll_per_step, needed to calculate the inductive electric field from the surface current.  Bug fixes to mcc.cpp and diffusion_fluid.cpp, and corrections to argon_gas.hpp. Run the files TestArICPXXXK0.inp and open Ey diagnostic to see the inductive heating field.  Files modified include: fields.hpp,cpp, diffusion_fluid.hpp,cpp, reaction.cpp, mcc.hpp,cpp, species.cpp, plsmdev.cpp, sptlrgn.cpp, pd1.cpp, argon_gas.hpp

(49) oopd1_110105: Added 4-level argon gas model with ground state (Ar), metastable level (Arm), resonance level (Arr) and higher energy level (Ar4p).  Radiation trapping of the resonance radiation from Arr -> Ar is implemented using the model of P.J. Walsh, Phys. Rev. 116, p. 511(1959). Radiative decay processes are implemented using a "collision" of the radiating species with a nominal "Unit" diffusion fluid species, having a density n=1, which must be declared in the Species section of the input file. See TestRadiationTrapping.inp, TestEmission.inp, and TestArm.inp for examples.  Cross sections for all processes are brought up-to-date by J.T. Gudmundsson.  Bug fixes to mcc.cpp.  Species names added in dumpfile printing. Files modified are: argon_gas.hpp, mcc.hpp, mcc.cpp, oxygen_mcc.cpp, oxygen_mcc.hpp, hydrocarbons_mcc.cpp, hydrocarbons_mcc.hpp, reaction.cpp, diffusion_fluid.hpp, fluid.hpp, reactionParse.cpp, reactionParse.hpp, reactiontype.cpp, reactiontype.hpp, plsmdev.hpp, plsmdev.cpp, species.hpp, species.cpp, and pd1.cpp.

(48) Bug fix to modify names of binned distributions to incorporate boundary location, thus making distinct names for all binned distributions; files modified: dist_binned.cpp

(47) Add keywords "allon" and "allon1D" to Diagnostics section in input file; keyword "alloff" still turns all diagnostics off; files modified are: diagnosticcontrol.cpp, diagnostic_parser.hpp,cpp

(46) Bug fix to correctly calculate the time-average diagnostics v1Ave(x), v2Ave(x), v3Ave(x), T1Ave(x), T2Ave(x), T3Ave(x), TperpAve(x), and TempAve(x); files modified are species.hpp,cpp, fields.hpp,cpp.

(45) New bulk temperature diagnostics T1(t), T2(t), T3(t), Tperp(t) and Temp(t), and new PhysicalNumber(t) diagnostic; files modified are: species.hpp,cpp; diagnosticcontrol.cpp, diagnostic_parser.hpp,cpp.

(44) Bug fix to save and restore the mcc variable "extra" in the dumpfile, which gives the number of excess collisions taken for each reaction group, after each call to the mcc; the new version 1.01 of the dumpfile is compatible with the previous version 1.00; files modified are: reaction.hpp; plasmdev.cpp; sptlrgn.hpp,cpp; pd1.cpp; mcc.hpp,cpp.

--------

(43) Modify code and makefile to enable compilation using the g++ compiler with optimization flag set -O1, -O2, or -O3 (or -fast with Macintosh).  A speed gain of 3.6 times is realized. Calls to inline functions get_r0(), get_r1(), and get_E(i,&E) are replaced by calls in species.cpp, sptlrgn.cpp, and pd1.cpp to non-inline functions getr0(), getr1(), and getE(i,&E). The non-inline functions are defined in boundary.hpp, cpp and fields.hpp, cpp. The makefile has the added line "MODE=optimized", which sets the -O1 optimization flag and removes the warning flag -Wall. To reset the debug mode, use "MODE=debug".

(42) Correct a minor bug in diagnosticcontrol.cpp, to properly evaluate Flux3Ave.

(41) Revise particle flux and energy density diagnostics to properly evaluate these quantities at time t (rather than time t-1/2).  This gives much more accurate computations of JdotE diagnostics, particle fluxes, etc.  Run TestJdotE1DVh32M50V2M0V100703.inp and TestJdotE3DVh32M50V2M0V100703.inp with dumpfile Vh32M50V2M0V100630_87mus.dmp, and display the diagnostics Boundary(0,0)_PwrAve(t) and J1dotE1PwrAve(t) to see the good agreement between the input circuit power and the total calculated JdotE's of the individual species. The velocity v and velocity-square v^2 grid arrays are now accumulated in the particle mover advance_v.hpp (used in particlegrouparray.cpp), and the grid array diagnostics are updated in species::accumulate_vel_moments(), now called, and timed, from sptlrgn.cpp.  Files modified include: advance_v.hpp, particlegrouparray.cpp and hpp, sptlrgn.cpp, species.cpp and hpp,  fields.cpp, grid_inline.hpp, and grid.hpp.

(40) Add dumpfile (version 1.00) and reload (version 1.00) for all particles, circuit and boundary charges, and drives (voltage and current sources). The dumpfile is written in pd1.cpp from the method Dump().  The top level restore from the dumpfile is done in plsmdev.cpp [PlsmDev::reload_dumpfile()], but the main restore is done in sptlrgn.cpp [SpatialRegion::reload_dumpfile_SR()].  The dumpfile can be printed out from SpatialRegion::print_dumpfile().  If desired, this can be called from pd1.cpp by the method Quit(), as a diagnostic. Particles of a given species are reloaded ONLY if the Load section of the input file has a section for that species.  For example, for an argon simulation with Load sections for electrons and Ar+,  if the mcc or boundary emission generates argon neutral PIC particles, then these will not be reloaded from the dumpfile.  To reload these, include a load section, Load {species = Argon }, in the input file.  See RFDischArReloadAll.inp for an example.  Modifications to the files: pd1, plsmdev, sptlrgn, species, particlegroup, load, boundary, conductor, circuit, and drive. In version 1.00,  fluid species densities are not reloaded, as these are currently all static.  When time-varying fluid densities are implemented, then they will need to be saved and reloaded. This will be done in future dumpfile versions. To generate a dumpfile after 100000 timesteps the fastest way, set "alloff" and set "naverage" >  100000 in the DiagnosticControl section of the input file, then run in "no X" mode:
./pd1 -nox -i inp/myinputfile.inp -s 100000 -dp 100000
To then reload the dumpfile myinputfile.dmp and gather diagnostics, unset "alloff" and set "naverage" to the desired number of steps, then run:
./pd1 -i inp/myinputfile.inp -d inp/myinputfile.dmp
One can generate a dumpfile at any time by clicking "Save" in the Control Panel.

(39) Kill emitter.cpp bug for good by resetting a mover-generated x+dx that is out of range of the grid, to be within range (see item 34 below)

(38) Add O2-O2 and O-O like particle (elastic) scattering for oxygen simulations; files modified are oxygen_gas.hpp, oxygen_mcc.cpp, reactiontype.hpp, reactiontype.cpp, and reaction.cpp.  These are important reactions in degrading the energy and isotropizing the angle of fast neutrals created in the sheaths.  A constant (Van der Waals radius) cross section is used for these reactions.

(37) Add subcycling for particle mover, weighting particles to the grid, mcc, and phase space diagnostics; files modified are particlegrouparray.cpp, advance_v.hpp, species.cpp, mcc.cpp, and diagnostic_phase.cpp (search on "subcycle").  The default value of subcycle = 1 in the Species section of the input file.  Particles are moved, weighted to the grid, and phase space is updated once every subcycle timesteps; binary collisions are realized once every min(subcycle1, subcycle2) timesteps; boundary and load calls to the particle mover are not subcycled.  Typically one can use subcycle = 20 for all heavy particles (retaining subcycle = 1 for electrons).

(36) Fixed bug in MCC::particle_particle(), which caused an erratic hangup, due to a flaw in the mcc logic when dealing with small numbers of projectiles and targets; the particle_particle mcc went into an infinite loop when it could not find a target collision partner to match the projectile.

(35) Add Any_eDetachment to oxygen_mcc.hpp and cpp, to correctly treat the electron detachment reaction e + O-  ->  2e + O

(34) Squash the segmentation fault erratically produced, due to a bug in particle mover 5 in emitter.cpp.  This bug occasionally produces a value of x+dx that is not within the range of the grid, 0<x<ng. The default mover selected in secondary.cpp is now mover 4; an error message is now produced and execution aborted whenever particles are to be added that are not in the range of the grid.  To reproduce this bug, set meth = 5 in Secondary in the input file Oxygen_Test3D.inp.

(33) Add 3d energy-angle binned diagnostics; files changed are dist_binned.hpp, dist_binned.cpp, diagnostic3d.hpp, and diagnostic3d.cpp.  Default behavior if "anglebins" is set equal to an interger >2 in the the input file is to plot f(energy), f(angle), and f(energy,angle).  To suppress the 3d plot, use the input file variable "uncoupled".

(32) Minor changes in poissonfield.cpp, diagnosticcontrol.cpp, and boundary.cpp to rename some diagnostics.

(31) Additions to species.hpp, species.cpp, diagnostic_fe.hpp, diagnostic_fe.cpp, and diagnosticcontrol.cpp: add J1dotE1PwrAve(t) diagnostic, etc, calculated from J1dotE1, J2dotE2, J3dotE3, and/or JdotE, so one can see time-average power absorbed by all species versus time.  The corresponding grid diagnostic (e.g., "J1dotE1") must be selected in the DiagnosticControl section of the input file, and naverage must be greater than 1.

(30) Additions to circuit.hpp, circuit.cpp, conductor.hpp, conductor.cpp, boundary.hpp and boundary.cpp: add circuit current Ickt(t), power Pwr(t) supplied by circuit, and average power AvePwr(t) supplied by circuit, at each conducting boundary.  The average power is displayed at the end of each naverage time interval (during the entire following time interval) if naverage > 1.  The general RLC circuit, ideal voltage source, and ideal current source are all treated.  Minor bug fixed in boundary so that input file Diagnostic open commands work for boundary quantities (the name no longer contains a blank character).

(29) Fixed longstanding bug in boundary.cpp, in adding phi(t) diagnostic when j0=j1

(28) Final corrections to oxygen_mcc.cpp by JTG and MAL: removed the unused Any_DissociationInFour; corrected both versions of Any_DissociationInThree and insured they work correctly and satisfy energy conservation; Corrected the 2nd version of Any_DissociationInTwo and insured it works correctly and satisfies energy conservation.  As for Ar and Xe, creation of oxygen PIC neutrals can be controlled using the parameter energyThrToAddAsPIC in the Species section of the input file. Default is to always add PIC neutrals created by collisions.

(27) Final revision to xenon_gas.hpp, and addition of 4p excitation states to argon_gas.hpp by JTG

(26) Newest argon_gas.hpp, xenon_gas.hpp, and nobelgasxterms_gas.hpp sent by JTG on 11:20 pm on 11/24/09 are included. Minor correction to xenon_gas.hpp was made (to eliminate a compile-time warning).

(25) Argon/Xenon mixtures implemented by creating a cross term file: nobelgasxterms_gas.hpp, JTG

(24) oxygen_mcc.cpp: Found (but not fixed) bugs in Any_DissociationInThree and AnyDissociationInTwo in oxygen_mcc.cpp. Dissociative ionization has been corrected; Any_eIonization and Any_eIonization2 were corrected by putting the appropriate DELETE_PARTICLES statements in the individual cross section calls for edetachment, eIonizationo, and eIonizationo2. 

(23) Modified most reactions in oxygen_mcc.cpp to incorporate the local floating point variable "energyThrToAddAsPIC" for creation of neutral particles.  One can now see creation of oxygen PIC species such as O2_vib1 and O_1D; see the input file Oxygen_AllPICNeutrals.inp.

(22) Modify and tested particle_particle() in the mcc.cpp to incorporate like-particle scattering for particle-particle like-particle reactions.

(21) Modify reactiontype.hpp, reactiontype.cpp, reaction.cpp, and argon_gas.hpp to incorporate like-particle scattering for particle-fluid like-particle reactions.  Modify argon_gas.hpp to add neutral elastic scattering (Ar + Ar -> Ar + Ar).

(20) Added and tested anymassElastic to mcc.hpp and mcc.cpp. Also, one (unlikely) special case is now treated correctly in halfmassElastic.

(19) Added the local floating point variable energyThrToAddAsPIC to Species in the input file, to control the generation of fast PIC neutrals created by collisions; changes in species.hpp, species.cpp, mcc.hpp, and mcc.cpp.  PIC neutrals are generated by collisions only if the neutral energy exceeds the energy threshold. The default value is energyThrToAddAsPIC = 0, so the default behavior is that neutrals are always added as PIC particles. The functions samemassElastic, halfmassElastic, anymassElastic, and ElectronExchange in mcc.cpp were changed to generate these fast PIC neutrals, 11/21/09

(18) New argon_gas.hpp and xenon_gas.hpp files provided by JTG, 11/20/09

(17) Fixed bug in E_ELASTIC so that the oxygen_gas.hpp file is now consistent with the argon_gas.hpp file; changes in reaction.cpp, mcc.cpp, and oxygen_gas.hpp, 11/20/09

(16) Added the local variable useSecDefaultWeight to Species in the input file, to force the generation of mcc-created secondary species with the default np2c's in the input file; default behavior is to generated secondary species with the smaller of the weights of the projectile and target.

(15) Fixed bug in ovector.hpp Vector3 calculation of angle between two vectors: the square root was missing; angular distributions are now calculated correctly.

(14) Fixed bug in diagnostic control.cpp so that species grid diagnostics now have the same colors as phase space, number(t), etc.

(13) Fixed nasty bug in particle-particle collisions in mcc.cpp.  This bug caused an uncontrollable growth in the number of simulation particles after a very long simulation time.  This was due to an incorrect algorithm to determine the weight for which to add new particles after a particle-particle collision. A much simpler algorithm is now implemented.

(12) dist_binned.cpp was completed, so the angular distributions can now be plotted, as well as the energy distributions.

(11) Bug fixed in emitter::emit_particles; a new function dXtogriddX was added in grid.cpp to fix this, and get the secondary emission from the surfaces working properly.

(10) If species grid diagnostics are alloff, or no diagnostic is requested, then species_density, species_scratch_density, species_ave_density, and species_temp_density are allocated and deallocated in species.cpp as one contiguous array.  If any 1D or 3D velocity moment diagnostic is requested in the input file, then all the required diagnostic arrays, including the above four density arrays, (as many as 67 arrays for each species, each of length ng) are allocated and deallocated as one contiguous array in diagnosticcontrol.cpp.  

(9) Added d0flag, d1flag, and d3flag, used by particlegrouparray to test for which WeightLinear function should be used: WeightLinear, WeightLinear1D, or WeightLinear3D, depending on whether to accumulate the density, the density and 1D velocity moments, or the density and 3D velocity moments.

(8) Revisions  to make the code run faster: Scratch arrays are used in PGA for fast accumulate_density calculations. Three new WeightLinear functions were added in grid_inline.hpp.  One weights unity to the grid for each particle.  A second, WeightLinear1D, weights density and the v1 and v1sq moments; a third, WeightLinear3D, weights density and the six moments v1, v2, v3, v1sq, v2sq, v3sq.  A very good improvement in speed with 3D weightings is obtained.

(7) Modifications to mcc.cpp.  The original method MCC::newcoordiante in mcc.cpp, which calculates the new normalized velocity vector for a scattered particle, was not correct; it led to the creation of nonphysical particle fluxes in the "2" direction (but not in the "3" direction!).  This was due to an incorrect calculation of cosT0 and sinT0. It has been corrected.  Also, the decision to use relativistic or nonrelativistic dynamics for eElastic and eExcitation scattering processes was not correctly made.  This has been corrected.

(6) Instantaneous "particleflux1"= <n*v1>,  "particleflux2"=<n*v2>, "particleflux3", "J1dotE1"=<e*n*v1*E1>,  "J2dotE2", "J3dotE3",  "JdotE", "energydensity1" =<0.5*M*n*v1*v1/e>,  "energydensity2", energydensity3",  "energydensity", "averagevelocity1"=<n*v1>/n, "averagevelocity2", "averagevelocity3",  "temperature1"=M*[<n*v1*v1>/n - <n*v1>*<n*v1>/(n*n)]/e, "temperature2", "temperature3", "temperatureperp", and "temperature" can be calculated and displayed for all species.  Here "1" refers to the simulation direction (along x), "2" and "3" are the two perpendicular directions, and < > denotes the usual averaging over the particles.  To both calculate and display these quantities, the appropriate name must appear in the input file in the DiagnosticControl section (see sample input files).  If the name does not appear, than the calculation and display of the quantity is not done. The name "alloff" can be used in the DiagnosticControl section to suppress the calculation and display of all these quantities. (Note that density n, charge density rho, electric field E1, and potential phi are always calculated and displayed, as these are required by the PIC mover.)

(5) New version of grid time averages for all species grid diagnostics, as well as potential phi and charge density rho. These are both calculated and displayed when the DiagnosticControl integer parameter "naverage" is set to a value greater than 1. These averages are not calculated or displayed when naverage < 2.  The averages are taken and displayed over successive groups of naverage timesteps.  (Normally one would set naverage to some integer multiple of the rf period). The default value of naverage is 1 billion, corresponding to a cumulative time average.  The main work is done in the method grid_time_average, which has been added to fields, and is called both in fields (for phiave and rhoave) and in species  (for the particle averages).

(4) Setting a new DiagnosticControl integer parameter "ngriddiagupdate" to some integer N updates the diagnostics windows every N timesteps. (Setting N > 1 can speed up the simulation.) The default value  is N = 1.  Setting N=10 is useful to increase the speed of the simulation.

(3) A sheathwidth diagnostic for both sheaths (at grid 0  for n=0 and grid ng-1 for n=1) was added, mainly with the new method Scalar find_sheath_width(int n) in fields.cpp.  This method is called in fields.  It assumes that space_rho has been updated and that the grid is uniform. The algorithm is to increment (decrement) the grid starting at the left (right) grid 0 (ng-1) until one finds for k0+1 successive grid steps satisfying the sheath condition, e.g., n–(t) = Lsheath*n+(t) for the left hand sheath.  I have set k0 = 3. This gives a fairly consistent and reliable calculation for the time-varying sheath widths.The other files were modified for control and access to the diagnostic parameters.  The sheath width diagnostic is controlled by the DiagnosticControl scalar input parameters Lsheath (left hand sheath) and Rsheath (for right hand sheath).  Default is Lsheath = Rsheath  = 0., which turns off the calculation and display of the sheath widths.  Setting Lsheath = 0.5, for example, finds the left hand sheath width where n–(t) = 0.5*n+(t).

(2) Corrected version of xpdp1 oxygen sections in oxygen_gas.hpp, due to Jon Gudmundson.  Jon added the halfmassElastic function and also neutral scattering of O-atoms off of O2 molecules.  Other corrections were made.  Jon is working on an updated set of oxygen cross sections, improving on those in xpdp1, but this has not yet been debugged.  The mcc was revised and can now handle ion-ion and neutral-neutral collisions (done previously.)

(1) Other bug fixes were previously made, ie, to conductor.cpp, fixing a bug for the cylindrical voltage-driven case with general RLC circuit.

The modified files all have modification dates after February 6. All changes made by me are labeled in the files with my initials:  MAL. Jon also annotated his changes with his initials: JTG. Jon made changes to the following eight files: oxygen_gas.hpp, oxygen_mcc.hpp, oxygen_mcc.cpp, mcc.hpp, mcc.cpp, reaction.hpp, reactiontype.hpp, and reactiontype.cpp. (I made changes to some of these files also).
