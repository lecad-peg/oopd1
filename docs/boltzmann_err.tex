\documentclass{article}
\usepackage{graphicx}
\usepackage{times}

\setlength{\topmargin}{-0.9in} 
\setlength{\oddsidemargin}{-0.1in} 
\setlength{\textwidth}{6.9in} 
\setlength{\textheight}{9.4in} 

\begin{document}

\title{Error Conditions for the Boltzmann Solver}
\author{Jeff Hammel}
\date{\today}

\maketitle

{\it Cartwright et al.} \cite{CWBOLTZ} 
uses convergence
for {\tt pdp1} with Boltzmann electrons:
\begin{equation}
\frac{|| \nabla^2 \phi + \rho / \epsilon 
||_{L_2}}{|| \textrm{boundary condition} + \rho / \epsilon
||_{L_2}} \le \varepsilon
\label{CART_CONVERGENCE}
\end{equation}

The boundary condition used is not specified by
{\it Cartwright et al.} \cite{CWBOLTZ}.

A more general convergence criterion is
\begin{equation}
\sqrt{
\frac{\int \left( \nabla \cdot \epsilon \nabla \phi 
+ \rho \right)^2 V dV}{\int
 \rho^2 V dV
+ \int \sigma^2 A dA} \le \varepsilon
\label{HAMM_CONVERGENCE}
\end{equation}

Units:  
\begin{itemize}
\item numerator: $\rho^2 V$ = [C$^2$/m$^3$]
\item surface charge: $\sigma^2 A$ = [C$^2$/m$^2$]
\item \underline{These aren't compatible!}
\end{itemize}

Equation \ref{HAMM_CONVERGENCE} works for any coordinate 
system and takes into account variation in dielectric
constant, $\epsilon$.  The boundary conditions are
explicitly accounted for in the integration of surface
charge, $\int \sigma^2 dA$.  Note that this integral is
over all surfaces.  Both the numerator and denominator in
Equation \ref{HAMM_CONVERGENCE} have units of charge (coulombs).

Note that neither Equation \ref{CART_CONVERGENCE} nor
Equation \ref{HAMM_CONVERGENCE} work for the analytically
neutral case.

The discretized (finite-difference) approximation of
$\nabla \cdot \left( \epsilon \nabla \phi \right)$ is
used.  The integration of volumes is converted into 
discrete sums.

\begin{equation}
\left[ A \right] \left\{ \phi \right\} = \left\{ \rho \right\}
\label{MATRIXEQ}
\end{equation}
Equation \ref{MATRIXEQ} ignores boundary condition, which would
modify the matrix $A$ and the right-hand side, $\rho$.

{\bf Methodology: }
\begin{enumerate}
\item Perform one step of Newton's method
\begin{enumerate}
\item Modify matrix and right hand side:
\begin{equation}
\left(
\left[ A \right]
+ \sum_{j}^{N} \frac{q_{j}^2 n_{0, j}}{e T_j}
\exp \left( 
- \frac{-q_j \phi^{i}}{e T_j} 
\right)  
\right) \phi^{i + 1}
= \rho_{ext} + 
\sum_{j}^{N} q_j n_{0 , j} \exp
\left( - \frac{q_j \phi^{i}}{e T_j} \right)
+ \frac{q_{j}^2 n_{0 , j} \phi^i}{e T_j}
\exp
\left( - \frac{q_j \phi^{i}}{e T_j} \right)
\end{equation}
\item solve for $\phi^{i+1}$ ({\it tridiagonal solve})
\end{enumerate}
\item Error checking:
\begin{enumerate}
\item using fresh matrix, subtract $\rho$
from $[ A ] \{ \phi^{i + 1}_{computed} \}$
\item square each quantity and sum to compute the 
{\bf numerator}
\item numerically integrate the total charge squared to find the 
{\bf denominator}. This charge includes:
\begin{itemize}
\item space charge
\item surface charge ({\it boundary conditions})
\end{itemize}
\item divide {\bf numerator} by {\bf denominator} and compared to 
$\varepsilon$
\end{enumerate}
\item if not converged, continue iteration
\end{enumerate}

\begin{figure}
\includegraphics[width=0.5 \linewidth]{poisson_solve}
\caption{Grid used for Poisson solver}
\label{POISSONGRID}
\end{figure}

The surface charge, $Q_i$ [C], 
 for conductors is found from knowledge of
electrostatic potential, $\phi$:
\begin{equation}
Q_i = A_{i} \sigma_{i}
\end{equation}
$i$ is the node of the conductor.  

For a thin conductor, 
\begin{equation}
Q_i = \underbrace{E_{i + 1/2} A_{i + 1/2} \epsilon_{i + 1/2}
- \rho_i V_{+}}_{+}
+ \underbrace{ \left( - E_{i - 1/2} A_{i + 1/2} \epsilon_{i - 1/2}
\right) - \rho_i V_{-}}_{-}
\label{QCONDUCTOR}
\end{equation}

$E$ is the on-axis component of the electric field.
$V_{+}$ is the volume between $i$ and $i + 1/2$
$V_{-}$ is the volume between $i - 1/2$ and $i$.
$\rho_i$ is the space charge at node $i$. [$V \rho$ is a simple
approximation to the integral $\int \rho dV$, which could
be used for higher accuracy. $\epsilon_{\left( i + 1/2 \right) / 
\left(i - 1/2 \right)}$ is the permitivity at $i + 1/2$ and
$i - 1/2$, respectively. 

This is illustrated in Figure \ref{POISSONGRID}.

This is from Gauss's law with the
assertation that the electric field is zero interior to the conductor.
 If the conductor had finite thickness (or
equivalently, was on a system boundary, the appropriate term
$+$ or $-$ could be eliminated.

Using the midpoint rule for $E$ (e.g. 
$E_{i+1/2} = \left( \phi_{i} - \phi_{i + 1} \right)/ 
\left( \Delta x \right) + O \left( \Delta x ^2 \right)$):

\begin{equation}
Q_i = \underbrace{
\left( \frac{\phi_{i} - \phi_{i + 1}}{\Delta x_{i + 1/2}} \right)
A_{i + 1/2} \epsilon_{i + 1/2}
- \rho_i V_{+}}_{+} 
+ \underbrace{ \left( \frac{\phi_{i} - \phi_{i - 1}}{\Delta x_{i - 1/2}} 
\right)
A_{i + 1/2} \epsilon_{i - 1/2}
 - \rho_i V_{-}}_{-}
\label{QCONDUCTORDISCRETE}
\end{equation}

\bibliographystyle{plain}
\bibliography{pd1}

\end{document}
