* Let the SEEC = 1 and there is a particle crossing the boundary.

 advance v/x: v_(n-1/2) -> v_(n+1/2), x_n -> x_(n+1)
   -> generate a secondary electron (#se_b) at the boundary
   -> push #se_b
       for the fractional timestep of f*delta_t
   -> push #se_b for one timestep
