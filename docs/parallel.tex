\section{Parallelization}

{\bf Introduction to Parallelization of {\tt oopd1}}

{\bf oopd1}, typical of Particle-In-Cell (PIC)
programs, uses an explicit particle-push to advance
particle positions and velocity.  The use of particles
to represent phase-space by collocation is much cheaper
than Vlasov/Boltzmann methods which represent full
six dimensional phase space and allows arbitrary plasma systems, 
unlike magnetohydrodynamic methods.

Space and time discretization must be chosen to accurate model electron
scales.  For a system where $NP \equiv$ the number of
particles in a cell, the total amount of work for
$NT$ timesteps is

\begin{equation}
W = NT \left[
NC \cdot NP \cdot W_p + NC \cdot W_c
\right]
\end{equation}
In this equation, $NC$ is the number of cells, $W_p$ is
the work per particle per timesteps
($O ( 4 - 50 )$, depending
on the physics used), and $W_c$ is the work per cell
per timestep ($O ( 3 - 50 )$).
For $W_p \approx W_c \approx 10$ and
$NP > 10$,
$NP \gg NC$ and the expression becomes

\begin{equation}
W = NT \cdot NC \cdot NP \cdot W_p
\end{equation}

Typical numbers for systems of interests 
(e.g., a plasma etch reactor)
are $NT = 10^7$,
$NP = 100$, and
$NC = 10^4$.
Using $W_p = 10$ gives the total work
$W = 10^{14}$ floating point operations.
For a 2GHz computer, at
full efficiency this would take 
50,000s $\approx$ 14 hours to simulate.
In practice, much lower efficiencies are
observed. 

In order to obtain a speedup of the order
of the number of processors, the
Message Passing Interface (MPI) was
used within the C++ code to facilitate communication
between processors.  

{\bf Parallelization of PIC Particle Push}

Particles used to represent plasma
phase space in Particle-In-Cell (PIC)
codes are associated with the {\it SpatialRegion}
which physically contains it.  When a particle 
strikes the boundary of the {\it SpatialRegion},
the needed particle data is stored in a buffer
of particle data partioned by species.  The particle
data is then deleted from the local processor.  After
moving all particles {\tt MPI\_Isend} is used to 
asynchronously communicate particle data between neighboring
processors.  While waiting for the MPI communication
to complete, the local particles are weighted to the mesh
for use in the Poisson solve.  The use of asynchronous 
communication and overlapping work should give
a speedup factor near the number of processors for
the particle work associated with the PIC methodology.


{\bf Parallelization of Poisson's equation using Superposition}

\begin{figure}
\includegraphics[width=0.99 \linewidth]{oopd1_laplace}
\caption{Solution of Laplace's equation for a planar system with
the conductors at ${\bf x_L}$ and ${\bf x_R}$ each biased to unity potential
with the other held at zero.  Three {\it SpatialRegion}s are
shown with boundaries between them denoted by dashed lines.  Dotted
lines denote the ``ghost'' nodes used in the solution of Poisson's 
equation.}
\label{PLANARLAPLACE}
\end{figure}

\begin{figure}
\includegraphics[width=0.99 \linewidth]{oopd1_poisson}
\caption{Solution of Poisson's equation for a planar system using
a non-zero charge density in the {\it SpatialRegion}
between ${\bf x_{S 1}}$
and ${\bf x_{S 2}}$ with the grounded conductors
at ${\bf x_L}$ and ${\bf x_R}$.}
\label{PLANARPOISSON}
\end{figure}

If the permitivity is constant between two
conductors, the electrostatic equation reduces to
\begin{equation}
\nabla^2 \Phi = \frac{\rho}{\epsilon}
\end{equation}
In this case, the total potential  may be written as
the superposition of the driving potential (the potential due
to the charge on the bounding electrodes) as well as
the sum of the potentials due to various groups of charge
sources with the electrodes grounded ($\Phi = 0$).
\begin{equation}
\Phi \left( x \right) = \sum_j \Phi_{j \left( drive \right)}  \left( x
\right)
+ \sum_i \Phi_{i \left( charge \right)} \left( x \right)
\label{SUPERPOSITION}
\end{equation}
In Equation \ref{SUPERPOSITION}, 
the $j$ summation is over electrodes (usually two)
and the $i$ summation is over the 
different groups of charges.  In {\bf oopd1}, particles are
divided based on {\it SpatialRegion}s (an {\bf oopd1} class).
The sum is then over the different {\it SpatialRegion}s.
The potential fields due to conductor boundaries is shown in
Figure \ref{PLANARLAPLACE}.  The potential field due to
a charge distribution within a {\it SpatialRegion} is shown in
Figure \ref{PLANARPOISSON}.

Since the Laplace (vacuum) solution is known from 
geometry, the value of the driving potentials is sufficient to find
their contribution to the electrostatic potential within
a {\it SpatialRegion}.  

The behavior of the field outside of a {\it SpatialRegion} for
solution of Poisson's equation is known from the potential 
non the {\it SpatialRegion} boundary and the geometry 
dependent behavior
of the fields in vacuum.

{\bf Planar systems:}

\begin{equation}
\frac{d^2 \Phi}{dx^2} = 0
\end{equation}
The potential is then linear outside the {\it SpatialRegion} of interest.
For node index $i$, ghost nodes $i-1$ and $i+1$ are introduced for 
$i$ on a left or right side {\it SpatialRegion} boundary, respectively.

For node $i$ on the left hand side (see Figure \ref{PLANARPOISSON}):
\begin{equation}
\Phi_{i-1} = \Phi_i - \frac{h_i \Phi_i}{x_{s 1} - x_L} =
\left[ 1 - \frac{h_i}{x_{s 1} - x_L} \right] \Phi_i
\end{equation}
In this equation, $h_i$ is the mesh spacing between $x_i$
and $x_{i + 1}$.

For the right hand side:
\begin{equation}
\Phi_{i+1} = \left[ 1 - \frac{h_i}{x_R - x_{s 2}} \right] \Phi_i
\end{equation}

{\bf Computational Procedure:}

\begin{enumerate}
\item Move particles using existing field
\item Communicate particles moved across
{\it SpatialRegion} boundaries to neighboring nodes
\item Weight particles to grid
\item Solve Poisson's equation using appropriate boundary conditions
\item Update circuit
\item Broadcast values of calculated $\Phi$ from each processor
to each processor (within two electrodes)
\item Update local $\Phi$ values using superposition
\item Communicate local $\Phi$'s to neighboring processors to
calculate $E$-field
\item Calculate $E$-field
\end{enumerate}
