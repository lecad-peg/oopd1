knownbugs.txt -- known bugs in oopd1.  Please update this list
(both adding entries for found bugs and deleting entries for
fixed bugs).

Format:
* bug description (initials of bug finder, when bug was documented)

-----------
* mcc pointer will be overwritten if multiple MCC entries are found in 
the input file.  Can easily be fixed, but unsure about what
infrastructure we want here (JH, 7/14/04)

* Binned<int D, class T>::operator= displays an error as
illustrated by ndarray.cpp [mybinned2.txt].  probably a roundoff
error regarding indexing (JH, 7/22/05)

* in move_uniform.hpp: 
to avoid absorption of secondary species created at the boundary, we
have: "if (cell == x) cell--;". Problem occurs because of single
precision. For example, when dx = -1e-6, and x = 100, Scalar sum = x+dx
verifies: int (sum) == cell, not cell-1 ! It means that the particle
remains inside the boundary and does not get absorbed...it can go out
without being deleted.
  -- It requires more explanation. Not understandable. Also, please include
    and specify the simplest the inputfile having that problem.
    (mindgame, Dec. 8, 2006)
Attempt to solve has been: Scalar sum = x+dx; if(cell == x &&int(sum)<
cell) cell --. But it is in conflict with the choice to make
secondary_iter a class variable, rather than to confine it inside a
function. Indeed if a secondary is re-absorbed, it will eventually
create new secondary species => secondary_iter will be reset, even if
the process was not over for the first impacting species.
Possible solutions: don't make secondary_iter a class variable, or
more safe: emit a secondary particle INSIDE the plasma (CN, 12/01/06).
 

