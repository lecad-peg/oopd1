\documentclass[12pt]{article}
\usepackage{graphicx}
\usepackage{times}

\newcommand{\pdiv}[2]{\frac{\partial #1}{\partial #2}}

 \pdfpagewidth 8.5in
\pdfpageheight 11in
\topmargin 0in
\headheight 0in
\headsep 0in
\textheight 7.7in
\textwidth 6.5in
\oddsidemargin 0in
\evensidemargin 0in

\begin{document}

\begin{equation}
{\bf \nabla \cdot D } = \rho
%\caption{Gauss's law}
\end{equation}
\begin{equation}
{\bf \nabla \times E } = - \pdiv{\bf B}{t} \rightarrow 0 
\Rightarrow {\bf E} = - \nabla \Phi
%\caption{Definition of electrostatic potential}
\end{equation}
\begin{equation}
{\bf D} = \epsilon {\bf E}
%\caption{Constitutive relation of {\bf E} and {\bf D}}
\label{CONSTITUTIVE}
\end{equation}

\begin{figure}
\includegraphics{poisson_solve}
\caption{Cells used in Poisson solve for node $i$}
\end{figure}

In planar coordinates, $\nabla \rightarrow d / dx$, and
in cylindrical and spherical coordinates,
$\nabla \rightarrow d / dr$.  At midcell locations
to the left ($i - 1/2$) and right ($i + 1/2$ of node $i$,
the electric field may be expressed using the midpoint rule:
\begin{equation}
\begin{array}{cc}
{\bf E}_{i - 1/2} = \frac{\Phi_{i-1} - \Phi_{i}}{\Delta x_-} 
+ O \left( \Delta x_- \right)^2 
; 
& 
{\bf E}_{i + 1/2} = \frac{\Phi_{i} - \Phi_{i+1}}{\Delta x_+}
+ O \left( \Delta x_+ \right)^2 
\end{array}
\end{equation}
$\Delta x_-$ and $\Delta x_+$ equal
$x_i - x_{i-1}$  and
$x_{i+1} - x_{i}$, respectively.
(Note: $x$ is used is the generic spatial independent variable
for planar and curvilinear coordinate systems.)

The integral form of Gauss's law may be written:
\begin{equation}
\oint {\bf D \cdot } d{\bf S} = Q
\label{INTEGRAL_GAUSS}
\end{equation}
$Q$ is the total charge enclosed inside the surface $S$. 

Let $\sigma_-$ and $\sigma_+$ be the surface charge
density on the left and right sides of node $i$, respectively.

The fluxes of ${\bf D}$ out of the volume to the left and the right
will each be individually considered with the assumption that the
portion of the volume not considered is charge-free and field-free.
The general case may be found by the addition of the fields and charges
not considered in each disparate formulation.

Using the flux out at $i - 1/2$ is then:
\begin{equation}
- A_{i - 1/2} D_{i - 1/2} + A_{i} D_i = A_{i} \sigma_- + 
\int_{x - 1/2}^{x} \rho_i dV
\label{LEFTFLUX}
\end{equation}
The negative sign is due to the positive sense of $D$.
$A$ is the surface area: $A \left( x \right)
= A_0 = $ constant in planar coordinates, 
$A \left( r \right) = 
2 \pi r z$ in cylindrical coordinates, 
and
$A \left( r \right) = 
4 \pi r^2$ in spherical coordinates.
The integral over volume, becomes:
$dV = A_0 dx$ in planar coordinates,
$dV = 2 \pi z r dr$ in cylindrical coordinates,
and
$dV = 4 \pi r^2 dr$ in spherical coordinates.
Doing the integrals assuming $\rho_i$ is constant in the
volume
and substituting into
Equation \ref{LEFTFLUX}, one obtains:
\begin{description}
\item[Planar:]
\begin{equation}
- D_{i - 1/2} +  D_i = \sigma_- + \rho_i \frac{\Delta x_-}{2}
\end{equation}
\item[Cylindrical:]
\begin{equation}
- r_{1 - 1/2} D_{i - 1/2} + r_i D_i = r_i \sigma_-
+ \frac{\rho_i}{2} \left( r_{i}^2 - r_{i - 1/2}^2 \right)
\end{equation}
\item[Spherical:]
\begin{equation}
- r_{1 - 1/2}^2 D_{i - 1/2} + r_{i}^2 D_{i}
= r_{i}^2 \sigma_-
+ \frac{\rho_i}{3} \left( r_{i}^3 - r_{i - 1/2}^3 \right)
\end{equation}
\end{description}

In general:
\begin{eqnarray}
- x_{i - 1/2}^n D_{i - 1/2} + x_{i}^n D_{i} 
= x_{i}^n \sigma_-
+ \frac{\rho_i}{n+1}\left( x_{i}^{n+1} - x_{i - 1/2}^{n+1} \right)
\nonumber \\
- x_{i - 1/2}^n \epsilon_{i - 1/2}
\left( \frac{\Phi_{i-1} - \Phi_{i}}{\Delta x_-} 
+ O \left( \Delta x_- \right)^2 \right)
 + x_{i}^n D_{i} 
= x_{i}^n \sigma_-
+ \frac{\rho_i}{n+1}\left( x_{i}^{n+1} - x_{i - 1/2}^{n+1} \right)
\label{LEFTFLUXGENERAL}
\end{eqnarray}
where $n$ corresponds to the power of $x$ whereby the
area expands ($n = 0$ for planar,
$n = 1$ for cylindrical,
and
$n = 2$ for spherical).

Similarly, for the flux to the right, one obtains:
\begin{description}
\item[Planar:]
\begin{equation}
D_{i + 1/2} - D_{i} = \sigma_+ + \rho_i \frac{\Delta x_+}{2}
\end{equation}
\item[Cylindrical:]
\begin{equation}
 r_{i + 1/2} D_{i + 1/2} - 
 r_{i} D_{i}
= r_i \sigma_+
+ \frac{\rho_i}{2} \left( r_{i + 1/2}^2 - r_{i}^2 \right)
\end{equation}
\item[Spherical:]
\begin{equation}
 r_{i + 1/2}^2 D_{i + 1/2} - r_{i}^2 D_{i}
= r_{i}^2 \sigma_+
+ \frac{\rho_i}{3} \left( r_{i + 1/2}^3 - r_{i}^3 \right)
\end{equation}
\end{description}

In general:
\begin{eqnarray}
x_{i + 1/2}^n D_{i + 1/2} 
- x_{i}^n D_{i} 
= x_{i}^n \sigma_+
+ \frac{\rho_i}{n+1}\left( x_{i + 1/2}^{n+1} - x_{i}^{n+1} \right)
\nonumber \\
x_{i + 1/2}^n \epsilon_{i + 1/2}
\left( \frac{\Phi_{i} - \Phi_{i+1}}{\Delta x_+} 
+ O \left( \Delta x_+ \right)^2
\right) -  x_{i}^n D_{i} 
= x_{i}^n \sigma_+
+ \frac{\rho_i}{n+1} \left( x_{i + 1/2}^{n+1} - x_{i}^{n+1} \right)
\label{RIGHTFLUXGENERAL}
\end{eqnarray}

If there is no electric flux to the right of node $i$, 
Equation \ref{LEFTFLUXGENERAL} may be used
with $D_i = 0$.  If there is
no electric flux to the left of node $i$, Equation
\ref{RIGHTFLUXGENERAL} may be used with $D_i = 0$.  
This physically corresponds
to a conductor on the left or right hand sides of the system or
a conductor with finite thickness internal to the system.
(as the
penetrating field is zero).  This also physically corresponds to
a dielectric bounding the system with a prescribed $E = 0$ 
($D = 0$) condition outside the system (Neumann boundary condition).
For bounding dielectrics, any value of $E_i$ or $D_i$ may
be prescribed as the boundary condition using
Equations \ref{INTEGRAL_GAUSS} and \ref{LEFTFLUXGENERAL} or
\ref{RIGHTFLUXGENERAL}.

For an internal cell, the fluxes in both direction must be accounted
for.  Adding Equations \ref{LEFTFLUXGENERAL} and 
\ref{RIGHTFLUXGENERAL}, the $D_i$ terms cancel and
one obtains a discretized expression for
Gauss's law:
\begin{eqnarray}
x_{i + 1/2}^n \epsilon_{i + 1/2}
\left( \frac{\Phi_{i} - \Phi_{i+1}}{\Delta x_+} 
+ O \left( \Delta x_+ \right)^2 \right)
-
x_{i - 1/2}^n \epsilon_{i - 1/2}
\left( \frac{\Phi_{i-1} - \Phi_{i}}{\Delta x_-} 
+ O \left( \Delta x_- \right)^2 \right)
\nonumber \\
= x_{i}^{n} \left( \sigma_+ + \sigma_- \right)
+ \frac{\rho_i}{n + 1} \left(
x_{i + 1/2}^{n+1} - x_{i - 1/2}^{n+1}
\right)
\label{DISCRETIZEDGAUSS}
\end{eqnarray}

Let $K \equiv \left( x_{i}^{n} \left( n + 1 \right) \right)
/ \left( x_{i + 1/2}^{n+1} - x_{i - 1/2}^{n+1} \right)$.
Equation \ref{DISCRETIZEDGAUSS} may be rearranged (dropping
the truncation error terms):
\begin{eqnarray}
- \frac{n + 1}{x_{i + 1/2}^{n+1} - x_{i - 1/2}^{n+1}}
\left(
x_{i + 1/2}^n \epsilon_{i + 1/2}
\left( \frac{\Phi_{i+1} - \Phi_{i}}{\Delta x_+} \right)
- 
x_{i - 1/2}^n \epsilon_{i - 1/2}
\left( \frac{\Phi_{i} - \Phi_{i-1}}{\Delta x_-} \right)
\right) 
\nonumber \\
= \rho_i + K \left( \sigma_+ + \sigma_- \right)
\label{POISSONFORM}
\end{eqnarray}

\begin{description}
\item[Planar ($n=0$):]
\begin{eqnarray}
- \frac{1}{x_{i + 1/2} - x_{i - 1/2}}
\left(
\epsilon_{i + 1/2}
\left( \frac{\Phi_{i+1} - \Phi_{i}}{\Delta x_+} \right)
- 
\epsilon_{i - 1/2}
\left( \frac{\Phi_{i} - \Phi_{i-1}}{\Delta x_-} \right)
\right) 
\nonumber \\
= \rho_i + K \left( \sigma_+ + \sigma_- \right)
\end{eqnarray}
Since $x_{i + 1/2} \equiv ( x_{i + 1} + x_i )/ 2$ 
and $x_{i - 1/2} \equiv ( x_{i} + x_{i - 1} )/ 2$,
$x_{i + 1/2} - x_{i - 1/2} = ( x_{i + 1} - x_{i - 1} )/ 2$
\item[Cylindrical ($n=1$):]
\begin{eqnarray}
- \frac{2}{x_{i + 1/2}^{2} - x_{i - 1/2}^{2}}
\left(
x_{i + 1/2} \epsilon_{i + 1/2}
\left( \frac{\Phi_{i+1} - \Phi_{i}}{\Delta x_+} \right)
- 
x_{i - 1/2} \epsilon_{i - 1/2}
\left( \frac{\Phi_{i} - \Phi_{i-1}}{\Delta x_-} \right)
\right) 
\nonumber \\
= \rho_i + K \left( \sigma_+ + \sigma_- \right)
\end{eqnarray}
This equation may be further simplified:
\begin{equation}
x_{i + 1/2}^{2} - x_{i - 1/2}^{2} = 
\left( x_{i + 1/2} + x_{i - 1/2} \right) \cdot
\left( x_{i + 1/2} - x_{i - 1/2} \right)
\end{equation}
\begin{equation}
x_{i + 1/2} + x_{i - 1/2} = 2 x_i
\end{equation}
\begin{equation}
2 x_i \left( x_{i + 1/2} - x_{i - 1/2} \right)
= x_i \left( x_{i + 1} - x_{i - 1} \right)
\end{equation}
The formulation in {\tt pd1.tex} uses:
\begin{equation}
r_i \left( \Delta r_{i + 1/2} + \Delta r_{i - 1/2} \right)
= r_i \left( r_{i + 1} - r_{i - 1} \right)
\end{equation}
The methods are therefore equivalent.

\item[Spherical ($n=2$):]
\begin{eqnarray}
- \frac{3}{x_{i + 1/2}^{3} - x_{i - 1/2}^{3}}
\left(
x_{i + 1/2}^2 \epsilon_{i + 1/2}
\left( \frac{\Phi_{i+1} - \Phi_{i}}{\Delta x_+} \right)
- 
x_{i - 1/2}^2 \epsilon_{i - 1/2}
\left( \frac{\Phi_{i} - \Phi_{i-1}}{\Delta x_-} \right)
\right) 
\nonumber \\
= \rho_i + K \left( \sigma_+ + \sigma_- \right)
\end{eqnarray}
\end{description}

{\bf Methodology for implementation in oopd1: }
\begin{enumerate}
\item Zero matrix
\item Add fluxs to left ($- D_{i - 1/2} A_{i - 1/2}$) to matrix:
\begin{verbatim}
for(i=1; i < ng; i++)
{
  // add left flux for node i of the matrix 
}
\end{verbatim}
\item  Add fluxs to right ($ D_{i + 1/2} A_{i + 1/2}$) to matrix:
\begin{verbatim}
for(i=0; i < ng-1; i++)
{
  // add right flux for node i of the matrix 
}
\end{verbatim}
\item Right hand side contains contributions
from: $\rho$, $\sigma$, and field ($E$/$D$) components out of the
system
\end{enumerate}

Two choices:
\begin{enumerate}
\item The right hand side can contain the 
\underline{total charge enclosed} by the volume.  In this case, $rho$ must
be integrated over the volume and $\sigma$ multiplied by $A_i$.
\item The right hand side can contain the 
\underline{charge density $\rho$}.  In this case, the matrix row 
and $\sigma$ must
be appropriately normalized (see Equation \ref{POISSONFORM}).
\end{enumerate}

\end{document}
