Current MCC Model:

.inp file:

  MCC
  {     
        relativistic    
        gas = Ar
        eSpecies = electrons
        iSpecies = argon
        sec_eSpecies = electrons
        sec_iSpecies = argon
        pressure = 0.015
        temperature = 0.026
  }

sptlrgn.cpp:

[SNIP!]
oopicListIter<Species> thei(specieslist);
  
  for(thei.restart(); !thei.Done(); thei++)
    {

      [...]
      if(mcc != NULL) { mcc -> collide(*thei()); }
      [...]

    }
[/SNIP!]

Disadvantages:
- only allows for collisions with a background gas
- only allows for a single gas

---------------------------------------

Proposed model:

.inp file:

// conceivably, a high-level MCC control section
// of the .inp file could go in the SpatialRegion section

MCC
{
  {
    sec_eSpecies = electrons // only needed for ionization -- 
      sec_iSpecies = argon     // not created if NULL
      }

}

Species
{
 name = electrons
 species_type = e-

 MCC
 {
  sec_eSpecies = electrons // only needed for ionization -- 
  sec_iSpecies = argon     // not created if NULL
 }
}


Species
{
 name = argon
 species_type = Ar+
 MCC
 {
  sec_eSpecies = electrons // only needed for ionization -- 
  sec_iSpecies = argon     // not created if NULL
 }

}

Species
{
 name = Ar_gas

 species_type = Ar;
 // a uniform fluid background -- conceivably, more complex
 UniformFluid
 {
    pressure = 0.015    // Torr
    temperature = 0.026 // eV
      
    // could also use n=1E... instead
 }
}

sptlrgn.cpp:

oopicListIter<Species> thei(specieslist);
oopicListIter<Species> thes(specieslist);
oopicListIter<Species> thet(specieslist);

  
  for(thei.restart(); !thei.Done(); thei++)
    {

      [...]
      for(thes=thei; !thes.Done(); thes++)
        { 
	mcc->collide(*thei(), *thes()); 
	
	for(thet=thes; !thet.Done(); thet++)
        { 
		mcc->collide(*thei(), *thes(), *thet()); 
        }
	}
      [...]

    }

Advantages:
- can (conceivably) handle arbitrary binary collisions
- can handle an arbitrary number of fluids

Disadvantages:
- more typing
- how to handle particle-particle collisions?

-------------------

Types of Collisions:
particle + fluid    |
fluid + fluid       |-> same cross-sections
particle + particle |

cross sections defined in terms of two Species
