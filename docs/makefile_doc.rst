======================
Makefile documentation
======================

This documentation explains how the OOPD1 makefile works.

How to run
----------

To compile and link OOPD1 you have to run

.. code:: bash

	make all

With this command a file called 'pd1.exe' will be created in the root directory of OOPD1. All the objects created during the compilation will be located in the folder /build.

An example of use of this program is:

.. code:: bash
	
	./pd1.exe -i input/test.inp

User parameters
---------------

Here we declare parameters that might be changed by the user before compiling. Some of them are important to obtain a sucessful compilation of the program.

1. Verbosity of the makefile: displays information when running the makefile, if it's value is TRUE, information will be displayed.

.. code:: bash

	VERBOSE = TRUE

2. MPI flag: If it is set to yes, the program will be compiled with mpicc.

.. code:: bash

	MPI=no

3. Libraries setup: Declare which libraries will be used and where are they located. This is an important feature that must be correctly set. The libraries needed for this program to compile and run are: tcl, tk and xgrafix. Also, the developer can provide any extra libraries to be included.

.. code:: bash

	TCLTK=8.6
	TKDIR=-L/usr/lib/tk8.6
	XGPATH = /usr/local/xgrafix
	EXTRA_LIBDIR=

4. Compilation flags: Instructions for the compiler to compile the code. We define the flags of paths that need to be explicitly included or extra flags in the compilation. We have defined some CUDA flags for the nvcc compilator. Also, we have defined flags to compile in debug or optimization mode.

.. code:: bash

	CFLAGS=-I$(XGPATH)/include
	CUDAFLAGS = -std=c++11 -I$(XGPATH)/include
	COPT= -O3
	CDEBUG= -Wall -g

Code structure
--------------

This makefile detects the source files from folders located in /src. Also, the headers are located into folders in /include. This allows us to have everything organized so the project is easier to read. If we have to create a new module that has to be placed in a new folder in /src and (or) /include, we have to define the new folder in the makefile in order to compile it. So let's proceed in how to do this.

Developers information
----------------------

After we have everything setup for a correct compilation, we might be interested in developing the code. In this section we are going to explain how the makefile works and some parameters to be modified.

Including new directories or libraries
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First of all, we have a definition of the directories that will be taken into account by the makefile to compile and link. 

.. code:: bash

	DIRS = main spatial_region diagnostics reactions distributions fields \
			 utils emmiter MCC atomic_properties xsections

In this parameter we have to include any folder in /src or /include in order to have it detected by the compilator. Of course, if you have a folder with the same name inside /src and /include, you only have to define it once in this variable. The makefile will search in /src and /include for this folders, and if the folders exist. For example, if /folder is found in /src and /include, it will search for every .c, .cpp, .cu object in /src/folder in order to compile them, and will search for every .h, .hpp and .hh header files in /include/folder to include them as header files.

After so, we have the definition of the libraries needed for compilation, some of them we have already defined. But we might be interested in using new libraries.

.. code:: bash

	LIBS  = -L/usr/local/lib $(EXTRA_LIBDIR) -L$(XGPATH)/lib -lXGC250 \
		-ltk$(TCLTK) -ltcl$(TCLTK) -lXpm -lX11 -lm -ldl

Makefile workflow
~~~~~~~~~~~~~~~~~

In this section we will be including a brief description on how the makefile works. This probably won't be edited at any time.

We have conditionals for:

1. Compilation with or without MPI:

.. code:: bash

	ifeq ($(MPI),no)
	   CC = g++
	   EXEC = pd1
	   ifeq ($(UNAME_S),Darwin)
	      TCLTK=
	      EXTRA_LIBDIR=-L/opt/local/lib -I/opt/local/include -L/opt/X11/lib
	   endif
	else
	   EXEC = mpipd1
	   ifeq ($(UNAME_S),Darwin)
	      CC = mpic++-mpich-devel-mp
	   else
	      CC = mpiCC
	      ifeq ($(UNAME_N),dev-intel16)
		 # HPCC @ MSU
		 XGPATH = $(HOME)/xgrafix
	      endif
	   endif
	endif

2. OS specific options for creating dirs, etc.

.. code:: bash

	ifeq ($(OS),Windows_NT)
	    RM = del /F /Q
	    RMDIR = -RMDIR /S /Q
	    MKDIR = -mkdir
	    ERRIGNORE = 2>NUL || true
	    SEP=\\
	else
	    RM = rm -rf
	    RMDIR = rm -rf
	    MKDIR = mkdir -p
	    ERRIGNORE = 2>/dev/null
	    SEP=/
	endif

3. Verbosity.

.. code:: bash

	ifeq ($(VERBOSE),TRUE)
	    HIDE =
	else
	    HIDE = @
	endif

4. Compilation mode.

.. code:: bash

	ifeq ($(MODE),optimized)
		override CFLAGS += $(COPT)
	else
		override CFLAGS += $(CDEBUG)
	endif

After so, we can see how the target, header and source files are located and included.

1. Definition of main folders and target file.

.. code:: bash

	PROJDIR := $(realpath $(CURDIR))
	SOURCEDIR := $(PROJDIR)/src
	BUILDDIR := $(PROJDIR)/build
	INCLUDEDIR := $(PROJDIR)/include
	TARGET = pd1.exe

2. Definition of the directories that the makefile with search for files.

.. code:: bash

	SOURCEDIRS = $(foreach dir, $(DIRS), $(addprefix $(SOURCEDIR)/, $(dir)))
	TARGETDIRS = $(foreach dir, $(DIRS), $(addprefix $(BUILDDIR)/, $(dir)))

3. Generation of the include parameters and adding the lists to VPATH.

.. code:: bash

	INCLUDES = $(foreach dir, $(DIRS), $(addprefix -I$(INCLUDEDIR)/, $(dir)))
	VPATH = $(SOURCEDIRS)

4. Create the list of .c, .cpp and .cu files in the source directories.

.. code:: bash

	SOURCESCPP = $(foreach dir, $(DIRS), $(shell find $(SOURCEDIR)/$(dir) -type f -name *.cpp))
	SOURCESC = $(foreach dir, $(DIRS), $(shell find $(SOURCEDIR)/$(dir) -type f -name *.c))
	SOURCESCUDA = $(foreach dir, $(DIRS), $(shell find $(SOURCEDIR)/$(dir) -type f -name *.cu))

5. Define the objetcts for all sources.

.. code:: bash 

	OBJS := $(subst $(SOURCEDIR),$(BUILDDIR),$(SOURCESCPP:.cpp=.o))
	OBJS += $(subst $(SOURCEDIR),$(BUILDDIR),$(SOURCESC:.c=.o))
	OBJS += $(subst $(SOURCEDIR),$(BUILDDIR),$(SOURCESCUDA:.cu=.o))

6. Define the dependencies from headers.

.. code:: bash

	DEPS := $(foreach dir, $(DIRS), $($(addprefix $(INCLUDEDIR)/, $(dir)):.hpp=.d))
	DEPS += $(foreach dir, $(DIRS), $($(addprefix $(INCLUDEDIR)/, $(dir)):.hh=.d))
	DEPS += $(foreach dir, $(DIRS), $($(addprefix $(INCLUDEDIR)/, $(dir)):.h=.d))

And, finally, the compilation part of the makefile.

1. Definition of the compilation rules for each file.

.. code:: bash

	define generateRules
	$(1)/%.o: %.cpp
		@echo Building $$@
		$(HIDE)$(CC) $$(INCLUDES) -c $(CFLAGS) -o $$(subst /,$$(PSEP),$$@) $$(subst /,$$(PSEP),$$<) $$(LIBS)
	$(1)/%.o: %.c
		@echo Building $$@
		$(HIDE)$(CC) $$(INCLUDES) -c $(CFLAGS) -o $$(subst /,$$(PSEP),$$@) $$(subst /,$$(PSEP),$$<) $$(LIBS)
	$(1)/%.o: %.cu
		@echo Building $$@
		$(HIDE)nvcc $$(INCLUDES) -c $(CUDALAGS) -o $$(subst /,$$(PSEP),$$@) $$(subst /,$$(PSEP),$$<) $$(LIBS)
	endef

2. The make all loop. This will call a function directories which will create all build directories. Will compile every source with the already defined rules and will link all of the object files to the target file.

.. code:: bash

	all:	directories $(TARGET)

	$(TARGET): $(OBJS)
		$(HIDE)echo Linking $@
		$(HIDE)$(CC) $(OBJS) -o $(TARGET) $(LIBS)

	# Include dependencies
	-include $(DEPS)

	# Generate rules
	$(foreach targetdir, $(TARGETDIRS), $(eval $(call generateRules, $(targetdir))))

	directories:
		$(HIDE)$(MKDIR) $(subst /,$(PSEP),$(TARGETDIRS)) $(ERRIGNORE)

The rest of the makefile contains some general tools that can be easily understood with this documentation.

