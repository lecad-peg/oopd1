Diagnostics Implementation in oopd1

Goals of the Diagnostic class:

The Diagnostic class must be flexible enough to serve all
design goals in displaying information useful for PIC-MCC in
an efficient way.

* display of 2-d diagnostics of Scalar * vs Scalar * arrays of
known length.  The independent variable will be "x" or "t" 
in general

* display of phase-space diagnostics.  Since particles are
stored in different groups (not contiguous in memory),
this is a challenge to work with Xgrafix

* display of 3-d diagnostics -- an array versus "x" and "t"

* display of diagnostics of derived quantities.  This should
be able to be controlled from the input file.  This allows
the construction of new diagnostics from existing information
-- plots of a quantity at some position, either on a node or
otherwise
-- plots of an system-wide quantity integrated over a volume versus
time
-- plots of maximum, minimum, or similar values
-- plots of one derived quantity versus another, such as 
electrode voltage versus current

* ability to Fourier transform quantities 
(giving "k" or "f", for spatial or temporal transforms, respectively).
3-d plots (a quantity versus "k" and "f").  Flexibility should
be considered in terms of how the transform is done.

* Time-averaged quantities.  Different types of averaging 
should be considered

* ability to turn off diagnostic calculations if not desired.
In some simulations, it has been noted that calculating
data associated with diagnostics in xpdp1 exceeds fifty percent
of wall clock time

* ability to save data to files for non-interactive run
according to input file.  This allows parallelization and
non-interactive runs to be practical for gathering information.

