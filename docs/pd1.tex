\documentclass{article}
\usepackage{graphicx}
\usepackage{times}
\usepackage{url}

\begin{document}
\title{oopd1 documentation}
\maketitle

\section{{\tt oopd1} overview}

{\tt oopd1} ({\bf o}bject {\bf o}riented
{\bf p}lasma {\bf d}evice {\bf 1}d) is a replacement to
the Plasma Theory and Simulation Group's (PTSG) 1d-3v suite
of PIC codes ({\bf xpdp1}, {\bf xpdc1}, {\bf xpds1}, {\bf xes1});

\section{Input File Format}

The input file follows a specific format.

\renewcommand{\labelitemi}{$ $}
\renewcommand{\labelitemii}{$ $}

\begin{itemize}
\item (Comments)
\item \{
\item Species
\item $\vdots$
\item SpatialRegion
\begin{itemize}
  \item \{
  \item Grid
  \item FieldSolver (PoissonSolver, etc.)
  \item Boundary (Conductor, etc.)
  \item $\vdots$
  \item Load
  \item $\vdots$
  \item \}
\end{itemize}
\item $\vdots$
\item \}
\item More comments can go here
\label{INPUTFILEFORMAT}

\end{itemize}

\section{Poisson Solver}

For meshes with a uniform value of $\epsilon$,
the Laplacian operator may be used:

\begin{description}
\item{Planar:}
\begin{equation}
\nabla^2 \Phi = \frac{d^2 \Phi}{dx^2}
\end{equation}
\item{Cylindrical:}
\begin{equation}
\nabla^2 \Phi = \frac{1}{r} \frac{d}{dr} \left( r \frac{d \Phi}{dr} \right)
=  \frac{d^2 \Phi}{dr^2} + \frac{1}{r} \frac{d \Phi}{dr}
\end{equation}
\item{Spherical:}
\begin{equation}
\nabla^2 \Phi = \frac{1}{r^2} \frac{d}{dr} \left( r^2 \frac{d \Phi}{dr}
\right) = \frac{1}{r} \frac{d^2}{dr^2} \left( r \Phi \right)
= \frac{d^2 \Phi}{dr^2} + \frac{2}{r} \frac{d \Phi}{dr}
\end{equation}
\end{description}

In finite difference form for a uniform mesh:
\begin{description}
\item{Planar:}
\begin{equation}
\nabla^2 \Phi_i = \frac{\Phi_{i-1} - 2 \Phi_i + \Phi_{i+1}}{\Delta x^2}
+ O \left( \Delta x^2 \right)
\end{equation}
\item{Cylindrical:}
\begin{equation}
\nabla^2 \Phi_i = \frac{\Phi_{i-1} - 2 \Phi_i + \Phi_{i+1}}{\Delta r^2}
+ \frac{1}{r_i} \frac{\Phi_{i+1} - \Phi_{i-1}}{2 \Delta r}
+ O \left( \Delta r^2 \right)
\end{equation}
\item{Spherical:}
\begin{equation}
\nabla^2 \Phi_i = \frac{\Phi_{i-1} - 2 \Phi_i + \Phi_{i+1}}{\Delta r^2}
+ \frac{2}{r_i} \frac{\Phi_{i+1} - \Phi_{i-1}}{2 \Delta r}
+ O \left( \Delta r^2 \right)
\end{equation}
\end{description}

For a mesh with variable $\epsilon$, the left hand side of 
Poisson's equation is more complex:
\begin{equation}
\nabla \cdot \left( \epsilon \nabla \Phi \right) = - \rho
\end{equation}

In one (radial) dimension, the gradient operator is of the
same form regardless of coordinate system (Cartesian, cylindrical,
spherical):
\begin{equation}
\nabla \Phi \Rightarrow \frac{d \Phi}{dx}
= \frac{\Phi_{i+1} - \Phi_{i-1}}{2 \Delta x} 
+ O \left( \Delta x^2 \right)
\end{equation}

In one dimension, the divergence operator is dependent on the
type of coordinate system:
\begin{description}
\item[Planar:] 
\begin{equation}
\nabla \cdot {\bf A} = \frac{d A}{dx}
= \frac{A_{i+1} - A_{i-1}}{2 \Delta x} 
+ O \left( \Delta x^2 \right)
\label{DIV_PLANAR}
\end{equation}

\begin{equation}
\nabla \cdot \left( \epsilon \nabla \Phi \right) \approx
\frac{1}{0.5 \left(\Delta x_{i} + \Delta x_{i+1} \right)} \left(
\frac{\epsilon_{i+1} \left( \Phi_{i+1} - \Phi_{i} \right)}{\Delta x_{i+1}}
 - \frac{\epsilon_i \left( \Phi_i - \Phi_{i-1} \right)}{\Delta x_i}
\right)
\end{equation}

The subscript on $\Phi$ is a nodal subscript and on 
$\Delta x$ and $\epsilon$ is a 
cellular subscript 
($i \Rightarrow$ left hand cell of node $i$, 
$i+1 \Rightarrow$ right hand cell of 
node $i$).

\item[Cylindrical:]
\begin{equation}
\nabla \cdot {\bf A} = \frac{1}{r} \frac{d}{dr} \left(
r A \right)
\label{DIV_CYLINDER}
\end{equation}

\begin{equation}
\nabla \cdot \left( \epsilon \nabla \Phi \right)_i \approx
\frac{1}{r_i} \frac{r_{i + 1/2} \epsilon_{i + 1/2} \left(
\frac{\Phi_{i+1} - \Phi_{i}}{\Delta r_{i + 1/2}} \right)
- r_{i - 1/2} \epsilon_{i - 1/2} \left(
\frac{\Phi_{i} - \Phi_{i-1}}{\Delta r_{i - 1/2}} \right)}
{0.5 \left( \Delta r_{i + 1/2} + \Delta r_{i - 1/2} \right)}
\end{equation}

\item[Spherical:]
\begin{equation}
\nabla \cdot {\bf A} = \frac{1}{r^2} \frac{d}{dr} \left(
r^2 A \right)
\label{DIV_SPHERE}
\end{equation}

\begin{equation}
\nabla \cdot \left( \epsilon \nabla \Phi \right)_i \approx
\frac{1}{r_{i}^2} \frac{r_{i + 1/2}^2 \epsilon_{i + 1/2} \left(
\frac{\Phi_{i+1} - \Phi_{i}}{\Delta r_{i + 1/2}} \right)
- r_{i - 1/2}^2 \epsilon_{i - 1/2} \left(
\frac{\Phi_{i} - \Phi_{i-1}}{\Delta r_{i - 1/2}} \right)}
{0.5 \left( \Delta r_{i + 1/2} + \Delta r_{i - 1/2} \right)}
\end{equation}

\end{description}
In Equations \ref{DIV_PLANAR}-\ref{DIV_SPHERE}, $A$ is the
one-dimensional component of ${\bf A}$.

\subsection{Ideal Voltage Source}

For a conductor at node $i$
biased to a specific voltage, row $i$ of the matrix is zeroed and
identity is placed on the diagonal.  The value of the voltage
is placed on the right-hand side.

\subsection{Circuit Boundary Conditions}

For more general circuit conditions, a conductor at node $i$
is coupled to the circuit
and plasma device via its surface charge.  

Gauss's law tells us:
\begin{equation}
\oint {\bf E \cdot} d{\bf S} = Q + V \rho_0
\end{equation}
$Q$ is the charge on the conductor (surface charge).  
$V$ is the volume of the space and $\rho_0$ is the charge density
at the conductor node.

For a positive/negative flux:
\begin{equation}
A_{\pm} E_{\pm} = 
A_{\pm} \frac{\Phi_{i} - \Phi_{i \pm 1}}{\Delta x_{\pm}} = 
Q_{\pm} + V_{\pm} \rho_0
\end{equation}
$A_{\pm}$ are the areas associated with $x_i \pm 0.5 \Delta x_{\pm}$.
$\Delta x_{\pm}$ is the mesh spacing to the right and left of node
$i$, respectively.  $Q_{\pm}$ is the charge on the right and left sides
of the conductor, respectively.  $V_{\pm}$ are the (positive)
volumes between
$x_i$ and $x_i \pm 0.5 \Delta x_{\pm}$, respectively.  Note that for
system-bounding conductors, only the $+$ equation is needed for a 
conductor at the left-hand (inner) side of the system
and only the $-$ equation is needed for a conductor at the right-hand
(outer) side of the system.  For interior conductors, the equations
must be summed using $Q_+ + Q_- = Q$:
\begin{equation}
A_+ \left( \frac{\Phi_i - \Phi_{i+1}}{\Delta x_+} \right)
+ A_- \left( \frac{\Phi_i - \Phi_{i-1}}{\Delta x_-} \right)
= Q + \left( V_+ + V_- \right) \rho_0
\end{equation}

\include{parallel}

\end{document}
