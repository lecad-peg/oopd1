#!/bin/bash

# convert .eps figures to .pdf figures
# and convert .pdf figures to .eps figures

EPSFILES=`ls *.eps`
PDFFILES=""

for i in ${EPSFILES}
do
  epstopdf ${i}
done

for i in ${PDFFILES}
do
  pdftops -eps ${i}
done
