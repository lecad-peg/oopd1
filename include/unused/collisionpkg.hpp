// mindgame: this file is depreciated.
/*
  Last update (09/06)

  #General Ideas:
  A collision Package is a storage place for cross - sections.
  It is initialized when creating the derived peculiar collision packages: argon, hydrogen...
  It is characterized by one "basis species", referred to as gasspecies or gas in MCC,
  and present in any reaction of the set.
  This basis species might collide with different species, called "colliders",
  to whom we associate a cross-sections set (XsectionSet).

  It is absolutely possible to create two collision packages with the same basis species.
  However take care of referring to them differently in the input file
  (and in the recognition process in mccParser.cpp).
  For the moment, we only need one collision Package per species,
  we called our collision packages with names refering to their basis species.

  #Structure:
  One collision package can have as many colliders as desired
  (we simply need to add colliders in collider-types, and their related cross section sets).
  Two colliders can point to the same cross sections set (it is typically the
  case of electrons and secondary electrons)

  #For adding a collision package:
  1) create the collision package: for example "argon.hpp" (more indications in argon.hpp).
  2) in mccParser.hpp, add #include "argon.hpp"
  3) in mccParser::check(), complete the recognition of the collision package
  Warning 1: It is not possible to give a collision package a name already used by
  species types (or the program does not understand who is who).
  Typically, we used: Air_gas (for the gas) / Air (for the species type),
  Hydrogen (for the gas)/ H (for the species type)... So check the species type names
  in speciestypes.hpp before creating a gas.
  Warning 2: It is assumed in MCC, that the cross sections in a gas are defined as
  function of the energy of the COLLIDER not the gas. This might be change
  (for example: create cross sections which are functions of relative velocity)
  ...but MCC would have to be modified. It is our convention for the moment.
*/

#ifndef COLLISION_PACKAGE
#define COLLISION_PACKAGE

#include "utils/ovector.hpp"
#include "reactions/reactiontype.hpp"

class XsectionSet;

// mindgame: this class is going to be depreciated.
class CollisionPkg
{
private:
  // mindgame: use STL to simplify the implementation
  vector<ReactionSpeciesType> colliders_types; // array of collider types.
  vector<XsectionSet*> xsectionarray; // Array of cross sections sets corresponding to the colliders
  XsectionSet* currentxsectionset;
  // mindgame: no idea what this is for.
  // bool currentxsectionset_unaffected;
  // C.N: was only relevant in the last implementation, where arrays where allocated separately

public:
  CollisionPkg();
  virtual ~CollisionPkg();

protected:
  // Recognition
  ostring pkgname; // for printing purposes only, until now

  // For collision package data creation:
  void add_collider (ReactionSpeciesType s);
  void add_xsection (Scalar (*f)(Scalar), Scalar _threshold, ReactionType _type);
  void add_Xsection_type1 (Scalar e0, Scalar e1, Scalar e2, Scalar sMax, ReactionType type);
  void add_Xsection_type2 (Scalar _a, Scalar _b, ReactionType _type);
  void add_fakeXsection (Scalar e0, Scalar e1, ReactionType type);
  void add_fakiXsection (Scalar _a, ReactionType _type);

  // Accessor functions
  inline XsectionSet* get_xsectionset (int i) {return xsectionarray[i];}
  inline ReactionSpeciesType get_collider(int i) { return colliders_types[i]; }
  inline int get_numberofcolliders(void) { return colliders_types.size();}

  // functions for implementing the kinetics of particles:
  // mindgame: why hydrocarbons? this is generic class.
  virtual int get_gasatomicnumber() {fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return -1; }

  // Printing
  void print_xsections(Scalar *a, int n, FILE *fp);
  void print_xsections(Scalar Emin, Scalar Emax, int n, bool logscale=false);
  void print_xsections(int idx, Scalar *a, int n, FILE *fp);
  void print_xsections(int idx, Scalar Emin, Scalar Emax, int n, bool logscale=false);
};

#endif
