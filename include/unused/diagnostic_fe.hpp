void  add_phasediagnostic(Species *s, BoundaryList * theb);
void  add_griddiagnostic(DataArray *a);
void  add_griddiagnostic(VectorArray *a);
void  add_timediagnostic(Scalar *val, ostring nnn);
void  add_timediagnostic(int (Species::*spif)(), ostring nnn, 
			 bool tosum=true);
void  add_timediagnostic(Scalar (Species::*spif)(), ostring nnn, 
			 bool tosum=true); // For integral of time-average J1dotE1 diagnostic, MAL 12/11/09
void  add_timediagnostic(Vector3 (Species::*spvf)(), ostring nnn);
void  add_timediagnostic(Vector3 (Species::*spvf)(), ostring nnn, XGoption xg_option);
void  add_timediagnostic(Scalar *arr, int nitms, ostring nnn, bool tosum=false);
void  add_timediagnostic(SpeciesList *spl, Scalar *arr, int nitms, ostring nnn, bool tosum);		// JK 2019-01-19

void  add_gridarray(const char *name, Scalar *array, int n=0);
void  add_gridvectors(const char *name, Vector3 *array, int vdim, int n=0);
void  add_sp_grid_array(const char *name, Scalar * (Species::*spga)() const);
void  add_eedfdiagnostic(Species *s, int, int, bool);			// added JK, 2017-11-01
