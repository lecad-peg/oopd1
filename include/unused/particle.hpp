#include "utils/ovector.hpp"
#include "main/parse.hpp"

class PtclGrp
{
  int np;  // max # of particles in the group
  Scalar nc2p;
  Scalar *x;
  Scalar *v[3];

  PtclGrp(int n)
    {
    }

  void allocate_particles(int n)
    {
      x = new Scalar[n];
      for(int i=0; i < 3; i++)
	{
	  v[i] = new Scalar [n];
	}
      np = n;
    }
};
