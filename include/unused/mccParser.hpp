#ifndef MCCPARSER_H
#define MCCPARSER_H

// MCCParser checks the input data, and create the MCC class

class MCCParser : public Parse
{
private:
  bool init_coltypes;
 
  oopicList<Species> *specieslist;
  oopicList<MCC> mcclist;  
  ostring mccPackage;
  
  bool createInInitialDefaultNames;
  bool createInSecondaryDefaultNames;
  bool useSecDefaultWeight;
 
  ostring sec_eSpecies; 
  ostring sec_iSpecies;
  ostring sec_iminusHSpecies;
  ostring sec_iminusH2Species;
  ostring sec_nminusHSpecies;
  ostring sec_nminusH2Species;
  ostring sec_nminusH3Species;   
  ostring sec_HSpecies;
  ostring sec_H2Species;
  ostring sec_pSpecies;

public:
  MCCParser (FILE *fp, oopicList<Parameter<Scalar> > float_variables, 
	     oopicList<Parameter<int> > int_variables, 
	     oopicList<Species> *slist, bool _init_coltypes);
  ~MCCParser()
  {
    mcclist.deleteAll();
  } 
  
  //functions inherited from parse:
  virtual void setdefaults(void);
  virtual void setparamgroup(void);
  virtual void check(void);
 
  // Accessor function:
  inline oopicList<MCC> * get_mcclist(void) { return &mcclist;}

  // initialize mcc objects.
  void init_mcc(Scalar _dt, Fields *tf);

  // initiatialize collisiontypes of species: done only if one object MCCParser is built.
  void init_collisiontypes(void);
  void init_collisiontype(Species *sp);

  // Collision Packages creation:
//  void create_Argon(void);
//  void create_Helium(void);
//  void create_Hydrogen(void);
  void create_Air_gas(void);
  void create_CH4_gas(void);
  void create_CH3_gas(void);
  void create_CH2_gas(void);
  void create_CH_gas(void);
  void create_Carbon(void);
  void create_CH4_ions_gas(void);
  void create_CH3_ions_gas(void);
  void create_CH2_ions_gas(void);
  void create_CH_ions_gas(void);
  void create_Fake(void);
};

#endif
