// Advance with partial timestep
//
// Original by HyunChul Kim, 2006
//
// in 1D simulations, x update is not necessary.
#include "utils/ovector.hpp"
#include "main/particlegroup.hpp"
#include "main/advance_v.hpp"
#include "main/particlegrouparray.hpp"

Scalar LOCALCLASS::advance_v_for_synchro_0th(ParticleGroup *p, int i,
					   Scalar frac, Direction dir)
{
  return 0;
}

Scalar LOCALCLASS::advance_v_for_synchro_1st(ParticleGroup *p, int i,
					   Scalar frac, Direction dir)
{
  p->advance_v(i,(0.5-frac)*dt,E[dir],B[dir]);
  return frac;
}

Scalar LOCALCLASS::advance_v_for_synchro_2nd(ParticleGroup *p, int i,
					     Scalar frac, Direction dir)
{
  Scalar frac_2nd, d;
  Vector3 E_v;

  if (thefield->is_magnetized())
  {
    Vector3 v_plus_half = p->get_vMKS(i);
    Vector3 v_plus_tmp, B_v;
    v_plus_tmp = v_plus_half
               + 0.5*(1-frac)*dt*p->get_qm0()*(E[dir]+v_plus_half%B[dir]);

    frac_2nd = frac*v_plus_half.e1()/v_plus_tmp.e1();

    d = 0.25*(2*frac_2nd-3.0);
    E_v = E[dir]-d*E_dt[dir]+(d+0.5)*v_plus_half.e1()*E_grad[dir]*dt;
    B_v = B[dir]-d*B_dt[dir]+(d+0.5)*v_plus_half.e1()*B_grad[dir]*dt;
    p->advance_v(i,(0.5-frac_2nd)*dt,E_v,B_v);
  }
  else
  {
    Scalar v_plus_tmp;
    Scalar v_plus_half = p->get_vMKS(i).e1();
    v_plus_tmp = v_plus_half
               + 0.5*(1-frac)*dt*p->get_qm0()*E[dir].e1();

    frac_2nd = frac*v_plus_half/v_plus_tmp;

    d = 0.25*(2*frac_2nd-3.0);
    E_v = E[dir]-d*E_dt[dir]+(d+0.5)*v_plus_half*E_grad[dir]*dt;
    p->advance_v(i,(0.5-frac_2nd)*dt,E_v);
  }

  return frac_2nd;
}
