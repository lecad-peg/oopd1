// diagnostic_fe.cpp -- functions for multiple classes
// the real contents are invoked when LOCALCLASS == DiagnosticControl
// Otherwise, a DiagnosticControl is instantiated and the Diagnostic
// is added there

void LOCALCLASS::add_gridarray(const char *name, Scalar *array, int n)
{
  #if LOCALCLASS == DiagnosticControl
  grid_arrays.add(new DataArray(name, n, array));
  #else
  DiagnosticControl dc;
  dc.add_gridarray(name, array, n);
  #endif
}

void LOCALCLASS::add_gridvectors(const char *name, Vector3 *array, int vdim, int n)
{
  #if LOCALCLASS == DiagnosticControl
  grid_vectors.add(new VectorArray(name, n, array, vdim));
  #else
  DiagnosticControl dc;
  dc.add_gridvectors(name, array, n);
  #endif
}

void LOCALCLASS::add_phasediagnostic(Species *s, BoundaryList * theb)
{
  #if LOCALCLASS == DiagnosticControl
  DiagnosticPhase *tpd;
  tpd = new DiagnosticPhase(s, theb);
  phase_diagnostics.add(tpd);
  add_diagnostic(tpd);

  #else
  DiagnosticControl dc;
  dc.add_phasediagnostic(s, theb);
  #endif
}

void LOCALCLASS::add_griddiagnostic(DataArray *a)
{
  #if LOCALCLASS == DiagnosticControl
  Diagnostic2d *td;

  td = new Diagnostic2d(gridarray, a);

  grid_diagnostics.add(td);
  add_diagnostic(td);

  #else
  DiagnosticControl dc;
  dc.add_griddiagnostic(a);
  #endif
}

void LOCALCLASS::add_griddiagnostic(VectorArray *a)
{
  #if LOCALCLASS == DiagnosticControl
  Diagnostic2d *td;

  td = new Diagnostic2d(gridarray, a);

  grid_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc;
  dc.add_griddiagnostic(a);
  #endif
}

void  LOCALCLASS::add_timediagnostic(Scalar *val, ostring nnn)
{
  #if LOCALCLASS == DiagnosticControl
  TimeDiagnostic *td;

  td = new TimeDiagnostic(timepointer, val, 1, nnn, false);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc;
  dc.add_time_diagnostic(val, nnn);
  #endif
}

void  LOCALCLASS::add_timediagnostic(int (Species::*spif)(), ostring nnn, bool tosum)
{
  #if LOCALCLASS == DiagnosticControl
  TimeDiagnostic *td;

  td = new TimeDiagnostic(timepointer, thesplist, spif, nnn, tosum);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc;
  dc.add_timediagnostic(spif, nnn, tosum);
  #endif
}

 // For integral of time-average J1dotE1 diagnostic, MAL 12/11/09
void  LOCALCLASS::add_timediagnostic(Scalar (Species::*spif)(), ostring nnn, bool tosum)
{
  #if LOCALCLASS == DiagnosticControl
  TimeDiagnostic *td;

  td = new TimeDiagnostic(timepointer, thesplist, spif, nnn, tosum);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc;
  dc.add_timediagnostic(spif, nnn, tosum);
  #endif
}

void  LOCALCLASS::add_timediagnostic(Vector3 (Species::*spvf)(), ostring nnn)
{
  #if LOCALCLASS == DiagnosticControl
  TimeDiagnostic *td;

  td = new TimeDiagnostic(timepointer, thesplist, spvf, nnn);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc;
  dc.add_timediagnostic(spvf);
  #endif
}

void  LOCALCLASS::add_timediagnostic(Vector3 (Species::*spvf)(), ostring nnn, XGoption xg_option)
{
  #if LOCALCLASS == DiagnosticControl
  TimeDiagnostic *td;

  td = new TimeDiagnostic(timepointer, thesplist, spvf, nnn, xg_option);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc;
  dc.add_timediagnostic(spvf);
  #endif
}

void LOCALCLASS::add_timediagnostic(Scalar *arr, int nitms, ostring nnn, bool tosum)
{
  #if LOCALCLASS == DiagnosticControl
  TimeDiagnostic *td;
  td = new TimeDiagnostic(timepointer, arr, nitms, nnn, tosum);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc; // changed "Dianostic" to "Diagnostic", MAL 6/12/09
  dc.add_timediagnostic(arr, nitms, nnn, tosum);
  #endif
}

// add species, JK 2019-01-19
void LOCALCLASS::add_timediagnostic(SpeciesList *spl, Scalar *arr, int nitms, ostring nnn, bool tosum)
{
  #if LOCALCLASS == DiagnosticControl
  TimeDiagnostic *td;
  td = new TimeDiagnostic(timepointer, spl, arr, nitms, nnn, tosum);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc; // changed "Dianostic" to "Diagnostic", MAL 6/12/09
  dc.add_timediagnostic(arr, nitms, nnn, tosum);
  #endif
}

void LOCALCLASS::add_sp_grid_array(const char *name, 
				   Scalar * (Species::*spga)() const)
{
  #if LOCALCLASS == DiagnosticControl
  sp_grid_arrays.add(new std::pair<ostring, Scalar * (Species::*)()const>(ostring(name), spga));
  #else
  DiagnosticControl dc; // changed "Dianostic" to "Diagnostic", MAL 6/12/09
  dc.add_sp_grid_array(name, spga);
  #endif
}

