#ifndef HYDROCARBONS_DECLARED
#define HYDROCARBONS_DECLARED
#include "hydrocarbons_model.hpp"

/*
  Collision processes of CHy and CHy+ hydrocarbons with plasma electrons and protons
  R.K. Janev and D. Reiter (2002)
- Cross-sections are integrated over excited states by Janev
- Masses are indicated, in case we don't follow all the secondary species: masses correpond to deuterium not hydrogen.
- Reactions for Carbon have been taken from  www.eirene.de/eigen
*/

class CH4_gas : public Hydrocarbons_model
{
public:
  CH4_gas(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, 
	  ostring sec_e, ostring sec_i, ostring sec_iminusH, ostring sec_iminusH2,
	  ostring sec_nminusH, ostring sec_nminusH2, ostring sec_nminusH3, 
	  ostring sec_H, ostring sec_H2, ostring sec_p)
    :Hydrocarbons_model(slist, sp,  _useSecDefaultWeight, sec_e, sec_i, sec_iminusH, sec_iminusH2, 
			sec_nminusH, sec_nminusH2, sec_nminusH3,  sec_H, sec_H2, sec_p)  
  {
    // Set properties for basic functions of the mcc class.
    pkgname = "CH4_gas";
    u_gas = &un;
    eFlag_gas = false;

    // Create the data for the total xsections.
    
    add_collider(E);
    add_xsection(sigmaI, 12.63, E_IONIZATION); 
    add_xsection(sigmaDI, 14.25, E_DISSIONIZATION);
    add_xsection(sigmaDE1, 8.8 , E_DISSEXCITATION1);
    add_xsection(sigmaDE2, 9.4, E_DISSEXCITATION2);
    
    add_collider(H_ions);
    add_xsection(sigmaeE, 0.0, P_eEXCHANGE);
    add_xsection(sigmahE, 0.0, P_hEXCHANGE); 
  }
  virtual ~CH4_gas(){}
  
  // Functions needed for reaction kinetics:
  //------------------
  virtual Scalar get_DIeEnergyloss(void) const {return 17.49;} // If energy loss does not correspond to the threshold
  
  virtual Scalar get_DE1energy(void) const { return 4.4; }
  virtual Scalar get_DE2energy (void) const { return 4.7;}
  virtual Scalar get_DIenergy(void) const { return 3.2;}
  virtual Scalar get_CXhenergy(void)  const {return 2.96;}

  virtual Scalar getm_nminusH(void) const {return 29.95e-27;}
  virtual Scalar getm_nminusH2(void) const {return 26.61e-27;}
  virtual Scalar getm_iminusH(void) const {return 29.95e-27;}

  // Cross sections:
  //-------------------
  static Scalar sigmaI(Scalar energy)
  {
    if (energy <= 12.63) return 0.0 ;
    return (7.9176e-19/energy)*(1.3541*log(energy/12.63) - 1.4665*(1-(12.63/energy)) 
				+ 1.6787e-1*pow( (1-(12.63/energy)), 2) + 6.1801*pow( (1-(12.63/energy)), 3) 
				- 1.5638e1*pow( (1-(12.63/energy)), 4)  + 1.0767e1*pow( (1-(12.63/energy)), 5));
  }
  static Scalar sigmaDI(Scalar energy)
  {
    if (energy <= 14.25) return 0.0 ;
    return  (7.1377e-19/energy)*(1.6074*log(energy/14.01)  - 1.4713*(1-(14.01/energy)) 
				 -2.7386e-1*pow( (1-(14.01/energy)), 2)  + 1.9556e-1*pow( (1-(14.01/energy)), 3) 
				 + 1.1343e-1*pow( (1-(14.01/energy)), 4) + 9.0166e-3*pow( (1-(14.01/energy)), 5));
  }  
  static Scalar sigmaDE1(Scalar energy)
  {
    if (energy <= 8.8) return 0.0;
    return 5.679e-19*(pow((1- (8.8/energy)), 3)/energy)*log(2.71828 + 0.15*energy);
  }  
  static Scalar sigmaDE2(Scalar energy)
  {
    if(energy <= 9.4) return 0;
    return 1.076e-19*(pow((1- (9.4/energy)), 3)/energy)*log(2.71828 + 0.15*energy);
  }
  static Scalar sigmaeE(Scalar energy) // energy assumed >=0.05 eV
  {
    return 1e-20*(3.93/(pow(energy, 0.5)+445.0*pow(energy, 2.3)) 
		  + 46.2/(pow(energy, 0.094) + 9e-6*pow(energy, 1.2) 
			  + 2.845e-18*pow(energy, 3.8) + 5.81e-22*pow(energy, 4.4)));
  }
  static Scalar sigmahE(Scalar energy) 
  {
    return 1.65e-19/(pow(energy, 0.5) + 0.5*pow(energy, 2.5));
  }
};

class CH3_gas : public Hydrocarbons_model
{
public:
  CH3_gas(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, 
	  ostring sec_e, ostring sec_i, ostring sec_iminusH, ostring sec_iminusH2,
	  ostring sec_nminusH, ostring sec_nminusH2, ostring sec_nminusH3, 
	  ostring sec_H, ostring sec_H2, ostring sec_p)
    :Hydrocarbons_model(slist, sp,  _useSecDefaultWeight, sec_e, sec_i, sec_iminusH, sec_iminusH2, 
			sec_nminusH, sec_nminusH2, sec_nminusH3,  sec_H, sec_H2, sec_p)
  {
    pkgname = "CH3_gas";
    u_gas = &un;
    eFlag_gas = false;

    add_collider(E);
    add_xsection(sigmaI, 9.84 ,  E_IONIZATION); 
    add_xsection(sigmaDI, 15.12 ,  E_DISSIONIZATION);
    add_xsection(sigmaDE1, 9.5 ,  E_DISSEXCITATION1);
    add_xsection(sigmaDE2, 10.0 ,  E_DISSEXCITATION2);
    
    add_collider(H_ions);
    add_xsection(sigmaeE, 0.0,  P_eEXCHANGE);
  }
  virtual ~CH3_gas(){}  
  
  // Functions needed for reaction kinetics:
  //------------------
  virtual Scalar get_DIeEnergyloss(void) const {return 20.4;}

  virtual Scalar get_DE1energy(void) const { return 4.7; }
  virtual Scalar get_DE2energy (void) const { return 5.5;}
  virtual Scalar get_DIenergy(void) const { return 5.3;}

  virtual Scalar getm_nminusH(void) const {return 26.61e-27;}
  virtual Scalar getm_nminusH2(void) const {return 23.27e-27;}
  virtual Scalar getm_iminusH(void) const {return 26.61e-27;} 

  // Cross sections
  //------------------
  static Scalar sigmaI(Scalar energy)
  {
    if (energy <= 9.84) return 0.0;
    return  (1.0204e-18/energy)*(1.9725*log(energy/9.8) - 2.1011*(1-(9.8/energy))
				 + 1.0593*pow( (1-(9.8/energy)), 2)- 6.3438*pow( (1-(9.8/energy)), 3) 
				 + 8.0140*pow( (1-(9.8/energy)), 4) -4.244 *pow( (1-(9.8/energy)), 5));
  }
  static Scalar sigmaDI(Scalar energy)
  {
    if (energy <= 15.12) return 0.0; 
    return  (7.1428e-19/energy)*(1.2824*log(energy/14.0) -1.3906*(1-(14.0/energy)) 
				 +6.2993e-1*pow( (1-(14.0/energy)), 2) + 9.4521e-1*pow( (1-(14.0/energy)), 3) 
				 -5.3629*pow( (1-(14.0/energy)), 4) +4.3087*pow( (1-(14.0/energy)), 5));
  }  
  static Scalar sigmaDE1(Scalar energy)
  {
    if (energy <= 9.5) return 0;
    return 5.37e-19*(pow((1- (9.5/energy)), 3)/energy)*log(2.71828 + 0.15*energy);
  }  
  static Scalar sigmaDE2(Scalar energy)
  {
    if(energy <= 10) return 0;
    return 9.058e-19*(pow((1- (10.0/energy)), 3)/energy)*log(2.71828 + 0.15*energy);
  }
  static Scalar sigmaeE(Scalar energy) // energy assumed >=0.05 eV
  {
    return 1e-20*(17.0/(pow(energy, 0.5)+385.0*pow(energy, 2.5)) 
		  + 51.3/(pow(energy, 0.096) + 2e-9*pow(energy, 2.0) 
			  + 5.5e-21*pow(energy, 4.3) ));
  }
};

class CH2_gas : public Hydrocarbons_model
{
public:
  CH2_gas(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, 
	  ostring sec_e, ostring sec_i, ostring sec_iminusH, ostring sec_iminusH2,
	  ostring sec_nminusH, ostring sec_nminusH2, ostring sec_nminusH3, 
	  ostring sec_H, ostring sec_H2, ostring sec_p)
    :Hydrocarbons_model(slist, sp,  _useSecDefaultWeight, sec_e, sec_i, sec_iminusH, sec_iminusH2, 
			sec_nminusH, sec_nminusH2, sec_nminusH3,  sec_H, sec_H2, sec_p)
  {
    pkgname = "CH2_gas";
    u_gas = &un;
    eFlag_gas = false;

    add_collider(E);
    add_xsection(sigmaI,10.4 ,  E_IONIZATION); 
    add_xsection(sigmaDI,15.53 ,  E_DISSIONIZATION);
    add_xsection(sigmaDE1, 8.5 ,  E_DISSEXCITATION1);

    add_collider(H_ions);
    add_xsection(sigmaeE, 0.0,  P_eEXCHANGE);
    add_xsection(sigmahE, 0.0,  P_hEXCHANGE); 
  }

  virtual ~CH2_gas(){}  
  
  // Functions needed for reaction kinetics:
  //------------------
  virtual Scalar get_DIeEnergyloss(void) const {return 20.4;}

  virtual Scalar get_DE1energy(void) const { return 4.25; }
  virtual Scalar get_DIenergy(void) const { return 5.0;}
  virtual Scalar get_CXhenergy(void)  const {return 5.17;}

  virtual Scalar getm_nminusH(void) const {return 23.27e-27;}
  virtual Scalar getm_nminusH2(void) const {return 19.93e-27;}
  virtual Scalar getm_iminusH(void) const {return 23.27e-27;}
  
  //   Cross sections
  //------------------
  static Scalar sigmaI(Scalar energy)
  {
    if (energy <= 10.4) return 0.0 ;
    return  (9.6153e-19/energy)*(1.7159*log(energy/10.4) - 1.7164*(1-(10.4/energy)) 
				 -6.5529e-1*pow( (1-(10.4/energy)), 2) + 2.1724*pow( (1-(10.4/energy)), 3) 
				 -5.4186*pow( (1-(10.4/energy)), 4) + 3.1616*pow( (1-(10.4/energy)), 5));   
  }
  static Scalar sigmaDI(Scalar energy)
  {
    if (energy <= 15.53) return 0.0 ;
    return  (6.4391e-19/energy)*(8.1919e-1*log(energy/15.53) - 7.5016e-1*(1-(15.53/energy)) 
				 - 3.8063e-3*pow((1-(15.53/energy)), 2) + 1.4065*pow( (1-(15.53/energy)), 3) 
				 - 3.6447*pow( (1-(15.53/energy)), 4) + 2.6220*pow( (1-(15.53/energy)), 5));   
  }  
  static Scalar sigmaDE1(Scalar energy)
  {
    if (energy <= 8.5) return 0;
    return 4.920e-19*(pow((1- (8.5/energy)), 3)/energy)*log(2.71828 + 0.15*energy);
  }
  
  static Scalar sigmaeE(Scalar energy) // energy assumed >=0.05 eV
  {
    return 1e-20*(7.32/(pow(energy, 0.5)+ 0.005*pow(energy, 3.0)) 
		  + 20.95*exp((-1.55)/pow(energy, 0.57))/(1.0 + 2.35e-7*pow(energy, 1.55) 
			  + 5.86e-21*pow(energy, 4.26)  ));
  }
  
  static Scalar sigmahE(Scalar energy)
  {
    return 1.301e-19/(pow(energy, 0.5) + 0.5*pow(energy, 2.5));
  }  
};

class CH_gas : public Hydrocarbons_model
{
public:
  CH_gas(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, 
	 ostring sec_e, ostring sec_i, ostring sec_iminusH, ostring sec_iminusH2,
	 ostring sec_nminusH, ostring sec_nminusH2, ostring sec_nminusH3, 
	 ostring sec_H, ostring sec_H2, ostring sec_p)
    :Hydrocarbons_model(slist, sp,  _useSecDefaultWeight, sec_e, sec_i, sec_iminusH, sec_iminusH2, 
			sec_nminusH, sec_nminusH2, sec_nminusH3,  sec_H, sec_H2, sec_p)
  {
    pkgname = "CH_gas";
    u_gas = &un;
    eFlag_gas = false;
    
    add_collider(E);
    add_xsection(sigmaI,11.13 ,  E_IONIZATION); 
    add_xsection(sigmaDI,14.74 ,  E_DISSIONIZATION);
    add_xsection(sigmaDE1, 7.0 ,  E_DISSEXCITATION1);

    add_collider(H_ions);
    add_xsection( sigmaeE, 0.0,  P_eEXCHANGE);
    add_xsection( sigmahE, 0.0,  P_hEXCHANGE);
  }
  virtual ~CH_gas(){}  

  // Functions needed for reaction kinetics:
  //------------------
  virtual Scalar get_DIeEnergyloss(void) const {return 18.35;}

  virtual Scalar get_DE1energy(void) const  {return 3.5;}
  virtual Scalar get_DIenergy(void) const {return 4.31;}
  virtual Scalar get_CXhenergy(void)  const {return 5.28;}

  virtual Scalar getm_nminusH(void) const {return 19.93e-27;}
  virtual Scalar getm_iminusH(void) const {return 19.93e-27;}
  
  // Cross sections
  //------------------
  static Scalar sigmaI(Scalar energy)
  {
    if (energy <= 11.13) return 0.0 ; 
    return  (8.9847e-19/energy)*(1.4439*log(energy/11.3) - 1.2724*(1-(11.3/energy)) 
				 - 2.2221*pow( (1-(11.3/energy)), 2) + 9.2822*pow( (1-(11.3/energy)), 3) 
				 - 1.5506e1*pow( (1-(11.3/energy)), 4) + 8.2778*pow( (1-(11.3/energy)), 5));   
  }
  static Scalar sigmaDI(Scalar energy)
  {
    if (energy <= 14.80) return 0.0 ; // indicated to be 14.74, changed to 14.8 to avoid negative crossx
    return  (6.7567e-19/energy)*(4.3045e-1*log(energy/14.8) - 4.1305e-1*(1-(14.8/energy)) 
				 -5.6881e-1*pow( (1-(14.8/energy)), 2) + 3.2957*pow( (1-(14.8/energy)), 3) 
				 - 5.6549*pow( (1-(14.8/energy)), 4) + 3.4295*pow( (1-(14.8/energy)), 5));   
  }  
  static Scalar sigmaDE1(Scalar energy)
  {
    if (energy <= 7.0) return 0;
    return 4.463e-19*(pow((1- (7.0/energy)), 3)/energy)*log(2.71828 + 0.15*energy);
  }

  static Scalar sigmaeE(Scalar energy) // energy assumed >=0.05 eV
  {
    return 1e-20*(4.28/(pow(energy, 0.5)+ 0.001*pow(energy, 3.0)) 
		  + 20.2*exp((-5.3)/pow(energy, 0.35))/(1.0 + 1.12e-6*pow(energy, 1.45) 
			  + 1.1e-20*pow(energy, 4.3)  ));
  }

  static Scalar sigmahE(Scalar energy)
  {
    return 9.517e-20/(pow(energy, 0.5) + 0.01*pow(energy, 3.5));
  }  
};

class Carbon : public Hydrocarbons_model
{
public:
  Carbon(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, 
	 ostring sec_e, ostring sec_i, ostring sec_iminusH, ostring sec_iminusH2,
	 ostring sec_nminusH, ostring sec_nminusH2, ostring sec_nminusH3, 
	 ostring sec_H, ostring sec_H2, ostring sec_p)
    :Hydrocarbons_model(slist, sp,  _useSecDefaultWeight, sec_e, sec_i, sec_iminusH, sec_iminusH2, 
			sec_nminusH, sec_nminusH2, sec_nminusH3,  sec_H, sec_H2, sec_p)
  {
    pkgname = "Carbon";
    u_gas = &un;
    eFlag_gas = false;
  
    // Allocate cross-section data
    add_collider(E);    
    add_xsection(sigmaI, 11.26,  E_IONIZATION);
  
    add_collider(H_ions);
    add_xsection(sigmaeE, 0.0,  P_eEXCHANGE);
  }

  virtual ~Carbon(){}
  // virtual int get_gasatomicnumber(){ return 6; }  // Not used here in the considered Carbon reactions.     
  
  // Cross sections:
  static  Scalar sigmaI(Scalar energy)
  {
    if (energy <= 11.26) return 0.0;
    else return   (8.88E-19/energy)*(2.1143*log(energy/11.26) - 1.9647*(1-(11.26/energy)) - 6.084e-1*pow(1-(11.26/energy), 2));
  } 

  static  Scalar sigmaeE(Scalar energy)
  {
    return 1e-20*(2.6838e2*exp((-1.2e-2)*energy)/(1.14e9*pow(energy, -3.02)) 
		  + 14.2*exp((-6.86e2)/energy)/(1.0 + 1.96e-9*pow(energy, 2.0) + 1.71e-23*pow(energy, 4.5)  ));
  }
};

class CH4_ions_gas : public Hydrocarbons_model
{
public:
  CH4_ions_gas(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, 
	 ostring sec_e, ostring sec_i, ostring sec_iminusH, ostring sec_iminusH2,
	 ostring sec_nminusH, ostring sec_nminusH2, ostring sec_nminusH3, 
	 ostring sec_H, ostring sec_H2, ostring sec_p)
    :Hydrocarbons_model(slist, sp,  _useSecDefaultWeight, sec_e, sec_i, sec_iminusH, sec_iminusH2, 
			sec_nminusH, sec_nminusH2, sec_nminusH3,  sec_H, sec_H2, sec_p)
  {
    pkgname = "CH4_ions_gas";
    u_gas = &ui;
    eFlag_gas = false;
    
    add_collider(E);
    add_xsection( sigmaDR1, 0.,  E_DISSRECOMBINATION1); 
    add_xsection( sigmaDR2, 0.,  E_DISSRECOMBINATION2);
    add_xsection( sigmaDR3, 0.,  E_DISSRECOMBINATION3);
    add_xsection( sigmaDR3, 5.2,  E_DISSEXCITATION3);
    add_xsection( sigmaDE4, 6.5,  E_DISSEXCITATION4);
    add_xsection( sigmaDE5, 8.0,  E_DISSEXCITATION5);
  }
  virtual ~CH4_ions_gas(){}  
  
  // Functions needed for reaction kinetics:
  //------------------
  virtual Scalar get_DR1energy(void) const {return 8.17;}
  virtual Scalar get_DR2energy(void) const {return 7.83;}
  virtual Scalar get_DR3energy(void) const {return 3.42;}
  virtual Scalar get_DE3energy(void) const {return 3.6;}
  virtual Scalar get_DE4energy(void) const {return 4.0;}
  virtual Scalar get_DE5energy(void) const {return 2.6;}

  virtual Scalar getm_nminusH(void) const {return 29.95e-27;}  
  virtual Scalar getm_nminusH2(void) const {return 26.61e-27;}  
  virtual Scalar getm_nminusH3(void) const {return 23.27e-27;}
  virtual Scalar getm_iminusH(void) const {return 29.95e-27;}
  virtual Scalar getm_iminusH2(void) const {return 26.61e-27;}
  
  // Cross sections
  //------------------  

  static Scalar sigmaDR1(Scalar energy) 
  {
    if (energy <= 1e-4) return 6.30e-16; 
    return 0.63e-20/(pow(energy,1.25)*(1+0.1*energy));
  }
  static Scalar sigmaDR2(Scalar energy) 
  {
    if (energy <= 1e-4) return 1.29e-15; 
    return 1.29e-20/(pow(energy,1.25)*(1+0.1*energy));
  }
  static Scalar sigmaDR3(Scalar energy) 
  {
    if (energy <= 1e-4) return 7.49e-16; 
    return 0.75e-20/(pow(energy,1.25)*(1+0.1*energy));
  }
    static Scalar sigmaDE3(Scalar energy)
  {
       if(energy <= 5.2) return 0.0; 
     return 3.313e-19*pow(1-(5.2/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
   }
  static Scalar sigmaDE4(Scalar energy)
   {
     if (energy <= 6.5) return 0.0;
     return 1.288e-19*pow(1-(6.5/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
   }
  static Scalar sigmaDE5(Scalar energy) 
    {
      if(energy <= 8.0) return 0.0; 
      return 2.897e-19*pow(1-(8.0/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
    }
};

class CH3_ions_gas : public Hydrocarbons_model
{
public:
  CH3_ions_gas(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, 
	 ostring sec_e, ostring sec_i, ostring sec_iminusH, ostring sec_iminusH2,
	 ostring sec_nminusH, ostring sec_nminusH2, ostring sec_nminusH3, 
	 ostring sec_H, ostring sec_H2, ostring sec_p)
    :Hydrocarbons_model(slist, sp,  _useSecDefaultWeight, sec_e, sec_i, sec_iminusH, sec_iminusH2, 
			sec_nminusH, sec_nminusH2, sec_nminusH3,  sec_H, sec_H2, sec_p)
  {
    pkgname = "CH3_ions_gas";
    u_gas = &ui;
    eFlag_gas = false;
    
    add_collider(E);
    add_xsection(sigmaDR1, 0.,  E_DISSRECOMBINATION1); 
    add_xsection(sigmaDR2, 0.,  E_DISSRECOMBINATION2);
    add_xsection(sigmaDR3, 0.,  E_DISSRECOMBINATION3);
    add_xsection( sigmaDE3, 10.6,  E_DISSEXCITATION3);
    add_xsection( sigmaDE4, 12.0,  E_DISSEXCITATION4);
    add_xsection( sigmaDE5, 11.0,  E_DISSEXCITATION5);
  }
  virtual ~CH3_ions_gas(){} 

  // Functions needed for reaction kinetics:
  //------------------
  virtual Scalar get_DR1energy(void) const {return 4.97;}
  virtual Scalar get_DR2energy(void) const {return 5.1;}
  virtual Scalar get_DR3energy(void) const {return 1.57;}
  virtual Scalar get_DE3energy(void) const {return 5.3;}
  virtual Scalar get_DE4energy(void) const {return 6.1;}
  virtual Scalar get_DE5energy(void) const {return 2.5;}

  virtual Scalar getm_nminusH(void) const {return 26.61e-27;}  
  virtual Scalar getm_nminusH2(void) const {return 23.27e-27;}  
  virtual Scalar getm_nminusH3(void) const {return 19.93e-27;}
  virtual Scalar getm_iminusH(void) const {return 26.61e-27;}
  virtual Scalar getm_iminusH2(void) const {return 23.27e-27;}  

  // Cross sections
  //------------------  
    
  static Scalar sigmaDR1(Scalar energy) 
  {
    if (energy <= 1e-4) return 4.82e-16; 
    return 1.92e-20/(pow(energy,1.1)*pow((1+0.8*energy),0.5));
  }
  static Scalar sigmaDR2(Scalar energy) 
  {
    if (energy <= 1e-4) return 1.90e-16; 
    return 0.76e-20/(pow(energy,1.1)*pow((1+0.8*energy),0.5));
  }
  static Scalar sigmaDR3(Scalar energy) 
  {
    if (energy <= 1e-4) return 3.61e-16; 
    return 1.44e-20/(pow(energy,1.1)*pow((1+0.8*energy),0.5));
  }
   static Scalar sigmaDE3(Scalar energy)
   {
     if(energy <= 10.6) return 0.0; 
     return 1.821e-19*pow(1-(10.6/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
   }
   static Scalar sigmaDE4(Scalar energy)
   {
     if (energy <= 12.0) return 0.0;
     return 8.893e-20*pow(1-(12.0/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
   }
  static Scalar sigmaDE5(Scalar energy) 
    {
      if(energy <= 11.0) return 0.0; 
      return 3.664e-19*pow(1-(11.0/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
    }
};

class CH2_ions_gas : public Hydrocarbons_model
{
public:
  CH2_ions_gas(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, 
	       ostring sec_e, ostring sec_i, ostring sec_iminusH, ostring sec_iminusH2,
	       ostring sec_nminusH, ostring sec_nminusH2, ostring sec_nminusH3, 
	       ostring sec_H, ostring sec_H2, ostring sec_p)
    :Hydrocarbons_model(slist, sp,  _useSecDefaultWeight, sec_e, sec_i, sec_iminusH, sec_iminusH2, 
			sec_nminusH, sec_nminusH2, sec_nminusH3,  sec_H, sec_H2, sec_p)
  {
    pkgname = "CH2_ions_gas";
    u_gas = &ui;
    eFlag_gas = false;
    
    add_collider(E);
    add_xsection(sigmaDR1, 0.,  E_DISSRECOMBINATION1); 
    add_xsection(sigmaDR2, 0.,  E_DISSRECOMBINATION2);
    add_xsection( sigmaDE3, 12.0,  E_DISSEXCITATION3);
    add_xsection( sigmaDE5, 9.0,  E_DISSEXCITATION5);  
}
  virtual ~CH2_ions_gas(){}  

  // Functions needed for reaction kinetics:
  //------------------
  virtual Scalar get_DR1energy(void) const {return 6.0;}
  virtual Scalar get_DR2energy(void) const {return 7.0;}
  virtual Scalar get_DE3energy(void) const {return 7.0;}
  virtual Scalar get_DE5energy(void) const {return 2.4;}

  virtual Scalar getm_nminusH(void) const {return 23.27e-27;}  
  virtual Scalar getm_nminusH2(void) const {return 19.93e-27;}  
  virtual Scalar getm_iminusH(void) const {return 23.27e-27;}

  // Cross sections
  //------------------  
  
  static Scalar sigmaDR1(Scalar energy) 
  {
    if (energy <= 1e-4) return 6.65e-16; 
    return 1.67e-20/(pow(energy,1.15)*pow((1+1.2*energy),0.5));
     }
  static Scalar sigmaDR2(Scalar energy) 
  {
    if (energy <= 1e-4) return 1.68e-15 ; 
    return 4.22e-20/(pow(energy,1.15)*pow((1+1.2*energy),0.5));
  }
  static Scalar sigmaDE3(Scalar energy)
   {
     if(energy <= 12.0) return 0.0; 
     return 9.803e-20*pow(1-(12.0/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
   }
  static Scalar sigmaDE5(Scalar energy) 
    {
      if(energy <= 9.0) return 0.0; 
      return 3.393e-19*pow(1-(9.0/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
    }
};

class CH_ions_gas : public Hydrocarbons_model
{
public:
  CH_ions_gas(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, 
	      ostring sec_e, ostring sec_i, ostring sec_iminusH, ostring sec_iminusH2,
	      ostring sec_nminusH, ostring sec_nminusH2, ostring sec_nminusH3, 
	      ostring sec_H, ostring sec_H2, ostring sec_p)
    :Hydrocarbons_model(slist, sp,  _useSecDefaultWeight, sec_e, sec_i, sec_iminusH, sec_iminusH2, 
			sec_nminusH, sec_nminusH2, sec_nminusH3,  sec_H, sec_H2, sec_p)
  {
    pkgname = "CH_ions_gas";
    u_gas = &ui;
    eFlag_gas = false;
    
    add_collider(E);
    add_xsection(sigmaDR1, 0.,  E_DISSRECOMBINATION1); 
    add_xsection( sigmaDE3, 2.5,  E_DISSEXCITATION3); 
    // For the Dissociative excitation of type 1, the proper dissociation channel is neglected,
    // but the capture ionization dissociation is considered for its near threshold behavior.
    add_xsection( sigmaDE5, 5.0,  E_DISSEXCITATION5);  
  }
  
  virtual ~CH_ions_gas(){}
  
  // Functions needed for reaction kinetics:
  //------------------
  virtual Scalar get_DR1energy(void) const {return 7.18;}
  virtual Scalar get_DE3energy(void) const {return 4.0;}
  virtual Scalar get_DE5energy(void) const {return  2.0;}

  virtual Scalar getm_nminusH(void) const {return 19.93e-27;}  
  virtual Scalar getm_iminusH(void) const {return 19.93e-27;}
  
  // Cross sections
  //------------------    
  static Scalar sigmaDR1(Scalar energy) 
  {
    if (energy <= 1e-4) return 3.15e-17; 
    return 3.16e-20/(pow(energy,0.75)*(1+1.13*energy));
  }  
  static Scalar sigmaDE3(Scalar energy)
   {
     if(energy <= 2.5) return 0.0; 
     return 2.06e-19*pow(1-(2.5/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
   }
  static Scalar sigmaDE5(Scalar energy) 
  {
    if(energy <= 5.0) return 0.0; 
    return 2.675e-19*pow(1-(5.0/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
  }
};

#endif
