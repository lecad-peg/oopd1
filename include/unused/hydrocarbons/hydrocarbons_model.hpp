#ifndef HYDROCARBONS_MODEL_DECLARED
#define HYDROCARBONS_MODEL_DECLARED
#include "MCC/mcc.hpp"

class Hydrocarbons_model: public MCC
{
protected: 
  Species* sec_iminusHSpecies;  // CH{y-1}+ , where the main species is of the form CHy or CHy+
  Species* sec_iminusH2Species; // CH{y-2}+
  Species* sec_nminusHSpecies;  // CH{y-1}
  Species* sec_nminusH2Species; // CH{y-2}
  Species* sec_nminusH3Species; // CH{y-3}
  Species* sec_HSpecies;  // Hydrogen
  Species* sec_H2Species; // Dihydrogen
  Species* sec_pSpecies;  // Proton H+
  
public:
  Hydrocarbons_model(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, 
		     ostring sec_e, ostring sec_i, 
		     ostring sec_iminusH, ostring sec_iminusH2,
		     ostring sec_nminusH, ostring sec_nminusH2, ostring sec_nminusH3, 
		     ostring sec_H, ostring sec_H2, ostring sec_p);
  virtual void dynamic(const Xsection& cx);   
  
  // Reactions:
  /////////////
  
  // Reactions on neutral species:
  virtual void eElastic(const Xsection& cx); 
  virtual void eExcitation(const Xsection& cx);  
  void pElastic(const Xsection& cx); // no data found  
  void eDissExcitation1(const Xsection& cx);
  void eDissExcitation2(const Xsection& cx);  
  // virtual void eIonization(const Xsection& cx); // No data found. Default model is used.  
  void eDissIonization(const Xsection & cx);  
  void pElectronExchange(const Xsection& cx);
  void pHydrogenExchange(const Xsection& cx);  
  
  // Reactions on ion species:
  void eDissExcitation3(const Xsection& cx);
  void eDissExcitation4(const Xsection& cx);
  void eDissExcitation5(const Xsection& cx);
  void eDissRecombination1(const Xsection& cx);
  void eDissRecombination2(const Xsection& cx);
  void eDissRecombination3(const Xsection& cx);
  
  // Functions used in common:
  void Any_eExcitation(const Xsection& cx, Scalar targetmass);
  void Any_DissociationInTwo(Vector3 & target_velocity, Species* s1, 
			   Species* s2, Scalar m1, Scalar m2, Scalar ReleasedEnergy);

  void Any_DissociationInThree(Vector3 & target_velocity, Species* s1, 
						  Species* s2, Species* s3, Scalar m1, Scalar m2, 
						  Scalar m3, Scalar ReleasedEnergy);
    
  // data necessary for reactions:
  ////////////////////////////////

  // Average loss energy of incident species (if not threshold)
  virtual Scalar get_DIeEnergyloss(void) const
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}

  // Secondary species masses: 
  // Warning: the masses we use correspond to deuterium ! 
  virtual Scalar getm_iminusH(void) const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar getm_iminusH2(void) const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar getm_nminusH(void) const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar getm_nminusH2(void) const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar getm_nminusH3(void) const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  Scalar getm_H(void)  const { return 3.34e-27;}
  Scalar getm_H2(void) const { return 6.68e-27;}
  Scalar getm_p(void) const  { return 3.34e-27;}
  
  // Released kinetic energy
  virtual Scalar get_DE1energy(void) const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar get_DE2energy(void)  const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar get_DE3energy(void)  const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar get_DE4energy(void)  const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar get_DE5energy(void)  const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar get_DIenergy(void) const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar get_DR1energy(void) const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar get_DR2energy(void) const  
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar get_DR3energy(void)  const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar get_CXeenergy(void)  const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}
  virtual Scalar get_CXhenergy(void)  const 
  { fprintf (stderr, "\nWarning: use of uninitialized value in Hydrocarbons data\n"); return - 1.0;}

};  

#endif
