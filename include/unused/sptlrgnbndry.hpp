class SpatialRegionBoundary : public Boundary
{
  Scalar *sendbuffer;
  Scalar *recvbuffer;
  int buffersize; // total number of particles available in the buffer
  int nsp;        // number of species
  int particlecount; // number of particles currently in the buffer
  int pindex;  // index into sendbuffer array;
  int sizeperparticle; // number of Scalars per particle
  int oldbuffersize, buffermult;
  int nv;  // number of velocity components
  int nw; // relative location of particle weight

 public:
  SpatialRegionBoundary(Fields *t, int i) : Boundary(t)
    {
      sendbuffer = recvbuffer = NULL;

      setdefaults();
      setparamgroup();
      j0 = j1 = i;
      check();
      init();      
    }

  ~SpatialRegionBoundary();

  // virtual functions from Parse
  void setdefaults(void);   
  void check(void);
  void init(void);

  int particleBC(ParticleGroup *p, int i, Scalar frac);

  void allocatebuffers(void);
  void sendparticles(void);  //communicate particles bt spatialregions
  void recvparticles(void);  //process recvd particles
};
