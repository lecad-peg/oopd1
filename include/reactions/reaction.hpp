// Reaction class
// by HyunChul Kim, 2006
// JTG and MAL melded file, 10/12/09 (no changes)
#ifndef REACTION_H
#define REACTION_H
#include "reactions/reactiontype.hpp" 
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "xsections/xsection.hpp"

#define debugReactionGroup // prints all reaction groups to the terminal, with debug info, MAL200117

//class Xsection;
class Species;
// mindgame
// i.e. E_Ionization
// reactant[0] + reactant[1] -> reactant[0] + product[0] + product[1];
// class for single reaction.

class Reaction
{
private:
  ostring get_speciesname(Species *sp) const;
  ostring get_speciesname_short(Species *sp) const;

protected:
  Xsection *xsection;
  ReactionType reactiontype;
  ostring mcctype;
  // mindgame: the priority in reactants and products_new is meaningful.
  //           the energy in xsection is dependent on the mass of reactant[0].
  vector<Species *> reactants;
  vector<Species *> products_new;
  vector<Scalar> characteristic_values;
  GasType gastype;
public:
  Reaction(Reaction *reaction)
  {
    xsection = reaction->get_xsection();
    reactiontype = reaction->get_reactiontype();
    reactants = reaction->get_reactants();
    products_new = reaction->get_products();
    mcctype = reaction->get_mcctype();
    characteristic_values = reaction->get_characteristic_values();
    gastype = reaction->get_gastype();
  }
  Reaction(GasType _gastype,
	   ReactionType _reactiontype, Species *reactant1, Species *reactant2,
	   Xsection *_xsection, const char *_mcctype, bool as_it_is,
	   Species *product_new1, Species *product_new2,
	   Species *product_new3, Species *product_new4)
  {
    init(_gastype, _reactiontype, reactant1, reactant2, _xsection, _mcctype);
    if (as_it_is || product_new1 || product_new2 || product_new3 || product_new4)
    {
      add_product(product_new1);
      add_product(product_new2);
      add_product(product_new3);
      add_product(product_new4);
    }
  }
  Reaction(GasType _gastype,
	   ReactionType _reactiontype, Species *reactant1, Species *reactant2,
	   Xsection *_xsection, const char *_mcctype, bool as_it_is,
	   Species *product_new1, Species *product_new2,
	   Species *product_new3)
  {
    init(_gastype, _reactiontype, reactant1, reactant2, _xsection, _mcctype);
    if (as_it_is || product_new1 || product_new2 || product_new3)
    {
      add_product(product_new1);
      add_product(product_new2);
      add_product(product_new3);
    }
  }
  Reaction(GasType _gastype,
	   ReactionType _reactiontype, Species *reactant1, Species *reactant2,
	   Xsection *_xsection, const char *_mcctype, bool as_it_is,
	   Species *product_new1, Species *product_new2)
  {
    init(_gastype, _reactiontype, reactant1, reactant2, _xsection, _mcctype);
    if (as_it_is || product_new1 || product_new2)
    {
      add_product(product_new1);
      add_product(product_new2);
    }
  }
  Reaction(GasType _gastype,
	   ReactionType _reactiontype, Species *reactant1, Species *reactant2,
	   Xsection *_xsection, const char *_mcctype, bool as_it_is,
	   Species *product_new1)
  {
    init(_gastype, _reactiontype, reactant1, reactant2, _xsection, _mcctype);
    if (as_it_is || product_new1)
    {
      add_product(product_new1);
    }
  }
  Reaction(GasType _gastype,
	   ReactionType _reactiontype, Species *reactant1, Species *reactant2,
	   Xsection *_xsection, const char *_mcctype)
  {
    init(_gastype, _reactiontype, reactant1, reactant2, _xsection, _mcctype);
  }
  void init(GasType _gastype,
	    ReactionType _reactiontype, Species *reactant1, Species *reactant2,
	    Xsection *_xsection, const char *_mcctype)
  {
    xsection = _xsection;
    reactiontype = _reactiontype;
    mcctype = _mcctype;
    gastype = _gastype;
    if (reactant1) add_reactant(reactant1);
    if (reactant2) add_reactant(reactant2);
  }
  void add_reactant(Species *reactant_sp)
  {
    reactants.push_back(reactant_sp);
  }
  void add_product(Species *product_sp)
  {
    products_new.push_back(product_sp);
  }
  void pop_products(int i=1)
  {
    for (; i>0; i--) products_new.pop_back();
  }

  ostring get_mcctype() const {return mcctype;}
  ReactionType get_reactiontype() const {return reactiontype;}
  ostring get_reactiontypename() const {return ReactionTypeName(reactiontype);}
  GasType get_gastype() const {return gastype;}
  ostring get_gasname() const {return GasTypeName(gastype);}
  vector<Scalar> get_characteristic_values() const {return characteristic_values;}
  Scalar get_characteristic_values(int i) const {return characteristic_values[i];}
  void add_characteristic_value(Scalar a) {characteristic_values.push_back(a);}
  void set_characteristic_values(vector<Scalar> _characteristic_values)
       {characteristic_values = _characteristic_values;}
  bool is_characteristic_value_empty() const {return characteristic_values.empty();}
  bool is_reactant_empty2() const {return reactants.empty();}
  bool is_reactant_empty() const
  {
    if (reactants.empty()) return true;
    else for (int j=reactants.size()-1; j>=0; j--)
    {
      if (reactants[j] != NULL) return false;
    }
    return true;
  }
  bool is_product_empty2() const {return products_new.empty();}
  bool is_product_empty() const
  {
    if (products_new.empty()) return true;
    else for (int j=products_new.size()-1; j>=0; j--)
    {
      if (products_new[j] != NULL) return false;
    }
    return true;
  }
  bool is_xsection_empty() const {return (xsection == NULL);}
  bool is_empty() const
  {
    return (is_reactant_empty() && is_xsection_empty() && is_product_empty());
  }
  void set_reactant(vector<Species *> _reactants) {reactants = _reactants;}
  void set_product(vector<Species *> _products) {products_new = _products;}
  void set_xsection(Xsection *_xsection) {xsection = _xsection;}
  vector<Species *> get_reactants() const {return reactants;}
  vector<Species *> get_products() const {return products_new;}
  Species * get_products(int i) const
  {
    if (i < (int)products_new.size()) return products_new[i];
    else return NULL;
  }
  Xsection* get_xsection() const {return xsection;}

  void print_chemical_equation(bool _debug) const;
  ostring get_chemical_equation_short(bool _debug) const;
  ostring get_chemical_equation(bool _debug) const;
  // return threshold energy of cross section; JK 2019-12-02
  double get_threshold_energy(void) {return xsection->get_threshold(); }
};

class ReactionList
{
	vector<Reaction *> reactions;
public:
	void deleteAll()
	{
		for (int i=reactions.size()-1; i>=0; i--)
		{
			delete reactions[i];
		}
	}
	void add(Reaction *reaction) {reactions.push_back(reaction);}
	void add(ReactionList _reactions)
	{
		int num = _reactions.num();
		for (int i=0; i<num; i++)
			reactions.push_back(_reactions.data(i));
	}
	void add(oopicList<Reaction> reaction)
	{
		oopicListIter<Reaction> thei(reaction);
		for (; !thei.Done(); thei++)
		{
			reactions.push_back(thei());
		}
	}
	Reaction *data(int i=0) const {return reactions[i];}
	int num() const {return reactions.size();}

	bool include(ReactionType reactiontype) const
	{
		int num = reactions.size();
		for (int i=0; i<num; i++)
		{
			if (reactions[i]->get_reactiontype() == reactiontype)
				return true;
		}
		return false;
	}

	ReactionList find(ReactionType reactiontype) const
	{
		ReactionList _reactions;
		int num = reactions.size();
		for (int i=0; i<num; i++)
		{
			if (reactions[i]->get_reactiontype() == reactiontype)
				_reactions.add(reactions[i]);
		}
		return _reactions;
	}
	// mindgame: Be aware that this function destroys some reactions
	//         in reactionlist.
	//           So, you would't want to reuse this object after this function
	//          is called.
	ReactionList find_n_erase(vector<Species *> _reactants)
	{
		ReactionList _reactions;
		int num = reactions.size();
		for (int i=0; i<num; i++)
		{
			if (reactions[i])
				if ((reactions[i]->get_reactants()[0] == _reactants[0]
																	&& reactions[i]->get_reactants()[1] == _reactants[1]) ||
						(reactions[i]->get_reactants()[1] == _reactants[0]
																		&& reactions[i]->get_reactants()[0] == _reactants[1]))
				{
					_reactions.add(reactions[i]);
					reactions[i] = NULL;
				}
		}
		return _reactions;
	}
	// mindgame: This is not general merging, but special kinds of merging.
	void merge_reactionlist(ReactionList _default_reactionlist,
			ReactionList &resultant_reactions) const;
	void print_reactionlist(bool _debug) const
	{
		int num = reactions.size();
		for (int i=0; i<num; i++)
			reactions[i]->print_chemical_equation(_debug);
		printf("\n");
	}

	// add to detect out-of-order reaction lists and put up a warning; MAL200116
	bool in_order_reactionlist() const
	{
		int num = reactions.size();
		if (num <= 1) return true;
		for (int i=1; i<num; i++)
			if (reactions[i]->get_reactants()[0] != reactions[0]->get_reactants()[0]) return false;
		return true;
	}
};

class MCC;
class DataArray;
// mindgame: class performing reactions
//           Inside the class, all reactions have
//          the same reactant1, reactan2, and mcctype.
class ReactionGroup
{
  ReactionList reactionlist;
  // mindgame: object regarding MCC Reaction.
  MCC *themcc;
  // mindgame: yet a blank for object regarding Fluid-Fluid Reaction.

  Species *reactant1, *reactant2;
public:
	ReactionGroup(ReactionList _reaction_list, SpeciesList *specieslist,
				Scalar dt, Fields *thefield);
	void update() const;
	inline  MCC * get_mcc() const  { return themcc; } // for dumpfile, MAL 8/12/10
	inline  Species * get_reactant1() const  { return reactant1; } // for dumpfile, MAL 8/12/10
	inline  Species * get_reactant2() const  { return reactant2; } // for dumpfile, MAL 8/12/10

	ReactionList * get_reaction_list() { return &reactionlist; }	//JK 2019-11-25; returning reaction list
	DataArray * get_collision_distribution(int reaction);

	void set_reactants();
	void print_reactionlist(bool _debug) const
	{
	printf("\n* Resultant Reaction Group from %s", reactionlist.data()->get_gasname().c_str());
	reactionlist.print_reactionlist(_debug);
	}
	void print_reactants() const;
  // mindgame: this should be moved to fluid-fluid class; move to MCC class, MAL200214
//	void fluid_fluid(Species *reactant1, Species *reactant2) const
//	{
//	}
	~ReactionGroup();
};

#endif
