// ReactionSubParse class
// by HyunChul Kim, 2006
#ifndef REACTIONSUBPARSE_H
#define REACTIONSUBPARSE_H

#include "main/parse.hpp"
#include "utils/message.hpp"
#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "reactions/reaction.hpp"
#include "xsections/xsection.hpp"

// mindgame: base class for reactionParses, includes all informations to define
//          reaction but no action is taken (action is supposed to be performed
//          in MCC and its derived classes).
class ReactionSubParse: public Parse, public Message
{
protected:
  vector<ostring> reactant1_names, reactant2_names;
  Xsection *xsection;
  vector<Species *> reactant1, reactant2;
  ReactionType reactiontype;
  SpeciesList *specieslist;
  oopicListIter<Species> species_iter;
  const ostring *gasname;
  Scalar e_threshold;
  Scalar c1, c2, sMax;
  Scalar b0, b1;
  Equation x_func;
  ostring reactionname;
  oopicList<Reaction> reactionlist;
  ostring x_file;

public:
  ReactionSubParse(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
	       oopicList<Parameter<int> > int_variables,
	       SpeciesList * slist, const ostring *_gasname);
  ~ReactionSubParse() {reactionlist.deleteAll(); delete xsection;}
  void setdefaults();
  void setparamgroup();
  void init();

  ReactionType get_reactiontype() const {return reactiontype;}
  Species *get_reactant1(int i) const {return reactant1[i];}
  Species *get_reactant2(int i) const {return reactant2[i];}
  Xsection *get_xsection() const {return xsection;}
  int reactant1_number() const {return reactant1.size();}
  int reactant2_number() const {return reactant2.size();}

  void build_xsection();
  void build_reactionlist();
  void convert_ostrings_to_species();
  oopicList<Reaction> & get_reactionlist()
  {
    return reactionlist;
  }
};

#endif
