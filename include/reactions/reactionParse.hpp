// ReactionParse
// by HyunChul Kim, 2006
// JTG and MAL melded file, 10/12/09
#ifndef REACTIONPARSE_H
#define REACTIONPARSE_H

#include "main/parse.hpp"
#include "utils/message.hpp"
#include "main/oopiclist.hpp"
#include "reactions/reaction.hpp"


class ReactionSubParse;
class Gas;

class ReactionParse : public Parse, public Message
{
	// mindgame: "sec" refers to "new product".
private:
	ostring sec_e_name;
	ostring sec_i_name;
	ostring sec_n_name; // add MAL 11/15/10
	ostring sec_H_name;
	ostring sec_H_ion_name;
	ostring sec_H2_name;
	ostring sec_C_name;
	ostring sec_C_ion_name;
	ostring sec_Xe_name;
	ostring sec_Xe_ion_name;
	ostring sec_Ar_name;
	ostring sec_Ar4p_name; // add MAL 11/15/10
	ostring sec_Arm_name; // add MAL 11/17/10
	ostring sec_Arr_name; // add MAL 11/17/10
	ostring sec_Ar_ion_name;
	ostring sec_Unit_name; // add MAL191211

	ostring sec_He_name;
  ostring sec_Hep3_name; // add SAS July 2 2014
  ostring sec_Hep1_name; // add SAS July 2 2014
  ostring sec_Hem3_name; // add SAS July 2 2014
  ostring sec_Hem1_name; // add SAS July 2 2014
	ostring sec_He_ion_name;
	ostring sec_CH_name;
	ostring sec_CH_ion_name;
	ostring sec_CH2_name;
	ostring sec_CH2_ion_name;
	ostring sec_CH3_name;
	ostring sec_CH3_ion_name;

	ostring sec_O2_rot_name;
	ostring sec_O2_vib1_name;
	ostring sec_O2_vib2_name;
	ostring sec_O2_vib3_name;
	ostring sec_O2_vib4_name;
	ostring sec_O2_lexc_name;
	ostring sec_O2_sing_delta_name;
	ostring sec_O2_sing_sigma_name;
	ostring  sec_O_name;
	ostring  sec_O_neg_name;
	ostring  sec_O2_name;
	ostring  sec_O2_ion_name;
	ostring  sec_O_ion_name; // added by JTG June 23 2009
	ostring  sec_O_1D_name; // added by JTG July 20 2009
	ostring  sec_O_1S_name; // added by JTG July 20 2009

	ostring sec_Cl_name; //20130604,Huang
	ostring sec_Cl_ion_name;
	ostring sec_Cl2_name;
	ostring sec_Cl2_ion_name;
	ostring sec_Cl_neg_ion_name;

	Species* sec_O2_rot;
	Species* sec_O2_vib1;
	Species* sec_O2_vib2;
	Species* sec_O2_vib3;
	Species* sec_O2_vib4;
	Species* sec_O2_lexc;
	Species* sec_O2_sing_delta;
	Species* sec_O2_sing_sigma;
	Species* sec_O;
	Species* sec_O_neg;
	Species* sec_O2;
	Species* sec_O2_ion;
	Species* sec_O_ion; // added by JTG June 23 2009
	Species* sec_O_1D; // added by JTG July 20 2009
	Species* sec_O_1S; // added by JTG July 20 2009

	Species* sec_Cl;//20130604,Huang
	Species* sec_Cl_neg_ion;
	Species* sec_Cl2;
	Species* sec_Cl2_ion;
	Species* sec_Cl_ion;

	Species* sec_Xe;   //   Added by JTG November 21 2009
	Species* sec_Xe_ion;

	Species* sec_Ar;
	Species* sec_Ar4p; // add MAL 11/15/10
	Species* sec_Arm; // add MAL 11/17/10
	Species* sec_Arr; // add MAL 11/17/10
	Species* sec_Ar_ion;
	Species* sec_Unit; // add MAL191211

	Species* sec_He;
	Species* sec_Hep3; // add SAS July 2 2014
	Species* sec_Hep1; // add SAS July 2 2014
	Species* sec_Hem3; // add SAS July 2 2014
	Species* sec_Hem1; // add SAS July 2 2014
	Species* sec_He_ion;

	Species* sec_e;
	Species* sec_i;
	Species* sec_n; // add MAL 11/15/10
	Species* sec_H;
	Species* sec_H_ion;
	Species* sec_H2;
	Species* sec_C;
	Species* sec_C_ion;
	Species* sec_CH;
	Species* sec_CH_ion;
	Species* sec_CH2;
	Species* sec_CH2_ion;
	Species* sec_CH3;
	Species* sec_CH3_ion;

	SpeciesList *specieslist;
	oopicListIter<Species> species_iter;
	ostring gasname;
	Gas *gas;
	Scalar dt;
	Fields *thefield;

	bool no_default_secondary_species_flag;

	ReactionList parse_reactionlist;
	oopicList<ReactionSubParse> reactionparselist;
	//  oopicList<Reaction> reactionlist;
	ReactionList reactionlist;

	oopicList<ReactionGroup> reactiongrouplist;

public:
	ReactionParse (FILE *fp, oopicList<Parameter<Scalar> > float_variables, oopicList<Parameter<int> > int_variables, SpeciesList *slist, Scalar _dt, Fields *tf);
	~ReactionParse();

	//functions inherited from parse:
	void setdefaults();
	void setparamgroup();
	void check();
	void init();

	inline oopicList<ReactionGroup> * get_reactiongrouplist() { return &reactiongrouplist;}

	Parse *special(ostring s);
};

#endif
