/* 
If a neutral collision type is created it should be added in mcc.cpp in the function is_neutral()
JTG and MAL melded file, 10/12/09
*/

class ostring;
#ifndef GasType
#ifndef GASTYPE_DECLARED
#define GASTYPE_DECLARED
enum GasType
{
  ARGON_GAS, XENON_GAS, HYDROGEN_GAS, HELIUM_GAS, FAKE_GAS, CH4_GAS, CH3_GAS, CH2_GAS,
  CH_GAS, CARBON_GAS, O2_GAS, Cl2_GAS,//20130604,Huang
   NOBELGASXTERMS_GAS
};

#endif
#endif
GasType GasTypeName(ostring name);
ostring GasTypeName(GasType rst);

#ifndef ReactionSpeciesType
#ifndef REACTIONSPECIESTYPE_DECLARED
#define REACTIONSPECIESTYPE_DECLARED
enum ReactionSpeciesType
{
  //
  NO_SPECIES,
  //electrons
  E,
  //ions: for the moment it simply means a cation (of order one: X+)
  //add ions between Ar_ion and Air_ion
  //put O_ion before Air_ion in the list below, MAL 4/13/09
  //in the list of ions (or neutrals), the first ion (neutral) for an ion-ion (neutral-neutral) collision is the projectile
  // and is considered to be "an ion" with velocity ui, and the second is the target and is considered to be "a neutral" with
  // velocity un, for the mcc.cpp handler, MAL 4/17/09
  Ar_ion, Xe_ion, He_ion, H_ion, Ne_ion, CH4_ion,  CH3_ion, CH2_ion, CH_ion, C_ion, O_neg_ion, O_ion, O2_ion, Cl_neg_ion, Cl_ion, Cl2_ion,//20130604,Huang
 Air_ion,
  // neutrals: atoms and methan derivatives.
  //add neutrals between Ar and Air
Ar, Arm, Arr, Ar4p, Xe, He, Hem3, Hem1, Hep3, Hep1, H, H2, Ne, CH4,  CH3,  CH2, CH,  C, O, O2,  // Change order of O and O2, JTG May 26 2009; add Arm, Arr, Ar4p, MAL 11/17/10 (Helium SAS 2014)
  O2_rot, O2_vib1, O2_vib2, O2_vib3, O2_vib4, O2_lexc, O2_sing_delta, O2_sing_sigma, O_1D, O_1S, Cl, Cl2,//20130604,Huang
Unit, Air, // add Unit, MAL 11/16/10  
  // Fake
  Fake_type
}; 
#endif
#endif
ReactionSpeciesType ReactionSpeciesTypeName(ostring name);
ostring ReactionSpeciesTypeName(ReactionSpeciesType rst);

#ifndef ReactionType
#ifndef REACTIONTYPE_DECLARED
#define REACTIONTYPE_DECLARED
// mindgame: the following requires an update.

/*
  For the moment  ReactionType should not only be understood as a type of reaction, but also as a "model".
  For example, we distinguish electron impact elastic reactions, and elastic collisions of species of 
  equivalent masses...even if they both correspond to the same physical process (but we don't model 
  them the same way). It is important to keep in mind that the default reactions included in mcc.cpp 
  have been developped in a special context (they are based on the argon behavior). But it is possible 
  to rewrite a more appropriate model for reactions involving other species (with derivation structure).
  
  If a collision type name starts with E_, it is an Electron impact collision, on a species of much higher mass. 
  (in the collision we will neglect the dynamics of the species of much higher massin comparison with the the 
  eletron dynamics). 
  P_ means proton impact. 
  SAMEMASS means the colliding species have similar masses.
  HALFMAS means the incomming specie has half the mass of the taget specie // JTG May 20 2009
  Ch_EXCHANGE in Charge exchange means the ion associated with a neutral is the impacting species

  - ELASTIC: colliding species are conserved.
  - EXCITATION: colliding species are conserved, a loss of energy is assumed to account for the 
  energy lost in the excitation and radiative desexcitation.
  - CHARGE EXCHANGE: "e"(electron) and "h"(H-) refer to the quantity exchanged in charge EXCHANGE 
  processes.
  - IONIZATION: an electron is taken off the target. The impacting species is conserved. 
  - E_DISS_IONIZATION = dissociative ionization: ex: E + CH4 -> CH3+ H+ 2E
  - E_DISS_EXCITATION1 = 1st dissociative excitation for neutral: form: E + CH4 -> CH3 + H + E
  - E_DISS_EXCITATION2 = 2nd  dissociative excitation for neutral: form: E + CH4 -> CH2 + H2 + E
  - E_DISS_EXCITATION3 = 1st dissociative excitation for ion: form: E + CH4+ -> E + CH3+ + H
  - E_DISS_EXCITATION4 = 2nd dissociative excitation for ion: form: E + CH4+ -> E + CH3 + H+ 
  - E_DISS_EXCITATION5 = 3rd  dissociative excitation for ion: form: E + CH4+ -> E + CH2+ + H2 
  - E_DISS_RECOMBINATION1 (dissociative recombination, channel 1): ex: E + CH4+ -> CH3 + H 
  - E_DISS_RECOMBINATION2 (dissociative recombination, channel 2): ex: E + CH4+ -> CH2 + H2
  - E_DISS_RECOMBINATION3 (dissociative recombination, channel 3): ex: E + CH4+ -> CH + H2 + H
  - FAKE is used to recognize the fake cross sections that can be created as will, in fake.hpp. 
  More explicit names may be found than "DISSEXCITATION1, 2, 3...". Set like this, for the moment
  to ease future introduction of the several dissociation channels.
*/
enum ReactionType {E_ELASTIC, EO_ELASTIC, SAMEMASS_ELASTIC, ANYMASS_ELASTIC, B_ANYMASS_ELASTIC, B_SAMEMASS_ELASTIC, HALFMASS_ELASTIC, P_ELASTIC,
		   E_DISS_EXCITATION1, E_DISS_EXCITATION2, E_DISS_EXCITATION3, E_DISS_EXCITATION4, E_DISS_EXCITATION5, 
		   E_EXCITATION, E_IONIZATION, E_DISS_IONIZATION, E_OIONIZATION,  E_O2IONIZATION,  

//20130604,Huang
E_Cl2IONIZATION, EDISSSingleIzCl2, E_DISS_ATTACHMENT_Cl2, POLARDISSCl2,E_ClIONIZATION, E_DETACHMENT_negCl,NEGCl2_DETCH,NEGCl_DETACHMENT, E_DISS_REC_posCl2,MN_posClnegCl,MN_posCl2negCl,CEposClCl,CEposCl2Cl2,CEposClCl2,CEnegClCl,E_LOSS_Cl2,E_LOSS_Cl,
E_LOSS_Cl2_3Pu,E_LOSS_Cl2_1Pu,E_LOSS_Cl2_3Pg,E_LOSS_Cl2_1Pg,E_LOSS_Cl2_3Su,E_LOSS_Cl2_R1Pu,E_LOSS_Cl2_R1Su,
Cl2Cl2_ELASTIC,ClCl_ELASTIC,El_ClCl2,El_posCl2Cl2,El_posClCl2,El_negClCl2,Cl2FRAG,CEposCl2Cl,EDISSDoubleIzCl2,E_Double_DET_negCl,


		   E_EXCITATION1,  E_EXCITATION2,  E_EXCITATION3,  E_EXCITATION4,  E_LEXCITATION,
		   E_SINGLET_DELTA, E_SINGLET_DELTAdex, E_SINGLET_SIGMAdex, E_SINGLET_SIGMA, E_LOSS1, E_LOSS2, E_LOSS3, E_LOSS4, E_ROTATIONAL, E_DISS_RECOMBINATION, 
		   E_DISS_ATTACHMENT, NEG_ELASTIC, POS_ELASTIC, POSO_ELASTIC, NEG_DETCH, NEUO_ELASTIC,  NEGO_DETACHMENT,
                   MUTUAL_NEUTRALIZATION, E_DETACHMENT, E_DISS_RECOMBINATION1, E_DISS_RECOMBINATION2, E_DISS_RECOMBINATION3,
                   E_EXC1D, E_EXC1Ddex, E_EXC1S, E_EXC3P0, E_EXC5S0, E_EXC3S0, MUTUAL_NEUTRALIZATIONO, O1DTOO2b, O2bO2quench,
		   Ch_EXCHANGE, Ch_EXCHANGEDM, Ch_EXCHANGEO, P_eEXCHANGE, P_e2EXCHANGE, P2_eEXCHANGE, P_hEXCHANGE, POLARDISS, O2FRAG, EDISSIZ, 
		   O2O2_ELASTIC, OO_ELASTIC, PHOTON_EMISSION, PENNING_IONIZATION, AM_PENNING_IONIZATION, SAMEMASS_EXCITATION, NO_TYPE,
                   NONRES_Ch_EXCHANGE};
                   // Added NEUO_ELASTIC May 26 2009 and E_EC1D July 20 2009   P2_eEXCHANGE EO_ELASTIC O2FRAG JTG 
		   // Added O2O2_ELASTIC and OO_ELASTIC, MAL 1/5/10;
                   // Added NONRES_Ch_EXCHANGE, MAL 11/20/11
#endif
#endif
ostring ReactionTypeName(ReactionType reactiontype);
ReactionType ReactionTypeName(ostring name);
bool is_neutral(ReactionSpeciesType rst);
bool is_ion(ReactionSpeciesType rst);
bool is_electron(ReactionSpeciesType rst);
bool is_before(ReactionSpeciesType rstone, ReactionSpeciesType rsttwo); // for ion-ion and neutral-neutral collisions, MAL 4/17/09
bool is_after(ReactionSpeciesType rstone, ReactionSpeciesType rsttwo); //MAL 4/17/09
bool is_thesame(ReactionSpeciesType rstone, ReactionSpeciesType rsttwo); // for like-particle collisions, MAL 11/23/09
class Species;
bool is_neutral(const Species *const rst);
bool is_electron(const Species *const rst);
bool is_ion(const Species *const rst);
bool is_before(const Species *const rstone, const Species *const rsttwo); //MAL 4/17/09
bool is_after(const Species *const rstone, const Species *const rsttwo); //MAL 4/17/09
bool is_thesame(const Species *const rstone, const Species *const rsttwo); //for like-particle collisions, MAL 11/23/09
