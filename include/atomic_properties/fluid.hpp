#ifndef FLUID_H
#define FLUID_H
// #define DEBUG // MAL200130
// Fluid : top-level fluid class
// #include "xgio.h" // add for fluids dump; MAL200228

#include "utils/ovector.hpp"
// forward declarations
class Fields;
class VDistribution;
class Species;

// Fluid -- abstract class for all Fluids:
// -> Boltzmann
class Fluid{
protected:
  Fields *thefield;
  Species *thespecies;

  // phi_small: small value of potential, for
  // top-level electrostatic_derivative (if used)
  Scalar phi_small;

  Scalar *phi_tmp; // temporary storage for calculated potential

  // Grid arrays
  Scalar *n_fluid; // density of fluid
  Scalar *n_fluid_old; // Alan Wu add
  Scalar *n_source;
  Scalar N_tot;  // total # of particles
  Scalar *n_fluid_temp; // for grid time average, MAL200301
  Scalar *n_fluid_ave; // for grid time average, MAL200301
bool nonlinear;

public:
  Fluid(Species * _thespecies);
  Fluid(Fields * _thefield, Species * _thespecies);
  virtual ~Fluid();

  // get new-ed velocity distribution at a grid node
  virtual VDistribution * velocity_distribution(int node) = 0;
  // mindgame: simpler way to get v from distribution
  virtual Vector3 get_v_from_distribution(int node) = 0;

  inline bool is_nonlinear() { return nonlinear; }

  // functions related to density
  virtual Scalar electrostatic_density(Scalar potential) = 0;
  Scalar electrostatic_density_grid(int i);
  Scalar density_grid(int i);

  inline Scalar * get_nfluid() const { return n_fluid; } // MAL200301
  inline Scalar * get_nfluidave() const { return n_fluid_ave; } // MAL200301
  inline Scalar get_n(int i) const { return n_fluid[i]; }
  inline void set_n(Scalar amt, int i) { n_fluid[i] = amt; }
  inline Scalar get_n_old(int i) const {return n_fluid_old[i];}
  inline void set_n_old(Scalar amt, int i) { n_fluid_old[i] = amt; }
  inline Scalar get_n_source(int i) const {return n_source[i];}
  inline Scalar get_Ntot() const { return N_tot; }

  virtual Scalar electrostatic_derivative(Scalar potential);
  virtual void set_thefield(Fields * _thefield);
  virtual void update(Scalar dt);
  virtual void update_N_tot() {};
  virtual void update_fields(Scalar *phi) {};
	virtual Scalar get_T() {return 0.0;} // add for access to T in MCC::setRadiationEscapeFactor, MAL200124
  virtual Species * get_thespecies() {return thespecies;} // add for fluid diffusion, MAL200124
  virtual bool get_evolvefluid() const {return false;} // for mcc access, MAL200208
  virtual int get_subcycle() const {return 100;} // for mcc access, MAL200220
  virtual void add_or_subtract_density(Scalar num_phys_part, Scalar gridx) {}; // for mcc access, MAL200208
  virtual ostring get_fluidname() { return ""; } // MAL200214
  virtual void dump_fluid(FILE * DMPFile) {} // add for fluids dump; MAL200228
  virtual void reload_fluid(FILE * DMPFile) {} // add for fluids reload from dumpfile; MAL200229

  // get_copy: returns a new copy of the fluid
  // with relavent data set
  virtual Fluid * get_copy() = 0;
  inline Scalar total_charge();
  Scalar interact_with_boundaries(); // no longer used, MAL200303
};
#endif
