#ifndef OXYGENGAS_H
#define OXYGENGAS_H
/*
XXX, got from xpdp1 oxygenmcc.c OBSOLETE
JTG file, with extensive revisions of all cross sections and many new reactions, 10/12/09, melded with MAL (no changes)
Order of reactants revised for some reactions, see below; MAL 9/25/12
Revised set of cross sections provided by JTG on 9/20/12 is used
The reaction set and cross sections are discussed in
J. T. Gudmundsson, E. Kawamura and M. A. Lieberman,
A benchmark study of a capacitively coupled oxygen discharge
of the oopd1 particle-in-cell Monte Carlo code, Plasma
Sources Science and Technology 22(3) (2013) 035011
and
J. T. Gudmundsson and M. A. Lieberman,
On the role of metastables in capacitively coupled oxygen discharges,
Plasma Sources Science and Technology, 24(3) (2015) 035016
and
H. Hannesdottir and J. T. Gudmundsson
The role of the metastable O2(b) and energy-dependent secondary electron emission
yields in capacitively coupled oxygen discharges
Plasma Sources Science and Technology, submitted April 206
*/
/*
 NOTE TO DEVELOPERS: // MAL200117
 A ReactionGroup is a set of reactions having the same reactant[0], reactant[1], and mcc type. The priority in reactants and reactions is meaningful.  The first reactant (reactant[0]) in the first reaction in a ReactionGroup determines the mass used to evaluate the energy for the cross section: energy = 0.5*mass(reactant[0])*(relative velocity)^2. Uncomment #define debugReactionGroup in reaction.hpp to print all ReactionGroups to the terminal during initialization.
 */

 #include "atomic_properties/gas.hpp"
 #include "utils/ovector.hpp"
 #include "main/oopiclist.hpp"
 #include "main/species.hpp"

class O2_Gas: public Gas
{
public:
  O2_Gas(oopicListIter<Species> species_iter, Species *sec_e, Species* sec_i,Species *sec_O2_rot,
	 Species *sec_O2_vib1, Species *sec_O2_vib2, Species *sec_O2_vib3, Species *sec_O2_vib4,
	 Species *sec_O2_lexc, Species *sec_O2_sing_delta, Species *sec_O_1D, Species *sec_O_1S,
	 Species *sec_O2_sing_sigma, Species *sec_O, Species *sec_O_neg, Species *sec_O2, Species *sec_O2_ion, Species *sec_O_ion, // added by JTG June 23  2009
	  bool no_default_secondary_species_flag)    {
    mcctype = "oxygen_mcc";
    gastype = O2_GAS;

    vector<Species *> E_species = Species::get_reaction_species_named_with(species_iter, E);
    vector<Species *> O_neg_species = Species::get_reaction_species_named_with(species_iter, O_neg_ion);
    vector<Species *> O2_species = Species::get_reaction_species_named_with(species_iter, O2);
    vector<Species *> O2_ion_species = Species::get_reaction_species_named_with(species_iter, O2_ion);
    vector<Species *> O_species = Species::get_reaction_species_named_with(species_iter, O);  // added by JTG May 26 2009
    vector<Species *> O_1D_species = Species::get_reaction_species_named_with(species_iter, O_1D);  // added by JTG September 1 2009
    vector<Species *> O_1S_species = Species::get_reaction_species_named_with(species_iter, O_1S);  // added by JTG September 1 2009
    vector<Species *> O_ion_species = Species::get_reaction_species_named_with(species_iter, O_ion); // added by JTG June 23  2009
    vector<Species *> O2_sing_delta_species = Species::get_reaction_species_named_with(species_iter, O2_sing_delta); // added by JTG January 4 2013
vector<Species *> O2_sing_sigma_species = Species::get_reaction_species_named_with(species_iter, O2_sing_sigma); // added by JTG February 2015
    if (!no_default_secondary_species_flag)
    {
      if (!sec_e && !E_species.empty()) sec_e = E_species[0];
      if (!sec_O2_rot) sec_O2_rot = Species::get_species_named_with(species_iter, O2_rot);
      if (!sec_O2_vib1) sec_O2_vib1 = Species::get_species_named_with(species_iter, O2_vib1);
      if (!sec_O2_vib2) sec_O2_vib2 = Species::get_species_named_with(species_iter, O2_vib2);
      if (!sec_O2_vib3) sec_O2_vib3 = Species::get_species_named_with(species_iter, O2_vib3);
      if (!sec_O2_vib4) sec_O2_vib4 = Species::get_species_named_with(species_iter, O2_vib4);
      if (!sec_O2_lexc) sec_O2_lexc = Species::get_species_named_with(species_iter, O2_lexc);
      if (!sec_O2_sing_delta) sec_O2_sing_delta = Species::get_species_named_with(species_iter, O2_sing_delta);
      if (!sec_O2_sing_sigma) sec_O2_sing_sigma = Species::get_species_named_with(species_iter, O2_sing_sigma);
      if (!sec_O) sec_O = Species::get_species_named_with(species_iter, O);
      if (!sec_O_neg) sec_O_neg = Species::get_species_named_with(species_iter, O_neg_ion);
      if (!sec_O2) sec_O2 = Species::get_species_named_with(species_iter, O2);
      if (!sec_O2_ion) sec_O2_ion = Species::get_species_named_with(species_iter, O2_ion);
      if (!sec_O_ion) sec_O_ion = Species::get_species_named_with(species_iter, O_ion);
      if (!sec_O_1D) sec_O_1D = Species::get_species_named_with(species_iter, O_1D); // added by JTG July 20 2009
      if (!sec_O_1S) sec_O_1S = Species::get_species_named_with(species_iter, O_1S); // added by JTG July 20 2009




    for (int j=O2_species.size()-1; j>=0; j--)
    {
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaI, 12.06, E_O2IONIZATION);
        reactionlist.add(new Reaction(gastype, E_O2IONIZATION, E_species[i], O2_species[j], xsection, mcctype(), true,  sec_O2_ion, sec_e));

        xsection = add_xsection(sigmaDISSIZ, 18.73, EDISSIZ);    // added by JTG August 18 2009
        reactionlist.add(new Reaction(gastype, EDISSIZ, E_species[i], O2_species[j], xsection, mcctype(), true, sec_O, sec_O_ion, sec_e));

				xsection = add_xsection(sigmaE, 0.0, E_ELASTIC);
        reactionlist.add(new Reaction(gastype, E_ELASTIC, E_species[i], O2_species[j], xsection, mcctype()));

        xsection = add_xsection(sigmaR, 0.02, E_ROTATIONAL);
        reaction = new Reaction(gastype, E_ROTATIONAL, E_species[i], O2_species[j], xsection, mcctype(), true, sec_O2_rot);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaDA, 4.2, E_DISS_ATTACHMENT);
        reaction = new Reaction(gastype, E_DISS_ATTACHMENT, E_species[i], O2_species[j], xsection, mcctype(), true,  sec_O_neg, sec_O);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaE1, 0.19, E_EXCITATION1);
        reaction = new Reaction(gastype, E_EXCITATION1, E_species[i], O2_species[j], xsection, mcctype(), true, sec_O2_vib1);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaE2, 0.38, E_EXCITATION2);
        reaction = new Reaction(gastype, E_EXCITATION2, E_species[i], O2_species[j], xsection, mcctype(), true, sec_O2_vib2);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaE3, 0.57, E_EXCITATION3);
        reaction = new Reaction(gastype, E_EXCITATION3, E_species[i], O2_species[j], xsection, mcctype(), true, sec_O2_vib3);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaE4, 0.75, E_EXCITATION4);
        reaction = new Reaction(gastype, E_EXCITATION4, E_species[i], O2_species[j], xsection, mcctype(), true, sec_O2_vib4);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaLE, 14.7, E_LEXCITATION); // Threshold is 14.7 V but the lost energy is 11.45 V
        reaction = new Reaction(gastype, E_LEXCITATION, E_species[i], O2_species[j], xsection, mcctype(), true, sec_O_1D, sec_O_1D);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaSD, 0.977, E_SINGLET_DELTA);
        reaction = new Reaction(gastype, E_SINGLET_DELTA, E_species[i], O2_species[j], xsection, mcctype(), true, sec_O2_sing_delta);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaSS, 1.627, E_SINGLET_SIGMA); //Energy was 0.977; changed it to 1.627; MAL 4/1/09
        reaction = new Reaction(gastype, E_SINGLET_SIGMA, E_species[i], O2_species[j], xsection, mcctype(), true, sec_O2_sing_sigma);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaL1, 4.05, E_LOSS1);  // Energy changed in accrodance with Y. Itikawa, JPCRD 38 (2009) JTG June 15 2009
//        xsection = add_xsection(sigmaL1, 4.5, E_LOSS1);  // xpdp1 has 4.5 V, not 4.05 V, MAL 8/11/12
        reaction = new Reaction(gastype, E_LOSS1, E_species[i], O2_species[j], xsection, mcctype());
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaL2, 6.12, E_LOSS2);  // Threshold energy is 6.12, in accrodance with Y. Itikawa, JPCRD 38 (2009) JTG June 15 2009
//				 and changed to include dissociation, but the energy lost is 5.09 V
//        xsection = add_xsection(sigmaL2, 6.0, E_LOSS2);  // xpdp1 energy is 6.0 V, MAL 8/11/12
        reaction = new Reaction(gastype, E_LOSS2, E_species[i], O2_species[j], xsection, mcctype(), true,  sec_O, sec_O);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaL3, 8.4, E_LOSS3); //Added dissociation JTG July 2009.  Threshold energy is 8.4 V but the energy lost is 7.13 V
        reaction = new Reaction(gastype, E_LOSS3, E_species[i], O2_species[j], xsection, mcctype(), true,  sec_O, sec_O_1D);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaL4, 9.97, E_LOSS4);  //Added dissociation JTG July 2009.  Threshold energy is 9.97 V but the energy lost is 9.09 V
//        xsection = add_xsection(sigmaL4, 10.0, E_LOSS4);  //xpdp1 has 10.0 V, MAL 8/11/12
        reaction = new Reaction(gastype, E_LOSS4, E_species[i], O2_species[j], xsection, mcctype(), true, sec_O_1D, sec_O_1D);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaPD, 15.0, POLARDISS); // added by JTG July 28 2009
        reaction = new Reaction(gastype, POLARDISS, E_species[i], O2_species[j], xsection, mcctype(), true, sec_O_neg, sec_O_ion, sec_e);
				reactionlist.add(reaction);
      }
   for (int i=O2_species.size()-1; i>=0; i--) // add for loop for like-particle collisions, MAL 1/5/10
      {
        xsection = add_xsection(sigmaO2O2, 0.0, O2O2_ELASTIC);
        reactionlist.add(new Reaction(gastype, O2O2_ELASTIC, O2_species[i], O2_species[j], xsection, mcctype()));
      }
      for (int i=O_1D_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaO1DO2,0.0, O1DTOO2b);
        reactionlist.add (new Reaction(gastype, O1DTOO2b, O2_species[j], O_1D_species[j], xsection, mcctype(), true, sec_O, sec_O2_sing_sigma));
      }
      for (int i=O2_sing_sigma_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaO2bO2quench,0.0, O2bO2quench);
        reactionlist.add (new Reaction(gastype, O2bO2quench, O2_species[j], O2_sing_sigma_species[j], xsection, mcctype(), true, sec_O2, sec_O2));
      }
    }

for (int j=O2_sing_delta_species.size()-1; j>=0; j--)
    {
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaIdelta, 11.80, E_O2IONIZATION);
        reactionlist.add(new Reaction(gastype, E_O2IONIZATION, E_species[i], O2_sing_delta_species[j], xsection, mcctype(), true,  sec_O2_ion, sec_e));

        xsection = add_xsection(sigmaDISSIZdelta, 17.753, EDISSIZ);    // added by JTG January 7 2013
        reactionlist.add(new Reaction(gastype, EDISSIZ, E_species[i], O2_sing_delta_species[j], xsection, mcctype(), true, sec_O, sec_O_ion, sec_e));

        xsection = add_xsection(sigmaL1delta, 3.073, E_LOSS1);  // Energy changed in accrodance with Y. Itikawa, JPCRD 38 (2009) JTG June 15 2009
//        xsection = add_xsection(sigmaL1, 4.5, E_LOSS1);  // xpdp1 has 4.5 V, not 4.05 V, MAL 8/11/12
        reaction = new Reaction(gastype, E_LOSS1, E_species[i], O2_sing_delta_species[j], xsection, mcctype());
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaL2delta, 5.143, E_LOSS2);  // Threshold energy is 6.12, in accrodance with Y. Itikawa, JPCRD 38 (2009) JTG June 15 2009
//				 and changed to include dissociation, but the energy lost is 5.09 V
//        xsection = add_xsection(sigmaL2, 6.0, E_LOSS2);  // xpdp1 energy is 6.0 V, MAL 8/11/12
        reaction = new Reaction(gastype, E_LOSS2, E_species[i], O2_sing_delta_species[j], xsection, mcctype(), true,  sec_O, sec_O);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaL3delta, 7.423, E_LOSS3); //Added dissociation JTG July 2009.  Threshold energy is 8.4 V but the energy lost is 7.13 V
        reaction = new Reaction(gastype, E_LOSS3, E_species[i], O2_sing_delta_species[j], xsection, mcctype(), true,  sec_O, sec_O_1D);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaL4delta, 8.993, E_LOSS4);  //Added dissociation JTG July 2009.  Threshold energy is 9.97 V but the energy lost is 9.09 V
        reaction = new Reaction(gastype, E_LOSS4, E_species[i], O2_sing_delta_species[j], xsection, mcctype(), true, sec_O_1D, sec_O_1D);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaDAdelta, 3.223, E_DISS_ATTACHMENT);
        reaction = new Reaction(gastype, E_DISS_ATTACHMENT, E_species[i], O2_sing_delta_species[j], xsection, mcctype(), true,  sec_O_neg, sec_O);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaSDdex, 0.0, E_SINGLET_DELTAdex);
        reaction = new Reaction(gastype, E_SINGLET_DELTAdex, E_species[i], O2_sing_delta_species[j], xsection, mcctype(), true, sec_O2);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaSSdelta, 0.65, E_SINGLET_SIGMA); // Threshold reduced JTG February 4 2015
        reaction = new Reaction(gastype, E_SINGLET_SIGMA, E_species[i], O2_sing_delta_species[j], xsection, mcctype(), true, sec_O2_sing_sigma);
        reactionlist.add(reaction);
}
for (int i=O2_species.size()-1; i>=0; i--) // added for singlet delta April 7 2014 JTG
      {
        xsection = add_xsection(sigmaO2O2, 0.0, O2O2_ELASTIC);
        reactionlist.add(new Reaction(gastype, O2O2_ELASTIC, O2_species[i], O2_sing_delta_species[j], xsection, mcctype()));
}
    }

for (int j=O2_sing_sigma_species.size()-1; j>=0; j--)  // added for singlet sigma February 4 2015 JTG
    {
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaIsigma, 10.433, E_O2IONIZATION);
        reactionlist.add(new Reaction(gastype, E_O2IONIZATION, E_species[i], O2_sing_sigma_species[j], xsection, mcctype(), true,  sec_O2_ion, sec_e));

   xsection = add_xsection(sigmaDISSIZsigma, 17.103, EDISSIZ);    // added by JTG February 4 2015
        reactionlist.add(new Reaction(gastype, EDISSIZ, E_species[i], O2_sing_sigma_species[j], xsection, mcctype(), true, sec_O, sec_O_ion, sec_e));

  xsection = add_xsection(sigmaL1sigma, 2.423, E_LOSS1);
  reaction = new Reaction(gastype, E_LOSS1, E_species[i], O2_sing_sigma_species[j], xsection, mcctype(), true, sec_O2);
   reactionlist.add(reaction);

        xsection = add_xsection(sigmaL2sigma, 4.493, E_LOSS2);
        reaction = new Reaction(gastype, E_LOSS2, E_species[i], O2_sing_sigma_species[j], xsection, mcctype(), true,  sec_O, sec_O);
        reactionlist.add(reaction);

    xsection = add_xsection(sigmaL3sigma, 6.773, E_LOSS3); // Threshold energy is 8.4 V but the energy lost is 7.13 V
        reaction = new Reaction(gastype, E_LOSS3, E_species[i], O2_sing_sigma_species[j], xsection, mcctype(), true,  sec_O, sec_O_1D);
        reactionlist.add(reaction); // Changed O2_sing_delta_species to O2_sing_sigma_species, HH 05/02/16

   xsection = add_xsection(sigmaL4sigma, 8.343, E_LOSS4);  //Added  JTG February 4 2015.  Threshold energy is 9.97 V but threshold reduced by 1.627 eV  the energy lost is 9.09 V
        reaction = new Reaction(gastype, E_LOSS4, E_species[i], O2_sing_sigma_species[j], xsection, mcctype(), true, sec_O_1D, sec_O_1D);
        reactionlist.add(reaction);


   xsection = add_xsection(sigmaDAsigma, 2.573, E_DISS_ATTACHMENT);
        reaction = new Reaction(gastype, E_DISS_ATTACHMENT, E_species[i], O2_sing_sigma_species[j], xsection, mcctype(), true,  sec_O_neg, sec_O);
        reactionlist.add(reaction);

  xsection = add_xsection(sigmaSSdex, 0.0, E_SINGLET_SIGMAdex); // Added by HH 02/08/16
  reaction = new Reaction(gastype, E_SINGLET_SIGMAdex, E_species[i], O2_sing_sigma_species[j], xsection, mcctype(), true, sec_O2);
  reactionlist.add(reaction);


  }
for (int i=O2_species.size()-1; i>=0; i--)
   {

   // Deleted, HH 05/02/16
   // xsection = add_xsection(sigmaO2bO2X,0.0,  SAMEMASS_EXCITATION);
   // reaction =new Reaction(gastype, SAMEMASS_EXCITATION, O2_sing_sigma_species[j], O2_species[i], xsection, mcctype(), true, sec_O2);

   xsection = add_xsection(sigmaO2O2, 0.0, O2O2_ELASTIC); // Changed to match scattering of O2(a), HH 05/02/16
   reactionlist.add(new Reaction(gastype, O2O2_ELASTIC, O2_species[i], O2_sing_sigma_species[j], xsection, mcctype()));
}
    }

    for (int j=O2_ion_species.size()-1; j>=0; j--)
    {
      for (int i=O_neg_species.size()-1; i>=0; i--)
	{
	  xsection = add_xsection(sigmaMN, 0.0, MUTUAL_NEUTRALIZATION);
	  reaction = new Reaction(gastype, MUTUAL_NEUTRALIZATION, O_neg_species[i], O2_ion_species[j], xsection, mcctype(), true, sec_O2, sec_O);
	  reactionlist.add(reaction);
	}

      for (int i=O2_species.size()-1; i>=0; i--)
	{
	  xsection = add_xsection(sigmaCE, 0.0, Ch_EXCHANGE);
//	  reaction = new Reaction(gastype, Ch_EXCHANGE, O2_species[i], O2_ion_species[j], xsection, mcctype());
		reaction = new Reaction(gastype, Ch_EXCHANGE, O2_ion_species[j], O2_species[i], xsection, mcctype()); //change order of reactants, MAL 8/29/12
  reactionlist.add(reaction);

          xsection = add_xsection(sigmaFRAG, 13.8, O2FRAG);  // The CM threshold is assumed to be 6.9 V, so need 13.8 V in target rest frame, MAL 10/2/12
//          xsection = add_xsection(sigmaFRAG, 6.9, O2FRAG);  // Added by JTG August 17 2009 The threshold is assumed to be 22 V
//  but the loss due to O+ + O creation from O2+ is 6.9 V; ie, 6.9 V is the dissociation energy, MAL 10/2/12
	  reaction = new Reaction(gastype, O2FRAG, O2_ion_species[j], O2_species[i], xsection, mcctype(), true, sec_O2, sec_O, sec_O_ion); //change order of reactants, MAL 9/2/12
	  reactionlist.add(reaction);

          xsection = add_xsection(sigmapoE, 0.0, POS_ELASTIC);
	  reaction = new Reaction(gastype, POS_ELASTIC, O2_ion_species[j], O2_species[i], xsection, mcctype()); //change order of reactants, MAL 9/2/12
	  reactionlist.add(reaction);
	}

      for (int i=O_species.size()-1; i>=0; i--)
	{
//          xsection = add_xsection(sigmaCEO2O, 1.56, P2_eEXCHANGE);
          xsection = add_xsection(sigmaCEO2O, 4.68, P2_eEXCHANGE); // Threshold energy = 3*1.56 eV in the target rest frame, MAL 10/12/12, 12/9/12
	 reaction = new Reaction(gastype, P2_eEXCHANGE, O2_ion_species[j], O_species[i], xsection, mcctype(),true, sec_O2, sec_O_ion); //change order of reactants, MAL 9/2/12
       	  reactionlist.add(reaction);
					reaction -> add_characteristic_value(-1.56); // energy "released" in CM system; the reaction absorbs 1.56 eV in CM, MAL 10/12/12, 12/9/12
	}

      for (int i=E_species.size()-1; i>=0; i--)
	{
//	  xsection = add_xsection(sigmaDR, 4.87, E_DISS_RECOMBINATION);
	  xsection = add_xsection(sigmaDR, 0.0, E_DISS_RECOMBINATION); // threshold energy = 0 for this reaction, released energy = 4.87, MAL 10/2/12
	  reaction = new Reaction(gastype, E_DISS_RECOMBINATION, E_species[i], O2_ion_species[j], xsection, mcctype(), true,  sec_O, sec_O_1D);
	  reactionlist.add(reaction);
	}

     for (int i=O2_sing_delta_species.size()-1; i>=0; i--)  // added May 16 2014 changed to P_eEXCHANGE October 15 2014
     {
       //         xsection = add_xsection(sigmaCE, 0.0, Ch_EXCHANGE);
       //         reaction = new Reaction(gastype, Ch_EXCHANGE, O2_ion_species[j], O2_sing_delta_species[i], xsection, mcctype()); //change order of reactants, MAL 8/29/12
       //        reactionlist.add(reaction);
	xsection = add_xsection(sigmaCE, 0.0, P_e2EXCHANGE);
	 reaction = new Reaction(gastype, P_e2EXCHANGE, O2_ion_species[j], O2_sing_delta_species[i], xsection, mcctype(),true, sec_O2_ion, sec_O2); //change order of reactants, MAL 9/7/12
       	 reactionlist.add(reaction);
	 reaction -> add_characteristic_value(0.98); // energy released in CM system, MAL 10/12/12, 12/9/12
}
for (int i=O2_sing_sigma_species.size()-1; i>=0; i--)  // added JTG February 13 2015
     {
	xsection = add_xsection(sigmaCE, 0.0, P_e2EXCHANGE);
	 reaction = new Reaction(gastype, P_e2EXCHANGE, O2_ion_species[j], O2_sing_sigma_species[i], xsection, mcctype(),true, sec_O2_ion, sec_O2);
       	 reactionlist.add(reaction);
	 reaction -> add_characteristic_value(1.627); // energy released in CM system
}

    }
    for (int j=O_neg_species.size()-1; j>=0; j--)
      {
	for (int i=O2_species.size()-1; i>=0; i--)
	  {
	  xsection = add_xsection(sigmaneD, 2.195, NEG_DETCH); // 1.463 (in CM system) -> (3/2)*1.463=2.195 in target rest frame, MAL 10/4/12
		reaction = new Reaction(gastype, NEG_DETCH, O_neg_species[j], O2_species[i], xsection, mcctype(), true, sec_O2, sec_O, sec_e);
		reactionlist.add(reaction);

        xsection = add_xsection(sigmaneE, 0.0, NEG_ELASTIC);
        reactionlist.add(new Reaction(gastype, NEG_ELASTIC, O_neg_species[j], O2_species[i], xsection, mcctype()));
        	  }
      }

for (int j=O_neg_species.size()-1; j>=0; j--)
      {
	for (int i=O2_sing_delta_species.size()-1; i>=0; i--)
	  {
	  xsection = add_xsection(sigmaneDdelta, 0.0, NEG_DETCH); // added JTG January 2013
		reaction = new Reaction(gastype, NEG_DETCH, O_neg_species[j], O2_sing_delta_species[i], xsection, mcctype(), true, sec_O2, sec_O, sec_e);
		reactionlist.add(reaction);
        	  }
         for (int i=O2_sing_sigma_species.size()-1; i>=0; i--)
	  {
	  xsection = add_xsection(sigmaneDsigma, 0.0, NEG_DETCH); // added JTG February 10 2015
		reaction = new Reaction(gastype, NEG_DETCH, O_neg_species[j], O2_sing_sigma_species[i], xsection, mcctype(), true, sec_O2, sec_O, sec_e);
		reactionlist.add(reaction);
        	  }
      }


       for (int j=O_species.size()-1; j>=0; j--)
       {
         for (int i=E_species.size()-1; i>=0; i--)
         {
        xsection = add_xsection(sigmaIO, 13.62, E_OIONIZATION);
        reactionlist.add(new Reaction(gastype, E_OIONIZATION, E_species[i], O_species[j], xsection, mcctype(), true,  sec_O_ion, sec_e));

      	xsection = add_xsection(sigmaEO, 0.0, EO_ELASTIC);
				reactionlist.add(new Reaction(gastype, EO_ELASTIC, E_species[i], O_species[j], xsection, mcctype(), true, sec_e, sec_O));

        xsection = add_xsection(sigmaE1D, 1.96, E_EXC1D);
        reaction = new Reaction(gastype, E_EXC1D, E_species[i], O_species[j], xsection, mcctype(), true, sec_O_1D);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaE1S, 4.18, E_EXC1S);
        reaction = new Reaction(gastype, E_EXC1S, E_species[i], O_species[j], xsection, mcctype(), true, sec_O_1S);
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaE3P0, 15.65, E_EXC3P0);  // Added July 30 2009 JTG
        reaction = new Reaction(gastype, E_EXC3P0, E_species[i], O_species[j], xsection, mcctype());
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaE5S0, 9.14, E_EXC5S0);  // Added July 30 2009 JTG
        reaction = new Reaction(gastype, E_EXC5S0, E_species[i], O_species[j], xsection, mcctype());
        reactionlist.add(reaction);

        xsection = add_xsection(sigmaE3S0, 9.51, E_EXC3S0);  // Added July 30 2009 JTG
        reaction = new Reaction(gastype, E_EXC3S0, E_species[i], O_species[j], xsection, mcctype());
        reactionlist.add(reaction);
        }
	for (int i=O_neg_species.size()-1; i>=0; i--) // Disabled this reaction to avoid segmentation fault, MAL 10/14/09; works now, 10/2/12
				{
        xsection = add_xsection(sigmanegOD, 0.0, NEGO_DETACHMENT);
	reaction = new Reaction(gastype, NEGO_DETACHMENT, O_species[j], O_neg_species[i], xsection, mcctype(), true,  sec_O2, sec_e);
	reactionlist.add(reaction);
				}
       for (int i=O2_species.size()-1; i>=0; i--)
				{
        xsection = add_xsection(sigmaneuE, 0.0, NEUO_ELASTIC);
        reactionlist.add(new Reaction(gastype, NEUO_ELASTIC, O_species[j], O2_species[i], xsection, mcctype()));
				}
       for (int i=O_species.size()-1; i>=0; i--) // add for loop for like-particle collisions, MAL 1/5/10, O2-species -> O->species, 12/9/12, MAL
       {
			 xsection = add_xsection(sigmaOO, 0.0, OO_ELASTIC);
			 reactionlist.add(new Reaction(gastype, OO_ELASTIC, O_species[i], O_species[j], xsection, mcctype()));
       }
  }


for (int j=O_1D_species.size()-1; j>=0; j--)
       {
         for (int i=E_species.size()-1; i>=0; i--)
         {
        xsection = add_xsection(sigmaIOD, 11.66, E_OIONIZATION);
        reactionlist.add(new Reaction(gastype, E_OIONIZATION, E_species[i], O_1D_species[j], xsection, mcctype(), true,  sec_O_ion, sec_e));

        xsection = add_xsection(sigmaE1Ddex, 0.0, E_EXC1Ddex);
        reaction = new Reaction(gastype, E_EXC1Ddex, E_species[i], O_1D_species[j], xsection, mcctype(), true, sec_O);
        reactionlist.add(reaction);
}
	for (int i=O2_species.size()-1; i>=0; i--)  // added April 11 2014  JTG
				{
        xsection = add_xsection(sigmaneuE, 0.0, NEUO_ELASTIC);
        reactionlist.add(new Reaction(gastype, NEUO_ELASTIC, O_1D_species[j], O2_species[i], xsection, mcctype()));
				}
       }

   for (int j=O_ion_species.size()-1; j>=0; j--)
          {
        for (int i=O_species.size()-1; i>=0; i--)
       	{
         xsection = add_xsection(sigmaCEO, 0.0, Ch_EXCHANGEO);
       	  reaction = new Reaction(gastype, Ch_EXCHANGEO, O_ion_species[j], O_species[i], xsection, mcctype()); //change order of reactants, MAL 9/7/12
       	  reactionlist.add(reaction);
       	}
        for (int i=O2_species.size()-1; i>=0; i--)
       	{
         xsection = add_xsection(sigmaCEOO, 0.0, P_eEXCHANGE);
	 reaction = new Reaction(gastype, P_eEXCHANGE, O_ion_species[j], O2_species[i], xsection, mcctype(),true, sec_O2_ion, sec_O); //change order of reactants, MAL 9/7/12
       	 reactionlist.add(reaction);
	 reaction -> add_characteristic_value(1.56); // energy released in CM system, MAL 10/12/12, 12/9/12

        xsection = add_xsection(sigmapooE, 0.0, POSO_ELASTIC);
	  reaction = new Reaction(gastype, POSO_ELASTIC, O_ion_species[j], O2_species[i], xsection, mcctype()); //change order of reactants, MAL 9/7/12
	  reactionlist.add(reaction);
       	}

        for (int i=O2_sing_delta_species.size()-1; i>=0; i--) // added by JTG May 16 2014
       	{
         xsection = add_xsection(sigmaCEOO, 0.0, P_eEXCHANGE);
	 reaction = new Reaction(gastype, P_eEXCHANGE, O_ion_species[j], O2_sing_delta_species[i], xsection, mcctype(),true, sec_O2_ion, sec_O); //change order of reactants, MAL 9/7/12
       	 reactionlist.add(reaction);
	 reaction -> add_characteristic_value(2.54); // energy released in CM system, MAL 10/12/12, 12/9/12
       	}

        for (int i=O2_sing_sigma_species.size()-1; i>=0; i--) // added by JTG February 13 2015
       	{
         xsection = add_xsection(sigmaCEOO, 0.0, P_eEXCHANGE);
	 reaction = new Reaction(gastype, P_eEXCHANGE, O_ion_species[j], O2_sing_sigma_species[i], xsection, mcctype(),true, sec_O2_ion, sec_O);
       	 reactionlist.add(reaction);
	 reaction -> add_characteristic_value(2.83); // energy released in CM system, MAL 10/12/12, 12/9/12
       	}

       for (int i=O_neg_species.size()-1; i>=0; i--)
	{
	  xsection = add_xsection(sigmaMNO, 0.0, MUTUAL_NEUTRALIZATIONO);
	  reaction = new Reaction(gastype, MUTUAL_NEUTRALIZATIONO, O_neg_species[i], O_ion_species[j], xsection, mcctype(), true, sec_O, sec_O);
	  reactionlist.add(reaction);
	}
        }
      for (int j=O_neg_species.size()-1; j>=0; j--)
      {

        for (int i=E_species.size()-1; i>=0; i--)
          {
            xsection = add_xsection(sigmaeD, 1.463, E_DETACHMENT);
            reaction = new Reaction(gastype, E_DETACHMENT, E_species[i], O_neg_species[j], xsection, mcctype(), true, sec_O, sec_e);
            reactionlist.add(reaction);
          }
      }

    }
  }



  // Cross sections:
  //-------------------
  static Scalar sigmaI(Scalar energy)
  //  e + O2 --> O2+ + 2e
  // Revised by a new fit to the data measured by E. Krishnakumar and S. K. Srivastava
  //Int. J. Mass Spectrometry and Ion Processes vol. 113, pp. 1-12 (1992)
  //  JTG april 7 2009
 {
    float temp5;


    temp5= 0.0;
    if(12.06 <= energy && energy <= 180.)
      {
      temp5 = (pow(10.0,(-2.5050 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 21.1338 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))  - 52.8155 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) - 37.2837 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 434.1281  * (log10(energy)) * (log10(energy)) * (log10(energy)) - 874.6800  * (log10(energy)) * (log10(energy)) + 772.1372  * (log10(energy)) - 283.5866)));
      }
      else if(180 < energy)
	{
	  temp5 = (pow(10.0,(-0.2981 * (log10(energy)) * (log10(energy)) + 1.0264 * (log10(energy))  - 20.5514)));
	}
      else if(energy < 12.06)
	{
	  temp5 = 0;
        }
    return(temp5);
 }

static Scalar sigmaDISSIZ(Scalar energy)
  //  e + O2 --> O+ + O + 2e
  // A  fit to the data measured by E. Krishnakumar and S. K. Srivastava
  // Int. J. Mass Spectrometry and Ion Processes vol. 113, pp. 1-12 (1992)
  //  JTG August 18 2009
  {
      float temp5;

      temp5 =0;
      if(18.73 <= energy && energy <= 150)
	{
	  temp5 = (pow(10.0,(5.1188 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) - 53.1537 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))  + 220.3817 * (log10(energy)) * (log10(energy)) * (log10(energy)) -457.3868 * (log10(energy)) * (log10(energy)) + 476.8381  * (log10(energy))  - 220.5401)));
	}
      else if(150 < energy)
	{
	 temp5 = (pow(10.0,(-0.9987 * (log10(energy)) * (log10(energy)) + 4.3037 * (log10(energy))  - 24.6189)));
	}
      else if(energy < 18.73)
	{
	  temp5 = 0;
        }
    return(temp5);
    }

  static Scalar sigmaDA(Scalar energy)
  // e + O2 --> O + O-
  // Revised by a new fit to the data recommended in the  review by
  // Y. Itikawa, JPCRD 38 (2009) 1 and is from Rapp and Briglia JPC 43(1965) 1480
//  JTG april 9 2009
{
     float temp5;
    temp5 =0;
 if(4.2 <= energy && energy <= 10)
	{
	temp5 = (pow(10.0,(5.18296e3 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))  - 2.091608e4 * (log10(energy)) * (log10(energy))* (log10(energy))* (log10(energy))  + 3.361400e4 * (log10(energy))* (log10(energy)) * (log10(energy)) - 2.694881e4 * (log10(energy)) * (log10(energy))  +1.080299e4* (log10(energy))-1.758524e3)));
	}
      else
	{
	  temp5 = 0;
	}
    return(temp5);
    }

 static Scalar sigmaE(Scalar energy)
  // e + O2 --> O2 + e
  // Revised by a new fit to the data recommended in the  review by
  // Y. Itikawa, JPCRD 38 (2009) 1
//  JTG april 8 2009
  {
      float temp5;

      temp5 =0;
      if(0.0 <= energy && energy <= 5.13)
	{
	temp5 = (pow(10.0,(-0.001478 * (log10(energy)) * (log10(energy)) * (log10(energy))   - 0.0354 * (log10(energy)) * (log10(energy))  + 0.1556 * (log10(energy))   - 19.2254)));
	}
      else
	{
	 temp5 = (pow(10.0,(0.0933 * (log10(energy)) * (log10(energy)) * (log10(energy))   - 0.7463 * (log10(energy)) * (log10(energy))  + 1.3218 * (log10(energy))   - 19.7293)));
	}
    return(temp5);
    }
static Scalar sigmaR(Scalar energy)
    {
      float temp1, temp2, temp3;
      float temp4, temp5, temp6;

      temp6= 0.0;
      if(.07 <= energy && energy <= 1.67)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;
	  temp3 = energy*temp2;
	  temp4 = energy*temp3;
	  temp5 = energy*temp4;

	  temp6 = -.0859*temp5 +.4233*temp4 -.7366*temp3 +.5205*temp2 -.1537*temp1 +.0604*energy -.0022;
	  temp6 *= 1e-20;
	}
      return(temp6);
    }
  static Scalar sigmaE1(Scalar energy) // This was mistakenly the 8.4eV en loss; changed to vibrational v=1, MAL 4/1/09
	{
		float temp1, temp2, temp3;
		float temp4, temp5, temp6;

		temp6= 0.0;
		if(.19 <= energy && energy <= 1.0) {
			temp1 = energy*energy;
			temp2 = energy*temp1;
			temp3 = energy*temp2;
			temp4 = energy*temp3;

			temp6 = -1.508395*temp4 +6.521786*temp3 -9.574636*temp2 +5.092031*temp1 -0.41602*energy -0.066398;
			temp6 *= 1e-20;
			if(temp6 <= 0.0) temp6=0.0;
		}
		else if(1.0 < energy && energy <= 1.67) temp6 = -7.2193e-22*(energy -1.67);
		else if(4.0 <= energy && energy <= 5.5) temp6 = 4.572852e-22*(energy -4.0);
		else if(5.5 < energy && energy <= 16.0)
		{
			temp1 = energy*energy;
			temp2 = energy*temp1;
			temp3 = energy*temp2;
			temp4 = energy*temp3;
			temp5 = energy*temp4;

			temp6 = 1.0835e-6*temp5 -9.229e-5*temp4 +0.0030853*temp3 -0.050981*temp2 +0.427934*temp1 -1.6682*energy +2.3919;
			temp6 *= 1e-20;
			if(temp6 <= 0.0) temp6=0.0;
		}
		else if(16.0 < energy && energy <= 25.0) temp6 = -4.098144e-23*(energy -25.0);

		return(temp6);
	}
	static Scalar sigmaE2(Scalar energy)
    {
      float temp1, temp2, temp3;
      float temp4, temp5;

      temp5= 0.0;
      if(.38 <= energy && energy <= 1.67)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;
	  temp3 = energy*temp2;

	  temp5 = -0.606022*temp3 +3.157773*temp2 -5.933895*temp1 +4.664064*energy -1.233443;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(4.0 <= energy && energy <= 14.5)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;
	  temp3 = energy*temp2;
	  temp4 = energy*temp3;

	  temp5 = -3.1339358e-6*temp4 +2.0994236e-4*temp3 -.00503577*temp2 +0.0515*temp1 -0.2074798*energy +0.279;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(14.5 < energy && energy <= 25.0) temp5 = -1.71326e-23*(energy -25.0);

      return(temp5);
    }
  static Scalar sigmaE3(Scalar energy)
    {
      float temp1, temp2, temp3;
      float temp4, temp5;

      temp5= 0.0;
      if(.57 <= energy && energy <= 1.67)
	{
	  temp1 = energy*energy;

	  temp5 = -.055083*temp1 +.12457*energy -.057531;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(4.0 <= energy && energy <= 15.0)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;
	  temp3 = energy*temp2;
	  temp4 = energy*temp3;

	  temp5 = -7.969385e-6*temp4 +4.78119632e-4*temp3 -.0107124*temp2 +0.1095564*temp1 -0.4962553*energy +0.80444;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(15.0 < energy && energy <= 20.0) temp5 = -1.76e-23*(energy -20.0);

      return(temp5);

    }
  static Scalar sigmaE4(Scalar energy)
    {
      float temp1, temp2, temp3, temp6;

      temp6= 0.0;
      if(.75 <= energy && energy <= 0.85) temp6 = 2.795e-25*(energy - 0.75);

      else if(.85 <= energy && energy <= 1.67)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;

	  temp6 = -0.049346*temp2 +0.16616*temp1 -0.174061*energy +0.058213;
	  temp6 *= 1e-20;
	  if(temp6 <= 0.0) temp6=0.0;
	}
      else if(6.0 <= energy && energy <= 15.0)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;
	  temp3 = energy*temp2;

	  temp6 = -1.3846154e-5*temp3 +8.8449e-4*temp2 -0.020271*temp1 +0.19111*energy -0.589505;
	  temp6 *= 1e-20;
	  if(temp6 <= 0.0) temp6=0.0;
	}
      return(temp6);

    }
  static Scalar sigmaLE(Scalar energy)
    {
      float temp1, temp2;
      float temp5;

      temp5= 0.0;
      if(14.7<= energy && energy <= 100.)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;

	  temp5 = 1.21665e-7*temp2 -3.0483e-5*temp1 +2.51713e-3*energy -.030335;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(100. < energy) temp5= 3.8e-22;

      return(temp5);

    }
  static Scalar sigmaSD(Scalar energy)
  {
    float temp1, temp2, temp3;
    float temp4, temp5;

    temp5= 0.0;
    if(0.977 <= energy && energy <= 10.5)
      {
	temp1 = energy*energy;
	temp2 = energy*temp1;
	temp3 = energy*temp2;
	temp4 = energy*temp3;

	temp5 = -3.0913e-6*temp4 +1.436e-4*temp3 -.0022876*temp2 +.0133286*temp1 -.0100266*energy -.0015636;
	temp5 *= 1e-20;
	if(temp5 <= 0.0) temp5=0.0;
      }
    else if(10.5 < energy && energy <= 45.0)
      {
	temp1 = energy*energy;
	temp2 = energy*temp1;

	temp5 = -1.0959e-6*temp2 +1.349e-4*temp1 -.005984*energy +.1079;
	temp5 *= 1e-20;
      }
    else if(45 < energy && energy <= 100.0)
      {
	temp5 = -2.18e-4*energy +2.18e-2;
	temp5 *= 1e-20;
      }
    return(temp5);

  }
  static Scalar sigmaSS(Scalar energy)
    {
      float temp1, temp2, temp3;
      float temp4, temp5;

      temp5= 0.0;
      if(1.627 <= energy && energy <= 25.0)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;
	  temp3 = energy*temp2;
	  temp4 = energy*temp3;

	  temp5 = 5.34245e-8*temp4 -4.7117e-6*temp3 +1.581e-4*temp2 -2.4783e-3*temp1 +1.70373e-2*energy -2.2343e-2;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(25.0 < energy && energy <= 45.0)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;

	  temp5 = 5.445e-7*temp2 -5.674e-5*temp1 +1.7502e-3*energy -1.0375e-2;
	  temp5 *= 1e-20;
	}
      else if(45 < energy && energy <= 100.0)
	{
	  temp5 = -5.64e-5*energy +5.64e-3;
	  temp5 *= 1e-20;
	}
      return(temp5);
    }

  static Scalar sigmaSSdex(Scalar energy) // Added by HH 02/08/16
    {
      float temp1, temp2, temp3;
      float temp4, temp5;

      energy = energy + 1.627;

      temp5= 0.0;
      if(1.627 <= energy && energy <= 25.0)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;
	  temp3 = energy*temp2;
	  temp4 = energy*temp3;

	  temp5 = 5.34245e-8*temp4 -4.7117e-6*temp3 +1.581e-4*temp2 -2.4783e-3*temp1 +1.70373e-2*energy -2.2343e-2;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(25.0 < energy && energy <= 45.0)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;

	  temp5 = 5.445e-7*temp2 -5.674e-5*temp1 +1.7502e-3*energy -1.0375e-2;
	  temp5 *= 1e-20;
	}
      else if(45 < energy && energy <= 100.0)
	{
	  temp5 = -5.64e-5*energy +5.64e-3;
	  temp5 *= 1e-20;
	}
      return(temp5*3*(1+1.627/(energy-1.627)));
    }

  static Scalar sigmaL1(Scalar energy)
    {
      //  Revised using a fit the cross sections measured by Shyn and Sweeney, PRA, 62 (2000) 022711
      //  and Green et al JPB 34 (2001) L157 taken from Y. Itikawa, JPCRD 38 (2009) 1
      //  JTG July 1 2009
      //
      float temp5;

      temp5= 0.0;
      if(4.05 < energy && energy <= 10)
	{
	  temp5 = 1.1933e-23  *energy * energy * energy - 2.4345e-22 * energy* energy + 1.7726e-21* energy - 3.9778e-21;
	}
      else if(10 < energy && energy < 40)
	{
          temp5 = (pow(10.0,(-16.7249 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 79.2236 * (log10(energy))* (log10(energy))* (log10(energy)) - 143.5578 * (log10(energy))* (log10(energy)) +116.2163* (log10(energy)) -56.0342 )));
	}
      return(temp5);
    }

  static Scalar sigmaL2(Scalar energy)
    {
      float temp1, temp2, temp3;
      float temp5;

      temp5= 0.0;
      if(6.0<= energy && energy <= 20.)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;
	  temp3 = energy*temp2;

	  temp5 = -1.1894e-4*temp3 +6.8655e-3*temp2 -.143425*temp1 +1.26276*energy -3.7338513;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
  }
      else if(20. < energy && energy <= 100.)
	{
	  temp1 = energy*energy;

	  temp5 = 9.9341e-6*temp1 -1.7857e-3*energy +7.924e-2;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      return(temp5);
    }

  static Scalar sigmaL3(Scalar energy)
    {
      float temp1, temp5;

      temp5= 0.0;
      if(8.4<= energy && energy <= 9.4)
	{
	  temp5 = energy -8.4;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(9.4 < energy && energy <= 100.)
	{
	  temp1 = energy*energy;

	  temp5 = -1.08852e-4*temp1 +1.10145e-2*energy +.92302246;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(100. < energy) temp5 = 9.4e-21;
      return(temp5);
    }
  static Scalar sigmaL4(Scalar energy)
    {
      float temp1, temp2, temp3;
      float temp5;

      temp5= 0.0;
      if(10. <= energy && energy <= 100.)
	{
	  temp1 = energy*energy;
	  temp2 = energy*temp1;
	  temp3 = energy*temp2;

	  temp5 = 1.1656e-9*temp3 -3.1555e-7*temp2 +1.8544e-5*temp1 +9.464e-4*energy -.0110422;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(100. < energy) temp5= 7e-22;
      return(temp5);
    }
  static Scalar sigmaneE(Scalar energy)
  // O- + O2 --> O- + O2
   // Revised by a new fit to the cross section measured by E. E. Muschlitz, 4th ICPIG 1959
  //  Uppsala Sweden  p. 52 - 56  by JTG May 18 2009
    {
      if (energy <= 300) return((-3.101e-22 * energy + 8.6525e-21 * sqrt(energy) + 3.2995e-20));
      return(9e-20);
    }

   static Scalar sigmaneuE(Scalar energy)
//  O + O2 --> O + O2
//  Scattering of O atoms off O2
//  Added, a fit to the cross section measured by Brunetti et al, JPC 74 (1981) 6734
//  by JTG October 3 2012
 {
      return((pow(10.0,(-0.16788 * (log10(energy)) - 17.76389 ))));
    }

  static Scalar sigmaeD(Scalar energy)
  // e + O- --> O + 2e
  // Revised by a new fit to the cross section measured by Vejby-Chrisensen et al., PRA 53 (1996) 2371
  // by JTG April 7 2009
    {
      if(energy < 1.465)                          return (0);
      else if(1.465 <= energy && energy < 20)  return((7.2763e-24*energy*energy*energy-4.6446e-22*energy*energy+9.703e-21*energy-1.3458e-20));
      return (5.4193e-20);
    }

  static Scalar sigmaMN(Scalar energy)
  // O- + O2+ --> O + O2
  // Revised by a new fit to the data measured by Olson JCP, 56 (1972) 2979 scaled down by a factor
  // of 5 to fit tha data of Padget and Pearton, JPB, 31 (1998) L995
  //  JTG April 7 2009
	// This is CM cross section, not target rest frame cross section; MAL 10/6/12
   {
	 energy = TWOTHIRDS*energy; // transform TRF energy to CM energy for projectile = O-, MAL 10/6/12
   float temp5;
   temp5 = 0;
  {
     if (energy < 100)   temp5 = (pow(10.0,(0.1544 * (log10(energy)) * (log10(energy)) - 0.5873 * (log10(energy)) - 17.6338)));
     else if (100 <= energy) temp5 = 6.4472e-19;
   }
  return(temp5);
   }
static Scalar sigmaMNO(Scalar energy)
 // O- + O+ --> O + O
  // A fit to the data measured by Olson JCP, 56 (1972) 2979 scaled down by a factor
  // of 6.4 to fit tha data of Hayton  and Peart, JPB, (1993) 2879
  //  JTG July 21 2009
	// This is CM cross section, not target rest frame cross section; MAL 10/6/12
  {
		energy = 0.5*energy; // transform TRF energy to CM energy, MAL 10/12/12
   float temp5;
   temp5 = 0;
  {
     if (energy < 100) temp5 = (pow(10.0,(0.1544 * (log10(energy)) * (log10(energy)) - 0.5873 * (log10(energy)) - 17.7410)));
     else if (100 <= energy) temp5 = 5.0368e-19;
   }
  return(temp5);
   }
static Scalar sigmaIO(Scalar energy)
// e + O --> O+ + 2e
  // A fit to the theoretical data from Kim and Deslaux, PRA, 66 (2002)  012708
  //  JTG June 23 2009                                                                                                                     {
{
   float temp5;
   temp5 = 0;
{
   if(energy < 13.62) temp5 = 0;
   else if(13.62 < energy && energy < 30)
      {
       temp5 = (pow(10.0,(49.7958  * (log10(energy)) * (log10(energy))* (log10(energy))  -215.5521   * (log10(energy)) * (log10(energy)) + 313.4800  * (log10(energy)) -173.5139)));
      }
   else if(30 <= energy && energy <= 5000)
      {
	temp5 = (pow(10.0,(-0.0393  * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy)) * (log10(energy)) + 0.6706  * (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy))* (log10(energy)) -4.7600 * (log10(energy)) * (log10(energy))* (log10(energy))* (log10(energy)) + 18.1277  * (log10(energy)) * (log10(energy))* (log10(energy))  -39.5597  * (log10(energy)) * (log10(energy)) + 47.0034 * (log10(energy)) -43.4784)));
      }
    else if (5000 < energy) temp5 = 0;
  }
return(temp5);
   }
  static Scalar sigmaEO(Scalar energy)
  // e + O --> O + e
  // The cross section for elastic scattering of electrons of oxygen atoms is taken from
  // the review by Itikawa and Ichimura, JPCRD 19 (1990) 637 for E > 2 eV and from the
  // theoretical work of Thomas and Nesbet, PRA 12 (1975) 1729
  // JTG July 20 2009
  {
       return(pow(10.0,(-0.2637 * (log10(energy)) * (log10(energy)) + 0.3825 * (log10(energy))  - 19.2932)));
    }

  static Scalar sigmaE1D(Scalar energy)
  // e + O(3P) --> O(1D) + e
  // The cross section for  electron excitation to the 1D state of oxygen atoms is taken from
  // the review by Laher and Gilmore, JPCRD 19 (1990) 277
  // JTG July 20 2009
  {
   if(energy < 1.96)                          return (0);
      else if(1.96 <= energy && energy < 200)
       return(pow(10.0,(-2.6057  * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy)) * (log10(energy)) + 22.3829 * (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy))* (log10(energy)) - 76.8464 * (log10(energy)) * (log10(energy))* (log10(energy))* (log10(energy)) + 134.4404  * (log10(energy)) * (log10(energy))* (log10(energy)) -127.0659  * (log10(energy)) * (log10(energy)) + 61.2974* (log10(energy)) -32.2819)));
   return(0);
    }

static Scalar sigmaE1S(Scalar energy)
  // e + O(3P) --> O(1S) + e
  // The cross section for  electron impact excitation to the 1S state of oxygen atoms is taken from
  // the review by Laher and Gilmore, JPCRD 19 (1990) 277
  // JTG July 20 2009
  {
   if(energy < 4.18)                          return (0);
      else if(4.18 <= energy && energy < 200)
       return(pow(10.0,(-3.8507  * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy)) * (log10(energy)) + 36.2079 * (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy))* (log10(energy)) - 138.9151 * (log10(energy)) * (log10(energy))* (log10(energy))* (log10(energy)) + 277.8211  * (log10(energy)) * (log10(energy))* (log10(energy)) - 305.7224 * (log10(energy)) * (log10(energy)) + 175.3352 * (log10(energy)) - 62.3533)));
   return(0);
    }

static Scalar sigmaE3P0(Scalar energy)
// e + O(3P) --> O(3P0) + e
  // The cross section for  electron impact excitation to the 3P0 state of oxygen atoms is taken from
  // the review by Laher and Gilmore, JPCRD 19 (1990) 277
  // JTG July 30 2009
  {
   if(energy < 15.65)                          return (0);
      else if(15.65 <= energy && energy < 200)
       return(pow(10.0,(-5.8339  * (log10(energy)) * (log10(energy)) * (log10(energy))  * (log10(energy)) + 45.9290 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 136.0395 * (log10(energy)) * (log10(energy)) + 178.6948  * (log10(energy)) - 108.4334)));
   return(0);
    }


static Scalar sigmaE5S0(Scalar energy)
// e + O(3P) --> O(5S0) + e
  // The cross section for  electron impact excitation to the 5S0 Rydberg state of oxygen atoms is taken from
  // the review by Laher and Gilmore, JPCRD 19 (1990) 277
  // JTG July 30 2009
  {
   if(energy < 9.14)                          return (0);
      else if(9.14 <= energy && energy < 200)
       return(pow(10.0,(-4.6176  * (log10(energy)) * (log10(energy)) * (log10(energy))  * (log10(energy)) + 34.2631 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 94.4228 * (log10(energy)) * (log10(energy)) + 111.5283 * (log10(energy)) - 69.0001)));
   return(0);
    }

static Scalar sigmaE3S0(Scalar energy)
// e + O(3P) --> O(3S0) + e
  // The cross section for  electron impact excitation to the 3S0 Rydberg state of oxygen atoms is taken from
  // the review by Laher and Gilmore, JPCRD 19 (1990) 277
  // JTG July 30 2009
  {
   if(energy < 9.51)                          return (0);
      else if(9.51 <= energy && energy < 200)
       return(pow(10.0,(-3.3173  * (log10(energy)) * (log10(energy)) * (log10(energy))  * (log10(energy)) + 22.9180 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 58.4781 * (log10(energy)) * (log10(energy)) + 64.9565 * (log10(energy)) - 47.4447)));
   return(0);
    }


  static Scalar sigmaCE(Scalar energy)
//   O2+ + O2 --> O2 + O2+
//   Revised by a new fit to the data from Ellis et al. Atomic Data and Nuclear Data Tables
//   17 (1977) 177 and the measured cross section of Baer et al. JPC 68 (1978) 4901 and
//   Wilcox and Moran, JPC, 85 (1981) 989
//   JTG June 17 2009
  {
       return(pow(10.0,(0.0503 * (log10(energy)) * (log10(energy)) - 0.3502 * (log10(energy))  - 18.5476)));
    }

static Scalar sigmapoE(Scalar energy)
//   O2+ + O2 --> O2 + O2+
//   The scattering cross section for O2+ off O2 is assumed to be half of the cross section for charge exchange
//   which is a fit to the data from Ellis et al. Atomic Data and Nuclear Data Tables
//   17 (1977) 177 and the measured cross section of Baer et al. JPC 68 (1978) 4901 and
//   Wilcox and Moran, JPC, 85 (1981) 989
  // JTG July 31 2009
  {
       return(0.5 * pow(10.0,(0.0503 * (log10(energy)) * (log10(energy)) - 0.3502 * (log10(energy))  - 18.5476)));
    }

  static Scalar sigmaCEO(Scalar energy)
//   O+ + O --> O + O
//   A fit given in the paper by Stebbings et al. JGR 69 (1964) 2349
//   JTG June 25 2009
  {
    return((5.95e-10 - 0.63e-10 * log(energy))*(5.95e-10 - 0.63e-10 * log(energy)));
    }

 static Scalar sigmaCEOO(Scalar energy)
//   O+ + O2 --> O + O2+
//   A fit given in the paper by Lindsay et al. JGR 110 (2005) A12213
//   JTG June 25 2009
  {
    return((2.16 * (1-exp(-0.250/energy)) * (1-exp(-0.250/energy)) + (2.85 + 0.103 * log(energy)) * (2.85 + 0.103 * log(energy)) * (1-exp(-energy/0.144))*(1-exp(-energy/0.144))*(1-exp(-energy/0.144))*(1-exp(-energy/0.144)) ) * 1e-20);
    }

 static Scalar sigmapooE(Scalar energy)
//   O+ + O2 --> O2 + O+
 // The cross section for elastic scattering of O+ off O2 is assumed to be half of
  // the cross section for the charge exchange given  in the paper by Lindsay et al. JGR 110 (2005) A12213
  // JTG July 31 2009
  {
    return(0.5*(2.16 * (1-exp(-0.250/energy)) * (1-exp(-0.250/energy)) + (2.85 + 0.103 * log(energy)) * (2.85 + 0.103 * log(energy)) * (1-exp(-energy/0.144))*(1-exp(-energy/0.144))*(1-exp(-energy/0.144))*(1-exp(-energy/0.144)) ) * 1e-20);
    }

static Scalar sigmaCEO2O(Scalar energy)
//   O2+ + O --> O2 + O+
//   A fit given in the paper by Stebbings et al. JCP (1963) 2280
//   JTG July 26 2009 revised November 4 2009
{
  float temp5;
  temp5 = 0;
{
if(energy < 4.68)      temp5 = 0; // 1.56V -> 4.68V; 1.56V is Ethr in CM frame, 4.68 is Ethr in target rest frame; MAL 10/6/12, 12/9/12
 else if (4.68 <= energy && energy < 82.035)   temp5 = 4.1111e-20 * ( 1 - 4.68/energy); // 1.56 -> 4.68, 2X; 3.9663 -> 4.1111, 12/9/12, MAL
      else if(82.035 <= energy)
 temp5 = (pow(10.0,0.1476 * (log10(energy)) - 19.6917));
     }
 return(temp5);
}
  static Scalar sigmaDR(Scalar energy)
//  e + O2+ --> O(3P) + O(1D)
//  Revised by a new fit to the combined data measured by Mul and McGowan, JPB, 12 (1979) 1591
//  and Peverall eta al, JCP 114 (2001) 6679
//  JTG April 7 2009
    {
      return(pow(10.0,-1.0993 * (log10(energy)) - 19.6146 ));
    }

  static Scalar sigmaneD(Scalar energy)
//  O- + O2 --> O + O2 + e
//  Revised by a new fit to the cross section measured by Comer and Schulz, JPB 7 (1974) L249
//  by JTG May 20 2009
	// This is CM cross section, not target rest frame cross section; MAL 10/12/12
  {
			energy = TWOTHIRDS*energy; // transform TRF energy to CM system, MAL 10/12/12
      if(energy < 1.465)                          return (0);
      if(1.465 <=energy && energy < 15)  return (pow(10.0,(1.8184 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 5.6529 * (log10(energy)) * (log10(energy)) + 6.2460 * (log10(energy))  - 22.2561)));
      return (1.694e-20);
    }

static Scalar sigmanegOD(Scalar energy)
// O- + O --> O2 + e
// Cross section estimated from a measured rate coefficient by Belostotsky et al. PSST (2005) 532
// and allowed to decay as 1/sqrt(E) for E < 0.184 eV and then take a constant
// value of 1.0964e-19 m^2
// JTG July 16 2009 revised November 3 2009 revised again August 20 2012
{
float temp5;
temp5 = 0;
{
// fprintf(stderr,"\n Energy = %i", energy);
temp5 = 1.0964e-19;
if (energy < 0.184) temp5 = 4.7032e-20/sqrt(energy);
}
return(temp5);
}

    static Scalar sigmaPD(Scalar energy)
//  e + O2 --> O+ + O- + e
//  Polar dissociation
//  A fit to the cross section measured by Rapp and Briglia, JCP 43 (1965) 1480
//  by JTG July 28 2009
    {
      if(energy < 15)                          return (0);
      else if(15 <=energy && energy < 50)  return (pow(10.0,(-34.9127 * (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy)) + 243.9555 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 634.9551 *  (log10(energy)) * (log10(energy)) + 729.7911 * (log10(energy))  - 334.66)));
      return (8.419e-23);
    }




static Scalar sigmaFRAG(Scalar energy)
//  O2+ + O2 --> O+ + O + O2
//  Fragmentation of O2+ and O2 by high energy O2+ ions
//  Cross section is the gas kinetic cross section for oxygen
//  with a threshold 22 V
//  JTG August 17 2009 revised November 4 2009
//cross section for O2+ + O2 -> O+ + O + O2 in fig 10 from moran68_3411.
// T. F. Moran and J. R. Roberts, JCP vol 49 p 3411 (1968)
// Revised June 5 2013 JTG and Shuo Huang
//{
//  float temp5;
//  temp5 = 0;
//    {
//      if (energy <= 13.8) temp5 = 0;
//    else  if(13.8 < energy)  	 temp5 = 4.9e-19*(1-13.8/energy);
//    }
//    return (temp5);
//  }
{
        energy=0.5*energy;//transform TRF energy to CMF system, similar to sigmaneD in oxygen_gas.hpp
    if (energy <= 6.3) return 0;
    else if (energy <= 6.44) return((pow(10.0,(  -6245.6256 * (log10(energy)) * (log10(energy))  +10148.965  * (log10(energy))     -4144.1414 ))));
else if (energy <= 6.96) return((pow(10.0,( -104.3908 * (log10(energy)) * (log10(energy)) * (log10(energy)) -4.109608 * (log10(energy)) * (log10(energy))  +234.15368 * (log10(energy))     -152.7442 ))));
    else if (energy <=10 ) return((pow(10.0,( -869.78668 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 3306.8163 * (log10(energy)) * (log10(energy)) * (log10(energy))     -4700.1768 * (log10(energy)) * (log10(energy))  +2960.24095  * (log10(energy)) -717.89269 ))));
    else if (energy <= 15) return((pow(10.0,( -9.996669 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 50.898256 * (log10(energy)) * (log10(energy)) * (log10(energy))     -97.55846 * (log10(energy)) * (log10(energy))  +83.23836  * (log10(energy)) -47.383609 ))));
else if (energy <= 10000) return((pow(10.0,( -20.75849   ))));
    else return 0;
}



   static Scalar sigmaO2O2(Scalar energy)
//  O2 + O2 --> O2 + O2
//  Scattering of O2 atoms off O2
//
//  A fit to the cross section measured by Brunetti et al, JPC 74 (1981) 6734
//  by JTG August 8 2012
//
  {
    	if (energy <=0.0672) return 3.0978E-18;
	else if (energy <= 0.13) return((pow(10.0,(  -2.0611* (log10(energy)) * (log10(energy)) * (log10(energy)) -6.859 * (log10(energy)) * (log10(energy))  -7.7694  * (log10(energy)) -20.511 ))));
	else if (energy <=0.175 ) return((pow(10.0,( -0.3295 * (log10(energy)) * (log10(energy)) * (log10(energy)) -0.2582 * (log10(energy)) * (log10(energy))  -0.0745  * (log10(energy)) -17.671 ))));
	else if (energy <=0.3320 ) return((pow(10.0,( -23.184 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) -59.162  * (log10(energy)) * (log10(energy)) * (log10(energy)) -55.274 * (log10(energy)) * (log10(energy))  -22.409  * (log10(energy)) -20.96 ))));
   	else return 2.3645E-18;
  }

   static Scalar sigmaOO(Scalar energy)
//  O + O --> O + O
//  Scattering of O atoms off O
//  A fit to the cross section measured by Brunetti et al, JPC 74 (1981) 6734
// for O + O2 --> O + O2  which is divided by 2
//  by JTG October 3 2012
  {
      return(0.5*(pow(10.0,(-0.16788 * (log10(energy)) -17.76389 ))));
    }



  static Scalar sigmaIdelta(Scalar energy)
  //  e + O2(Delta) --> O2+ + 2e
  //   Fit to the data measured by E. Krishnakumar and S. K. Srivastava
  //Int. J. Mass Spectrometry and Ion Processes vol. 113, pp. 1-12 (1992)
  // but threshold reduced
  //  JTG January 4 2013
 {
    float temp5;


    temp5= 0.0;
    if(11.08 <= energy && energy <= 180.)
      {
      temp5 = (pow(10.0,(-2.5050 * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) + 21.1338 * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98))  - 52.8155 * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) - 37.2837 * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) + 434.1281  * (log10(energy+0.98)) * (log10(energy+0.98)) * (log10(energy+0.98)) - 874.6800  * (log10(energy+0.98)) * (log10(energy+0.98)) + 772.1372  * (log10(energy+0.98)) - 283.5866)));
      }
      else if(180 < energy)
	{
	  temp5 = (pow(10.0,(-0.2981 * (log10(energy+0.98)) * (log10(energy+0.98)) + 1.0264 * (log10(energy+0.98))  - 20.5514)));
	}
      else if(energy+0.98 < 12.06)
	{
	  temp5 = 0;
        }
    return(temp5);
 }

static Scalar sigmaIsigma(Scalar energy)
  //  e + O2(Sigma) --> O2+ + 2e
  //   Fit to the data measured by E. Krishnakumar and S. K. Srivastava
  //Int. J. Mass Spectrometry and Ion Processes vol. 113, pp. 1-12 (1992)
  // but threshold reduced by 1.627 eV
  //  JTG February 4 2015
 {
    float temp5;


    temp5= 0.0;
    if(10.433 <= energy && energy <= 180.)
      {
      temp5 = (pow(10.0,(-2.5050 * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) + 21.1338 * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627))  - 52.8155 * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) - 37.2837 * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) + 434.1281  * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) - 874.6800  * (log10(energy+1.627)) * (log10(energy+1.627)) + 772.1372  * (log10(energy+1.627)) - 283.5866)));
      }
      else if(180 < energy)
	{
	  temp5 = (pow(10.0,(-0.2981 * (log10(energy+1.627)) * (log10(energy+1.627)) + 1.0264 * (log10(energy+1.627))  - 20.5514)));
	}
      else if(energy+1.627 < 12.06)
	{
	  temp5 = 0;
        }
    return(temp5);
 }


static Scalar sigmaL1delta(Scalar energy)
    {
      //  A fit the cross sections measured by Shyn and Sweeney, PRA, 62 (2000) 022711
      //  and Green et al JPB 34 (2001) L157 taken from Y. Itikawa, JPCRD 38 (2009) 1
      //   Threshold reduced JTG January 7 2013
      //
      float temp5;

      temp5= 0.0;
      if(3.073 < energy && energy <= 10-0.977)
	{
	  temp5 = 1.1933e-23  *(energy+0.977) * (energy+0.977) * (energy+0.977) - 2.4345e-22 * (energy+0.977)* (energy+0.977) + 1.7726e-21* (energy+0.977) - 3.9778e-21;
	}
      else if(10-0.977 < energy && energy < 40)
	{
          temp5 = (pow(10.0,(-16.7249 * (log10(energy+0.977)) * (log10(energy+0.977)) * (log10(energy+0.977)) * (log10(energy+0.977)) + 79.2236 * (log10(energy+0.977))* (log10(energy+0.977))* (log10(energy+0.977)) - 143.5578 * (log10(energy+0.977))* (log10(energy+0.977)) +116.2163* (log10(energy+0.977)) -56.0342 )));
	}
      return(temp5);
    }

  static Scalar sigmaL2delta(Scalar energy)
    {
      float temp1, temp2, temp3;
      float temp5;

      temp5= 0.0;
      if(5.023<= energy && energy <= 19.023)
	{
	  temp1 = (energy+0.977)*(energy+0.977);
	  temp2 = (energy+0.977)*temp1;
	  temp3 = (energy+0.977)*temp2;

	  temp5 = -1.1894e-4*temp3 +6.8655e-3*temp2 -.143425*temp1 +1.26276*(energy+0.977) - 3.7338513;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
  }
      else if(19.023 < energy && energy <= 99.023)
	{
	  temp1 = (energy+0.977)*(energy+0.977);

	  temp5 = 9.9341e-6*temp1 -1.7857e-3*(energy+0.977) +7.924e-2;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      return(temp5);
    }

  static Scalar sigmaL3delta(Scalar energy)
    {
      float temp1, temp5;

      temp5= 0.0;
      if(7.423<= energy && energy <= 8.423)
	{
	  temp5 = (energy+0.977) -8.4;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(8.423 < energy && energy <= 99.023)
	{
	  temp1 = (energy+0.977)*(energy+0.977);

	  temp5 = -1.08852e-4*temp1 +1.10145e-2*(energy+0.977) +.92302246;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(99.023 < energy) temp5 = 9.4e-21;
      return(temp5);
    }


  static Scalar sigmaL4delta(Scalar energy)
    {
      float temp1, temp2, temp3;
      float temp5;

      temp5= 0.0;
      if(9.023 <= energy && energy <= 99.023)
	{
	  temp1 = (energy+0.977)*(energy+0.977);
	  temp2 = (energy+0.977)*temp1;
	  temp3 = (energy+0.977)*temp2;

	  temp5 = 1.1656e-9*temp3 -3.1555e-7*temp2 +1.8544e-5*temp1 +9.464e-4*(energy+0.977) -.0110422;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(99.023 < energy) temp5= 7e-22;
      return(temp5);
    }


static Scalar sigmaSSdelta(Scalar energy)
//  e + O2(delta) -> O2(sigma) + e
//  Use the cross section for the excitation of O2(X) and apply
//  threshold reduction of 0.65 eV
//  The cross section is also scaled up by a factor of 10 according to
//  Hall and Trajmar, J. Phys B, 8(12) (1975) L293
    {
      float temp1, temp2, temp3;
      float temp4, temp5;

      temp5= 0.0;
      if(0.65 <= energy && energy <= 24.35)
	{
	  temp1 = (energy+0.65)*(energy+0.65);
	  temp2 = (energy+0.65)*temp1;
	  temp3 = (energy+0.65)*temp2;
	  temp4 = (energy+0.65)*temp3;

	  temp5 = 5.34245e-8*temp4 -4.7117e-6*temp3 +1.581e-4*temp2 -2.4783e-3*temp1 +1.70373e-2*(energy+0.65) -2.2343e-2;
	  temp5 *= 1e-20*10;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(24.35 < energy && energy <= 44.35)
	{
	  temp1 = (energy+0.65)*(energy+0.65);
	  temp2 = (energy+0.65)*temp1;

	  temp5 = 5.445e-7*temp2 -5.674e-5*temp1 +1.7502e-3*(energy+0.65) -1.0375e-2;
	  temp5 *= 1e-20*10;
	}
      else if(44.35 < energy && energy <= 99.35)
	{
	  temp5 = -5.64e-5*(energy+0.65) +5.64e-3;
	  temp5 *= 1e-20*10;
	}
      return(temp5);
    }

static Scalar sigmaL1sigma(Scalar energy)
    {
      //  A fit the cross sections measured by Shyn and Sweeney, PRA, 62 (2000) 022711
      //  and Green et al JPB 34 (2001) L157 taken from Y. Itikawa, JPCRD 38 (2009) 1
      //   Threshold reduced by 1.627 eV  JTG February 4 2015
      //
      float temp5;

      temp5= 0.0;
      if(2.423 < energy && energy <= 10-1.627)
	{
	  temp5 = 1.1933e-23  *(energy+1.627) * (energy+1.627) * (energy+1.627) - 2.4345e-22 * (energy+1.627)* (energy+1.627) + 1.7726e-21* (energy+1.627) - 3.9778e-21;
	}
      else if(10-1.627 < energy && energy < 40)
	{
          temp5 = (pow(10.0,(-16.7249 * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) + 79.2236 * (log10(energy+1.627))* (log10(energy+1.627))* (log10(energy+1.627)) - 143.5578 * (log10(energy+1.627))* (log10(energy+1.627)) +116.2163* (log10(energy+1.627)) -56.0342 )));
	}
      return(temp5);
    }

  static Scalar sigmaL2sigma(Scalar energy)
    {
      float temp1, temp2, temp3;
      float temp5;

      temp5= 0.0;
      if(4.493<= energy && energy <= 18.373)
	{
	  temp1 = (energy+1.627)*(energy+1.627);
	  temp2 = (energy+1.627)*temp1;
	  temp3 = (energy+1.627)*temp2;

	  temp5 = -1.1894e-4*temp3 +6.8655e-3*temp2 -.143425*temp1 +1.26276*(energy+1.627) - 3.7338513;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
  }
      else if(18.373 < energy && energy <= 98.373)
	{
	  temp1 = (energy+1.627)*(energy+1.627);

	  temp5 = 9.9341e-6*temp1 -1.7857e-3*(energy+1.627) +7.924e-2;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      return(temp5);
    }

  static Scalar sigmaL3sigma(Scalar energy)
    {
      float temp1, temp5;

      temp5= 0.0;
      if(6.773<= energy && energy <= 7.773)
	{
	  temp5 = (energy+1.627) - 8.4;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(7.773 < energy && energy <= 98.373)
	{
	  temp1 = (energy+1.627)*(energy+1.627);

	  temp5 = -1.08852e-4*temp1 +1.10145e-2*(energy+1.627) +.92302246;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(98.373 < energy) temp5 = 9.4e-21;
      return(temp5);
    }


  static Scalar sigmaL4sigma(Scalar energy)
    {
      float temp1, temp2, temp3;
      float temp5;

      temp5= 0.0;
      if(8.3730 <= energy && energy <= 98.3730)
	{
	  temp1 = (energy+1.627)*(energy+1.627);
	  temp2 = (energy+1.627)*temp1;
	  temp3 = (energy+1.627)*temp2;

	  temp5 = 1.1656e-9*temp3 -3.1555e-7*temp2 +1.8544e-5*temp1 +9.464e-4*(energy+1.627) -.0110422;
	  temp5 *= 1e-20;
	  if(temp5 <= 0.0) temp5=0.0;
	}
      else if(98.373 < energy) temp5= 7e-22;
      return(temp5);
    }


static Scalar sigmaDISSIZdelta(Scalar energy)
  //  e + O2 --> O+ + O + 2e
  // A  fit to the data measured by E Krishnakumar and S. K. Srivastava
  // Int. J. Mass Spectrometry and Ion Processes vol. 113, pp. 1-12 (1992)
  //  Threshold reduced JTG January 7 2013
  {
      float temp5;

      temp5 =0;
      if(18.73-0.977 <= energy && energy <= 150-0.977)
	{
	  temp5 = (pow(10.0,(5.1188 * (log10(energy+0.977)) * (log10(energy+0.977)) * (log10(energy+0.977)) * (log10(energy+0.977)) * (log10(energy+0.977)) - 53.1537 * (log10(energy+0.977)) * (log10(energy+0.977)) * (log10(energy+0.977)) * (log10(energy+0.977))  + 220.3817 * (log10(energy+0.977)) * (log10(energy+0.977)) * (log10(energy+0.977)) -457.3868 * (log10(energy+0.977)) * (log10(energy+0.977)) + 476.8381  * (log10(energy+0.977))  - 220.5401)));
	}
      else if(150 < energy+0.977)
	{
	 temp5 = (pow(10.0,(-0.9987 * (log10(energy+0.977)) * (log10(energy+0.977)) + 4.3037 * (log10(energy+0.977))  - 24.6189)));
	}
      else if(energy+0.977 < 18.73)
	{
	  temp5 = 0;
        }
    return(temp5);
    }


static Scalar sigmaDISSIZsigma(Scalar energy)
//   e + O2(b) --> O+ + O + 2e
//   A  fit to the data measured by E. Krishnakumar and S. K. Srivastava
//   Int. J. Mass Spectrometry and Ion Processes vol. 113, pp. 1-12 (1992)
//   Threshold reduced  by 1.627 eV JTG February 4 2015
  {
      float temp5;

      temp5 =0;
      if(18.73-1.627 <= energy && energy <= 150-1.627)
	{
	  temp5 = (pow(10.0,(5.1188 * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) - 53.1537 * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627))  + 220.3817 * (log10(energy+1.627)) * (log10(energy+1.627)) * (log10(energy+1.627)) -457.3868 * (log10(energy+1.627)) * (log10(energy+1.627)) + 476.8381  * (log10(energy+1.627))  - 220.5401)));
	}
      else if(150 < energy+1.627)
	{
	 temp5 = (pow(10.0,(-0.9987 * (log10(energy+1.627)) * (log10(energy+1.627)) + 4.3037 * (log10(energy+1.627))  - 24.6189)));
	}
      else if(energy+1.627 < 18.73)
	{
	  temp5 = 0;
        }
    return(temp5);
    }



  static Scalar sigmaneDdelta(Scalar energy)
//  O- + O(singlet delta)2 --> O + O2 + e
// Cross section estimated from a measured rate coefficient by Midey et al J. Phys. Chem A 112 3040 (2008)
// and allowed to decay as 1/sqrt(E) for E < 0.184 eV and then take a constant
// value of
// kfit/sqrt(2*1.6e-19*0.0258*400/300/10.667/1.67e-27)*sqrt(0.0258*400/300)/sqrt(0.184) = 5.7502e-20 m^2
  // The mesured rate coefficient at 400 K is used
// JTG January 7 2013 and JTG December 3 2014
{
float temp5;
temp5 = 0;
{
// fprintf(stderr,"\n Energy = %i", energy);
  temp5 = 5.7502e-20;
if (energy < 0.184) temp5 = 2.4665e-20/sqrt(energy);
}
return(temp5);
}



  static Scalar sigmaneDsigma(Scalar energy)
//  O- + O(singlet sigma)2 --> O + O2 + e
// Cross section estimated from a rate coefficient estimated by Aleksandrov Sov. Phys. - Techn. Phys 23 (1978) 806
// and allowed to decay as 1/sqrt(E) for E < 0.184 eV and then take a constant
// value of
// 6.9E-16/sqrt(2*1.6e-19*0.0258*400/300/10.667/1.67e-27)*sqrt(0.0258*400/300)/sqrt(0.184) = 3.795E-19 m^2
// JTG February 10 2015
{
float temp5;
temp5 = 0;
{
// fprintf(stderr,"\n Energy = %i", energy);
  temp5 = 3.7953e-19;
if (energy < 0.184) temp5 = 1.628e-19/sqrt(energy);
}
return(temp5);
}



 static Scalar sigmaO1DO2(Scalar energy)
// O(1D) + O2  --> O + O2(b1Sigma)
//
//
// Cross section estimated from a rate coefficient given by Baulch JPCRD 1982 327
// and allowed to decay as 1/sqrt(E) for E < 0.184 eV and then take a constant
// value of
// 2.56E-17exp(67/300)/sqrt(2*1.6e-19*0.0258*400/300/10.667/1.67e-27)*sqrt(0.0258*400/300)/sqrt(0.184) = 1.76E-20 m^2
// JTG February 10 2015
{
float temp5;
temp5 = 0;
{
// fprintf(stderr,"\n Energy = %i", energy);
  temp5 = 1.76E-20;
if (energy < 0.184) temp5 = 7.5516E-21/sqrt(energy);
}
return(temp5);
}


static Scalar sigmaO2bO2quench(Scalar energy)
// O2(b) + O2  --> O2 + O2
//
//
// Cross section estimated from a rate coefficient given by Baulch JPCRD 1982 327
// and allowed to decay as 1/sqrt(E) for E < 0.184 eV and then take a constant
// value of
// 4E-23/sqrt(2*1.6e-19*0.0258*400/300/10.667/1.67e-27)*sqrt(0.0258*400/300)/sqrt(0.184) =  2.2E-26 m^2
// JTG February 13 2015
{
float temp5;
temp5 = 0;
{
// fprintf(stderr,"\n Energy = %i", energy);
  temp5 = 2.2E-26;
if (energy < 0.184) temp5 = 9.4377E-27/sqrt(energy);
}
return(temp5);
}


 static Scalar sigmaDAdelta(Scalar energy)
  // e + O2(singlet delta) --> O + O-
  // Fit to the cross section of Jaffke et al CPL 193 (1992) 62
//  JTG January 8 2013
{
     float temp5;
    temp5 =0;
 if(2.36 <= energy && energy <= 11.86)
        {
        temp5 = (pow(10.0,(102.9751 * (log10(energy)) * (log10(energy))* (log10(energy))* (log10(energy))  - 300.7401 * (log10(energy))* (log10(energy)) * (log10(energy)) + 295.6272 * (log10(energy)) * (log10(energy))  -111.0596* (log10(energy))-9.9554)));
        }
      else
        {
          temp5 = 0;
        }
    return(temp5);
    }



 static Scalar sigmaDAsigma(Scalar energy)
  // e + O2(singlet sigma) --> O + O-
  // Fit to the cross section of Jaffke et al CPL 193 (1992) 62
//   but threshold reduced by 1.627 - 0.977 = 0.65 eV
 // JTG February 4 2015
{
     float temp5;
    temp5 =0;
 if(1.71 <= energy && energy <= 11.21)
	{
	temp5 = (pow(10.0,(102.9751 * (log10(energy+0.65)) * (log10(energy+0.65))* (log10(energy+0.65))* (log10(energy+0.65))  - 300.7401 * (log10(energy+0.65))* (log10(energy+0.65)) * (log10(energy+0.65)) + 295.6272 * (log10(energy+0.65)) * (log10(energy+0.65))  -111.0596* (log10(energy+0.65))-9.9554)));
	}
      else
	{
	  temp5 = 0;
	}
    return(temp5);
    }



static Scalar sigmaSDdex(Scalar energy)
  {
    float temp1, temp2, temp3;
    float temp4, temp5;

    temp5= 0.0;
    if(0.0 <= energy && energy <= 9.523)
      {
	temp1 = (energy+0.977)*(energy+0.977);
	temp2 = (energy+0.977)*temp1;
	temp3 = (energy+0.977)*temp2;
	temp4 = (energy+0.977)*temp3;

	temp5 = -3.0913e-6*temp4 +1.436e-4*temp3 -.0022876*temp2 +.0133286*temp1 -.0100266*(energy+0.977) -.0015636;
	temp5 *= 1e-20;
	if(temp5 <= 0.0) temp5=0.0;
      }
    else if(9.523 < energy && energy <= 44.023)
      {
	temp1 = (energy+0.977)*(energy+0.977);
	temp2 = (energy+0.977)*temp1;

	temp5 = -1.0959e-6*temp2 +1.349e-4*temp1 -.005984*(energy+0.977) +.1079;
	temp5 *= 1e-20;
      }
    else if(45 < (energy+0.977) && (energy+0.977) <= 100.0)
      {
	temp5 = -2.18e-4*(energy+0.977) +2.18e-2;
	temp5 *= 1e-20;
      }
    return(temp5*3/2*(1 + 0.977/energy));

  }

static Scalar sigmaIOD(Scalar energy)
// e + O(1D) --> O+ + 2e
  // A fit to the theoretical data from Kim and Deslaux, PRA, 66 (2002)  012708
  // Threshold reduced  JTG January 10 2013                                                                                                                    {
{
   float temp5;
   temp5 = 0;
{
   if(energy < 11.66) temp5 = 0;
   else if(11.66 < energy && energy < 28.04)
      {
       temp5 = (pow(10.0,(49.7958  * (log10(energy+1.96)) * (log10(energy+1.96))* (log10(energy+1.96))  -215.5521   * (log10(energy+1.96)) * (log10(energy+1.96)) + 313.4800  * (log10(energy+1.96)) -173.5139)));
      }
   else if(30 <= energy+1.96 && energy+1.96 <= 5000)
      {
	temp5 = (pow(10.0,(-0.0393  * (log10(energy+1.96)) * (log10(energy+1.96)) * (log10(energy+1.96)) * (log10(energy+1.96))* (log10(energy+1.96)) * (log10(energy+1.96)) + 0.6706  * (log10(energy+1.96)) * (log10(energy+1.96)) * (log10(energy+1.96))* (log10(energy+1.96))* (log10(energy+1.96)) -4.7600 * (log10(energy+1.96)) * (log10(energy+1.96))* (log10(energy+1.96))* (log10(energy+1.96)) + 18.1277  * (log10(energy+1.96)) * (log10(energy+1.96))* (log10(energy+1.96))  -39.5597  * (log10(energy+1.96)) * (log10(energy+1.96)) + 47.0034 * (log10(energy+1.96)) -43.4784)));
      }
    else if (5000 < energy+1.96) temp5 = 0;
  }
return(temp5);
   }

static Scalar sigmaO2bO2X(Scalar energy) // Added by JTG February 6 2015
//  O2(b) + O2(X) -> O2(X) + O2(X)
// Assume sigma=K0/sqrt(2*e*E/m) with K = K0 = const
// Rate coefficient 4 x 10^-23 m^3/s from Baulch et al JPCRD 11 (1982) 327
 {
   if (energy > 1E-6) return(1.7988E-26/sqrt(energy));
    else return(1.7988E-23);
 }



 static Scalar sigmaE1Ddex(Scalar energy)
  // e + O(1D) --> O(3P) + e
  // The cross section for  electron excitation to the 1D state of oxygen atoms is taken from
  // the review by Laher and Gilmore, JPCRD 19 (1990) 277 and reversed using detailed balancing
  // JTG January 10 2013
  {
    if(energy+1.96 > 200.0) return (0);
//   else if(energy < 198.04)
    else // MAL200117
	  return((1+1.96/energy)*9/5*pow(10.0,(-2.6057  * (log10(energy+1.96)) * (log10(energy+1.96)) * (log10(energy+1.96)) * (log10(energy+1.96))* (log10(energy+1.96)) * (log10(energy+1.96)) + 22.3829 * (log10(energy+1.96)) * (log10(energy+1.96)) * (log10(energy+1.96))* (log10(energy+1.96))* (log10(energy+1.96)) - 76.8464 * (log10(energy+1.96)) * (log10(energy+1.96))* (log10(energy+1.96))* (log10(energy+1.96)) + 134.4404  * (log10(energy+1.96)) * (log10(energy+1.96))* (log10(energy+1.96)) -127.0659  * (log10(energy+1.96)) * (log10(energy+1.96)) + 61.2974* (log10(energy+1.96)) -32.2819)));
  }

    private:
  // Functions needed for reaction kinetics:
  //------------------<
  Scalar get_DIeEnergyloss() const {return 17.49;} // If energy loss does not correspond to the threshold

  Scalar get_DE1energy() const { return 4.4; }
  Scalar get_DE2energy () const { return 4.7;}
  Scalar get_DIenergy() const { return 3.2;}
  Scalar get_CXhenergy()  const {return 2.96;}

  Scalar get_DR1energy() const {return 8.17;}
  Scalar get_DR2energy() const {return 7.83;}
  Scalar get_DR3energy() const {return 3.42;}
  Scalar get_DE3energy() const {return 3.6;}
  Scalar get_DE4energy() const {return 2.6;}
  Scalar get_DE5energy() const {return 4.0;}
};

#endif
