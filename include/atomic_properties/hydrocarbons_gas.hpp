#ifndef HYDROCARBONSGAS_H
#define HYDROCARBONSGAS_H

/*
  Collision processes of CHy and CHy+ hydrocarbons with plasma electrons and protons
  R.K. Janev and D. Reiter (2002)
- Cross-sections are integrated over excited states by Janev
- Masses are indicated, in case we don't follow all the secondary species: masses correpond to deuterium not hydrogen.
- Reactions for Carbon have been taken from  www.eirene.de/eigen
*/
/*
 NOTE TO DEVELOPERS: // MAL200117
 A ReactionGroup is a set of reactions having the same reactant[0], reactant[1], and mcc type. The priority in reactants and reactions is meaningful.  The first reactant (reactant[0]) in the first reaction in a ReactionGroup determines the mass used to evaluate the energy for the cross section: energy = 0.5*mass(reactant[0])*(relative velocity)^2. Uncomment #define debugReactionGroup in reaction.hpp to print all ReactionGroups to the terminal during initialization.
 */

#include "atomic_properties/gas.hpp"
#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "main/species.hpp"

class CH4_Gas: public Gas
{
public:
  CH4_Gas(oopicListIter<Species> species_iter, Species *sec_e, Species *sec_i,
	  Species *sec_H, Species *sec_H_ion, Species *sec_H2,
	  Species *sec_CH, Species *sec_CH2, Species *sec_CH2_ion,
	  Species *sec_CH3, Species *sec_CH3_ion,
	  bool no_default_secondary_species_flag)
  {
    mcctype = "hc_mcc";
    gastype = CH4_GAS;

    vector<Species *> E_species = Species::get_reaction_species_named_with(species_iter, E);
    vector<Species *> CH4_species = Species::get_reaction_species_named_with(species_iter, CH4);
    vector<Species *> H_ion_species = Species::get_reaction_species_named_with(species_iter, H_ion);

    if (!no_default_secondary_species_flag)
    {
      if (!sec_e && !E_species.empty()) sec_e = E_species[0];
      if (!sec_i) sec_i = Species::get_species_named_with(species_iter, CH4_ion);
      if (!sec_H) sec_H = Species::get_species_named_with(species_iter, H);
      if (!sec_H_ion && !H_ion_species.empty()) sec_H_ion = H_ion_species[0];
      if (!sec_H2) sec_H2 = Species::get_species_named_with(species_iter, H2);
      if (!sec_CH) sec_CH = Species::get_species_named_with(species_iter, CH);
      if (!sec_CH2) sec_CH2 = Species::get_species_named_with(species_iter, CH2);
      if (!sec_CH2_ion) sec_CH2_ion = Species::get_species_named_with(species_iter, CH2_ion);
      if (!sec_CH3) sec_CH3 = Species::get_species_named_with(species_iter, CH3);
      if (!sec_CH3_ion) sec_CH3_ion = Species::get_species_named_with(species_iter, CH3_ion);
    }


    for (int j=CH4_species.size()-1; j>=0; j--)
    {
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaI, 12.63, E_IONIZATION);
        reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], CH4_species[j], xsection, mcctype(), true, sec_e, sec_i));
        xsection = add_xsection(sigmaDI, 14.25, E_DISS_IONIZATION);
        reaction = new Reaction(gastype, E_DISS_IONIZATION, E_species[i], CH4_species[j], xsection, mcctype(), true, sec_e, sec_H, sec_CH3_ion);
	reaction->add_characteristic_value(get_DIeEnergyloss());
	reaction->add_characteristic_value(get_DIenergy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDE1, 8.8, E_DISS_EXCITATION1);
        reaction = new Reaction(gastype, E_DISS_EXCITATION1, E_species[i], CH4_species[j], xsection, mcctype(), true, sec_H, sec_CH3);
	reaction->add_characteristic_value(get_DE1energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDE2, 9.4, E_DISS_EXCITATION2);
        reaction = new Reaction(gastype, E_DISS_EXCITATION2, E_species[i], CH4_species[j], xsection, mcctype(), true, sec_H2, sec_CH2);
	reaction->add_characteristic_value(get_DE2energy());
        reactionlist.add(reaction);
      }
      for (int i=H_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaeE, 0.0, P_eEXCHANGE);
        reactionlist.add(new Reaction(gastype, P_eEXCHANGE, H_ion_species[i], CH4_species[j], xsection, mcctype(), true, sec_H, sec_i));
        xsection = add_xsection(sigmahE, 0.0, P_hEXCHANGE);
        reaction = new Reaction(gastype, P_hEXCHANGE, H_ion_species[i], CH4_species[j], xsection, mcctype(), true, sec_H2, sec_CH3_ion);
	reaction->add_characteristic_value(get_CXhenergy());
        reactionlist.add(reaction);
      }
    }

    vector<Species *> CH4_ion_species = Species::get_reaction_species_named_with(species_iter, CH4_ion);

    for (int j=CH4_ion_species.size()-1; j>=0; j--)
    {
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaDE3, 5.2, E_DISS_EXCITATION3);
        reaction = new Reaction(gastype, E_DISS_EXCITATION3, E_species[i], CH4_ion_species[j], xsection, mcctype(), true, sec_H, sec_CH3_ion);
	reaction->add_characteristic_value(get_DE3energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDE4, 8.0, E_DISS_EXCITATION4);
        reaction = new Reaction(gastype, E_DISS_EXCITATION4, E_species[i], CH4_ion_species[j], xsection, mcctype(), true, sec_H_ion, sec_CH3);
	reaction->add_characteristic_value(get_DE4energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDE5, 6.5, E_DISS_EXCITATION5);
        reaction = new Reaction(gastype, E_DISS_EXCITATION5, E_species[i], CH4_ion_species[j], xsection, mcctype(), true, sec_H2, sec_CH2_ion);
	reaction->add_characteristic_value(get_DE5energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDR1, 0., E_DISS_RECOMBINATION1);
        reaction = new Reaction(gastype, E_DISS_RECOMBINATION1, E_species[i], CH4_ion_species[j], xsection, mcctype(), true, sec_H, sec_CH3);
	reaction->add_characteristic_value(get_DR1energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDR2, 0., E_DISS_RECOMBINATION2);
        reaction = new Reaction(gastype, E_DISS_RECOMBINATION2, E_species[i], CH4_ion_species[j], xsection, mcctype(), true, sec_H2, sec_CH2);
	reaction->add_characteristic_value(get_DR2energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDR3, 0., E_DISS_RECOMBINATION3);
        reaction = new Reaction(gastype, E_DISS_RECOMBINATION3, E_species[i], CH4_ion_species[j], xsection, mcctype(), true, sec_H, sec_H2, sec_CH);
	reaction->add_characteristic_value(get_DR3energy());
        reactionlist.add(reaction);
      }
    }
  }


  // Cross sections:
  //-------------------
  static Scalar sigmaI(Scalar energy)
  {
    if (energy <= 12.63) return 0.0 ;
    return (7.9176e-19/energy)*(1.3541*log(energy/12.63) - 1.4665*(1-(12.63/energy))
				+ 1.6787e-1*pow( (1-(12.63/energy)), 2) + 6.1801*pow( (1-(12.63/energy)), 3)
				- 1.5638e1*pow( (1-(12.63/energy)), 4)  + 1.0767e1*pow( (1-(12.63/energy)), 5));
  }
  static Scalar sigmaDI(Scalar energy)
  {
    if (energy <= 14.25) return 0.0 ;
    return  (7.1377e-19/energy)*(1.6074*log(energy/14.01)  - 1.4713*(1-(14.01/energy))
				 -2.7386e-1*pow( (1-(14.01/energy)), 2)  + 1.9556e-1*pow( (1-(14.01/energy)), 3)
				 + 1.1343e-1*pow( (1-(14.01/energy)), 4) + 9.0166e-3*pow( (1-(14.01/energy)), 5));
  }
  static Scalar sigmaDE1(Scalar energy)
  {
    if (energy <= 8.8) return 0.0;
    return 5.679e-19*(pow((1- (8.8/energy)), 3)/energy)*log(2.71828 + 0.15*energy);
  }
  static Scalar sigmaDE2(Scalar energy)
  {
    if(energy <= 9.4) return 0;
    return 1.076e-19*(pow((1- (9.4/energy)), 3)/energy)*log(2.71828 + 0.15*energy);
  }
  static Scalar sigmaeE(Scalar energy) // energy assumed >=0.05 eV
  {
    return 1e-20*(3.93/(pow(energy, 0.5)+445.0*pow(energy, 2.3))
		  + 46.2/(pow(energy, 0.094) + 9e-6*pow(energy, 1.2)
			  + 2.845e-18*pow(energy, 3.8) + 5.81e-22*pow(energy, 4.4)));
  }
  static Scalar sigmahE(Scalar energy)
  {
    return 1.65e-19/(pow(energy, 0.5) + 0.5*pow(energy, 2.5));
  }
  static Scalar sigmaDR1(Scalar energy)
  {
    if (energy <= 1e-4) return 6.30e-16;
    return 0.63e-20/(pow(energy,1.25)*(1+0.1*energy));
  }
  static Scalar sigmaDR2(Scalar energy)
  {
    if (energy <= 1e-4) return 1.29e-15;
    return 1.29e-20/(pow(energy,1.25)*(1+0.1*energy));
  }
  static Scalar sigmaDR3(Scalar energy)
  {
    if (energy <= 1e-4) return 7.49e-16;
    return 0.75e-20/(pow(energy,1.25)*(1+0.1*energy));
  }
  static Scalar sigmaDE3(Scalar energy)
  {
       if(energy <= 5.2) return 0.0;
     return 3.313e-19*pow(1-(5.2/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
  }
  static Scalar sigmaDE4(Scalar energy)
  {
      if(energy <= 8.0) return 0.0;
      return 2.897e-19*pow(1-(8.0/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
  }
  static Scalar sigmaDE5(Scalar energy)
  {
     if (energy <= 6.5) return 0.0;
     return 1.288e-19*pow(1-(6.5/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
  }
private:
  // Functions needed for reaction kinetics:
  //------------------
  Scalar get_DIeEnergyloss() const {return 17.49;} // If energy loss does not correspond to the threshold

  Scalar get_DE1energy() const { return 4.4; }
  Scalar get_DE2energy () const { return 4.7;}
  Scalar get_DIenergy() const { return 3.2;}
  Scalar get_CXhenergy()  const {return 2.96;}

  Scalar get_DR1energy() const {return 8.17;}
  Scalar get_DR2energy() const {return 7.83;}
  Scalar get_DR3energy() const {return 3.42;}
  Scalar get_DE3energy() const {return 3.6;}
  Scalar get_DE4energy() const {return 2.6;}
  Scalar get_DE5energy() const {return 4.0;}
};

class CH3_Gas: public Gas
{
public:
  CH3_Gas(oopicListIter<Species> species_iter, Species *sec_e, Species *sec_i,
	  Species *sec_H, Species *sec_H_ion, Species *sec_H2, Species *sec_C,
	  Species *sec_CH, Species *sec_CH_ion,
	  Species *sec_CH2, Species *sec_CH2_ion,
	  bool no_default_secondary_species_flag)
  {
    mcctype = "hc_mcc";
    gastype = CH3_GAS;

    vector<Species *> E_species = Species::get_reaction_species_named_with(species_iter, E);
    vector<Species *> CH3_species = Species::get_reaction_species_named_with(species_iter, CH3);
    vector<Species *> H_ion_species = Species::get_reaction_species_named_with(species_iter, H_ion);

    if (!no_default_secondary_species_flag)
    {
      if (!sec_e && !E_species.empty()) sec_e = E_species[0];
      if (!sec_i) sec_i = Species::get_species_named_with(species_iter, CH3_ion);
      if (!sec_H_ion && !H_ion_species.empty()) sec_H_ion = H_ion_species[0];
      if (!sec_H) sec_H = Species::get_species_named_with(species_iter, H);
      if (!sec_H2) sec_H2 = Species::get_species_named_with(species_iter, H2);
      if (!sec_C) sec_C  = Species::get_species_named_with(species_iter, C);
      if (!sec_CH) sec_CH = Species::get_species_named_with(species_iter, CH);
      if (!sec_CH_ion) sec_CH_ion  = Species::get_species_named_with(species_iter, CH_ion);
      if (!sec_CH2) sec_CH2 = Species::get_species_named_with(species_iter, CH2);
      if (!sec_CH2_ion) sec_CH2_ion = Species::get_species_named_with(species_iter, CH2_ion);
    }

    for (int j=CH3_species.size()-1; j>=0; j--)
    {
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaI, 9.84, E_IONIZATION);
        reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], CH3_species[j], xsection, mcctype(), true, sec_e, sec_i));
        xsection = add_xsection(sigmaDI, 15.12, E_DISS_IONIZATION);
        reaction = new Reaction(gastype, E_DISS_IONIZATION, E_species[i], CH3_species[j], xsection, mcctype(), true, sec_e, sec_H, sec_CH2_ion);
	reaction->add_characteristic_value(get_DIeEnergyloss());
	reaction->add_characteristic_value(get_DIenergy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDE1, 9.5, E_DISS_EXCITATION1);
        reaction = new Reaction(gastype, E_DISS_EXCITATION1, E_species[i], CH3_species[j], xsection, mcctype(), true, sec_H, sec_CH2);
	reaction->add_characteristic_value(get_DE1energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDE2, 10.0, E_DISS_EXCITATION2);
        reaction = new Reaction(gastype, E_DISS_EXCITATION2, E_species[i], CH3_species[j], xsection, mcctype(), true, sec_H2, sec_CH);
	reaction->add_characteristic_value(get_DE2energy());
        reactionlist.add(reaction);
      }
      for (int i=H_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaeE, 0.0, P_eEXCHANGE);
        reactionlist.add(new Reaction(gastype, P_eEXCHANGE, H_ion_species[i], CH3_species[j], xsection, mcctype(), true, sec_H, sec_i));
      }
    }

    vector<Species *> CH3_ion_species = Species::get_reaction_species_named_with(species_iter, CH3_ion);

    for (int j=CH3_ion_species.size()-1; j>=0; j--)
    {
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaDE3, 10.6, E_DISS_EXCITATION3);
        reaction = new Reaction(gastype, E_DISS_EXCITATION3, E_species[i], CH3_ion_species[j], xsection, mcctype(), true, sec_H, sec_CH2_ion);
	reaction->add_characteristic_value(get_DE3energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDE4, 11.0, E_DISS_EXCITATION4);
        reaction = new Reaction(gastype, E_DISS_EXCITATION4, E_species[i], CH3_ion_species[j], xsection, mcctype(), true, sec_H_ion, sec_CH2);
	reaction->add_characteristic_value(get_DE4energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDE5, 12.0, E_DISS_EXCITATION5);
        reaction = new Reaction(gastype, E_DISS_EXCITATION5, E_species[i], CH3_ion_species[j], xsection, mcctype(), true, sec_H2, sec_CH_ion);
	reaction->add_characteristic_value(get_DE5energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDR1, 0., E_DISS_RECOMBINATION1);
        reaction = new Reaction(gastype, E_DISS_RECOMBINATION1, E_species[i], CH3_ion_species[j], xsection, mcctype(), true, sec_H, sec_CH2);
	reaction->add_characteristic_value(get_DR1energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDR2, 0., E_DISS_RECOMBINATION2);
        reaction = new Reaction(gastype, E_DISS_RECOMBINATION2, E_species[i], CH3_ion_species[j], xsection, mcctype(), true, sec_H2, sec_CH);
	reaction->add_characteristic_value(get_DR2energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDR3, 0., E_DISS_RECOMBINATION3);
        reaction = new Reaction(gastype, E_DISS_RECOMBINATION3, E_species[i], CH3_ion_species[j], xsection, mcctype(), true, sec_H, sec_H2, sec_C);
	reaction->add_characteristic_value(get_DR3energy());
        reactionlist.add(reaction);
      }
    }
  }

  // Cross sections
  //------------------
  static Scalar sigmaI(Scalar energy)
  {
    if (energy <= 9.84) return 0.0;
    return  (1.0204e-18/energy)*(1.9725*log(energy/9.8) - 2.1011*(1-(9.8/energy))
				 + 1.0593*pow( (1-(9.8/energy)), 2)- 6.3438*pow( (1-(9.8/energy)), 3)
				 + 8.0140*pow( (1-(9.8/energy)), 4) -4.244 *pow( (1-(9.8/energy)), 5));
  }
  static Scalar sigmaDI(Scalar energy)
  {
    if (energy <= 15.12) return 0.0;
    return  (7.1428e-19/energy)*(1.2824*log(energy/14.0) -1.3906*(1-(14.0/energy))
				 +6.2993e-1*pow( (1-(14.0/energy)), 2) + 9.4521e-1*pow( (1-(14.0/energy)), 3)
				 -5.3629*pow( (1-(14.0/energy)), 4) +4.3087*pow( (1-(14.0/energy)), 5));
  }
  static Scalar sigmaDE1(Scalar energy)
  {
    if (energy <= 9.5) return 0;
    return 5.37e-19*(pow((1- (9.5/energy)), 3)/energy)*log(2.71828 + 0.15*energy);
  }
  static Scalar sigmaDE2(Scalar energy)
  {
    if(energy <= 10) return 0;
    return 9.058e-19*(pow((1- (10.0/energy)), 3)/energy)*log(2.71828 + 0.15*energy);
  }
  static Scalar sigmaeE(Scalar energy) // energy assumed >=0.05 eV
  {
    return 1e-20*(17.0/(pow(energy, 0.5)+385.0*pow(energy, 2.5))
		  + 51.3/(pow(energy, 0.096) + 2e-9*pow(energy, 2.0)
			  + 5.5e-21*pow(energy, 4.3) ));
  }

  static Scalar sigmaDR1(Scalar energy)
  {
    if (energy <= 1e-4) return 4.82e-16;
    return 1.92e-20/(pow(energy,1.1)*pow((1+0.8*energy),0.5));
  }
  static Scalar sigmaDR2(Scalar energy)
  {
    if (energy <= 1e-4) return 1.90e-16;
    return 0.76e-20/(pow(energy,1.1)*pow((1+0.8*energy),0.5));
  }
  static Scalar sigmaDR3(Scalar energy)
  {
    if (energy <= 1e-4) return 3.61e-16;
    return 1.44e-20/(pow(energy,1.1)*pow((1+0.8*energy),0.5));
  }
   static Scalar sigmaDE3(Scalar energy)
   {
     if(energy <= 10.6) return 0.0;
     return 1.821e-19*pow(1-(10.6/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
   }
  static Scalar sigmaDE4(Scalar energy)
    {
      if(energy <= 11.0) return 0.0;
      return 3.664e-19*pow(1-(11.0/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
    }
   static Scalar sigmaDE5(Scalar energy)
   {
     if (energy <= 12.0) return 0.0;
     return 8.893e-20*pow(1-(12.0/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
   }
private:
  // Functions needed for reaction kinetics:
  //------------------
  Scalar get_DIeEnergyloss() const {return 20.4;}

  Scalar get_DE1energy() const { return 4.7; }
  Scalar get_DE2energy () const { return 5.5;}
  Scalar get_DIenergy() const { return 5.3;}

  Scalar get_DR1energy() const {return 4.97;}
  Scalar get_DR2energy() const {return 5.1;}
  Scalar get_DR3energy() const {return 1.57;}
  Scalar get_DE3energy() const {return 5.3;}
  Scalar get_DE4energy() const {return 2.5;}
  Scalar get_DE5energy() const {return 6.1;}
};

class CH2_Gas: public Gas
{
public:
  CH2_Gas(oopicListIter<Species> species_iter, Species *sec_e, Species *sec_i,
	  Species *sec_H, Species *sec_H_ion, Species *sec_H2, Species *sec_C,
	  Species *sec_CH, Species *sec_CH_ion,
	  bool no_default_secondary_species_flag)
  {
    mcctype = "hc_mcc";
    gastype = CH2_GAS;

    vector<Species *> E_species = Species::get_reaction_species_named_with(species_iter, E);
    vector<Species *> CH2_species = Species::get_reaction_species_named_with(species_iter, CH2);
    vector<Species *> H_ion_species = Species::get_reaction_species_named_with(species_iter, H_ion);

    if (!no_default_secondary_species_flag)
    {
      if (!sec_e && !E_species.empty()) sec_e = E_species[0];
      if (!sec_i) sec_i = Species::get_species_named_with(species_iter, CH2_ion);
      if (!sec_H_ion && !H_ion_species.empty()) sec_H_ion = H_ion_species[0];
      if (!sec_H) sec_H = Species::get_species_named_with(species_iter, H);
      if (!sec_H2) sec_H2 = Species::get_species_named_with(species_iter, H2);
      if (!sec_CH) sec_CH = Species::get_species_named_with(species_iter, CH);
      if (!sec_CH_ion) sec_CH_ion  = Species::get_species_named_with(species_iter, CH_ion);
      if (!sec_C) sec_C  = Species::get_species_named_with(species_iter, C);
    }

    for (int j=CH2_species.size()-1; j>=0; j--)
    {
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaI, 10.4, E_IONIZATION);
        reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], CH2_species[j], xsection, mcctype(), true, sec_e, sec_i));
        xsection = add_xsection(sigmaDI, 15.53, E_DISS_IONIZATION);
        reaction = new Reaction(gastype, E_DISS_IONIZATION, E_species[i], CH2_species[j], xsection, mcctype(), true, sec_e, sec_H, sec_CH_ion);
	reaction->add_characteristic_value(get_DIeEnergyloss());
	reaction->add_characteristic_value(get_DIenergy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDE1, 8.5, E_DISS_EXCITATION1);
        reaction = new Reaction(gastype, E_DISS_EXCITATION1, E_species[i], CH2_species[j], xsection, mcctype(), true, sec_H, sec_CH);
	reaction->add_characteristic_value(get_DE1energy());
        reactionlist.add(reaction);
      }
      for (int i=H_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaeE, 0.0, P_eEXCHANGE);
        reactionlist.add(new Reaction(gastype, P_eEXCHANGE, H_ion_species[i], CH2_species[j], xsection, mcctype(), true, sec_H, sec_i));
        xsection = add_xsection(sigmahE, 0.0, P_hEXCHANGE);
        reaction = new Reaction(gastype, P_hEXCHANGE, H_ion_species[i], CH2_species[j], xsection, mcctype(), true, sec_H2, sec_CH_ion);
	reaction->add_characteristic_value(get_CXhenergy());
        reactionlist.add(reaction);
      }
    }

    vector<Species *> CH2_ion_species = Species::get_reaction_species_named_with(species_iter, CH2_ion);

    for (int j=CH2_ion_species.size()-1; j>=0; j--)
    {
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaDE3, 12.0, E_DISS_EXCITATION3);
        reaction = new Reaction(gastype, E_DISS_EXCITATION3, E_species[i], CH2_ion_species[j], xsection, mcctype(), true, sec_H, sec_CH_ion);
	reaction->add_characteristic_value(get_DE3energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDE4, 9.0, E_DISS_EXCITATION4);
        reaction = new Reaction(gastype, E_DISS_EXCITATION4, E_species[i], CH2_ion_species[j], xsection, mcctype(), true, sec_H_ion, sec_CH);
	reaction->add_characteristic_value(get_DE4energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDR1, 0., E_DISS_RECOMBINATION1);
        reaction = new Reaction(gastype, E_DISS_RECOMBINATION1, E_species[i], CH2_ion_species[j], xsection, mcctype(), true, sec_H, sec_CH);
	reaction->add_characteristic_value(get_DR1energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDR2, 0., E_DISS_RECOMBINATION2);
        reaction = new Reaction(gastype, E_DISS_RECOMBINATION2, E_species[i], CH2_ion_species[j], xsection, mcctype(), true, sec_H2, sec_C);
	reaction->add_characteristic_value(get_DR2energy());
        reactionlist.add(reaction);
      }
    }
  }
  //   Cross sections
  //------------------
  static Scalar sigmaI(Scalar energy)
  {
    if (energy <= 10.4) return 0.0 ;
    return  (9.6153e-19/energy)*(1.7159*log(energy/10.4) - 1.7164*(1-(10.4/energy))
				 -6.5529e-1*pow( (1-(10.4/energy)), 2) + 2.1724*pow( (1-(10.4/energy)), 3)
				 -5.4186*pow( (1-(10.4/energy)), 4) + 3.1616*pow( (1-(10.4/energy)), 5));
  }
  static Scalar sigmaDI(Scalar energy)
  {
    if (energy <= 15.53) return 0.0 ;
    return  (6.4391e-19/energy)*(8.1919e-1*log(energy/15.53) - 7.5016e-1*(1-(15.53/energy))
				 - 3.8063e-3*pow((1-(15.53/energy)), 2) + 1.4065*pow( (1-(15.53/energy)), 3)
				 - 3.6447*pow( (1-(15.53/energy)), 4) + 2.6220*pow( (1-(15.53/energy)), 5));
  }
  static Scalar sigmaDE1(Scalar energy)
  {
    if (energy <= 8.5) return 0;
    return 4.920e-19*(pow((1- (8.5/energy)), 3)/energy)*log(2.71828 + 0.15*energy);
  }

  static Scalar sigmaeE(Scalar energy) // energy assumed >=0.05 eV
  {
    return 1e-20*(7.32/(pow(energy, 0.5)+ 0.005*pow(energy, 3.0))
		  + 20.95*exp((-1.55)/pow(energy, 0.57))/(1.0 + 2.35e-7*pow(energy, 1.55)
			  + 5.86e-21*pow(energy, 4.26)  ));
  }

  static Scalar sigmahE(Scalar energy)
  {
    return 1.301e-19/(pow(energy, 0.5) + 0.5*pow(energy, 2.5));
  }
  // Cross sections
  //------------------

  static Scalar sigmaDR1(Scalar energy)
  {
    if (energy <= 1e-4) return 6.65e-16;
    return 1.67e-20/(pow(energy,1.15)*pow((1+1.2*energy),0.5));
     }
  static Scalar sigmaDR2(Scalar energy)
  {
    if (energy <= 1e-4) return 1.68e-15 ;
    return 4.22e-20/(pow(energy,1.15)*pow((1+1.2*energy),0.5));
  }
  static Scalar sigmaDE3(Scalar energy)
   {
     if(energy <= 12.0) return 0.0;
     return 9.803e-20*pow(1-(12.0/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
   }
  static Scalar sigmaDE4(Scalar energy)
    {
      if(energy <= 9.0) return 0.0;
      return 3.393e-19*pow(1-(9.0/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
    }
private:
  // Functions needed for reaction kinetics:
  //------------------
  Scalar get_DIeEnergyloss() const {return 20.4;}

  Scalar get_DE1energy() const { return 4.25; }
  Scalar get_DIenergy() const { return 5.0;}
  Scalar get_CXhenergy()  const {return 5.17;}

  // Functions needed for reaction kinetics:
  //------------------
  Scalar get_DR1energy() const {return 6.0;}
  Scalar get_DR2energy() const {return 7.0;}
  Scalar get_DE3energy() const {return 7.0;}
  Scalar get_DE4energy() const {return 2.4;}
};

class CH_Gas: public Gas
{
public:
  CH_Gas(oopicListIter<Species> species_iter, Species *sec_e, Species *sec_i,
	  Species *sec_H, Species *sec_H_ion, Species *sec_H2, Species *sec_C,
	  Species *sec_C_ion,
	  bool no_default_secondary_species_flag)
  {
    mcctype = "hc_mcc";
    gastype = CH_GAS;

    vector<Species *> E_species = Species::get_reaction_species_named_with(species_iter, E);
    vector<Species *> CH_species = Species::get_reaction_species_named_with(species_iter, CH);
    vector<Species *> H_ion_species = Species::get_reaction_species_named_with(species_iter, H_ion);

    if (!no_default_secondary_species_flag)
    {
      if (!sec_e && !E_species.empty()) sec_e = E_species[0];
      if (!sec_i) sec_i = Species::get_species_named_with(species_iter, CH_ion);
      if (!sec_H_ion && !H_ion_species.empty()) sec_H_ion = H_ion_species[0];
      if (!sec_H) sec_H = Species::get_species_named_with(species_iter, H);
      if (!sec_H2) sec_H2 = Species::get_species_named_with(species_iter, H2);
      if (!sec_C) sec_C  = Species::get_species_named_with(species_iter, C);
      if (!sec_C_ion) sec_C_ion  = Species::get_species_named_with(species_iter, C_ion);
    }

    for (int j=CH_species.size()-1; j>=0; j--)
    {
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaI, 11.13, E_IONIZATION);
        reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], CH_species[j], xsection, mcctype(), true, sec_e, sec_i));
        xsection = add_xsection(sigmaDI, 14.74, E_DISS_IONIZATION);
        reaction = new Reaction(gastype, E_DISS_IONIZATION, E_species[i], CH_species[j], xsection, mcctype(), true, sec_e, sec_H, sec_C_ion);
	reaction->add_characteristic_value(get_DIeEnergyloss());
	reaction->add_characteristic_value(get_DIenergy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDE1, 7.0, E_DISS_EXCITATION1);
        reaction = new Reaction(gastype, E_DISS_EXCITATION1, E_species[i], CH_species[j], xsection, mcctype(), true, sec_H, sec_C);
	reaction->add_characteristic_value(get_DE1energy());
        reactionlist.add(reaction);
      }
      for (int i=H_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaeE, 0.0, P_eEXCHANGE);
        reactionlist.add(new Reaction(gastype, P_eEXCHANGE, H_ion_species[i], CH_species[j], xsection, mcctype(), true, sec_H, sec_i));
        xsection = add_xsection(sigmahE, 0.0, P_hEXCHANGE);
        reaction = new Reaction(gastype, P_hEXCHANGE, H_ion_species[i], CH_species[j], xsection, mcctype(), true, sec_H2, sec_C_ion);
	reaction->add_characteristic_value(get_CXhenergy());
        reactionlist.add(reaction);
      }
    }

    vector<Species *> CH_ion_species = Species::get_reaction_species_named_with(species_iter, CH_ion);

    for (int j=CH_ion_species.size()-1; j>=0; j--)
    {
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaDE3, 2.5, E_DISS_EXCITATION3);
        reaction = new Reaction(gastype, E_DISS_EXCITATION3, E_species[i], CH_ion_species[j], xsection, mcctype(), true, sec_H, sec_C_ion);
	reaction->add_characteristic_value(get_DE3energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDE4, 5.0, E_DISS_EXCITATION4);
        reaction = new Reaction(gastype, E_DISS_EXCITATION4, E_species[i], CH_ion_species[j], xsection, mcctype(), true, sec_H_ion, sec_C);
	reaction->add_characteristic_value(get_DE4energy());
        reactionlist.add(reaction);
        xsection = add_xsection(sigmaDR1, 0., E_DISS_RECOMBINATION1);
        reaction = new Reaction(gastype, E_DISS_RECOMBINATION1, E_species[i], CH_ion_species[j], xsection, mcctype(), true, sec_H, sec_C);
	reaction->add_characteristic_value(get_DR1energy());
        reactionlist.add(reaction);
      }
    }
  }
  // Cross sections
  //------------------
  static Scalar sigmaI(Scalar energy)
  {
    if (energy <= 11.13) return 0.0 ;
    return  (8.9847e-19/energy)*(1.4439*log(energy/11.3) - 1.2724*(1-(11.3/energy))
				 - 2.2221*pow( (1-(11.3/energy)), 2) + 9.2822*pow( (1-(11.3/energy)), 3)
				 - 1.5506e1*pow( (1-(11.3/energy)), 4) + 8.2778*pow( (1-(11.3/energy)), 5));
  }
  static Scalar sigmaDI(Scalar energy)
  {
    if (energy <= 14.80) return 0.0 ; // indicated to be 14.74, changed to 14.8 to avoid negative crossx
    return  (6.7567e-19/energy)*(4.3045e-1*log(energy/14.8) - 4.1305e-1*(1-(14.8/energy))
				 -5.6881e-1*pow( (1-(14.8/energy)), 2) + 3.2957*pow( (1-(14.8/energy)), 3)
				 - 5.6549*pow( (1-(14.8/energy)), 4) + 3.4295*pow( (1-(14.8/energy)), 5));
  }
  static Scalar sigmaDE1(Scalar energy)
  {
    if (energy <= 7.0) return 0;
    return 4.463e-19*(pow((1- (7.0/energy)), 3)/energy)*log(2.71828 + 0.15*energy);
  }

  static Scalar sigmaeE(Scalar energy) // energy assumed >=0.05 eV
  {
    return 1e-20*(4.28/(pow(energy, 0.5)+ 0.001*pow(energy, 3.0))
		  + 20.2*exp((-5.3)/pow(energy, 0.35))/(1.0 + 1.12e-6*pow(energy, 1.45)
			  + 1.1e-20*pow(energy, 4.3)  ));
  }

  static Scalar sigmahE(Scalar energy)
  {
    return 9.517e-20/(pow(energy, 0.5) + 0.01*pow(energy, 3.5));
  }
  // Cross sections
  //------------------
  static Scalar sigmaDR1(Scalar energy)
  {
    if (energy <= 1e-4) return 3.15e-17;
    return 3.16e-20/(pow(energy,0.75)*(1+1.13*energy));
  }
  static Scalar sigmaDE3(Scalar energy)
   {
     if(energy <= 2.5) return 0.0;
     return 2.06e-19*pow(1-(2.5/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
   }
  static Scalar sigmaDE4(Scalar energy)
  {
    if(energy <= 5.0) return 0.0;
    return 2.675e-19*pow(1-(5.0/energy), 2.5)*log(2.71828 + 0.9*energy)/energy;
  }
private:
  Scalar get_DR1energy() const {return 7.18;}
  Scalar get_DE3energy() const {return 4.0;}
  Scalar get_DE4energy() const {return  2.0;}
  Scalar get_DIeEnergyloss() const {return 18.35;}

  Scalar get_DE1energy() const  {return 3.5;}
  Scalar get_DIenergy() const {return 4.31;}
  Scalar get_CXhenergy()  const {return 5.28;}
};

class Carbon_Gas: public Gas
{
public:
  Carbon_Gas(oopicListIter<Species> species_iter, Species *sec_e, Species *sec_i,
	  Species *sec_H,
	  bool no_default_secondary_species_flag)
  {
    mcctype = "hc_mcc";
    gastype = CARBON_GAS;

    vector<Species *> E_species = Species::get_reaction_species_named_with(species_iter, E);
    vector<Species *> C_species = Species::get_reaction_species_named_with(species_iter, C);
    vector<Species *> H_ion_species = Species::get_reaction_species_named_with(species_iter, H_ion);

    if (!no_default_secondary_species_flag)
    {
      if (!sec_e && !E_species.empty()) sec_e = E_species[0];
      if (!sec_i) sec_i = Species::get_species_named_with(species_iter, C_ion);
      if (!sec_H) sec_H = Species::get_species_named_with(species_iter, H);
    }

    for (int j=C_species.size()-1; j>=0; j--)
    {
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaI, 11.26, E_IONIZATION);
        reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], C_species[j], xsection, mcctype(), true, sec_e, sec_i));
      }
      for (int i=H_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaeE, 0.0, P_eEXCHANGE);
        reactionlist.add(new Reaction(gastype, P_eEXCHANGE, H_ion_species[i], C_species[j], xsection, mcctype(), true, sec_H, sec_i));
      }
    }
  }
  // Cross sections:
  static  Scalar sigmaI(Scalar energy)
  {
    if (energy <= 11.26) return 0.0;
    else return   (8.88E-19/energy)*(2.1143*log(energy/11.26) - 1.9647*(1-(11.26/energy)) - 6.084e-1*pow(1-(11.26/energy), 2));
  }

  static  Scalar sigmaeE(Scalar energy)
  {
    return 1e-20*(2.6838e2*exp((-1.2e-2)*energy)/(1.14e9*pow(energy, -3.02))
		  + 14.2*exp((-6.86e2)/energy)/(1.0 + 1.96e-9*pow(energy, 2.0) + 1.71e-23*pow(energy, 4.5)  ));
  }
};

#endif
