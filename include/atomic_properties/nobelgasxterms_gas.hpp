// Cross terms between nobel gases
// by Jon Tomas Gudmundsson
//  Added He-Ar cross terms JTG July 27 2011
//  Added He-Xe cross terms SAS Summer 2014
/*
 NOTE TO DEVELOPERS: // MAL200117
 A ReactionGroup is a set of reactions having the same reactant[0], reactant[1], and mcc type. The priority in reactants and reactions is meaningful.  The first reactant (reactant[0]) in the first reaction in a ReactionGroup determines the mass used to evaluate the energy for the cross section: energy = 0.5*mass(reactant[0])*(relative velocity)^2. Uncomment #define debugReactionGroup in reaction.hpp to print all ReactionGroups to the terminal during initialization.
 */

#ifndef NOBELGASXTERMS_H
#define NOBELGASXTERMS_H
#include "atomic_properties/gas.hpp"
#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "main/species.hpp"
class Nobelgasxterms_Gas: public Gas
{
public:
  // mindgame: "sec" refers to "new product".
  Nobelgasxterms_Gas(oopicListIter<Species> species_iter, Species *sec_e, Species *sec_i, Species *sec_He, Species *sec_Hem3, Species *sec_Hem1, Species *sec_Ar_ion, Species *sec_Xe, Species *sec_Xe_ion, Species *sec_Ar, Species *sec_He_ion,
bool no_default_secondary_species_flag)
  {
    mcctype = "generic_mcc";
    gastype = NOBELGASXTERMS_GAS;

    // mindgame: find lists of reactants
    vector<Species *> E_species = Species::get_reaction_species_named_with(species_iter, E);
    vector<Species *> Xe_species = Species::get_reaction_species_named_with(species_iter, Xe);
    vector<Species *> Xe_ion_species = Species::get_reaction_species_named_with(species_iter, Xe_ion);
    vector<Species *> Ar_species = Species::get_reaction_species_named_with(species_iter, Ar);
    vector<Species *> Ar_ion_species = Species::get_reaction_species_named_with(species_iter, Ar_ion);
    vector<Species *> He_species = Species::get_reaction_species_named_with(species_iter, He);  // added by JTG July 29 2011
    vector<Species *> He_ion_species = Species::get_reaction_species_named_with(species_iter, He_ion); // added by JTG July 29 2011
    vector<Species *> Hem3_species = Species::get_reaction_species_named_with(species_iter, Hem3); // added by SAS July 4 2014
    vector<Species *> Hem1_species = Species::get_reaction_species_named_with(species_iter, Hem1); // added by SAS July 4 2014

    // mindgame: if empty, assign default secondary species
    if (!no_default_secondary_species_flag)
    {
      if (!sec_e && !E_species.empty()) sec_e = E_species[0];
      if (!sec_i && !Xe_ion_species.empty()) sec_i = Xe_ion_species[0];
      if (!sec_i && !He_ion_species.empty()) sec_i = He_ion_species[0];
      if (!sec_i && !Ar_ion_species.empty()) sec_i = Ar_ion_species[0];
      if (!sec_He && !He_species.empty()) sec_He = He_species[0];
      if (!sec_He_ion && !He_ion_species.empty()) sec_He_ion = He_ion_species[0];
      if (!sec_Ar && !Ar_species.empty()) sec_Ar = Ar_species[0];
      if (!sec_Ar_ion && !Ar_ion_species.empty()) sec_Ar_ion = Ar_ion_species[0];
      if (!sec_Hem3 && !Hem3_species.empty()) sec_Hem3 = Hem3_species[0];
      if (!sec_Hem1 && !Hem1_species.empty()) sec_Hem1 = Hem1_species[0];
      //if (!sec_He) sec_He = Species::get_species_named_with(species_iter, He); // added by JTG July 29 2011
      //if (!sec_Ar_ion) sec_Ar_ion = Species::get_species_named_with(species_iter, Ar_ion); // added by JTG July 29 2011
        if (!sec_Xe_ion) sec_Xe_ion = Species::get_species_named_with(species_iter, Xe_ion); // uncomment; MAL200117
      //  if (!sec_Ar_ion) sec_Ar_ion = Species::get_species_named_with(species_iter, Ar_ion);
        if (!sec_Xe) sec_Xe = Species::get_species_named_with(species_iter, Xe); // uncomment; MAL200117
      //  if (!sec_Ar) sec_Ar = Species::get_species_named_with(species_iter, Ar);
    }


		Reaction * newreaction = NULL;
    for (int j=Xe_species.size()-1; j>=0; j--)
    {
      for (int i=Ar_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaESAX,0.0, ANYMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, ANYMASS_ELASTIC, Ar_ion_species[i], Xe_species[j], xsection, mcctype()));
	      	xsection = add_xsection(sigmaCEAX,0.0, NONRES_Ch_EXCHANGE); // MAL 11/20/11
					newreaction = new Reaction(gastype, NONRES_Ch_EXCHANGE, Ar_ion_species[i], Xe_species[j], xsection, mcctype(),true,sec_Xe_ion, sec_Ar); // MAL 10/7/12
//	        reactionlist.add(new Reaction(gastype, NONRES_Ch_EXCHANGE, Ar_ion_species[i], Xe_species[j], xsection, mcctype(),true,sec_Xe_ion, sec_Ar)); // MAL 11/20/11
					reactionlist.add(newreaction); // MAL 10/7/12
					newreaction -> add_characteristic_value(3.6298); // CM ReleasedEnergy = Eiz(Ar) - Eiz(Xe) for this exothermic reaction, MAL 10/7/12
      }
      for (int i=He_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaESHX,0.0, ANYMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, ANYMASS_ELASTIC, He_ion_species[i], Xe_species[j], xsection, mcctype()));
	      	xsection = add_xsection(sigmaCEHX,0.0, NONRES_Ch_EXCHANGE); // MAL 11/20/11
					newreaction = new Reaction(gastype, NONRES_Ch_EXCHANGE, He_ion_species[i], Xe_species[j], xsection, mcctype(),true,sec_Xe_ion, sec_He); // MAL 10/7/12
//	        reactionlist.add(new Reaction(gastype, NONRES_Ch_EXCHANGE, He_ion_species[i], Xe_species[j], xsection, mcctype(),true,sec_Xe_ion, sec_Ar)); // MAL 11/20/11
					reactionlist.add(newreaction); // MAL 10/7/12
					newreaction -> add_characteristic_value(12.47); // CM ReleasedEnergy = Eiz(He) - Eiz(Xe) for this exothermic reaction, MAL 10/7/12
      }
      for (int i=Ar_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaXeAr,0.0, ANYMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, ANYMASS_ELASTIC, Ar_species[i], Xe_species[j], xsection, mcctype()));
      }
    }
   for (int j=Ar_species.size()-1; j>=0; j--)
    {
      for (int i=Xe_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaESXA,0.0, ANYMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, ANYMASS_ELASTIC, Xe_ion_species[i], Ar_species[j], xsection, mcctype()));
	//		xsection = add_xsection(sigmaCEAX,0.0, Ch_EXCHANGEDM);
	//		reactionlist.add(new Reaction(gastype, Ch_EXCHANGEDM, Ar_ion_species[i], Xe_species[j], xsection, mcctype()));
      }
    }

    for (int j=He_species.size()-1; j>=0; j--)
    {
      for (int i=Ar_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaHeAr,0.0, ANYMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, ANYMASS_ELASTIC, Ar_species[i], He_species[j], xsection, mcctype()));
      }
      for (int i=Xe_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaHeXe,0.0, ANYMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, ANYMASS_ELASTIC, Xe_species[i], He_species[j], xsection, mcctype()));
      }
      for (int i=Xe_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaESXH,0.0, ANYMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, ANYMASS_ELASTIC, Xe_ion_species[i], He_species[j], xsection, mcctype()));
      }

      for (int i=Ar_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaESAH,0.0, ANYMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, ANYMASS_ELASTIC, Ar_ion_species[i], He_species[j], xsection, mcctype()));
      }
    }

    for (int j=He_ion_species.size()-1; j>=0; j--)
    {
      for (int i=Ar_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaESHA,0.0, ANYMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, ANYMASS_ELASTIC, He_ion_species[j], Ar_species[i], xsection, mcctype()));
	xsection = add_xsection(sigmaCEAH,0.0, NONRES_Ch_EXCHANGE); // MAL 11/20/11
        newreaction = new Reaction(gastype, NONRES_Ch_EXCHANGE, He_ion_species[i], Ar_species[j], xsection, mcctype(),true,sec_Ar_ion, sec_He); // MAL 10/7/12
//	        reactionlist.add(new Reaction(gastype, NONRES_Ch_EXCHANGE, Ar_ion_species[i], Xe_species[j], xsection, mcctype(),true,sec_Xe_ion, sec_Ar)); // MAL 11/20/11
					reactionlist.add(newreaction); // MAL 10/7/12
					newreaction -> add_characteristic_value(8.84); // CM ReleasedEnergy = Eiz(He) - Eiz(Ar) for this exothermic reaction, MAL 10/7/12
      }
    }

    for (int j=Hem3_species.size()-1; j>=0; j--)
    {
      for (int i=Ar_species.size()-1; i>=0; i--)
      {
	// ReleasedEnergy = Em - Eiz = 4.08 eV, MAL 1/3/11 (SAS July 4 2014)
        xsection = add_xsection(sigmaHem3ArPenningIz, 0.0, AM_PENNING_IONIZATION); // add, MAL 11/26/10 (SAS July 4 2014)
        newreaction = new Reaction(gastype, AM_PENNING_IONIZATION, Ar_species[i], Hem3_species[j], xsection, mcctype(), true, sec_e, sec_i, sec_He);
	                        reactionlist.add(newreaction);
				newreaction->add_characteristic_value(4.08);
      }
      for (int i=Xe_species.size()-1; i>=0; i--)
      {
	// ReleasedEnergy = Em - Eiz = 4.08 eV, MAL 1/3/11 (SAS August 4 2014)
        xsection = add_xsection(sigmaHem3XePenningIz, 0.0, AM_PENNING_IONIZATION); // add, MAL 11/26/10 (SAS July 4 2014)
        newreaction = new Reaction(gastype, AM_PENNING_IONIZATION, Xe_species[i], Hem3_species[j], xsection, mcctype(), true, sec_e, sec_i, sec_He);
	                        reactionlist.add(newreaction);
				newreaction->add_characteristic_value(7.71);
      }
    }

    for (int j=Hem1_species.size()-1; j>=0; j--)
    {
      for (int i=Ar_species.size()-1; i>=0; i--)
      {
	// ReleasedEnergy = Em - Eiz = 4.90 eV, MAL 1/3/11 (SAS July 4 2014)
        xsection = add_xsection(sigmaHem1ArPenningIz, 0.0, AM_PENNING_IONIZATION); // add, MAL 11/26/10 (SAS July 4 2014)
        newreaction = new Reaction(gastype, AM_PENNING_IONIZATION, Ar_species[i], Hem1_species[j], xsection, mcctype(), true, sec_e, sec_i, sec_He);
				reactionlist.add(newreaction);
				newreaction->add_characteristic_value(4.90);
      }
      for (int i=Xe_species.size()-1; i>=0; i--)
      {
	// ReleasedEnergy = Em - Eiz = 4.08 eV, MAL 1/3/11 (SAS August 4 2014)
        xsection = add_xsection(sigmaHem1XePenningIz, 0.0, AM_PENNING_IONIZATION); // add, MAL 11/26/10 (SAS July 4 2014)
        newreaction = new Reaction(gastype, AM_PENNING_IONIZATION, Xe_species[i], Hem1_species[j], xsection, mcctype(), true, sec_e, sec_i, sec_He);
	                        reactionlist.add(newreaction);
				newreaction->add_characteristic_value(8.53);
      }
    }


    // Add ion-ion Coulomb collisions, MAL 11/25/11 (SAS July 4 2014)
    for (int j=Xe_ion_species.size()-1; j>=0; j--)
    {
      for (int i=Ar_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaCoulombArXe,0.0, ANYMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, ANYMASS_ELASTIC, Ar_ion_species[i], Xe_ion_species[j], xsection, mcctype()));
      }

      for (int i=He_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaCoulombHeXe,0.0, ANYMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, ANYMASS_ELASTIC, He_ion_species[i], Xe_ion_species[j], xsection, mcctype()));
      }
    }

    // Add ion-ion Coulomb collisions, MAL 11/25/11 (SAS July 4 2014)
    for (int j=Ar_ion_species.size()-1; j>=0; j--)
    {
      for (int i=He_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaCoulombArHe,0.0, ANYMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, ANYMASS_ELASTIC, He_ion_species[i], Ar_ion_species[j], xsection, mcctype()));
      }
    }
  }



static Scalar sigmaCEAX(Scalar energy)
//  Ar+ + Xe -> Ar + Xe+
//  We use the the same cross section as for
//  Xe+ + Xe -> Xe + Xe+ which is taken from a
//   a fit given by Miller et al JAP 92 (2002) 984
//  however we scale it down by a factor of 2.04e-3 to match shul87
//  JTG November 21 2009
    {
      return((87.3 - 13.6 * (log10(energy)))*2.04e-23);
    }



static Scalar sigmaESAX(Scalar energy)
//*---------------------------------------------------------------------------*/
//*  Ar+ + Xe -> Ar+ + Xe   Elastic  Scattering                               */
//*---------------------------------------------------------------------------*/
// Elastic Scattering:
//  We assume the scattering cross section for Ar+ + Xe is the same as for Ar+ + Ar
//  which is a fit to the isotropic cross section given by
//   McDaniel "Collision Phenomena in Ionised Gases" (1964)
// W. H. Cramer JCP (1959) 641  4 - 400 eV
//
  {
    return (pow(10.0,( - 0.1568 * (log10(energy))  - 18.3301)));
  }



static Scalar sigmaESXA(Scalar energy)
//*---------------------------------------------------------------------------*/
//*  Xe+ + Ar -> Xe+  + Ar  Elastic  Scattering                               */
//*---------------------------------------------------------------------------*/
// Elastic Scattering:
//   McDaniel "Collision Phenomena in Ionised Gases" (1964)
// We assume the scattering cross section for Xe+ + Ar is the same as for Ar+ + Ar
// W. H. Cramer JCP (1959) 641  4 - 400 eV
  {
    return (pow(10.0,( - 0.1568 * (log10(energy))  - 18.3301)));
  }



static Scalar sigmaESHX(Scalar energy)
//*---------------------------------------------------------------------------*/
//*  He+ + Xe -> He+ + Xe   Elastic  Scattering                               */
//*---------------------------------------------------------------------------*/
//
// Same as Xe+ + Xe -> Xe+ + Xe
//
// SAS August 5, 2014
  {
    return (pow(10.0,(-0.0102 * (log10(energy)) * (log10(energy)) * (log10(energy)) + 0.0415 * (log10(energy)) * (log10(energy)) - 0.1105 * (log10(energy))  - 17.8044)));
  }



static Scalar sigmaESXH(Scalar energy)
//*---------------------------------------------------------------------------*/
//*  Xe+ + He -> Xe+  + He  Elastic  Scattering                               */
//*---------------------------------------------------------------------------*/
//
// Same as He+ + He -> He+ + He CRAMER57
//
// SAS August 5, 2014
  {
        return(pow(10.0,(-0.0089-(0.0089*energy+0.0011)/(1+energy))*log10(energy)*log10(energy)*log10(energy)-(0.2392+(0.2392*energy+0.0655)/(1+energy))*log10(energy)-(9.2435+(9.2435*energy+9.2855)/(1+energy))));
  }



static Scalar sigmaXeAr(Scalar energy)
//*---------------------------------------------------------------------------*/
//*  Xe + Ar -> Xe + Ar  Elastic  Scattering                               */
//*---------------------------------------------------------------------------*/
  {
    return(((pow(10.0,(-0.0021 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy)) + 0.0081 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))+0.0184 * (log10(energy)) * (log10(energy)) * (log10(energy)) -0.0480 * (log10(energy)) * (log10(energy)) - 0.2599 * (log10(energy))  - 17.7461))) + (pow(10.0,(0.0040 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))-0.0113 * (log10(energy)) * (log10(energy)) * (log10(energy)) -0.0248 * (log10(energy)) * (log10(energy)) - 0.1678 * (log10(energy))  - 17.3759))))/2);
  }


static Scalar sigmaHeAr(Scalar energy)
//*---------------------------------------------------------------------------*/
//*  He + Ar -> He + Ar  Elastic  Scattering                               */
//*---------------------------------------------------------------------------*/
//
// The data is taken form the Phelps data set
// http://jila.colorado.edu/~avp/collision_data/neutralneutral/atomatom.txt
//
//  JTG July 27 2011
  {
    return(pow(10.0,(-0.0108 * (log10(energy)) * (log10(energy)) * (log10(energy)) + 0.0439 * (log10(energy)) * (log10(energy)) -0.1167 * (log10(energy)) - 18.3270)));
  }



static Scalar sigmaHeXe(Scalar energy)
//*---------------------------------------------------------------------------*/
//*  He + Xe -> He + Xe  Elastic  Scattering                               */
//*---------------------------------------------------------------------------*/
// The average of the cross sections for Xe-Xe and He-He scattering
  {
  return ((pow(10.0,(0.0040 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))-0.0113 * (log10(energy)) * (log10(energy)) * (log10(energy)) -0.0248 * (log10(energy)) * (log10(energy)) - 0.1678 * (log10(energy))  - 17.3759))+pow(10.0,-0.0046*log10(energy)*log10(energy)*log(energy)-0.0687*log10(energy)-18.5799))/2);
  }



static Scalar sigmaESHA(Scalar energy)
//*---------------------------------------------------------------------------*/
//*  He+ + Ar -> He+  + Ar  Elastic  Scattering                               */
//*---------------------------------------------------------------------------*/
//
// The data is taken form the Phelps data set
// http://jila.colorado.edu/~avp/collision_data/ionneutral/IONATOM.TXT
//
//  JTG July 27 2011
//

  {
    return(pow(10.0,(-0.0135 * (log10(energy)) * (log10(energy)) -0.3260 * (log10(energy)) - 17.3256)));
  }


static Scalar sigmaESAH(Scalar energy)
//*---------------------------------------------------------------------------*/
//*  Ar+ + He -> Ar+ + He   Elastic  Scattering                               */
//*---------------------------------------------------------------------------*/
// Elastic Scattering:
//
// The data is taken form the Phlps data set
// http://jila.colorado.edu/~avp/collision_data/ionneutral/IONATOM.TXT
//
 //  JTG July 27 2011
  {
  return (pow(10.0,( 0.0347 * (log10(energy)) * (log10(energy)) - 0.3391 * (log10(energy))  - 18.0053)));
  }


static Scalar sigmaCEAH(Scalar energy)
//  He+ + Ar -> He + Ar+
//
//  The average of the cross section for the Ar+ + Ar
//  and He+ + He charge exchange
//
//  Argon:
//  W. H. Cramer JCP (1959) 641  4 - 400 eV
//   Hegerberg & Elford, J.Phys.B  797 (1982)  0.1 - 0.7 eV
//  JTG July 27 2011
  {
    return((pow(10.0,(-0.0011-(0.0011*energy+0.0089)/(1+energy))*log10(energy)*log10(energy)*log10(energy)-(0.0655+(0.0655*energy+0.2392)/(1+energy))*log10(energy)-(9.2855+(9.2855*energy+9.2435)/(1+energy)))+(pow(10.0,(-0.0067 * (log10(energy)) * (log10(energy)) - 0.1468 * (log10(energy))  - 18.2701))))/2);
  }


static Scalar sigmaCEHX(Scalar energy)
//  He+ + Xe -> He + Xe+
//
//  The average of the cross section for the Xe+ + Xe
//  and He+ + He charge exchange
//
//  Argon:
//  W. H. Cramer JCP (1959) 641  4 - 400 eV
//   Hegerberg & Elford, J.Phys.B  797 (1982)  0.1 - 0.7 eV
//  JTG July 27 2011
  {
    return((pow(10.0,(-0.0011-(0.0011*energy+0.0089)/(1+energy))*log10(energy)*log10(energy)*log10(energy)-(0.0655+(0.0655*energy+0.2392)/(1+energy))*log10(energy)-(9.2855+(9.2855*energy+9.2435)/(1+energy)))+(87.3 - 13.6 * (log10(energy)))*1e-20)/2);
  }



static Scalar sigmaHem3ArPenningIz(Scalar energy) // add, SAS July 4 2014
//
// Hem3 + Ar -> Ar+ + He + e
//
// The cross section is fitted to measurements taken from
// Burdenski81_296, ishida01_9379 and olson72_1031
//
  {
    if (energy<10000) return(pow(10,(0.0073*(energy/(1+energy))*pow(log10(energy),4)-0.0457*pow(log10(energy),3)+0.0245*pow(log10(energy),2)+0.6004*(1/(1+energy))*log10(energy)-18.7090)));
    return(0);
  }

static Scalar sigmaHem3XePenningIz(Scalar energy) // add, SAS August 4 2014
//
// Hem3 + Xe -> Xe+ + He + e
//
// Same as the Hem3Ar Penning cross section
//
  {
    if (energy<10000) return(pow(10,(0.0073*(energy/(1+energy))*pow(log10(energy),4)-0.0457*pow(log10(energy),3)+0.0245*pow(log10(energy),2)+0.6004*(1/(1+energy))*log10(energy)-18.7090)));
    return(0);
  }



static Scalar sigmaHem1ArPenningIz(Scalar energy) // add, SAS July 4 2014
//
// Hem1 + Ar -> Ar+ + He + e
//
// The cross section is fitted to measurements taken from
// Burdenski81_296, ishida01_9379 and olson72_1031
//
  {
    if (energy<10000) return(pow(10,(0.0092*(energy/(1+energy))*pow(log10(energy),4)-0.0543*pow(log10(energy),3)+0.0279*pow(log10(energy),2)+0.7123*(1/(1+energy))*log10(energy)-18.6791)));
    return(0);
  }

static Scalar sigmaHem1XePenningIz(Scalar energy) // add, SAS August 4 2014
//
// Hem1 + Xe -> Xe+ + He + e
//
// Same as the Hem1Ar Penning cross section
//
  {
    if (energy<10000) return(pow(10,(0.0092*(energy/(1+energy))*pow(log10(energy),4)-0.0543*pow(log10(energy),3)+0.0279*pow(log10(energy),2)+0.7123*(1/(1+energy))*log10(energy)-18.6791)));
    return(0);
  }



static Scalar sigmaCoulombArXe(Scalar energy) // add, MAL 11/25/11 (SAS July 4 2014)
// Coulomb momentum xfer cross section sigma_m=pi*b_0^2*ln(Lambda), with
// b_0=e/4*pi*eps0*E_R^2 = 1.439E-9/E_R^2, with E_R=CM energy in eV
// ln(Lambda)=ln(2*lambda_De/b_0), lambda_De=electron Debye length
// Use ln(Lambda)=13 -> sigmaCoulomb=8.457E-17/E_R^2
// To find the scattering cross section the momentum transfer cross section
// has to be multiplied by a factor of
// (m1 + m2)/m2 = (131.293 + 39.948)/39.948 = 4.2866

 {
   if (energy > 0.0026) return(4.2866*8.457E-17/(energy*energy));
   else return 4.2866*1.251E-11;
 }


static Scalar sigmaCoulombArHe(Scalar energy) // add, MAL 11/25/11 (SAS July 4 2014)
// See above
// (m1 + m2)/m2 = (4.003 + 39.948)/4.003 = 10.980
 {
    if (energy > 0.0026) return(10.980*8.457E-17/(energy*energy));
    else return 10.980*1.251E-11;
 }


static Scalar sigmaCoulombHeXe(Scalar energy) // add, MAL 11/25/11 (SAS July 4 2014)
// See above
// (m1 + m2)/m2 = (4.003 + 131.293)/4.003 = 33.79
 {
    if (energy > 0.0026) return(33.79*8.457E-17/(energy*energy));
    else return 33.79*1.251E-11;
 }

};
#endif
