// Helium_gas class includes all properties regarding helium
// by HyunChul Kim, 2006
// Helium gas:  ion cross sections from curve fits to data from
// Cramer and Simons, Journal of Chemical Physics, Vol 26, No. 5, May, 1957.
// See also E.W. McDaniel, 1993
// Electron x-sections from oopic/xpdp1
// Cross sections somewhat revised JTG July 27 2011
//
// Some CS revised and some added Roozbeh June 13 2012
//
// All CS revised and some added (Added Hem3, Hem1, Hep3, Hep1, unit) SAS June 30 2014
/*
 NOTE TO DEVELOPERS: // MAL200117
 A ReactionGroup is a set of reactions having the same reactant[0], reactant[1], and mcc type. The priority in reactants and reactions is meaningful.  The first reactant (reactant[0]) in the first reaction in a ReactionGroup determines the mass used to evaluate the energy for the cross section: energy = 0.5*mass(reactant[0])*(relative velocity)^2. Uncomment #define debugReactionGroup in reaction.hpp to print all ReactionGroups to the terminal during initialization.
 */

#ifndef HELIUMGAS_H
#define HELIUMGAS_H

#include "atomic_properties/gas.hpp"
#include "main/oopiclist.hpp"
#include "main/species.hpp"

#define   HE_NEMAX    500
#define   HE_NIMAX    500
#define   HE_NMMAX    10

class Helium_Gas: public Gas
{

public:
  // mindgame: "sec" refers to "new product".
  // Add Species *sec_He_ion and Species *sec_Unit to the Helium_Gas constructor
  Helium_Gas(oopicListIter<Species> species_iter, Species *sec_e, Species *sec_i, Species *sec_He_ion, Species *sec_Unit,
	Species *sec_He, Species *sec_Hem3, Species *sec_Hem1, Species *sec_Hep3, Species *sec_Hep1, bool no_default_secondary_species_flag)
  {
    mcctype = "generic_mcc";
    gastype = HELIUM_GAS;

    // mindgame: find lists of reactants
    vector<Species *> E_species = Species::get_reaction_species_named_with(species_iter, E);
    vector<Species *> He_species = Species::get_reaction_species_named_with(species_iter, He);
    vector<Species *> He_ion_species = Species::get_reaction_species_named_with(species_iter, He_ion);
    vector<Species *> Hep3_species = Species::get_reaction_species_named_with(species_iter, Hep3); // add MAL 11/15/10 (SAS 2 July 2014)
    vector<Species *> Hep1_species = Species::get_reaction_species_named_with(species_iter, Hep1); // add MAL 11/15/10 (SAS 2 July 2014)
    vector<Species *> Hem3_species = Species::get_reaction_species_named_with(species_iter, Hem3); // add MAL 11/16/10 (SAS 2 July 2014)
    vector<Species *> Hem1_species = Species::get_reaction_species_named_with(species_iter, Hem1); // add MAL 11/16/10 (SAS 2 July 2014)
    vector<Species *> Unit_species = Species::get_reaction_species_named_with(species_iter, Unit); // add MAL 11/16/10 (SAS 2 July 2014)

    // mindgame: if empty, assign default secondary species
    if (!no_default_secondary_species_flag)
    {
      if (!sec_e && !E_species.empty()) sec_e = E_species[0];
//      if (!sec_i && !He_ion_species.empty()) sec_i = He_ion_species[0]; // remove sec_i, MAL191219
      if (!sec_He_ion && !He_ion_species.empty()) sec_He_ion = He_ion_species[0]; // add MAL191211
      if (!sec_Hep3 && !Hep3_species.empty()) sec_Hep3 = Hep3_species[0]; // add MAL 11/15/10 (SAS 2 July 2014)
      if (!sec_Hep1 && !Hep1_species.empty()) sec_Hep1 = Hep1_species[0]; // add MAL 11/15/10 (SAS 2 July 2014)
      if (!sec_Hem3 && !Hem3_species.empty()) sec_Hem3 = Hem3_species[0]; // add MAL 11/17/10 (SAS 2 July 2014)
      if (!sec_Hem1 && !Hem1_species.empty()) sec_Hem1 = Hem1_species[0]; // add MAL 11/17/10 (SAS 2 July 2014)
      if (!sec_He && !He_species.empty()) sec_He = He_species[0]; // add MAL 11/16/10 (SAS 2 July 2014)
      if (!sec_Unit && !Unit_species.empty()) sec_Unit = Unit_species[0]; // add MAL191211
}

// NOTE: When adding reactions, the priority in reactants and products is meaningful.
// The calculation of sigma(energy)*v(energy) in the mcc depends on the mass of reactant[0].
// The order of products must be the same here as in MCC::init_mcc() and MCC::dynamic().
// MAL, 12/31/10

		Reaction * newreaction = NULL;
    for (int j=He_species.size()-1; j>=0; j--)
    {
      He_species[j]->set_atomicnumber(2);
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaElastic, 0.0, E_ELASTIC);
        reactionlist.add(new Reaction(gastype, E_ELASTIC, E_species[i], He_species[j], xsection, mcctype()));
        xsection = add_xsection(sigmaExcm3, 19.82, E_EXCITATION);// add Roozbeh June 17 2012
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], He_species[j], xsection, mcctype(), true, sec_Hem3));
        xsection = add_xsection(sigmaExcm1, 20.62, E_EXCITATION);// add Roozbeh June 17 2012
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], He_species[j], xsection, mcctype(), true, sec_Hem1));
        xsection = add_xsection(sigmaExcp3, 20.97, E_EXCITATION);// add Roozbeh June 17 2012
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], He_species[j], xsection, mcctype(), true, sec_Hep3));
        xsection = add_xsection(sigmaExcp1, 21.22, E_EXCITATION);// add Roozbeh June 17 2012
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], He_species[j], xsection, mcctype(), true, sec_Hep1));

        xsection = add_xsection(sigmaEHeIz, 24.59, E_IONIZATION);
        reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], He_species[j], xsection, mcctype(), true, sec_e, sec_He_ion)); // sec_i -> sec_He_ion, MAL191219
      }
      for (int i=He_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaISO, 0.0, SAMEMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, He_ion_species[i], He_species[j], xsection, mcctype()));
        xsection = add_xsection(sigmaB,0.0, B_SAMEMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, B_SAMEMASS_ELASTIC, He_ion_species[i], He_species[j], xsection, mcctype()));
      }
      for (int i=He_species.size()-1; i>=0; i--) // add for loop for like-particle collisions, MAL 11/23/09
      {
           xsection = add_xsection(sigmaHeHe,0.0, SAMEMASS_ELASTIC);
           reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, He_species[i], He_species[j], xsection, mcctype()));
      }
      for (int i=Hem3_species.size()-1; i>=0; i--) // add for loop for Hem3-He particle collisions, MAL 11/23/09
      {
        xsection = add_xsection(sigmaHeHe,0.0, SAMEMASS_ELASTIC); // use sigmaHeHe, MAL 11/26/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, Hem3_species[i], He_species[j], xsection, mcctype()));
      }
      for (int i=Hem1_species.size()-1; i>=0; i--) // add for loop for Hem1-He particle collisions, MAL 11/23/09
      {
        xsection = add_xsection(sigmaHeHe,0.0, SAMEMASS_ELASTIC); // use sigmaHeHe, MAL 11/26/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, Hem1_species[i], He_species[j], xsection, mcctype()));
      }
    }

    for (int j=Hem3_species.size()-1; j>=0; j--)
    {
      Hem3_species[j]->set_atomicnumber(2);
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaElastic, 0.0, E_ELASTIC); // add, MAL 11/20/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, E_ELASTIC, E_species[i], Hem3_species[j], xsection, mcctype()));
        xsection = add_xsection(sigmaEHem3EHe, -19.82, E_EXCITATION); // add, MAL 11/20/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Hem3_species[j], xsection, mcctype(), true, sec_He));
        xsection = add_xsection(sigmaEHem3Iz,4.76, E_IONIZATION); // add, MAL 11/20/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], Hem3_species[j], xsection, mcctype(), true, sec_e, sec_He_ion)); // sec_i -> sec_He_ion, MAL191219
        xsection = add_xsection(sigmaEHem3EHem1, 0.796, E_EXCITATION); // add, MAL 11/20/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Hem3_species[j], xsection, mcctype(), true, sec_Hem1));
        xsection = add_xsection(sigmaEHem3EHep3, 1.148, E_EXCITATION); // add, MAL 11/20/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Hem3_species[j], xsection, mcctype(), true, sec_Hep3));
        xsection = add_xsection(sigmaEHem3EHep1, 1.404, E_EXCITATION); // add, MAL 11/20/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Hem3_species[j], xsection, mcctype(), true, sec_Hep1));
      }
    }


    for (int j=Hem1_species.size()-1; j>=0; j--)
    {
      Hem1_species[j]->set_atomicnumber(2);
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaElastic, 0.0, E_ELASTIC); // add, MAL 11/20/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, E_ELASTIC, E_species[i], Hem1_species[j], xsection, mcctype()));
        xsection = add_xsection(sigmaEHem1EHe, -20.62, E_EXCITATION); // add, MAL 11/20/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Hem1_species[j], xsection, mcctype(), true, sec_He));
        xsection = add_xsection(sigmaEHem1Iz,3.97, E_IONIZATION); // add, MAL 11/20/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], Hem1_species[j], xsection, mcctype(), true, sec_e, sec_He_ion)); // sec_i -> sec_He_ion, MAL191219
        xsection = add_xsection(sigmaEHem1EHem3, -0.796, E_EXCITATION); // add, MAL 11/20/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Hem1_species[j], xsection, mcctype(), true, sec_Hem3));
        xsection = add_xsection(sigmaEHem1EHep3, 0.348, E_EXCITATION); // add, MAL 11/20/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Hem1_species[j], xsection, mcctype(), true, sec_Hep3));
        xsection = add_xsection(sigmaEHem1EHep1, 0.604, E_EXCITATION); // add, MAL 11/20/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Hem1_species[j], xsection, mcctype(), true, sec_Hep1));
      }
    }

    for (int j=Unit_species.size()-1; j>=0; j--)
    {
      Unit_species[j]->set_atomicnumber(1000000);
      for (int i=Hep3_species.size()-1; i>=0; i--) // add for loop for Helium 2p collisions, MAL 11/16/10
      {
        xsection = add_xsection(sigmaHep3Hem3Emission, 0.0, PHOTON_EMISSION); // add cross section and reaction, MAL 11/17/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, PHOTON_EMISSION, Hep3_species[i], Unit_species[j], xsection, mcctype(), true, sec_Hem3));
      }
      for (int i=Hep1_species.size()-1; i>=0; i--) // add for loop for Helium 2p collisions, MAL 11/16/10
      {
        xsection = add_xsection(sigmaHep1Hem1Emission, 0.0, PHOTON_EMISSION); // add cross section and reaction, MAL 11/17/10 (SAS June 30 2014)
        reactionlist.add(new Reaction(gastype, PHOTON_EMISSION, Hep1_species[i], Unit_species[j], xsection, mcctype(), true, sec_Hem1));
// THIS IS RESONANCE RADIATION, SO MUST CALCULATE THE RADIATION TRAPPING FACTOR FOR THIS REACTION (see mcc.cpp), MAL 5/13/16
        xsection = add_xsection(sigmaHep1HeEmission, 0.0, PHOTON_EMISSION); // add cross section and reaction, MAL 11/17/10 (SAS June 30 2014)
        newreaction = new Reaction(gastype, PHOTON_EMISSION, Hep1_species[i], Unit_species[j], xsection, mcctype(), true, sec_He);
        reactionlist.add(newreaction);
        newreaction->add_characteristic_value(58.4e-9); // wavelength lambda10 in [m] for He 2P1->1S0 transition
        newreaction->add_characteristic_value(1.79e9); // He 1P1 level intrinsic decay frequency nu10 in [s^{-1}] (NIST DATABASE, Smirnov below)
        newreaction->add_characteristic_value(1.04e-17); // Hep1-He cross section sigma00 (at Tg = 0.026 eV) in [m^2]
          // cross section sigma00=sigma_t/2, as given from B.M. Smirnov, Theory of Gas Discharge Plasma, pp. 71-74 (Springer, 2015)
        newreaction->add_characteristic_value(3.0); // He upper (1P1)/lower(1S0) level statistical weight ratio g1/g0
      }
    }

// Add ion-ion coulomb collisions, MAL 11/25/11
    for (int j=He_ion_species.size()-1; j>=0; j--)
    {
      He_ion_species[j]->set_atomicnumber(2);
      for (int i=He_ion_species.size()-1; i>=0; i--) // add for loop for like-particle collisions, MAL 11/23/09
      {
        xsection = add_xsection(sigmaCoulombHe,0.0, SAMEMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, He_ion_species[i], He_ion_species[j], xsection, mcctype()));
      }
    }
//
  }


////////////////////////
// cross sections
////////////////////////
// Elastic scattering: e + He -> e + He
//
// Changed the exponent from -1.1 to -0.98 to
// fit better the cross section given by
// Li et al., Radiation Physics and Chemistry,
// vol 48, no 6, page 711 (1996)
//
// and
//
// S J Buckman and B Lohmann, JPB, vol 19, no 16, p. 2547
//
//  This may need to be refitted completely
// JTG July 27 2011
//
//  revised:
//The cross sections are taken from
//
//michels69-467, Fig. 1
//CALCULATION OF CROSS-SECTIONS FOR ELECTRON-HELIUM COLLISIONS *
//H. H. MICHELS, F. E. HARRIS** and R. M. SCOLSKY
//
//and
//
//brunger92_1823, Table 2
//Elastic electron scattering from helium: absolute experimental cross sections, theory and derived interaction potentials
//M J Brungert, S J Buckman, L J Allen, I E McCarthy and K Ratnavelu
//
//and
//
//Vinodkumar07-3259, Table 1
//Calculations of elastic, ionization and total cross sections for inert
//gases upon electron impact: threshold to 2 keV
//Minaxi Vinodkumar, Chetan Limbachiya, Bobby Antony and K N Joshipura
//
// Roozbeh June 13 2012
  static Scalar sigmaElastic(Scalar energy)
  {

    if (energy <= 100) return((pow(10.0,( -0.0156 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))  - 0.0578 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 0.0791 * (log10(energy)) * (log10(energy)) + 0.0188 * (log10(energy)) - 19.2217))) );
    else if (energy < 10000) return((pow(10.0,(-0.0237 * (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy)) * (log10(energy)) + 0.0781 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 0.9186 * (log10(energy)) * (log10(energy)) * (log10(energy)) -5.6247 * (log10(energy)) * (log10(energy))  + 9.5316 * (log10(energy)) -24.6268))));
    else return 0;

  }



// Excitation from GS to 23S metastable level: e + He -> e + He*
// Cross sections are taken from LXcat, http://www.lxcat.laplace.univ-tlse.fr, REFERENCES: Cross sections extracted from PROGRAM MAGBOLTZ, VERSION 8.9 March 2010
// Roozbeh June 15 2012
//
// Revised:
// Cross section is taken from
// Ralchenko et al, Atomic Data and Nuclear Data Tables,
//  Vol 94, No. 4, 2008 p. 603-622.
//
// SAS June 24 2014
  static Scalar sigmaExcm3(Scalar energy)
  {
    if (energy<19.82) return(0);
    else if (energy<26.333) return(((3.8117e-20)/(4*((energy-16.1595)/0.9151)))*(0.3076*log(((energy-16.1595)/0.9151)/4)-0.2748*(1-4/((energy-16.1595)/0.9151))+0.4462*pow(1-4/((energy-16.1595)/0.9151),2)-0.1841*pow(1-4/((energy-16.1595)/0.9151),3)+1.336*pow(1-4/((energy-16.1595)/0.9151),4)-1.775*pow(1-4/((energy-16.1595)/0.9151),5)));
    else if(energy<10000) return((0.8797e-20)*(13.6057/energy)*(0.2823+2.048/(energy/19.82)+5.287/((energy/19.82)*(energy/19.82))-7.363/((energy/19.82)*(energy/19.82)*(energy/19.82)))*(1/(((energy/19.82)*(energy/19.82))+27.28)));
    else return(0);
  }



// Excitation from GS to 21S metastable level: e + He -> e + He*
// Cross sections are taken from LXcat, http://www.lxcat.laplace.univ-tlse.fr, REFERENCES: Cross sections extracted from PROGRAM MAGBOLTZ, VERSION 8.9 March 2010
// Roozbeh June 15 2012
//
// Revised:
// Cross section is taken from
// Ralchenko et al, Atomic Data and Nuclear Data Tables,
//  Vol 94, No. 4, 2008 p. 603-622.
//
// SAS June 24 2014
  static Scalar sigmaExcm1(Scalar energy)
  {
    if (energy<20.62) return(0);
    else if (energy < 26.1216) return(((0.003354e-17)/(4*((energy-17.5279)/0.7730)))*(0.3076*log(((energy-17.5279)/0.7730)/4)-0.2748*(1-4/((energy-17.5279)/0.7730))+0.4462*pow(1-4/((energy-17.5279)/0.7730),2)-0.1841*pow(1-4/((energy-17.5279)/0.7730),3)+1.336*pow(1-4/((energy-17.5279)/0.7730),4)-1.775*pow(1-4/((energy-17.5279)/0.7730),5)));
    else if (energy <10000) return((0.8797e-20)*(13.6057/energy)*(0.1888-0.5754/(energy/20.62)+3.439/((energy/20.62)*(energy/20.62))-2.088/((energy/20.62)*(energy/20.62)*(energy/20.62)))*(((energy/20.62)*(energy/20.62))/(((energy/20.62)*(energy/20.62))+25.44)));
    return(0);
  }



// Excitation from GS to 23P: e + He -> e + He(23P)
// Cross sections are taken from LXcat, http://www.lxcat.laplace.univ-tlse.fr, REFERENCES: Cross sections extracted from PROGRAM MAGBOLTZ, VERSION 8.9 March 2010
// Roozbeh June 15 2012
//
// Revised:
// Cross section is taken from
// Ralchenko et al, Atomic Data and Nuclear Data Tables,
//  Vol 94, No. 4, 2008 p. 603-622.
//
// SAS June 24 2014
  static Scalar sigmaExcp3(Scalar energy)
  {
   // if(energy < 20.96) return (0.0);
   // else if( energy <25.4) return ((pow(10.0,( -36499000 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 303160000 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) - 1049100000 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 1936000000 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 2009500000 * (log10(energy)) * (log10(energy)) + 1112300000  * (log10(energy)) -256500000))) );
   // else if (energy < 425) return((pow(10.0,(-0.6433 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 6.114 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 21.8728  * (log10(energy)) * (log10(energy)) + 34.5023  * (log10(energy)) - 41.134 ))));
   // else if (energy < 10000) return((pow(10.0,(-0.6942 * (log10(energy)) - 19.4159 ))));
   // else return 0;

    if (energy<20.97) return(0);
    else if (energy<27.8612) return(((0.0036e-17)/(4*((energy-17.0969)/0.9683)))*(0.3076*log(((energy-17.0969)/0.9683)/4)-0.2748*(1-4/((energy-17.0969)/0.9683))+0.4462*pow(1-4/((energy-17.0969)/0.9683),2)-0.1841*pow(1-4/((energy-17.0969)/0.9683),3)+1.336*pow(1-4/((energy-17.0969)/0.9683),4)-1.775*pow(1-4/((energy-17.0969)/0.9683),5)));
    else if(energy<10000) return((0.8797e-20)*(13.6057/energy)*(0.2823+2.048/(energy/20.97)+5.287/((energy/20.97)*(energy/20.97))-7.363/((energy/20.97)*(energy/20.97)*(energy/20.97)))*(1/(((energy/20.97)*(energy/20.97))+27.28)));
    else return(0);
  }



// Excitation from GS to 21P: e + He -> e + He(21P)
// Cross sections are taken from LXcat, http://www.lxcat.laplace.univ-tlse.fr, REFERENCES: Cross sections extracted from PROGRAM MAGBOLTZ, VERSION 8.9 March 2010
// Roozbeh June 15 2012
//
// Revised:
// Cross section is taken from
// Ralchenko et al, Atomic Data and Nuclear Data Tables,
//  Vol 94, No. 4, 2008 p. 603-622.
//
// SAS June 24 2014
  static Scalar sigmaExcp1(Scalar energy)
  {
    //if(energy < 20.96) return (0.0);
    //else if( energy <25.4) return ((pow(10.0,( -36499000 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 303160000 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) - 1049100000 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 1936000000 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 2009500000 * (log10(energy)) * (log10(energy)) + 1112300000  * (log10(energy)) -256500000))) );
    //else if (energy < 425) return((pow(10.0,(-0.6433 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 6.114 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 21.8728  * (log10(energy)) * (log10(energy)) + 34.5023  * (log10(energy)) - 41.134 ))));
    //else if (energy < 10000) return((pow(10.0,(-0.6942 * (log10(energy)) - 19.4159 ))));
    //else return 0;
    if (energy<21.22) return(0);
    else if (energy < 10000) return((0.8797e-20)*(13.6057/energy)*((0.7087*log(energy/21.22)-0.09347-(1.598/(energy/21.22))+2.986/((energy/21.22)*(energy/21.22))-1.293/((energy/21.22)*(energy/21.22)*(energy/21.22)))*(((energy/21.22)+1)/((energy/21.22)+0.3086))));
    return(0);
  }



// Excitation from 23S metastable level to 23P: e + He(23S) -> e + He(23P)
// Cross section is taken from
// Ralchenko et al, Atomic Data and Nuclear Data Tables,
//  Vol 94, No. 4, 2008 p. 603-622.
//
// SAS June 24 2014
  static Scalar sigmaEHem3EHep3(Scalar energy)
  {
    if (energy<1.148) return(0);
    else if (energy < 4.4005) return(((11.3651e-17)/(4*((energy+0.68)/0.457)))*(0.3076*log(((energy+0.68)/0.457)/4)-0.2748*(1-4/((energy+0.68)/0.457))+0.4462*pow(1-4/((energy+0.68)/0.457),2)-0.1841*pow(1-4/((energy+0.68)/0.457),3)+1.336*pow(1-4/((energy+0.68)/0.457),4)-1.775*pow(1-4/((energy+0.68)/0.457),5)));
    else if (energy < 10000) return((0.8797e-20)*(13.6057/(3*energy))*((76.96*log(energy/1.148)+125+(49.38/(energy/1.148))-477.8/((energy/1.148)*(energy/1.148))+318.9/((energy/1.148)*(energy/1.148)*(energy/1.148)))*(((energy/1.148)+1)/((energy/1.148)+8.157))));
    return(0);
  }



// Excitation from 23S metastable level to 21P: e + He(23S) -> e + He(21P)
// Cross section is taken from
// Ralchenko et al, Atomic Data and Nuclear Data Tables,
//  Vol 94, No. 4, 2008 p. 603-622.
//
// SAS June 24 2014
  static Scalar sigmaEHem3EHep1(Scalar energy)
  {
    if (energy<1.404) return(0);
    else if (energy < 2.063) return(((0.3126e-17)/(4*((energy-1.0336)/0.0926)))*(0.3076*log(((energy-1.0336)/0.0926)/4)-0.2748*(1-4/((energy-1.0336)/0.0926))+0.4462*pow(1-4/((energy-1.0336)/0.0926),2)-0.1841*pow(1-4/((energy-1.0336)/0.0926),3)+1.336*pow(1-4/((energy-1.0336)/0.0926),4)-1.775*pow(1-4/((energy-1.0336)/0.0926),5)));
    else if (energy < 10000) return((0.8797e-20)*(13.6057/(3*energy))*((48.93+4251/(energy/1.404)-4330/((energy/1.404)*(energy/1.404))+193.4/((energy/1.404)*(energy/1.404)*(energy/1.404)))*(1/(((energy/1.404)*(energy/1.404))+892.8))));
    return(0);
  }



// Excitation from 21S metastable level to 23P: e + He(21S) -> e + He(23P)
// Cross section is taken from
// Ralchenko et al, Atomic Data and Nuclear Data Tables,
//  Vol 94, No. 4, 2008 p. 603-622.
//
// SAS June 24 2014
  static Scalar sigmaEHem1EHep3(Scalar energy)
  {
    if (energy<0.348) return(0);
    else if (energy < 0.4875) return(((4.763e-17)/(4*((energy-0.2696)/0.0196)))*(0.3076*log(((energy-0.2696)/0.0196)/4)-0.2748*(1-4/((energy-0.2696)/0.0196))+0.4462*pow(1-4/((energy-0.2696)/0.0196),2)-0.1841*pow(1-4/((energy-0.2696)/0.0196),3)+1.336*pow(1-4/((energy-0.2696)/0.0196),4)-1.775*pow(1-4/((energy-0.2696)/0.0196),5)));
    else if (energy < 10000) return((0.8797e-20)*(13.6057/(1*energy))*((598.3-531/(energy/0.348)+334.8/((energy/0.348)*(energy/0.348))-241.2/((energy/0.348)*(energy/0.348)*(energy/0.348)))*(1/(((energy/0.348)*(energy/0.348))+223.9))));
    return(0);
  }



// Excitation from 21S metastable level to 21P: e + He(21S) -> e + He(21P)
// Cross section is taken from
// Ralchenko et al, Atomic Data and Nuclear Data Tables,
//  Vol 94, No. 4, 2008 p. 603-622.
//
// SAS June 24 2014
  static Scalar sigmaEHem1EHep1(Scalar energy)
  {
    if (energy<0.604) return(0);
    else if (energy < 2.4434) return(((28.3954e-17)/(4*((energy+0.4298)/0.2585)))*(0.3076*log(((energy+0.4298)/0.2585)/4)-0.2748*(1-4/((energy+0.4298)/0.2585))+0.4462*pow(1-4/((energy+0.4298)/0.2585),2)-0.1841*pow(1-4/((energy+0.4298)/0.2585),3)+1.336*pow(1-4/((energy+0.4298)/0.2585),4)-1.775*pow(1-4/((energy+0.4298)/0.2585),5)));
    else if (energy < 10000) return((0.8797e-20)*(13.6057/(1*energy))*((34.04*log(energy/0.604)+72.67+(171/(energy/0.604))-703.3/((energy/0.604)*(energy/0.604))+470.4/((energy/0.604)*(energy/0.604)*(energy/0.604)))*(((energy/0.604)+1)/((energy/0.604)+11.94))));
    return(0);
  }



// Excitation from 23S metastable level to 21S metastable level: e + He(23S) -> e + He(21S)
// Cross section is taken from
// Ralchenko et al, Atomic Data and Nuclear Data Tables,
//  Vol 94, No. 4, 2008 p. 603-622.
//
// SAS June 24 2014
  static Scalar sigmaEHem3EHem1(Scalar energy)
  {
    if (energy<0.796) return(0);
    else if (energy < 1.015) return(((1.80694e-17)/(4*((energy-0.672914)/0.03077)))*(0.3076*log(((energy-0.672914)/0.03077)/4)-0.2748*(1-4/((energy-0.672914)/0.03077))+0.4462*pow(1-4/((energy-0.672914)/0.03077),2)-0.1841*pow(1-4/((energy-0.672914)/0.03077),3)+1.336*pow(1-4/((energy-0.672914)/0.03077),4)-1.775*pow(1-4/((energy-0.672914)/0.03077),5)));
    else if (energy < 10000) return((0.8797e-20)*(13.6057/(3*energy))*(54.75+348300/(energy/0.796)+234500/((energy/0.796)*(energy/0.796))-509000/((energy/0.796)*(energy/0.796)*(energy/0.796)))*(1/(((energy/0.796)*(energy/0.796))+54240)));
    return(0);
  }



// De-excitation from 21S metastable level to 23S metastable level: e + He(21S) -> e + He(23S)
// Detailed balancing is used to convert the cross section obtained in sigmaEHem3EHem1 here above
//
// SAS June 24 2014
  static Scalar sigmaEHem1EHem3(Scalar energy)
  {
    if (energy+0.796 < 1.015) return(3*(1+0.796/energy)*((1.80694e-17)/(4*((energy+0.796-0.672914)/0.03077)))*(0.3076*log(((energy+0.796-0.672914)/0.03077)/4)-0.2748*(1-4/((energy+0.796-0.672914)/0.03077))+0.4462*pow(1-4/((energy+0.796-0.672914)/0.03077),2)-0.1841*pow(1-4/((energy+0.796-0.672914)/0.03077),3)+1.336*pow(1-4/((energy+0.796-0.672914)/0.03077),4)-1.775*pow(1-4/((energy+0.796-0.672914)/0.03077),5)));
    else if (energy+0.796 < 10000) return(3*(1+0.796/energy)*(0.8797e-20)*(13.6057/(3*(energy+0.796))*(54.75+348300/((energy+0.796)/0.796)+234500/(((energy+0.796)/0.796)*((energy+0.796)/0.796))-509000/(((energy+0.796)/0.796)*((energy+0.796)/0.796)*((energy+0.796)/0.796)))*(1/((((energy+0.796)/0.796)*((energy+0.796)/0.796))+54240))));
    return(0);
  }



// De-excitation from 23P level to ground state: e + He(23P) -> e + He(11S)
// Detailed balancing is used to convert the cross section obtained in sigmaExcp3 here above
//
// SAS July 7 2014
  static Scalar sigmaEHep3EHe(Scalar energy)
  {
   // if(energy < 20.96) return (0.0);
   // else if( energy <25.4) return ((pow(10.0,( -36499000 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 303160000 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) - 1049100000 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 1936000000 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 2009500000 * (log10(energy)) * (log10(energy)) + 1112300000  * (log10(energy)) -256500000))) );
   // else if (energy < 425) return((pow(10.0,(-0.6433 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 6.114 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 21.8728  * (log10(energy)) * (log10(energy)) + 34.5023  * (log10(energy)) - 41.134 ))));
   // else if (energy < 10000) return((pow(10.0,(-0.6942 * (log10(energy)) - 19.4159 ))));
   // else return 0;

    if (energy+20.97<27.8612) return(0.33333*(1+20.97/energy)*((0.0036e-17)/(4*(((energy+20.97)-17.0969)/0.9683)))*(0.3076*log((((energy+20.97)-17.0969)/0.9683)/4)-0.2748*(1-4/(((energy+20.97)-17.0969)/0.9683))+0.4462*pow(1-4/(((energy+20.97)-17.0969)/0.9683),2)-0.1841*pow(1-4/(((energy+20.97)-17.0969)/0.9683),3)+1.336*pow(1-4/(((energy+20.97)-17.0969)/0.9683),4)-1.775*pow(1-4/(((energy+20.97)-17.0969)/0.9683),5)));
    else if(energy+20.97<10000) return(0.33333*(1+20.97/energy)*(0.8797e-20)*(13.6057/(energy+20.97))*(0.2823+2.048/((energy+20.97)/20.97)+5.287/(((energy+20.97)/20.97)*((energy+20.97)/20.97))-7.363/(((energy+20.97)/20.97)*((energy+20.97)/20.97)*((energy+20.97)/20.97)))*(1/((((energy+20.97)/20.97)*((energy+20.97)/20.97))+27.28)));
    else return(0);

  }



// De-excitation from 21P level to ground state: e + He(21P) -> e + He(11S)
// Detailed balancing is used to convert the cross section obtained in sigmaExcp1 here above
//
// SAS July 7 2014
  static Scalar sigmaEHep1EHe(Scalar energy)
  {
    //if(energy < 20.96) return (0.0);
    //else if( energy <25.4) return ((pow(10.0,( -36499000 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 303160000 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) - 1049100000 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 1936000000 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 2009500000 * (log10(energy)) * (log10(energy)) + 1112300000  * (log10(energy)) -256500000))) );
    //else if (energy < 425) return((pow(10.0,(-0.6433 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 6.114 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 21.8728  * (log10(energy)) * (log10(energy)) + 34.5023  * (log10(energy)) - 41.134 ))));
    //else if (energy < 10000) return((pow(10.0,(-0.6942 * (log10(energy)) - 19.4159 ))));
    //else return 0;

    if ((energy+21.22) < 10000) return((1+21.22/energy)*(0.8797e-20)*(13.6057/(energy+21.22))*((0.7087*log((energy+21.22)/21.22)-0.09347-(1.598/((energy+21.22)/21.22))+2.986/(((energy+21.22)/21.22)*((energy+21.22)/21.22))-1.293/(((energy+21.22)/21.22)*((energy+21.22)/21.22)*((energy+21.22)/21.22)))*((((energy+21.22)/21.22)+1)/(((energy+21.22)/21.22)+0.3086))));
    return(0);
  }



// De-excitation from 23S metastable level to ground state: e + He(23S) -> e + He(11S)
// Detailed balancing is used to convert the cross section obtained in sigmaExcm3 here above
//
// SAS June 24 2014
  static Scalar sigmaEHem3EHe(Scalar energy)
  {
    if (energy+19.82 <26.333) return(0.33333*(1+19.82/energy)*((3.8117e-20)/(4*((energy+19.82-16.1595)/0.9151)))*(0.3076*log(((energy+19.82-16.1595)/0.9151)/4)-0.2748*(1-4/((energy+19.82-16.1595)/0.9151))+0.4462*pow(1-4/((energy+19.82-16.1595)/0.9151),2)-0.1841*pow(1-4/((energy+19.82-16.1595)/0.9151),3)+1.336*pow(1-4/((energy+19.82-16.1595)/0.9151),4)-1.775*pow(1-4/((energy+19.82-16.1595)/0.9151),5)));
    else if(energy+19.82 <10000) return(0.33333*(1+19.82/energy)*(0.8797e-20)*(13.6057/(energy+19.82))*(0.2823+2.048/((energy+19.82)/19.82)+5.287/(((energy+19.82)/19.82)*((energy+19.82)/19.82))-7.363/(((energy+19.82)/19.82)*((energy+19.82)/19.82)*((energy+19.82)/19.82)))*(1/((((energy+19.82)/19.82)*((energy+19.82)/19.82))+27.28)));
    else return(0);
  }



// De-excitation from 21S metastable level to ground state: e + He(21S) -> e + He(11S)
// Detailed balancing is used to convert the cross section obtained in sigmaExcm1 here above
//
// SAS June 24 2014
  static Scalar sigmaEHem1EHe(Scalar energy)
  {
    if (energy+20.62 < 26.1216) return((1+20.62/energy)*((0.003354e-17)/(4*((energy+20.62-17.5279)/0.7730)))*(0.3076*log(((energy+20.62-17.5279)/0.7730)/4)-0.2748*(1-4/((energy+20.62-17.5279)/0.7730))+0.4462*pow(1-4/((energy+20.62-17.5279)/0.7730),2)-0.1841*pow(1-4/((energy+20.62-17.5279)/0.7730),3)+1.336*pow(1-4/((energy+20.62-17.5279)/0.7730),4)-1.775*pow(1-4/((energy+20.62-17.5279)/0.7730),5)));
    else if (energy+20.62 <10000) return((1+20.62/energy)*(0.8797e-20)*(13.6057/(energy+20.62))*(0.1888-0.5754/((energy+20.62)/20.62)+3.439/(((energy+20.62)/20.62)*((energy+20.62)/20.62))-2.088/(((energy+20.62)/20.62)*((energy+20.62)/20.62)*((energy+20.62)/20.62)))*((((energy+20.62)/20.62)*((energy+20.62)/20.62))/((((energy+20.62)/20.62)*((energy+20.62)/20.62))+25.44)));
    return(0);
  }



// ionization: e + He -> e + e + He+
//
// The cross section is taken from measurements of Rejoub, Lindsay, and Stebbings,
// Phys. Rev. A, vol 65, no 4, 042713 (2002)
//
// JTG July 27 2011
  static Scalar sigmaEHeIz(Scalar energy)
  {
    if (energy <= 24.59) return(0);
    return(1e-17*(energy - 24.59)/((energy + 50)*pow(energy+300.0, 1.2)));
  }



// ionization from metastable state 23S: e + He(23S) -> 2e + He+
// Cross section is taken from
// Ralchenko et al, Atomic Data and Nuclear Data Tables,
//  Vol 94, No. 4, 2008 p. 603-622.
//
// SAS June 24 2014
  static Scalar sigmaEHem3Iz(Scalar energy)
  {
    if (energy <= 4.76) return(0);
    return(((1e-17)/(4.76*energy))*(0.2427*log(energy/4.76)-0.19*(1-4.76/energy)+0.3205*pow(1-4.76/energy,2)+0.7631*pow(1-4.76/energy,3)-0.8329*pow(1-4.76/energy,4)-0.2405*pow(1-4.76/energy,5)));
  }



// Ionization from metastable state 21S: e + He(21S) -> 2e + He+
// Cross section is taken from
// Ralchenko et al, Atomic Data and Nuclear Data Tables,
//  Vol 94, No. 4, 2008 p. 603-622.
//
// SAS June 24 2014
  static Scalar sigmaEHem1Iz(Scalar energy)
  {
    if (energy <= 3.97) return(0);
    return(((1e-17)/(3.97*energy))*(0.3076*log(energy/3.97)-0.2748*(1-3.97/energy)+0.4462*pow(1-3.97/energy,2)-0.1841*pow(1-3.97/energy,3)+1.336*pow(1-3.97/energy,4)-1.775*pow(1-3.97/energy,5)));
  }



//*---------------------------------------------------------------------------*/
//* Helium 23P Effective Emission Cross-section for unit target density         */
//*---------------------------------------------------------------------------*/
//*  Hep3 (+ Unit) -> Hem3 (+ Unit) + photon    */
//
// sigma = nu/sqrt(2*e*energy/M), M = Hem3 mass
//
// nu = 1E7 s^-1
//
// W. Wiese and J. Fuhr, Journal of Physical and Chemical Reference Data 38, 613 (2009)
  static Scalar sigmaHep3Hem3Emission(Scalar energy)  // Added by MAL, 11/17/10 (SAS June 30 2014)

 {
    if (energy >= 1E-6) {return(1.439E3 / sqrt(energy));}
    else {return(1.439E6);}
 }



//*---------------------------------------------------------------------------*/
//* Helium 21P Effective Emission Cross-section for unit target density         */
//*---------------------------------------------------------------------------*/
//*  Hep1 (+ Unit) -> Hem1 (+ Unit) + photon    */
//
// sigma = nu/sqrt(2*e*energy/M), M = Hem1 mass
//
// nu = 2E6 s^-1
//
// W. Wiese and J. Fuhr, Journal of Physical and Chemical Reference Data 38, 613 (2009)
  static Scalar sigmaHep1Hem1Emission(Scalar energy)  // Added by MAL, 11/17/10 (SAS June 30 2014)

 {
    if (energy >= 1E-6) {return(2.88E2 / sqrt(energy));}
    else {return(2.88E5);}
 }



//*---------------------------------------------------------------------------*/
//* Helium 21P Effective Emission Cross-section for unit target density         */
//*---------------------------------------------------------------------------*/
//*  Hep1 (+ Unit) -> He (+ Unit) + photon    */
//
// sigma = nu/sqrt(2*e*energy/M), M = He mass
//
// nu = 1.8E9 s^-1
//
// W. Wiese and J. Fuhr, Journal of Physical and Chemical Reference Data 38, 613 (2009)
  static Scalar sigmaHep1HeEmission(Scalar energy)  // Added by MAL, 11/17/10 (SAS June 30 2014)

 {
    if (energy >= 1E-6) {return(2.59E5 / sqrt(energy));}
    else {return(2.59E8);}
 }



static Scalar sigmaB(Scalar energy)
// BACKWARDS PORTION OF ION-NEUTRAL SCATTERING as in Phelps JAP 76 (1994) 747
// Revised by SAS August 2014
    {
      return(1E-19*pow(energy/1000,-0.15)*pow(1+energy/1000,-0.25)*pow(1+5/energy,-0.15));
    }


static Scalar sigmaISO(Scalar energy)
// ISOTROPIC PORTION OF ION-NEUTRAL SCATTERING as in Phelps JAP 76 (1994) 747
// Revised by SAS August 2014
    {
      return (7.63E-20*pow(energy,-0.5));
    }



// for like-particle scattering
//*---------------------------------------------------------------------------*/
//*  He + He -> He + He   Elastic  Scattering                               */
//*---------------------------------------------------------------------------*/
// Elastic Scattering of Helium on Helium:
//	These are fits to the data given at
//	http://jilawww.colorado.edu/~avp/collision_data/neutralneutral/atomatom.txt
  static Scalar sigmaHeHe(Scalar energy) //Added by SAS 23 June 2014
  {
    return(pow(10.0,-0.0046*log10(energy)*log10(energy)*log(energy)-0.0687*log10(energy)-18.5799));
  }

// add, MAL 11/25/11 (SAS July 4 2014)
// Coulomb momentum xfer cross section sigma_m=pi*b_0^2*ln(Lambda), with
// b_0=e/4*pi*eps0*E_R^2 = 1.439E-9/E_R^2, with E_R=CM energy in eV
// ln(Lambda)=ln(2*lambda_De/b_0), lambda_De=electron Debye length
// Use ln(Lambda)=13 -> sigmaCoulomb=8.457E-17/E_R^2
// To find the scattering cross section the momentum transfer cross section
// has to be multiplied by a factor of
// (m1 + m2)/m2 = 2
  static Scalar sigmaCoulombHe(Scalar energy)
  {
    if (energy > 0.0026) return(2*8.457E-17/(energy*energy));
    else return 2*1.251E-11;
  }

};
#endif
