#ifndef DIFFUSION_FLUID_H
#define DIFFUSION_FLUID_H

// DiffusionFluid : a simple fluid with no EM interaction
// suitable for a background gas
// [really, a static fluid for now, but diffusion could be added]
// Alan Wu began adding diffusion, 6/2010, not completed, MAL 1/17/11
// JH, June 10, 2006

#include "utils/ovector.hpp"
#include "atomic_properties/fluid.hpp"
#include "main/parse.hpp"
#include "utils/message.hpp"
#include "utils/ostring.hpp"

class DiffusionFluid : public Fluid, public Parse, public Message{ // add public Message, MAL200131
private:
  Scalar T; // temperature
  bool flux_out; // whether the fluid is lost to the walls
  bool evolve_fluid; // evolve fluid in time due to diffusion and collisions, MAL200127
  Equation density;
  Equation source; // Alan Wu add
  Equation Dcoefficient; // equation for the density diffusion coefficient, MAL200127
  Scalar gammaL; // fluid loss probability at left wall, MAL200128
  Scalar gammaR; // fluid loss probability at right wall, MAL200128
  // Scalar subcycle; // Alan Wu add
  int subcycle; // used for subcycling fluid-fluid collisions, should be an int, MAL200220
  Scalar leftflux; // Alan Wu add; positive for inward flux, MAL200128
  Scalar rightflux; // Alan Wu add; positive for inward flux, MAL200128
  int ng; // grid number, MAL200205
  Scalar dx; // uniform grid interval, MAL200205
  Scalar vbar; // fluid  mean speed sqrt(8*e*T/PI*M), MAL200205
  Scalar vonNeumannstability; // von Neumann stability parameter maxD*dt/(dx)^2 for FTCS diffusion calculation, MAL200213
  bool density_diff_flag; // true if the density diffusion coefficient is defined and > 0, MAL200204
  bool molefrac_diff_flag; // true if the mole-fraction diffusion coefficient and n_fixed are defined, MAL200204
  ostring fluidname; // MAL200127
  Scalar * D_coef; // a grid array density or mole-fraction diffusion coefficient, MAL 200129
  Scalar * n_fixed; // for mole-fraction diffusion, a grid array sum of all the fixed fluid densities, MAL200201
  Scalar * molefrac_old; // a grid array of the previous timestep mole fractions, MAL200204
  Scalar diff_vol; // for mole-fraction diffusion, the species diffusion volume [(cm)^3/mole]; see
  // E.N Fuller, P.D. Schettler, and J. Calvin Giddings, Ind. Engin. Chem. vol. 58, May 1966, pp. 19-27; MAL200205
public:
  DiffusionFluid(Species * _thespecies, bool _parse);
  ~DiffusionFluid(){ // MAL200201
    delete [] D_coef; D_coef=0;
    delete [] n_fixed; n_fixed=0;
    delete [] molefrac_old; molefrac_old=0; // MAL200204
  }
  // virtual functions from Parse
  void setdefaults();
  void setparamgroup();
  void init();
  vector<DiffusionFluid * > get_fixedfluids() const; // MAL200124
  DiffusionFluid * get_fixedfluid(int i) const; // MAL200124
  void addto_fixedfluids(DiffusionFluid * _pdf); // MAL200124
  Scalar * get_Dcoef() const {return D_coef;} // MAL200129
  inline Scalar get_Dcoef(int i) const {return D_coef[i];} // MAL200129
  Scalar * get_n_fixed() const {return n_fixed;} // MAL200201
  inline Scalar get_nfixed(int i) const {return n_fixed[i];} // MAL200201
  void is_evolve_fluid_in_time_implemented(); // MAL200131
  bool get_evolvefluid() const {return evolve_fluid;} // MAL200208
  int get_subcycle() const {return subcycle;} // MAL200220
  inline Scalar get_diffvol() const {return diff_vol;} // MAL200201
  bool set_density_diff(); // MAL200204
  bool set_molefrac_diff(); // MAL200204
  void advance_density(Scalar _dt); // MAL200205
  void advance_molefrac(Scalar _dt); // MAL200205
  void add_or_subtract_density(Scalar num_phys_part, Scalar gridx); // MAL200209
  void update_N_tot(); // MAL200212
  void interact_evolvingfluid_with_boundaries(); // Add, MAL200223
  // virtual functions from Fluid
  VDistribution * velocity_distribution(int node);
  Vector3 get_v_from_distribution(int node);
  Scalar electrostatic_density(Scalar potential);
  void set_thefield(Fields * _thefield);
  void update(Scalar dt);
  void dump_fluid(FILE * DMPFile); // add for fluids dump; MAL200228
  void reload_fluid(FILE * DMPFile); // add for fluids reload from dumpfile; MAL200229
  Fluid * get_copy();
	Scalar get_T() { return T; } // add, MAL 12/30/10
  Species * get_thespecies() { return thespecies; } // add, MAL200124
  ostring get_fluidname() { return fluidname; } // MAL200127
};

static vector<DiffusionFluid * > fixedfluids; // add to accumulate the sum of fixed fluids, MAL200124
#endif
