// Argon_gas class includes all properties regarding argon
// by HyunChul Kim, 2006
// Revised, added metastable, resonance, and 4p argon levels, MAL 12/10
// Revised, corrected certain cross sections, JTG 12/15/10
// Revised, added resonance radiation trapping data in characteristic_values for Arr -> Ar resonance emission, MAL 1/3/11
// Corrected Penning ionization ReleasedEnergy values, MAL 1/3/11
// Latest cross section corrections of JTG, sent to MAL on 1/9/11
/*
 NOTE TO DEVELOPERS: // MAL200117
 A ReactionGroup is a set of reactions having the same reactant[0], reactant[1], and mcc type. The priority in reactants and reactions is meaningful.  The first reactant (reactant[0]) in the first reaction in a ReactionGroup determines the mass used to evaluate the energy for the cross section: energy = 0.5*mass(reactant[0])*(relative velocity)^2. Uncomment #define debugReactionGroup in reaction.hpp to print all ReactionGroups to the terminal during initialization.
 NOTE: For reactions with only one product (E_EXCITATION, SAMEMASS_EXCITATION, etc), the first reactant must be the conserved (unchanged) species; MAL200215
 */

#ifndef ARGONGAS_H
#define ARGONGAS_H

#include "atomic_properties/gas.hpp"
#include "utils/ovector.hpp"
#include "main/species.hpp"

#define   AR_NEMAX    50000
#define   AR_NIMAX    500
#define   AR_NMMAX    10
#define   EN10K    10000-0.01
#include "atomic_properties/argon_xsec.hpp"

Scalar sigma; // add, MAL 11/21/10
float asigma1(float), asigma2(float), asigma3(float), asigma4(float), asigma5(float),asigma6(float),
		asigma7(float), asigma8(float), asigma9(float);
float vg_therm;
float 	eexc=11.62, //* excitation threshold */
		eion=15.76, //* ionisation threshold */
		thrsh_engy1 = 11.548,   //*  e + Ar -> e + Ar* Metastable */
		thrsh_engy2 = 4.21,    //*  e + Ar* -> e + Ar+ Ionization*/
		thrsh_engy3 = 0.0,    //*  Ar* + Ar*->Ar+Ar++e Ioniz.*/
		thrsh_engy4 = 1.6;    //*  Ar* + e->Ar(r)+e Quench.*/

class Argon_Gas: public Gas
{

public:
	// mindgame: "sec" refers to "new product".
	// add sec_Ar4p below, MAL 11/16/10
	Argon_Gas(oopicListIter<Species> species_iter, Species *sec_e, Species *sec_i, Species *sec_Ar_ion,
			  Species *sec_Ar, Species *sec_Arm, Species *sec_Arr, Species *sec_Ar4p, Species *sec_Unit,
			  bool no_default_secondary_species_flag) // add *sec_Ar_ion and sec_Unit to list, MAL191210, MAL191211
	{
		mcctype = "generic_mcc";
		gastype = ARGON_GAS;

		// mindgame: find lists of reactants
		vector<Species *> E_species = Species::get_reaction_species_named_with(species_iter, E);
		vector<Species *> Ar_species = Species::get_reaction_species_named_with(species_iter, Ar);
		vector<Species *> Ar_ion_species = Species::get_reaction_species_named_with(species_iter, Ar_ion);
		vector<Species *> Ar4p_species = Species::get_reaction_species_named_with(species_iter, Ar4p); // add MAL 11/15/10
		vector<Species *> Arm_species = Species::get_reaction_species_named_with(species_iter, Arm); // add MAL 11/17/10
		vector<Species *> Arr_species = Species::get_reaction_species_named_with(species_iter, Arr); // add MAL 11/17/10
		vector<Species *> Unit_species = Species::get_reaction_species_named_with(species_iter, Unit); // add MAL 11/16/10

		// mindgame: if empty, assign default secondary species
		if (!no_default_secondary_species_flag)
		{
			if (!sec_e && !E_species.empty()) sec_e = E_species[0];
//      if (!sec_i && !Ar_ion_species.empty()) sec_i = Ar_ion_species[0];
			if (!sec_Ar4p && !Ar4p_species.empty()) sec_Ar4p = Ar4p_species[0]; // add MAL 11/15/10
			if (!sec_Arm && !Arm_species.empty()) sec_Arm = Arm_species[0]; // add MAL 11/17/10
			if (!sec_Arr && !Arr_species.empty()) sec_Arr = Arr_species[0]; // add MAL 11/17/10
			if (!sec_Ar && !Ar_species.empty()) sec_Ar = Ar_species[0]; // add MAL 11/16/10
			if (!sec_Ar_ion && !Ar_ion_species.empty()) sec_Ar_ion = Ar_ion_species[0]; // add MAL191210
			if (!sec_Unit && !Unit_species.empty()) sec_Unit = Unit_species[0]; // add MAL191211
		}

		// NOTE: When adding reactions, the priority in reactants and products is meaningful.
		// The calculation of sigma(energy)*v(energy) in the mcc depends on the mass of reactant[0].
		// The order of products must be the same here as in MCC::init_mcc() and MCC::dynamic().
		// MAL, 12/31/10

// BEGIN SECTION ADDING COLLISIONAL REACTIONS

		Reaction * newreaction = NULL;

// BEGIN collisions of Ar with all species, including Ar, except the Unit (density = 1) species
		for (int j=Ar_species.size()-1; j>=0; j--)
		{
			Ar_species[j]->set_atomicnumber(18);
			for (int i=E_species.size()-1; i>=0; i--)
			{
				// mindgame: I don't know why this crosssection, which doesn't have
				//          crosssection at energy less than 0.3 eV, has been used.
				xsection = add_xsection(sigmaElastic,0.0, E_ELASTIC);
				reactionlist.add(new Reaction(gastype, E_ELASTIC, E_species[i], Ar_species[j], xsection, mcctype()));
				xsection = add_xsection(sigmaExc, eexc, E_EXCITATION);
				// change (Species *)NULL -> sec_Arr below
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Ar_species[j], xsection, mcctype(), true, sec_Arr));
				xsection = add_xsection(sigmaExcm, thrsh_engy1, E_EXCITATION);
				// change (Species *)NULL -> sec_Arm below
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Ar_species[j], xsection, mcctype(), true, sec_Arm));
				xsection = add_xsection(sigmaEx4p, 13.2, E_EXCITATION);
				// change (Species *)NULL -> sec_Ar4p below
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Ar_species[j], xsection, mcctype(), true, sec_Ar4p));
				xsection = add_xsection(sigmaII, 14.09, E_EXCITATION); // added by JTG January 6 2011
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Ar_species[j], xsection, mcctype(), true, (Species *)NULL));
				xsection = add_xsection(sigmaIII, 14.71, E_EXCITATION); // added by JTG January 6 2011
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Ar_species[j], xsection, mcctype(), true, (Species *)NULL));
				xsection = add_xsection(sigmahigher, 15.20, E_EXCITATION); // added by JTG January 6 2011
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Ar_species[j], xsection, mcctype(), true, (Species *)NULL));
				xsection = add_xsection(sigmaIz,15.76, E_IONIZATION);
				reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], Ar_species[j], xsection, mcctype(), true, sec_e, sec_Ar_ion)); // sec_i -> sec_Ar_ion, MAL191214
			}
			for (int i=Ar_ion_species.size()-1; i>=0; i--)
			{
				xsection = add_xsection(sigmaISO,0.0, SAMEMASS_ELASTIC);
				reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, Ar_ion_species[i], Ar_species[j], xsection, mcctype()));
				xsection = add_xsection(sigmaB,0.0, B_SAMEMASS_ELASTIC);
				reactionlist.add(new Reaction(gastype, B_SAMEMASS_ELASTIC, Ar_ion_species[i], Ar_species[j], xsection, mcctype()));

				// before merge on 12/30/19 by JK
				// xsection = add_xsection(sigmaES,0.0, SAMEMASS_ELASTIC);
				// reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, Ar_ion_species[i], Ar_species[j], xsection, mcctype()));
				// xsection = add_xsection(sigmaCE,0.0, Ch_EXCHANGE);
				// reactionlist.add(new Reaction(gastype, Ch_EXCHANGE, Ar_ion_species[i], Ar_species[j], xsection, mcctype()));
			}
			for (int i=Ar_species.size()-1; i>=0; i--) // add for loop for like-particle collisions, MAL 11/23/09
			{
				xsection = add_xsection(sigmaArAr,0.0, SAMEMASS_ELASTIC);
				reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, Ar_species[i], Ar_species[j], xsection, mcctype()));
			}
			for (int i=Arm_species.size()-1; i>=0; i--) // add for loop for Arm-Ar particle collisions, MAL 11/23/09
			{
				xsection = add_xsection(sigmaArAr,0.0, SAMEMASS_ELASTIC); // use sigmaArAr, MAL 11/26/10
				reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, Arm_species[i], Ar_species[j], xsection, mcctype()));
				xsection = add_xsection(sigmaArmArArrAr,0.0, SAMEMASS_EXCITATION); // use sigmaArAr, MAL 11/26/10
//        reactionlist.add(new Reaction(gastype, SAMEMASS_EXCITATION, Arm_species[i], Ar_species[j], xsection, mcctype(), true, sec_Arr)); // FIX BUG, the order of reactants must be inverted, MAL200215
				reactionlist.add(new Reaction(gastype, SAMEMASS_EXCITATION, Ar_species[j], Arm_species[i], xsection, mcctype(), true, sec_Arr)); // correct order of reactants, MAL200215
			}
			for (int i=Arr_species.size()-1; i>=0; i--) // add for loop for Arr-Ar particle collisions, MAL 11/23/09
			{
				xsection = add_xsection(sigmaArAr,0.0, SAMEMASS_ELASTIC); // use sigmaArAr, MAL 11/26/10
				reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, Arr_species[i], Ar_species[j], xsection, mcctype()));
				xsection = add_xsection(sigmaArrArArmAr,0.0, SAMEMASS_EXCITATION); // use sigmaArAr, MAL 11/26/10
//        reactionlist.add(new Reaction(gastype, SAMEMASS_EXCITATION, Arr_species[i], Ar_species[j], xsection, mcctype(), true, sec_Arm)); // FIX BUG, the order of reactants must be inverted, MAL200215
				reactionlist.add(new Reaction(gastype, SAMEMASS_EXCITATION, Ar_species[j], Arr_species[i], xsection, mcctype(), true, sec_Arm)); // correct order of reactants, MAL200215
			}
			for (int i=Ar4p_species.size()-1; i>=0; i--) // add for loop for Ar4p-Ar particle collisions, MAL 11/23/09
			{
				xsection = add_xsection(sigmaArAr,0.0, SAMEMASS_ELASTIC); // use sigmaArAr, MAL 11/26/10
				reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, Ar4p_species[i], Ar_species[j], xsection, mcctype()));
			}
		}
// END Ar collisions with itself and all other species, except for Unit species
//
// BEGIN Arm collisions with all other species, except Ar
		for (int j=Arm_species.size()-1; j>=0; j--)
		{
			Arm_species[j]->set_atomicnumber(18);
			for (int i=E_species.size()-1; i>=0; i--)
			{
				xsection = add_xsection(sigmaElastic, 0.0, E_ELASTIC); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_ELASTIC, E_species[i], Arm_species[j], xsection, mcctype()));
				xsection = add_xsection(sigmaEArmEAr, -11.55, E_EXCITATION); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Arm_species[j], xsection, mcctype(), true, sec_Ar));
				xsection = add_xsection(sigmaEArmIz,4.2, E_IONIZATION); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], Arm_species[j], xsection, mcctype(), true, sec_e, sec_Ar_ion)); // sec_i -> sec_Ar_ion, MAL191214
				xsection = add_xsection(sigmaEArmEArr, 0.0, E_EXCITATION); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Arm_species[j], xsection, mcctype(), true, sec_Arr));
				xsection = add_xsection(sigmaEArmEAr4p, 1.59, E_EXCITATION); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Arm_species[j], xsection, mcctype(), true, sec_Ar4p));
			}
			for (int i=Arm_species.size()-1; i>=0; i--)
			{
				// ReleasedEnergy = 2*Em - Eiz = 7.34 eV, MAL 1/3/11 (1s5 Paschen level)
				xsection = add_xsection(sigmaArmArmPenningIz, 0.0, PENNING_IONIZATION); // add, MAL 11/26/10
				newreaction = new Reaction(gastype, PENNING_IONIZATION, Arm_species[i], Arm_species[j], xsection, mcctype(), true, sec_e, sec_Ar_ion, sec_Ar); // sec_i -> sec_Ar_ion, MAL191214
				reactionlist.add(newreaction);
				newreaction->add_characteristic_value(7.34);
			}
			for (int i=Arr_species.size()-1; i>=0; i--)
			{
				// ReleasedEnergy = Em + Er - Eiz = 7.62 eV, MAL 1/3/11 (1s5 + 1s2 Paschen levels)
				xsection = add_xsection(sigmaArmArrPenningIz, 0.0, PENNING_IONIZATION); // add, MAL 11/26/10
				newreaction = new Reaction(gastype, PENNING_IONIZATION, Arr_species[i], Arm_species[j], xsection, mcctype(), true, sec_e, sec_Ar_ion, sec_Ar); // sec_i -> sec_Ar_ion, MAL191214
				reactionlist.add(newreaction);
				newreaction->add_characteristic_value(7.62);
			}
		}
// END Arm collisions with all other species except Ar and Unit species

// BEGIN Ar4p collisions with all other species, except Ar, Arm, and Unit species
		for (int j=Ar4p_species.size()-1; j>=0; j--)
		{
			Ar4p_species[j]->set_atomicnumber(18);
			for (int i=E_species.size()-1; i>=0; i--)
			{
				xsection = add_xsection(sigmaElastic, 0.0, E_ELASTIC); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_ELASTIC, E_species[i], Ar4p_species[j], xsection, mcctype()));
				xsection = add_xsection(sigmaEArmEAr, -13.13, E_EXCITATION); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Ar4p_species[j], xsection, mcctype(), true, sec_Ar));
				xsection = add_xsection(sigmaEAr4pIz,4.2, E_IONIZATION); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], Ar4p_species[j], xsection, mcctype(), true, sec_e, sec_Ar_ion)); // sec_i -> sec_Ar_ion, MAL191214
				xsection = add_xsection(sigmaEAr4pEArr, -1.59, E_EXCITATION); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Ar4p_species[j], xsection, mcctype(), true, sec_Arr));
				xsection = add_xsection(sigmaEAr4pEArm, -1.59, E_EXCITATION); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Ar4p_species[j], xsection, mcctype(), true, sec_Arm));
			}
			for (int i=Ar4p_species.size()-1; i>=0; i--)
			{
				// ReleasedEnergy = 2 * E4p - Eiz = 11.20 eV, MAL 1/3/11 (2p1 level Paschen notation)
				xsection = add_xsection(sigmaAr4pAr4pPenningIz, 0.0, PENNING_IONIZATION); // add, MAL 11/26/10
				newreaction = new Reaction(gastype, PENNING_IONIZATION, Ar4p_species[i], Ar4p_species[j], xsection, mcctype(), true, sec_e, sec_Ar_ion, sec_Ar); // sec_i -> sec_Ar_ion, MAL191214
				reactionlist.add(newreaction);
				newreaction->add_characteristic_value(11.20);
			}
		}
// END Ar4p collisions with all other species except Ar, Arm, and Unit species

// BEGIN Arr collisions with all other species, except Ar, Arm, Ar4p, and Unit species
		for (int j=Arr_species.size()-1; j>=0; j--)
		{
			Arr_species[j]->set_atomicnumber(18);
			for (int i=E_species.size()-1; i>=0; i--)
			{
				xsection = add_xsection(sigmaElastic, 0.0, E_ELASTIC); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_ELASTIC, E_species[i], Arr_species[j], xsection, mcctype()));
				xsection = add_xsection(sigmaEArrEAr, -12.6, E_EXCITATION); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Arr_species[j], xsection, mcctype(), true, sec_Ar));
				xsection = add_xsection(sigmaEArrEArm, 0.0, E_EXCITATION); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Arr_species[j], xsection, mcctype(), true, sec_Arm));
				xsection = add_xsection(sigmaEArrEAr4p, 1.59, E_EXCITATION); // add, MAL 11/20/10
				reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Arr_species[j], xsection, mcctype(), true, sec_Ar4p));
			}
			for (int i=Arr_species.size()-1; i>=0; i--)
			{
				// ReleasedEnergy = 2 * Er - Eiz = 7.90 eV, MAL 1/3/11 (1s2 resonance level)
				xsection = add_xsection(sigmaArrArrPenningIz, 0.0, PENNING_IONIZATION); // add, MAL 11/26/10
				newreaction = new Reaction(gastype, PENNING_IONIZATION, Arr_species[i], Arr_species[j], xsection, mcctype(), true, sec_e, sec_Ar_ion, sec_Ar); // sec_i -> sec_Ar_ion, MAL191214
				reactionlist.add(newreaction);
				newreaction->add_characteristic_value(7.90);
			}
		}
// END Arr collisions with all other species, except Ar, Arm, Ar4p, and Unit species
//
// BEGIN Unit species (density = 1) collisions with all other species (THESE ARE ALL PHOTON EMISSIONS)
		for (int j=Unit_species.size()-1; j>=0; j--)
		{
			Unit_species[j]->set_atomicnumber(1000000);
			for (int i=Ar4p_species.size()-1; i>=0; i--) // add for loop for argon 4p collisions, MAL 11/16/10
			{
				xsection = add_xsection(sigmaAr4pArmEmission, 0.0, PHOTON_EMISSION); // add cross section and reaction, MAL 11/17/10
				reactionlist.add(new Reaction(gastype, PHOTON_EMISSION, Ar4p_species[i], Unit_species[j], xsection, mcctype(), true, sec_Arm));
				xsection = add_xsection(sigmaAr4pArrEmission, 0.0, PHOTON_EMISSION); // add cross section and reaction, MAL 11/17/10
				reactionlist.add(new Reaction(gastype, PHOTON_EMISSION, Ar4p_species[i], Unit_species[j], xsection, mcctype(), true, sec_Arr));
			}
			for (int i=Arr_species.size()-1; i>=0; i--) // add for loop for argon resonance state collisions, MAL 11/17/10
			{
				xsection = add_xsection(sigmaArrArEmission, 0.0, PHOTON_EMISSION); // add cross section and reaction, MAL 11/17/10
				newreaction = new Reaction(gastype, PHOTON_EMISSION, Arr_species[i], Unit_species[j], xsection, mcctype(), true, sec_Ar);
				reactionlist.add(newreaction);
				newreaction->add_characteristic_value(104.9e-9); // wavelength lambda10 in [m] for 1P1 -> 1S0 transition
				newreaction->add_characteristic_value(5.1e8); // 1P1 level intrinsic decay frequency nu10 in [s^{-1}] (NIST DATABASE, Smirnov also)
				// cross section sigma00=sigma_t/2, as given from B.M. Smirnov, Theory of Gas Discharge Plasma, pp. 71-74 (Springer, 2015), MAL 5/13/16
				newreaction->add_characteristic_value(3.4e-17); // Arr-Ar cross section sigma00 (at Tg = 0.026 eV) in [m^2] 3.5E-18 -> 3.4E-17
				newreaction->add_characteristic_value(3.0); // upper (1P1)/lower(1S0) level statistical weight ratio g1/g0
			}
		}
// END Unit collisions with all other species

// SAMEMASS_ELASTIC calls samemassElastic(const Xsection& cx)); uses Ar_ion_species, MAL191209
		// Add ion-ion coulomb collisions, MAL 11/25/11
		for (int j=Ar_ion_species.size()-1; j>=0; j--)
		{
			Ar_ion_species[j]->set_atomicnumber(18);
			for (int i=Ar_ion_species.size()-1; i>=0; i--) // add for loop for like-particle collisions, MAL 11/23/09
			{
				xsection = add_xsection(sigmaCoulombAr,0.0, SAMEMASS_ELASTIC);
				reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, Ar_ion_species[i], Ar_ion_species[j], xsection, mcctype()));
			}
		}
	} // END CONSTRUCTOR OF Argon_Gas class

#if 0
	// temporary commented out: previous version
	static Scalar sigmaElastic(Scalar energy)
	// e + Ar -> e + Ar
	// Biagi-v7.1 database, www.lxcat.net. Retrieved august 7, 2014 by SAS.
	//
	{

		if(energy < 0.2303)
			return(pow(10,(-0.2610*pow(log10(energy),3)-1.9145*pow(log10(energy),2)-4.7952*log10(energy)-23.3889)));
		else if(energy < 11)
			return(pow(10,(0.3913*pow(log10(energy),3)-0.6017*pow(log10(energy),2)+1.2974*log10(energy)-19.8867)));
		else if(energy < 241.7)
			return(pow(10,(0.2336*pow(log10(energy),3)-1.2979*pow(log10(energy),2)+1.1558*log10(energy)-18.8166)));
		else
			return(1.2925e-18/energy);
	}
#endif

	static Scalar sigmaElastic(Scalar energy)
	//
	//Elastic 0 - 20 :
	//***************Janez (JK) collected the data(lxcat2019) and added by De-Qi here 02/20/2019, to use the latest elastic cross section
	//***************from LXCat Biagi (Magboltz versions 8.9 and higher)
	//***************previous sections should be effective momentum transfer cross sections, especially for electrons with large kinetic energy(>500eV)
	//***************NOW it is elastic momentum transfer cross sections[such as refer to PSST 24(2015)015019(13PP)].
	{
		int i;
		double els_sigma, alpha;
		static double *elastic1, *elastic2;
		static int init_flag=1;
		// compute slope for extrapolation
		static double k;
		static double logEnN;
		static double logSigmaN;

		//******  Initialization   *********/
		if(init_flag) {
			double engy;

			elastic1 = (double *) malloc(101*sizeof(double));
			elastic2 = (double *) malloc(AR_NEMAX*sizeof(double));
			logEnN = log10(argon_elas_cs[argon_elas_cs_len-1][0]);
			logSigmaN = log10(argon_elas_cs[argon_elas_cs_len-1][1]);
			k = (logSigmaN-log10(argon_elas_cs[argon_elas_cs_len-2][1])) / (logEnN-log10(argon_elas_cs[argon_elas_cs_len-2][0]));

			//*****  With the Ramseur min.  *****/
			elastic1[0] = interp(argon_elas_cs, argon_elas_cs_len, 0.0);
			for(i=1; i<101; i++) {
				engy = .01*i;
				elastic1[i] = interp(argon_elas_cs, argon_elas_cs_len, engy);
			}

			for(i=1; i<AR_NEMAX; i++) {
				engy = i;
				if( engy < argon_elas_cs[argon_elas_cs_len-1][0] ){
					elastic2[i] = interp(argon_elas_cs, argon_elas_cs_len, engy);
				} else {
					// extrapolate from last 2 points
					//printf("%d: %g\n", i, logSigmaN + k*(log10(engy) - logEnN) );
					elastic2[i] = pow(10, logSigmaN + k*(log10(engy) - logEnN) );
				}
			}

			init_flag =0;
		}
		//*--------------------------------------------------------------------*/
		if(energy < 1.0) {
			i= 100*energy;
			alpha = 100*energy -i;
			els_sigma = elastic1[i] + alpha*(elastic1[i+1] - elastic1[i]);
		}
		else {
			i= energy;
			if(i < AR_NEMAX-1) {
				alpha = energy -i;
				els_sigma = elastic2[i] + alpha*(elastic2[i+1] - elastic2[i]);
			} else {
				els_sigma = elastic2[AR_NEMAX-1];
			}
		}

		return(els_sigma);
	}

	static Scalar sigmaExc(Scalar energy)
	//
	// Revised  by JTG December 14 2010
	//Excitation:
	///*---------------------------------------------------------------------------*/
	///*  e + Ar -> e + Ar^r  Excitation    */
	///*---------------------------------------------------------------------------*/
	//   The two cross section for the radiative levels 11.62 eV and 11.83 eV are combined
	//
	// The data is taken from
	// http://jila.colorado.edu/~avp/collision_data/electronneutral/hayashi.txt
	{
		static double exc_k = 0, exc_xsec10k = 0;
		static int exc_init = 0;

		if ( exc_init == 0 ){
			exc_init = 1;		// in initialization
			double en9 = 9990.0;
			double xsec9 = log10(sigmaExc(en9));
			exc_xsec10k = log10(sigmaExc(EN10K));
			exc_k = (exc_xsec10k - xsec9) / (log10(EN10K) - log10(en9));
		}

		if (energy <= 11.62) return(0);
		else if (energy < 14.86) return ((pow(10.0,(-66.69 * (log10(energy)) * (log10(energy)) + 161.2550
				* (log10(energy))   - 118.817)))
				+ (pow(10.0,(-140.3276 * (log10(energy)) * (log10(energy)) * (log10(energy)) + 419.9929 * (log10(energy))
						* (log10(energy))-399.1460 * (log10(energy)) + 95.6975))) );
		else if (energy <= 10000) return ((pow(10.0,( -0.4208 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))
				+ 3.8343	* (log10(energy)) * (log10(energy))* (log10(energy))
				-13.0844 * (log10(energy)) * (log10(energy))
				+ 19.2602 * (log10(energy))  - 31.3637)))+(pow(10.0,( -0.2736 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))
						+ 2.9071	* (log10(energy)) * (log10(energy))* (log10(energy))
						-11.5092 * (log10(energy)) * (log10(energy))
						+ 19.3711 * (log10(energy))  - 32.1718))));
		else {
			// interpolate between last two points (9.9keV - 10keV)
			return pow(10.0, (exc_xsec10k + exc_k*(log10(energy)-log10(EN10K)) ));
		}
	}

#if 0
	// excitation cross section from LXCat - all exciation levels grouped in one level
	/* ********************************** */
	/*  e + Ar -> e + Ar  Excitation threshold:11.548 eV  */
	// De-Qi And Janez (lxcat) whould note be used
	static Scalar sigmaExc(Scalar energy)
	//********** cross sections source: Lxcat 2019 new Janez and De-Qi single excitation process
	{
			int i;
			double exc_sigma, alpha;
			static int init_flag=1;
			static double *excit;

			//******  Initialization   *********/
			if(init_flag)
			{
				double engy;
				excit = (double *) malloc(AR_NEMAX*sizeof(double));

				// compute slope for extrapolation
				double k;
				double logEnN = log10(argon_exc_cs[argon_exc_cs_len-1][0]);
				double logSigmaN = log10(argon_exc_cs[argon_exc_cs_len-1][1]);
				k = (logSigmaN-log10(argon_exc_cs[argon_exc_cs_len-2][1])) / (logEnN-log10(argon_exc_cs[argon_exc_cs_len-2][0]));

				for(i=0; i<AR_NEMAX; i++)
				{
					engy = i;
					if( engy < 11.54800 ) {
						excit[i] = 0.0;
					} else if( engy < argon_exc_cs[argon_exc_cs_len-1][0] ){
						excit[i] = interp(argon_exc_cs, argon_exc_cs_len, engy);
					} else {
						// extrapolate from last 2 points
						excit[i] = pow(10, logSigmaN + k*(log10(engy) - logEnN) );
					}
				}
				init_flag =0;
			}
			//****************************/

			if( energy < 11.54800 ) {
				exc_sigma = 0.0;
			} else {
				i = energy;
				if(i < AR_NEMAX-1) {
					alpha= energy - i;
					exc_sigma = excit[i] + alpha*(excit[i+1] - excit[i]);
				} else {
					exc_sigma = excit[AR_NEMAX-1];
				}
			}

			return(exc_sigma);
		}
#endif

	static Scalar sigmaII(Scalar energy)
	//
	// Added by JTG January 6 2011
	//Excitation:
	///*---------------------------------------------------------------------------*/
	///*  e + Ar -> e + Ar^II  Excitation    */
	///*---------------------------------------------------------------------------*/
	//   The two cross section for group II excited states as
	//   given by Eggarter JCP 62 (1975) 833
	//   with threshold 14.09 - 14.30
	//
	{
		static double excII_k = 0, excII_xsec10k = 0;
		static int excII_init = 0;

		if ( excII_init == 0 ){
			excII_init = 1;		// in initialization
			double en9 = 9990.0;
			double xsec9 = log10(sigmaII(en9));
			excII_xsec10k = log10(sigmaII(EN10K));
			excII_k = (excII_xsec10k - xsec9) / (log10(EN10K) - log10(en9));
		}

		if (energy <= 14.09) return(0);
		else if (energy <= 16) return((pow(10.0,( -1310 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))  + 7353 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 15423 * (log10(energy)) * (log10(energy)) + 14331 * (log10(energy)) - 4999))) );
		else if (energy <= 10000) {
			return((pow(10.0,(0.4157 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 3.7906 * (log10(energy)) * (log10(energy))   + 9.6252 * (log10(energy))  - 27.9792))));
		} else {
			// interpolate between last two points (9.9keV - 10keV)
			return pow(10.0, (excII_xsec10k + excII_k*(log10(energy)-log10(EN10K)) ));
		}
	}

	static Scalar sigmaIII(Scalar energy)
	//
	// Added by JTG January 6 2011
	//Excitation:
	///*---------------------------------------------------------------------------*/
	///*  e + Ar -> e + Ar^III  Excitation    */
	///*---------------------------------------------------------------------------*/
	//   The two cross section for group III excited states as
	//   given by Eggarter JCP 62 (1975) 833
	//   with threshold 14.71 - 15.189
	//
	{
		static double excIII_k = 0, excIII_xsec10k = 0;
		static int excIII_init = 0;

		if ( excIII_init == 0 ){
			excIII_init = 1;		// in initialization
			double en9 = 9990.0;
			double xsec9 = log10(sigmaIII(en9));
			excIII_xsec10k = log10(sigmaIII(EN10K));
			excIII_k = (excIII_xsec10k - xsec9) / (log10(EN10K) - log10(en9));
		}

		if (energy <= 14.71) return(0);
		else if (energy <=40 ) return((pow(10.0,(20.1890 * (log10(energy)) * (log10(energy))* (log10(energy))  -98.2947 * (log10(energy))* (log10(energy))  + 158.6903* (log10(energy)) - 105.7007))));
		else if (energy <= 10000) {
			return((pow(10.0,(-1.8550 * (log10(energy)) * (log10(energy))* (log10(energy))   + 10.0648 * (log10(energy))* (log10(energy))  - 18.2805* (log10(energy)) - 9.6552))));
		} else {
			// interpolate between last two points (9.9keV - 10keV)
			return pow(10.0, (excIII_xsec10k + excIII_k*(log10(energy)-log10(EN10K)) ));
		}
	}

	static Scalar sigmahigher(Scalar energy)
	//
	// Added by JTG January 6 2011
	//Excitation:
	///*---------------------------------------------------------------------------*/
	///*  e + Ar -> e + Ar^higher  Excitation    */
	///*---------------------------------------------------------------------------*/
	//   The two cross section for highest excited states of argon
	//   with threshold 15.20 - 15.76
	//
	// The data is taken from
	// http://jila.colorado.edu/~avp/collision_data/electronneutral/hayashi.txt
	{
		static double excH_k = 0, excH_xsec10k = 0;
		static int excH_init = 0;

		if ( excH_init == 0 ){
			excH_init = 1;		// in initialization
			double en9 = 9990.0;
			double xsec9 = log10(sigmahigher(en9));
			excH_xsec10k = log10(sigmahigher(EN10K));
			excH_k = (excH_xsec10k - xsec9) / (log10(EN10K) - log10(en9));
		}

		if (energy <= 15.20) return(0);
		else if (energy <= 20) return((pow(10.0,(55.5617* (log10(energy)) * (log10(energy))* (log10(energy))  -253.8618 * (log10(energy)) * (log10(energy))  + 385.7905 * (log10(energy))  - 215.8094))));
		else if (energy <= 10000) {
			return((pow(10.0,( -0.5310 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))
				+ 5.1863 * (log10(energy)) * (log10(energy))* (log10(energy))  -18.9355 * (log10(energy)) * (log10(energy))  + 29.8975 * (log10(energy))  - 37.8614))));
		} else {
			// interpolate between last two points (9.9keV - 10keV)
			return pow(10.0, (excH_xsec10k + excH_k*(log10(energy)-log10(EN10K)) ));
		}
	}

	static Scalar sigmaExcm(Scalar energy)
	//*---------------------------------------------------------------------------*/
	//* Argon Metastable Collision Cross-sections                                 */
	//*---------------------------------------------------------------------------*/
	//*  e + Ar -> e + Ar_m       Metastable creation   */
	//
	// Revised  by JTG December 14 2010
	//   The two cross section for the metastable levels 11.55 eV and 11.72 eV are combined
	//
	// The data is taken from
	// http://jila.colorado.edu/~avp/collision_data/electronneutral/hayashi.txt
	{
		static double excm_k = 0, excm_xsec10k = 0;
		static int excm_init = 0;

		if ( excm_init == 0 ){
			excm_init = 1;		// in initialization
			double en9 = 190.0;
			double xsec9 = log10(sigmaExcm(en9));
			excm_xsec10k = log10(sigmaExcm(200-0.01));
			excm_k = (excm_xsec10k - xsec9) / (log10(200-0.01) - log10(en9));
		}

		if (energy <= 11.55) return(0);
		else if (energy < 16.98) return((pow(10.0,(-37.857 * (log10(energy)) * (log10(energy)) + 92.5465
				* (log10(energy))   - 77.8271)))
				+ ( pow(10.0,(72.0705 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) - 920.0452  * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 4981	   * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) - 14821  * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) +26173  * (log10(energy)) * (log10(energy)) * (log10(energy))
						-27426	* (log10(energy)) * (log10(energy))
						-15789 * (log10(energy))
						- 38738)))    );
		else if (energy <= 200) {
			return((pow(10.0,(-1.5379 * (log10(energy)) * (log10(energy))
				+ 2.5690	* (log10(energy))
				- 22.0966)))+(pow(10.0,(72.0705 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) - 920.0452  * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 4981	   * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) - 14821  * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) +26173  * (log10(energy)) * (log10(energy)) * (log10(energy))
						-27426	* (log10(energy)) * (log10(energy))
						-15789 * (log10(energy))
						- 38738))));
		} else {
			// interpolate between last two points (9.9keV - 10keV)
			return pow(10.0, (excm_xsec10k + excm_k*(log10(energy)-log10(200-0.01)) ));
		}
	}

	static Scalar sigmaEx4p(Scalar energy)  // Added by JTG November 28 2009
	//*---------------------------------------------------------------------------*/
	//* Argon Excitation 4p Collision Cross-sections                                 */
	//*---------------------------------------------------------------------------*/
	//*  e + Ar -> e + Ar(4p)      */
	//
	//  Eggarter JCP 62 (1975) 833
	{
		static double exc4p_k = 0, exc4p_xsec10k = 0;
		static int exc4p_init = 0;

		if ( exc4p_init == 0 ){
			exc4p_init = 1;		// in initialization
			double en9 = 9990.0;
			double xsec9 = log10(sigmaEx4p(en9));
			exc4p_xsec10k = log10(sigmaEx4p(EN10K));
			exc4p_k = (exc4p_xsec10k - xsec9) / (log10(EN10K) - log10(en9));
		}

		if (energy <= 13.2) return(0);
		else if (energy < 22.7) return((pow(10.0,(95.2748 * (log10(energy)) * (log10(energy)) * (log10(energy))-379.1870
				* (log10(energy)) * (log10(energy))  + 503.6592 * (log10(energy))  - 243.5251))));
		else if (energy < 200) return((pow(10.0,(1.5246 * (log10(energy)) * (log10(energy)) * (log10(energy))- 8.3548
				* (log10(energy)) * (log10(energy))  + 13.9461 * (log10(energy))  - 27.5910))));
		else if (energy <= 10000) {
			return((pow(10.0,(-1.0734 * (log10(energy))  - 18.7403))));
		} else {
			// interpolate between last two points (9.9keV - 10keV)
			return pow(10.0, (exc4p_xsec10k + exc4p_k*(log10(energy)-log10(EN10K)) ));
		}
	}

	static Scalar sigmaIz(Scalar energy)
	//*---------------------------------------------------------------------------*/
     /*  e + Ar -> e + e + Ar+  Ion.     */
	// Added by De-Qi and Janez (JK) 02/20/2019 data from LXCat Biagi (Magboltz versions 8.9 and higher)
	{
		int i;
		double ion_sigma, alpha;
		static int init_flag=1;
		static double *ioniz;

		//******  Initialization   *********/
		if(init_flag)
		{
			double engy;
			ioniz = (double *) malloc(AR_NEMAX*sizeof(double));

			// compute slope for extrapolation
			double k;
			double logEnN = log10(argon_ion_cs[argon_ion_cs_len-1][0]);
			double logSigmaN = log10(argon_ion_cs[argon_ion_cs_len-1][1]);
			k = (logSigmaN-log10(argon_ion_cs[argon_ion_cs_len-2][1])) / (logEnN-log10(argon_ion_cs[argon_ion_cs_len-2][0]));

			for(i=0; i<AR_NEMAX; i++)
			{
				engy = i;
				if( engy < eion ) {
					ioniz[i] = 0.0;
				} else if( engy < argon_ion_cs[argon_ion_cs_len-1][0] ){
					ioniz[i] = interp(argon_ion_cs, argon_ion_cs_len, engy);
				} else {
					// extrapolate from last 2 points
					ioniz[i] = pow(10, logSigmaN + k*(log10(engy) - logEnN) );
				}
			}
			init_flag =0;
		}
		//****************************/

		if( energy < eion ) {
			ion_sigma = 0.0;
		} else {
			i = energy;
			if(i < AR_NEMAX-1) {
				alpha= energy - i;
				ion_sigma = ioniz[i] + alpha*(ioniz[i+1] - ioniz[i]);
			} else {
				//ion_sigma = ioniz[NEMAX-1];
                ion_sigma = 9.14386e-19*log(energy)/energy;
			}
		}

		return(ion_sigma);
	}

	static Scalar sigmaB(Scalar energy)
	// BACKWARDS PORTION OF ION-NEUTRAL SCATTERING Phelps94
	// Revised by SAS August 2014
	{
		if (energy < 0.1)
			return ((100000*pow(energy,2.5)/(1+100000*pow(energy,2.5)))*(0.575e-18*pow(energy,-0.1)*pow(1+0.015/energy,0.6)-1e-19/(1+energy)/pow(energy,0.5)-1.5e-19*energy/pow(1+energy/3,2.3)));
		else if (energy < 10000)
			return (0.575e-18*pow(energy,-0.1)*pow(1+0.015/energy,0.6)-1e-19/(1+energy)/pow(energy,0.5)-1.5e-19*energy/pow(1+energy/3,2.3));

		return(0);
	}

	static Scalar sigmaCE(Scalar energy)
	//*---------------------------------------------------------------------------*/
	//*  Ar + Ar+ -> Ar+ + Ar  Charge X  */
	//*---------------------------------------------------------------------------*/
	//   W. H. Cramer JCP (1959) 641  4 - 400 eV
	//   Hegerberg & Elford, J.Phys.B  797 (1982)  0.1 - 0.7 eV
	{
		return (pow(10.0,(-0.0067 * (log10(energy)) * (log10(energy)) - 0.1468 * (log10(energy))  - 18.2701)));
	}

	static Scalar sigmaISO(Scalar energy)
	// ISOTROPIC PORTION OF ION-NEUTRAL SCATTERING Phelps94
	// Revised by SAS August 2014
    {
		if(energy < 0.1)
			return ((1.5-0.5*energy/0.1)*(2e-19/(1+energy)/pow(energy,0.5)+3e-19*energy/pow(1+energy/3,2.3)));
		else if(energy < 10000)
			return (2e-19/(1+energy)/pow(energy,0.5)+3e-19*energy/pow(1+energy/3,2.3));

		return(0);
	}

	static Scalar sigmaES(Scalar energy)
	//*---------------------------------------------------------------------------*/
	//*  Ar + Ar+ -> Ar + Ar+   Elastic  Scattering                               */
	//*---------------------------------------------------------------------------*/
	// Elastic Scattering:
	//   McDaniel "Collision Phenomena in Ionised Gases" (1964)
	// W. H. Cramer JCP (1959) 641  4 - 400 eV
	{
		return (pow(10.0,( - 0.1568 * (log10(energy))  - 18.3301)));
	}

	static Scalar sigmaArAr(Scalar energy) // for like-particle scattering, MAL 11/23/09
	//*---------------------------------------------------------------------------*/
	//*  Ar + Ar -> Ar + Ar   Elastic  Scattering                               */
	//*---------------------------------------------------------------------------*/
	// Elastic Scattering of argon on argon:
	//  Phelps et al JPB 33 (2000) 2969
	{
		return (pow(10.0,(-0.0021 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy)) + 0.0081 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))+0.0184 * (log10(energy)) * (log10(energy)) * (log10(energy)) -0.0480 * (log10(energy)) * (log10(energy)) - 0.2599 * (log10(energy))  - 17.7461)));
	}

	static Scalar sigmaAr4pArmEmission(Scalar energy)  // Added by MAL, 11/17/10
	//*---------------------------------------------------------------------------*/
	//* Argon 4p Effective Emission Cross-section for unit target density         */
	//*---------------------------------------------------------------------------*/
	//*  Ar4p (+ Unit) -> Arm (+ Unit) + photon    */
	//
	// sigma = nu/sqrt(2*e*energy/M), M = Arm mass
	//
	// nu = 3E7 s^-1
	//
	// Lee, Jang, and Chung, PoP 13, 053502 (2006) gives natural lifetime for decay to metastable state
	{
		if (energy >= 1E-6) {return(1.371793E4 / sqrt(energy));}
		else {return(1.371793E7);}
	}

	static Scalar sigmaAr4pArrEmission(Scalar energy)  // Added by MAL, 11/17/10
	//*---------------------------------------------------------------------------*/
	//* Argon 4p Effective Emission Cross-section for unit target density         */
	//*---------------------------------------------------------------------------*/
	//*  Ar4p (+ Unit) -> Arr (+ Unit) + photon    */
	//
	// sigma = nu/sqrt(2*e*energy/M), M = Arr mass
	//
	// nu = 3E7 s^-1
	//
	// Lee, Jang, and Chung, PoP 13, 053502 (2006) gives natural lifetime for decay to resonance state
	{
		if (energy >= 1E-6) {return(1.371793E4 / sqrt(energy));}
		else {return(1.371793E7);}
	}

	static Scalar sigmaArrArEmission(Scalar energy)  // Added by MAL, 11/17/10
	//*---------------------------------------------------------------------------*/
	//* Argon 4s resonance state Effective Emission Cross-section for unit target density         */
	//*---------------------------------------------------------------------------*/
	//*  Arr (+ Unit) -> Ar (+ Unit) + photon    */
	//
	// sigma = nu/sqrt(2*e*energy/M), M = Arr mass
	//
	// nu = 1E8 s^-1 for the 3P1 (1s2) state, 4E8 for the 3P1 (1s4) state
	//
	// use 4E8 below
	{
		if (energy >= 1E-6) {return(1.829058E5 / sqrt(energy));}
		else {return(1.829058E8);}
	}

	static Scalar sigmaEArmEAr(Scalar energy) // add, MAL 11/20/10
	// Assume constant sigma with K = sigma*vebar, vebar=sqrt(8*e*Te/pi*m)
	// Fit to Table 1 rate coef K at Te=3eV, in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		return 8.361E-22;
	}

	static Scalar sigmaEArmIz(Scalar energy) // add, MAL 11/20/10
	//   Revised by JTG December 15 2010
	// {
	//  This cross section is taken from Margreiter et al Contributions to Plasma Physics (1990) 487
	//  and the fit is from the pdp1 code used in M. Roberto et al IEEE PS 31 (2003)  1292
	//
	{
		int i, j, N=13;
		float engy, alpha, q;
		static int init_flag=1;
		static float *ioniz2;

		static float en[13] = { 0.0, 3.4, 4.21, 4.76, 5.44, 6.8, 10.2, 17.0, 27.2,
				40.8, 54.4, 81.6, 108.8 };
		static float sig[13] = { 0.0, 0.0, 7.18, 8.05, 8.37, 8.33, 7.17, 5.07,
				3.38, 2.40, 1.84, 1.25, 0.951};

		if (init_flag)
		{
			ioniz2 = (float *) malloc(AR_NEMAX*sizeof(float));
			for(i=0; i<AR_NEMAX; i++)
			{
				engy = i;

				if (engy<4.16)
					ioniz2[i]=0;
				else if (engy>en[N-1])
				{
					ioniz2[i]=sig[N-1]+(sig[N-1]-sig[N-2])*(engy-en[N-1])/(en[N-1]-en[N-2]);
					if (ioniz2[i]<0) ioniz2[i]=0;
				}
				else
					for (j=1;j<N;j++)
						if (engy>en[j-1] && engy<=en[j])
							ioniz2[i]=sig[j-1]+(sig[j]-sig[j-1])*(engy-en[j-1])/(en[j]-en[j-1]);
				ioniz2[i]*=1e-20;
			}
			init_flag=0;
		}
		i=energy;
		if (i<AR_NEMAX-1)
		{
			alpha=energy-i;
			q=ioniz2[i]+alpha*(ioniz2[i+1] - ioniz2[i]);
		}
		else q = ioniz2[AR_NEMAX-1];

		return(q);

	}

	static Scalar sigmaArmArmPenningIz(Scalar energy) // add, MAL 11/26/10
	// Revised by JTG December 15 2010
	// The cross section at room temperature 1.35e-17 m2 is from  the
	// measurements of Okada and Sugawara, JJAP 35 (1996) 4535
	//
	{
		// printf("\nAccessing sigmaArmArmPenningIz\n");
		if (energy > 1E-6) return(2.1768E-18/sqrt(energy));
		else return(2.1768E-15);
	}

	static Scalar sigmaArmArrPenningIz(Scalar energy) // add, MAL 11/26/10
	// Assume sigma=K0/sqrt(2*e*E/m) with K = K0 = const
	// Fit to Table 1 rate coef K at Te=3eV, in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		// printf("\nAccessing sigmaArmArrPenningIz\n");
		if (energy > 1E-6) return(9.603E-19/sqrt(energy));
		else return(9.603E-16);
	}

	static Scalar sigmaArrArrPenningIz(Scalar energy) // add, MAL 11/26/10
	// Assume sigma=K0/sqrt(2*e*E/m) with K = K0 = const
	// Fit to Table 1 rate coef K at Te=3eV, not in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007); use Arr_Arr
	{
		if (energy > 1E-6) return(9.603E-19/sqrt(energy));
		else return(9.603E-16);
	}

	static Scalar sigmaAr4pAr4pPenningIz(Scalar energy) // add, MAL 11/26/10
	// Assume sigma=K0/sqrt(2*e*E/m) with K = K0 = const
	// Fit to Table 1 rate coef K at Te=3eV, in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		if (energy > 1E-6) return(2.286E-19/sqrt(energy));
		else return(2.286E-16);
	}

	static Scalar sigmaArmArArrAr(Scalar energy) // add, MAL 11/26/10
	// Assume sigma=K0/sqrt(2*e*E/m) with K = K0 = const
	// Fit to Table 1 rate coef K at Te=3eV, in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		if (energy > 1E-6) return(9.602E-25/sqrt(energy));
		else return(9.602E-22);
	}

	static Scalar sigmaArrArArmAr(Scalar energy) // add, MAL 11/26/10
	// Assume sigma=K0/sqrt(2*e*E/m) with K = K0 = const
	// Use rate coefficient of inverse reaction in Table 1 of Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		if (energy > 1E-6) return(9.602E-25/sqrt(energy));
		else return(9.602E-22);
	}

	static Scalar sigmaEArmEArr(Scalar energy) // add, MAL 11/20/10
	// Assume sigma=K0/sqrt(2*e*E/m) with K = K0 = const
	// Fit to Table 1 rate coef K at Te=3eV, in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		if (energy > 1E-6) return(6.236E-19/sqrt(energy));
		else return(6.236E-16);
	}

	static Scalar sigmaEArmEAr4p(Scalar energy) // add, MAL 11/20/10
	//  Revised by JTG December 15 2010
	//  The cross section is taken from the pdp1 code used in M. Roberto, IEEE PS 31 (2003) 1292
	//  and is from the measurements of Boffard et al, PRA 59 (1999) 2749 which measure the
	//   cross section from the metastable levels to 8 of 10 levels of the 4p manifold.
	//   Note that this is wrongly claimed to be quenching to the resonant levels in
	//  the code and in  M. Roberto, IEEE PS 31 (2003) 1292
	//float asigma9(float energy)
	{
		int i, j, N=11;
		float engy, alpha, sigma;
		static int init_flag=1;
		static float *quench;

		static float en[11] = { 1.6, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0,
				10.0, 11.0};
		static float sig[11] = { 0.0, 3.5, 15.0, 17.0, 18.0, 19.0, 19.0, 19.0,
				19.0, 18.0, 18.0};
		if (init_flag)
		{
			quench = (float *) malloc(AR_NEMAX*sizeof(float));
			for(i=0; i<AR_NEMAX; i++)
			{
				engy = i;

				if (engy<1.6)
					quench[i]=0;
				else if (engy>en[N-1])
				{
					quench[i]=sig[N-1]+(sig[N-1]-sig[N-2])*(engy-en[N-1])/(en[N-1]-en[N-2]);
					if (quench[i]<0) quench[i]=0;
				}
				else
					for (j=1;j<N;j++)
						if (engy>en[j-1] && engy<=en[j])
							quench[i]=sig[j]+(sig[j]-sig[j-1])*(engy-en[j])/(en[j]-en[j-1]);
				quench[i]*=1e-20;
			}
			init_flag=0;
		}

		i=energy;

		if (i<AR_NEMAX-1)
		{
			alpha=energy-i;
			sigma=quench[i]+alpha*(quench[i+1] - quench[i]);
		}
		else sigma = quench[AR_NEMAX-1];

		return(sigma);
	}


	static Scalar sigmaEAr4pEAr(Scalar energy) // add, MAL 11/20/10
	// Assume constant sigma with K = sigma*vebar, vebar=sqrt(8*e*Te/pi*m)
	// Fit to Table 1 rate coef K at Te=3eV, in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		return 7.337E-22;
	}

	static Scalar sigmaEAr4pIz(Scalar energy) // add, MAL 11/20/10
	// Assume sigma=sigma0(1-E0/E) with K = sigma0*vebar*exp(-E0/Te), vebar=sqrt(8*e*Te/pi*m)
	// Fit to Table 1 rate coef K at Te=3eV, in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		sigma=3.034E-19*(1.0-2.61/energy);
		if (sigma > 0.0) return sigma;
		else return 0.0;
	}

	static Scalar sigmaEAr4pEArr(Scalar energy) // add, MAL 11/20/10
	// Assume constant sigma with K = sigma*vebar, vebar=sqrt(8*e*Te/pi*m)
	// Fit to Table 1 rate coef K at Te=3eV, in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		return 4.53E-19;
	}

	static Scalar sigmaEAr4pEArm(Scalar energy) // add, MAL 11/20/10
	// Assume constant sigma with K = sigma*vebar, vebar=sqrt(8*e*Te/pi*m)
	// Fit to Table 1 rate coef K at Te=3eV, in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		return 4.53E-19;
	}

	static Scalar sigmaEArrEAr(Scalar energy) // add, MAL 11/20/10
	// Assume constant sigma with K = sigma*vebar, vebar=sqrt(8*e*Te/pi*m)
	// Fit to Table 1 rate coef K at Te=3eV, in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		return 8.361E-22;
	}

	static Scalar sigmaEArrEArm(Scalar energy) // add, MAL 11/20/10
	// Assume sigma=K0/sqrt(2*e*E/m) with K = K0 = const
	// Fit to Table 1 rate coef K at Te=3eV, in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		if (energy > 1E-6) return(1.534E-18/sqrt(energy));
		else return(1.534E-15);
	}

	static Scalar sigmaEArrEAr4p(Scalar energy) // add, MAL 11/20/10
	// Assume sigma=sigma0(1-E0/E) with K = sigma0*vebar*exp(-E0/Te), vebar=sqrt(8*e*Te/pi*m)
	// Fit to Table 1 rate coef K at Te=3eV, in Gudmundsson and Thorsteinsson, PSST 16, 399 (2007)
	{
		sigma=1.344E-18*(1.0-1.59/energy);
		if (sigma > 0.0) return sigma;
		else return 0.0;
	}

	static Scalar sigmaCoulombAr(Scalar energy) // add, MAL 11/25/11
	// Coulomb momentum xfer cross section sigma_m=pi*b_0^2*ln(Lambda), with
	// b_0=e/4*pi*eps0*E_R^2 = 1.439E-9/E_R^2, with E_R=CM energy in eV
	// ln(Lambda)=ln(2*lambda_De/b_0), lambda_De=electron Debye length
	// Use ln(Lambda)=13 -> sigmaCoulomb=8.457E-17/E_R^2
	// To find the scattering cross section the momentum transfer cross section
	// has to be multiplied by a factor of
	// (m1 + m2)/m2 = 2
	{
		if (energy > 0.0026) return(2*8.457E-17/(energy*energy));
		else return (2*1.251E-11);
	}
};
#endif
