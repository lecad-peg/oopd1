// Xenon_gas class includes all properties regarding argon
// by Chul-Hyun Lim
/*
 NOTE TO DEVELOPERS: // MAL200117
 A ReactionGroup is a set of reactions having the same reactant[0], reactant[1], and mcc type. The priority in reactants and reactions is meaningful.  The first reactant (reactant[0]) in the first reaction in a ReactionGroup determines the mass used to evaluate the energy for the cross section: energy = 0.5*mass(reactant[0])*(relative velocity)^2. Uncomment #define debugReactionGroup in reaction.hpp to print all ReactionGroups to the terminal during initialization.
 */

#ifndef XENONGAS_H
#define XENONGAS_H

#include "atomic_properties/gas.hpp"
#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "main/species.hpp"
#include "atomic_properties/argon_gas.hpp"

class Xenon_Gas: public Gas
{
public:
  // mindgame: "sec" refers to "new product".
  // add Species *sec_Xe and Species *sec_Xe_ion to the Xenon_Gas constructor, MAL191211
  Xenon_Gas(oopicListIter<Species> species_iter, Species *sec_e, Species *sec_i, Species *sec_Xe, Species *sec_Xe_ion, bool no_default_secondary_species_flag)
  {
    mcctype = "generic_mcc";
    gastype = XENON_GAS;

    // mindgame: find lists of reactants
    vector<Species *> E_species = Species::get_reaction_species_named_with(species_iter, E);
    vector<Species *> Xe_species = Species::get_reaction_species_named_with(species_iter, Xe);
    vector<Species *> Xe_ion_species = Species::get_reaction_species_named_with(species_iter, Xe_ion);

    // mindgame: if empty, assign default secondary species
    if (!no_default_secondary_species_flag)
    {
      if (!sec_e && !E_species.empty()) sec_e = E_species[0];
      if (!sec_Xe && !Xe_species.empty()) sec_Xe = Xe_species[0]; // add, MAL191211
      if (!sec_i && !Xe_ion_species.empty()) sec_i = Xe_ion_species[0];
      if (!sec_Xe_ion && !Xe_ion_species.empty()) sec_Xe_ion = Xe_ion_species[0]; // add, MAL191211
    }

    for (int j=Xe_species.size()-1; j>=0; j--)
    {
      Xe_species[j]->set_atomicnumber(54);
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaElastic,0.0, E_ELASTIC);
        reactionlist.add(new Reaction(gastype, E_ELASTIC, E_species[i], Xe_species[j], xsection, mcctype()));
        xsection = add_xsection(sigmaExc,9.447, E_EXCITATION);
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Xe_species[j], xsection, mcctype(), true, (Species *)NULL));
        xsection = add_xsection(sigmaqexm, 8.315, E_EXCITATION); // added by JTG November 25 2009
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Xe_species[j], xsection, mcctype(), true, (Species *)NULL));
        xsection = add_xsection(sigmaqexr, 8.437, E_EXCITATION);// added by JTG November 25 2009
        reactionlist.add(new Reaction(gastype, E_EXCITATION, E_species[i], Xe_species[j], xsection, mcctype(), true, (Species *)NULL));
        xsection = add_xsection(sigmaIz,12.12987, E_IONIZATION); // 11.2202 -> 12.12987 as per JTG 11/6/12; MAL 11/14/12
        reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], Xe_species[j], xsection, mcctype(), true, sec_e, sec_i));
      }
      for (int i=Xe_ion_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaISO,0.0, SAMEMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, Xe_ion_species[i], Xe_species[j], xsection, mcctype()));
        xsection = add_xsection(sigmaB,0.0, B_SAMEMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, B_SAMEMASS_ELASTIC, Xe_ion_species[i], Xe_species[j], xsection, mcctype()));
      }
      for (int i=Xe_species.size()-1; i>=0; i--)
               {
           xsection = add_xsection(sigmaXeXe,0.0, SAMEMASS_ELASTIC);
           reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, Xe_species[i], Xe_species[j], xsection, mcctype()));
    	 }
    }

// Add ion-ion coulomb collisions, MAL 11/25/11
    for (int j=Xe_ion_species.size()-1; j>=0; j--)
    {
      Xe_ion_species[j]->set_atomicnumber(54);
      for (int i=Xe_ion_species.size()-1; i>=0; i--) // add for loop for like-particle collisions, MAL 11/23/09
      {
        xsection = add_xsection(sigmaCoulombXe,0.0, SAMEMASS_ELASTIC);
        reactionlist.add(new Reaction(gastype, SAMEMASS_ELASTIC, Xe_ion_species[i], Xe_ion_species[j], xsection, mcctype()));
      }
    }

  }

   static Scalar sigmaElastic(Scalar energy)
  {
    // sknam: Refer to A. Mozumder, J. Chem. Phys. 72, 6289 (1980). (09/2007)
    if (energy < 0.11964) return 4.7887e-19*exp(-energy/0.00994) + 9.9619e-19*exp(-energy/0.07007);
    else if (energy < 0.57703) return 7.0901e-21 + 7.7995e-19*exp(-energy/0.0796);
    else if (energy < 1.24197) return 2.15219e-20 - energy*(5.93371e-20 - 6.11539e-20*energy);
    else if (energy < 2.11606) return 1.03277e-20 - energy*(1.64816e-21 - 2.19614e-20*energy);
    else if (energy < 3.15756) return 1.42969e-19 - energy*(1.00858e-19 - 3.92231e-20*energy);
    else if (energy < 3.74655) return 3.04225e-19 - energy*(1.19411e-19 - 2.89250e-20*energy);
    else if (energy < 4.37229) return 0.00368894e-19 + 8.26053e-17 - energy*(8.40175e-17 - 3.19774e-17*energy + 5.38364e-18*energy*energy - 3.38521e-19*energy*energy*energy);
    else if (energy < 5.04136) return 8.34187e-19 - energy*(2.51762e-19 - 2.91621e-20*energy);
    else if (energy < 5.57637) return 0.0032395e-19 - 7.18147e-19 + energy*(3.67486e-19 - 3.26055e-20*energy);
    else if (energy < 7.66278) return 0.00450879e-19 + 4.99037e-18 - energy*(3.07572e-18 - 7.52107e-19*energy + 8.06286e-20*energy*energy - 3.17676e-21*energy*energy*energy);
    //
    else if (energy < 12.0226) return 2.23972e-19 + energy*(2.61272e-20 - 2.81077e-21*energy);
    else if (energy < 24.3) return 3.39014e-19 + energy*(-2.28038e-20 + 4.64864e-22*energy);
    else if (energy < 70.79) return 9.550e-20 + energy*(-1.80585e-21 + 1.24365e-23*energy);
    else if (energy < 186.2) return 4.54567e-20 + energy*(-2.78976e-22 + 6.93189e-25*energy);
    else if (energy < 537.032) return 2.62e-20 + energy*(-5.72321e-23 + 4.79915e-26*energy);
    else if (energy < 1000) return 1.46698e-20 + energy*(-1.28135e-23 + 3.98769e-27*energy);
    else if (energy < 10000) return 5.855e-18/energy;
    else return 0;

  }
 static Scalar sigmaExc(Scalar energy)
 //
  // Excitation to all higher levels
  // Sakai et al. JPD 24 (1991) 283
  {
		static double exc_k = 0, exc_xsec10k = 0;
		static int exc_init = 0;

		if ( exc_init == 0 ){
			exc_init = 1;		// in initialization
			double en9 = 9990.0;
			double xsec9 = log10(sigmaExc(en9));
			exc_xsec10k = log10(sigmaExc(EN10K));
			exc_k = (exc_xsec10k - xsec9) / (log10(EN10K) - log10(en9));
		}

    if (energy <= 9.447) return(0);
    else if (energy < 47) return(8.5166e-20 * (1 - 9.570/energy));
    else if (energy <= 10000) {
      return((pow(10.0,(-0.2745 * (log10(energy))  - 18.7096))));
      //De-Qi Wen: safely extend the maximum value to 10000, plot checked
		} else {
			// interpolate between last two points (9.9keV - 10keV)
			return pow(10.0, (exc_xsec10k + exc_k*(log10(energy)-log10(EN10K)) ));
		}
  }
   static Scalar sigmaqexm(Scalar energy)
  //
  // Excitation to the metastable states ^3P_0,2
  // Sakai et al. JPD 24 (1991) 283
  {
		static double qexm_k = 0, qexm_xsec10k = 0;
		static int qexm_init = 0;

		if ( qexm_init == 0 ){
			qexm_init = 1;		// in initialization
			double en9 = 9990.0;
			double xsec9 = log10(sigmaqexm(en9));
			qexm_xsec10k = log10(sigmaqexm(EN10K));
			qexm_k = (qexm_xsec10k - xsec9) / (log10(EN10K) - log10(en9));
		}

    if (energy <= 8.315) return(0);
    else if (energy < 17) return(8.649e-21 * (1 - 8.315/energy));
    else if (energy <= 10000) {
      return((pow(10.0,(-4.0619 * (log10(energy)) * (log10(energy))  + 11.3090 * (log10(energy))  - 28.1202))));
      //De-Qi Wen:2019-01-03 safely extend the maximum to 10000, plot checked
 		} else {
			// interpolate between last two points (9.9keV - 10keV)
			return pow(10.0, (qexm_xsec10k + qexm_k*(log10(energy)-log10(EN10K)) ));
		}
  }

 static Scalar sigmaqexr(Scalar energy)
  //
  // Excitation to the radiative state ^3P_1
  // Sakai et al. JPD 24 (1991) 283
  {
		static double qexr_k = 0, qexr_xsec10k = 0;
		static int qexr_init = 0;

		if ( qexr_init == 0 ){
			qexr_init = 1;		// in initialization
			double en9 = 9990.0;
			double xsec9 = log10(sigmaqexr(en9));
			qexr_xsec10k = log10(sigmaqexr(EN10K));
			qexr_k = (qexr_xsec10k - xsec9) / (log10(EN10K) - log10(en9));
		}

    if (energy <= 8.437) return(0);
    else if (energy < 20) return(3.5064e-21 * (1 - 8.437/energy));
    else if (energy < 1000) return((pow(10.0,(0.6960 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 4.3146 * (log10(energy)) * (log10(energy)) + 7.2208 * (log10(energy))  - 24.3172))));
    //De-Qi Wen (2020-01-03): extend the energy maximum to 10000eV by linearly fitting the trend of the original data in loglog plot. energy1=700eV, and energy2=1000eV
    else if (energy <= 10000) {
      return(pow(10.0,-22.694200543703-(-22.694200543703+22.66940552)/(3-2.84507804001425)*(3.0-log10(energy))));
 		} else {
			// interpolate between last two points (9.9keV - 10keV)
			return pow(10.0, (qexr_xsec10k + qexr_k*(log10(energy)-log10(EN10K)) ));
		}
  }



  static Scalar sigmaIz(Scalar energy)
//*---------------------------------------------------------------------------*/
//*  e + Xe ->  Xe+ + 2e   Electron impact ionization                         */
//*---------------------------------------------------------------------------*/
// Rapp et al JCP 43 (1965) 1464
  {
    if (energy <= 12.12987) return 0;
    else if (energy <= 38) return(-6.127e-23 * energy * energy + 4.7495e-21 * energy - 4.8365e-20);
    else if (38 < energy) return((pow(10.0,(0.1001 * (log10(energy)) * (log10(energy)) * (log10(energy)) - 1.1490 * (log10(energy)) * (log10(energy)) + 3.3653 * (log10(energy))  - 22.2076))));
	else return 0; // add this to eliminate warning, MAL 11/26/09
  }


static Scalar sigmaB(Scalar energy)
// BACKWARDS PORTION OF ION-NEUTRAL SCATTERING Piscitelli03
// Revised by SAS August 2014
    {
      return(3.6e-19*pow(1+energy*energy/0.01,0.2)/pow(energy,0.42)/(1+pow(0.09/energy,1.3))/(1+pow(energy/1000,0.25)));
    }

static Scalar sigmaISO(Scalar energy)
// ISOTROPIC PORTION OF ION-NEUTRAL SCATTERING Piscitelli03
// Revised by SAS August 2014
    {
      return (3.39e-19/pow(energy,0.5));
    }


static Scalar sigmaXeXe(Scalar energy)
//*---------------------------------------------------------------------------*/
//*  Xe + Xe -> Xe + Xe   Elastic  Scattering                               */
//*---------------------------------------------------------------------------*/
// Elastic Scattering:
//   These are fits to the data given at
// http://jilawww.colorado.edu/~avp/collision_data/neutralneutral/atomatom.txt
//
 {
  return (pow(10.0,(0.0040 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy))-0.0113 * (log10(energy)) * (log10(energy)) * (log10(energy)) -0.0248 * (log10(energy)) * (log10(energy)) - 0.1678 * (log10(energy))  - 17.3759)));
}


static Scalar sigmaCoulombXe(Scalar energy) // add, MAL 11/25/11
// Coulomb momentum xfer cross section sigma_m=pi*b_0^2*ln(Lambda), with
// b_0=e/4*pi*eps0*E_R^2 = 1.439E-9/E_R^2, with E_R=CM energy in eV
// ln(Lambda)=ln(2*lambda_De/b_0), lambda_De=electron Debye length
// Use ln(Lambda)=13 -> sigmaCoulomb=8.457E-17/E_R^2
// To find the scattering cross section the momentum transfer cross section
// has to be multiplied by a factor of
// (m1 + m2)/m2 = 2
 {
    if (energy > 0.0026) return(2*8.457E-17/(energy*energy));
    else return 2*1.251E-11;
 }

};
#endif
