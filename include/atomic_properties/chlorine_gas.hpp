// Chlorine_gas class includes all properties regarding chlorine
// by Jon Tomas Gudmundsson and Shuo Haung 2012 - 2013
// The reaction set and cross sections are discussed in
// Shuo Huang and J. T. Gudmundsson, A particle-in-cell/Monte Carlo simulation
// of a capacitively coupled chlorine discharge,
// Plasma Sources Science and Technology 22(5) (2013) 055020
/*
 NOTE TO DEVELOPERS: // MAL200117
 A ReactionGroup is a set of reactions having the same reactant[0], reactant[1], and mcc type. The priority in reactants and reactions is meaningful.  The first reactant (reactant[0]) in the first reaction in a ReactionGroup determines the mass used to evaluate the energy for the cross section: energy = 0.5*mass(reactant[0])*(relative velocity)^2. Uncomment #define debugReactionGroup in reaction.hpp to print all ReactionGroups to the terminal during initialization.
 */

#ifndef CHLORINEGAS_H
#define CHLORINEGAS_H

#include "atomic_properties/gas.hpp"
#include "main/species.hpp"

class Chlorine_Gas: public Gas
{
public:
Chlorine_Gas(oopicListIter<Species> species_iter, Species *sec_e, Species *sec_i, Species *sec_Cl, Species *sec_Cl_neg_ion, Species *sec_Cl2, Species *sec_Cl2_ion, Species *sec_Cl_ion, bool no_default_secondary_species_flag)
  {
    mcctype = "chlorine_mcc";
    gastype = Cl2_GAS;

    vector<Species *> E_species = Species::get_reaction_species_named_with(species_iter, E);
    vector<Species *> Cl2_species = Species::get_reaction_species_named_with(species_iter, Cl2);
    vector<Species *> Cl2_ion_species = Species::get_reaction_species_named_with(species_iter, Cl2_ion);
    vector<Species *> Cl_species = Species::get_reaction_species_named_with(species_iter, Cl);
    vector<Species *> Cl_ion_species = Species::get_reaction_species_named_with(species_iter, Cl_ion);
    vector<Species *> Cl_neg_ion_species = Species::get_reaction_species_named_with(species_iter, Cl_neg_ion);

    if (!no_default_secondary_species_flag)
    {
      if (!sec_e && !E_species.empty()) sec_e = E_species[0];

      if (!sec_Cl) sec_Cl = Species::get_species_named_with(species_iter, Cl);
      if (!sec_Cl_neg_ion) sec_Cl_neg_ion = Species::get_species_named_with(species_iter, Cl_neg_ion);
      if (!sec_Cl2) sec_Cl2 = Species::get_species_named_with(species_iter, Cl2);
      if (!sec_Cl2_ion) sec_Cl2_ion = Species::get_species_named_with(species_iter, Cl2_ion);
      if (!sec_Cl_ion) sec_Cl_ion = Species::get_species_named_with(species_iter, Cl_ion);



///////////////////////////////////////// The followings are the presentation of all the reactions.
// order: Cl2, Cl, Cl2+, Cl+, Cl-.

//////// The followings are reactions including Cl2
for (int j=Cl2_species.size()-1; j>=0; j--)
    {
      //// e + Cl2 reactions
      for (int i=E_species.size()-1; i>=0; i--)
      {
        // elastic scattering: e + Cl2 -> e + Cl2
	xsection = add_xsection(sigmaEleCl2, 0.0, E_ELASTIC);
        reactionlist.add(new Reaction(gastype, E_ELASTIC, E_species[i], Cl2_species[j], xsection, mcctype()));

	// ionization: e + Cl2 -> Cl2+ + 2e
        xsection = add_xsection(sigmaCl2Iz, 11.47, E_Cl2IONIZATION);
        reactionlist.add(new Reaction(gastype, E_Cl2IONIZATION, E_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl2_ion, sec_e));

	// dissociative ionization: e + Cl2 -> Cl + Cl+ + 2e
        xsection = add_xsection(sigmaDissSingleIz, 15.7, EDISSSingleIzCl2);
        reactionlist.add(new Reaction(gastype, EDISSSingleIzCl2, E_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl, sec_Cl_ion, sec_e));

	// dissociative double ionization: e + Cl2 -> Cl+ + Cl+ + 3e
        xsection = add_xsection(sigmaDissDoubleIz, 31.13, EDISSDoubleIzCl2);
        reactionlist.add(new Reaction(gastype, EDISSDoubleIzCl2, E_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl_ion, sec_Cl_ion, sec_e, sec_e));

	// dissociative attachment: e + Cl2 -> Cl + Cl-
	xsection = add_xsection(sigmaDissAtt, 0, E_DISS_ATTACHMENT_Cl2);
        reactionlist.add(new Reaction(gastype, E_DISS_ATTACHMENT_Cl2, E_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl_neg_ion, sec_Cl));

	// polar dissociation: e + Cl2 -> Cl+ + Cl- + e
	xsection = add_xsection(sigmaPolarDiss, 11.9, POLARDISSCl2);
        reactionlist.add(new Reaction(gastype, POLARDISSCl2, E_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl_neg_ion, sec_Cl_ion, sec_e));

	// rotational excitation: e + Cl2(J=0) -> e + Cl2 (J>1)
	xsection = add_xsection(sigmaExcv0v1, 0.01, E_LOSS_Cl2);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl2, E_species[i], Cl2_species[j], xsection, mcctype()));


    	// vibrational excitation 1: e + Cl2(v=0) -> e + Cl2 (v=1)
	xsection = add_xsection(sigmaExcv0v1, 0.07, E_LOSS_Cl2);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl2, E_species[i], Cl2_species[j], xsection, mcctype()));

	// vibrational excitation 2: e + Cl2(v=0) -> e + Cl2 (v=2)
	xsection = add_xsection(sigmaExcv0v2, 0.14, E_LOSS_Cl2);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl2, E_species[i], Cl2_species[j], xsection, mcctype()));

	// vibrational excitation 3: e + Cl2(v=0) -> e + Cl2 (v=3)
	xsection = add_xsection(sigmaExcv0v3, 0.21, E_LOSS_Cl2);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl2, E_species[i], Cl2_species[j], xsection, mcctype()));

	// electronic excitation 1: e + Cl2 -> e + Cl2 (3Pu)
	xsection = add_xsection(sigmaExc3Pu, 3.24, E_LOSS_Cl2_3Pu);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl2_3Pu, E_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl, sec_Cl));

	// electronic excitation 2: e + Cl2 -> e + Cl2 (1Pu)
	xsection = add_xsection(sigmaExc1Pu, 4.04, E_LOSS_Cl2_1Pu);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl2_1Pu, E_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl, sec_Cl));

	// electronic excitation 3: e + Cl2 -> e + Cl2 (3Pg)
	xsection = add_xsection(sigmaExc3Pg, 6.23, E_LOSS_Cl2_3Pg);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl2_3Pg, E_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl, sec_Cl));

	// electronic excitation 4: e + Cl2 -> e + Cl2 (1Pg)
	xsection = add_xsection(sigmaExc1Pg, 6.86, E_LOSS_Cl2_1Pg);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl2_1Pg, E_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl, sec_Cl));

	// electronic excitation 5: e + Cl2 -> e + Cl2 (3Su)
	xsection = add_xsection(sigmaExc3Su, 6.80, E_LOSS_Cl2_3Su);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl2_3Su, E_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl, sec_Cl));

	// electronic excitation 6: e + Cl2 -> e + Cl2 (R1Pu)
	xsection = add_xsection(sigmaExcR1Pu, 9.22, E_LOSS_Cl2_R1Pu);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl2_R1Pu, E_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl, sec_Cl));

	// electronic excitation 7: e + Cl2 -> e + Cl2 (R1Su)
	xsection = add_xsection(sigmaExcR1Su, 9.32, E_LOSS_Cl2_R1Su);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl2_R1Su, E_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl, sec_Cl));



       } //// end of e + Cl2 reactions


      //// Cl2 + Cl2 reactions
      for (int i=Cl2_species.size()-1; i>=0; i--)
      {
	// elastic scattering: Cl2 + Cl2 -> Cl2 + Cl2
        xsection = add_xsection(sigmaElCl2Cl2,0.0, Cl2Cl2_ELASTIC);
        reactionlist.add(new Reaction(gastype, Cl2Cl2_ELASTIC, Cl2_species[i], Cl2_species[j], xsection, mcctype()));
      }//// end of Cl2 + Cl2 reactions


      //// Cl + Cl2 reactions
	for (int i=Cl_species.size()-1; i>=0; i--)
      {
      // elastic scattering: Cl + Cl2 -> Cl + Cl2
        xsection = add_xsection(sigmaElClCl2,0.0, El_ClCl2);
        reactionlist.add(new Reaction(gastype, El_ClCl2, Cl_species[i], Cl2_species[j], xsection, mcctype()));

     }
      //// end of Cl + Cl2 reactions


      ////  Cl2+ + Cl2 reactions
      for (int i=Cl2_ion_species.size()-1; i>=0; i--)
      {
	// charge exchange: Cl2+ + Cl2 -> Cl2 + Cl2+
        xsection = add_xsection(sigmaCEposCl2Cl2,0.0, CEposCl2Cl2);
        reactionlist.add(new Reaction(gastype, CEposCl2Cl2, Cl2_ion_species[i], Cl2_species[j], xsection, mcctype()));

	// elastic scattering: Cl2+ + Cl2 -> Cl2+ + Cl2
        xsection = add_xsection(sigmaElposCl2Cl2,0.0, El_posCl2Cl2);
        reactionlist.add(new Reaction(gastype, El_posCl2Cl2, Cl2_ion_species[i], Cl2_species[j], xsection, mcctype()));

	// fragmentation: Cl2+ + Cl2 -> Cl+ + Cl + Cl2
        xsection = add_xsection(sigmaFRAG_Cl2,7.04, Cl2FRAG); //3.52=15-11.48, 7.04=2*3.52
        //xsection = add_xsection(sigmaFRAG_Cl2, 12.6, Cl2FRAG); //12.6=6.3*2, 6.3 is the threshold in CMF from fig 10 in moran68_3411, 2 is (m(O2+)+m(O2))/m(O2), which is used to transform CMF to TRF. similar to sigmaneD in oxygen_gas.hpp
        reactionlist.add(new Reaction(gastype, Cl2FRAG, Cl2_ion_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl2, sec_Cl, sec_Cl_ion));


      } //// end of Cl2+ + Cl2 reactions


      ////  Cl+ + Cl2 reactions (To be added...)
      for (int i=Cl_ion_species.size()-1; i>=0; i--)
      {
	// charge exchange: Cl+ + Cl2 -> Cl + Cl2+
        xsection = add_xsection(sigmaCEposClCl2,0.0, CEposClCl2);
        reaction = new Reaction(gastype, CEposClCl2, Cl_ion_species[i], Cl2_species[j], xsection, mcctype(), true, sec_Cl2_ion, sec_Cl);
	 reactionlist.add(reaction);
         reaction -> add_characteristic_value(1.52); // energy released in CM system JTG July 7 2013


	// elastic scattering: Cl+ + Cl2 -> Cl+ + Cl2
        xsection = add_xsection(sigmaElposClCl2,0.0, El_posClCl2);
        reactionlist.add(new Reaction(gastype, El_posClCl2, Cl_ion_species[i], Cl2_species[j], xsection, mcctype()));

	}
      ////  end of Cl+ + Cl2 reactions



      //// Cl- + Cl2 reactions
      for (int i=Cl_neg_ion_species.size()-1; i>=0; i--)
      {
	// electron detachment from a negative ion by Cl2: Cl- + Cl2 -> Cl + Cl2 + e
//This cross section is the target-rest-frame cross section. So the threshold should be 5.4.
        xsection = add_xsection(sigmaDetbyCl2, 5.415, NEGCl2_DETCH);
// 5.415=3.61*(3/2). 3.61 eV is the electron affinity of Cl. 3.61 (in CM system)->3.61*(3/2)=5.415  in target rest frame.
        reactionlist.add(new Reaction(gastype, NEGCl2_DETCH, Cl_neg_ion_species[i], Cl2_species[j],  xsection, mcctype(),true, sec_Cl2, sec_Cl, sec_e));

	// elastic scattering: Cl- + Cl2 -> Cl- + Cl2
        xsection = add_xsection(sigmaneE,0.0, El_negClCl2);
        reactionlist.add(new Reaction(gastype, El_negClCl2, Cl_neg_ion_species[i], Cl2_species[j], xsection, mcctype()));

      } //// end of Cl- + Cl2 reactions



    } //////// end of reactions including Cl2




//////// The followings are reactions including Cl (without Cl2)
for (int j=Cl_species.size()-1; j>=0; j--)
    {

      //// e + Cl reactions
      for (int i=E_species.size()-1; i>=0; i--)
      {
	// elastic scattering: e + Cl -> e + Cl
	xsection = add_xsection(sigmaEleCl, 0.0, E_ELASTIC);
        reactionlist.add(new Reaction(gastype, E_ELASTIC, E_species[i], Cl_species[j], xsection, mcctype()));

	// ionization: e + Cl -> Cl+ + e + e
	xsection = add_xsection(sigmaClIz, 12.99, E_ClIONIZATION);
        reactionlist.add(new Reaction(gastype, E_ClIONIZATION, E_species[i], Cl_species[j], xsection, mcctype(), true, sec_Cl_ion, sec_e));

        // excitation 1: e + Cl -> e + Cl (4s)
	xsection = add_xsection(sigmaExc4s, 9.1, E_LOSS_Cl);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl, E_species[i], Cl_species[j], xsection, mcctype()));

	// excitation 2: e + Cl -> e + Cl (4p)
	xsection = add_xsection(sigmaExc4p, 10.5, E_LOSS_Cl);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl, E_species[i], Cl_species[j], xsection, mcctype()));

	// excitation 3: e + Cl -> e + Cl (3d)
	xsection = add_xsection(sigmaExc3d, 11.2, E_LOSS_Cl);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl, E_species[i], Cl_species[j], xsection, mcctype()));

	// excitation 4: e + Cl -> e + Cl (5s)
	xsection = add_xsection(sigmaExc5s, 11.4, E_LOSS_Cl);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl, E_species[i], Cl_species[j], xsection, mcctype()));

	// excitation 5: e + Cl -> e + Cl (5p)
	xsection = add_xsection(sigmaExc5p, 11.8, E_LOSS_Cl);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl, E_species[i], Cl_species[j], xsection, mcctype()));

	// excitation 6: e + Cl -> e + Cl (4d)
	xsection = add_xsection(sigmaExc4d, 12.0, E_LOSS_Cl);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl, E_species[i], Cl_species[j], xsection, mcctype()));

	// excitation 7: e + Cl -> e + Cl (6s)
	xsection = add_xsection(sigmaExc6s, 12.1, E_LOSS_Cl);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl, E_species[i], Cl_species[j], xsection, mcctype()));

	// excitation 8: e + Cl -> e + Cl (5d)
	xsection = add_xsection(sigmaExc5d, 12.4, E_LOSS_Cl);
        reactionlist.add(new Reaction(gastype, E_LOSS_Cl, E_species[i], Cl_species[j], xsection, mcctype()));



       } //// end of e + Cl reactions



	//// Cl + Cl reactions
	  for (int i=Cl_species.size()-1; i>=0; i--)
	  {
	 	// elastic scattering:  Cl + Cl -> Cl + Cl
 xsection = add_xsection(sigmaElClCl,0.0, ClCl_ELASTIC);
        reactionlist.add(new Reaction(gastype, ClCl_ELASTIC, Cl_species[i], Cl_species[j], xsection, mcctype()));
	  }//// end of Cl + Cl reactions



	//// Cl2+ + Cl reactions
	for (int i=Cl2_ion_species.size()-1; i>=0; i--)
	  {
	 // charge exchange:  Cl2+ + Cl -> Cl2 + Cl+ // 1.52=12.99(Iz energy of Cl)-11.47(Iz energy of Cl2), 1.52*3=4.56
	 xsection = add_xsection(sigmaCEposCl2Cl,4.56, CEposCl2Cl);
reaction= new Reaction(gastype, CEposCl2Cl, Cl2_ion_species[i], Cl_species[j], xsection, mcctype(),true, sec_Cl2, sec_Cl_ion);

        reactionlist.add(reaction);

    reaction -> add_characteristic_value(-1.52); // energy "released" in CM system; the reaction absorbs 1.52 eV in CM
	  }//// end of Cl2+ + Cl reactions


	//// Cl+ + Cl reactions
	for (int i=Cl_ion_species.size()-1; i>=0; i--)
	  {
	// charge exchange:  Cl+ + Cl -> Cl + Cl+
 	xsection = add_xsection(sigmaCEposClCl,0.0, CEposClCl);
        reactionlist.add(new Reaction(gastype, CEposClCl, Cl_ion_species[i], Cl_species[j], xsection, mcctype()));


	  } //// end of Cl+ + Cl reactions


      //// Cl- + Cl reactions
      for (int i=Cl_neg_ion_species.size()-1; i>=0; i--)
      {
        // associative detachment: Cl- + Cl -> Cl2 + e
	xsection = add_xsection(sigmanegOD, 2.26, NEGCl_DETACHMENT);// 1.13 is the difference between electron affinity of Cl (3.61 eV) and the dissociation energy of Cl2 (2.48 eV), and we assume the threshold to be twice of this value.1.13*2=2.26
	reactionlist.add(new Reaction(gastype, NEGCl_DETACHMENT, Cl_neg_ion_species[i], Cl_species[j], xsection, mcctype(), true,  sec_Cl2, sec_e));

       		// charge exchange:  Cl- + Cl -> Cl + Cl-
 xsection = add_xsection(sigmaCEnegClCl,0.0, CEnegClCl);
        reactionlist.add(new Reaction(gastype, CEnegClCl, Cl_neg_ion_species[i], Cl_species[j], xsection, mcctype()));

	} //// end of Cl- + Cl reactions



    }//////// end of reactions including Cl (without Cl2)



//////// The followings are reactions including Cl2+ (without Cl2, Cl)
for (int j=Cl2_ion_species.size()-1; j>=0; j--)
    {


      //// e + Cl2+ reactions
      for (int i=E_species.size()-1; i>=0; i--)
      {
        // dissociative recombination: e + Cl2+ -> Cl + Cl
	xsection = add_xsection(sigmaDissRec, 0, E_DISS_REC_posCl2);
	reactionlist.add(new Reaction(gastype, E_DISS_REC_posCl2, E_species[i], Cl2_ion_species[j], xsection, mcctype(), true, sec_Cl, sec_Cl));
	} //// end of e + Cl2+ reactions


      //// Cl- + Cl2+ reactions
      for (int i=Cl_neg_ion_species.size()-1; i>=0; i--)
      {
	// neutralization: Cl- + Cl2+ -> Cl + Cl + Cl
	xsection = add_xsection(sigmaMNposCl2negCl,0, MN_posCl2negCl);
        reactionlist.add(new Reaction(gastype, MN_posCl2negCl, Cl_neg_ion_species[i], Cl2_ion_species[j], xsection, mcctype(),true, sec_Cl, sec_Cl,sec_Cl));
	}


      //// end of Cl- + Cl2+ reactions


    }//////// end of reactions including Cl2+ (without Cl2, Cl)



//////// The followings are reactions including Cl+ (without Cl2, Cl, Cl2+)
for (int j=Cl_ion_species.size()-1; j>=0; j--)
    {
      //// Cl- + Cl+ reactions
	for (int i=Cl_neg_ion_species.size()-1; i>=0; i--)
	{
	// neutralization: Cl- + Cl+ -> Cl + Cl
	xsection = add_xsection(sigmaMNposClnegCl,0, MN_posClnegCl);
        reactionlist.add(new Reaction(gastype, MN_posClnegCl, Cl_neg_ion_species[i], Cl_ion_species[j], xsection, mcctype(),true, sec_Cl, sec_Cl));
	}//// end of Cl- + Cl+ reactions


    }//////// end of reactions including Cl+ (without Cl2, Cl, Cl2+)



//////// The followings are reactions including Cl- (without Cl2, Cl, Cl2+, Cl+)
for (int j=Cl_neg_ion_species.size()-1; j>=0; j--)
    {
      //// e + Cl- reactions
      for (int i=E_species.size()-1; i>=0; i--)
      {
        // detachment: e + Cl- -> Cl + 2e
	xsection = add_xsection(sigmaeSingleDet,3.4, E_DETACHMENT_negCl);
        reactionlist.add(new Reaction(gastype, E_DETACHMENT_negCl, E_species[i], Cl_neg_ion_species[j], xsection, mcctype(),true, sec_Cl, sec_e));

        // double detachment: e + Cl- -> Cl+ + 3e
	xsection = add_xsection(sigmaeDoubleDet,28.6, E_Double_DET_negCl);
        reactionlist.add(new Reaction(gastype, E_Double_DET_negCl, E_species[i], Cl_neg_ion_species[j], xsection, mcctype(),true, sec_Cl_ion, sec_e,sec_e));


	}//// end of e + Cl- reactions


    } //////// end of reactions including Cl- (without Cl2, Cl, Cl2+, Cl+)


///////////////////////////////////////// The above are the presentations of all the reactions.

}


  } // In the domain of public area, this bracket corresponds to the one that below "Chlorine_Gas(oopicListIter<Species>", constituting the construction fuction.






//////////////////////////////////////// The followings are cross sections for each reaction.


////////cross section of Cl2 included reactions

//// e + Cl2 reactions

  // elastic scattering: e + Cl2 -> e + Cl2
  // Gregorio and Pitchford presented a set of cross sections for electron scattering from the ground state neutral chlorine molecules in the energy range from 0.01 to 100 eV.
  // We have extrapolated this cross section to 10^4 eV assuming σ ∝ E^{−1/2} , where E is the electron energy
  // J Gregorio and L C Pitchford. Updated compilation of electron-Cl2 scattering cross sections. Plasma Sources Science and Technology, 21(3):032002, 2012.
  static Scalar sigmaEleCl2(Scalar energy)
  {
    	if (energy <= 0.01983) return((pow(10.0,( -0.08001 * (log10(energy))  - 18.531 ))));
    	else if (energy <= 0.07882) return((pow(10.0,(126.721 * (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy)) * (log10(energy)) + 932.726* (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 2731.99 * (log10(energy)) * (log10(energy)) * (log10(energy)) +3976.73 * (log10(energy)) * (log10(energy))  + 2872.82 * (log10(energy)) + 803.945))));
	else if (energy <= 0.1509) return((pow(10.0,(1030.6 * (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy)) * (log10(energy)) + 4555.11* (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 7980.4 * (log10(energy)) * (log10(energy)) * (log10(energy)) +6920.77 * (log10(energy)) * (log10(energy))  + 2967.19 * (log10(energy)) + 483.284))));
	else if (energy <= 0.23) return((pow(10.0,(  550.354* (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 1669.53 * (log10(energy)) * (log10(energy)) * (log10(energy)) + 1875.12* (log10(energy)) * (log10(energy))  + 920.421 * (log10(energy)) + 146.57))));
	else if (energy <= 2.600) return((pow(10.0,(  2.16059* (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) -1.0257 * (log10(energy)) * (log10(energy)) * (log10(energy)) -0.8121* (log10(energy)) * (log10(energy))  + 1.158 * (log10(energy)) -19.321))));
	else if (energy <= 13.5) return((pow(10.0,( 1.43668* (log10(energy)) * (log10(energy)) * (log10(energy))* (log10(energy)) * (log10(energy)) -6.7312 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 10.8708 * (log10(energy)) * (log10(energy)) * (log10(energy)) -7.773 * (log10(energy)) * (log10(energy))  + 2.95464 * (log10(energy)) -19.471 ))));
	else if (energy <= 10000) return((pow(10.0,( -0.5001 * (log10(energy))  - 18.135 ))));
	else return 0;
  }

  // ionization: e + Cl2 -> e + e + Cl2+
  // A fit to the measured data from Basner and Becker (2004), New Journal of physics 6(1), 118.
  // We have extrapolated this cross section to 10^4 eV assuming σ ∝ E^{−1} , where E is the electron energy
static Scalar sigmaCl2Iz(Scalar energy)
 {
    	if (energy <= 11.47) return 0;
	else if (energy <= 11.97) return((pow(10.0,( 150.135 * (log10(energy))  -182.82 ))));
    	else if (energy <= 22) return((pow(10.0,( -542.757 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) +2746.163  * (log10(energy)) * (log10(energy)) * (log10(energy)) -5217.19 * (log10(energy)) * (log10(energy))  +4413.649  * (log10(energy)) -1423.36 ))));
	else if (energy <= 44) return((pow(10.0,( 56.117 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) -325.485  * (log10(energy)) * (log10(energy)) * (log10(energy)) +703.173 * (log10(energy)) * (log10(energy))  -670.116  * (log10(energy)) +218.119 ))));
	else if (energy <= 900) return((pow(10.0,( 0.1137 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) -0.9701  * (log10(energy)) * (log10(energy)) * (log10(energy)) +2.6531 * (log10(energy)) * (log10(energy))  -2.6889  * (log10(energy)) -18.6422 ))));
	else if (energy <= 10000) return((pow(10.0,( -1 * (log10(energy)) -16.834))));
	else return 0;
 }

  // dissociative single ionization: e + Cl2 -> Cl + Cl+ + e
  // This cross section is obtained by multiplying the branching ratio given by Calandra et al., (2000), JCP 112(24),10821-10830 by the cross section used for the non-dissociative ionization (e + Cl2 -> e + e + Cl2+) cross section. And this cross section is extrapolated in the same way as e + Cl2 -> e + e + Cl2+.
static Scalar sigmaDissSingleIz(Scalar energy)
{
	if (energy <= 15.7) return 0;
	else if (energy <= 25.9) return((pow(10.0,( 449.822 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-1829.66 * (log10(energy)) * (log10(energy))  +2490.43  * (log10(energy)) 	-1154.91 ))));
	else if (energy <= 28) return((pow(10.0,( 222.130 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-987.134 * (log10(energy)) * (log10(energy))  + 1465.96 * (log10(energy)) 	-747.147 ))));
	else if (energy <= 38) return((pow(10.0,( 3438.992 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-20926.558  * (log10(energy)) * (log10(energy)) * (log10(energy)) +47708.448 * (log10(energy)) * (log10(energy))  	-48293.967  * (log10(energy)) +18294.609 ))));
	else if (energy <= 190) return((pow(10.0,( 4.05459 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-30.6323  * (log10(energy)) * (log10(energy)) * (log10(energy)) +86.6625 * (log10(energy)) * (log10(energy))  	-109.178  * (log10(energy)) +32.1939 ))));
	else if (energy <= 900) return((pow(10.0,( 0.566730 * (log10(energy)) * (log10(energy)) * (log10(energy)) -4.82207 * (log10(energy)) * (log10(energy))  +12.9997  * (log10(energy)) -31.0117 ))));
	else if (energy <= 10000) return((pow(10.0,( -1 * (log10(energy))  -17.1310 ))));
	else return 0;
}


  // dissociative double ionization: e + Cl2 -> Cl+ + Cl+ + 2e
  // This cross section is obtained by multiplying the branching ratio given by Calandra et al., (2000), JCP 112(24),10821-10830 by the cross section used for the non-dissociative ionization (e + Cl2 -> e + e + Cl2+) cross section. And this cross section is extrapolated in the same way as e + Cl2 -> e + e + Cl2+.
static Scalar sigmaDissDoubleIz(Scalar energy)
 {
	if (energy <= 31.13 ) return 0;
	else if (energy <= 34) return((pow(10.0,( -274.0458  * (log10(energy)) * (log10(energy))  +872.5405  * (log10(energy)) -714.7985 ))));
	else if (energy <= 47.8) return((pow(10.0,( 17959.464 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-114216.778  * (log10(energy)) * (log10(energy)) * (log10(energy)) +272211.365 * (log10(energy)) * (log10(energy))  	-288135.197 * (log10(energy)) +114267.638 ))));
	else if (energy <= 59.3) return((pow(10.0,( -26737.06 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) +186303.41  * (log10(energy)) * (log10(energy)) * (log10(energy))	-486712.99 * (log10(energy)) * (log10(energy))  + 565010.75 * (log10(energy)) -245935.69 ))));
	else if (energy <= 121) return((pow(10.0,( 5.711036 * (log10(energy)) * (log10(energy)) * (log10(energy)) -37.09219 * (log10(energy)) * (log10(energy))  +79.62404  * (log10(energy)) 	-76.27494 ))));
	else if (energy <= 900) return((pow(10.0,( -0.549728 * (log10(energy)) * (log10(energy)) * (log10(energy)) +4.104523 * (log10(energy)) * (log10(energy))  	-10.71168  * (log10(energy)) -10.26517 ))));
	else if (energy <= 10000) return((pow(10.0,( -1.00006 * (log10(energy))  -17.30606 ))));
	else return 0;
 }



  // dissociative attachment: e + Cl2 -> Cl + Cl-
  // The cross section is compiled from two cross sections. For energies above 0.2 eV, we use measured data from Kurepa and Belic (1978), JPB 11(21), 3719-3729 after increasing its value by 30% give agreement with electron swarm data (Christophorou(1999), Journal of Physical and Chemical Reference Data, 28(1):131–169 ). For the energies below 0.2 eV, we use measured data from Ruf et al. (2004), JPB 37(1), 41-62.
  // July 26, 2012
static Scalar sigmaDissAtt(Scalar energy)
{
    	if (energy <= 0.00028) return((pow(10.0,( 0.36786 * (log10(energy))  -19.138))));
	else if (energy <= 0.006) return((pow(10.0,(  0.09543 * (log10(energy)) * (log10(energy))  + 0.9858 * (log10(energy)) -18.147 ))));
	else if (energy <= 0.2) return((pow(10.0,( -0.4997 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) -3.5439  * (log10(energy)) * (log10(energy)) * (log10(energy)) -9.5569 * (log10(energy)) * (log10(energy))  -11.289  * (log10(energy)) -24.47 ))));
	else if (energy <= 2) return((pow(10.0,( 0.99252 * (log10(energy)) * (log10(energy)) * (log10(energy)) +5.40784 * (log10(energy)) * (log10(energy))  + 0.32084 * (log10(energy)) -22.247 ))));
	else if (energy <= 4.2) return((pow(10.0,( 535.794 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) -852.49  * (log10(energy)) * (log10(energy)) * (log10(energy)) +478.888 * (log10(energy)) * (log10(energy))  -111.58 * (log10(energy)) -12.574 ))));
	else if (energy <= 6.7) return((pow(10.0,(  -21.652 * (log10(energy)) * (log10(energy))  + 33.291 * (log10(energy)) -34.011 ))));
	else if (energy <= 8.4) return((pow(10.0,(  -17.38 * (log10(energy)) * (log10(energy))  + 20.9912 * (log10(energy)) -26.776 ))));
	else if (energy <= 10000) return((pow(10.0,( -0.9968 * (log10(energy))  -21.315 ))));
	else return 0;
 }

  // polar dissociation: e + Cl2 -> Cl+ + Cl- + e
  // This cross section is taken from the revised version in Golovitskii (2000), Technical Physics 45(5), 532-537.
  // Aug 26, 2012
static Scalar sigmaPolarDiss(Scalar energy)
{
	if (energy <= 11.9) return 0;
	else if (energy <= 24) return((pow(10.0,( -8.579564 * (log10(energy)) * (log10(energy)) * (log10(energy)) +23.55637 * (log10(energy)) * (log10(energy))  	-9.648759 * (log10(energy)) -31.2090 ))));
	else if (energy <= 43) return((pow(10.0,( -555.652 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 3525.639 * (log10(energy)) * (log10(energy)) * (log10(energy)) -8366.367 * (log10(energy)) * (log10(energy))  + 8799.404 * (log10(energy)) -3482.886 ))));
	else if (energy <= 61) return((pow(10.0,( -3219.256 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 21904.75 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-55854.77 * (log10(energy)) * (log10(energy))  + 63260.00 * (log10(energy)) -26874.32 ))));
	else if (energy <= 87) return((pow(10.0,( -203.6662 * (log10(energy)) * (log10(energy)) * (log10(energy)) +1121.873 * (log10(energy)) * (log10(energy))  	-2056.064  * (log10(energy)) +1232.133 ))));
	else if (energy <= 10000) return((pow(10.0,( -1.692017 * (log10(energy))  -18.11264 ))));
	else return 0;
}


  // rotational excitation: e + Cl2(J=0) -> e + Cl2(J>0)
  // From 0.01 (threshold) - 2 eV, we use the calculated cross section in Kutz et al., (1995), PRA 51(5), 3819-3830; from 2-100 eV, we use cross section from experiment measurement in Gote and Ehrhardt (1995), JPB 28(17), 3957-3986. From 100-10000 eV, we use extrapolation assuming \A6ҡ\D8 1/electron energy.
  // Aug 25, 2012
static Scalar sigmaRotExc(Scalar energy)
{
	if (energy <= 0.01) return 0;
	else if (energy <= 0.15) return((pow(10.0,( -0.0991307 * (log10(energy)) * (log10(energy)) * (log10(energy)) -1.0489 * (log10(energy)) * (log10(energy))  -3.33625  * (log10(energy)) -21.1856 ))));
	else if (energy <= 0.417) return((pow(10.0,( 49.5588 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 135.370 * (log10(energy)) * (log10(energy)) * (log10(energy)) +	137.126 * (log10(energy)) * (log10(energy))  +	58.5423  * (log10(energy)) -11.0742 ))));
	else if (energy <= 4.7) return((pow(10.0,( 2.05008 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-0.989376  * (log10(energy)) * (log10(energy)) * (log10(energy)) -1.42143 * (log10(energy)) * (log10(energy))  + 1.61077 * (log10(energy)) -19.195 ))));
	else if (energy <= 70) return((pow(10.0,( 0.139840 * (log10(energy)) * (log10(energy)) * (log10(energy)) -1.28351 * (log10(energy)) * (log10(energy))  +2.23026  * (log10(energy)) 	-19.6005 ))));
	else if (energy <= 10000) return((pow(10.0,( -0.943358 * (log10(energy))  -17.2382 ))));
	else return 0;
}

  // vibrational excitations 1: e + Cl2(v=0) -> e + Cl2(v=1)
  // A fit to the calculated data from Kolorenc and Horacek (2006), PRA 74(6), 062703
  // July 29, 2012
static Scalar sigmaExcv0v1(Scalar energy)
{
	if (energy <= 0.07) return 0;
	else if (energy <= 0.0823) return ((pow(10.0,(  -234.715 * (log10(energy)) * (log10(energy))  -489.03  * (log10(energy)) -276.72 ))));
	else if (energy <= 0.21) return ((pow(10.0,( 21.677 * (log10(energy)) * (log10(energy)) * (log10(energy)) +49.565 * (log10(energy)) * (log10(energy))  + 39.194 * (log10(energy)) -10.548 ))));
	else if (energy <= 1.253) return ((pow(10.0,( 0.2844 * (log10(energy)) * (log10(energy)) * (log10(energy)) -1.5338* (log10(energy)) * (log10(energy))  -1.3599  * (log10(energy)) -21.224 ))));
	else if (energy <= 10000) return((pow(10.0,( -1.7694 * (log10(energy))  -21.2 ))));
	else return 0;
}

  // vibrational excitations 2: e + Cl2(v=0) -> e + Cl2(v=2)
  // A fit to the calculated data from Kolorenc and Horacek (2006), PRA 74(6), 062703
  // July 29, 2012
static Scalar sigmaExcv0v2(Scalar energy)
{
	if (energy <= 0.14) return 0;
	else if (energy <= 0.165) return ((pow(10.0,(  -275.69 * (log10(energy)) * (log10(energy))  -412.268  * (log10(energy)) -176.019 ))));
	else if (energy <= 0.254) return ((pow(10.0,( 47.8213 * (log10(energy)) * (log10(energy)) * (log10(energy)) +86.1685 * (log10(energy)) * (log10(energy))  + 53.481 * (log10(energy)) -10.233 ))));
	else if (energy <= 1.3) return ((pow(10.0,( 2.2684 * (log10(energy)) * (log10(energy)) * (log10(energy)) -1.0447* (log10(energy)) * (log10(energy))  -2.1117  * (log10(energy)) -22.0324 ))));
	else if (energy <= 10000) return((pow(10.0,( -2.5586 * (log10(energy))  -21.996 ))));
	else return 0;
}

  // vibrational excitations 3: e + Cl2(v=0) -> e + Cl2(v=3)
  // A fit to the calculated data from Kolorenc and Horacek (2006), PRA 74(6), 062703
  // July 29, 2012
static Scalar sigmaExcv0v3(Scalar energy)
{
	if (energy <= 0.21) return 0;
	else if (energy <= 0.21699) return ((pow(10.0,(  -3232.6863 * (log10(energy)) * (log10(energy))  -4123.492  * (log10(energy)) -1335.77 ))));
	else if (energy <= 0.265) return ((pow(10.0,( 428.7149 * (log10(energy)) * (log10(energy)) * (log10(energy)) +700.1616 * (log10(energy)) * (log10(energy))  + 382.5317 * (log10(energy)) +47.7898 ))));
	else if (energy <= 1.2759) return ((pow(10.0,( 6.0035 * (log10(energy)) * (log10(energy)) * (log10(energy)) -0.201* (log10(energy)) * (log10(energy))  -2.9373  * (log10(energy)) -22.657))));
	else if (energy <= 10000) return((pow(10.0,( -3.4103 * (log10(energy))  -22.6123 ))));
	else return 0;
}

  // electronic excitation 1: e + Cl2 -> e + Cl2 (3Pu)
  // A fit to the calculated data from Rescigno (1994), PRA 50(2), 1382-1389
  // July 29, 2012
static Scalar sigmaExc3Pu(Scalar energy)
{
	if (energy <= 3.24) return 0;
	else if (energy <= 4.58) return ((pow(10.0,( 24.5747 * (log10(energy))  -37.5517 ))));
	else if (energy <= 5) return((pow(10.0,(  -94.0381 * (log10(energy)) * (log10(energy))  + 138.091 * (log10(energy)) -71.5 ))));
	else if (energy <= 25) return ((pow(10.0,( 2.0799 * (log10(energy)) * (log10(energy)) * (log10(energy)) -8.8932* (log10(energy)) * (log10(energy))  +12.2784  * (log10(energy)) -25.8654))));
	else if (energy <= 10000) return((pow(10.0,( -0.9211 * (log10(energy))  -19.1131 ))));
	else return 0;
}

  // electronic excitation 2: e + Cl2 -> e + Cl2 (1Pu)
  // A fit to the calculated data from Rescigno (1994), PRA 50(2), 1382-1389
  // July 29, 2012
static Scalar sigmaExc1Pu(Scalar energy)
{
	if (energy <= 4.04) return 0;
	else if (energy <= 4.36) return((pow(10.0,( 98.4307 * (log10(energy))  -84.6866 ))));
	else if (energy <= 4.54) return((pow(10.0,(  -274.033 * (log10(energy)) * (log10(energy))  +378.895  * (log10(energy)) -151.981 ))));
	else if (energy <= 8) return((pow(10.0,( 80.8766 * (log10(energy)) * (log10(energy)) * (log10(energy)) -196.815 * (log10(energy)) * (log10(energy))  +161.055  * (log10(energy)) -65.1207 ))));
	else if (energy <= 15.9) return((pow(10.0,( 57.1922 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) -247.053  * (log10(energy)) * (log10(energy)) * (log10(energy)) +393.137 * (log10(energy)) * (log10(energy))  -272.7848  * (log10(energy)) +49.0151 ))));
	else if (energy <= 24.5) return((pow(10.0,( -451.1878 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 2298.07 * (log10(energy)) * (log10(energy)) * (log10(energy)) -4371.2 * (log10(energy)) * (log10(energy))  + 3679.34 * (log10(energy)) -1176.675 ))));
	else if (energy <= 30) return((pow(10.0,( -23.8017  * (log10(energy)) * (log10(energy))  +65.3553  * (log10(energy)) -65.3899 ))));
	else if (energy <= 10000) return((pow(10.0,( -3.0959 * (log10(energy))  -16.2129 ))));
	else return 0;
}

  // electronic excitation 3: e + Cl2 -> e + Cl2 (3Pg)
  // A fit to the calculated data from Rescigno (1994), PRA 50(2), 1382-1389
  // July 29, 2012
static Scalar sigmaExc3Pg(Scalar energy)
{
	if (energy <= 6.23) return 0;
	else if (energy <= 7.43) return((pow(10.0,( 52.4497 * (log10(energy))  -66.7015 ))));
	else if (energy <= 14.8) return((pow(10.0,( 116.819 * (log10(energy)) * (log10(energy)) * (log10(energy)) -353.122 * (log10(energy)) * (log10(energy))  + 354.168 * (log10(energy)) -138.786 ))));
	else if (energy <= 29) return((pow(10.0,( 14.214 * (log10(energy)) * (log10(energy)) * (log10(energy)) -60.8109 * (log10(energy)) * (log10(energy))  + 86.3568 * (log10(energy)) -61.2445 ))));
	else if (energy <= 10000) return((pow(10.0,( -1.0201 * (log10(energy))  -19.0629 ))));
	else return 0;
}

  // electronic excitation 4: e + Cl2 -> e + Cl2 (1Pg)
  // A fit to the calculated data from Rescigno (1994), PRA 50(2), 1382-1389
  // July 29, 2012
static Scalar sigmaExc1Pg(Scalar energy)
{
	if (energy <= 6.86) return 0;
	else if (energy <= 7.5) return((pow(10.0,( -562.775  * (log10(energy)) * (log10(energy))  +1027.07  * (log10(energy)) -490.339 ))));
	else if (energy <= 12.03)  return((pow(10.0,( -3660.42 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 15084.5 * (log10(energy)) * (log10(energy)) * (log10(energy)) -23242.8 * (log10(energy)) * (log10(energy))  + 15873.9 * (log10(energy)) -4076.76 ))));
	else if (energy <= 20.05)  return((pow(10.0,( -119.932 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 630.253 * (log10(energy)) * (log10(energy)) * (log10(energy)) -1230.65 * (log10(energy)) * (log10(energy))  + 1059.46 * (log10(energy)) -360.606 ))));
	else if (energy <= 30) return((pow(10.0,( -32.9802  * (log10(energy)) * (log10(energy))  + 91.3056 * (log10(energy)) -83.943 ))));
	else if (energy <= 10000) return((pow(10.0,( -3.53893 * (log10(energy))  -15.8065 ))));
	else return 0;
}

  // electronic excitation 5: e + Cl2 -> e + Cl2 (3Su)
  // A fit to the calculated data from Rescigno (1994), PRA 50(2), 1382-1389
  // July 29, 2012
static Scalar sigmaExc3Su(Scalar energy)
{
	if (energy <= 6.8) return 0;
	else if (energy <= 7.45) return((pow(10.0,(-1130.65   * (log10(energy)) * (log10(energy))  + 2023.28 * (log10(energy)) -925.749 ))));
	else if (energy <= 12.03) return((pow(10.0,( -1660 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) +6973.84  * (log10(energy)) * (log10(energy)) * (log10(energy)) -10945.5 * (log10(energy)) * (log10(energy))  + 7605.83 * (log10(energy)) -1994.96 ))));
	else if (energy <= 25) return((pow(10.0,( -53.0518 * (log10(energy)) * (log10(energy)) * (log10(energy)) + 189.028* (log10(energy)) * (log10(energy))  -222.062  * (log10(energy)) +65.3135 ))));
	else if (energy <= 10000) return((pow(10.0,( -1.8981 * (log10(energy))  -17.9912 ))));
	else return 0;
}

  // electronic excitation 6: e + Cl2 -> e + Cl2 (R1Pu)
  // A fit to the calculated data from Rescigno (1994), PRA 50(2), 1382-1389
  // July 29, 2012
static Scalar sigmaExcR1Pu(Scalar energy)
{
	if (energy <= 9.22) return 0;
	else if (energy <= 11.14) return((pow(10.0,( 40.4735 * (log10(energy))  -64.0461 ))));
	else if (energy <= 14.68) return((pow(10.0,( 749.401 * (log10(energy)) * (log10(energy)) * (log10(energy)) -2509.95 * (log10(energy)) * (log10(energy))  + 2806.1 * (log10(energy)) -1068.34 ))));
	else if (energy <= 20.7) return((pow(10.0,( 2124.915 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + -10446 * (log10(energy)) * (log10(energy)) * (log10(energy)) +19217.03 * (log10(energy)) * (log10(energy))  -15676.64  * (log10(energy)) +4763.24 ))));
	else if (energy <= 29.97) return((pow(10.0,(  -4.2259* (log10(energy)) * (log10(energy)) * (log10(energy)) + 15.2406* (log10(energy)) * (log10(energy))  -16.7437  * (log10(energy)) -15.2289 ))));
	else if (energy <= 10000) return((pow(10.0,(  -20.3276 ))));
	else return 0;
}

  // electronic excitation 7: e + Cl2 -> e + Cl2 (R1Su)
  // A fit to the calculated data from Rescigno (1994), PRA 50(2), 1382-1389
  // July 29, 2012
static Scalar sigmaExcR1Su(Scalar energy)
{
	if (energy <= 9.32) return 0;
	else if (energy <= 10.83) return((pow(10.0,(  -165.567 * (log10(energy)) * (log10(energy))  + 368.278 * (log10(energy)) -226.420 ))));
	else if (energy <= 18.83) return((pow(10.0,( -1461.014 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 6629.665 * (log10(energy)) * (log10(energy)) * (log10(energy)) -11271.92 * (log10(energy)) * (log10(energy))  + 8518.223 * (log10(energy)) -2438.094 ))));
	else if (energy <= 29.947) return((pow(10.0,( -79.8822 * (log10(energy)) * (log10(energy)) * (log10(energy)) + 334.036* (log10(energy)) * (log10(energy))  -464.295  * (log10(energy)) + 193.415))));
	else if (energy <= 10000) return((pow(10.0,(  -21.023 ))));
	else return 0;
}



//// Cl2 + Cl2 reactions

  // elastic scattering: Cl2 + Cl2 -> Cl2 + Cl2
  // We assume Cl2 + Cl2 behaves like O2 + O2 and use data from Brunetti et al. (1981), JCP 74(12), 6734-6741. (Using the data directly, without assuming that they are proportional to the mass)
  // July 26, 2012
static Scalar sigmaElCl2Cl2(Scalar energy)
//  O2 + O2 --> O2 + O2
//  Scattering of O2 atoms off O2
//  A fit to the cross section measured by Brunetti et al, JPC 74 (1981) 6734
//
  {
    	if (energy <=0.0672) return 3.0978E-18;
	else if (energy <= 0.13) return((pow(10.0,(  -2.0611* (log10(energy)) * (log10(energy)) * (log10(energy)) -6.859 * (log10(energy)) * (log10(energy))  -7.7694  * (log10(energy)) -20.511 ))));
	else if (energy <=0.175 ) return((pow(10.0,( -0.3295 * (log10(energy)) * (log10(energy)) * (log10(energy)) -0.2582 * (log10(energy)) * (log10(energy))  -0.0745  * (log10(energy)) -17.671 ))));
	else if (energy <=0.3320 ) return((pow(10.0,( -23.184 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) -59.162  * (log10(energy)) * (log10(energy)) * (log10(energy)) -55.274 * (log10(energy)) * (log10(energy))  -22.409  * (log10(energy)) -20.96 ))));
   	else return 2.3645E-18;
  }


//// Cl2+ + Cl2 reactions

  // charge exchange: Cl2+ + Cl2 -> Cl2 + Cl2+
  // Assume sigma=K0/sqrt(2*e*E/m) with K = K0 = const, m is the reduced mass
  // K0=0.8E-15 m^3/s from Subramonium (2003), Simulation of transients and transport in plasma processing reactors, PhD thesis, UIUC. (This K0 is estimated by Subramonium)
  // July 26, 2012
static Scalar sigmaCEposCl2Cl2(Scalar energy)
  {
energy=0.5*energy; //transform TRF energy to CMF energy
    	if (energy > 1E-6) return(3.4307e-019/sqrt(energy));
    	else return(3.4307e-016);
  }

  // elastic scattering: Cl2+ + Cl2 -> Cl2+ + Cl2
  // Assuming that the cross seciton for this reaction is half of the cross section for charge exchange: Cl2+ + Cl2 -> Cl2 + Cl2+
  // Sep 5, 2012
static Scalar sigmaElposCl2Cl2(Scalar energy)
  {
energy=0.5*energy; //transform TRF energy to CMF energy
    	if (energy > 1E-6) return(0.5*3.4307e-019/sqrt(energy));
    	else return(0.5*3.4307e-016);
  }


// fragmentation: Cl2+ + Cl2 -> Cl+ + Cl + Cl2
// Using the cross section of O2+ + O2 --> O+ + O + O2, in which changing 13.8 (6.9*2) to 3.52*2=7.04
//static Scalar sigmaFRAG_Cl2(Scalar energy)
//  Cross section (4.9e-19) is the gas kinetic cross section for oxygen obtained from P312 in Lieberman (2005) Principles of Plasma Discharge and Materials Processing, 2nd edition.

//{
//  float temp5;
//  temp5 = 0;
//    {
//     if (energy <= 7.04) temp5 = 0;
//    else  if(7.04 < energy)  	 temp5 = 4.9e-19*(1-7.04/energy);
//    }
//    return (temp5);
//  }

// Fragmnentation by fas Cl2+ is assumed to be the same as for oxygen
//cross section for O2+ + O2 -> O+ + O + O2 in fig 10 from moran68_3411.
// T. F. Moran and J. R. Roberts, JCP vol 49 p 3411 (1968)
static Scalar sigmaFRAG_Cl2(Scalar energy)
{
        energy=0.5*energy;//transform TRF energy to CMF system, similar to sigmaneD in oxygen_gas.hpp
	//if (energy <= 6.3) return 0;
	if (energy <= 3.52) return 0; //extend threshold from 6.3 to 3.52 in order to fit the fragmentation in chlorine
	else if (energy <= 6.44) return((pow(10.0,(  -6245.6256 * (log10(energy)) * (log10(energy))  +10148.965  * (log10(energy)) 	-4144.1414 ))));
else if (energy <= 6.96) return((pow(10.0,( -104.3908 * (log10(energy)) * (log10(energy)) * (log10(energy)) -4.109608 * (log10(energy)) * (log10(energy))  +234.15368 * (log10(energy)) 	-152.7442 ))));
	else if (energy <=10 ) return((pow(10.0,( -869.78668 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 3306.8163 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-4700.1768 * (log10(energy)) * (log10(energy))  +2960.24095  * (log10(energy)) -717.89269 ))));
	else if (energy <= 15) return((pow(10.0,( -9.996669 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 50.898256 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-97.55846 * (log10(energy)) * (log10(energy))  +83.23836  * (log10(energy)) -47.383609 ))));
else if (energy <= 10000) return((pow(10.0,( -20.75849   ))));
	else return 0;
}



//// Cl+ + Cl2 reactions

  // charge exchange: Cl+ + Cl2 -> Cl + Cl2+
  // Assume sigma=K0/sqrt(2*e*E/m) with K = K0 = const, m is the reduced mass
  // K0=5.4E-16 m^3/s from the measurement in Spanel et al., (1993), JCP 98(11), 8660--8666.
  // Aug 26, 2012
static Scalar sigmaCEposClCl2(Scalar energy)
  {
energy=0.667*energy; //transform TRF energy to CMF energy
    	if (energy > 1E-6) return(1.8911e-019/sqrt(energy));
    	else return(1.8911e-016);
  }


//// Cl- + Cl2 reactions

//  Cl- + Cl2 -> Cl + Cl2 + e
//  A fit to the measured cross section in Huq et al., (1984), JCP 80(8), 3651-3655.
//  We extend the threshold from 8eV in the figure to 5.4eV in order to match the electron affinity of Cl.3.6 eV is the electron affinity in CM system, while we convert it to target rest frame using 3.6*(3/2)=5.4.
//  Aug, 28, 2012
static Scalar sigmaDetbyCl2(Scalar energy)

  {
     if (energy <= 5.415) return 0;
	else if (energy <= 10.7) return((pow(10.0,( 13.93217  * (log10(energy)) * (log10(energy))  	+5.758669  * (log10(energy)) -41.6908 ))));
	else if (energy <= 12) return((pow(10.0,(  -81.71451 * (log10(energy)) * (log10(energy))  + 186.3366 * (log10(energy)) 	-126.22466 ))));
	else if (energy <= 47) return((pow(10.0,( 2.404250 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-13.85134 * (log10(energy)) * (log10(energy))  +26.33003 * (log10(energy)) 	-35.61685 ))));
	else if (energy <= 114) return((pow(10.0,( 1.722514 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-10.00165 * (log10(energy)) * (log10(energy))  +19.60016  * (log10(energy)) 	-31.93785 ))));
	else if (energy <= 138) return((pow(10.0,( 96.17376 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-618.1271 * (log10(energy)) * (log10(energy))  + 1323.459 * (log10(energy)) 	-962.9179 ))));
	else if (energy <= 165) return((pow(10.0,( 17.493868  * (log10(energy)) * (log10(energy))  	-76.277778  * (log10(energy)) + 64.16468))));
	else if (energy <= 10000) return((pow(10.0,( -18.9586073 ))));
	else return 0;
    }


//////// cross sections of Cl included collisions (without Cl2)

//// e + Cl reactions

//  // elastic scattering: e + Cl -> e + Cl
//  // The cross section is compiled from two cross sections. For energies 0-0.4 eV, we use calculated data from Fabrikant (1994), JPB 27(19), 4545-4557. For energies 0.4-16 eV, we use calculated data from Griffin et al. (1995), PRA 51(3), 2265-2276.
//  // July 29, 2012
//static Scalar sigmaEleCl(Scalar energy)
//{
//	if (energy <= 0.006193) return((pow(10.0,(  -0.09285* (log10(energy))  -19.0311 ))));
//	else if (energy <= 0.206) return((pow(10.0,( 0.0524612 * (log10(energy)) * (log10(energy)) * (log10(energy)) +0.0437042 * (log10(energy)) * (log10(energy))  -0.624814  * (log10(energy)) -19.8589 ))));
//	else if (energy <= 1.09) return((pow(10.0,(2.18031  * (log10(energy)) * (log10(energy)) * (log10(energy)) +	2.92862 * (log10(energy)) * (log10(energy))  + 0.684314 * (log10(energy)) -19.6322 ))));
//	else if (energy <= 7.5) return((pow(10.0,( -0.3875 * (log10(energy)) * (log10(energy)) * (log10(energy)) +0.336671 * (log10(energy)) * (log10(energy))  + 1.12197 * (log10(energy)) 	-19.6380 ))));
//	else if (energy <= 16) return((pow(10.0,(2.99733  * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-11.5301 * (log10(energy)) * (log10(energy))  +14.2311  * (log10(energy)) 	-24.2939 ))));
//	else if (energy <= 10000) return((pow(10.0,( -0.5 * (log10(energy))  -18.0442 ))));
//	else return 0;
//}

// elastic scattering: e + Cl -> e + Cl
// This cross section is obtained from the calculation from Wang et al. Phys. Rev. A 87, 022703 (2013).
// We use the data from the BSR-pol calculation in figure 1.
// July 18 2013
static Scalar sigmaEleCl(Scalar energy)
{

         if (energy <= 0.001) return((pow(10.0,( -19.149234   ))));
         else if (energy <= 0.0954) return((pow(10.0,( 0.01749966 * (log10(energy)) * (log10(energy)) * (log10(energy)) +0.000741148754 * (log10(energy)) * (log10(energy))         -0.46907633  * (log10(energy)) -20.0930296))));
         else if (energy <=1 ) return((pow(10.0,( -0.21437878 * (log10(energy)) * (log10(energy)) * (log10(energy)) + 0.296567475* (log10(energy)) * (log10(energy))  + 0.8242529 * (log10(energy)) -19.32238 ))));
         else if (energy <= 10.9) return((pow(10.0,( -0.11507168 * (log10(energy)) * (log10(energy)) * (log10(energy)) +0.04281876 * (log10(energy)) * (log10(energy))  + 0.74899417 * (log10(energy)) -19.32512 ))));
         else if (energy <= 50) return((pow(10.0,( 1.084556 * (log10(energy)) * (log10(energy)) * (log10(energy)) -5.218808 * (log10(energy)) * (log10(energy))  + 7.4187667 * (log10(energy)) -21.925985 ))));
	 else if (energy <= 10000) return((pow(10.0,( -0.77426306 * (log10(energy))  -17.751682 ))));
         else return 0;
}







  // ionization: e + Cl -> Cl+ + e + e
  // From threshold (12.99 eV) -200 eV, we use measured cross section from Hayes et al., (1987), PRA 35(2), 578-584. Above 200 eV, we use calculated cross section from Ali and Kim (2005), Surface and Interface Analysis 37(11), 969-972.
  // Aug 26, 2012
static Scalar sigmaClIz(Scalar energy)
{
	if (energy <= 12.99) return 0;
	else if (energy <= 13) return((pow(10.0,( 6885.163 * (log10(energy))  -7691.3806 ))));
	else if (energy <= 14.4) return((pow(10.0,( -359.209  * (log10(energy)) * (log10(energy))  +845.36886  * (log10(energy)) 	-517.6604))));
	else if (energy <= 25) return((pow(10.0,( 40.76897 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-168.8604 * (log10(energy)) * (log10(energy))  + 234.6093 * (log10(energy)) 	-128.9648 ))));
	else if (energy <= 93) return((pow(10.0,( 1.838002 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-10.230305 * (log10(energy)) * (log10(energy))  + 18.89151 * (log10(energy)) 	-31.03664))));
	else if (energy <= 200) return((pow(10.0,(  0.07735036* (log10(energy)) * (log10(energy)) * (log10(energy)) -0.882323 * (log10(energy)) * (log10(energy))  + 2.389049 * (log10(energy)) 	-21.34964 ))));
	else if (energy <= 938) return((pow(10.0,( 0.2743981 * (log10(energy)) * (log10(energy)) * (log10(energy)) -2.347658 * (log10(energy)) * (log10(energy))  + 5.936351 * (log10(energy)) 	-24.15439 ))));
	else if (energy <= 4962) return((pow(10.0,(  -0.06282042 * (log10(energy)) * (log10(energy))  -0.41303104 * (log10(energy)) -18.26098 ))));
	else if (energy <= 10000) return((pow(10.0,( -1 * (log10(energy))  -16.94607 ))));
	else return 0;
}

  // excitation 1: e + Cl -> e + Cl(4s)
  // A fit to the calculated data from Ganas (1988), 63(2), 277-279
  // July 29, 2012
static Scalar sigmaExc4s(Scalar energy)
{
if (energy <= 9.1) return 0;
	else if (energy <=9.65 ) return((pow(10.0,(58.2619  * (log10(energy)) 	-80.87555 ))));
	else if (energy <= 9.835) return((pow(10.0,(  1.359186 * (log10(energy)) * (log10(energy))  + 	301.471 * (log10(energy)) 	-321.656 ))));
	else if (energy <= 11.5) return((pow(10.0,( -43.4114  * (log10(energy)) * (log10(energy))  +95.2586  * (log10(energy)) 	-72.7896 ))));
	else if (energy <= 566) return((pow(10.0,( -0.404856 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 3.464885 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-11.1386 * (log10(energy)) * (log10(energy))  + 15.3399 * (log10(energy)) -27.959 ))));
	else if (energy <= 10000) return((pow(10.0,( -0.788125 * (log10(energy))  -18.9240 ))));
	else return 0;
}

  // excitation 2: e + Cl -> e + Cl(4p)
  // A fit to the calculated data from Ganas (1988), 63(2), 277-279
  // July 29, 2012
static Scalar sigmaExc4p(Scalar energy)
{
if (energy <= 10.5) return 0;
	else if (energy <=10.509 ) return((pow(10.0,( 6506.867 * (log10(energy))  	-6669.743 ))));
	else if (energy <= 10.864) return((pow(10.0,( 7994.184 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-24644.035 * (log10(energy)) * (log10(energy))  +25475.814  * (log10(energy)) 	-8851.986 ))));
	else if (energy <= 11.522) return((pow(10.0,( 9761.874 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-31305.82 * (log10(energy)) * (log10(energy))  +33466.48  * (log10(energy)) 	-11945.8 ))));
	else if (energy <= 15.462) return((pow(10.0,( 69.2673 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-250.761 * (log10(energy)) * (log10(energy))  + 301.950 * (log10(energy)) 	-140.821 ))));
	else if (energy <= 368) return((pow(10.0,( 0.10068 * (log10(energy)) * (log10(energy)) * (log10(energy)) -0.647509 * (log10(energy)) * (log10(energy))  + 0.407545 * (log10(energy)) -19.6046 ))));
	else if (energy <= 10000) return((pow(10.0,( -1.028694 * (log10(energy))  -18.4868 ))));
	else return 0;
}

  // excitation 3: e + Cl -> e + Cl(3d)
  // A fit to the calculated data from Ganas (1988), 63(2), 277-279
  // July 29, 2012
static Scalar sigmaExc3d(Scalar energy)
{
if (energy <= 11.2) return 0;
	else if (energy <=11.5 ) return((pow(10.0,( 374.660 * (log10(energy))  	-418.100 ))));
	else if (energy <= 12.437) return((pow(10.0,( -67.8200  * (log10(energy)) * (log10(energy))  + 153.653 * (log10(energy)) 	-107.376 ))));
	else if (energy <= 27.655) return((pow(10.0,( 6.85215 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-29.1832 * (log10(energy)) * (log10(energy))  + 41.5617 * (log10(energy)) -39.9591 ))));
	else if (energy <= 511.47) return((pow(10.0,( 0.197979 * (log10(energy)) * (log10(energy)) * (log10(energy)) -1.47284 * (log10(energy)) * (log10(energy))  + 2.95192 * (log10(energy)) 	-21.9491 ))));
	else if (energy <= 10000) return((pow(10.0,( -0.802241 * (log10(energy))  -18.6397 ))));
	else return 0;
}

  // excitation 4: e + Cl -> e + Cl(5s)
  // A fit to the calculated data from Ganas (1988), 63(2), 277-279
  // July 29, 2012
static Scalar sigmaExc5s(Scalar energy)
{
if (energy <= 11.4) return 0;
	else if (energy <=12 ) return((pow(10.0,( 163.9945 * (log10(energy))  	-198.327 ))));
	else if (energy <= 12.699) return((pow(10.0,( -58.6388  * (log10(energy)) * (log10(energy))  + 134.0526 * (log10(energy)) 		-97.7212 ))));
	else if (energy <= 30.541) return((pow(10.0,( 5.66974 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-24.4684 * (log10(energy)) * (log10(energy))  + 34.9146 * (log10(energy)) -37.5517 ))));
	else if (energy <= 627.48) return((pow(10.0,( 0.102382 * (log10(energy)) * (log10(energy)) * (log10(energy)) -0.812090 * (log10(energy)) * (log10(energy))  + 1.399055 * (log10(energy)) 	-21.7141 ))));
	else if (energy <= 10000) return((pow(10.0,( -0.812999 * (log10(energy))  -19.6358 ))));
	else return 0;
}

  // excitation 5: e + Cl -> e + Cl(5p)
  // A fit to the calculated data from Ganas (1988), 63(2), 277-279
  // July 29, 2012
static Scalar sigmaExc5p(Scalar energy)
{
if (energy <= 11.8) return 0;
	else if (energy <=11.8218 ) return((pow(10.0,( 4488.332 * (log10(energy))  	-4835.962 ))));
	else if (energy <= 12.55) return((pow(10.0,( 18247.95 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-60561.35 * (log10(energy)) * (log10(energy))  +66998.99  * (log10(energy)) 	-24728.37))));
	else if (energy <= 17.5) return((pow(10.0,( 151.764 * (log10(energy)) * (log10(energy)) * (log10(energy)) -551.855 * (log10(energy)) * (log10(energy))  + 668.785 * (log10(energy)) -290.587 ))));
	else if (energy <= 145) return((pow(10.0,( 0.209268 * (log10(energy)) * (log10(energy)) * (log10(energy)) -1.21459 * (log10(energy)) * (log10(energy))  + 1.38804 * (log10(energy))	-20.7161 ))));
	else if (energy <= 10000) return((pow(10.0,( -0.990074 * (log10(energy)) -19.1329 ))));
	else return 0;
}

  // excitation 6: e + Cl -> e + Cl(4d)
  // A fit to the calculated data from Ganas (1988), 63(2), 277-279
  // July 29, 2012
static Scalar sigmaExc4d(Scalar energy)
{
	if (energy <=12 ) return 0;
	else if (energy <= 12.305) return((pow(10.0,(  363.963* (log10(energy))  	-417.782 ))));
	else if (energy <= 16) return((pow(10.0,( 113.452 * (log10(energy)) * (log10(energy)) * (log10(energy))	-425.139 * (log10(energy)) * (log10(energy))  +530.995  * (log10(energy)) 	-241.622 ))));
	else if (energy <= 58.016) return((pow(10.0,(1.85222  * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-9.31229 * (log10(energy)) * (log10(energy))  +15.2981  * (log10(energy)) 	-28.7380 ))));
	else if (energy <= 739.43) return((pow(10.0,( 0.145560 * (log10(energy)) * (log10(energy)) * (log10(energy)) -1.12721 * (log10(energy)) * (log10(energy))  +2.19667  * (log10(energy)) 	-21.7282 ))));
	else if (energy <= 10000) return((pow(10.0,( -0.801227 * (log10(energy))  + -18.9732))));
	else return 0;
}

  // excitation 7: e + Cl -> e + Cl(6s)
  // A fit to the calculated data from Ganas (1988), 63(2), 277-279
  // July 29, 2012
static Scalar sigmaExc6s(Scalar energy)
{
	if (energy <=12.1) return 0;
	else if (energy <= 12.6) return((pow(10.0,( 170.598 * (log10(energy))  	-209.721 ))));
	else if (energy <= 12.8) return((pow(10.0,( -620.053  * (log10(energy)) * (log10(energy))  + 1412.83 * (log10(energy)) 	-825.868 ))));
	else if (energy <= 20) return((pow(10.0,( 24.6788 * (log10(energy)) * (log10(energy)) * (log10(energy))	-98.6028 * (log10(energy)) * (log10(energy))  +131.084 * (log10(energy)) 	-79.4524 ))));
	else if (energy <= 200) return((pow(10.0,( 0.471621 * (log10(energy)) * (log10(energy)) * (log10(energy)) -2.84129 * (log10(energy)) * (log10(energy))  +5.02577  * (log10(energy)) 	-24.2303 ))));
	else if (energy <= 10000) return((pow(10.0,( -0.794760 * (log10(energy))  -20.1373 ))));
	else return 0;
}

  // excitation 8: e + Cl -> e + Cl(5d)
  // A fit to the calculated data from Ganas (1988), 63(2), 277-279
  // July 29, 2012
static Scalar sigmaExc5d(Scalar energy)
{
	if (energy <= 12.4) return 0;
	else if (energy <=12.435 ) return((pow(10.0,( 2480.865 * (log10(energy))  	-2737.552 ))));
	else if (energy <= 12.56) return((pow(10.0,( 780.193  * (log10(energy)) * (log10(energy))  	-1558.96  * (log10(energy)) + 749.656))));
	else if (energy <= 27) return((pow(10.0,( 11.80126 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-52.7754 * (log10(energy)) * (log10(energy))  + 78.2310 * (log10(energy)) -59.2134 ))));
	else if (energy <= 531.11) return((pow(10.0,( 0.208545 * (log10(energy)) * (log10(energy)) * (log10(energy)) -1.54239 * (log10(energy)) * (log10(energy))  +3.09379  * (log10(energy)) 	-22.6363 ))));
	else if (energy <= 10000) return((pow(10.0,(-0.784255  * (log10(energy))  -19.2936 ))));
	else return 0;
}

//// Cl + Cl reactions

  // elastic scattering: Cl + Cl -> Cl + Cl
  // Assuming that the cross seciton for this reaction is half of the cross section for elastic scattering: O + O2 -> O + O2
  // Sep 5, 2012
static Scalar sigmaElClCl(Scalar energy)
{
	if (energy <= 0.0299) return(0.5*(pow(10.0,( -17.542045   ))));
	else if (energy <= 0.0915) return(0.5*(pow(10.0,( 0.1721372 * (log10(energy)) * (log10(energy)) * (log10(energy)) +0.6598555 * (log10(energy)) * (log10(energy))  + 0.5454695 * (log10(energy)) -17.63426 ))));
	else if (energy <= 0.1627) return(0.5*(pow(10.0,( 0.6695981 * (log10(energy)) * (log10(energy)) * (log10(energy)) +2.8330981 * (log10(energy)) * (log10(energy))  + 	3.416784 * (log10(energy)) 	-16.43970 ))));
	else if (energy <= 0.2689) return(0.5*(pow(10.0,( 4.494896 * (log10(energy)) * (log10(energy)) * (log10(energy)) +9.1671445 * (log10(energy)) * (log10(energy))  + 	6.200954 * (log10(energy)) 	-16.30707 ))));
	else if (energy <= 0.3660) return(0.5*(pow(10.0,( -0.4201156  * (log10(energy)) * (log10(energy))  	-0.5646581 * (log10(energy)) -17.88057 ))));
	else if (energy <= 10000) return(0.5*(pow(10.0,( -17.713914   ))));
	else return 0;
}


//// Cl+ + Cl reactions

// charge exchange: Cl+ + Cl -> Cl + Cl+
// Assume sigma=K0/sqrt(2*e*E/m), m is the reduced mass
// K0=1E-15 m^3/s from Subramonium (2003), Simulation of transients and transport in plasma processing reactors, PhD thesis, UIUC. (This K0 is estimated by Subramonium)
// Aug 26, 2012  Corrected (sqrt was missing) Dec 3 2019 by JTG
static Scalar sigmaCEposClCl(Scalar energy)
{
	energy=0.5*energy; //transform TRF energy to CMF energy
	if (energy > 1E-6) return(3.0328e-19/sqrt(energy));
    	else return(3.0328e-16);
}


//// Cl- + Cl reactions

//  detachment: Cl- + Cl -> Cl2 + e
//  Using the rate coefficient of reaction: O- + O --> O2 + e, 1.9×10^{−16} m^3 s^{−1} to estimate the cross section through sigma=K0/sqrt(2*e*E/m), m is the reduced mass.
// We change the threshold from 0 to 1.13*2=2.26
static Scalar sigmanegOD(Scalar energy)
// Cross section estimated from a measured rate coefficient by Belostotsky et al. PSST (2005) 532
//take a constant
// value of 1.0964e-19 m^2
{
energy=0.5*energy; // transform energy from TRF to CMF
float temp5;
temp5 = 0;
{
temp5 = 1.09e-19;
if (energy < 1.13) temp5 = 0;
}
return(temp5*(1-1.13/energy));

}


//////// cross sections of Cl2+ included collisions (without Cl2, Cl)

//// Cl- + Cl2+ reactions

  // mutual neutralization: Cl- + Cl2+ -> Cl + Cl + Cl
  // Assume sigma=K0/sqrt(2*e*E/m), m is the refuced mass
  // K0=5E-14*(300/(11605T))^0.5 m^3/s (T in eV) from Thorsteinsson and Gudmundsson (2010), PSST 19(1), 015001.
  // July 26, 2012
static Scalar sigmaMNposCl2negCl(Scalar energy)
{
energy=0.667*energy;//transform TRF energy to CMF energy
	if (energy > 1E-3) return(0.28148e-017/energy);
    	else return(0.28148e-014);
 }

//// e + Cl2+ reactions

  // dissociative recombination: e + Cl2+ -> Cl + Cl
  // Previously (before April 8, 2013) we use the rate coefficient from Thorsteinsson and Gudmundsson (2010), PSST 19(1), 015001.  to estimate the cross section for dissociative recombination, which is as follows.
  // Assume sigma=K/sqrt(2*e*E/m) with K = 9E-14*Te^(-0.5) (the unit of Te is eV, which is the same as E), m is the refuced mass (here m approximately equals to the electron mass)
  // K = 9E-14*Te^(-0.5) from Thorsteinsson and Gudmundsson (2010), PSST 19(1), 015001.
  // July 26, 2012
//static Scalar sigmaDissRec(Scalar energy)
//{
//	if (energy > 1E-3) return(1.5185e-19/energy);
//    	else return(1.5185e-16);
//}

// Since April 8, 2013, we use the cross section calculated by Zhang et al. (2011), Physical Review A 84, 052707. (The solid blue line in figure 5 in this paper.)
static Scalar sigmaDissRec(Scalar energy)
{
         if (energy <= 0.01) return((pow(10.0,( -0.8124758 * (log10(energy))  -19.76590 ))));
         else if (energy <=2.01e-1 ) return((pow(10.0,( 0.1720371 * (log10(energy)) * (log10(energy)) * (log10(energy)) + 1.032353* (log10(energy)) * (log10(energy))  + 1.1531425 * (log10(energy)) -18.584465 ))));
         else if (energy <=8e-1 ) return((pow(10.0,( -5.9830676 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) -17.72980  * (log10(energy)) * (log10(energy)) * (log10(energy)) -16.83162 * (log10(energy)) * (log10(energy))  -5.521514  * (log10(energy)) -19.205174 ))));
         else if (energy <=1.74 ) return((pow(10.0,( -69.679301 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) + 29.480575 * (log10(energy)) * (log10(energy)) * (log10(energy)) +2.276027 * (log10(energy)) * (log10(energy))      -2.655456  * (log10(energy)) -19.043044 ))));
         else if (energy <= 3.84) return((pow(10.0,( 91.747045 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) -172.14997  * (log10(energy)) * (log10(energy)) * (log10(energy)) +87.082565 * (log10(energy)) * (log10(energy))        -17.328075  * (log10(energy)) -18.151169 ))));
         else if (energy <= 10000) return((pow(10.0,( -13.098095 * (log10(energy))  -14.533637 ))));
         else return 0;

}

//////// cross sections of Cl+ included collisions (without Cl2, Cl, Cl2+)

//// Cl- + Cl+ reactions

  // mutual neutralization: Cl- + Cl+ -> Cl + Cl
  // Assume the cross section to be the same as that of Cl- + Cl2+ -> Cl + Cl + Cl according to Thorsteinsson and Gudmundsson (2010), PSST 19(1), 015001.
  // Aug 26, 2012
static Scalar sigmaMNposClnegCl(Scalar energy)
{
energy=0.5*energy;//transform TRF energy to CMF energy
	if (energy > 1E-3) return(0.28148e-017/energy);
    	else return(0.28148e-014);
 }


/////// cross sections of Cl- included collisions (without Cl2, Cl, Cl2+, Cl+)

//// e + Cl- reactions

  // electron detachment: e + Cl- -> Cl + 2e
  // A fit to the measured data from Fritioff et al. (2003), PRA 68(1), 012712.
  // July 26, 2012
static Scalar sigmaeSingleDet(Scalar energy)
  {
    	if (energy <=3.4) return 0;
	else if (energy <= 9.9) return((pow(10.0,( -120.23 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) +418.501  * (log10(energy)) * (log10(energy)) * (log10(energy)) -548.1 * (log10(energy)) * (log10(energy))  +324.7  * (log10(energy)) -94.984 ))));
	else if (energy <= 21.5) return((pow(10.0,( -2.1814  * (log10(energy)) * (log10(energy))  + 6.93861 * (log10(energy)) -24.881 ))));
	else if (energy <= 76) return((pow(10.0,( -0.9474  * (log10(energy)) * (log10(energy))  + 3.20135 * (log10(energy)) -22.087 ))));
	else if (energy <= 94.5) return((pow(10.0,(  -2.3834 * (log10(energy)) * (log10(energy))  + 8.7904 * (log10(energy)) -27.5276 ))));
	else if (energy <= 10000) return((pow(10.0,( -1 * (log10(energy))  -17.488 ))));
	else return 0;
  }

  // electron detachment: e + Cl- -> Cl+ + 3e
  // A fit to the measured data from Fritioff et al. (2003), PRA 68(1), 012712.
  // Aug 26, 2012
static Scalar sigmaeDoubleDet(Scalar energy)
{
	if (energy <= 28.6) return 0;
	else if (energy <= 29.09) return((pow(10.0,( 333.24881 * (log10(energy))  	-508.88323 ))));
	else if (energy <= 36.8) return((pow(10.0,( 99.8540 * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-475.2021 * (log10(energy)) * (log10(energy))  + 756.7628 * (log10(energy)) 	-423.797))));
	else if (energy <= 97) return((pow(10.0,( -9.4599387 * (log10(energy)) * (log10(energy)) * (log10(energy)) * (log10(energy)) +79.24975  * (log10(energy)) * (log10(energy)) * (log10(energy)) 	-247.3574 * (log10(energy)) * (log10(energy)) + 340.8534  * (log10(energy)) -195.2145 ))));
	else if (energy <= 10000) return((pow(10.0,(  -1* (log10(energy))  -18.30751 ))));
	else return 0;
}



  // charge exchange: Cl- + Cl -> Cl + Cl-
  // A fit to the calculation in Karmohapatro (1965), Journal of the physical society in Japan 20(5), 839-841.
  // Sep 5, 2012
static Scalar sigmaCEnegClCl(Scalar energy)
{
	if (energy <=25 ) return((pow(10.0,(  -17.39279  ))));
	else if (energy <= 900) return((pow(10.0,( -0.0468044 * (log10(energy)) * (log10(energy)) * (log10(energy)) +0.27462696 * (log10(energy)) * (log10(energy))  -0.6160801  * (log10(energy)) -16.940367 ))));
	else if (energy <= 10000) return((pow(10.0,( -0.1158507 * (log10(energy)) * (log10(energy)) * (log10(energy)) +1.147385 * (log10(energy)) * (log10(energy))  -3.917455  * (log10(energy)) -13.02362 ))));
	else return 0;
}

  // elastic scattering: Cl + Cl2 -> Cl + Cl2
  // We assume Cl + Cl2 behaves like O + O2 and use data from Brunetti et al. (1981), JCP 74(12), 6734-6741. (Using the data directly, without assuming that they are proportional to the mass)
   static Scalar sigmaElClCl2(Scalar energy)
//  O + O2 --> O + O2
//  Scattering of O atoms off O2
//  Added, a fit to the cross section measured by Brunetti et al, JPC 74 (1981) 6734
//  by JTG October 3 2012
 {
     	if (energy <= 0.0299) return((pow(10.0,( -17.542045   ))));
	else if (energy <= 0.0915) return((pow(10.0,( 0.1721372 * (log10(energy)) * (log10(energy)) * (log10(energy)) +0.6598555 * (log10(energy)) * (log10(energy))  + 0.5454695 * (log10(energy)) -17.63426 ))));
	else if (energy <= 0.1627) return((pow(10.0,( 0.6695981 * (log10(energy)) * (log10(energy)) * (log10(energy)) +2.8330981 * (log10(energy)) * (log10(energy))  + 	3.416784 * (log10(energy)) 	-16.43970 ))));
	else if (energy <= 0.2689) return((pow(10.0,( 4.494896 * (log10(energy)) * (log10(energy)) * (log10(energy)) +9.1671445 * (log10(energy)) * (log10(energy))  + 	6.200954 * (log10(energy)) 	-16.30707 ))));
	else if (energy <= 0.3660) return((pow(10.0,( -0.4201156  * (log10(energy)) * (log10(energy))  	-0.5646581 * (log10(energy)) -17.88057 ))));
	else if (energy <= 10000) return((pow(10.0,( -17.713914   ))));
	else return 0;
    }



  // elastic scattering: Cl+ + Cl2 -> Cl+ + Cl2
  // Assuming that the cross seciton for this reaction is half of the cross section for charge exchange: Cl+ + Cl2 -> Cl + Cl2+
  // Sep 5, 2012
static Scalar sigmaElposClCl2(Scalar energy)
  {
energy=0.667*energy; //transform TRF energy to CMF energy
    	if (energy > 1E-6) return(0.5*1.8911e-019/sqrt(energy));
    	else return(0.5*1.8911e-016);
  }


  // Elastic scattering: Cl- + Cl2 -> Cl- + Cl2
  // Assuming that the cross section is the same with O- + O2 -> O- + O2, whose cross section is revised by a new fit to the cross section measured by E. E. Muschlitz, 4th ICPIG 1959
  // Sep 5 2012
static Scalar sigmaneE(Scalar energy)
    {
      if (energy <= 300) return((-3.101e-22 * energy + 8.6525e-21 * sqrt(energy) + 3.2995e-20));
      return(9e-20);
    }



// Cl2+ + Cl -> Cl2 + Cl+ (non-resonant charge exchange with a threshold)
// Using cross section of the corresponding reaction in oxygen
// Change threshold to 1.52*3=4.56
//   O2+ + O --> O2 + O+
//   A fit given in the paper by Stebbings et al. JCP (1963) 2280
//   JTG July 26 2009 revised November 4 2009
static Scalar sigmaCEposCl2Cl(Scalar energy)
{

  float temp5;
  temp5 = 0;
{
if(energy < 4.56)      temp5 = 0; // 1.42V -> 4.26V; 1.42V is Ethr in CM frame, 4.26 is Ethr in target rest frame; MAL 10/6/12
 else if (4.56 <= energy && energy < 82.035)   temp5 = 4.1111e-20 * ( 1 - 4.26/energy);
      else if(82.035 <= energy)
 temp5 = (pow(10.0,0.1476 * (log10(energy)) - 19.6917));
     }
 return(temp5);

}




};

#endif
