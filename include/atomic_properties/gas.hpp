// gas base class
// by HyunChul Kim, 2006
#ifndef GAS_H
#define GAS_H

#include "utils/ovector.hpp"
#include "utils/ostring.hpp"
#include "main/oopiclist.hpp"
#include "reactions/reaction.hpp"
#include "xsections/xsection_func.hpp"
#include "xsections/xsectionsamples.hpp"

class Gas
{
protected:
  ReactionList reactionlist;
  oopicList<Xsection> xsectionlist;
  Xsection *xsection;
  ostring mcctype;
  Reaction *reaction;
  GasType gastype;
public:
  Gas()
  {
    mcctype = "";
  }
  ~Gas()
  {
    xsectionlist.deleteAll();
    reactionlist.deleteAll();
  }
  ReactionList & get_reactionlist()
  {
    return reactionlist;
  }
  Xsection *add_xsection(Scalar (*f)(Scalar), Scalar _threshold, ReactionType _type)
  {
    Xsection *_xsection;
    _xsection = new Xsection_Func(f, _threshold, _type);
    xsectionlist.add(_xsection);
    return _xsection;
  }

  Xsection *add_xsection(Scalar e0, Scalar e1, Scalar e2, Scalar sMax,
                      ReactionType _type)
  {
    Xsection *_xsection;
    _xsection = new Xsection_type1(e0, e1, e2, sMax, _type);
    xsectionlist.add(_xsection);
    return _xsection;
  }

  Xsection *add_xsection(Scalar _a, Scalar _b, ReactionType _type)
  {
    Xsection *_xsection;
    _xsection = new Xsection_type2(_a, _b,_type);
    xsectionlist.add(_xsection);
    return _xsection;
  }
};
#endif
