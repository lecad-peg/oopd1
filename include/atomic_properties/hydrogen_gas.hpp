// Hydrogen_gas class includes all properties regarding Hydrogen
// by HyunChul Kim, 2006
#ifndef HYDROGENGAS_H
#define HYDROGENGAS_H

#include "atomic_properties/gas.hpp"
#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "main/species.hpp"

class Hydrogen_Gas: public Gas
{
public:
  Hydrogen_Gas(oopicListIter<Species> species_iter, Species *sec_e, Species *sec_i, bool no_default_secondary_species_flag)
  {
    mcctype = "generic_mcc";
    gastype = HYDROGEN_GAS;

    vector<Species *> E_species = Species::get_reaction_species_named_with(species_iter, E);
    vector<Species *> H_species = Species::get_reaction_species_named_with(species_iter, H);
    vector<Species *> H_ion_species = Species::get_reaction_species_named_with(species_iter, H_ion);

    if (no_default_secondary_species_flag)
    {
      if (!sec_e && !E_species.empty()) sec_e = E_species[0];
      if (!sec_i && !H_ion_species.empty()) sec_i = H_ion_species[0];
    }

    for (int j=H_species.size()-1; j>=0; j--)
    {
      H_species[j]->set_atomicnumber(1);
      for (int i=E_species.size()-1; i>=0; i--)
      {
        xsection = add_xsection(sigmaIz, 13.6, E_IONIZATION);
        reactionlist.add(new Reaction(gastype, E_IONIZATION, E_species[i], H_species[j], xsection, mcctype(), true, sec_e, sec_i));
      }
    }
  }

  static Scalar sigmaIz(Scalar energy)
  {
    if (energy <= 13.6) return 0.;
    else if (energy <= 17.789) return 8.208063e-21*log(energy)-2.071614e-20;
    else if (energy <= 53.6999) return 3.751578e-21*log(energy)-7.887766e-21;
    else return (2.996231e-19*log(energy)-8.145988e-19)/energy;
  }

};

#endif
