#ifndef AIR_DECLARED
#define AIR_DECLARED

#include "MCC/mcc.hpp"
#include "atomic_properties/gas.hpp"
#include "collisionpkg.hpp"

/*phys. Rev. A vol.6 p.1507 (1972)*/

class Species;

class Air_gas : public MCC, CollisionPkg
{
public:
  Air_gas(SpeciesList * slist, Species* sp, bool _useSecDefaultWeight, ostring sec_e, ostring sec_i):MCC(slist, sp, _useSecDefaultWeight, sec_e, sec_i)
  {
    pkgname = "Air";
    Vector3* u_gas = &un;
    bool eFlag_gas = false;

    add_collider(E);
    add_xsection(sigmaIz, 13.6, E_IONIZATION);
  }
  virtual ~Air_gas(){}

  static  Scalar sigmaIz(Scalar energy)
  {
    Scalar gamma = 1 + energy/0.511E6;
    Scalar beta = sqrt(gamma*gamma-1)/gamma;
    Scalar x_1 = log(gamma*gamma-1)/beta*beta-1;
    Scalar x_2 = 1/beta/beta;
    return 1.874E-24*(3.83*x_1+35.6*x_2);   // return the cross section in m^2
 }


};

#endif
