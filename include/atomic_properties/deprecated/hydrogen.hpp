#ifndef HYDROGEN_DECLARED
#define HYDROGEN_DECLARED
#include "MCC/mcc.hpp"
// mindgame: this class is going to be depreciated.

class Xsection;

class Hydrogen : public MCC
{
public:
  Hydrogen(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, ostring sec_e, ostring sec_i):MCC(slist, sp, _useSecDefaultWeight, sec_e, sec_i)
  {
    pkgname = "Hydrogen";
    u_gas = &un;
    eFlag_gas = false;

    add_collider(E);
    add_xsection(sigmaIz, 13.6, E_IONIZATION);
  }

  virtual ~Hydrogen(){}
  virtual int get_gasatomicnumber(){ return 1; }

  static  Scalar sigmaIz(Scalar energy)
  {
    if (energy <= 13.6) return 0.;
    else if (energy <= 17.789) return 8.208063e-21*log(energy)-2.071614e-20;
    else if (energy <= 53.6999) return 3.751578e-21*log(energy)-7.887766e-21;
    else return (2.996231e-19*log(energy)-8.145988e-19)/energy;
  }
};

#endif
