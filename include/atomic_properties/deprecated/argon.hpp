#ifndef ARGON_H
#define ARGON_H

#include "MCC/mcc.hpp"
#include "collisionpkg.hpp"

// mindgame: this class is going to be depreciated.
class Argon : public MCC, CollisionPkg
{
public:
  Argon(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, ostring sec_e, ostring sec_i):MCC(slist, sp, _useSecDefaultWeight, sec_e, sec_i)
    {
      pkgname = "Argon";
      u_gas = &un;
      eFlag_gas = false;

      // First set of cross - sections: allocate number of cross section, then list related colliders (eventually several)
      add_collider(E);
      add_Xsection_type1(0.3, 15., 20., 1.2e-19, E_ELASTIC);
      add_Xsection_type1(11.55, 30., 100., 7.e-21, E_EXCITATION);
      add_Xsection_type1(15.76, 30., 100., 3.e-20, E_IONIZATION);

      // Second set of cross - sections
      add_collider(Ar_ions);
      add_Xsection_type2(1.8e-19, 4e-19, SAMEMASS_ELASTIC);
      add_Xsection_type2(2.e-19, 5.5e-19, Ch_EXCHANGE);
     }
  virtual ~Argon(){}
  virtual int get_gasatomicnumber(){ return 18; }
};
#endif
