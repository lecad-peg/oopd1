#ifndef HELIUM_H
#define HELIUM_H

// Helium gas:  ion cross sections from curve fits to data from
// Cramer and Simons, Journal of Chemical Physics, Vol 26, No. 5, May, 1957.
// See also E.W. McDaniel, 1993
// Electron x-sections from oopic/xpdp1

// mindgame: this class is going to be depreciated.

#include "MCC/mcc.hpp"

class Helium : public MCC
{
public:
  Helium(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, ostring sec_e, ostring sec_i):MCC(slist, sp, _useSecDefaultWeight, sec_e, sec_i)
  {
    pkgname = "Helium";
    u_gas = &un;
    eFlag_gas = false;

    add_collider(E);
    add_xsection(sigmaElastic, 0.0, E_ELASTIC);
    add_xsection(sigmaExc, 19.8, E_EXCITATION);
    add_xsection(sigmaIz, 24.5, E_IONIZATION);

    add_collider (He_ions);
    add_xsection (sigmaIelastic, 0.0, SAMEMASS_ELASTIC);
    add_xsection (sigmaCX,0.0, Ch_EXCHANGE);
  }

  virtual ~Helium(){}
  virtual int get_gasatomicnumber(){ return 2; }
  ////////////////////////
  // cross sections
  ////////////////////////

  // elastic scattering: e + He -> e + He
  static Scalar sigmaElastic(Scalar energy)
  {
    return (8.5e-19/(pow(energy+10.0, 1.1)));
  }

  // excitation: e + He -> e + He*
  static Scalar sigmaExc(Scalar energy)
  {
    const Scalar extengy0 = 19.8;
    if(energy < extengy0) return (0.0);
    else if(extengy0 <= energy && energy <27.0)
      return (2.08e-22*(energy -extengy0));
    return (3.4e-19/(energy +200));
  }

  // ionization: e + He -> e + e + He+
  static Scalar sigmaIz(Scalar energy)
  {
    const Scalar ionengy0 = 24.5;
    if(energy < ionengy0) return (0.0);
    return(1e-17*(energy -ionengy0)/((energy +50)*pow(energy+300.0, 1.2)));
  }

  // ion elastic scattering: He+ + He -> He+ + He
  static Scalar sigmaIelastic(Scalar energy)
  {
    Scalar crossx;
    const Scalar emin = 0.01;
    if(energy < emin) { energy = emin; }

    crossx = 3.6463e-19/sqrt(energy) - 7.9897e-21;

    if(crossx < 0) { return 0.0; }
    return crossx;
  }

  // charge exchange: He+ + He -> He + He+
  static Scalar sigmaCX(Scalar energy)
  {
    Scalar crossx;
    const Scalar emin = 0.01;
    const Scalar cutoff = 377.8;

    if(energy < emin) { energy = emin; }
    if(energy < cutoff)
      {
	crossx = 1.2996e-19  - 7.8872e-23*energy + 1.9873e-19/sqrt(energy);
      }
    else
      {
	crossx = (1.5554e-18*log(energy)/energy) + 8.6407e-20;
      }
    return crossx;
  }

};

#endif
