#ifndef EMITTER_H
#define EMITTER_H

#include "main/parse.hpp"
#include "utils/ovector.hpp"
#include "main/boundary.hpp"
#include "main/species.hpp"

// forward declarations
class Fields;
class VDistribution;

// emitter class:  emits particles from boundaries and
// recenters their velocities for the leap frog method

// Outstanding issues:
// int Emitter::get_nparticles(Scalar tstart, Scalar tend, int norm)
// * shouldn't q be evaluated on a species by species basis?
// in fact, should it even be stored in the emitter? (see init())
// * what does the cold bool mean? (see also init() and setparamgroup())
// * get_determination(): where does this go?  should be inlined

class Emitter : public Parse
{
protected:
	int method;
	Scalar T;
	Scalar np2c0;
	ostring speciesname;
	Scalar vt_x;
	Scalar vt_y;
	Scalar vt_z;
	Scalar v0_x;
	Scalar energy_x;
	Scalar energy_y;
	Scalar energy_z;
	Scalar v0_y;
	Scalar v0_z;
	Vector3 vt;
	Vector3 v0;
	// mindgame: in grid units
	Scalar j0;
	Scalar qm;
	int iter;
	Scalar q;
	Vector3 a[2],b[2];
	Fields *thefield;
	Grid *theg;
	VDistribution *distri;
	Boundary *theboundary;
	void (Emitter::*get_vandx)(Vector3 &, Scalar &,
			Scalar, Vector3, Scalar, Scalar, Direction);

	bool var_energy;
	Equation vx_equation;
	Scalar time;

	int nmax;

	SpeciesList *specieslist; // master list of species

	Species *spemit;
	// mindgame: vel direction of particles emitted, not that of boundary
	int direction;

	ParticleGroup *emit_pg;

	Emitter(Fields *t, Boundary *theb, int _method=1);

	// virtual functions from Parse
	void setdefaults_emit();
	void setparamgroup_emit();
	void check_emit();
	void init_emit();
	void maintain_emit();
	// mindgame: choose appropriate get_vandx() depending on method.
	void choose_method_function(int _method);

	void get_vandx0(Vector3 &vnhalf, Scalar &xn,
			Scalar f, Vector3 vnf, Scalar xnf,Scalar dt, Direction dir);

	void get_vandx5(Vector3 &vnhalf, Scalar &xn,
			Scalar f, Vector3 vnf, Scalar xnf,Scalar dt, Direction dir);

	void get_vandx1(Vector3 &vnhalf, Scalar &xn,
			Scalar f, Vector3 vnf, Scalar xnf,Scalar dt, Direction dir);

	void get_vandx2(Vector3 &vnhalf, Scalar &xn,
			Scalar f, Vector3 vnf, Scalar xnf, Scalar dt, Direction dir);

	void get_vandx3(Vector3 &vnhalf, Scalar &xn,
			Scalar f, Vector3 vnf, Scalar xnf, Scalar dt, Direction dir);

	void get_vandx4(Vector3 &vnhalf, Scalar &xn,
			Scalar f, Vector3 vnf, Scalar xnf, Scalar dt, Direction dir);

	virtual int get_nparticles(Scalar tstart, Scalar tend)=0;
	void get_times(int npart, Scalar *t, Scalar ts, Scalar te);
	void get_velocities(int npart, Vector3 *v, Scalar *f);

	void get_en(Scalar xnf1, Vector3 &_en,
			Vector3 &_en1, Direction dir);

	void get_bn(Scalar xnf1, Vector3 &_bn,
			Vector3 &_bn1,Scalar &_On, Scalar &_On1, Direction dir);

	void get_Gradeb(Scalar xnf1,Vector3 &_graden,
			Vector3 &_gradbn,Scalar &_gradOn, Direction dir);

	void Boris_push_integrator(Vector3 V_init,Vector3 &V_final,
			Vector3 e, Vector3 b,
			Scalar O, Scalar c1, Scalar c2,
			Scalar dt);
	void add_particles(int nn, Scalar weight, Scalar *xinit, Scalar *dX,
			Vector3 *vfinal, bool vnorm, Direction dir);

	void get_particlegroups();

public:
	~Emitter();

	inline void set_position()
	{
		if (direction == POSITIVE) j0 = Scalar(theboundary->get_j1());
		else if (direction == NEGATIVE) j0 = Scalar(theboundary->get_j0());
	}

	// function to emit particles from t=[tstart:tend]
	// using properties of emitter class
	void emit(Scalar tstart, Scalar tend);

	// function to emit a single particle
	void emit(Scalar & xp, Vector3 & vp, Scalar timefrac, Scalar ddtt,
			Direction direction, Scalar weight);

	// function to emit particles and re-center velocity with known
	// x and v values
	void emit_particles(int nn, Scalar xstart, Vector3 *vstart,
			Scalar *ftmp, Scalar ddtt,
			Direction direction, Scalar weight);

	void add_particles_to_list();

	// JK, 2019-01-13; return emitted species name
	ostring get_species_name() {return spemit->get_name();};
};
#endif
