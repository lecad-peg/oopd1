// Secondary Emitter Class
//
// Original by HyunChul Kim, 2006
//
#ifndef SECONDARYEMITTER_H
#define SECONDARYEMITTER_H

#include "emmiter/emitter.hpp"
#include "utils/ovector.hpp"

class Boundary;
class Species;

class SecondaryEmitter : public Emitter
{
  bool variable_weight;
  int get_nparticles(Scalar tstart, Scalar tend) { return 0; }
  void setdefaults();
  void init();

public:
  SecondaryEmitter(Fields *t, Boundary *theb, Species *sp,
		   int _method, int _nmax, Scalar _np2c0, int _vw);
  ~SecondaryEmitter();
};
#endif
