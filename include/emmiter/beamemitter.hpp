#ifndef BEAMEMITTER_H
#define BEAMEMITTER_H

#include "emmiter/emitter.hpp"
#include "main/oopiclist.hpp"
#include "utils/ovector.hpp"

class Species;
class BeamEmitter : public Emitter
{
  Equation I;
  Equation F;

  Species *species;

  int get_nparticles(Scalar tstart, Scalar tend);

  void setdefaults();
  void setparamgroup();
  void check();
  void init();
  void maintain();

public:
  BeamEmitter(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
			oopicList<Parameter<int> > int_variables, Fields *t,
			Boundary *theb);
  ~BeamEmitter();

};
#endif
