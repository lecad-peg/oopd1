#ifndef SPATIALREGION_H
#define SPATIALREGION_H

#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "main/parse.hpp"


// forward declarations
class Fields;
class Species;
class Boundary;
class Conductor;
class Drive; // add for dumpfile, MAL 5/22/10
class Dielectric;
class Grid;
class Load;
class PlasmaSource;
class ReactionGroup;
class ReactionParse;
class DiagnosticParser;
class DiagnosticControl;

class SpatialRegion : public Parse
{
 protected:
  ostring name; // name of this SpatialRegion
  Scalar t;  // global time
  Scalar dt; // time step
    int nsteps_total; // Total timesteps in simulation, HH 03/03/16
    Scalar f_low, w0, w0_temp; // Lowest frequency, HH 03/04/16
    int steps_rf; // Timesteps in each rf cycle, HH 03/04/16
    int total_rf_periods; // Total number of rf periods in simulation, HH 05/04/16
    int naverage, count_naverage; // Steps to average, HH 03/04/16
    int ng; // Number of grid points, HH 03/04/16
    int naverage_array; // Number of data points to average over in array, HH 03/04/16
    Scalar *V_DC; // DC bias voltage, HH 05/04/16
    bool dcbiasflag; // HH 05/20/16
    int nsteps; // HH 05/20/16
    bool spatiotemporal_flag; // Flag to include spatiotemporal diagnostics, HH 05/20/16

  SpeciesList specieslist;
  oopicList<Load> loadlist;
  oopicList<PlasmaSource> plasmaSourcelist;
  BoundaryList boundarylist;
  oopicList<Conductor> conductorlist;
  oopicList<Dielectric> dielectriclist;
	oopicList<Drive> drivelist; // add for dumpfile, MAL 5/22/10
  // mindgame: necessary to avoid memleaks
  oopicList<Fields> fieldlist;
  oopicList<ReactionGroup> reactiongrouplist;
  oopicList<ReactionParse> reactionparselist;

  Grid *thegrid;
  Fields *thefield;

  // variables for MPI
  int nproc, rank;

  // variables for diagnostics
  DiagnosticParser *thedp;
  DiagnosticControl *thediags;

 public:
  SpatialRegion();
  SpatialRegion(int nn, Scalar *newgrid, int geomtype,
		SpeciesList *slist, int pgtype); // grid constructor
  SpatialRegion(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables,
		SpeciesList *sl, int pgtype);

  ~SpatialRegion();

  void update();
	void reload_dumpfile_SR(); // called after reload_dumpfile in PlsmDev, MAL 5/16/10

  // construction functions
  void add_species(SpeciesList *slist);
  void set_grid(int nn, Scalar *newgrid, int geomtype);

  // access functions
  inline  void set_t(Scalar tt) { t = tt; }
  inline  void set_dt(Scalar tt) { dt = tt; }
  inline  Scalar get_t() { return t; }
  inline  Scalar get_dt() { return dt; }

  inline  Grid * get_grid() { return thegrid; }
  inline  Fields * get_field() { return thefield; }
  inline SpeciesList * get_specieslist() { return &specieslist; }
  inline BoundaryList * get_boundarylist() { return &boundarylist; }

  inline oopicList<Conductor> * get_conductorlist()
  {
    return &conductorlist;
  }
  inline oopicList<Dielectric> * get_dielectriclist() // add for dumpfile, MAL 5/11/10
  {
    return &dielectriclist;
  }
  inline oopicList<ReactionGroup> * get_reactiongrouplist() // add for dumpfile, MAL 8/12/10
  {
    return &reactiongrouplist;
  }

  inline void set_nproc(int i) { nproc = i; }
  inline void set_rank(int i)  { rank = i; }

    inline void set_nsteps_total(int _nsteps_total){nsteps_total =_nsteps_total;} // HH 03/03/16
    inline int get_nsteps_total(){return nsteps_total;} // HH 03/03/16
    inline int get_steps_rf(){return steps_rf;} // HH 03/04/16
    inline int get_naverage(){return naverage;} // HH 04/05/16
    inline Scalar *get_V_DC(){return V_DC;} // HH 05/04/16
    inline bool get_V_DC_flag() {return dcbiasflag;} // HH 05/20/16
    inline bool get_spatiotemporal_flag() {return spatiotemporal_flag;} // HH 05/20/16

  // access functions for diagnostics
  inline ostring get_name(void) { return name; }
  void    set_name(ostring nm);
  void set_diagnostic_parser(DiagnosticParser *tdptr);

  void set_ParticleGroupType(int type);
  void set_SpeciesFields(void);

  int  total_number_particles(void); // all particles in the SpatialRegion

  // virtual functions from parse
  virtual void setdefaults(void);
  virtual void setparamgroup(void);
  virtual void check(void);
  virtual void init(void);
  virtual Parse * special(ostring s);

  // SpatialRegion (local) versions of parse functions
  void setdefaults_SR(void);
  void setparamgroup_SR(void);
  void check_SR(void);
  void init_SR(void);
  Parse * special_SR(ostring s);

  void check_boundaries(void);
  bool check_Dirichlet(void);

#ifdef MPIDEFINED
  // MPI functions for parallel processing
  void send_classdata(void);
  void recv_classdata(void);
  void send_speciesdata(void);
  void recv_speciesdata(void);
#endif

  // Diagnostics functions, HH 05/20/16
  int calculate_steps_rf();
  void init_spatiotemporal_diagnostics();
  void spatiotemporal_diagnostics();
  bool check_V_DC_diagnostics();
  void V_DC_diagnostics();


};

#endif
