#ifndef SPATIALREGIONGROUP_H
#define SPATIALREGIONGROUP_H

#include "spatial_region/sptlrgn.hpp"
#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "main/parse.hpp"

class PlsmDev;
class Grid;

class SpatialRegionGroup : public SpatialRegion
{
 private:
  Grid *mastergrid;
  int inputfileline;  // line non-root processors should start reading at
  PlsmDev *theplasmadevice;

 public:
  SpatialRegionGroup(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		     oopicList<Parameter<int> > int_variables,
		     SpeciesList *sl, int pgtype, int r, int np,
		     PlsmDev *thepd);

  void createSpatialRegions(PlsmDev *thep);

  // virtual functions from Parse and SpatialRegion
  void setdefaults();
  void setparamgroup();
  void check();
  void init();
  Parse * special(ostring s);
};

#endif
