//********************************************************
// secondary.hpp
//
// Revision history
//
// CH Lim (July/04/2005) Original code.
//
//********************************************************


#ifndef SECONDARY_H
#define SECONDARY_H

// base class for secondary emission

#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "main/parse.hpp"

// forward declarations
class Species;
class Fields;
class Grid;
class ParticleGroup;
class VDistribution;
class SecondaryEmitter;
class Boundary;

class Secondary : public Parse, public Message
{
	enum METHOD{BASIC, BRUINING, VAUGHAN, YAMAMURA, ROTH, NEUTRALO, NEUTRALO2, OXO, OXO2, NEUTRALAR, DAR, VAUGHAN_VERTICAL_EMISSION, ERROR}; //ERROR}type; -> ERROR};, MAL 9/16/09
	SecondaryEmitter *semitter;
	Boundary *theboundary;
	Grid *thegrid;
	SpeciesList *specieslist;
	vector<ostring> impact_names;
	SpeciesList iSpeciesList; //impact particle which generate secondaries

	oopicListIter<Species> ispecies_iter;
	Species *secSpecies;//secondary emitted species
	Species *impact_Species;// impact species Edit HH 07/16/15
	ostring secspecies_name;
	VDistribution *dist;//distribution for true secondaries
	Scalar np2c0;
	bool constant_np2c;
	bool variable_weight;

	// Methods:
	ostring method;
	// General:
	Scalar threshold;
	Scalar secondary;
	Scalar Eemit;
	Scalar v_emit;
	Scalar energy_eV;
	// Vaughan
	Scalar f_ref;// fraction reflected primaries
	Scalar f_sca;// fraction scattered primaries
	Scalar energy_max;//energy at which sec. coeff = secondary for normal inc.
	Scalar ks;//surfact smoothes ( 0=rough, 2 = smooth)
	// Yamamura: used to make a fitting of the yield angular dependence
	Scalar pic_angle; // Angle of maximum emission
	Scalar angular_slope; // determines slope in Yamamura's angular dependence
	// Roth: Chemical sputtering
	Scalar wall_temp; // Wall temperature
	Scalar Edes, Edam; // Thresholds for desorption and damage
	Scalar flux; // incident flux (assumed approximatively constant)
	Scalar csp; // Parameter in Roth's formula
	// C. N.: Might be interesting to add the possibility to enter an equation for the yield formula in input file.

	Scalar dt;
	int meth; // this is the particle push method used in emitter.cpp!!, MAL 9/17/09
	int nmax;
	int nparticles; // MAL 9/16/09

	Vector3 normal_vector;//normal vector
	Vector3 tan;//tangent to the surface

public:

	Secondary (FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		 oopicList<Parameter<int> > int_variables,
		 Fields *t,Boundary *theb);
	~Secondary();

	Scalar number(ParticleGroup *p, int i, Species *ispecies = nullptr, Vector3 v_incident = 0.); // add args ispecies and v_incident, MAL200225
	void createSecondary(ParticleGroup *p, int i, Scalar frac, Direction j, Vector3 v_incident = 0.); // add v_incident as arg for fluid incident species, MAL200225
	int createSecondaries(ParticleGroup *p, int i, Scalar frac, Direction j);
	int evolvingfluid_createSecondaries(Species * ispecies, Scalar num_incident, VDistribution *v, Direction dir);
	void emit_secondary_product(Scalar xtmp, Vector3 v, Scalar frac, Direction dir);

	SecondaryEmitter *get_emitter(void) {return semitter;}

	METHOD get_method()
	{
		if (method == "Basic") return BASIC;
		else if (method == "Bruning") return BRUINING;
		else if (method == "Vaughan") return VAUGHAN;
		else if (method == "Yamamura") return YAMAMURA;
		else if (method == "Roth") return ROTH;
		else if (method == "NeutralO") return NEUTRALO;
		else if (method == "NeutralO2") return NEUTRALO2;
		else if (method == "NeutralAr") return NEUTRALAR;
		else if (method == "OxO") return OXO;
		else if (method == "OxO2") return OXO2;
		else if (method == "DAr") return DAR;
		else if (method == "Vaughan_Vertical") return VAUGHAN_VERTICAL_EMISSION;
		return ERROR;
	}

	//functions inherited from parse
	virtual void setdefaults(void);
	virtual void setparamgroup(void);
	virtual void check(void);
	virtual void init(void);

	//initialization for particular methods
	void init_roth(void);

	// Functions for V_DC calculations, HH 05/02/16
	inline Species *get_secSpecies() {return secSpecies;}
	inline double get_secSpecies_np2c() {return np2c0;}

};

#endif
