//********************************************************
// advance_v.hpp
//
// This code is included by the particlegroupxxxx classes to
// simulate the effect of polymorphism via inclusion, without
// forcing calls through virtual tables. It must be preceded by
// '#define ParticleGroupArray xxxx'.
//
// Revision history
//
// (JohnV 08Jul2003) Original code.
//
//********************************************************

// advance_v():
// advance the particle velocities; assumes all particles in a group have
// constant q/m ratio. currently does not do the weighting here?
#include "utils/ovector.hpp"
#include "main/particlegrouparray.hpp"
#include "main/particlegroup.hpp"


//Modified LOCALCLASS to ParticleGroupArray, VGT 11-10-20

#ifdef SINGLE
void LOCALCLASS::advance_v(int i, Scalar ddt, Vector3 E_sp, Vector3 B_sp){ // advance v
	dt = ddt; // add, so as to not subcycle load and boundary calls to advance_v, MAL 1/1/10
#else
void LOCALCLASS::advance_v(Scalar ddt){ // advance v
	int subcycle = get_species()->get_subcycle(); // add for subcycle, MAL 1/1/10
	if (theFields->get_nsteps() % subcycle != 0) return; // add for subcycle, MAL 1/1/10
  dt = ddt*subcycle; // add "*subcycle" for subcycle, MAL 1/1/10
  register int i;
#endif
  Scalar a_e[3]; // electric field acceleration array
  Scalar f0 = dt/dxdt*qm0; // force factor for v advance

#ifndef SINGLE
  int num = get_n();
#endif

  if (theFields->is_magnetized())
  {
    // magnetized pushes; assumes B independent of position
    Scalar t1, t2; // magnetic field rotation components
    f0 *= 0.5; // split Efield advance in 2 parts
    Scalar B[2], Bmag; // magnetic field [SHOULD THERE BE 3 COMPONENTS?, JH, 6/2005]
#ifdef SINGLE
    B[0] = B_sp.e1();
    B[1] = B_sp.e2();
    Bmag = B_sp.magnitude();
#else
    theFields->get_B(B, 2); // put B1,B2 into B.
    Bmag = theFields->magB();
#endif
    t2 = tan(f0*Bmag);
    t1 = t2*B[0]/Bmag;
    t2 *= B[1]/Bmag;
    Scalar itmag1 = 1.0/(1 + t1*t1 + t2*t2);

#ifdef SINGLE
    a_e[0] = f0*E_sp.e1();
    a_e[1] = f0*E_sp.e2();
    a_e[2] = f0*E_sp.e3();
    add_v(i,a_e); // 3 velocity half push in E
    rotate_v(i, t1, t2, itmag1); // rotate v for B
    add_v(i,a_e); // 3 velocity half push in E
#else
    if (theFields->is_transverseE())
    {
      for (i=0; i<num; i++) { // do a Boris push:
		  a_e[0] = f0*E1[i];
		  a_e[1] = f0*E2[i];
		  a_e[2] = f0*E3[i];
		  add_v(i,a_e); // 3 velocity half push in E
		  rotate_v(i, t1, t2, itmag1); // rotate v for B
		  add_v(i,a_e); // 3 velocity half push in E
      }
    }
    else { // Boris push for 1D E + B
      for (i=0; i<num; i++) { // do a Boris push:
		  a_e[0] = f0*E1[i]; // theFields.E1(get_x(i));
		  add_v(i,a_e[0]); // 1 velocity half push in E
		  rotate_v(i, t1, t2, itmag1); // rotate v for B
		  add_v(i,a_e[0]); // 1 velocity half push in E
      }
    }
#endif
  }
  else {
#ifdef SINGLE
    a_e[0] = f0*E_sp.e1();
    a_e[1] = f0*E_sp.e2();
    a_e[2] = f0*E_sp.e3();
    add_v(i,a_e); // 3 velocity half push in E
#else
    // unmagnetized pushes
    if (theFields->is_transverseE())
    {
      for (i=0; i<num; i++) { // E only push:
		  a_e[0] = f0*E1[i];
		  a_e[1] = f0*E2[i];
		  a_e[2] = f0*E3[i];
		  add_v(i,a_e); // 3 velocity half push in E
      }
    }
    else {
      // 1 velocity push in E
      for (i=0; i<num; i++)
	{
	  a_e[0] = f0*E1[i];
	  add_v(i, a_e[0]);  // 1 velocity half push in E
	}
    }
#endif
  }
}

// advance_v1D():
// advance the particle velocities; assumes all particles in a group have
// constant q/m ratio. Do 1D weighting of v and v^2 to the grid, for 1D flux and energy, MAL 6/19/10

#ifdef SINGLE
void LOCALCLASS::advance_v1D(int i, Scalar ddt, Vector3 E_sp, Vector3 B_sp){ // advance v
	dt = ddt; // add, so as to not subcycle load and boundary calls to advance_v, MAL 1/1/10
#else
void LOCALCLASS::advance_v1D(Scalar ddt){ // advance v
	int subcycle = get_species()->get_subcycle(); // add for subcycle, MAL 1/1/10
	if (theFields->get_nsteps() % subcycle != 0) return; // add for subcycle, MAL 1/1/10
  dt = ddt*subcycle; // add "*subcycle" for subcycle, MAL 1/1/10
  register int i;
	int ng = theGrid->getng();
	Scalar v1temp;
	Scalar *species_flux1 = theSpecies->get_species_flux1(); // for flux1 (n*v1) diagnostics, MAL 9/30/09
	Scalar *species_enden1 = theSpecies->get_species_enden1(); // for enden1 (n*v1*v1) diagnostics
	Scalar *species_scratch_flux1 = theSpecies->get_species_scratch_flux1(); // for flux1 diagnostics
	Scalar *species_scratch_enden1 = theSpecies->get_species_scratch_enden1(); // for enden1 diagnostics

#endif
  Scalar a_e[3]; // electric field acceleration array
  Scalar f0 = dt/dxdt*qm0; // force factor for v advance

#ifndef SINGLE
  int num = get_n();
#endif

  if (theFields->is_magnetized())
  {
    // magnetized pushes; assumes B independent of position
    Scalar t1, t2; // magnetic field rotation components
    f0 *= 0.5; // split Efield advance in 2 parts
    Scalar B[2], Bmag; // magnetic field [SHOULD THERE BE 3 COMPONENTS?, JH, 6/2005]
#ifdef SINGLE
    B[0] = B_sp.e1();
    B[1] = B_sp.e2();
    Bmag = B_sp.magnitude();
#else
    theFields->get_B(B, 2); // put B1,B2 into B.
    Bmag = theFields->magB();
#endif
    t2 = tan(f0*Bmag);
    t1 = t2*B[0]/Bmag;
    t2 *= B[1]/Bmag;
    Scalar itmag1 = 1.0/(1 + t1*t1 + t2*t2);

#ifdef SINGLE
    a_e[0] = f0*E_sp.e1();
    a_e[1] = f0*E_sp.e2();
    a_e[2] = f0*E_sp.e3();
    add_v(i,a_e); // 3 velocity half push in E
    rotate_v(i, t1, t2, itmag1); // rotate v for B
    add_v(i,a_e); // 3 velocity half push in E
#else
    if (theFields->is_transverseE() & is_variableWeight()) // variable weight particles
    {
      for (i=0; i<num; i++) { // do a Boris push:
		  a_e[0] = f0*E1[i];
		  a_e[1] = f0*E2[i];
		  a_e[2] = f0*E3[i];
			v1temp=v1[i]; // v1 at t-1/2
		  add_v(i,a_e); // 3 velocity half push in E
		  rotate_v(i, t1, t2, itmag1); // rotate v for B
		  add_v(i,a_e); // 3 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			Scalar v1iMKS = 0.5*get_vMKS(v1temp);
			Scalar v1isqMKS = v1iMKS*v1iMKS;
			theGrid->WeightLinear(x[i], w[i]*v1iMKS, species_flux1);
			theGrid->WeightLinear(x[i], w[i]*v1isqMKS, species_enden1);
			}
		}
		else if (theFields->is_transverseE() & !is_variableWeight()) // constant weight particles
		{
			memset(species_scratch_flux1, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden1, 0, ng*sizeof(Scalar));
      for (i=0; i<num; i++) { // do a Boris push:
		  a_e[0] = f0*E1[i];
		  a_e[1] = f0*E2[i];
		  a_e[2] = f0*E3[i];
			v1temp=v1[i]; // v1 at t-1/2
		  add_v(i,a_e); // 3 velocity half push in E
		  rotate_v(i, t1, t2, itmag1); // rotate v for B
		  add_v(i,a_e); // 3 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			// constant weight v1, and v1sq to the mesh with unit weight, v1 and v1sq in grid units, MAL 9/12/09
			theGrid->WeightLinear1D(x[i], v1temp, species_scratch_flux1, species_scratch_enden1);
			}
			Scalar w0 = get_w0();
			Scalar wdxdt = 0.5*w0*get_dxdt();
			Scalar wdxdtsq = 0.5*wdxdt*get_dxdt();
			for (int j=0; j<ng; j++) // add weight and convert to MKS velocities, MAL 9/12/09
			{
				species_flux1[j] += species_scratch_flux1[j]*wdxdt;
				species_enden1[j] += species_scratch_enden1[j]*wdxdtsq;
			}
    }
    else if (is_variableWeight()) // Boris push for 1D E + B, variable weight
		{
      for (i=0; i<num; i++) { // do a Boris push:
		  a_e[0] = f0*E1[i]; // theFields.E1(get_x(i));
			v1temp=v1[i]; // v1 at t-1/2
			add_v(i,a_e[0]); // 1 velocity half push in E
		  rotate_v(i, t1, t2, itmag1); // rotate v for B
		  add_v(i,a_e[0]); // 1 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			Scalar v1iMKS = 0.5*get_vMKS(v1temp);
			Scalar v1isqMKS = v1iMKS*v1iMKS;
			theGrid->WeightLinear(x[i], w[i]*v1iMKS, species_flux1);
			theGrid->WeightLinear(x[i], w[i]*v1isqMKS, species_enden1);
      }
    }
		else // Boris push for 1D E + B, constant weight particles
		{
			memset(species_scratch_flux1, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden1, 0, ng*sizeof(Scalar));
      for (i=0; i<num; i++) { // do a Boris push:
		  a_e[0] = f0*E1[i];
		  a_e[1] = f0*E2[i];
		  a_e[2] = f0*E3[i];
			v1temp=v1[i]; // v1 at t-1/2
		  add_v(i,a_e); // 3 velocity half push in E
		  rotate_v(i, t1, t2, itmag1); // rotate v for B
		  add_v(i,a_e); // 3 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			// constant weight particles, v1, and v1sq to the mesh with unit weight, v1 and v1sq in grid units, MAL 9/12/09
			theGrid->WeightLinear1D(x[i], v1temp, species_scratch_flux1, species_scratch_enden1);
			}
			Scalar w0 = get_w0();
			Scalar wdxdt = 0.5*w0*get_dxdt();
			Scalar wdxdtsq = 0.5*wdxdt*get_dxdt();
			for (int j=0; j<ng; j++) // add weight and convert to MKS velocities, MAL 9/12/09
			{
				species_flux1[j] += species_scratch_flux1[j]*wdxdt;
				species_enden1[j] += species_scratch_enden1[j]*wdxdtsq;
			}
    }
#endif
  }
  else {
#ifdef SINGLE
    a_e[0] = f0*E_sp.e1();
    a_e[1] = f0*E_sp.e2();
    a_e[2] = f0*E_sp.e3();
    add_v(i,a_e); // 3 velocity half push in E
#else
    // unmagnetized pushes
    if (theFields->is_transverseE() & is_variableWeight())
    {
      for (i=0; i<num; i++) { // E only push:
		  a_e[0] = f0*E1[i];
		  a_e[1] = f0*E2[i];
		  a_e[2] = f0*E3[i];
			v1temp=v1[i]; // v1 at t-1/2
		  add_v(i,a_e); // 3 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			Scalar v1iMKS = 0.5*get_vMKS(v1temp);
			Scalar v1isqMKS = v1iMKS*v1iMKS;
			theGrid->WeightLinear(x[i], w[i]*v1iMKS, species_flux1);
			theGrid->WeightLinear(x[i], w[i]*v1isqMKS, species_enden1);
      }
    }
    else if (theFields->is_transverseE() & !is_variableWeight())
		{
			memset(species_scratch_flux1, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden1, 0, ng*sizeof(Scalar));
      for (i=0; i<num; i++) { // E only push:
		  a_e[0] = f0*E1[i];
		  a_e[1] = f0*E2[i];
		  a_e[2] = f0*E3[i];
			v1temp=v1[i]; // v1 at t-1/2
		  add_v(i,a_e); // 3 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			// constant weight particles, v1, and v1sq to the mesh with unit weight, v1 and v1sq in grid units, MAL 9/12/09
			theGrid->WeightLinear1D(x[i], v1temp, species_scratch_flux1, species_scratch_enden1);
			}
			Scalar w0 = get_w0();
			Scalar wdxdt = 0.5*w0*get_dxdt();
			Scalar wdxdtsq = 0.5*wdxdt*get_dxdt();
			for (int j=0; j<ng; j++) // add weight and convert to MKS velocities, MAL 9/12/09
			{
				species_flux1[j] += species_scratch_flux1[j]*wdxdt;
				species_enden1[j] += species_scratch_enden1[j]*wdxdtsq;
			}
    }
		else if (is_variableWeight()) // 1 velocity push in E, variable weight particles
		{
      for (i=0; i<num; i++) {
			a_e[0] = f0*E1[i];
			v1temp=v1[i]; // v1 at t-1/2
			add_v(i, a_e[0]);  // 1 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			Scalar v1iMKS = 0.5*get_vMKS(v1temp);
			Scalar v1isqMKS = v1iMKS*v1iMKS;
			theGrid->WeightLinear(x[i], w[i]*v1iMKS, species_flux1);
			theGrid->WeightLinear(x[i], w[i]*v1isqMKS, species_enden1);
      }
    }
		else // 1 velocity push in E, constant weight particles
		{
			memset(species_scratch_flux1, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden1, 0, ng*sizeof(Scalar));
      for (i=0; i<num; i++) {
			a_e[0] = f0*E1[i];
			v1temp=v1[i]; // v1 at t-1/2
			add_v(i, a_e[0]);  // 1 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			// constant weight particles, v1, and v1sq to the mesh with unit weight, v1 and v1sq in grid units, MAL 9/12/09
			theGrid->WeightLinear1D(x[i], v1temp, species_scratch_flux1, species_scratch_enden1);
			}
			Scalar w0 = get_w0();
			Scalar wdxdt = 0.5*w0*get_dxdt();
			Scalar wdxdtsq = 0.5*wdxdt*get_dxdt();
			for (int j=0; j<ng; j++) // add weight and convert to MKS velocities, MAL 9/12/09
			{
				species_flux1[j] += species_scratch_flux1[j]*wdxdt;
				species_enden1[j] += species_scratch_enden1[j]*wdxdtsq;
			}
    }
#endif
  }
}

// advance_v3D():
// advance the particle velocities; assumes all particles in a group have
// constant q/m ratio. Do 3D weighting of v and v^2 to the grid, for 3D fluxes and energies, MAL 6/19/10

#ifdef SINGLE
void LOCALCLASS::advance_v3D(int i, Scalar ddt, Vector3 E_sp, Vector3 B_sp){ // advance v
	dt = ddt; // add, so as to not subcycle load and boundary calls to advance_v, MAL 1/1/10
#else
void LOCALCLASS::advance_v3D(Scalar ddt){ // advance v
	int subcycle = get_species()->get_subcycle(); // add for subcycle, MAL 1/1/10
	if (theFields->get_nsteps() % subcycle != 0) return; // add for subcycle, MAL 1/1/10
  dt = ddt*subcycle; // add "*subcycle" for subcycle, MAL 1/1/10
  register int i;
	int ng = theGrid->getng();
	Scalar v1temp, v2temp, v3temp;
	Scalar *species_flux1 = theSpecies->get_species_flux1(); // for flux1 (n*v1) diagnostics, MAL 9/30/09
	Scalar *species_enden1 = theSpecies->get_species_enden1(); // for enden1 (n*v1*v1) diagnostics
	Scalar *species_scratch_flux1 = theSpecies->get_species_scratch_flux1(); // for flux1 diagnostics
	Scalar *species_scratch_enden1 = theSpecies->get_species_scratch_enden1(); // for enden1 diagnostics
	Scalar *species_flux2 = theSpecies->get_species_flux2(); // for flux2 (n*v2) diagnostics, MAL 9/30/09
	Scalar *species_enden2 = theSpecies->get_species_enden2(); // for enden2 (n*v2*v2) diagnostics
	Scalar *species_scratch_flux2 = theSpecies->get_species_scratch_flux2(); // for flux2 diagnostics
	Scalar *species_scratch_enden2 = theSpecies->get_species_scratch_enden2(); // for enden2 diagnostics
	Scalar *species_flux3 = theSpecies->get_species_flux3(); // for flux3 (n*v3) diagnostics, MAL 9/30/09
	Scalar *species_enden3 = theSpecies->get_species_enden3(); // for enden3 (n*v3*v3) diagnostics
	Scalar *species_scratch_flux3 = theSpecies->get_species_scratch_flux3(); // for flux3 diagnostics
	Scalar *species_scratch_enden3 = theSpecies->get_species_scratch_enden3(); // for enden3 diagnostics

#endif
  Scalar a_e[3]; // electric field acceleration array
  Scalar f0 = dt/dxdt*qm0; // force factor for v advance

#ifndef SINGLE
  int num = get_n();
#endif

  if (theFields->is_magnetized())
  {
    // magnetized pushes; assumes B independent of position
    Scalar t1, t2; // magnetic field rotation components
    f0 *= 0.5; // split Efield advance in 2 parts
    Scalar B[2], Bmag; // magnetic field [SHOULD THERE BE 3 COMPONENTS?, JH, 6/2005]
#ifdef SINGLE
    B[0] = B_sp.e1();
    B[1] = B_sp.e2();
    Bmag = B_sp.magnitude();
#else
    theFields->get_B(B, 2); // put B1,B2 into B.
    Bmag = theFields->magB();
#endif
    t2 = tan(f0*Bmag);
    t1 = t2*B[0]/Bmag;
    t2 *= B[1]/Bmag;
    Scalar itmag1 = 1.0/(1 + t1*t1 + t2*t2);

#ifdef SINGLE
    a_e[0] = f0*E_sp.e1();
    a_e[1] = f0*E_sp.e2();
    a_e[2] = f0*E_sp.e3();
    add_v(i,a_e); // 3 velocity half push in E
    rotate_v(i, t1, t2, itmag1); // rotate v for B
    add_v(i,a_e); // 3 velocity half push in E
#else
    if (theFields->is_transverseE() & is_variableWeight()) // variable weight particles
    {
      for (i=0; i<num; i++) { // do a Boris push:
		  a_e[0] = f0*E1[i];
		  a_e[1] = f0*E2[i];
		  a_e[2] = f0*E3[i];
			v1temp=v1[i]; // v1 at t-1/2
			v2temp=v2[i]; // v2 at t-1/2
			v3temp=v3[i]; // v3 at t-1/2
		  add_v(i,a_e); // 3 velocity half push in E
		  rotate_v(i, t1, t2, itmag1); // rotate v for B
		  add_v(i,a_e); // 3 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			v2temp += v2[i]; // 2*v2 at t
			v3temp += v3[i]; // 2*v3 at t
			Scalar v1iMKS = 0.5*get_vMKS(v1temp);
			Scalar v1isqMKS = v1iMKS*v1iMKS;
			Scalar v2iMKS = 0.5*get_vMKS(v2temp);
			Scalar v2isqMKS = v2iMKS*v2iMKS;
			Scalar v3iMKS = 0.5*get_vMKS(v3temp);
			Scalar v3isqMKS = v3iMKS*v3iMKS;
			theGrid->WeightLinear(x[i], w[i]*v1iMKS, species_flux1);
			theGrid->WeightLinear(x[i], w[i]*v1isqMKS, species_enden1);
			theGrid->WeightLinear(x[i], w[i]*v2iMKS, species_flux2);
			theGrid->WeightLinear(x[i], w[i]*v2isqMKS, species_enden2);
			theGrid->WeightLinear(x[i], w[i]*v3iMKS, species_flux3);
			theGrid->WeightLinear(x[i], w[i]*v3isqMKS, species_enden3);
			}
		}
		else if (theFields->is_transverseE() & !is_variableWeight()) // constant weight particles
		{
			memset(species_scratch_flux1, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden1, 0, ng*sizeof(Scalar));
			memset(species_scratch_flux2, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden2, 0, ng*sizeof(Scalar));
			memset(species_scratch_flux3, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden3, 0, ng*sizeof(Scalar));
      for (i=0; i<num; i++) { // do a Boris push:
		  a_e[0] = f0*E1[i];
		  a_e[1] = f0*E2[i];
		  a_e[2] = f0*E3[i];
			v1temp=v1[i]; // v1 at t-1/2
			v2temp=v2[i]; // v2 at t-1/2
			v3temp=v3[i]; // v3 at t-1/2
		  add_v(i,a_e); // 3 velocity half push in E
		  rotate_v(i, t1, t2, itmag1); // rotate v for B
		  add_v(i,a_e); // 3 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			v2temp += v2[i]; // 2*v2 at t
			v3temp += v3[i]; // 2*v3 at t
			// constant weight particles, v1, v2, v3 and v1sq, v2sq v3sq to the mesh with unit weight, v's and vsq's in grid units, MAL 9/12/09
			theGrid->WeightLinear3D(x[i], v1temp, v2temp, v3temp, species_scratch_flux1,
				species_scratch_flux2, species_scratch_flux3, species_scratch_enden1, species_scratch_enden2, species_scratch_enden3);
			}
			Scalar w0 = get_w0();
			Scalar wdxdt = 0.5*w0*get_dxdt();
			Scalar wdxdtsq = 0.5*wdxdt*get_dxdt();
			for (int j=0; j<ng; j++) // add weight and convert to MKS velocities, MAL 9/12/09
			{
				species_flux1[j] += species_scratch_flux1[j]*wdxdt;
				species_enden1[j] += species_scratch_enden1[j]*wdxdtsq;
				species_flux2[j] += species_scratch_flux2[j]*wdxdt;
				species_enden2[j] += species_scratch_enden2[j]*wdxdtsq;
				species_flux3[j] += species_scratch_flux3[j]*wdxdt;
				species_enden3[j] += species_scratch_enden3[j]*wdxdtsq;
			}
    }
    else if (is_variableWeight()) // Boris push for 1D E + B, variable weight
		{
      for (i=0; i<num; i++) { // do a Boris push:
		  a_e[0] = f0*E1[i]; // theFields.E1(get_x(i));
			v1temp=v1[i]; // v1 at t-1/2
			v2temp=v2[i]; // v2 at t-1/2
			v3temp=v3[i]; // v3 at t-1/2
			add_v(i,a_e[0]); // 1 velocity half push in E
		  rotate_v(i, t1, t2, itmag1); // rotate v for B
		  add_v(i,a_e[0]); // 1 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			v2temp += v2[i]; // 2*v2 at t
			v3temp += v3[i]; // 2*v3 at t
			Scalar v1iMKS = 0.5*get_vMKS(v1temp);
			Scalar v1isqMKS = v1iMKS*v1iMKS;
			Scalar v2iMKS = 0.5*get_vMKS(v2temp);
			Scalar v2isqMKS = v2iMKS*v2iMKS;
			Scalar v3iMKS = 0.5*get_vMKS(v3temp);
			Scalar v3isqMKS = v3iMKS*v3iMKS;
			theGrid->WeightLinear(x[i], w[i]*v1iMKS, species_flux1);
			theGrid->WeightLinear(x[i], w[i]*v1isqMKS, species_enden1);
			theGrid->WeightLinear(x[i], w[i]*v2iMKS, species_flux2);
			theGrid->WeightLinear(x[i], w[i]*v2isqMKS, species_enden2);
			theGrid->WeightLinear(x[i], w[i]*v3iMKS, species_flux3);
			theGrid->WeightLinear(x[i], w[i]*v3isqMKS, species_enden3);
      }
    }
		else // Boris push for 1D E + B, constant weight particles
		{
			memset(species_scratch_flux1, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden1, 0, ng*sizeof(Scalar));
			memset(species_scratch_flux2, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden2, 0, ng*sizeof(Scalar));
			memset(species_scratch_flux3, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden3, 0, ng*sizeof(Scalar));
      for (i=0; i<num; i++) { // do a Boris push:
		  a_e[0] = f0*E1[i];
		  a_e[1] = f0*E2[i];
		  a_e[2] = f0*E3[i];
			v1temp=v1[i]; // v1 at t-1/2
			v2temp=v2[i]; // v2 at t-1/2
			v3temp=v3[i]; // v3 at t-1/2
		  add_v(i,a_e); // 3 velocity half push in E
		  rotate_v(i, t1, t2, itmag1); // rotate v for B
		  add_v(i,a_e); // 3 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			v2temp += v2[i]; // 2*v2 at t
			v3temp += v3[i]; // 2*v3 at t
			// constant weight particles, v1, v2, v3 and v1sq, v2sq v3sq to the mesh with unit weight, v's and vsq's in grid units, MAL 9/12/09
			theGrid->WeightLinear3D(x[i], v1temp, v2temp, v3temp, species_scratch_flux1,
				species_scratch_flux2, species_scratch_flux3, species_scratch_enden1, species_scratch_enden2, species_scratch_enden3);
			}
			Scalar w0 = get_w0();
			Scalar wdxdt = 0.5*w0*get_dxdt();
			Scalar wdxdtsq = 0.5*wdxdt*get_dxdt();
			for (int j=0; j<ng; j++) // add weight and convert to MKS velocities, MAL 9/12/09
			{
				species_flux1[j] += species_scratch_flux1[j]*wdxdt;
				species_enden1[j] += species_scratch_enden1[j]*wdxdtsq;
				species_flux2[j] += species_scratch_flux2[j]*wdxdt;
				species_enden2[j] += species_scratch_enden2[j]*wdxdtsq;
				species_flux3[j] += species_scratch_flux3[j]*wdxdt;
				species_enden3[j] += species_scratch_enden3[j]*wdxdtsq;
			}
    }
#endif
  }
  else {
#ifdef SINGLE
    a_e[0] = f0*E_sp.e1();
    a_e[1] = f0*E_sp.e2();
    a_e[2] = f0*E_sp.e3();
    add_v(i,a_e); // 3 velocity half push in E
#else
    // unmagnetized pushes
    if (theFields->is_transverseE() & is_variableWeight())
    {
      for (i=0; i<num; i++) { // E only push:
		  a_e[0] = f0*E1[i];
		  a_e[1] = f0*E2[i];
		  a_e[2] = f0*E3[i];
			v1temp=v1[i]; // v1 at t-1/2
			v2temp=v2[i]; // v2 at t-1/2
			v3temp=v3[i]; // v3 at t-1/2
		  add_v(i,a_e); // 3 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			v2temp += v2[i]; // 2*v2 at t
			v3temp += v3[i]; // 2*v3 at t
			Scalar v1iMKS = 0.5*get_vMKS(v1temp);
			Scalar v1isqMKS = v1iMKS*v1iMKS;
			Scalar v2iMKS = 0.5*get_vMKS(v2temp);
			Scalar v2isqMKS = v2iMKS*v2iMKS;
			Scalar v3iMKS = 0.5*get_vMKS(v3temp);
			Scalar v3isqMKS = v3iMKS*v3iMKS;
			theGrid->WeightLinear(x[i], w[i]*v1iMKS, species_flux1);
			theGrid->WeightLinear(x[i], w[i]*v1isqMKS, species_enden1);
			theGrid->WeightLinear(x[i], w[i]*v2iMKS, species_flux2);
			theGrid->WeightLinear(x[i], w[i]*v2isqMKS, species_enden2);
			theGrid->WeightLinear(x[i], w[i]*v3iMKS, species_flux3);
			theGrid->WeightLinear(x[i], w[i]*v3isqMKS, species_enden3);
      }
    }
    else if (theFields->is_transverseE() & !is_variableWeight())
		{
			memset(species_scratch_flux1, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden1, 0, ng*sizeof(Scalar));
			memset(species_scratch_flux2, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden2, 0, ng*sizeof(Scalar));
			memset(species_scratch_flux3, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden3, 0, ng*sizeof(Scalar));
      for (i=0; i<num; i++) { // E only push:
		  a_e[0] = f0*E1[i];
		  a_e[1] = f0*E2[i];
		  a_e[2] = f0*E3[i];
			v1temp=v1[i]; // v1 at t-1/2
			v2temp=v2[i]; // v2 at t-1/2
			v3temp=v3[i]; // v3 at t-1/2
		  add_v(i,a_e); // 3 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			v2temp += v2[i]; // 2*v2 at t
			v3temp += v3[i]; // 2*v3 at t
			// constant weight particles, v1, v2, v3 and v1sq, v2sq v3sq to the mesh with unit weight, v's and vsq's in grid units, MAL 9/12/09
			theGrid->WeightLinear3D(x[i], v1temp, v2temp, v3temp, species_scratch_flux1,
				species_scratch_flux2, species_scratch_flux3, species_scratch_enden1, species_scratch_enden2, species_scratch_enden3);
			}
			Scalar w0 = get_w0();
			Scalar wdxdt = 0.5*w0*get_dxdt();
			Scalar wdxdtsq = 0.5*wdxdt*get_dxdt();
			for (int j=0; j<ng; j++) // add weight and convert to MKS velocities, MAL 9/12/09
			{
				species_flux1[j] += species_scratch_flux1[j]*wdxdt;
				species_enden1[j] += species_scratch_enden1[j]*wdxdtsq;
				species_flux2[j] += species_scratch_flux2[j]*wdxdt;
				species_enden2[j] += species_scratch_enden2[j]*wdxdtsq;
				species_flux3[j] += species_scratch_flux3[j]*wdxdt;
				species_enden3[j] += species_scratch_enden3[j]*wdxdtsq;
			}
    }
		else if (is_variableWeight()) // 1 velocity push in E, variable weight particles
		{
      for (i=0; i<num; i++) {
			a_e[0] = f0*E1[i];
			v1temp=v1[i]; // v1 at t-1/2
			v2temp=v2[i]; // v2 at t-1/2
			v3temp=v3[i]; // v3 at t-1/2
			add_v(i, a_e[0]);  // 1 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			v2temp += v2[i]; // 2*v2 at t
			v3temp += v3[i]; // 2*v3 at t
			Scalar v1iMKS = 0.5*get_vMKS(v1temp);
			Scalar v1isqMKS = v1iMKS*v1iMKS;
			Scalar v2iMKS = 0.5*get_vMKS(v2temp);
			Scalar v2isqMKS = v2iMKS*v2iMKS;
			Scalar v3iMKS = 0.5*get_vMKS(v3temp);
			Scalar v3isqMKS = v3iMKS*v3iMKS;
			theGrid->WeightLinear(x[i], w[i]*v1iMKS, species_flux1);
			theGrid->WeightLinear(x[i], w[i]*v1isqMKS, species_enden1);
			theGrid->WeightLinear(x[i], w[i]*v2iMKS, species_flux2);
			theGrid->WeightLinear(x[i], w[i]*v2isqMKS, species_enden2);
			theGrid->WeightLinear(x[i], w[i]*v3iMKS, species_flux3);
			theGrid->WeightLinear(x[i], w[i]*v3isqMKS, species_enden3);
      }
    }
		else // 1 velocity push in E, constant weight particles
		{
			memset(species_scratch_flux1, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden1, 0, ng*sizeof(Scalar));
			memset(species_scratch_flux2, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden2, 0, ng*sizeof(Scalar));
			memset(species_scratch_flux3, 0, ng*sizeof(Scalar));
			memset(species_scratch_enden3, 0, ng*sizeof(Scalar));
      for (i=0; i<num; i++) {
			a_e[0] = f0*E1[i];
			v1temp=v1[i]; // v1 at t-1/2
			v2temp=v2[i]; // v2 at t-1/2
			v3temp=v3[i]; // v3 at t-1/2
			add_v(i, a_e[0]);  // 1 velocity half push in E
			v1temp += v1[i]; // 2*v1 at t
			v2temp += v2[i]; // 2*v2 at t
			v3temp += v3[i]; // 2*v3 at t
			// constant weight particles, v1, v2, v3 and v1sq, v2sq v3sq to the mesh with unit weight, v's and vsq's in grid units, MAL 9/12/09
			theGrid->WeightLinear3D(x[i], v1temp, v2temp, v3temp, species_scratch_flux1,
				species_scratch_flux2, species_scratch_flux3, species_scratch_enden1, species_scratch_enden2, species_scratch_enden3);
			}
			Scalar w0 = get_w0();
			Scalar wdxdt = 0.5*w0*get_dxdt();
			Scalar wdxdtsq = 0.5*wdxdt*get_dxdt();
			for (int j=0; j<ng; j++) // add weight and convert to MKS velocities, MAL 9/12/09
			{
				species_flux1[j] += species_scratch_flux1[j]*wdxdt;
				species_enden1[j] += species_scratch_enden1[j]*wdxdtsq;
				species_flux2[j] += species_scratch_flux2[j]*wdxdt;
				species_enden2[j] += species_scratch_enden2[j]*wdxdtsq;
				species_flux3[j] += species_scratch_flux3[j]*wdxdt;
				species_enden3[j] += species_scratch_enden3[j]*wdxdtsq;
			}
    }
#endif
  }
}
