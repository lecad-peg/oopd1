#ifndef UNIFORMMESH_H
#define UNIFORMMESH_H

#include "utils/ovector.hpp"

// forward declaration
class DataArray;

class UniformMesh
{
private:
	DataArray *thearray;
	DataArray *xarray;
	int ng;
	Scalar min, max, dx;
	Scalar missed_weight; // weight not accumulated
	int weight_order; // order of weighting function
public:
	UniformMesh();
	UniformMesh(DataArray * _thearray, Scalar _min, Scalar _max);
	~UniformMesh();

	void init_mesh(DataArray * _thearray, Scalar _min, Scalar _max);
	void init_mesh(int _ng, Scalar _min, Scalar _max);
	DataArray * get_xarray(ostring name="");

	inline Scalar val(int i) { return min + i*dx; }

	// accessor functions
	inline int size(void) {    return ng;  }
	inline Scalar get_min(void) { return min; }
	inline Scalar get_max(void) { return max; }
	inline Scalar get_dx(void) { return dx; }			// JK, 2018-01-15

	// indexing functions
	int floor_index(Scalar xval, Scalar *frac=0);

	// weighting functions
	void weight(Scalar xval, Scalar weight);
	void weight_NGP(Scalar xval, Scalar weight);
	void weight_linear(Scalar xval, Scalar weight);

	bool is_within(UniformMesh & a);
};

#endif
