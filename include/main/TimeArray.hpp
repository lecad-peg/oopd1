#ifndef TIMEARRAY_H
#define TIMEARRAY_H

#include "utils/VariableArray.hpp"

class TimeArray : public VariableArray
{
private:
public:
  TimeArray();
  TimeArray(int n);
};
#endif
