#ifndef BOLTZMANNMODEL
#define BOLTZMANNMODEL
#include "utils/ovector.hpp"
#include "main/parse.hpp"
// forward declarations
//class Species;
class PoissonSolver;
class VDistribution;
//class Fluid;
#include "main/species.hpp"
#include "atomic_properties/fluid.hpp"

class BoltzmannModel : public Fluid, public Parse
{
private:
  Scalar T; // temperature in eV [PLEASE FIX UNITS!!!]
  Scalar n0; // density where electrostatic potential = 0
  Scalar qeT; // q/eT
  Scalar deriv_coeff;
  bool quasineutral; // whether to start quasineutral
  bool n0const;
  Species * thespecies;
  Scalar flux_in;

  Scalar normalized_density(int i);

public:
  BoltzmannModel(Species * _thespecies, bool _parse);
  BoltzmannModel(PoissonSolver * theps, Species * _thespecies);
  BoltzmannModel(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		 oopicList<Parameter<int> > int_variables,
		 Species * _thespecies);
  BoltzmannModel(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		 oopicList<Parameter<int> > int_variables,
		 PoissonSolver * theps, Species * _thespecies);

  // accessor functions
  inline Scalar get_T(void) { return T; }
  inline void set_T(Scalar Ttmp) { T = Ttmp; }
  inline Scalar get_n0(void) { return n0; }
  Scalar get_n0(Scalar _N_tot, Scalar * _phi);
  inline void set_n0(Scalar n0tmp)
  {
    n0 = n0tmp;
    deriv_coeff = -qeT*thespecies->get_q()*n0;
  }
  inline Scalar get_rho0(void);
  inline bool is_quasineutral() { return quasineutral; }

  // virtual functions from Parse
  void setdefaults(void);
  void setparamgroup(void);
  void check(void);
  void init(void);

  // virtual functions from Fluid
  void set_thefield(Fields * _thefield);
  VDistribution * velocity_distribution(int node);
  Vector3 get_v_from_distribution(int node);
  Scalar electrostatic_density(Scalar potential);
  Scalar electrostatic_derivative(Scalar potential);
  void update(Scalar dt);
  void update_N_tot();
  void update_fields(Scalar *phi);
  Fluid * get_copy(void);
  void init_diagnostics(void);

  void calculate_density(void);
  Scalar calculate_Ntot(Scalar _n0 = 1.);

};
#endif
