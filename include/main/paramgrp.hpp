#ifndef PARAMGROUP_H
#define PARAMGROUP_H

// paramgrp.hpp -- classes for ParamGroup (Parameter groups)

#include<stdio.h>
#include<stdarg.h>

#include "utils/ovector.hpp"
#include "main/pd1.hpp"
#include "utils/ostring.hpp"
#include "main/oopiclist.hpp"
#include "utils/equation.hpp"

// include files associated with parameters
#include "main/param.hpp"
#include "utils/param_func.hpp"
#include "main/enumlist.hpp"

// class VariableGroup:  not currently used.  Designed
// to be a replacement someday for the variables in
// ParamGroup
class VariableGroup
{
	// variables
	oopicList<Parameter<int> > int_variables;
	oopicList<Parameter<Scalar> > float_variables;
	oopicList<Equation> equations;
};

// class ParamGroup -- groups of parameters
// and methods to parse them
class ParamGroup
{
private:
	int currentlinenumber;
	bool forcechecking;

public:
	// variables
	oopicList<Parameter<int> > int_variables;
	oopicList<Parameter<Scalar> > float_variables;
	oopicList<Equation> global_equations;
	//  VariableGroup vg;

	// parameters
	oopicList<Parameter<int> > iparam;
	oopicList<Parameter<Scalar> > fparam;
	oopicList<Flag<int> > intflag;
	oopicList<Flag<bool> > boolflag;
	oopicList<Parameter<ostring> > strings;
	oopicList<Parameter<vector<ostring> > > stringlist;

	// equations
	oopicList<Func> funcparam;

	// "global" (but not static) variables for all Parse objects
	bool debug;

	ParamGroup()
	{
		// no debugging by default
		debug = false;
		add_bflag("debug", &debug, true,
				"activate debugging [works on all objects]");

		// start line counter to (-1) [uninitialized]
		currentlinenumber = -1;

		forcechecking = true;
	}

	~ParamGroup()
	{
		clear();
	}

	void clear(void)
	{
		// mindgame: to avoid memory leaks
		int_variables.deleteAll();
		float_variables.deleteAll();
		iparam.deleteAll();
		fparam.deleteAll();
		intflag.deleteAll();
		boolflag.deleteAll();
		strings.deleteAll();
		stringlist.deleteAll();
		funcparam.deleteAll();
	}

	void setlinenumber(int nll)
	{
		currentlinenumber = nll;
	}

	void setchecking(bool val)
	{
		forcechecking = val;
	}

	// print -- print parameters and their values to FILE *fk
	void print(FILE *fk = stdout, bool htmlmode=false)
	{
		ostring setstring;
		oopicListIter<Parameter<ostring> > thes(strings);
		oopicListIter<Parameter<vector<ostring> > > thel(stringlist);
		oopicListIter<Flag<int> > theiflag(intflag);
		oopicListIter<Flag<bool> > thebflag(boolflag);
		oopicListIter<Parameter<int> > thei(iparam);
		oopicListIter<Parameter<Scalar> > thef(fparam);
		oopicListIter<Func> thefunc(funcparam);

		if(htmlmode) { fprintf(fk, "\n<ul>\n"); }

#ifdef DEBUG
		oopicListIter<Parameter<int> > theivar(int_variables);
		oopicListIter<Parameter<Scalar> > thefvar(float_variables);
		// print variables
		for(theivar.restart(); !theivar.Done(); theivar++)
		{
			if(htmlmode) { fprintf(fk, "<li>"); }
			fprintf(fk, "\n  %s=%d", theivar()->name(), theivar()->val());
		}

		for(thefvar.restart(); !thefvar.Done(); thefvar++)
		{
			if(htmlmode) { fprintf(fk, "<li>"); }
			fprintf(fk, "\n  %s=%e", thefvar()->name(), thefvar()->val());
		}
#endif

		// print parameters
		for(thes.restart(); !thes.Done(); thes++)
		{
			if(htmlmode) { fprintf(fk, "<li>"); }
			fprintf(fk, "\n  %s=\"%s\" ; (%s) %s", thes()->name(),
					thes()->val().c_str(), thes()->type(),
					thes()->desc());
		}

		for(thel.restart(); !thel.Done(); thel++)
		{
			if(htmlmode) { fprintf(fk, "<li>"); }
			fprintf(fk, "\n  %s=\"", thel()->name());
			for(int i = thel()->val().size()-1; i >= 0; i--)
			{
				fprintf(fk, "%s", thel()->val()[i].c_str());
				if (i > 0) fprintf(fk, ",");
			}
			fprintf(fk, "\" ; (%s) %s", thel()->type(), thel()->desc());
		}

		for(theiflag.restart(); !theiflag.Done(); theiflag++)
		{
			if(htmlmode) { fprintf(fk, "<li>"); }
			setstring = "";
			if(theiflag()->is_set()) { setstring = "[default]"; }
			fprintf(fk, "\n  %s ; %s %s", theiflag()->name(),
					setstring(), theiflag()->desc());
		}


		for(thebflag.restart(); !thebflag.Done(); thebflag++)
		{
			if(htmlmode) { fprintf(fk, "<li>"); }
			setstring = "";
			if(thebflag()->is_set()) { setstring = "[default]"; }

			fprintf(fk, "\n  %s ; %s %s", thebflag()->name(), setstring(),
					thebflag()->desc());
		}

		for(thei.restart(); !thei.Done(); thei++)
		{
			if(htmlmode) { fprintf(fk, "<li>"); }
			fprintf(fk, "\n  %s=%d ; (%s) %s", thei()->name(), thei()->val(),
					thei()->type(), thei()->desc());
		}

		for(thef.restart(); !thef.Done(); thef++)
		{
			if(htmlmode) { fprintf(fk, "<li>"); }
			fprintf(fk, "\n  %s=%g ; (%s) %s", thef()->name(), thef()->val(),
					thef()->type(), thef()->desc());
		}

		for(thefunc.restart(); !thefunc.Done(); thefunc++)
		{
			if(htmlmode) { fprintf(fk, "<li>"); }
			fprintf(fk, "\n  %s=%s ; (equation) %s", thefunc()->lhs().c_str(),
					thefunc()->get_rhs().c_str(),
					thefunc()->get_description().c_str());
		}

		if(htmlmode) { fprintf(fk, "\n</ul>\n"); }
		fprintf(fk, "\n");
	}

	/////////////////////////////////////////////
	// adding functions for types of parameters
	/////////////////////////////////////////////

	void add_ivar(const char *name, int i)
	{
		int_variables.add(new Parameter<int>(name, i));
	}

	void add_fvar(const char *name, Scalar f)
	{
		float_variables.add(new Parameter<Scalar>(name, f));
	}

	void add_iparameter(const char *name, int *i, const char *desc=0)
	{
		if(desc)
		{
			iparam.add(new Parameter<int>(name, i, desc, "int"));
		}
		else
		{
			iparam.add(new Parameter<int>(name, i, "", "int"));
		}
	}
	void add_fparameter(const char *name, Scalar *f, const char *desc=0)
	{
		if(desc)
		{
			fparam.add(new Parameter<Scalar>(name, f, desc, "scalar"));
		}
		else
		{
			fparam.add(new Parameter<Scalar>(name, f, "", "scalar"));
		}
	}
	void add_string(const char *name, ostring *s, const char *desc=0)
	{
		if(desc)
		{
			strings.add(new Parameter<ostring>(name, s, desc, "string"));
		}
		else
		{
			strings.add(new Parameter<ostring>(name, s, "", "string"));
		}
	}
	// mindgame: add a list of strings
	void add_strings(const char *name, vector<ostring> *s, const char *desc=0)
	{
		if(desc)
		{
			stringlist.add(new Parameter<vector<ostring> >(name, s, desc, "strings"));
		}
		else
		{
			stringlist.add(new Parameter<vector<ostring> >(name, s, "", "strings"));
		}
	}
	void add_flag(const char *name, int *i, int val, const char *desc=0)
	{
		if(desc)
		{
			intflag.add(new Flag<int>(name, i, val, desc));
		}
		else
		{
			intflag.add(new Flag<int>(name, i, val, ""));
		}
	}

	void add_bflag(const char *name, bool *i, bool val, const char *desc=0)
	{
		if(desc)
		{
			boolflag.add(new Flag<bool>(name, i, val, desc));
		}
		else
		{
			boolflag.add(new Flag<bool>(name, i, val, ""));
		}
	}

	// add_reverse_bflags --
	// adds a list of length of boolflag with the same
	// bool *, the opposite value, and the name predicated
	// by the string "no"
	void add_reverse_bflags(void)
	{
		oopicList<Flag<bool> > tmp;

		oopicListIter<Flag<bool> > thei(boolflag);

		for(thei.restart(); !thei.Done(); thei++)
		{
			tmp.add(new Flag<bool>(ostring("no") + thei()->name,
					thei()->getval(), !thei()->gettag()));
		}

		oopicListIter<Flag<bool> > thej(tmp);
		for(thej.restart(); !thej.Done(); thej++)
		{
			boolflag.add(thej());
		}

		tmp.removeAll();
	}

	// add_function -- add a functional Parameter
	void add_function(const char *name, const char * desc, Equation *teqp, int nvar, ...)
	{
		const char *place;
		va_list ap;
		oopicList<ostring> tmpnames;

		va_start(ap, nvar);
		while(nvar--)
		{
			place = va_arg(ap, const char *);
			tmpnames.addToEnd(new ostring(place));
		}
		va_end(ap);

		// trying to figure out why all variables don't get passed, JH, 8/15
#ifdef DEBUG
		oopicListIter<ostring> thei(tmpnames);
		fprintf(stderr, "Function %s:\n", name);
		for(thei.restart(); !thei.Done(); thei++)
		{
			fprintf(stderr, ">%s\n", thei()->c_str());
		}
#endif

		Func * fptr;
		fptr = new Func(ostring(name), teqp, tmpnames);

		funcparam.add(fptr);
		fptr->set_description(desc);
		//      tmpnames.deleteAll();
	}




	void error(const char *line, const char *message, int i=1) const
	{
		if(i){ printf("\nError, "); }
		else { printf("\nWarning, "); }
		printf("line %d: ", currentlinenumber);
		printf("\"%s\"", line);
		printf("\n%s\n", message);
		//      print();
		if(i) { exit(1); }
	}


	bool is_dependent(Equation *testeq, ostring testvar)
	{
		oopicListIter<Func> thefunc(funcparam);

		if(!testeq) { return false; } // should this be here?

		for(thefunc.restart(); !thefunc.Done(); thefunc++)
		{
			if(thefunc()->get_equation() == testeq)
			{
				return thefunc()->is_dependent(testvar);
			}
		}
		error("is_dependent(,)", "equation not found");
		return true;
	}

	bool is_dependent(Equation *testeq, const char *testvar)
	{
		return is_dependent(testeq, ostring(testvar));
	}

	// add variables and global Equations to myeq
	void add_variables(Equation *myeq)
	{
		oopicListIter<Parameter<int> > theivar(int_variables);
		oopicListIter<Parameter<Scalar> > thefvar(float_variables);
		oopicListIter<Equation> theeqvar(global_equations);

		// add variables
		for(theivar.restart(); !theivar.Done(); theivar++)
		{
			myeq->add_Constant(theivar()->name(),	theivar()->val());
		}
		for(thefvar.restart(); !thefvar.Done(); thefvar++)
		{
			myeq->add_Constant(thefvar()->name(), thefvar()->val());
		}
		// add equations
		for(theeqvar.restart(); !theeqvar.Done(); theeqvar++)
		{
			myeq->add_Equation(theeqvar());
		}
	}

	// returns true on error
	bool parse_equation(Equation *myeq, ostring str)
	{

		myeq->set_nonfatal(); // let parse capture error

		// add variables
		add_variables(myeq);

		bool eqerrflag = false;
		if(myeq->parseequation(str) == 0) { eqerrflag = true; }
		else
		{
			if(myeq->initfparser() != -1) { eqerrflag = true; }
		}

		return eqerrflag;
	}

	// assumes whitespace and comments have been stripped
	bool parse(ostring str, int varparse=0)
	{
		int eqloc, ival, iflag, fflag;
		double fval;
		char *tmp1, *tmp2;
		Equation  *tmpequation;
		bool consteqflag;
		ostring tmp;

		consteqflag = false;
		tmpequation = NULL;
		ival = 0;
		fval = 0.;
		iflag = fflag = 0;

		oopicListIter<Parameter<int> > theivar(int_variables);
		oopicListIter<Parameter<Scalar> > thefvar(float_variables);

		oopicListIter<Parameter<int> > thei(iparam);
		oopicListIter<Parameter<Scalar> > thef(fparam);
		oopicListIter<Parameter<ostring> > thes(strings);
		oopicListIter<Parameter<vector<ostring> > > thel(stringlist);

		oopicListIter<Func> thefunc(funcparam);

		oopicListIter<Flag<int> > theflag(intflag);
		oopicListIter<Flag<bool> > thebflag(boolflag);

		if(str.charcount('=') > 1)
		{
			error(str(), "Only one equals sign supported per line");
		}

		tmp1 = new char[strlen(str) + 1];
		tmp2 = new char[strlen(str) + 1];

		eqloc = str.find('=', 0);
		if(eqloc >= 0)
		{
			strcpy(tmp1, str());
			tmp1[eqloc] = '\0';
			strcpy(tmp2, str()+eqloc+1);

			tmp = tmp2;
			if(tmp.isint())
			{
				iflag = 1;
				sscanf(tmp(), "%d", &ival);
			}
			else if(tmp.isfloat())
			{
				fflag = 1;
				sscanf(tmp(), "%lf", &fval);
			}
			else
			{
				for(theivar.restart(); !theivar.Done(); theivar++)
				{
					if(theivar()->name == tmp)
					{
						ival = (*theivar()).val();
						iflag = 1;
						break;
					}
				}
				if(theivar.Done())
				{
					for(thefvar.restart(); !thefvar.Done(); thefvar++)
					{
						if(thefvar()->name == tmp)
						{
							fval = (*thefvar()).val();
							fflag = 1;
							break;
						}
					}
				}

			}

			if(varparse)
			{
				if((iflag == 0) && (fflag == 0))
				{
					delete [] tmp1;
					delete [] tmp2;
					error(str(), "String variables not supported!");
				}
				if(iflag)
				{
					add_ivar(tmp1, ival);
				}
				else
				{
					add_fvar(tmp1, fval);
				}

				delete [] tmp1;
				delete [] tmp2;

				return true;
			}

			ostring lhstmp = tmp1;

			if((iflag || fflag) && (lhstmp.charcount('(') > 0))
			{
				iflag = fflag = 0;
			}

			if((iflag == 0) && (fflag == 0))
			{
				bool sflag = false;
				for(thes.restart(); !thes.Done(); thes++)
				{
					if(thes()->name == ostring(tmp1))
					{
						thes()->setval(tmp);
						sflag = true;
						break;
					}
				}
				if (!sflag)
				{
					for(thel.restart(); !thel.Done(); thel++)
					{
						if(thel()->name == ostring(tmp1))
						{
							thel()->setval(tmp.partition(','));
							sflag = true;
							break;
						}
					}
				}
				if(sflag)
				{
					delete [] tmp1;
					delete [] tmp2;
					return true;
				}

				//////// parse equations //////////
				tmpequation = new Equation();
				bool eqerrflag = parse_equation(tmpequation, str);
				if(eqerrflag)
				{
					if(forcechecking)
					{
						error(str(),
								"Right hand size is not a variable or number.");
					}
					return false;
				}
				// treat special case of constant equations
				consteqflag = tmpequation->is_constant();

				if(consteqflag)
				{
					// mindgame: to avoid compilation warning
					// Scalar *tmps;
					//  fval = tmpequation->eval(tmps);
					fval = tmpequation->eval();
					fflag = true;
					strcpy(tmp1, tmpequation->get_equation_name().c_str());

					delete tmpequation;
					tmpequation = 0;
				}
			}

			for(thei.restart(); !thei.Done(); thei++)
			{
				if(thei()->name == ostring(tmp1))
				{
					if(fflag)
					{
						error(str(),
								"Right hand side conversion of float to int not supported");
					}
					thei()->setval(ival);
					break;
				}
			}
			if(thei.Done())
			{
				for(thef.restart(); !thef.Done(); thef++)
				{
					if(thef()->name == ostring(tmp1))
					{
						if(fflag)
						{
							thef()->setval(fval);
							break;
						}
						else
						{
							thef()->setval(Scalar(ival));
							break;
						}
					}
				}
				if(thef.Done())
				{
					// test for Funcs
					Func *tf;

					ostring teststring = "";
					if(iflag) { fval = Scalar(ival); fflag = true; }
					if(fflag) { teststring = tmp1; }
					if(tmpequation != NULL)
					{
						teststring = tmpequation->get_equation_name();
					}
					if(teststring == ostring(""))
					{
						delete [] tmp1;
						delete [] tmp2;
						error(str(), "You shouldn't ever see this");
					}

					// reparse functions
					for(thefunc.restart(); !thefunc.Done(); thefunc++)
					{
						if(thefunc()->get_name() == teststring)
						{
#ifdef DEBUG
							fprintf(stderr, "reparsing equation %s\n",
									teststring());
#endif

							tf = thefunc();
							Equation *neweq = tf->get_equation();
							// test variables for equations
							if(tmpequation)
							{
								bool vartest = true;
								oopicListIter<ostring> vn(tf->varnames);

								for(vn.restart(); !vn.Done(); vn++)
								{
									if(!tmpequation->is_independent(*(vn())))
									{
										vartest = false;
									}
								}
								if(!vartest){ error(str(), "Invalid variable");}

								ostring psstr;
								psstr = tf->reparse(tmpequation);
								delete tmpequation;
								neweq->reset_variables();
								if(parse_equation(neweq, psstr))
								{
									error("Function parser", "syntax error");
								}
								neweq->Optimize();

								break;
							}
							else // constant
							{
								tf->set_constant(fval);
								break;
							}
						}
					}

					if(thefunc.Done())
					{
						delete [] tmp1;
						delete [] tmp2;

						if(forcechecking)
						{
							error(str(), "No such left hand side variable!");
						}
						return false;
					}
				}
			}
		}
		else
		{
			if(varparse)
			{
				delete [] tmp1;
				delete [] tmp2;

				error(str(), "No '=' sign in variable declaration");
			}

			for(thebflag.restart(); !thebflag.Done(); thebflag++)
			{
				if(thebflag()->name == str)
				{
					thebflag()->settag();
					break;
				}
			}
			if(thebflag.Done())
			{
				for(theflag.restart(); !theflag.Done(); theflag++)
				{
					if(theflag()->name == str)
					{
						theflag()->settag();
						break;
					}
				}
				if(theflag.Done())
				{
					delete [] tmp1;
					delete [] tmp2;

					if(forcechecking)
					{
						error(str(), "Keyword not found");
					}
					return false;
				}
			}
		}

		delete [] tmp1;
		delete [] tmp2;
		return true;
	}

	bool parse(const char *c)
	{
		return parse(ostring(c));
	}
};

#endif
