#ifndef CONDUCTOR_H
#define CONDUCTOR_H

#include "main/boundary.hpp"
#include "main/oopiclist.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/param.hpp"

// forward class declarations
class Circuit;
class Boundary;
class TriMatrix;
class Fields;
class Species;
class PoissonSolver;
class VDistribution;

class Conductor : public Boundary
{
private:
	Circuit *thecircuit;
	Scalar Aplus, Aminus, V, Vplus, Vminus;
	Scalar circuit_Q, lhs_coeff, rhs_coeff; // Added by JDN.
	int idealvoltagesource;
	Scalar dQ_circ; // Charge supplied by ideal voltage source at time t, MAL 12/4/09
	Scalar dQ_circ1; // Charge supplied by ideal voltage source at time t - dt, MAL 12/5/09
	Scalar dQ_circ2; // Charge supplied by ideal voltage source at time t - 2dt, MAL 12/5/09
	Scalar I_plushalfdt; // Circuit current at t+dt/2 on current source conductor, MAL 12/5/09
	Scalar I_minushalfdt; // Circuit current at t-dt/2 on current source conductor, MAL 12/5/09
	Scalar tempP_circ; // for time average power at conductor, MAL 12/5/09

public:
	Conductor(Fields *t, int i); //: Boundary(t)
	Conductor(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
			oopicList<Parameter<int> > int_variables, Fields *t);
	~Conductor();

	Property particle_BC(ParticleGroup *p, int i, Scalar frac, Direction j);
	Scalar fluid_BC(Scalar density, VDistribution *v, Direction dir,
                  vector<Scalar> * moments); // added, but not used, MAL200223
	// to calculate the outgoing fluxes at the boundaries:
	void evolvingfluid_BC(Species * ispecies, Scalar density, Scalar recombcoef, VDistribution *v, Direction dir); // add, MAL200223
	void update_BC();

	inline bool is_idealvoltagesource() { return bool(idealvoltagesource); }
	inline Circuit * get_thecircuit() { return thecircuit; } // for dumpfile access to circuit q's, MAL 5/9/10
	inline Scalar get_I_plushalfdt() const { return I_plushalfdt; } // for dumpfile access to previous current source current
	inline void set_I_plushalfdt(Scalar f) { I_plushalfdt = f; } // for dumpfile access to previous current source current
	inline Scalar get_dQ_circ() const { return dQ_circ; } // for dumpfile access to previous voltage source charge, MAL 5/21/10
	inline void set_dQ_circ(Scalar f) { dQ_circ = f; } // for dumpfile access to previous current source current
	inline Scalar get_dQ_circ1() const { return dQ_circ1; } // for dumpfile access to previous voltage source charge
	inline void set_dQ_circ1(Scalar f) { dQ_circ1 = f; } // for dumpfile access to previous voltage source charge

	// functions for interaction with Poisson field
	void modify_coeffs(PoissonSolver * theps);

	Scalar get_sigma(unsigned int i, Scalar * phi, Scalar * rho, bool coulombs=true);
	void calculate_RLC_I_and_P(unsigned int j); // for RLC circuit current and power, MAL 12/6/09
	void calculate_idealcurrent_I_and_P(unsigned int j); // for ideal current source circuit current and power, MAL 12/6/09
	void calculate_idealvoltage_I_and_P(unsigned int j); // for ideal voltage source circuit current and power, MAL 12/6/09

	// functions inherited from Parse
	void setdefaults();
	void setparamgroup();
	void check();
	void init();
	Parse * special(ostring s);

	void check_conductor();
	void init_conductor();
	Parse * special_conductor(ostring s);

};

#endif
