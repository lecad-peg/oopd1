// Energy-Velocity conversion functions and variables
//   : For Species, ParticleGroup, and ParticleGroupArray Classes
//
// Original by HyunChul Kim, 2005
//
// In class 'PG Array'
//  In functions get_beta*(), get_speed*(), get_energy*(), and get_gamma*(),
// the normalized value of velocity and beta is expected for both arguments
// and return values. But it is assumed that energy and gamma values are not
// normalized unlike velocity and beta. The weighting value is not multiplied
// to all of them.
//  Be careful that the argument in most of functions is velocity^2 (v2 or u2)
// instead of velocity itself (v or u) for computational efficiency.
//  get_energy() and get_energy_eV() return energy in units of Joule and eV,
// respectively. The argument of get_beta() and get_speed() is energy in units
// of eV.
//  The argument and return value of get_xxx() are relativistic velocity
// 'u = gamma*v', while the argument and return value of get_xxx2() is usual
// velocity 'v'.
//
// In class 'Species'
//  The functions get_beta*(), get_speed*(), get_energy*(), and get_gamma*()
// accept arguments in MKS units (without normalization) and return in MKS
// units.


protected:
#ifndef PGARRAY_PART
Scalar m0c_sq_e, half_m0_e;
double nr_eV_limit, nr_v2_limit, nr_u2_limit, nr_u_limit;
#endif

#ifdef SPECIES_PART
bool force_nr_flag;

void set_nr_limit()
{
  if (force_nr_flag)
    {
      nr_u_limit = SPEED_OF_LIGHT;
      nr_u2_limit = SPEED_OF_LIGHT_SQ;
    }
  else
    {
      nr_u2_limit = nr_u_limit*nr_u_limit;
      if (nr_u_limit >= SPEED_OF_LIGHT)
	{
	  force_nr_flag = true;
	}
      else
	{
	  nr_v2_limit = nr_u2_limit/get_gamma_sq_nr_limit(nr_u2_limit);
	  nr_eV_limit = get_energy_eV_nr_limit(nr_u2_limit);
	}
    }
}

inline Scalar get_gamma2(Scalar v2) const
{
  if (force_nr_flag || v2 < nr_v2_limit)
    return 1.;
  else
    return 1/sqrt(1.- iSPEED_OF_LIGHT_SQ*v2);
}

inline Scalar get_gamma(Scalar u2) const
{
  if (u2 < nr_u2_limit)
    return 1.;
  else
    return sqrt(1. + iSPEED_OF_LIGHT_SQ*u2);
}

#endif

#ifdef PGARRAY_PART
inline Scalar get_gamma2(Scalar v2) const
{
  if (is_nr_forced() || v2 < nr_v2_limit)
    return 1.;
  else
    return 1/sqrt(1.- iSPEED_OF_LIGHT_SQ_NORM*v2);
}

inline Scalar get_gamma(Scalar u2) const
{
  if (u2 < nr_u2_limit)
    return 1.;
  else
    return sqrt(1. + iSPEED_OF_LIGHT_SQ_NORM*u2);
}
#else
inline Scalar get_half_m0_e() const { return half_m0_e; }
inline Scalar get_m0c_sq_e() const {return m0c_sq_e;} // rest mass energy
#endif

#ifdef PG_PART
double SPEED_OF_LIGHT_NORM;
double iSPEED_OF_LIGHT_NORM;
double SPEED_OF_LIGHT_SQ_NORM;
double iSPEED_OF_LIGHT_SQ_NORM;

void set_nr_limit()
{
  double temp;
  temp = theSpecies->get_nr_u_limit();
  nr_u_limit = temp/dxdt;
  nr_u2_limit = sqr(nr_u_limit);
  if (!is_nr_forced())
    {
      temp*=temp;
      nr_v2_limit = nr_u2_limit/theSpecies->get_gamma_sq_nr_limit(temp);
      nr_eV_limit = theSpecies->get_energy_eV_nr_limit(temp);
    }
}

void set_norm_light_speed()
{
  SPEED_OF_LIGHT_NORM = SPEED_OF_LIGHT/dxdt;
  iSPEED_OF_LIGHT_NORM = 1. / SPEED_OF_LIGHT_NORM;
  SPEED_OF_LIGHT_SQ_NORM = sqr(SPEED_OF_LIGHT_NORM);
  iSPEED_OF_LIGHT_SQ_NORM = 1. / SPEED_OF_LIGHT_SQ_NORM;
}

bool is_nr_forced() const
{
  return (theSpecies->is_nr_forced());
}
#endif

public:

#ifdef SPECIES_PART
inline bool is_nr_forced() const { return force_nr_flag; }


// Only for special purpose.
inline double get_gamma_sq_nr_limit(double u2) const
{
  return (1. + iSPEED_OF_LIGHT_SQ*u2);
}

// Only for special purpose.
inline double get_energy_eV_nr_limit(double u2) const
{
  return (get_gamma(u2)-1.)*get_m0c_sq_e();
}

inline Scalar get_gamma(Vector3 u) const
{
  Scalar u2 = u*u;
  if (u2 < nr_u2_limit)
    return 1.;
  else
    return sqrt(1. + iSPEED_OF_LIGHT_SQ*u2);
}

inline Scalar get_beta(Scalar energy) const
{
  if (is_nr_forced() || energy < nr_eV_limit)
    return get_speed(energy)*iSPEED_OF_LIGHT;
  else
    {
      Scalar gamma=energy/get_m0c_sq_e()+1;
      return sqrt(gamma*gamma -1);
    }
}

inline Scalar get_gamma2(Vector3 v) const
{
  Scalar v2 = v*v;
  if (is_nr_forced() || v2 < nr_v2_limit)
    return 1.;
  else
    return 1/sqrt(1.- iSPEED_OF_LIGHT_SQ*v2);
}

inline Scalar get_beta2(Scalar energy) const
{
  if (is_nr_forced() || energy < nr_eV_limit)
    return get_speed(energy)*iSPEED_OF_LIGHT;
  else
    {
      Scalar gamma2=energy/get_m0c_sq_e()+1;
      return sqrt(1.-1./(gamma2*gamma2));
    }
}

inline Scalar gamma(Scalar u) const
{
  if (fabs(u) < nr_u_limit) // u -> fabs(u), bug fix from Min, 12/9/12, MAL
    return 1.;
  else
    return sqrt(1. + iSPEED_OF_LIGHT_SQ*u*u);
}
#endif

#ifdef PGARRAY_PART
inline Scalar get_energy_eV(int i) const
{
  return get_energy_eV(get_v(i));
}

inline Scalar get_gamma(int i) const
{
  return get_gamma(get_v(i));
}

inline Scalar get_gamma(Vector3 u) const
{
  Scalar u2 = u*u;
  if (u2 < nr_u2_limit)
    return 1.;
  else
    return sqrt(1. + iSPEED_OF_LIGHT_SQ_NORM*u2);
}

inline Scalar get_beta(Scalar energy) const
{
  if (is_nr_forced() || energy < nr_eV_limit)
    return get_speed(energy)*iSPEED_OF_LIGHT;
  else
    {
      Scalar gamma=energy/get_m0c_sq_e()+1;
      return sqrt(gamma*gamma -1)/dxdt;
    }
}

inline Scalar get_gamma2(Vector3 v) const
{
  Scalar v2 = v*v;
  if (is_nr_forced() || v2 < nr_v2_limit)
    return 1.;
  else
    return 1/sqrt(1.- iSPEED_OF_LIGHT_SQ_NORM*v2);
}

inline Scalar get_beta2(Scalar energy) const
{
  if (is_nr_forced() || energy < nr_eV_limit)
    return get_speed(energy)*iSPEED_OF_LIGHT;
  else
    {
      Scalar gamma2=energy/get_m0c_sq_e()+1;
      return sqrt(1.-1./(gamma2*gamma2))/dxdt;
    }
}

inline Scalar gamma(Scalar u) const
{
  if (fabs(u) < nr_u_limit) // u -> fabs(u), bug fix from Min, 12/9/12, MAL
    return 1.;
  else
    return sqrt(1. + iSPEED_OF_LIGHT_SQ_NORM*u*u);
}

inline Scalar convert_to_nr_v(Scalar u) const
{
  return u/gamma(u);
}
#else
inline double get_nr_u_limit() const { return nr_u_limit; }
#endif

#ifdef PG_PART

virtual Scalar get_energy_eV(int i) const = 0;
virtual Scalar get_energy(Vector3 u) const = 0;
virtual Scalar get_energy2_eV(Vector3 v) const = 0;
virtual Scalar get_energy2_eV(Scalar v) const =0;
// nonrel_vel = rel_vel/get_gamma
virtual Scalar get_gamma(int i) const = 0;
virtual Scalar get_gamma(Vector3 u) const = 0;
virtual Scalar gamma(Scalar u) const = 0;
virtual Scalar get_beta(Scalar energy) const = 0;
virtual Scalar get_speed(Scalar energy) const = 0;
// rel_vel = nonrel_vel*get_gamma2
virtual Scalar get_gamma2(Vector3 v) const = 0;
virtual Scalar get_beta2(Scalar energy) const = 0;
virtual Scalar get_speed2(Scalar energy) const = 0;
// rel_vel -> nonrel_vel
virtual Scalar convert_to_nr_v(Scalar u) const = 0;
#else
inline Scalar get_energy(Vector3 u) const
{
  return get_energy_eV(u)*PROTON_CHARGE;
}

inline Scalar get_energy_eV(Vector3 u) const
{
  Scalar u2 = u*u;
  if (u2 < nr_u2_limit)
    return get_half_m0_e()*u2;
  else
    return (get_gamma(u2)-1.)*get_m0c_sq_e();
}

inline Scalar get_energy2(Vector3 v) const
{
  return get_energy2_eV(v)*PROTON_CHARGE;
}

inline Scalar get_energy2_eV(Vector3 v) const
{
  Scalar v2 = v*v;
  if (is_nr_forced() || v2 < nr_v2_limit)
    return get_half_m0_e()*v2;
  else
    return (get_gamma2(v2)-1.)*get_m0c_sq_e();
}

inline Scalar get_energy2_eV(Scalar v)  const
{
  Scalar v2 = v*v;
  if (is_nr_forced() || v2 < nr_v2_limit)
    return get_half_m0_e()*v2;
  else
    return (get_gamma2(v2)-1.)*get_m0c_sq_e();
}

inline Scalar get_speed(Scalar energy) const
{
  if (is_nr_forced() || energy < nr_eV_limit)
    {
      return sqrt(energy/get_half_m0_e());
    }
  else
    return get_beta(energy)*SPEED_OF_LIGHT;
}

inline Scalar get_speed2(Scalar energy) const

{
  if (is_nr_forced() || energy < nr_eV_limit)
    {
      return sqrt(energy/get_half_m0_e());
    }
  else
    {
      return get_beta2(energy)*SPEED_OF_LIGHT;
    }
}
#endif
