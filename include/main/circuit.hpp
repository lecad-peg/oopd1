#ifndef CIRCUIT
#define CIRCUIT

#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "main/parse.hpp"
#include "main/param.hpp"

// forward class declarations
class Fields;
class Drive;
class DiagnosticControl;
class Boundary;

class Circuit : public Parse
{
 private:
  Boundary *thebound;
  Fields *thefield;
  oopicList<Drive> drivelist;
  Scalar R, L, C, Q, Q0;
  Scalar sourcestrength;  // source strength for current iteration
  int sourcetype, idealvsource;
  void (Circuit::*circuitptr)(void);
  Scalar epsilon, length, area;
 // qconv is never set; not needed, as get_rhs_coef() and update_RLC() use calculate_Q() directly, MAL 5/11/10
	Scalar qconv, alpha_zero, dt, A;
  Scalar a0,a1,a2,a3,a4,sigma, k;
  Scalar circ_q, circ_q1, circ_q2, circ_q3;
  Scalar source_q, source_q1, source_q2, source_q3; // placeholder for 2nd (source) current loop, for dumpfile, MAL 5/25/10
	Scalar circ_I; // For current diagnostic, MAL 12/4/09
 public:
  Circuit(Fields * _thefield);
  Circuit(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
	  oopicList<Parameter<int> > int_variables, Fields *_thefield);

  ~Circuit();

  // Parse functions
  void setdefaults(void);
  void setparamgroup(void);
  void check(void);
  void init(void);
  Parse * special(ostring s);

  void check_drive(void);
  bool voltagesource(void);
  int idealvoltagesource(void);

  // accessor functions
  inline Scalar get_Q(void) { return Q; }
  inline void add_Q(Scalar a) { Q += a; }
  inline void set_Q(Scalar a) { Q = a; } // for current source dumpfile restore, MAL 5/24/10
  inline Scalar get_sourcestrength(void) { return sourcestrength; }
	inline void set_sourcestrength(Scalar s) { sourcestrength = s; } // for dumpfile, MAL 5/23/10
  inline int get_sourcetype(void)        { return sourcetype; }
  inline Scalar get_alpha_zero(void) { return a0; }
	inline Scalar get_circ_I(void) { return circ_I; } // For current diagnostic, MAL 12/4/09
	inline Scalar get_circ_q(void) { return circ_q; } // for dumpfile, MAL 5/9/10
	inline Scalar get_circ_q1(void) { return circ_q1; } // for dumpfile, MAL 5/9/10
	inline Scalar get_circ_q2(void) { return circ_q2; } // for dumpfile, MAL 5/9/10
	inline Scalar get_circ_q3(void) { return circ_q3; } // for dumpfile, MAL 5/9/10
	inline Scalar get_source_q(void) { return source_q; } // placeholde for 2nd current loop for dumpfile, MAL 5/25/10
	inline Scalar get_source_q1(void) { return source_q1; } // for dumpfile, MAL 5/25/10
	inline Scalar get_source_q2(void) { return source_q2; } // for dumpfile, MAL 5/25/10
	inline Scalar get_source_q3(void) { return source_q3; } // for dumpfile, MAL 5/25/10
	inline Scalar get_sigma(void) { return sigma; } // for dumpfile, MAL 5/11/10
	inline void set_circ_q(Scalar q) {circ_q = q; } // for dumpfile, MAL 5/9/10
	inline void set_circ_q1(Scalar q1) { circ_q1 = q1; } // for dumpfile, MAL 5/9/10
	inline void set_circ_q2(Scalar q2) { circ_q2 = q2; } // for dumpfile, MAL 5/9/10
	inline void set_circ_q3(Scalar q3) { circ_q3 = q3; } // for dumpfile, MAL 5/9/10
	inline void set_source_q(Scalar q) {source_q = q; } // for dumpfile, MAL 5/25/10
	inline void set_source_q1(Scalar q1) { source_q1 = q1; } // for dumpfile, MAL 5/25/10
	inline void set_source_q2(Scalar q2) { source_q2 = q2; } // for dumpfile, MAL 5/25/10
	inline void set_source_q3(Scalar q3) { source_q3 = q3; } // for dumpfile, MAL 5/25/10
	inline void set_sigma(Scalar s) { sigma = s; } // for dumpfile, MAL 5/11/10
	inline oopicList<Drive> * get_drivelist() { return &drivelist; }

  // update the circuit
  void update(bool update_circuitptr=true);

  // circuit functions
  void currentsource(void);
  void shortcircuit(void);
  void RLC_circuit(void);
  Scalar get_rhs_coeff(Scalar qconv, Scalar A);
  void update_RLC(Scalar phi, Scalar qconv, Scalar A);
};

#endif
