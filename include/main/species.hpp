#ifndef SPECIES_H
#define SPECIES_H

//********************************************************
// species.hpp
//
// Revision history
//
// (JohnV 14Jul2003) added accessor functions for ParticleGroupXXX
//
//********************************************************

#include "reactions/reactiontype.hpp"
#include "main/parse.hpp"
#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#define DEFAULT_NP2C 1e9
// for null collision method in MCC::init_maxSigmaV(), MAL 9/22/12
#define MAX_ENERGY_FOR_SIGMA_V 1000.
// forward declarations
class ParticleGroup;
class Fields;
class Grid;
class Fluid;

class Species : public Parse
{
	ostring name; // name of species
	ostring reaction_name;
	ReactionSpeciesType reaction_type;
	Scalar q, m, qm;
	int atomicnumber;
	// species_density, species_scratch_density, species_ave_density, and species_temp_density are initially allocated in species.cpp
	// array size is 4*ng, MAL 9/30/09
	Scalar *species_density;
	Scalar *species_scratch_density; // scratch_density pointer used in particlegrouparray::accumulate_density to avoid a multiply in loop over particles, MAL 9/12/09
	Scalar *species_ave_density; // cumulative time-ave density array for this species, MAL 4/25/09
	Scalar *species_temp_density; // temporary time-ave density array for this species, MAL 6/12/09
	// if velocity moments are calculated in particlegrouparray.cpp, than the above four arrays become pointers
	// into a single contiguous array, sptr, that is allocated in diagnosticcontrol.cpp, MAL 9/30/09
	// all remaining diagnostic arrays below are pointers into sptr, on an as-needed basis, in diagnosticcontrol.cpp, MAL 9/30/09
	Scalar *species_density_array; // Spatiotemporal diagnostics, HH 04/18/16
	Scalar *species_Temp_array; // Spatiotemporal diagnostics, HH 04/18/16
	Scalar *species_JdotE_array; // Spatiotemporal diagnostics, HH 04/18/16
	Scalar *species_flux1; // flux1=n*v1 pointer for this species, MAL 9/11/09
	Scalar *species_scratch_flux1; // scratch_flux1 pointer, to avoid multiplies in loop over particles, MAL 9/12/09
	Scalar *species_ave_flux1; // cumulative time-average flux1 for this species, MAL 4/25/09
	Scalar *species_temp_flux1; // temporary time-average flux1 for this species, MAL 6/12/09
	Scalar *species_J1dotE1; // J1 dot E1 array for this species, MAL 4/24/09
	Scalar *species_ave_J1dotE1; // cumulative time-average J1 dot E1 array for this species, MAL 4/25/09
	Scalar *species_temp_J1dotE1; // temporary time-average J1 dot E1 array for this species, MAL 6/12/09
	Scalar *species_flux2;
	Scalar *species_scratch_flux2;
	Scalar *species_ave_flux2;
	Scalar *species_temp_flux2;
	Scalar *species_flux3;
	Scalar *species_scratch_flux3;
	Scalar *species_ave_flux3;
	Scalar *species_temp_flux3;
	Scalar *species_J2dotE2;
	Scalar *species_ave_J2dotE2;
	Scalar *species_temp_J2dotE2;
	Scalar *species_J3dotE3;
	Scalar *species_ave_J3dotE3;
	Scalar *species_temp_J3dotE3;
	Scalar *species_enden1;
	Scalar *species_scratch_enden1;
	Scalar *species_ave_enden1;
	Scalar *species_temp_enden1;
	Scalar *species_enden2;
	Scalar *species_scratch_enden2;
	Scalar *species_ave_enden2;
	Scalar *species_temp_enden2;
	Scalar *species_enden3;
	Scalar *species_scratch_enden3;
	Scalar *species_ave_enden3;
	Scalar *species_temp_enden3;
	Scalar *species_vel1;
	Scalar *species_ave_vel1;
	Scalar *species_temp_vel1;
	Scalar *species_vel2;
	Scalar *species_ave_vel2;
	Scalar *species_temp_vel2;
	Scalar *species_vel3;
	Scalar *species_ave_vel3;
	Scalar *species_temp_vel3;
	Scalar *species_JdotE;
	Scalar *species_ave_JdotE;
	Scalar *species_temp_JdotE;
	Scalar *species_enden;
	Scalar *species_ave_enden;
	Scalar *species_temp_enden;
	Scalar *species_T1;
	Scalar *species_ave_T1;
	Scalar *species_temp_T1;
	Scalar *species_T2;
	Scalar *species_ave_T2;
	Scalar *species_temp_T2;
	Scalar *species_T3;
	Scalar *species_ave_T3;
	Scalar *species_temp_T3;
	Scalar *species_Tperp;
	Scalar *species_ave_Tperp;
	Scalar *species_temp_Tperp;
	Scalar *species_Temp;
	Scalar *species_ave_Temp;
	Scalar *species_temp_Temp;

	Scalar species_ave_J1dotE1dV; // integral of J1dotE1 dV for the species, MAL 12/12/09
	Scalar species_ave_J2dotE2dV;
	Scalar species_ave_J3dotE3dV;
	Scalar species_ave_JdotEdV;
	Scalar ave_Iconv; // MAL 12/12/09
	Scalar Ntot_particles; // total number of physical particles
	Scalar T1tot, T2tot, T3tot, Tperptot, Temptot; // time-varying average temperatures over all physical particles, MAL 8/4/10
	Scalar np2c; // default particle weight for this species
	bool useSecDefaultWeight; // local to species, create species with default weight, MAL 10/15/09
	Scalar energyThrToAddAsPIC; // add (neutral) PIC particles due to ion-neutral collisions above this energy (eV), MAL 11/21/09
	Scalar maxEnergyForSigmaV; // maximum energy used when calculating max(sigmaV) for null collision method in MCC::init_maxSigmaV(), MAL 9/22/12
	int subcycle;
	int nmax;  // maximum # when creating ParticleGroups
	int nmult; // amount to increase number of particles by
	int ident; // unique identification # > top-level
	bool v3_flag;
	Grid *theGrid;
	Fields *theField;

	// linked list of ParticleGroups
	ParticleGroupList particleGroupList;
	bool variable_weight; // default behavior when creating ParticleGroups
	int pg_type; // type of particle groups to create (default)
	bool nonlinearflag; // does the species require a nonlinear field solve?

	// linked list of Fluids
	oopicList<Fluid> fluidList;
	oopicList<Fluid> nonlinearfluids;

	// variables for moments of distributions
	// -- are these used?  What is the development direction for these guys?
	// -- JH, March 2006
	int nmoment;
	Scalar **moment;
	bool accumulate_moment;

public:
	Species();
	Species(Parse * _parent, int i);
	Species(Scalar qq, Scalar mm, Scalar n_np2c, ostring nname);
	Species(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
			oopicList<Parameter<int> > int_variables, int i);
	Species(Species *s);
	Species(Species &s);
	~Species();

	void copy(Species *s);

	////////////////////////////////
	// virtual functions from Parse
	////////////////////////////////

	void setdefaults();
	void setparamgroup();
	void check();
	void init();
	Parse * special(ostring s);

	void set_theGrid(Grid *_theGrid);
	void set_theField(Fields *t);

	///////////////////////////
	// functions for particles
	///////////////////////////

	////->  functions for ParticleGroups:

	void set_ParticleGroupType(int type) { pg_type = type; }
	void add_ParticleGroup(ParticleGroup *pg);
	ParticleGroup * add_ParticleGroup(Scalar w0, int nm=-1);
	ParticleGroup * add_ParticleGroup(Scalar w0, bool vw, int nm=-1);
	ParticleGroup * get_ParticleGroup(Scalar w, bool vw, int n = 1);
	inline ParticleGroupList *get_ParticleGroupList()
	{    return &particleGroupList;    }
	void advance_vload(Scalar dt);

	// add a single particle
	void add_particle(Scalar x, Scalar v1, Scalar v2=0., Scalar v3=0.,
			bool norm=true, bool update_field=false, Scalar w=-1.,
			bool vw=false);
	void add_particle(Scalar x, Vector3 v,
			bool norm=true, bool update_field=false, Scalar w=-1.,
			bool vw=false);

	// add multiple particles
	ParticleGroup * add_particles(int n, Scalar w0, Scalar *X,
			Vector3 *V=NULL, bool norm=true,
			bool update_field=false);

	// add multiple particles
	ParticleGroup * add_particles(int n, Scalar w0, Scalar *X,
			Scalar *dX, Vector3 *V=NULL, bool norm=true,
			bool update_field=false);

	// mindgame: add particles of _pg into particlegrouplist in species class
	void add_particles(ParticleGroup *_pg, bool vnorm, bool update_field);

	// print the particles to a file
	void print_particles(FILE *fp=stdout);

  // dump the particles to a file MAL 5/4/10
  void dump_particles(FILE *fp);

  // dump the fluids to a file MAL200228
  void dump_fluids(FILE *fp);

  // dump the fluids to a file MAL200229
  void reload_fluids(FILE *fp);

	// accumulate species density
	void accumulate_density();

	// accumulate velocity moments, MAL 10/2/09, 7/8/10
	void accumulate_vel_moments();

	// Return the nature of species (particle or fluid)
	bool includes_particles() const { return particleGroupList.nItems(); }
	bool includes_fluids() const { return fluidList.nItems(); }

	// compute number of particles of this species
	int number_particles() ;
	Scalar number_fluidparticles() ;

	// for time-variation of the time-average integrals of J1dotE1 dV, etc, MAL 12/11/09
	Scalar get_species_ave_J1dotE1dV();
	Scalar get_species_ave_J2dotE2dV();
	Scalar get_species_ave_J3dotE3dV();
	Scalar get_species_ave_JdotEdV();
	Scalar get_ave_Iconv(); // MAL 12/12/09

	void turn_on_dual_index();
	void turn_off_dual_index();

	// front-end to particle group functions > ParticleSpecies
	void advance_v(Scalar dt);
	void advance_x(Scalar dt);
	void Gather_E(void);

	////////////////////////
	// functions for fluids
	////////////////////////

	Scalar electrostatic_density(Scalar potential);
	Scalar electrostatic_derivative(Scalar potential);
	void integrate_fluids(Scalar dt);  // integrate fluids forward in time
	void update_N_tot();
	void update_fluids_from_fields(Scalar * _phi);

	Scalar vt(Scalar T);
	Vector3 vthermal(Scalar T, bool volts=true);

	//////////////////////
	// accessor functions
	//////////////////////

	inline Scalar get_maxEnergyForSigmaV() const { return maxEnergyForSigmaV; } //for null collision method in MCC::init_maxSigmaV(), MAL 9/22/12
	inline int    get_subcycle() const { return subcycle; }
	inline Scalar get_q() const {return q;} // default charge per particle
	inline Scalar get_m() const {return m;} // default mass per particle
	inline Scalar get_qm() const { return qm; }
	inline int get_atomicnumber() const { return atomicnumber; }
	inline bool is_variableWeight() const {return variable_weight;}
	inline Scalar get_np2c()  const { return np2c; }
	inline bool use_SecDefaultWeight() const { return useSecDefaultWeight; } // MAL 10/15/09
	inline Scalar get_energyThrToAddAsPIC() const { return energyThrToAddAsPIC; } // MAL 11/21/09
	inline Scalar *get_species_density() const { return species_density; }
	// added all get and set accessor functions below, MAL 9/30/09
	inline Scalar *get_species_scratch_density() const { return species_scratch_density; }
	inline Scalar *get_species_ave_density()  const { return species_ave_density; }
	inline Scalar *get_species_temp_density()  const { return species_temp_density; }
	inline Scalar *get_species_flux1()  const { return species_flux1; }
	inline Scalar *get_species_scratch_flux1() const { return species_scratch_flux1; }
	inline Scalar *get_species_ave_flux1()  const { return species_ave_flux1; }
	inline Scalar *get_species_temp_flux1()  const { return species_temp_flux1; }
	inline Scalar *get_species_J1dotE1()  const { return species_J1dotE1; }
	inline Scalar *get_species_ave_J1dotE1()  const { return species_ave_J1dotE1; }
	inline Scalar *get_species_temp_J1dotE1()  const { return species_temp_J1dotE1; }
	inline Scalar *get_species_flux2()  const { return species_flux2; }
	inline Scalar *get_species_scratch_flux2() const { return species_scratch_flux2; }
	inline Scalar *get_species_ave_flux2()  const { return species_ave_flux2; }
	inline Scalar *get_species_temp_flux2()  const { return species_temp_flux2; }
	inline Scalar *get_species_flux3()  const { return species_flux3; }
	inline Scalar *get_species_scratch_flux3() const { return species_scratch_flux3; }
	inline Scalar *get_species_ave_flux3()  const { return species_ave_flux3; }
	inline Scalar *get_species_temp_flux3()  const { return species_temp_flux3; }
	inline Scalar *get_species_J2dotE2()  const { return species_J2dotE2; }
	inline Scalar *get_species_ave_J2dotE2()  const { return species_ave_J2dotE2; }
	inline Scalar *get_species_temp_J2dotE2()  const { return species_temp_J2dotE2; }
	inline Scalar *get_species_J3dotE3()  const { return species_J3dotE3; }
	inline Scalar *get_species_ave_J3dotE3()  const { return species_ave_J3dotE3; }
	inline Scalar *get_species_temp_J3dotE3()  const { return species_temp_J3dotE3; }
	inline Scalar *get_species_enden1()  const { return species_enden1; }
	inline Scalar *get_species_scratch_enden1() const { return species_scratch_enden1; }
	inline Scalar *get_species_ave_enden1()  const { return species_ave_enden1; }
	inline Scalar *get_species_temp_enden1()  const { return species_temp_enden1; }
	inline Scalar *get_species_enden2()  const { return species_enden2; }
	inline Scalar *get_species_scratch_enden2() const { return species_scratch_enden2; }
	inline Scalar *get_species_ave_enden2()  const { return species_ave_enden2; }
	inline Scalar *get_species_temp_enden2()  const { return species_temp_enden2; }
	inline Scalar *get_species_enden3()  const { return species_enden3; }
	inline Scalar *get_species_scratch_enden3() const { return species_scratch_enden3; }
	inline Scalar *get_species_ave_enden3()  const { return species_ave_enden3; }
	inline Scalar *get_species_temp_enden3()  const { return species_temp_enden3; }
	inline Scalar *get_species_vel1()  const { return species_vel1; }
	inline Scalar *get_species_ave_vel1()  const { return species_ave_vel1; }
	inline Scalar *get_species_temp_vel1()  const { return species_temp_vel1; }
	inline Scalar *get_species_vel2()  const { return species_vel2; }
	inline Scalar *get_species_ave_vel2()  const { return species_ave_vel2; }
	inline Scalar *get_species_temp_vel2()  const { return species_temp_vel2; }
	inline Scalar *get_species_vel3()  const { return species_vel3; }
	inline Scalar *get_species_ave_vel3()  const { return species_ave_vel3; }
	inline Scalar *get_species_temp_vel3()  const { return species_temp_vel3; }
	inline Scalar *get_species_JdotE()  const { return species_JdotE; }
	inline Scalar *get_species_ave_JdotE()  const { return species_ave_JdotE; }
	inline Scalar *get_species_temp_JdotE()  const { return species_temp_JdotE; }
	inline Scalar *get_species_enden()  const { return species_enden; }
	inline Scalar *get_species_ave_enden()  const { return species_ave_enden; }
	inline Scalar *get_species_temp_enden()  const { return species_temp_enden; }
	inline Scalar *get_species_T1()  const { return species_T1; }
	inline Scalar *get_species_ave_T1()  const { return species_ave_T1; }
	inline Scalar *get_species_temp_T1()  const { return species_temp_T1; }
	inline Scalar *get_species_T2()  const { return species_T2; }
	inline Scalar *get_species_ave_T2()  const { return species_ave_T2; }
	inline Scalar *get_species_temp_T2()  const { return species_temp_T2; }
	inline Scalar *get_species_T3()  const { return species_T3; }
	inline Scalar *get_species_ave_T3()  const { return species_ave_T3; }
	inline Scalar *get_species_temp_T3()  const { return species_temp_T3; }
	inline Scalar *get_species_Tperp()  const { return species_Tperp; }
	inline Scalar *get_species_ave_Tperp()  const { return species_ave_Tperp; }
	inline Scalar *get_species_temp_Tperp()  const { return species_temp_Tperp; }
	inline Scalar *get_species_Temp()  const { return species_Temp; }
	inline Scalar *get_species_ave_Temp()  const { return species_ave_Temp; }
	inline Scalar *get_species_temp_Temp()  const { return species_temp_Temp; }
	// set from diagnosticcontrol.cpp if diagnostic arrays need to be allocated, added MAL 9/25/09
	inline void set_species_density(Scalar * ptr) { species_density = ptr; }
	inline void set_species_flux1(Scalar * ptr) { species_flux1 = ptr; }
	inline void set_species_flux2(Scalar * ptr) { species_flux2 = ptr; }
	inline void set_species_flux3(Scalar * ptr) { species_flux3 = ptr; }
	inline void set_species_enden1(Scalar * ptr) { species_enden1 = ptr; }
	inline void set_species_enden2(Scalar * ptr) { species_enden2 = ptr; }
	inline void set_species_enden3(Scalar * ptr) { species_enden3 = ptr; }
	inline void set_species_scratch_density(Scalar * ptr) { species_scratch_density = ptr; }
	inline void set_species_scratch_flux1(Scalar * ptr) { species_scratch_flux1 = ptr; }
	inline void set_species_scratch_flux2(Scalar * ptr) { species_scratch_flux2 = ptr; }
	inline void set_species_scratch_flux3(Scalar * ptr) { species_scratch_flux3 = ptr; }
	inline void set_species_scratch_enden1(Scalar * ptr) { species_scratch_enden1 = ptr; }
	inline void set_species_scratch_enden2(Scalar * ptr) { species_scratch_enden2 = ptr; }
	inline void set_species_scratch_enden3(Scalar * ptr) { species_scratch_enden3 = ptr; }
	inline void set_species_ave_density(Scalar * ptr) { species_ave_density = ptr; }
	inline void set_species_temp_density(Scalar * ptr) { species_temp_density = ptr; }
	inline void set_species_ave_flux1(Scalar * ptr) { species_ave_flux1 = ptr; }
	inline void set_species_temp_flux1(Scalar * ptr) { species_temp_flux1 = ptr; }
	inline void set_species_J1dotE1(Scalar * ptr) { species_J1dotE1 = ptr; }
	inline void set_species_ave_J1dotE1(Scalar * ptr) { species_ave_J1dotE1 = ptr; }
	inline void set_species_temp_J1dotE1(Scalar * ptr) { species_temp_J1dotE1 = ptr; }
	inline void set_species_ave_flux2(Scalar * ptr) { species_ave_flux2 = ptr; }
	inline void set_species_temp_flux2(Scalar * ptr) { species_temp_flux2 = ptr; }
	inline void set_species_ave_flux3(Scalar * ptr) { species_ave_flux3 = ptr; }
	inline void set_species_temp_flux3(Scalar * ptr) { species_temp_flux3 = ptr; }
	inline void set_species_J2dotE2(Scalar * ptr) { species_J2dotE2 = ptr; }
	inline void set_species_ave_J2dotE2(Scalar * ptr) { species_ave_J2dotE2 = ptr; }
	inline void set_species_temp_J2dotE2(Scalar * ptr) { species_temp_J2dotE2 = ptr; }
	inline void set_species_J3dotE3(Scalar * ptr) { species_J3dotE3 = ptr; }
	inline void set_species_ave_J3dotE3(Scalar * ptr) { species_ave_J3dotE3 = ptr; }
	inline void set_species_temp_J3dotE3(Scalar * ptr) { species_temp_J3dotE3 = ptr; }
	inline void set_species_ave_enden1(Scalar * ptr) { species_ave_enden1 = ptr; }
	inline void set_species_temp_enden1(Scalar * ptr) { species_temp_enden1 = ptr; }
	inline void set_species_ave_enden2(Scalar * ptr) { species_ave_enden2 = ptr; }
	inline void set_species_temp_enden2(Scalar * ptr) { species_temp_enden2 = ptr; }
	inline void set_species_ave_enden3(Scalar * ptr) { species_ave_enden3 = ptr; }
	inline void set_species_temp_enden3(Scalar * ptr) { species_temp_enden3 = ptr; }
	inline void set_species_vel1(Scalar * ptr) { species_vel1 = ptr; }
	inline void set_species_ave_vel1(Scalar * ptr) { species_ave_vel1 = ptr; }
	inline void set_species_temp_vel1(Scalar * ptr) { species_temp_vel1 = ptr; }
	inline void set_species_vel2(Scalar * ptr) { species_vel2 = ptr; }
	inline void set_species_ave_vel2(Scalar * ptr) { species_ave_vel2 = ptr; }
	inline void set_species_temp_vel2(Scalar * ptr) { species_temp_vel2 = ptr; }
	inline void set_species_vel3(Scalar * ptr) { species_vel3 = ptr; }
	inline void set_species_ave_vel3(Scalar * ptr) { species_ave_vel3 = ptr; }
	inline void set_species_temp_vel3(Scalar * ptr) { species_temp_vel3 = ptr; }
	inline void set_species_JdotE(Scalar * ptr) { species_JdotE = ptr; }
	inline void set_species_ave_JdotE(Scalar * ptr) { species_ave_JdotE = ptr; }
	inline void set_species_temp_JdotE(Scalar * ptr) { species_temp_JdotE = ptr; }
	inline void set_species_enden(Scalar * ptr) { species_enden = ptr; }
	inline void set_species_ave_enden(Scalar * ptr) { species_ave_enden = ptr; }
	inline void set_species_temp_enden(Scalar * ptr) { species_temp_enden = ptr; }
	inline void set_species_T1(Scalar * ptr) { species_T1 = ptr; }
	inline void set_species_ave_T1(Scalar * ptr) { species_ave_T1 = ptr; }
	inline void set_species_temp_T1(Scalar * ptr) { species_temp_T1 = ptr; }
	inline void set_species_T2(Scalar * ptr) { species_T2 = ptr; }
	inline void set_species_ave_T2(Scalar * ptr) { species_ave_T2 = ptr; }
	inline void set_species_temp_T2(Scalar * ptr) { species_temp_T2 = ptr; }
	inline void set_species_T3(Scalar * ptr) { species_T3 = ptr; }
	inline void set_species_ave_T3(Scalar * ptr) { species_ave_T3 = ptr; }
	inline void set_species_temp_T3(Scalar * ptr) { species_temp_T3 = ptr; }
	inline void set_species_Tperp(Scalar * ptr) { species_Tperp = ptr; }
	inline void set_species_ave_Tperp(Scalar * ptr) { species_ave_Tperp = ptr; }
	inline void set_species_temp_Tperp(Scalar * ptr) { species_temp_Tperp = ptr; }
	inline void set_species_Temp(Scalar * ptr) { species_Temp = ptr; }
	inline void set_species_ave_Temp(Scalar * ptr) { species_ave_Temp = ptr; }
	inline void set_species_temp_Temp(Scalar * ptr) { species_temp_Temp = ptr; }


	inline Scalar get_Ntot_particles() const { return Ntot_particles;}
	inline Scalar gett_Ntot_particles() { return Ntot_particles;} // for timediagnostic, MAL 8/4/10
	inline Scalar gett_T1tot() { return T1tot;} // for timediagnostic, MAL 8/4/10
	inline Scalar gett_T2tot() { return T2tot;} // for timediagnostic, MAL 8/4/10
	inline Scalar gett_T3tot() { return T3tot;} // for timediagnostic, MAL 8/4/10
	inline Scalar gett_Tperptot() { return Tperptot;} // for timediagnostic, MAL 8/4/10
	inline Scalar gett_Temptot() { return Temptot;} // for timediagnostic, MAL 8/4/10
	inline ostring get_name() const { return name; }
	inline ostring get_reaction_name() const { return reaction_name; }
	inline ReactionSpeciesType get_reactiontype() const { return reaction_type; }
	inline ostring get_reactionname() const { return ReactionSpeciesTypeName(reaction_type); }
	inline void set_atomicnumber(int _atomicnumber) { atomicnumber = _atomicnumber; }
	inline int get_ident() const { return ident; }
	inline int get_particlegrouptype() const { return pg_type; }
	inline oopicList<Fluid> * get_fluidList() { return &fluidList;}
	inline bool is_3v() const { return v3_flag; }
	inline void set_3v_flag() { v3_flag=true; }
	bool is_nonlinear();
	bool is(ostring _name) const;
	bool is(ReactionSpeciesType _reaction_type) const {return (_reaction_type == reaction_type);}

	Scalar get_dt() const;
	Scalar get_dxdt() const;  // should this be in species? JH, March 2006
	inline  Grid * get_grid() const  { return theGrid;  }
	inline Fields * get_field() const { return theField; }

	////////////////////////////
	// functions for statistics
	////////////////////////////

	Vector3 average_velocity(); // get average velocity
	Scalar max_position(); //get maximum position of particles
	Vector3 max_velocity(); //get maximum velocity of particles
	Vector3 front_velocity(); //get front velocity of particles

	// calculate moments of the distribution function
	void calculate_moments();

	// mindgame: return species named with "name"
	static Species * get_species_named_with(oopicListIter<Species> species_iter, ostring name);
	static Species * get_species_named_with(oopicListIter<Species> species_iter, ReactionSpeciesType _name);
	static vector<Species *> get_reaction_species_named_with(oopicListIter<Species> species_iter, ReactionSpeciesType name);
	static vector<Species *> get_reaction_species_named_with(oopicListIter<Species> species_iter, ReactionSpeciesType name1, ReactionSpeciesType name2);

	static ostring get_species_name_with_ident(oopicListIter<Species> species_iter, int _ident); // for dumpfile, MAL 11/17/10

	void convert_ostrings_to_speciestypes();

	////////////////////////////////////////////////////////
	// functions for spatiotemporal diagnostics, HH 04/18/16
	////////////////////////////////////////////////////////
	void init_arrays(int size_array);
	void set_arrays(int ng, int steps_rf, int m);
	inline Scalar *get_n_array() {return species_density_array;}
	inline Scalar *get_Temp_array() {return species_Temp_array;}
	inline Scalar *get_JdotE_array() {return species_JdotE_array;}

#define SPECIES_PART
#include "main/conv_engy_vel.hpp"
#undef SPECIES_PART
};

#endif
