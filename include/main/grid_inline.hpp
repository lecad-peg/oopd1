inline Scalar Grid::get_dxcell(int i)
{
  if(uniform) { return dx; }
  return dxcell[i];
}

inline Scalar Grid::get_dxnode(int i)
{
  if(uniform) { return dx; }
  return dxnode[i];
}

inline Scalar Grid::getA(Scalar R)
{
  return area_coeff*ppow(R, type);
}

inline Scalar Grid::get_area(int i) { return getA(X[i]); }

inline Scalar Grid::getRfromV(Scalar V)
{
  // nonsensical if called by PLANAR grids
  if(type == PLANAR)
    {
      error("Grid::getRfromV", "should not be called by PLANAR grids!");
    }
  if(V < 0.) { error("Grid::getRfromV(Scalar V)", "V < 0."); }
  
  return pow(V/vol_coeff, Scalar(1./Scalar(type+1)));
}

inline Scalar Grid::getV(Scalar R)
{
  if(R < 0.) { error("Grid::getV(Scalar R)", "R < 0."); }
  return vol_coeff*ppow(R, type+1);
}

// gets volume between points X0 and X1
inline Scalar Grid::getV(Scalar X0, Scalar X1)
{
  if(X1 < X0) 
    {
      char buffer[STR_SIZE];
      sprintf(buffer, "X1 < X0 (%g < %g)", X1, X0);
      error("Grid::getV(Scalar,Scalar)", buffer); 
    }
  
  if(type == PLANAR)
    {
      return area*(X1-X0);
    }
  else
    {
      return (getV(X1) - getV(X0));
    }
}

// returns 2nd order gradient on uniform meshes
// must be called by an interior point or segfault
inline Scalar Grid::gradient_interior(Scalar *field, int location)
{
  return (0.5/dx)*(field[location+1] - field[location-1]);
}

// returns 2nd order gradient on uniform meshes
inline Scalar Grid::gradient(Scalar *field, int location)
{
  if(location == 0)
    {
      if(periodic)
	{
	  return (0.5/dx)*(field[1] - field[ng-2]);
	}
      else
	{ 
	  return (0.5/dx)*(4.*field[1] - field[2] - 3.*field[0]);
	}
    }
  else if(location == ng-1)
    {
      if(periodic)
	{
	  return (0.5/dx)*(field[1] - field[ng-2]);
	}
      else
	{
	  return (0.5/dx)*(3.*field[location] - 
			   4.*field[location-1] + field[location-2]);
	}
    }
  else { return gradient_interior(field, location); }
}

inline Vector3 Grid::gradient_interior(Vector3 *field, int location)
{
  return (0.5/dx)*(field[location+1] - field[location-1]);
}

// mindgame: gradient in the positive direction
inline Vector3 Grid::gradient_positive(Vector3 *field, int location)
{
  if (location <= ng-3)
  {
    return (0.5/dx)*(4.*field[location+1] - field[location+2] - 3.*field[location]);
  }
  else 
  {
    if (periodic)
    {
      if (location == ng-2)
        return (0.5/dx)*(4.*field[0] - field[1] - 3.*field[ng-2]);
      else
        return (0.5/dx)*(4.*field[1] - field[2] - 3.*field[0]);
    }
    else return 0;
  }
}

// mindgame: gradient in the negative direction
inline Vector3 Grid::gradient_negative(Vector3 *field, int location)
{
  if (location >= 2)
  {
    return (0.5/dx)*(-4.*field[location-1] + field[location-2]
		     +3.*field[location]);
  }
  else
  {
    if (periodic)
    {
      if (location == 1)
	return (0.5/dx)*(-4.*field[location-1] + field[ng-2]
			 +3.*field[location]);
      else
	return (0.5/dx)*(-4.*field[ng-2] + field[ng-3] + 3.*field[location]);
    }
    else return 0;
  }
}


// returns 2nd order gradient on uniform meshes
inline Vector3 Grid::gradient(Vector3 *field, int location)
{
  if(location == 0)
    {
      if(periodic)
        {
          return (0.5/dx)*(field[1] - field[ng-2]);
        }
      else
        {
          return (0.5/dx)*(4.*field[1] - field[2] - 3.*field[0]);
        }
    }
  else if(location == ng-1)
    {
      if(periodic)
        {
          return (0.5/dx)*(field[1] - field[ng-2]);
        }
      else
        {
          return (0.5/dx)*(3.*field[location] -
                           4.*field[location-1] + field[location-2]);
        }
    }
  else { return gradient_interior(field, location); }
}

// DivGrad -- gets coefficients for Div(field*Grad(desired_field))
// e.g. Div(epsilon*Grad(Phi)) -- coefficients for Phi
// Doesn't work with mesh endpoints
// field_left = field in left-hand cell (at midpoint)
// field_right = field at right-hand cell (at midpoint)
inline void Grid::DivGrad(int i, Scalar & left, Scalar & center, 
			  Scalar & right,
			  Scalar field_left, Scalar field_right)
{
  Scalar r = ipow(X[i], type);
  Scalar r_right = ipow(X[i]+0.5*get_dxcell(i), type);
  Scalar r_left = ipow(X[i]-0.5*get_dxcell(i-1), type);
  left = right = 1./(r*get_dxnode(i));
  right *= r_right*field_right/get_dxcell(i);
  left *= r_left*field_left/get_dxcell(i);
  center = -(left + right);
}

// gets the coefficients for the Laplacian on a uniform mesh
inline void Grid::Laplacian(int i, Scalar & left, Scalar & center, 
			    Scalar & right)
{
  left = center = right = inv_dxdx;
  center *= -2.;
  Scalar mult = 0.5;
  switch(type)
    {
    case SPHERICAL:
      mult *= 2.;
    case CYLINDRICAL:
      mult /= (X[i]*dx); // breaks for x[i]==r==0
      right += mult;
      left -= mult;
    }
}

inline void Grid::WeightLinear(Scalar *xval, Scalar *fval, int n, 
			       Scalar *gridval)
{
  for(int i=0; i < n; i++)
    {
      WeightLinear(xval[i], fval[i], gridval); 
    }
}

// weights fval to thearray using linear weighting
// assumes thearray has ng members, x in grid units
inline void Grid::WeightLinear(Scalar x, Scalar fval, Scalar *thearray)
{
  int j = int(x);
  Scalar w = x - j;
  thearray[j+1] += w*fval;
  thearray[j] += (1.-w)*fval;
}

// Special case of above function for fval=1; avoid multiply within particle loop, MAL 9/12/09
// weights fval to thearray using linear weighting
// assumes thearray has ng members, x in grid units
inline void Grid::WeightLinear(Scalar x, Scalar *thearray)
{
  int j = int(x);
  Scalar w = x - j;
  thearray[j+1] += w;
  thearray[j] += (1.-w);
}

// Weights 1D particle velocity moments to grid using linear weighting, MAL 9/12/09, revised 7/5/10
// assumes the arrays have ng members, x and v1 in grid units
inline void Grid::WeightLinear1D(Scalar x, Scalar v1, Scalar *v1array, Scalar *v1sqarray)
{
  int j = int(x);
  Scalar w = x - j;
	v1array[j+1] += w*v1;
	v1array[j] += (1-w)*v1;
	v1 *= v1;
	v1sqarray[j+1] += w*v1;
	v1sqarray[j] += (1-w)*v1;
}
// Weights 3D velocity moments of particles to grid using linear weighting, MAL 9/12/09, revised 7/5/10
// assumes the arrays have ng members, x and v in grid units
inline void Grid::WeightLinear3D(Scalar x, Scalar v1, Scalar v2, Scalar v3, Scalar *v1array, Scalar *v2array,
	Scalar *v3array, Scalar *v1sqarray, Scalar *v2sqarray, Scalar *v3sqarray)
{
	int j = int(x);
	Scalar w = x - j;
	Scalar wb = 1-w; // faster to do this, MAL 7/5/10
	v1array[j+1] += w*v1;
	v1array[j] += wb*v1;
	v1 *= v1;
	v1sqarray[j+1] += w*v1;
	v1sqarray[j] += wb*v1;
	v2array[j+1] += w*v2;
	v2array[j] += wb*v2;
	v2 *= v2;
	v2sqarray[j+1] += w*v2;
	v2sqarray[j] += wb*v2;
	v3array[j+1] += w*v3;
	v3array[j] += wb*v3;
	v3 *= v3;
	v3sqarray[j+1] += w*v3;
	v3sqarray[j] += wb*v3;
}

// weights fval to thearray using nearest grid point weighting
// assumes thearray has ng members, x in grid units
inline void Grid::WeightNGP(Scalar x, Scalar fval, Scalar *thearray)
{
  int j = (int)x;
  if(x - j > 0.5)
    {
      thearray[j+1] += fval;
    }
  else
    {
      thearray[j] += fval;
    }
}

// linear interpolation of thearray to point x
// assumes thearray has ng members, x in grid units  
inline Scalar Grid::GatherLinear(Scalar x, Scalar *thearray)
{
  int j=int(x);
  Scalar w = x-j;
  return (w*thearray[j+1] + (1. - w)*thearray[j]);
}

inline Vector3 Grid::GatherLinear(Scalar x, Vector3 *thearray, int dim)
{ 
  int j=int(x);
  Scalar w1 = x-j;
  Scalar w0 = 1.-w1;
  Vector3 v;
  for(int i=1; i <= dim; i++)
    {
      v.set(i, w0*thearray[j].e(i) + w1*thearray[j+1].e(i));
    }
  return v;
}

// nearest grid point gather for point x
// assumes thearray has ng members, x in grid units
inline Scalar Grid::GatherNGP(Scalar x, Scalar *thearray)
{
  int j=int(x);
  if(x - j > 0.5)
    {
      return thearray[j+1];
    }
  else
    {
      return thearray[j];
    }
}

inline Scalar Grid::gridXtoX(Scalar gridX)
{
  if(uniform)
    {
      return gridXtoX_uniform(gridX);
    }
  else
    {
      fprintf(stderr, "\nGrid::gridXtoX nonuniform -- not yet implemented!");
      return -1.;
    }
}  

inline Scalar Grid::XtogridX(Scalar x)
{
  if(uniform)
    {
      return XtogridX_uniform(x);
    }
  else
    {
      fprintf(stderr, "\nGrid::XtogridX nonuniform -- not yet implemented!");
      return -1.;
    }
}

void Grid::gridVtoV(int n, Scalar *gridv, Scalar dt)
{
  register int i;
  register Scalar scale;

  scale = dx/dt;

  if(uniform)
    {
      for(i=0; i < n; i++)
	{
	  gridv[i] *= scale;
	}
    }
  else
    {
      error("VtogridV(int n, Scalar *gridX)", 
	    "Not yet implement for nonuniform meshes");
      
    }
}

void Grid::VtogridV(int n, Scalar *v, Scalar dt)
{
  register int i;
  register Scalar scale;

  scale = dt/dx;

  if(uniform)
    {
      for(i=0; i < n; i++)
	{
	  v[i] *= scale;
	}
    }
  else
    {
      error("VtogridV(int n, Scalar *gridX)", 
	    "Not yet implement for nonuniform meshes");
      
    }
}
void Grid::VtogridV(int n, Vector3 *v, Scalar dt)
{
  register int i;
  register Scalar scale;

  scale = dt/dx;

  if(uniform)
    {
      for(i=0; i < n; i++)
        {
          v[i] *= scale;
        }
    }
  else
    {
      error("VtogridV(int n, Scalar *gridX)",
            "Not yet implement for nonuniform meshes");

    }
}

inline void Grid::move(Scalar &x, Scalar dx, ParticleGroup *p, int & i)
{
  if(dx < 0.)
    {
#define NEGATIVE_MOVE
      #include "main/move_uniform.hpp"
#undef NEGATIVE_MOVE
    }
  else
    { 
      #include "main/move_uniform.hpp"
    }
}

inline void Grid::move_nonuniform(Scalar &x, Scalar dx, 
				  ParticleGroup *p, int & i)
{
#define NONUNIFORM
  if(dx < 0.)
    {
#define NEGATIVE_MOVE
#include "main/move.hpp"
#undef NEGATIVE_MOVE
    }
  else
    {
#include "main/move.hpp"
    }
#undef NONUNIFORM
}  

template<class T>
Scalar Grid::integrate(Scalar (T::*f)(int), T * instance, int start, int end)
{
  Scalar val = 0.;

  if(end < 0) { end = ng; }

  for(int i=start; i < end; i++)
    {
      val += V[i]*(instance->*f)(i);
    }    

  return val;
}
