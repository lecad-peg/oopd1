#ifndef PARSE_H
#define PARSE_H

#include <stdarg.h>
#include "main/paramgrp.hpp"

#ifdef MPIDEFINED
#include <mpi.h>
#endif

// generic class for parsing hierarchal data from a file
// an abstract factory class
// classes that wish to parse data thusless should inherit from this class

class Parse
{
protected:
	ParamGroup pg;
	FILE *f;

	ostring classname;  // name of the local class (programmer must define)
	ostring progdesc;   // description of the program

	oopicList<char> ws;
	oopicList<ostring> comments;

	// data for hierarchy
	Parse *parent;
	oopicList<Parse> children;
	vector<vector<ostring> > child_names;
	vector<ostring> group_names;

	ostring current_child;

	static bool htmlmode;
	static bool finish_construction; // true if not in documenting mode

	void error(const char *line, const char *message, bool fatal=true) const
	{
		if(finish_construction)
		{
			printf("\nError %s", classname());
			if(globallinenumber >= 0)
			{
				printf(", line %d", globallinenumber);
			}
			printf(": ");

			if(strlen(line)) { printf("\"%s\"", line); }
			printf("\n%s\n", message);

			if(fatal) { exit(1); }
		}
	}

	void error(const char *message) const
	{
		error("", message);
	}

public:
	static int globallinenumber;

	Parse()
	{
		classname = "";
		progdesc = "";
		parent = 0;
		current_child = "";
	}

	Parse(Parse * _parent)
	{
		classname = "";
		progdesc = "";
		parent = _parent;
		parent->add_child(this);

		current_child = "";
	}

	virtual ~Parse()
	{
		comments.deleteAll();
		ws.deleteAll();
	}

	bool debug() { return pg.debug; }
	void set_debug(bool val=true) { pg.debug = val; }

	void add_child(Parse *child)
	{
		children.add(child);
	}

	void add_child_names(const char *grpname, int number, ...)
	{
		vector<ostring> tv;
		va_list ap;

		va_start(ap, number);

		while(number--)
		{
			tv.push_back(ostring(va_arg(ap, const char *)));
		}

		child_names.push_back(tv);
		group_names.push_back(ostring(grpname));
		va_end(ap);
	}

	void add_child_name(const char *name)
	{
		add_child_names(name, 1, name);
	}

	void print_child_names(FILE *ffpp) const
	{
		if(htmlmode) { fprintf(ffpp, "\n<ul>"); }
		for(unsigned int i=0; i < child_names.size(); i++)
		{
			if(htmlmode) { fprintf(ffpp, "<li>"); }
			fprintf(ffpp, "// ");

			if(child_names[i].size() > 1)
			{
				fprintf(ffpp, "%s : ", group_names[i].c_str());
			}

			for(unsigned int j=0; j < child_names[i].size(); j++)
			{
				if(htmlmode)
				{
					fprintf(ffpp, "<a href=\"#%s\">", child_names[i][j]());
				}
				fprintf(ffpp, "%s", child_names[i][j].c_str());
				if(htmlmode) { fprintf(ffpp, "</a>"); }

				if(j < child_names[i].size() - 1) { fprintf(ffpp, ", "); }
			}

			fprintf(ffpp, "\n");
		}
		if(htmlmode) { fprintf(ffpp, "\n</ul>\n"); }
	}

	inline ostring get_current_child() const
	{
		return current_child;
	}

	void set_htmlmode()
	{
		htmlmode = true;
	}

	void set_program_description(ostring _desc)
	{
		progdesc = _desc;
	}

	void set_name(ostring & _name)
	{
		classname = _name;
	}

	ostring get_name() { return classname; }

	virtual ostring get_description() { return ostring(""); }

	void set_parent(Parse * _parent) { parent = _parent; }

	FILE * get_file() { return f; }

	// line counter functions
	void setlinecounter(int lcval=0)
	{
		globallinenumber = lcval;
	}
	void incrementlinecounter() { globallinenumber++; }

	// initializes and parses a Parse object
	void init_parse(FILE *fp, oopicList<Parameter<Scalar> > float_variables, oopicList<Parameter<int> > int_variables)
	{
		setdefaults();
		setparamgroup();

		if(finish_construction)
		{
			parse(fp, float_variables, int_variables);
			check();
			init();
		}
	}

	void init_parse()
	{

		if(parent)
		{
			init_parse(parent->get_file());
		}
		else
		{
			error("init_parse()", "error");
		}
	}

	void init_parse(FILE *fp)
	{
		setdefaults();
		setparamgroup();

		if(finish_construction)
		{
			if(parent) { parent->add_variables(&pg); }
			parse(fp);
			check();
			init();
		}
	}

	void print(FILE *fp = stdout)
	{
		if(htmlmode)
		{
			fprintf(fp, "<a name=\"%s\"></a><h3>", classname.c_str());
		}
		fprintf(fp, "%s", classname.c_str());
		if(htmlmode) { fprintf(fp, "</h3>"); }

		ostring ts = get_description();
		if(ts.length())
		{
			fprintf(fp, ": %s", ts());
		}

		fprintf(fp, "\n{");

		if(parent)
		{
			if(htmlmode)
			{
				fprintf(fp, "<p><b>// Parent:</b>\n");
				fprintf(fp, "<a href=\"#%s\">%s</a>\n",
						parent->get_name().c_str(), parent->get_name().c_str());
			}
			else
			{
				fprintf(fp, "\n// Parent: %s\n", parent->get_name().c_str());
			}
		}

		if(htmlmode) { fprintf(fp, "<p><b>"); }
		fprintf(fp, "\n// Parameters: ");
		if(htmlmode) { fprintf(fp, "</b>"); }
		pg.print(fp, htmlmode);
	}

	int parse_line(ostring s, int varparse=0)
	{
		oopicListIter<ostring> thei(comments);
		oopicListIter<char> thej(ws);

		for(thei.restart(); !thei.Done(); thei++)
		{
			s.stripcomment((*thei()).c_str());
		}
		for(thej.restart(); !thej.Done(); thej++)
		{
			s.strip(*thej());
		}
		if(strcmp(s(), "}") == 0)
		{
			return -1;
		}
		if(strcmp(s(), "{") == 0)
		{
			return 1;
		}
		if(strcmp(s(), "") == 0) { return 0; }
		if(strcmp(s(), "Variables") == 0)
		{
			if(varparse)
			{
				error(s(), "Nested sets of variables are non-sensical");
			}
			else
			{
				parse(f, 1);
				return 0;
			}
		}

		if(varparse)
		{
			pg.parse(s(), varparse);
		}
		else
		{
			current_child = s;
			Parse *pp = special(s);
			if(pp)
			{
				pp->set_name(s);
				return 0;
			}

			pg.parse(s());
		}
		return 0;
	}

	void parse(FILE *fp, int varparse=0)
	{
		char buffer[STR_SIZE];
		int par = 0;

		f = fp;
		set_comments();
		set_whitespace();

		while(fgets(buffer, STR_SIZE - 1, fp))
		{
			ostring s(buffer);

			incrementlinecounter();
			pg.setlinenumber(globallinenumber);

			par += parse_line(s, varparse);
			maintain();

			if(par < 0)
			{
				error(buffer, "Unmatched }'s");
			}
			if(par > 1)
			{
				error(buffer, "Unmatched {'s");
			}
			if(par == 0)
			{
				return;
			}
		}
		fprintf(stderr, "\nEOF\n");
	}

	void parse(FILE *fp, oopicList<Parameter<Scalar> > fvar,
			oopicList<Parameter<int> > ivar)
	{
		pg.float_variables += fvar;
		pg.int_variables += ivar;
		parse(fp);
	}

	void parse(FILE *fp, Parse * _parent)
	{
		_parent->add_variables(&pg);
		parse(fp);
	}

	void add_variables(ParamGroup * _pg)
	{
		_pg->float_variables += pg.float_variables;
		_pg->int_variables += pg.int_variables;
	}

	void add_comment(const char *c)
	{
		comments.add(new ostring(c));
	}

	// standard comment characters
	void set_comments()
	{
		add_comment(";");
		add_comment("//");
		add_comment("#");
	}

	// adds a character to be considered white space
	void add_whitespace(char c)
	{
		ws.add(new char(c));
	}

	void set_whitespace()
	{
		add_whitespace(' ');
		add_whitespace('\n');
		add_whitespace('\t');
	}

	/////////////////////////////////////////
	// default virtual functions for parsing
	/////////////////////////////////////////

	virtual Parse * special(ostring s)
	{
		return 0;
	}

	virtual void setdefaults()
	{
	}
	virtual void setparamgroup()
	{
	}
	virtual void maintain()
	{
	}
	virtual void check()
	{
	}
	virtual void init()
	{
	}

	void clear_parameters()
	{
		group_names.clear();
		child_names.clear();
		pg.clear();
	}

	void print_close(FILE *ffpp = stdout)
	{
		if(htmlmode) { fprintf(ffpp, "\n</body></html>"); }
	}

	void print_open(FILE *ffpp = stdout)
	{
		if(htmlmode)
		{
			fprintf(ffpp, "<html><head><title>%s</title></head>", progdesc());
			fprintf(ffpp, "<body>\n");
		}
		if(htmlmode) { fprintf(ffpp, "<h1>"); }
		fprintf(ffpp, "%s\n", progdesc());
		if(progdesc.length()) { fprintf(ffpp, "\n"); }
		if(htmlmode) { fprintf(ffpp, "</h1><p>\n"); }
	}

	void print_parameters(FILE *ffpp = stdout)
	{
		Parse *tmptr;

		print(ffpp);

		if(child_names.size())
		{
			if(htmlmode) { fprintf(ffpp, "<b>"); }
			fprintf(ffpp, "\n// Subsections: \n");
			if(htmlmode) { fprintf(ffpp, "</b>"); }
			print_child_names(ffpp);
		}

		if(htmlmode) { fprintf(ffpp, "<br>"); }
		fprintf(ffpp, "}\n");

		finish_construction = false;
		for(unsigned int i=0; i < child_names.size(); i++)
		{
			for(unsigned int j=0; j < child_names[i].size(); j++)
			{
				tmptr = special(child_names[i][j]);

				if (tmptr) {
					tmptr->set_name(child_names[i][j]);
					tmptr->set_parent(this);
					tmptr->clear_parameters();
					tmptr->setdefaults();
					tmptr->setparamgroup();
					tmptr->print_parameters(ffpp);
				}
			}
		}
	}

#ifdef MPIDEFINED
	// MPI functions
	void sendScalar(Scalar *a)
	{
	}

	void sendint(int *a)
	{
	}

	void recvScalar(Scalar *a)
	{
	}

	void recvint(int *b)
	{
	}

	void mpicall(void *buf)
	{
		MPI_Datatype thetype;

	}
#endif // MPIDEFINED

};

#endif
