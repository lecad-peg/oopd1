#ifndef PD1
#define PD1

/* Previous definitions for correct compilation*/

#define Scalar SCALAR
#define STR_SIZE 501


#include "xgscalar.h"
#include <math.h>

#include "main/oopiclist.hpp"
#include "main/precision.hpp"
#include "utils/message.hpp"



//      Define classes for forward references.

class   SpatialRegion;
class   Fields;
class   Grid;
class   Boundary;
class   ParticleGroup;
class   Particle;
class   Species;
class   Diagnostic;

//--------------------------------------------------------------------
//      typedefs

typedef oopicList<Species> SpeciesList;
typedef oopicList<ParticleGroup> ParticleGroupList;
typedef oopicList<Boundary> BoundaryList;
typedef oopicList<Diagnostic> DiagList;
typedef oopicList<SpatialRegion> SpatialRegionList;

//// numeric constants
#define PI M_PI
const double	TWOPI = 2*PI;
#define ONETHIRD 0.33333333333333333333333333333333333333333333333
#define TWOTHIRDS 0.66666666666666666666666666666666666666666666667


//// physical constants
#define EPSILON0 8.85418781761E-12
const double  SPEED_OF_LIGHT = 299792458.;
const double iSPEED_OF_LIGHT = 1. / SPEED_OF_LIGHT;
const double  SPEED_OF_LIGHT_SQ = SPEED_OF_LIGHT * SPEED_OF_LIGHT;
const double iSPEED_OF_LIGHT_SQ = 1. / SPEED_OF_LIGHT_SQ;
const double  MU0 = 4.0e-07 * PI;
const double iMU0 = 1. / MU0;
const double iEPS0 = MU0 * SPEED_OF_LIGHT_SQ;
const double  EPS0 = 1/iEPS0;
const double ELECTRON_MASS    = 9.1093897e-31;   // in kg
const double ELECTRON_MASS_EV = 510999.06;       // in eV
const double PROTON_MASS    = ELECTRON_MASS    / 5.44617013e-04;
const double PROTON_MASS_EV = ELECTRON_MASS_EV / 5.44617013e-04;
const double PROTON_CHARGE   =  ELECTRON_MASS * 1.75881962e+11;
const double ELECTRON_CHARGE = -PROTON_CHARGE;
const double K_GAS = 1.380650E-23; // gas constant, in J/K
const double EV_IN_K = PROTON_CHARGE / K_GAS;
const double NeVperTORR = 8.3221e20;

#ifndef LoadType
#ifndef LOAD_TYPE_DECLARED
enum LoadType {UNIFORM, UNIFORM_NO_EP, RANDOM, QUIET, FROM_DUMPFILE}; //add FROM_DUMPFILE, for dumpfile, MAL 5/15/10
#define LOAD_TYPE_DECLARED
#endif
#endif

#ifndef Direction
#ifndef DIRECTION_DECLARED
#define DIRECTION_DECLARED
enum Direction {POSITIVE=0, NEGATIVE=1, NONE=2};
#endif
#endif


//	macros
#define MIN(a,b)			((a<b) ? (a) : (b))
#define MAX(a,b)			((a>b) ? (a) : (b))

#define NMAX 50000 // default maximum # per particle group

// types of particle groups
#define PARTICLEGROUPARRAYTYPE 0

#if 0
// types of particle boundary conditions
#define TRANSMITTED 0
#define ABSORBED 1
#endif

// types of circuits
#define VOLTAGESOURCE 0
#define CURRENTSOURCE 1

// no POSIX integer round?
// note:  rounds towards 0 for negative values (bug)
#define ROUND(x)  int(x + 0.5)

// MPI related C preprocessor macros
#define PD1_MPI_ROOT 0

// NSPDATA: # of data needed for species construction (minimum):
// q, m, np2c
#define NSPDATA 3
#define NSPQ    0
#define NSPM    1
#define NSPNP2C 2

// should this and the below #define be merged?  JH, 8/3/2005
#ifdef CPPWORKSPROPERLY
#if (sizeof(Scalar) == sizeof(double))
#define MPI_SCALAR MPI_DOUBLE
#endif
#if sizeof(Scalar) == sizeof(float)
#define MPI_SCALAR MPI_FLOAT
#endif
#endif

#endif
