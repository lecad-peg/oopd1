#ifndef BINNEDRECURSE
#define BINNEDRECURSE

// BinnedRecurse -- recursively walk through the binned class

template<int N, class TT, class B, class RETVAL, class ARGS>

class BinnedRecurse
{
public:
  BinnedRecurse() {};

  void recurse(Binned<N,TT> & a, B &obj, RETVAL (B::*func)(ARGS*), int dim=N-1)
  {
    static ARGS buffer[N];
    static vector<int> indices(N);

    for(int i=0; i < a.get_axis(dim).size(); i++)
      {
	indices[dim] = i;
	buffer[dim] = a.get_axis(dim).val(i);

	if(dim == 0)
	  {
	    RETVAL tmp = ((obj).*(func))(buffer);
	    a.get_data().val(&indices[0]) = tmp;
	  }
	else
	  {
	    recurse(a, obj, func, dim-1);
	  }
      }
  }

};

#endif
