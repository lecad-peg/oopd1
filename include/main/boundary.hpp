#ifndef BOUNDARY_H
#define BOUNDARY_H


#include "utils/ovector.hpp"
#include "main/parse.hpp"
// generic Boundary class

// forward class declarations
class Fields;
class PoissonSolver;
class ParticleGroup;
class Species;
class Grid;
class TriMatrix;
class Emitter;
class BeamEmitter;
class FieldEmitter;
class Secondary;
class Radiation;
class BinnedDistribution;
class DiagnosticControl;
class VDistribution;
class DataArray;

#ifndef Material
#ifndef MATERIAL_DECLARED
#define MATERIAL_DECLARED
enum Material {CONDUCTOR,DIELECTRIC,EMPTY};
#endif
#endif

// types of particle boundary conditions
#ifndef Property
#ifndef PROPERTY_DECLARED
#define PROPERTY_DECLARED
enum Property {ABSORBED,TRANSMITTED};
#endif
#endif

class Boundary : public Parse, public Message
{
protected:
    ostring name, name2, name3;

    // one variable-weight particle-group per species
    ParticleGroup **particles;
    ParticleGroup **secondaryparticles;

    Scalar **weights;
    Scalar **weights_old;
    Scalar *I_sp;
    Scalar Q; // total charge on the Boundary
    double *absorbed_particles; // Number of absorbed particles at the wall, HH 04/28/16.
    double *emitted_particles; // Number of emitted particles at the wall, HH 04/28/16.
    double V_DC; // DC self bias, HH 04/28/16.
    double C; // Constant in Yonemura and Nanbu's algorithm, HH 04/28/16.
    double Delta_V_DC; // Constant in Yonemura and Nanbu's algorithm, HH 04/28/16.
    Scalar *Iconv_array; // Spatiotemporal diagnostics, HH 04/18/16
    Scalar *Ickt_array; // Spatiotemporal diagnostics, HH 04/18/16
    SpeciesList * theSpeciesList;
    oopicListIter<Species> species_iter;
    oopicList<Emitter> emitterlist;
    oopicListIter<Emitter> emitter_iter;
    oopicList<Secondary> secondarylist;
    oopicListIter<Secondary> secondary_iter;
    oopicList<BinnedDistribution> distlist;
    oopicListIter<BinnedDistribution> dist_iter;
    oopicList<Radiation> radiationlist;
    oopicListIter<Radiation> radiation_iter;

    Fields *thefield;
    Grid *thegrid;
    int j0, j1, nspecies, nmax;
    int npoints;
    int surface_npoints;
    Scalar dt;
    Scalar phi0, phi1;

    Scalar I_circ; // Circuit current at conductor, MAL 12/4/09
    Scalar P_circ; // Circuit power at conductor at time t, MAL 12/4/09
    Scalar aveP_circ; // for time average power at conductor, MAL 12/5/09

    Scalar sum_convQ; // Added by JDN. Running total of convective charge deposited
    // on boundary.
    bool *usable;

    Material material;
    Property property;

    bool calculate_current;
    bool accumulate_particles;   // are particles accumulated on impact?
    bool * accumlate_sp;   // which species are accumulated? [UNUSED, currently]

    Vector3 *E, *E_dt, *E_grad;
    Vector3 *B, *B_dt, *B_grad;

    int accuracy_synchro;
    Scalar (Boundary::*advance_v_for_synchro)(ParticleGroup *p, int i,
                                              Scalar frac, Direction j);
    Scalar advance_v_for_synchro_0th(ParticleGroup *p, int i,
                                     Scalar frac, Direction j);
    Scalar advance_v_for_synchro_1st(ParticleGroup *p, int i,
                                     Scalar frac, Direction j);
    Scalar advance_v_for_synchro_2nd(ParticleGroup *p, int i,
                                     Scalar frac, Direction j);

public:
    Boundary(Fields *t);
    Boundary(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
             oopicList<Parameter<int> > int_variables, Fields *t);
    ~Boundary();

    void set_enable(Direction j) {usable[j] = true;}
    void set_disable(Direction j) {usable[j] = false;}
    inline bool is_usable(Direction j) const {return usable[j];}
    inline bool is_usable(int j) const {return usable[(Direction)j];}
    Material get_material() const {return material;}
    Property get_property() const {return property;}
    ostring get_name() const {return name;}
    inline int get_surface_npoints() const {return surface_npoints;}
    inline Scalar get_sumQ() const {return sum_convQ; } // for pd1::Dump access to dumpfile sum_convQ, MAL 5/21/10
    inline void set_sumQ(Scalar f) {sum_convQ = f; } // for SpatialRegion::reload_dumpfile_SR() access to dumpfile sum_convQ, MAL 5/21/10
    inline Scalar get_weights(int j, int i) const { return weights[j][i]; } // for pd1::Dump access to dumpfile weights, MAL 5/16/10

    // accessor functions
    inline SpeciesList * get_SpeciesList() const { return theSpeciesList; }
    inline Fields * get_thefield() const { return thefield; }
    inline Grid * get_thegrid() { return thegrid; }

    // get left (j0) and right (j1) grid indices
    inline int get_j0() const { return j0; }
    inline int get_j1() const { return j1; }

    // get left (r0) and right (r1) coordinates of boundary
    inline Scalar get_r0() const;
    inline Scalar get_r1() const;
    Scalar getr0() const;
    Scalar getr1() const;

    inline Scalar get_A(Direction dir) const;

    // get coordinates of j0 - 1/2 and j1 + 1/2 of boundary
    // if on system boundary, return system boundary
    Scalar get_rminus() const;
    Scalar get_rplus() const;

    // update_BC: updates the BC for the timestep
    virtual void update_BC();

    void update_BC_fields();
    void get_E(Direction dir, Vector3 &E_n) const;
    void get_Egradient(Direction dir, Vector3 &E_n) const;
    void get_dE(Direction dir, Vector3 &E_n) const;
    void get_B(Direction dir, Vector3 &B_n) const;
    void get_Bgradient(Direction dir, Vector3 &B_n) const;
    void get_dB(Direction dir, Vector3 &B_n) const;
    void get_EB(Direction dir, Vector3 &E_n, Vector3 &B_n) const;
    void get_EBgradient(Direction dir, Vector3 &E_n, Vector3 &B_n) const;
    void calculate_phi();

    // update_BC_top: top-level update -- should be used for general stuff
    void update_BC_top();

    void update_BC_post();

    // particle_BC:  passes particle group and index i
    // p: ParticleGroup of particle
    // i: index of particle
    // frac: fraction of time remaining in timestep
    // mindgame: 'dir' is the direction of impacting particles to the boundary
    //         , not ejected ones.
    virtual Property particle_BC(ParticleGroup *p, int i,
                                 Scalar frac, Direction dir);

    // fluid_BC: interactions a velocity distribution with the boundary
    // v: the distribtion function of the fluid
    // returns total number of particles absorbed by boundary
  virtual Scalar fluid_BC(Scalar density, VDistribution *v, Direction dir,
                          vector<Scalar> * moments = 0); // NO LONGER USED; REPLACED WITH evolvingfluid_BC; MAL200303
  virtual void evolvingfluid_BC(Species * ispecies, Scalar density, Scalar recombcoef, VDistribution *v, Direction dir); // add, MAL200223

    // boundary conditions for PoissonSolver
    virtual void modify_coeffs(PoissonSolver * theps) { };

    // inline functions for particle collection
    inline void delete_particle(ParticleGroup *p, int i);
    inline void absorb(ParticleGroup *p, int i, Direction j);

    inline void accumulate_particle(ParticleGroup *p, int i, Scalar frac,
                                    Direction dir, bool advance_frac);

    void eject(int n, Species *species, Scalar np2c, Direction dir);

    void process_particles(); // process the collected particles
    void accumulate_distribution();
    void delete_particles(); // delete the collected particles
    void reload_weights(FILE * DMPFile); // reload weights from dumpfile, MAL 5/16/10

    // functions for obtaining charge on the boundary
    Scalar calculate_sp_Q(Species *thes);
    Scalar calculate_sp_Q(Species *thes, Direction j);
    Scalar calculate_sp_I(Species *thes);
    Scalar calculate_Q();
    Scalar calculate_Q(Direction j);
    Scalar calculate_I();
    void zero_weights();
    Scalar get_Q_and_delete();
    Scalar sum_Q(); //Added by JDN

    // get_sigma: gets the surface charge on the boundary at node i
    // phi: potential in the SpatialRegion
    // coulombs:  if true, return in units of C.  if false, C/m^2
    virtual Scalar get_sigma(unsigned int i, Scalar * phi,
                             Scalar *rho, bool coulombs=true) const
    { printf("virtual\n"); return 0.; }

    Scalar get_sigma(Scalar *phi, Scalar *rho, bool coulombs=true) const
    {
        Scalar tot = 0.;
        for(unsigned int i=j0; i <= (unsigned)j1; i++)
        {
            tot += get_sigma(i, phi, rho, coulombs);
        }
        printf("no i\n"); return tot;
    }

    void time_average(Scalar &g, Scalar &tempg, Scalar &aveg); // for time averages (power), MAL 12/5/09

    // parse functions -- should be called from child class
    void check_boundary();
    void init_boundary();
    Parse * special_boundary(ostring s);

    void setdefaults();
    void setparamgroup();
    void init();
    void check();
    Parse * special(ostring s);

    void secondary_emission(ParticleGroup *p, int i, Scalar frac, Direction j);
  void evolvingfluid_secondaryemission(Species * ispecies, Scalar number, VDistribution *v, Direction dir); // add, MAL200224
    void radiation(ParticleGroup *p, int i);

    // For IED's, HH
    Scalar get_nBoundaries();
    Scalar get_nEnergyBins(int i);
    ostring get_BoundaryPosition(int i);
    DataArray *get_energydist(int i);
    DataArray *get_energybins(int i);
    // FOR IAD's, HH 06/02/16
    Scalar get_nAngleBins(int i);
    ostring get_IADnames(int i);
    DataArray *get_angledist(int i);
    DataArray *get_anglebins(int i);

    // Power absorbed, HH 04/22/16
    inline Scalar get_pwrAve() {return aveP_circ;}
    ostring get_BoundaryName();

    // Current diagnostics, HH 05/09/16
    void init_arrays(int size_array);
    void set_arrays(int steps_rf, int m);
    inline Scalar *get_Iconv_array() {return Iconv_array;} // For each species
    inline Scalar *get_Ickt_array() {return Ickt_array;}

    // Functions to find flux of species to the walls,
    // and flux of secondary emission, HH 04/28/16.
    void reset_absorbed_particles();
    void reset_emitted_particles();
    inline double *get_absorbed_particles() {return absorbed_particles;}
    inline double *get_emitted_particles() {return emitted_particles;}
    double absorbed_positive_flux();
    double absorbed_negative_flux();
    double emitted_positive_flux();
    double emitted_negative_flux();
    double calculate_V_DC();

    void initialize_diagnostics(DiagnosticControl *dc);

};

#endif
