#ifndef ENUMLIST
#define ENUMLIST

#include "utils/ostring.hpp"
#include "main/oopiclist.hpp"
#include "main/param.hpp"
// enumerated list:  matches lhs value with one of enumerated rhs values
class EnumList
{
private:
  ostring name;
  ostring desc;
  oopicList<Flag<int> > rhsvals;
public:
  EnumList(ostring & _name)
  {
    name = _name;
  }

  void add_item(ostring itemname, int *i, ostring desc)
  {

  }

  void print(FILE *fp)
  {
    ostring padding;
    oopicListIter<Flag<int> > thei(rhsvals);

    fprintf(fp, "\n%s : %s", name(), desc());
    padding.spaces(name.length() + 2);
    for(thei.restart(); !thei.Done(); thei++)
      {
	fprintf(fp, "\n%s", padding());
      }
  }
};

#endif
