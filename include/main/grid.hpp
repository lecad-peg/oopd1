#ifndef GRID_H
#define GRID_H


#include "utils/ovector.hpp"
#include "main/parse.hpp"

// forward declarations
class Boundary;
class Dielectric;
class ParticleGroup;

// Note that these defines correspond to the power of r
// needed to corresonding to area
// PLANAR MUST BE 0!
// CYLINDRICAL MUST BE 1
// SPHEREICAL MUST BE 2
// mindgame : Simpler way to define
#ifndef GEOM_TYPE_DECLARED
enum {PLANAR=0, CYLINDRICAL=1, SPHERICAL=2};
#define GEOM_TYPE_DECLARED
#endif

// ALL geometry dependent stuff should be done here
class Grid : public Parse, public Message
{
protected:

	int ng, nc;
	int type;
	Scalar *X, *V, *A; // locations, volumes, and surface areas of nodes
	// these arrays are ng long when allocated, save A is ng+1, as
	// each node has two surface areas:  A[ng] = area of electrode at x1
	// V[] is the volumes for weighting

	// cell_volume[] : volumes delimited by half cells [x-1/2:x+1/2]
	// For system boundary nodes, the volume is delimited on
	// the appropropriate side by the system boundary
	Scalar *cell_volume;

	Scalar *dxcell; // length associated with each cell (nc values)
	// dxcell[i] is the cell between node i and node i+1

	Scalar *dxnode; // length associated with each node

	bool Xsetflag; // TRUE if X, V, and A have been computed
	Scalar *Xcell; // location of cell boundaries
	Scalar *ratio; // ratio of dxs for nonuniform mesh

	bool boundary_setflag;
	BoundaryList **boundary_list;

	Scalar dx, x0, x1, L, area, height;
	Scalar area_coeff, vol_coeff;
	Scalar dxdx;     // dx^2
	Scalar inv_dxdx; // inverse dx^2 -- used in Laplacian operators

	Scalar dtdx;

	bool uniform, periodic;

public:
	Grid();
	Grid(Parse * _parent);
	Grid(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
			oopicList<Parameter<int> > int_variables);
	Grid(int nngg, Scalar *xx, int geomtype);
	~Grid();

	// accessor functions
	inline bool isuniform(void) { return uniform; }
	inline bool isperiodic(void) { return periodic; }
	inline int getnc(void) { return nc; }
	inline int getng(void) const { return ng; }
	inline Scalar getdx(void) { return dx; }
	inline Scalar get_dxdx(void) { return dxdx; }
	inline Scalar getx0(void) { return x0; }
	inline Scalar getx1(void) { return x1; }
	inline Scalar getL(void) { return L; }
	inline Scalar * getX(void) { return X; }
	inline Scalar getX(int i) { return X[i]; }
	inline Scalar getV(int i) { return V[i]; }
	inline Scalar get_cell_volume(int i) { return cell_volume[i]; }
	inline int geometry(void) { return type; }
	inline Scalar get_dxcell(int i);
	inline Scalar get_dxnode(int i);

	//////////////////////////////////////////
	// inline functions for system boundaries
	//////////////////////////////////////////

	inline Scalar lhs_area(void) { return A[0]; }
	inline Scalar rhs_area(void) { return A[ng]; }

	inline bool is_lhs(int i)
	{
		if(i == 0) { return true; }
		return false;
	}

	inline bool is_rhs(int i)
	{
		if(i == (ng - 1)) { return true; }
		return false;
	}

	ostring lhs_string() const;
	ostring rhs_string() const;

	ostring geometry_string(int i=0) const;

	// functions inherited from Parse
	void setdefaults();
	void setparamgroup();
	void check();
	void init();

	inline Scalar getA(Scalar R);
	inline Scalar get_area(int i);
	inline Scalar getRfromV(Scalar V);
	inline Scalar getV(Scalar R);

	// gets volume between points X0 and X1
	inline Scalar getV(Scalar X0, Scalar X1);

	Scalar getGridVolume() const;

	void setXcell();
	void setV();
	void setX();
	void setdx();
	void setboundaries(BoundaryList & blist);
	void setdtdx(Scalar dt);

	void get_uniform_volume_mesh(Scalar *x, Scalar xstart, Scalar xend,
			int n, bool endpoints=false);

	void creategrid(void);
	void creategridfromarray(int nngg, Scalar *xx, int geomtype);

	// integration functions -- integrate f over grid volume

	Scalar integrate(Scalar *f, int start=0, int end=-1, int power=1,
			bool vmult=false);

	template<class T>
	Scalar integrate(Scalar (T::*f)(int), T * instance, int start=0, int end=-1);

	// returns 2nd order gradient on uniform meshes
	// must be called by an interior point or segfault
	inline Scalar gradient_interior(Scalar *field, int location);

	// returns 2nd order gradient on uniform meshes
	inline Scalar gradient(Scalar *field, int location);
	inline Vector3 gradient_interior(Vector3 *field, int location);
	inline Vector3 gradient(Vector3 *field, int location);
	inline Vector3 gradient_positive(Vector3 *field, int location);
	inline Vector3 gradient_negative(Vector3 *field, int location);
	// DivGrad -- gets coefficients for Div(field*Grad(desired_field))
	// e.g. Div(epsilon*Grad(Phi)) -- coefficients for Phi
	// Doesn't work wtih mesh endpoints
	// field_left = field in left-hand cell (at midpoint)
	// field_right = field at right-hand cell (at midpoint)
	inline void DivGrad(int i, Scalar & left, Scalar & center, Scalar & right,
			Scalar field_left, Scalar field_right);

	// gets the coefficients for the Laplacian on a uniform mesh
	inline void Laplacian(int i,
			Scalar & left, Scalar & center, Scalar & right);

	//  weighting functions
	inline void WeightLinear(Scalar *xval, Scalar *fval, int n,
			Scalar *gridval);

	// weights fval to thearray using linear weighting
	// assumes thearray has ng members, x in grid units
	inline void WeightLinear(Scalar x, Scalar fval, Scalar *thearray);

	// Added for PGA accumulate_density; avoids multiply within particle loop, MAL 9/12/09
	// weights unity to thearray using linear weighting
	// assumes thearray has ng members, x in grid units, MAL 9/12/09
	inline void WeightLinear(Scalar x, Scalar *thearray);

	// Added for advance_v.hpp; weights 1D particle velocity moments to grid using linear weighting, MAL 9/12/09, revised 7/5/10
	// assumes the arrays have ng members, x and v1 in grid units
	inline void WeightLinear1D(Scalar x, Scalar v1, Scalar *v1array, Scalar *v1sqarray);

	// Added for advance_v.hpp, weights 3D velocity moments of particles to grid using linear weighting, MAL 9/12/09, revised 7/5/10
	// assumes the arrays have ng members, x and v in grid units
	inline void WeightLinear3D(Scalar x, Scalar v1, Scalar v2, Scalar v3, Scalar *v1array, Scalar *v2array,
			Scalar *v3array, Scalar *v1sqarray, Scalar *v2sqarray, Scalar *v3sqarray);

	// weights fval to thearray using nearest grid point weighting
	// assumes thearray has ng members, x in grid units
	inline void WeightNGP(Scalar x, Scalar fval, Scalar *thearray);

	// linear interpolation of thearray to point x
	// assumes thearray has ng members, x in grid units
	inline Scalar GatherLinear(Scalar x, Scalar *thearray);
	inline Vector3 GatherLinear(Scalar x, Vector3 *thearray, int dim=3);

	// nearest grid point gather for point x
	// assumes thearray has ng members, x in grid units
	inline Scalar GatherNGP(Scalar x, Scalar *thearray);

	// conversion of grid units to physical units
	inline Scalar gridXtoX_uniform(Scalar gridX) { return x0 + dx*gridX; }
	inline Scalar gridXtoX(Scalar gridX);

	// converts array gridX into physical units
	void gridXtoX(int n, Scalar *gridX);

	inline void gridVtoV(int n, Scalar *gridv, Scalar dt);
	inline void VtogridV(int n, Vector3 *v, Scalar dt);

	// conversion of physical units to grid units
	inline Scalar XtogridX_uniform(Scalar x) { return (x-x0)/dx; }
	inline Scalar XtogridX(Scalar x);
	void dXtogriddX(int n, Scalar *_dx); // added to fix bug in emitter.cpp, MAL 9/17/09
	void XtogridX(int n, Scalar *x);
	inline void VtogridV(int n, Scalar *v, Scalar dt);
	void print_grid(FILE *fp = stdout) const;

	inline void move(Scalar &x, Scalar dx, ParticleGroup *p, int & i);
	inline void move_nonuniform(Scalar &x, Scalar dx, ParticleGroup *p, int & i);
};

#endif
