#ifndef PARTICLEGROUP_H
#define PARTICLEGROUP_H

//********************************************************
// particlegroup.hpp
//
// Revision history
//
// (JohnV 17Jun2003) Original code.
//
//********************************************************
#include "utils/ovector.hpp"
#include "main/species.hpp"
// forward declarations
class Fields;
class Grid;

class ParticleGroup
{
protected:
	bool variableWeight; // flag = 0 for uniform weighting, 1 for variable
	bool reallocflag;    // reallocate particles automatically if overflow
	int reallocmult;    // amount to multiply nmax by during reallocation

	int nmax; // maximum number of particles allowed
	int n; // number of particles in the group
	// if dual_index_flag is on
	//   n_old is used instead of n.
	int n_old;
	// if dual_index_flag is on/off, n_ptr -> n_old/n
	int* n_ptr;
	bool dual_index_flag;
	Species *theSpecies; // pointer to the Species for this ParticleGroup
	Fields *theFields; // pointer to the Fields
	Grid *theGrid; // pointer to the Grid containing this ParticleGroup
	Scalar q0; // base charge for this group
	Scalar m0; // base mass for this group

	Scalar qm0; // q0/m0

	Scalar w0; // base weight for this group
	Scalar dt; // timestep
	Scalar dxdt; // dx/dt

public:

	ParticleGroup(int _nmax, Species *_theSpecies, Fields *_theFields,
			Scalar _w0, bool _variableWeight);
	virtual ~ParticleGroup() {}

	//// functions for adding particles

	// add_particle method must be supplied by derived class
	// returns 0 upon success, 1 if particle group full
	virtual int add_particle(Scalar X, Scalar V1,
			Scalar V2=0., Scalar V3=0., bool vnorm=true,
			bool update_field=false, Scalar W=-1.) = 0;
	virtual int add_particles(int n, Scalar W, Scalar *X, Vector3 *V,
			bool vnorm=true, bool update_field=false) = 0;
	virtual int add_particles(int n, Scalar W, Scalar *X, Scalar *dX,
			Vector3 *V,
			bool vnorm=true, bool update_field=false) = 0;
	virtual int add_particles(int N, Scalar *X, Scalar *V1=NULL, Scalar *V2=NULL,
			Scalar *V3=NULL, bool vnorm=true,
			bool update_field=false, Scalar *W=NULL) = 0;

	int add_particle(Scalar X, Vector3 Vvec, bool vnorm=true,
			bool update_field=false, Scalar W=-1.);

	int add_particle(ParticleGroup *p, int i, bool vnorm=true,
			bool update_field=false);
	bool is_capable_of_adding_particles(int N);

	//// functions for deleting particles

	void delete_all() { n = 0; }
	virtual void delete_particle(int i) = 0;

	virtual void realloc(int i) = 0;  // reallocate memory for i particles

	// advance particle velocities must be supplied by derived class
	// add 1D and 3D versions to weight v and v^2 to the grid for flux, energy and JdotE
	// diagnostics, MAL 6/20/10
	virtual void advance_v(Scalar ddt) = 0;

	virtual void advance_v(int i, Scalar ddt, Vector3, Vector3 B_sp=0) = 0;

	virtual void advance_v1D(Scalar ddt) = 0;

	virtual void advance_v1D(int i, Scalar ddt, Vector3, Vector3 B_sp=0) = 0;

	virtual void advance_v3D(Scalar ddt) = 0;

	virtual void advance_v3D(int i, Scalar ddt, Vector3, Vector3 B_sp=0) = 0;

	virtual void advance_vload(Scalar ddt) = 0;

	// advance particle positions
	virtual void advance_x(Scalar dt) = 0;

	void init_dual_index();
	void turn_off_dual_index();

	// check for legal data
	void checkData();

	virtual int get_particlegrouptype() = 0;

	// set reallocation flag (default = don't reallocate memory)
	// and appropriate multiplier
	void setreallocflag(bool fl, int mult=-1)
	{
		reallocflag = fl;
		if(mult > 1)
		{
			reallocmult = mult;
		}
	}

	// accumulate number density into theSpecies->species_density
	virtual void accumulate_density() = 0;

	// accessor functions for data:
	inline int get_n() const {return *n_ptr;};
	inline int get_nmax() const {return nmax;};
	inline Scalar get_q0() const {return q0;};
	inline Scalar get_qm0() const {return qm0;};
	inline Scalar get_m0() const {return m0;};
	inline Scalar get_w0() const {return w0;};
	inline bool is_variableWeight() const {return variableWeight;};
	inline Species * get_species() {return theSpecies;};
	inline Scalar get_dxdt() const {return dxdt;}

	// accessor functions for derived class data:
	virtual Scalar get_x(int i) const = 0; // position of ith particle
	Scalar get_xMKS(int i) const;          // MKS position of ith particle
	virtual Vector3 get_v(int i) const= 0; // velocity of the ith particle

	// return velocity in MKS units
	Vector3 get_vMKS(int i) const {return get_v(i)*dxdt;}
	inline Vector3 get_vMKS(Vector3 v) const {return v*dxdt;}
	inline Scalar get_vMKS(Scalar v) const {return v*dxdt;}

	// return velocity in grid units
	inline Vector3 get_vnorm(Vector3 v) const {return v/dxdt;}

	// set velocity of the ith particle
	virtual void set_v(int i, Vector3 v) = 0;

	// charge of the ith particle
	virtual Scalar get_q(int i) const {return get_w(i)*q0;}

	virtual Scalar get_w(int i) const = 0; // return the particle weight

	// get electric field
	virtual void get_E(int i, Scalar & e1, Scalar & e2, Scalar & e3) = 0;
	virtual Vector3 get_e(int i) = 0; // electric field at ith particle
	virtual Scalar get_E(int i) = 0;

	// set electric field
	virtual void add_E(int i, Scalar e1, Scalar e2, Scalar e3) = 0;

	// set electric field
	virtual void set_E(int i, Vector3 &E) = 0;
	virtual void set_E(int i, Scalar &E) = 0;

	virtual void set_frac(int i, Scalar _frac) = 0;
	virtual void set_direction(int i, Direction _dir) = 0;

	// set electric field at particles to zero
	virtual void reset_E() = 0;

	// obtain electric field from grid
	virtual void Gather_E();

	// copies num values of x
	virtual void copy_x(Scalar *xcp, int ns, int num=-1) = 0;

	// copies num values of v_i
	virtual void copy_v(Scalar *vcp, int ns, int dim, int num=-1) = 0;

	virtual void average_velocity(Vector3 &val, Scalar &www) = 0;
	virtual Scalar max_position() = 0;
	virtual Vector3 front_velocity() = 0;
	virtual Vector3 max_velocity() = 0;

	// adds moments of the distribution function to thearray
	virtual void add_moment(int nmom, Scalar *thearray);

	void print_particles(FILE *fp=stderr);

	void dump_particles(FILE *fp); // for dumpfile, MAL 5/4/10

	//sknam: set x for the particle passing the periodic boundary
	virtual void set_x(int i, Scalar xnew) = 0;

#define PG_PART
#include "main/conv_engy_vel.hpp"
#undef PG_PART
};

#endif // ifndef PARTICLEGROUP
