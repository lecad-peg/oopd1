#ifndef TIMING_H
#define TIMING_H

#include "utils/ovector.hpp"
#include <cstdio>
/* timing.hpp --
 * time different components of the oopd1 PIC code
 */

#include <string.h>

#undef TIME

#ifdef MPIDEFINED
#include <mpi.h>
#define TIME_GET MPI_Wtime()
#define TIME_TYPE 	Scalar
#define TIMESCALE 		1.0
#else
#include <time.h>
#define TIME_GET clock()
#define TIME_TYPE clock_t
#define TIMESCALE (Scalar (CLOCKS_PER_SEC))
#endif

// events to be timed
#define PARTICLE_PUSH  0
#define FIELD_SOLVE    1
#define EMISSION       2
#define DIAGNOSTICS    3
#define INITIALIZATION 4
#define REACTION       5
#define XGRAFIXTIME    6

#ifdef MPIDEFINED
#define MPITIME 7
#define NTIMES  8
#else
#define NTIMES  7
#endif

class Time
{
 public:
  //  static Scalar ttimes[NTIMES]; // time in seconds
  static Scalar * ttimes; // time in seconds

  static TIME_TYPE ts[NTIMES]; // start times
  static TIME_TYPE te[NTIMES]; // end times
  static Scalar npartpushed;
  static int current_type;

  static Scalar * set_times(void)
    {
      ttimes = new Scalar[NTIMES];
      memset(ttimes, 0, NTIMES*sizeof(Scalar));
      return ttimes;
    }

  Time();

  void add_time(int type);
  Scalar total_time(void);
  void print_description(char *c, int i);
  void print_times(FILE *fp=stdout, bool open=false);

  void start_time(int type);
  void end_time(int type);

  void start_particle_time(int np);


  inline void end_time(void)  {      end_time(current_type);    }
  inline void switch_time(int type)
    {
      end_time();
      start_time(type);
    }
  inline void time_particles(int nparticles)
    {
      npartpushed += nparticles;
      switch_time(PARTICLE_PUSH);
    }
};

#endif
