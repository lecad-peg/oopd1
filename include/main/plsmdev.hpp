#ifndef PLSMDEV_H
#define PLSMDEV_H

#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "main/parse.hpp"
// forward declarations
class Species;
class SpatialRegion;
class DiagnosticParser;

class PlsmDev : public Parse
{
	ostring filename; // name of the input file

	SpeciesList specieslist;
	oopicListIter<Species> species_iter; // add for print dumpfile, MAL 11/17/10
	SpatialRegionList sptlrgnlist;
	double t;
	Scalar dt;
	Scalar ttemp; // just for parsing
	int particlegrouptype;
	int nsteps_total; // HH 03/03/16

	DiagnosticParser *thediagparser;

	// MPI global variables
	int rank, nproc;
	int loadline;			// added by JK, 2017-09-22

public:
    PlsmDev();
    ~PlsmDev();

    void update();

    void setfilename(char *c);

    void print_dumpfile(char * dumpfile); // to debug dumpfile, MAL 5/13/10
    void reload_dumpfile(); // top level function to reload dumpfile, MAL 5/16/10

    // accessor functions
    inline Scalar get_t() { return t; }
    inline double * get_t_ptr() { return &t; }
    inline int get_rank()  { return rank; }
    inline int get_nproc() { return nproc; }
    inline SpatialRegionList * get_sptlrgnlist() { return &sptlrgnlist; } //add for pd1.cpp access to sptlrgnlist, MAL 5/4/10
    inline void set_nsteps_total(int _nsteps_total){nsteps_total =_nsteps_total;} // HH 03/03/16
    inline int get_nsteps_total(){return nsteps_total;} // HH 03/03/16

    // access filename, HH 02/22/16
    inline ostring get_filename() {return filename;}

    // functions from Parse
    void setdefaults();
    void setparamgroup();
    void init();
    Parse * special(ostring s);

    // MPI related functions
    void add_SpatialRegion(int nn, Scalar *newgrid, int geomtype, Scalar dt);
    void get_SpatialRegions_fromremote();

};

#endif
