#ifndef PARSECPP
#define PARSECPP

#include<stdio.h>
#include<stdlib.h>

#include "utils/ostring.hpp"
#include "main/pd1.hpp"
#include "main/parse.hpp"
#include "main/plsmdev.hpp"

bool Parse::finish_construction = true;
bool Parse::htmlmode = false;

void parse(char *fname)
{
	bool flag = 1;
	int i, j;
	char buffer[STR_SIZE];
	FILE *fp;
	fp = fopen(fname, "r");

	i = 0;
	while(flag)
	{

		if(!fgets(buffer, STR_SIZE - 1, fp))
		{
			fprintf(stderr, "\nPremature EOF");
			exit(1);
		}
		i++;

		ostring s(buffer);
		s.strip(' ');
		s.strip('\n');
		s.strip('\t');
		s.stripcomment("//");
		s.stripcomment(";");
		s.stripcomment("#");
		if(s == ostring("{"))
		{
			flag = 0;
		}
	}
	fclose(fp);
	fp = fopen(fname, "r");
	for(j = 0; j < i-1; j++) {
		char* temp = fgets(buffer, STR_SIZE - 1, fp);
		fprintf(stdout, "%s", buffer);
		theplasmadevice.incrementlinecounter();
	}
	theplasmadevice.setfilename(fname);
	theplasmadevice.parse(fp);

	Parse::globallinenumber = -1; // done parsing

	fclose(fp);
}

#endif
