#include "utils/ovector.hpp"
#include "main/parse.hpp"
#ifdef NEGATIVE_MOVE
#define CELL_ADJUSTMENT
#define PP
#define COMPARE <
#define CELL_READJUST cell--;
#define NEG -
#define DIRECTION POSITIVE
#else
#define CELL_ADJUSTMENT  +1
#define PP ++
#define COMPARE >
#define CELL_READJUST
#define NEG
#define DIRECTION NEGATIVE
#endif
Scalar xrem, dxorig;
int cell = int(x);
oopicListIter<Boundary> thej;
bool absorbed_done = false;

xrem = Scalar(cell CELL_ADJUSTMENT) - x;
dxorig = dx;
while(dx != 0.)
{
#ifdef NONUNIFORM
  xrem *= ratio[cell];
#endif

  if(xrem COMPARE dx)
    {
      x += dx;
      i++;
      return;
    }
  else
    {
      dx -= xrem;
      if(!(boundary_list[PP cell]->isEmpty()))
      {
	for(thej.restart(*boundary_list[cell]); !thej.Done(); thej++)
	{
	  if(thej()->particle_BC(p, i, dx/dxorig, DIRECTION) == ABSORBED)
	    {
	      absorbed_done  = true;
	    }
	}
	if (absorbed_done) return;
      }
      xrem = NEG 1.;
      x = cell;
      CELL_READJUST

    }
}
i++;

#undef CELL_ADJUSTMENT
#undef COMPARE
#undef PP
#undef CELL_READJUST
#undef NEG
#undef DIRECTION
