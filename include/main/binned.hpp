#ifndef BINNED
#define BINNED

// class Binned:
// binned distribution class

#include<stdarg.h>
#include "utils/ovector.hpp"
#include "utils/equation.hpp"
#include "main/uniformmesh.hpp"
#include "main/ndarray.hpp"
template <int D, class T>

class Binned
{
private:
  UniformMesh axes[D];
  ndarray<T> data;
  ndarray<Scalar> weights;  // needed for linear weighting

#include "main/binned_recurse.hpp"

public:
  Binned() { };
  Binned(int d1, ...)
  {
    int i;
    int sizetmp[D];

    va_list argp;
    va_start(argp, d1);
    sizetmp[0] = d1;
    for(i=0; i < D; i++)
      {
	if(i > 0) { sizetmp[i] = va_arg(argp, int); }
	axes[i].init_mesh(sizetmp[i], 0., 1.);  // use zero and one for now
      }
    va_end(argp);

    data.alloc(D, sizetmp);
    data = 0.;

    // for linear weighting -- need two neighbors in each dimension
    for(i=0; i < D; i++)
      {
	sizetmp[i] = 2;
      }
    weights.alloc(D, sizetmp);
  }

  T eval(Scalar *x)
  {
    register int i;
    int loc[D];
    Scalar frac[D];
    T ans = 0.;

    // determine location in mesh, including fraction
    for(i=0; i < D; i++)
      {
	frac[i] = 0.;
	loc[i] = axes[i].floor_index(x[i], &(frac[i]));
      }

    // linear weighting
    weights = 1.;

    calculate_weights(D-1, frac);
    //weights.print();

    return weigh(loc);
  }

  void calculate_weights(int dim, Scalar *frac)
  {
    Scalar prodtmp;

    for(int i=0; i < 2; i++)
      {
	switch(i)
	  {
	    //	  case 0:
	  case 1:
	    prodtmp = frac[dim];
	    break;
	    //	  case 1:
	  case 0:
	    prodtmp = 1. - frac[dim];
	    break;
	  }
	weights.multiply_row(dim, i, prodtmp);
      }

    if(dim == 0)
      {
	return;
      }
    else
      {
	calculate_weights(dim-1, frac);
      }
  }

  T weigh(int * loc, int dim=D-1)
  {
    static T val;
    static int data_index[D];
    static int weight_index[D];

    if(dim == D-1)
      {
	val = 0;
      }

    for(int i=0; i < 2; i++)
      {
	weight_index[dim] = i;
	data_index[dim] = i + loc[dim];

	if(dim == 0)
	  {
	    val += data(data_index)*weights(weight_index);
	  }
	else
	  {
	    weigh(loc, dim-1);
	  }
      }
    return val;
  }


  ///////////////////////////////////
  // Functions for Filling the Bins
  ///////////////////////////////////

  // populate a binned mesh with linear weighting
  Binned<D, T> & operator = (Binned<D, T> & a)
  {
    // check to make sure LHS mesh is within RHS mesh
    for(int i=0; i < D; i++)
      {
	if(!axes[i].is_within(a.get_axis(i)))
	  {
	    fprintf(stderr, "\nError, Binned<D, T> & operator =\n");
	    fprintf(stderr, "LHS mesh not within RHS mesh\n");
	    exit(1);
	  }
      }

    // perform weighting -- TO BE DONE!
    BinnedRecurse<D, T, Binned<D,T>, T, Scalar> recursor;
    recursor.recurse(*this, a, &Binned<D,T>::eval);

    return *this;
  }

  // set a binned distribution equal to an Equation
  void set_equal(Equation *theq, int dim=D-1)
  {
    /*
    static double eqbuffer[D];
    static vector<int> indices(D);

    for(int i=0; i < axes[dim].size(); i++)
      {
	indices[dim] = i;
	eqbuffer[dim] = axes[dim].val(i);
	if(dim == 0)
	  {
	    Scalar tmp = theq->eval(eqbuffer);
	    data(&indices[0]) = tmp;
	  }
	else
	  {
	    set_equal(theq, dim-1);
	  }
      }
    */
    BinnedRecurse<D, T, Equation, Scalar, double> testcase;
    testcase.recurse(*this, *theq, &Equation::eval);
  }

  void set_limits(int i, Scalar min, Scalar max)
  {
    axes[i].init_mesh(data.get_size(i), min, max);
  }

  ///////////////////////////
  // Accessor Functions
  ///////////////////////////
  inline UniformMesh & get_axis(int i) { return axes[i]; }
  inline ndarray<T> & get_data(void) { return data; }

  ///////////////////////////
  // Functions for Printing
  ///////////////////////////

  void print(FILE *fp=stdout, int dim=D-1)
  {
    static vector<int> indices;
    T val;

    if(indices.size() != D)
      {
	indices.resize(D);
      }

    for(int i=0; i < axes[dim].size(); i++)
      {
	indices[dim] = i;

	if(dim == 0)
	  {
	    val = data(&indices[0]);
	    for(int j=0; j < indices.size(); j++)
	      {
		fprintf(fp, "%g ", axes[j].val(indices[j]));
	      }
	    fprintf(fp, "%g\n", val);
	  }
	else
	  {
	    //	    fprintf(fp, "%g ", axes[dim].val(i));
	    print(fp, dim-1);
	  }
      }

    if(dim == 0)
      {
	fprintf(fp, "\n");
      }
  }

  void print_axes(FILE *fp=stdout)
  {
    int i, j;
    for(i=0; i < D; i++)
      {
	for(j=0; j < axes[i].size(); j++)
	  {
	    fprintf(fp, "%g ", axes[i].val(j));
	  }
	fprintf(fp, "\n");
      }
  }

};

#endif
