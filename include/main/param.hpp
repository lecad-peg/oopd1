#ifndef PARAM_H
#define PARAM_H

#include<stdio.h>
#include<stdarg.h>

#include "main/pd1.hpp"
#include "utils/ostring.hpp"
#include "main/oopiclist.hpp"
#include "utils/equation.hpp"

// Parameter:  templated class of named variables
// used for parsing
template<class T>
class Parameter
{
 protected:
  bool def;
  T *value;
  T vl;
 public:
  ostring name;
  ostring desc;
  ostring type;
  Parameter()
    {
      value = NULL;
      def = 0;
      name = "";
      desc = "";
      type = "";
    }

  Parameter(const char *n)
    {
      name = n;
      desc = "";
      type = "";
      value = NULL;
      def = 0;
    }
  Parameter(const char *n, T *v)
    {
      name = n;
      desc = "";
      type = "";
      setval(v);
      def = 1;
    }
  Parameter(const char *n, T *v, T t)
    {
      name = n;
      desc = "";
      type = "";
      setval(v);
      setval(t);
      def = 1;
    }
  Parameter(const char *n, T *v, const char *_desc, const char *_type)
  {
    name = n;
    desc = _desc;
    type = _type;
    setval(v);
    def = 1;
  }

  Parameter(const char *n, T t)
    {
      name = n;
      desc = "";
      type = "";
      setval(&vl);
      setval(t);
      def = 1;
    }

  ~Parameter()
    {
    }

  T val() const
    {
      if(value != NULL) { return *value; }
      else { return vl; }
    }

  void setval(T *theval)
    {
      value = theval;
    }

  void setval(T theval)
    {
      def = 1;
      if(value == NULL)
	{
	  vl = theval;
	  return;
	}
      *value = theval;
    }

  T * getval(void) { return value; }

  bool defined(void)
    {
      return def;
    }

  void normalize_strings(void)
  {
   
  }
};

template<class T>
class Flag : public Parameter<T>
{
 private:
  T tag;

 public:
  Flag(char *n, T *v, T t)
    {
      this->name = n;
      this->value = v;
      tag = t;
    }

  Flag(ostring n, T *v, T t)
    {
      this->name = n;
      this->value = v;
      tag = t;
    }

  Flag(char *n, T *v, T t, char *_desc)
    {
      this->name = n;
      this->value = v;
      this->desc = _desc;
      tag = t;
    }

  Flag(ostring n, T *v, T t, ostring _desc)
    {
      this->name = n;
      this->value = v;
      this->desc = _desc;
      tag = t;
    }

  void settag(void)
    {
      *(this->value) = tag;
    }

  inline bool is_set(void) 
  { 
    if((*(this->value)) == tag) { return true; }
    return false; 
  }

  T gettag(void) { return tag; }
};
#endif
