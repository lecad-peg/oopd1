#ifndef NDARRAY
#define NDARRAY

#include<stdarg.h>
#include<stdio.h>
#include<vector>
using namespace std;

template<class T>

class ndarray
{

private:
  int N;  // dimension #
  int tot_size;
  int *size;
  int *offset;
  bool dataalloc;

public:
  T *data;

  ndarray()
  {
    set_defaults();
    build_index(0, 0);
  }

  ndarray(int M, int *sizes)
  {
    set_defaults();
    build_index(M, sizes);

    data = new T[offset[0]];
    dataalloc = true;
  }

  ndarray(int M, int *sizes, T *d)
  {
    set_defaults();
    build_index(M, sizes);
    data = d;
  }

  ndarray(int M, int d1, ...)
  {
    int i;
    int *sizetmp;
    va_list argp;

    set_defaults();
    sizetmp = new int[M];

    va_start(argp, d1);
    sizetmp[0] = d1;

    for(i=1; i < M; i++)
      {
	sizetmp[i] = va_arg(argp, int);
      }

    build_index(M, sizetmp);
    alloc();

    va_end(argp);
    delete [] sizetmp;
  }

  ~ndarray()
  {
    if(size) { delete [] size; }
    delete [] offset;
    dealloc();
  }

  void set_defaults(void)
  {
    size = 0;
    offset = 0;
    data = 0;
    dataalloc = false;
  }

  void build_index(int M, int *sizes)
  {
    int i, j;

    // delete pointers if not NULL
    if(size)   { delete [] size; }
    if(offset) { delete [] offset; }

    N = M;
    if(N <= 0)
      {
	N = 0;
	size = 0;
	offset = new int[1];
	offset[0] = 0;
	return;
      }
    size = new int[N];
    offset = new int[N];
    for(i=0, offset[0] = 1; i < N; i++)
      {
	j = i+1;
	if(j == N) { j = 0; }
	size[i] = sizes[i];
	offset[j] = offset[i]*sizes[i];
      }
  }

  void build_index(int M, int d1, ...)
  {
    int i;
    int *sizetmp;
    va_list argp;

    sizetmp = new int[M];

    va_start(argp, d1);
    sizetmp[0] = d1;

    for(i=1; i < M; i++)
      {
	sizetmp[i] = va_arg(argp, int);
      }

    build_index(M, sizetmp);

    va_end(argp);
    delete [] sizetmp;
  }

  void alloc(void)
  {
    if(data && (!dataalloc)) { return; }

    if(offset[0])
      {
	dealloc();
	data = new T[offset[0]];
	dataalloc = true;
      }
  }

  void alloc(int M, int *sizes)
  {
    build_index(M, sizes);
    alloc();
  }

  void dealloc(void)
  {
    if(dataalloc)
      {
	if(data) { delete [] data; }
      }
  }

  inline int get_size(int i) { return size[i]; }

  inline int arraysize(void)
  {
    return offset[0];
  }

  inline int index(int *indices)
  {
    int i;
    int val = indices[0];
    for(i=1; i < N; i++)
      {
	val += offset[i]*indices[i];
      }
    return val;
  }

  //  inline T operator () (int *i)
  //  {
  //    return data[this->index(i)];
  //  }

  inline T & val(int *i) {     return data[this->index(i)]; }
  inline T & operator () (int *i)
  {
    return data[this->index(i)];
  }

  /* copy an array by pointer */
  friend void swap(ndarray<T> & a1, ndarray<T> & a2)
  {
    T *temp;
    temp = a1.data;
    a1.data = a2.data;
    a2.data = temp;

  }

  void minmax(T & max, T &min)
  {
    max = min = data[0];

    for(int i=1; i < offset[0]; i++)
      {
	if(data[i] < min) { min = data[i]; }
	if(data[i] > max) { max = data[i]; }
      }
  }

  ndarray<T> & operator = (const T a)
  {
    for(int i=0; i < offset[0]; i++)
      {
	data[i] = a;
      }
    return *this;
  }

  void multiply_row(int dim, int ind, const T a)
  {
    if(N <= 0) { return; }

    multiply_row(N-1, dim, ind, a);
  }

  void multiply_row(int d1, int d2, int ind, const T a)
  {
    int k;
    static vector<int> indices(N);

    if(indices.size() !=  N)
      {
	indices.resize(N);
      }
    indices[d2] = ind;

    if(d1 == d2)
      {
	multiply_row(d1-1, d2, ind, a);

	if(d1 == 0)
	  {
	    k = index(&indices[0]);
	    data[k] *= a;
	  }
      }
    else
      {
	for(int i=0; i < size[d1]; i++)
	  {
	    indices[d1] = i;

	    if(d1 == 0)
	      {
		k = index(&indices[0]);
		data[k] *= a;
	      }
	    else
	      {
		multiply_row(d1-1, d2, ind, a);
	      }

	  }
      }
  }

  void print(FILE *fp=stdout, int dim=-1)
  {
    static vector<int> indices;

    if(dim < 0) { dim = N-1; }

    T value;

    if(indices.size() != N)
      {
	indices.resize(N);
      }

    for(int i=0; i < size[dim]; i++)
      {
	indices[dim] = i;

	if(dim == 0)
	  {
	    value = val(&indices[0]);
	    for(int j=0; j < indices.size(); j++)
	      {
		fprintf(fp, "%d ", indices[j]);
	      }
	    fprintf(fp, "%g\n", value);
	  }
	else
	  {
	    print(fp, dim-1);
	  }
      }

    if(dim == 0)
      {
	fprintf(fp, "\n");
      }
  }


};

#endif
