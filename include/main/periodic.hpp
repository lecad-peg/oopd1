// sknam: For periodic BC (04/2007)
#ifndef PERIODIC_H
#define PERIODIC_H

#include "main/parse.hpp"
#include "main/oopiclist.hpp"
#include "main/boundary.hpp"

// forward class declarations
class TriMatrix;
class Fields;
class PoissonSolver;


class Periodic : public Boundary
{
 private:

 public:
  Periodic(Fields *t, int i); //: Boundary(t)
  Periodic(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
	    oopicList<Parameter<int> > int_variables, Fields *t);
  ~Periodic();

  Property particle_BC(ParticleGroup *p, int i, Scalar frac, Direction j);


  // functions for interaction with Poisson field
  void modify_coeffs(PoissonSolver * theps);


  // functions inherited from Parse
  void setdefaults();
  void setparamgroup();
  void check();
  void init();
  Parse * special(ostring s);

  void check_periodic();
  void init_periodic();
//  Parse * special_periodic(ostring s);

 };

#endif
