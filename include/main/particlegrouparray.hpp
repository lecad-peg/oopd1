#ifndef PARTICLEGROUPARRAY_H
#define PARTICLEGROUPARRAY_H

//********************************************************
// particlegrouparray.hpp
//
// Provides a particle group based on separate arrays
//
// Revision history
//
// (JohnV 17Jun2003) Original code.
// (JohnV 08Jul2003) Completed push code.
// (JohnV 14Jul2003) Added E array storage.
//
//********************************************************
#include "utils/ovector.hpp"
#include "main/particlegroup.hpp"
#include "main/species.hpp"
class ParticleGroupArray : public ParticleGroup
{
	Scalar *x; // positions array in grid units
	// What is this for ?
	Scalar *x2;
	Scalar *v[3]; // velocity array normalized by dx/dt
	Scalar *v1, *v2, *v3; // velocity components for fast access
	Scalar *w; // weight array
	Scalar *E[3]; // electric field at particle location
	Scalar *E1, *E2, *E3; // electric field components for fast access
	Scalar *frac; // fractional time-step
	Direction *dir; // direction

	int narrays;  // # of arrays being used
	Scalar **arrays; // index of all arrays;

	void save_into_array(int N, Scalar *X, Scalar *V1, Scalar *V2,
			Scalar *V3, bool vnorm, Scalar *W);
	void save_into_array(int N, Scalar W, Scalar *X, Vector3 *V,
			bool vnorm);

public:
	ParticleGroupArray(int nmax, Species *theSpecies, Fields *theFields,
			Scalar np2c0, bool variableWeight=false,
			bool frac_time=false, bool directional=false);
	~ParticleGroupArray();

	int get_particlegrouptype() { return PARTICLEGROUPARRAYTYPE; }

	void allocate_arrays(Species *theSpecies,
			Fields *theFields, bool fractime,
			bool directional);

	// mindgame: update_field is the flag to determine whether the field
	//          at particle position (not Poisson equation itself) needs
	//          to be calculated or not.
	//           This flag should be true when particles are added
	//          before (not after) advance_x/v()
	//          since the field might not be assigned on the particle position.
	//           vnorm is the flag to determined whether
	//          normalization for velocity is required inside.
	int add_particle(Scalar X, Scalar V1, Scalar V2=0.,
			Scalar V3=0., bool vnorm=true, bool update_field=false,
			Scalar W=-1.);
	int add_particles(int N, Scalar *X, Scalar *V1=NULL, Scalar *V2=NULL,
			Scalar *V3=NULL, bool vnorm=true, bool update_field=false,
			Scalar *W=NULL);
	int add_particles(int N, Scalar W, Scalar *X, Vector3 *V=NULL,
			bool vnorm=true, bool update_field=false);
	int add_particles(int N, Scalar W, Scalar *X, Scalar *dX, Vector3 *V=NULL,
			bool vnorm=true, bool update_field=false);

	void delete_particle(int i);

	void realloc(int i); // reallocate memory for i particles

	void accumulate_density(); // accumulates the species density

	// add_v: adds a to the existing v[i] for the E-push
	void add_v(int i, Scalar a) {v1[i] += a;}
	void add_v(int i, Scalar *a);
	void add_v(int i, Vector3 a);

	// v_updateB3(): rotate v for general magnetic field.
	// t1, t2 are the normalized field components
	// itmag1 = 1/(1+t dot t);
	void rotate_v(int i, Scalar t1, Scalar t2, Scalar itmag1);

	// advance_x():
	// updates the particle positions using the velocities. Recall that x is in
	// grid units, and v is in dx/dt units.
	void advance_x(Scalar dt);

	void advance_v(Scalar ddt);
	// mindgame: for single particle
	void advance_v(int i, Scalar ddt, Vector3 E_sp, Vector3 B_sp=0);

	void advance_v1D(Scalar ddt); //advance v and accumulate 1D flux and energy,  MAL 6/19/10
	// mindgame: for single particle
	void advance_v1D(int i, Scalar ddt, Vector3 E_sp, Vector3 B_sp=0);

	void advance_v3D(Scalar ddt); //advance v and accumulate 3D fluxes and energies, MAL 6/19/10
	// mindgame: for single particle
	void advance_v3D(int i, Scalar ddt, Vector3 E_sp, Vector3 B_sp=0);

	// accessor functions
	Scalar get_x(int i) const {return x[i];}
	Scalar get_v1(int i) const {return v1[i];}
	Scalar get_frac(int i) const { return frac[i]; }
	Direction get_direction(int i) const { return dir[i]; }
	Scalar get_q(int i) const { // return appropriate charge for the ith particle
		if (is_variableWeight()) return w[i]*q0;
		else return w0*q0;
	}
	Vector3 get_v(int i) const;
	void set_v(int i, Vector3 v);

	Vector3 get_e(int i);

	inline Scalar * get_v_ptr(int i) const { return v[i]; }
	inline Scalar * get_x_ptr()  const { return x;    }

	Scalar get_m(int i) const { // return the mass for the ith particle
		if (is_variableWeight()) return w[i]*m0;
		else return w0*m0;
	}
	Scalar get_w(int i) const // return weight of the ith particle
	{
		if(is_variableWeight()) { return w[i]; }
		return w0;
	}

	// get electric field
	void get_E(int i, Scalar & e1, Scalar & e2, Scalar & e3);
	void get_E(int i, Vector3 & e);

	Scalar get_E(int i) {return E1[i];}

	// accumulate electric field
	void add_E(int i, Scalar e1, Scalar e2, Scalar e3);

	void set_E(int i, Vector3 &E); // set electric field
	inline void set_E(int i, Scalar &E) {E1[i] = E;}; // set electric field
	void reset_E();
	void Gather_E();

	void set_frac(int i, Scalar _frac) { frac[i] = _frac; }
	void set_direction(int i, Direction _dir) { dir[i] = _dir; }

	void average_velocity(Vector3 &val, Scalar &www);
	Scalar max_position();
	Vector3 max_velocity();
	Vector3 front_velocity();

	void advance_vload(Scalar ddt);
	void advance_vload_2nd(Scalar ddt);

	// why is this here? JH, Oct. 14, 2005
	Scalar get_determination(Scalar a11,Scalar a12, Scalar a13, Scalar a21,
			Scalar a22,Scalar a23, Scalar a31, Scalar a32,
			Scalar a33);

	void copy_x(Scalar *xcp, int ns, int num=-1); // copies num values of x
	void copy_v(Scalar *vcp, int ns, int dim, int num=-1); // copies num v_i

	//sknam: set x for the particle passing the periodic boundary
	void set_x(int i, Scalar xnew);

#define PGARRAY_PART
#include "main/conv_engy_vel.hpp"
#undef PGARRAY_PART

};

#endif
