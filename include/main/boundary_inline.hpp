inline void Boundary::delete_particle(ParticleGroup *p, int i)
{
  p->delete_particle(i);
}
  
inline void Boundary::absorb(ParticleGroup *p, int i, Direction dir)
{
  weights[dir][p->get_species()->get_ident()] += p->get_w(i);
  delete_particle(p, i);
}

inline void Boundary::accumulate_particle(ParticleGroup *p, int i, 
					  Scalar frac, Direction dir,
					  bool advance_frac)
{
  int grp=p->get_species()->get_ident();
  int index; 
  particles[grp]->add_particle(p, i, false);
  index = particles[grp]->get_n()-1;
  if (advance_frac)
  {
    // mindgame: for the transparent boundary
    Scalar frac2 = (this->*advance_v_for_synchro)(particles[grp], index, frac, dir);
    particles[grp]->set_frac(index, frac2);
  }
  else
  {
    particles[grp]->set_frac(index, frac);
  }
  particles[grp]->set_direction(index, dir); 
}

inline Scalar Boundary::get_r0() const { return thegrid->getX(j0); }
inline Scalar Boundary::get_r1() const { return thegrid->getX(j1); }

inline Scalar Boundary::get_A(Direction dir) const
{
  if(dir == NEGATIVE)
    {
      return thegrid->get_area(j0);
    }
  return thegrid->get_area(j1);
}
