#ifndef PLASMASOURCE_H
#define PLASMASOURCE_H

#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "main/load.hpp"

class Grid;

class PlasmaSource: public Load
{
 private:
  Scalar rate; // max rate to load (m^-3*sec^-1)

 public:
  PlasmaSource(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
       oopicList<Parameter<int> > int_variables, Grid *t,
       SpeciesList *thespecieslist);
  ~PlasmaSource();

  // functions inherited from Parse via Load
  void setdefaults();
  void setparamgroup();
  // void check(); uses base class check()
  void init();
  void load(Scalar t, Scalar dt); // load the particles
};

#endif
