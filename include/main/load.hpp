#ifndef LOAD_H
#define LOAD_H


#include "utils/ovector.hpp"
#include "main/parse.hpp"

class Grid;
class Species;
class Fields;
class XDistribution;
class VDistribution;

class Load : public Parse, public Message
{
 protected:
  Scalar n; // density to load (max density for variable load)
  Scalar x0, x1; // starting, endpoints for load
  Scalar np2c; // number of real particles per computational particles
  Scalar T_load;
  Scalar vt_x;
  Scalar vt_y;
  Scalar vt_z;
  Scalar vcu_x;
  Scalar vcu_y;
  Scalar vcu_z;
  Scalar vcl_x;
  Scalar vcl_y;
  Scalar vcl_z;
  Scalar v0_x;
  Scalar v0_y;
  Scalar v0_z;
  Vector3 vt;
  Vector3 v0;
  Vector3 vcl;
  Vector3 vcu;
  Species *species; // Species to load
  SpeciesList *specieslist; // list of all species in SpatialRegion
  ostring speciesname; // temporary holding name for species
  Grid *thegrid;
  int vload_type;
  int xload_type;
  Equation density_func;
  int np;
  XDistribution *xdist;
  VDistribution *vdist;

 public:
  Load(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
       oopicList<Parameter<int> > int_variables, Grid *t,
       SpeciesList *thespecieslist);

  Load(){}; // constructor called by derived classes

  ~Load();

  // functions inherited from Parse
  virtual void setdefaults();
  virtual void setparamgroup();
  virtual void check();
  virtual void init();
  void init_dist();
	inline Species * get_loadspecies() const { return species; } // for dumpfile, MAL 5/14/10
  virtual void load(Scalar dt); // load the particles
	virtual void reload(Scalar dt, int is3v, int npart, FILE * DMPFile); // reload the dumped particles, MAL 5/14/10
};

#endif
