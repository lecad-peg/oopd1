#ifndef DIELECTRIC_H
#define DIELECTRIC_H

#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/oopiclist.hpp"
#include "main/param.hpp"
#include "main/boundary.hpp"

// forward declarations
class TriMatrix;
class PoissonSolver;

class Dielectric : public Boundary
{
 private:
  Scalar *rho_old;
  Scalar diel_epsilon_r;
  Scalar diel_epsilon;

  Scalar D0, E0;
 public:
  Dielectric(Fields *t, int i); //: Boundary(t)
  Dielectric(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
	    oopicList<Parameter<int> > int_variables, Fields *t);
  ~Dielectric();

  Property particle_BC(ParticleGroup *p, int i, Scalar frac, Direction dir);

  inline Scalar get_epsilon() {return diel_epsilon;}

  void modify_rho(Scalar *rho,Scalar *space_rho);
  void restore_rho(Scalar *rho);
  void modify_coeffs(PoissonSolver * theps);

  // functions inherited from Parse
  void setdefaults();
  void setparamgroup();
  void check();
  void init();
  Parse * special(ostring s);

  void check_dielectric();
  void init_dielectric();
 };

#endif
