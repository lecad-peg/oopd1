// move for uniform grids only


#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/oopiclist.hpp"

#ifdef NEGATIVE_MOVE
#define COMPARE >=
#define INCREMENT --
#define CELLADJ
#define DIRECTION POSITIVE
#else
#define COMPARE <=
#define INCREMENT ++
#define CELLADJ +1
#define DIRECTION NEGATIVE
#endif

int cell = int(x) CELLADJ ;


//Solve this by giving the offset to the initial position of injection
// mindgame: to prevent all particles injected at the right boundary from being
//          wrongly absorbed at that position.
//#ifdef NEGATIVE_MOVE
//if (cell == x) cell--;
//#endif
oopicListIter<Boundary> thej;
bool absorbed_done = false;

x += dx;

// mindgame:
//  There is an assumption that particles are not already inside the boundary.
//  Be sure that initially loaded particles are not inside the boundary.
//
while(cell COMPARE x)
{
  if(!(boundary_list[cell]->isEmpty()))
  {
    for(thej.restart(*boundary_list[cell]); !thej.Done(); thej++)
    {
      if(thej()->particle_BC(p, i, fabs((Scalar(cell) - x)/dx), DIRECTION)
	 == ABSORBED)
      {
	absorbed_done = true;
      }
    }
    if (absorbed_done) return;
  }
  cell INCREMENT;
}

i++;

#undef COMPARE
#undef INCREMENT
#undef CELLADJ
#undef DIRECTION
