inline Scalar Drive::phase(Scalar t)
{
// printf("\nDrive::phase(), t=%g, dt=%g, theta0(init)=%g",t,dt,theta0); // debug dumpfile, MAL 5/22/10
 if(t < chirpstart)
   {
   theta0 += dt*w0;
   }
  else
    { 
      if(t > chirpend)
	{
	  theta0 += dt*wend;
	}
      else
	{
	  theta0 += dt*(w0 - chirpstart*wt) + wt*(dt*t + 0.5*dt*dt);
	}
    }
// printf(", theta0(final)=%g\n",theta0); // debug dumpfile, MAL 5/22/10
  return theta0;
}

inline Scalar Drive::getphi(void)  // returns original phase
{
  return phi;
}

inline Scalar Drive::value(Scalar t)
{
 Scalar strength;
 strength = DC + AC*sin(phase(t)) + timefunc.eval(t);
// printf("\nDrive::value(t), t=%g",t);
 return strength;
// return DC + AC*sin(phase(t))+ timefunc.eval(t);
}
