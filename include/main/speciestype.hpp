#ifndef SPECIESTYPE
#define SPECIESTYPE

enum Species_type 
{
  //electrons
  E, sec_E, 
  //ions: for the moment it simply means a cation (of order one: X+)
  Ar_ions, sec_Ar_ions, He_ions, sec_He_ions,  H_ions, sec_H_ions, CH4_ions, sec_CH4_ions, CH3_ions, sec_CH3_ions, CH2_ions, sec_CH2_ions, CH_ions, sec_CH_ions, C_ions, sec_C_ions, Air_ions, sec_Air_ions, 
  // neutrals: atoms and methan derivatives.
  Ar, sec_Ar, He, sec_He, H, sec_H, H2, sec_H2, CH4, sec_CH4, CH3, sec_CH3, CH2, sec_CH2, CH, sec_CH, C, sec_C, Ne, sec_Ne, Air, 
  // used to categorize species in mcc
  Fake_species,  sec_especies, sec_ispecies, sec_iminusHspecies, sec_Hspecies, sec_H2species, sec_nminusHspecies, sec_nminusH2species, sec_nminusH3species, 
  ELSE
}; 

#endif
