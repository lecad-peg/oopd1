
//********************************************************
// radiation.hpp
//
// Revision history
//
// 
//
//********************************************************

#ifndef RADIATION_H
#define RADIATION_H

#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "fields/fields.hpp"
#include "utils/message.hpp"


// forward declarations
class Species;
class ParticleGroup;
class DataArray;

const Scalar NA = 6.022E23;

class Radiation : public Parse 
{

  int photon_energy_number;
  int position_max;

  Scalar energy_criteria;
  Scalar energy_fraction;

  Scalar photon_energy_max;
  Scalar photon_energy_min;  Scalar photon_energy_interval;
  Scalar solid_angle;

  Scalar numberofatom, angle, density, azi_angle;
  Scalar azi, mott_angle;
  ostring target_name;
  ostring target_file;
  ostring impact_species;  
  Species * iSpecies;
  Scalar atomic_weight;
  Scalar atomic_number;
  DataArray * radiation;
  DataArray * photon_energy; 
  Scalar * absorption;
  Scalar * abs_mat;
  

 
  SpeciesList *specieslist;
  DiagList tds;

public:
  
  Radiation() {}; 
  Radiation (FILE *fp, oopicList<Parameter<Scalar> > float_variables,
	     oopicList<Parameter<int> > int_variables, Fields *tf);
  ~Radiation();
   
  void scatteringangle(Scalar energy);
  void radiate(ParticleGroup *p, int i);
  Scalar brem_rad(Scalar energy, Scalar photon, Scalar angle_theta);  
  Scalar energyreduction(Scalar &energy);
  Scalar mean_free(Scalar energy); 
  Scalar get_theta(Scalar x_dir, Scalar y_dir);
  void get_direction(Scalar& x_dir, Scalar& y_dir, Scalar& z_dir);
  Scalar absorb(Scalar energy);
  Scalar absorb_fromfile(Scalar energy);
  void showMatrix(Scalar x[], int Size);
  void read_fromfile(void);
  
  //functions inherited from parse
  virtual void setdefaults(void);
  virtual void setparamgroup(void);
  virtual void check(void);
  virtual void init(void);

  virtual void init_diagnostics(void);
};

#endif

