#ifndef DRIVE
#define DRIVE

#include "main/oopiclist.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/param.hpp"

class Drive : public Parse
{
	private:
		Scalar phi, w0, DC, AC;
		Scalar chirpstart, chirpend; // time when chirping the freq. starts and ends
		Scalar wt, wend;
		Scalar dt; // should be set from SpatialRegion
		Scalar theta0;
		Equation timefunc;

	public:
		bool iflag, vflag, dcbiasflag;

		Drive();
		Drive(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
				oopicList<Parameter<int> > int_variables);

		// functions inheritted from Parse
		void setdefaults(void);
		void setparamgroup(void);
		void check(void);
		void init(void);

		inline void set_dt(Scalar dtval) { dt = dtval; theta0 -= w0*dt; }

		// updates the phase, phi
		inline Scalar phase(Scalar t);
		inline Scalar getphase(void) { return theta0; }
		inline Scalar getphi(void);  // returns original phase
		inline Scalar getDC(void) { return DC; }
		inline Scalar getAC(void) { return AC; } // HH 03/04/16
		inline bool get_vflag(void) { return vflag; } // HH 03/04/16
		inline bool get_dcbiasflag(void) { return dcbiasflag; } // HH 05/18/16
		inline Scalar value(Scalar t);
		inline Scalar getw0(void) { return w0; } // for dumpfile, MAL 5/22/10
		inline void setphase(Scalar _theta0) { theta0 = _theta0; } // for dumpfile, MAL 5/22/10

		inline void set_DC(Scalar _DC){DC = _DC;} // HH 05/08/16

		void print(FILE *fp = stdout, Scalar t=0);
};

#endif
