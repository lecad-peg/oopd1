#ifndef _ostack_h
#define _ostack_h
#include <cstddef>
/*
====================================================================

oopicStack.h

1.00 (JohnV 31May2004) Original code (rewrite)

====================================================================
*/

template <class Type> class oopicStackItem
{
  Type data;
  oopicStackItem<Type> *next;
 public:
  oopicStackItem(Type _data, oopicStackItem<Type> *_next) {
    data = _data;
    next = _next;
  }
  Type get_data(void) {return data;}
  oopicStackItem<Type>* get_next(void) {return next;}
};

template <class Type> class oopicStack
{
 protected:
  oopicStackItem<Type> *head;
  oopicStackItem<Type> *tmp;
  Type tmpData;
 public:
  oopicStack(void) {head = NULL;}
  ~oopicStack(void) {removeAll();}
  void removeAll(void) {while(head) pop();}
  void push(Type data) {head = new oopicStackItem<Type>(data, head);}
  Type pop(void) {
    tmp = head;
    head = head->get_next();
    tmpData = tmp->get_data();
    delete tmp;
    return tmpData;
  }
  bool isEmpty(void) {return !(bool)head;}
};

#endif
