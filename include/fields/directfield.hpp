#ifndef DIRECTFIELD
#define DIRECTFIELD

//********************************************************
// directfield.hpp
//
// Revision history
//
// (JohnV 02Jun2004) Original code.
// (JohnV 26Jun2004) Mods for BaseClass_Direct_Field
//
//********************************************************

#include "fields/Base_Class_Direct_Field.hpp"

// SortElement provides a pointer to a single particle identified by
// ParticleGroup and index within that group. Also provides a key for
// sorting.

class DirectSolver :  public Base_Class_Direct_Field
{
 public:
  Scalar *E_p; // array for E by particle index according to sortKeys
  Scalar E_r; // E at the right wall
  Scalar E_l; // E at the left wall

  // flag for whether E diagnostic is viewed (must be int for Xgrafix)
  int E_shown; 

  // partitioning func for qsort
  int partition(SortElement *data, int l, int r); 

  void qsort(void); // non-recursive quicksort sortKeys by position x  
  
  DirectSolver(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables, SpatialRegion *sr);
  ~DirectSolver(void);
  bool is_E_shown() {return (bool) E_shown;}
  virtual void update_fields(void); // sort particles

  // compute E at particles and write to particles
  virtual void compute_E(void); 

  // functions from Parse
  virtual void setdefaults(void);
  virtual void setparamgroup(void) {Base_Class_Direct_Field::setparamgroup();}
  virtual void check(void){Base_Class_Direct_Field::check();}
  virtual void init(void);
  
};

#endif // DIRECTFIELD
