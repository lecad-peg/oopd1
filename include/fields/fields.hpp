//********************************************************
// fields.hpp
//
// Revision history
//
// (JohnV 16Jun2003) Original code.
//
//********************************************************
#ifndef FIELDS_H
#define FIELDS_H

#include "main/parse.hpp"

// forward declarations
class SpatialRegion;
class Species;
class Grid;
class Conductor;
class Dielectric;
class Equation;

class Fields : public Parse, public Message
{
protected:
	Grid *theGrid; // pointer to the Grid object
	BoundaryList *boundarylist;   // pointer to list of boundaries
	oopicList<Conductor> *conductorlist; // pointer to list of conductors
	oopicList<Dielectric> *dielectriclist; // pointer to list of dielectrics
	SpeciesList *theSpeciesList; // pointer to the list of species
	SpatialRegion *theregion; // pointer to the spatial region
	Scalar *rho; // net charge density on the grid
	Scalar *space_rho; // net volume charge density excluding surface charge, change -> charge, MAL 6/16/09
	Scalar *space_rho_neg; // volume negative charge density excluding surface charge, for sheathwidth diagnostic, MAL 6/23/09
	Scalar *phi_temp; // pointer to temporary time-average potential, MAL 6/13/09
	Scalar *rho_temp; // pointer to temporary time-average space charge density, MAL 6/13/09
	Scalar *phi_ave; // pointer to cumulative time-average potential, MAL 6/13/09
	Scalar *rho_ave; // pointer to cumulative time-average space charge density, MAL 6/13/09
	Equation rhoback; // background charge density
	Equation efield2; // electric field in y direction
	Equation efield3; // electric field in z direction
	Equation efield1; // DQW added 08/01/2018 to consider the Ex in multi-pactor process same as efield2 and efield3
	Scalar epsilon, epsilonr; // permitivity in absolute and relative terms;
	Scalar * eps; // cell-based permitivity
	bool eps_alloc; // flag if eps is allocated
	bool *field_continuity;

	// variables for electrostatic potential (phi)
	// should these be moved to PoissonSolver? JH, June 20, 2006
	Scalar *phi; // pointer to the potential on the grid
	Scalar *fields_phi_array; // Spatiotemporal diagnostics, HH 04/19/16
	Scalar *fields_E_array; // Spatiotemporal diagnostics, HH 05/13/16
	Scalar ref_pot; // reference potential

	Vector3 *E; // electric field on the grid
	Vector3 *B; // magnetic field on the grid
	bool transverseE; // flag for transverse E fields imposed
	bool magnetized; // flag = false if B = 0 everywhere
	bool constB; // flag = true if B is constant
	bool const_epsilon; // flag = true if permivity is constant
	int ng;  // number of grid points
	int e_dim; // number of dimensions for E

	// true for gridded field solves, false for gridless (skips Gather_E)
	bool gridded;

	bool E_shown; // determines whether E diagnostic is needed

	// magnetic field
	Equation *Bxeq;
	Equation *Byeq;
	Equation *Bzeq;

	// added variables for grid time averages,  MAL 6/14/09
	int naverage; // number of timesteps over which to average
	long int nsteps; // number of timesteps taken
	int nstepsmod; // number of timesteps taken modulo naverage
	Scalar avecoef; // averaging coefficient used to accumulate grid time averages

	// added variables for sheath width diagnostics, MAL 6/28/09
	Scalar Lparam, Rparam; // n-/n+ for left and right hand sheath diagnostics
	int k0; // number of successive true tests required for sheath condition to be satisfied
	Scalar scoef[2]; // rho/(-rhoneg) for left and right hand sheath diagnostics
	Scalar sheathwidth[2]; // sheath widths at grid [0] and grid [ng]
	// added for extrapolating sheath position to the next timestep
	int previoussheathj[2]; // previous grid j for sheaths at grid [0] and grid [ng]
	int currentsheathj[2]; // current grid j for sheaths at grid [0] and grid [ng]

	// variables added for control of species gridded diagnostics, MAL 8/28/09 and 6/26/09
	bool flux1, flux2, flux3, J1dotE1, J2dotE2, J3dotE3, enden1, enden2, enden3;
	bool vel1, vel2, vel3, JdotE, enden, T1, T2, T3, Tperp, Temp; //true if the corresponding diagnostic should be calculated and displayed, MAL 9/22/09
	int ngdu; // update grid diagnostics calculations every ngdu timesteps
	bool f1flag, f2flag, f3flag, e1flag, e2flag, e3flag; // true if flux1,2,3 or enden1,2,3 should be calculated for some diagnostic, MAL 9/22/09
	bool d0flag, d1flag, d3flag; // determines whether WeightLinear, WeightLinear1D, or WeightLinear3D is called by particlegrouparray
	// depending on which diagnostics are wanted; one and only one of the three flags is true, MAL 9/22/09
	bool nodiagupdate; // true if xgraphix diagnostics windows should NOT be updated, MAL 9/22/09

	// added variables and functions for inductively coupled plasma (icp) model, MAL 1/7/11
	bool icp2; // use the icp model to set the transverse electric field E[2] for a given sinusoidal surface current density K2 [A/m]
	bool icp3; // use the icp model to set the transverse electric field E[3] for a given sinusoidal surface current density K3 [A/m]
	Scalar K2; // amplitude of 2-component of sinusoidal surface current density [A/m]
	Scalar K3; // amplitude of 3-component of sinusoidal surface current density [A/m]
	Scalar f2; // frequency [Hz] for K2
	Scalar f3; // frequency [Hz] for K3
	Scalar phi2; // initial phase [degrees] for K2
	Scalar phi3; // initial phase [degrees] for K3
	Scalar wavespeed; // speed of "light" for icp model; default is SPEED_OF_LIGHT
	int nupdateE; // update the icp fields every nupdateE steps
	Scalar * Er2; // grid array; real part of the complex amplitude of E2
	Scalar * Ei2; // grid array; imaginary part of the complex amplitude of E2
	Scalar * Er3; // grid array; real part of the complex amplitude of E3
	Scalar * Ei3; // grid array; imaginary part of the complex amplitude of E3
	// calculate the complex amplitude of transverse E for the given surface current (and electron density ne and collision frequency num)
	// called every nupdateE steps
	//DQW 07/23/2018 add ExEzSwitch to control whether open Ex in multipactor. 
	bool ExEzSwitch;

	void update_icp_fields(Scalar * _Er, Scalar * _Ei, Scalar & _K, Scalar & _f);

public:
	// constructors
	Fields(SpatialRegion *sr);
	Fields(Grid *_theGrid, SpeciesList *_theSpeciesList,
		   SpatialRegion *sr);
	// destructor
	~Fields();

	// accessor functions
	inline  bool is_B_constant() { return constB; }
	inline Grid* get_theGrid()  { return theGrid; }
	inline int get_ng() {return ng;}
	inline  bool is_magnetized() { return magnetized; }
	inline  bool is_transverseE(){ return transverseE; }
	inline  Scalar get_epsilon(int i=0);
	inline Scalar * get_rho() { return rho; }
	inline Scalar get_phi(int i) { return phi[i]; }
	inline Scalar * get_phi() { return phi; }
	inline Scalar * get_phiAve() {return phi_ave;} // HH 02/19/16
	Scalar get_dt();
	Scalar get_t();
	inline long int get_nsteps() { return nsteps; }
	inline void set_nsteps(long int _nsteps) { nsteps = _nsteps; } // MAL 9/25/09
	inline int get_naverage() { return naverage; } // MAL 8/28/09
	inline void set_naverage(int _naverage) { naverage = _naverage; } // MAL 9/25/09
	inline int get_nupdateE() { return nupdateE; } //  for icp field calculation, MAL 1/11/11
	inline Scalar get_Lparam() { return Lparam; } // for left hand sheath diagnostic, MAL 6/25/09
	inline Scalar get_Rparam() { return Rparam; } // for right hand sheath diagnostic, MAL 6/25/09
	inline void set_scoef0(Scalar _scoef0) { scoef[0] = _scoef0; } // for left hand sheathwidth diagnostic, MAL 9/25/09
	inline void set_scoef1(Scalar _scoef1) { scoef[1] = _scoef1; } // for right hand sheathwidth diagnostic, MAL 9/25/09
	inline void set_k0(int _k0) { k0 = _k0; } // for sheathwidth diagnostic, MAL 9/25/09
	// added MAL 6/26/09
	inline bool is_flux1enabled() { return flux1; }
	inline bool is_flux2enabled() { return flux2; }
	inline bool is_flux3enabled() { return flux3; }
	inline bool is_J1dotE1enabled() { return J1dotE1; }
	inline bool is_J2dotE2enabled() { return J2dotE2; }
	inline bool is_J3dotE3enabled() { return J3dotE3; }
	inline bool is_enden1enabled() { return enden1; }
	inline bool is_enden2enabled() { return enden2; }
	inline bool is_enden3enabled() { return enden3; }
	inline bool is_vel1enabled() { return vel1; }
	inline bool is_vel2enabled() { return vel2; }
	inline bool is_vel3enabled() { return vel3; }
	inline bool is_T1enabled() { return T1; }
	inline bool is_T2enabled() { return T2; }
	inline bool is_T3enabled() { return T3; }
	inline bool is_Tperpenabled() { return Tperp; }
	inline bool is_Tempenabled() { return Temp; }
	inline bool is_JdotEenabled() { return JdotE; }
	inline bool is_endenenabled() { return enden; }
	inline void set_flux1() { flux1 = true; }
	inline void set_flux2() { flux2 = true; }
	inline void set_flux3() { flux3 = true; }
	inline void set_J1dotE1() { J1dotE1 = true; }
	inline void set_J2dotE2() { J2dotE2 = true; }
	inline void set_J3dotE3() { J3dotE3 = true; }
	inline void set_enden1() { enden1 = true; }
	inline void set_enden2() { enden2 = true; }
	inline void set_enden3() { enden3 = true; }
	inline void set_vel1() { vel1 = true; }
	inline void set_vel2() { vel2 = true; }
	inline void set_vel3() { vel3 = true; }
	inline void set_T1() { T1 = true; }
	inline void set_T2() { T2 = true; }
	inline void set_T3() { T3 = true; }
	inline void set_Tperp() { Tperp = true; }
	inline void set_Temp() { Temp = true; }
	inline void set_JdotE() { JdotE = true; }
	inline void set_enden() { enden = true; }
	inline int get_ngdu() { return ngdu; }
	inline void set_ngdu(int _ngdu) { ngdu = _ngdu; } // MAL 9/25/09
	inline bool get_nodiagupdate() { return nodiagupdate; } // true if diagnostics should NOT be updated, MAL 9/22/09
	inline bool get_diagupdate() {return !nodiagupdate; } // true if diagnostics should be updated, MAL 9/22/09
	inline Scalar *get_sheathwidths() { return sheathwidth; } // for sheathwidths diagnostic control, MAL 6/25/09
	inline Scalar *get_Lsheathwidth() { return &sheathwidth[0]; } // for left sheathwidth diagnostic control, MAL 6/25/09
	inline Scalar *get_Rsheathwidth() { return &sheathwidth[1]; } // for right sheathwidth diagnostic control, MAL 6/25/09
	// Add to control calculation of fluxes and energy densities needed for diagnostic calcuation and display, MAL 9/22/09
	inline bool get_f1flag() { return f1flag; }
	inline bool get_f2flag() { return f2flag; }
	inline bool get_f3flag() { return f3flag; }
	inline bool get_e1flag() { return e1flag; }
	inline bool get_e2flag() { return e2flag; }
	inline bool get_e3flag() { return e3flag; }
	inline void set_f1flag() { f1flag = true; }
	inline void set_f2flag() { f2flag = true; }
	inline void set_f3flag() { f3flag = true; }
	inline void set_e1flag() { e1flag = true; }
	inline void set_e2flag() { e2flag = true; }
	inline void set_e3flag() { e3flag = true; }
	// Add to control array accumulation in ParticleGroupArray::accumulate_density(), MAL 9/22/09
	inline bool get_d0flag() { return d0flag; }
	inline bool get_d1flag() { return d1flag; }
	inline bool get_d3flag() { return d3flag; }
	inline void set_d0flag(bool _d0) { d0flag = _d0; }
	inline void set_d1flag(bool _d1) { d1flag = _d1; }
	inline void set_d3flag(bool _d3) { d3flag = _d3; }

	inline bool is_gridded() {return gridded;}
	inline bool is_E_shown() {return E_shown;}

	inline bool is_field_continuous(int i) {return field_continuity[i];};

	inline int Edim() { if(is_transverseE()) { return 3; } return 1; }

	inline SpatialRegion* get_region() { return theregion; }
	inline SpeciesList * get_specieslist() { return theSpeciesList;}
	inline BoundaryList * get_boundarylist() { return boundarylist; }
	inline oopicList<Conductor> * get_conductorlist() { return conductorlist;}

	inline void get_E(Scalar x, Scalar & E1, Scalar & E2, Scalar &E3);
	inline void get_E(Scalar x, Scalar & E1);
	inline void get_E(Scalar x, Vector3 &E);
	inline void get_E(int i, Vector3 &E);
	void getE(int i, Vector3 &E); // needed for Species::accumulate_vel_moments(), MAL 7/11/10
	inline void get_B(Scalar x, Scalar & B1, Scalar & B2, Scalar & B3);
	inline void get_B(Scalar x, Scalar & B1, Scalar & B2, Scalar & B3,
			Scalar ttt);
	inline void get_B(Scalar *f, int dim = 3);
	inline void get_B(Scalar x, Vector3 &B);
	inline void get_B(int i, Vector3 &B);
	inline Scalar magB(Scalar x);
	inline Scalar magB();
	inline void get_Egradient(Scalar x,Vector3 &E);
	inline void get_Egradient_interior(Scalar x,Vector3 &E);
	inline void get_Egradient_plus(Scalar x,Vector3 &E);
	inline void get_Egradient_minus(Scalar x,Vector3 &E);
	inline void get_Egradient_minus(int i,Vector3 &E);
	inline void get_Bgradient(Scalar x,Vector3 &B);
	inline void get_Bgradient_interior(Scalar x,Vector3 &B);
	inline void get_Bgradient_plus(Scalar x,Vector3 &B);
	inline void get_Bgradient_minus(Scalar x,Vector3 &B);
	inline void get_Bgradient_minus(int i,Vector3 &B);
	inline void get_EBgradient(Scalar x,Vector3 &E, Vector3 &B);
	inline void get_EBgradient_plus(Scalar x,Vector3 &E, Vector3 &B);
	inline void get_EBgradient_minus(Scalar x,Vector3 &E, Vector3 &B);
	inline void get_EBgradient_interior(Scalar x,Vector3 &E, Vector3 &B);
	inline void get_EBgradient_plus(int i,Vector3 &E, Vector3 &B);
	inline void get_EBgradient_minus(int i,Vector3 &E, Vector3 &B);
	inline Vector3  get_B(Scalar x, Scalar t);
	inline void get_EB(Scalar, Vector3 &E, Vector3 &B);
	virtual void setconductors(oopicList<Conductor> *c);
	void modify_epsilon();
	virtual void setdielectrics(oopicList<Dielectric> *d);
	virtual void setmatrixcoef() { }
	virtual bool is_PoissonField() { return false; }

	void allocate_epsilon();

	// accumulate charge density to grid
	void accumulate_rho(); // for all Species

	// find sheathwidth for sheath diagnostic, MAL 6/18/09
	Scalar find_sheath_width(int n);

	// updates grid time averages of density, fluxes, energy density, J dot E's each timestep, MAL 6/14/09
	void grid_time_average(Scalar *garray, Scalar *tempgarray, Scalar *avegarray, int subcycle = 1); // add subcycle=1, MAL 9/10/12
	void grid_time_ave(Scalar * vave, Scalar * nave, Scalar * fave); // for time-average velocities v1, v2, and v3, MAL 9/11/10
	// add np2c to grid_time_ave for temperature calculations, MAL 10/13/12
	void grid_time_ave(Scalar m, Scalar np2c, Scalar * Tave, Scalar * nave, Scalar * fave, Scalar * eave); // for time-average temperatures T1, T2, and T3, MAL 9/11/10
	void grid_time_ave(Scalar m, Scalar np2c, Scalar * Tperpave, Scalar * nave, Scalar * f2ave, Scalar * e2ave, Scalar * f3ave, Scalar * e3ave); // for time-average perpendicular temp Tperp, MAL 9/11/10
	void grid_time_ave(Scalar m, Scalar np2c, Scalar * Tempave, Scalar * nave, Scalar * f1ave, Scalar * e1ave, Scalar * f2ave, Scalar * e2ave, Scalar * f3ave, Scalar * e3ave); // time ave total temp Temp, MAL 9/11/10


	// update_fields -- updates the current field
	virtual void update_fields() = 0;

	// reset E
	void reset_E();

	// front ends for virtual functions used in Parse
	// must be called from inherited classes
	void setdefaults_fields();
	void setparamgroup_fields();
	void check_fields();
	void init_fields();
	void get_Bfields();
	void print_E(FILE *fp=stdout);

	////////////////////////////////////////////////////////
	// functions for spatiotemporal diagnostics, HH 04/19/16
	////////////////////////////////////////////////////////

	void init_arrays(int size_array);
	void set_arrays(int ng, int steps_rf, int m);
	inline Scalar *get_phi_array() {return fields_phi_array;}
	inline Scalar *get_E_array() {return fields_E_array;}
};

#endif

