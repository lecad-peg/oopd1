#ifndef POISSONSOLVER
#define POISSONSOLVER

// PoissonSolver Fields class:
// solves 2nd order Finite Difference of Poisson's equation using
// a tridiagonal matrix

#include "fields/fields.hpp"
#include "main/oopiclist.hpp"

// forward declarations
class TriMatrix;
class Circuit;
class Conductor;

class PoissonSolver : public Fields
{
 private:
  TriMatrix *A;

  // data for matrix and right hand side modifications
  TriMatrix * matrix_bak;
  bool * rhs_fixed;

  // data for the Laplace-splitting incarnation of the solver
  bool laplaceflag;
  oopicList<Conductor> regionalconductors;
  oopicList<Dielectric> regionaldielectrics;
  bool llflag, rlflag;

  vector<int> conductor_locs;
  vector<int> dielectric_locs;

  // data for non-linear solver
  bool nonlinearflag;
  Scalar nonlinear_residual; // tolerence for nonlinear solver
  int itmax; // maximum number of iterations for nonlinear solver

  Scalar *residual; // residual of poisson solve

 public:
  PoissonSolver(SpatialRegion *sr);
  PoissonSolver(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables,
		SpatialRegion *sr);

  ~PoissonSolver();

  // accessor functions
  inline TriMatrix * get_matrix(void) { return A; }
  inline bool getlaplaceflag(void) { return laplaceflag; }
  bool is_nonlinear(void);

  // functions inherited from Fields
  void setmatrixcoef(void);
  bool is_PoissonField(void) { return true; }

  // call initialization routines for the general Fields
  // class as well as PoissonSolver specific data
  void setdefaults(void);
  void setparamgroup(void);
  void check(void);
  void init(void);

  void setdefaults_poisson(void);
  void setparamgroup_poisson(void);
  void check_poisson(void);
  void init_poisson(void);

  // updating + solver functions
  void update_fields(void);
  void compute_E(void);
  void solve_linear(Scalar * _rho);

  // methods for matrix and rhs manipulation
  void modify_matrix(void);  // top level matrix+rhs modification function
  void add_coeff(int j, Scalar a_val, Scalar b_val, Scalar c_val);
  void set_coeff(int j, Scalar a_val, Scalar b_val, Scalar c_val);
  void add_charge(int j, Scalar Q);
  void add_rhs(int j, Scalar rhs_val);
  void set_rhs(int j, Scalar rhs_val);

  // methods for restoring matrix and rhs data
  void restore_matrix_data(void);
  void restore_rhs(Scalar *new_rhs, bool compute_nonlin=false,
		   Scalar *potential=0);

  // functions for the nonlinear solver
  Scalar electrostatic_density(Scalar potential);
  Scalar electrostatic_derivative(Scalar potential);
  void add_nonlinear_density(Scalar *new_rhs, Scalar *potential);
  void update_species_fields(Scalar * _phi);

  // residual of the poisson solve (error)
  Scalar calculate_residual(Scalar *phi_check, Scalar *rho_check);

  void print_phi(FILE *fp=stdout);
  void print_phi(const char *);

  void print_matrix(FILE *fp=stdout);

  // functions to solve via Laplace splitting
  void add_Laplace(void); // adds Laplacian field using electrode voltages

};

#endif
