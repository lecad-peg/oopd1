#ifndef FIELDEMITTER_H
#define FIELDEMITTER_H

#include "emmiter/emitter.hpp"
#include "main/oopiclist.hpp"
#include "utils/ovector.hpp"

class FieldEmitter : public Emitter
{
private:
  Scalar Fi_w; // work function to determine the inject current density [ev]

  int get_nparticles(Scalar tstart, Scalar tend);

  void setdefaults();
  void setparamgroup();
  void check();
  void init();
  void maintain();

public:
  FieldEmitter(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
			oopicList<Parameter<int> > int_variables, Fields *t,
			Boundary *theb);
  ~FieldEmitter();
};
#endif
