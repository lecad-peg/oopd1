inline Scalar Fields::get_epsilon(int i) 
{ 
  if(const_epsilon) { return epsilon; }
  else { return eps[i]; }
}


inline void Fields::get_E(Scalar x, Scalar & E1, Scalar & E2, Scalar &E3)
{
  Vector3 Ex = theGrid->GatherLinear(x, E, e_dim);
  E1 = Ex.e1();
  E2 = Ex.e2();
  E3 = Ex.e3();
}
  
inline void Fields::get_E(Scalar x, Scalar & E1)
{
  Vector3 Ex = theGrid->GatherLinear(x, E);
  E1 = Ex.e1();
}

inline void Fields::get_E(Scalar x, Vector3 & Eloc)
{
  Eloc = theGrid->GatherLinear(x, E, e_dim);
}

inline void Fields::get_E(int i, Vector3 & Eloc)
{
  Eloc = E[i];
}
  
inline void Fields::get_B(Scalar x, Scalar & B1, Scalar & B2, Scalar & B3)
{
  if(constB)
    {
      B1 = B->e1();
      B2 = B->e2();
      B3 = B->e3();
    }
  else
    {
      Vector3 Bx = theGrid->GatherLinear(x, B, 3);
      B1 = Bx.e1();
      B2 = Bx.e2();
      B3 = Bx.e3();
    }
}

inline void Fields::get_B(Scalar x, Scalar & B1, Scalar & B2, Scalar & B3, Scalar ttt)
{
  return get_B(x, B1, B2, B3);
}

inline void Fields::get_B(Scalar x,Vector3 & Bloc)
{
  Bloc = theGrid->GatherLinear(x, B);
}

inline void Fields::get_B(int i,Vector3 & Bloc)
{
  Bloc = B[i];
}

inline Vector3  Fields::get_B(Scalar x, Scalar t)
{
  Vector3 B;
  if(Bxeq)
    {
     B.set_e1( Bxeq->eval(x, t));
    }
  else
    {
      Vector3 B1;
      get_B(x,B1);
      B.set_e1(B1.e1());
    }
  if(Byeq)
    {
     B.set_e2( Byeq->eval(x, t));
    }
  else
    {
      Vector3 B2;
      get_B(x,B2);
      B.set_e2(B2.e2());
    }
  if(Bzeq)
    {
     B.set_e3( Bzeq->eval(x, t));
    }
  else
    {
      Vector3 B3;
      get_B(x,B3);
      B.set_e3(B3.e3());
    }
 
 return B;   
}

inline void Fields::get_B(Scalar *f, int dim)
{
  if(!constB)
    {
      error("Fields::get_B(Scalar *f, int dim = 2)",
	    "can't be used when B field not constant! (constB = false)");
    }
  for(int i=0; i < dim; i++)
    {
      f[i] = B->e(i);
    }
}


inline Scalar Fields::magB(Scalar x)
{
  if(constB)
    {
      return B->magnitude();	  
    }
  else
    {
      Vector3 Bx = theGrid->GatherLinear(x, B);
      return Bx.magnitude();
    }  
}

inline Scalar Fields::magB(void)
{
  if(!constB)
    {
      error("Fields::magB(void)",
	    "can't be used when B field not constant! (constB = false)");	  
    }
  return B->magnitude();
}

// x in grid units
inline void Fields::get_Egradient_interior(Scalar x, Vector3 &Egrad)
{
  int y =int(x);
  Scalar w1= x-y;
  Scalar w0=1.-w1;
  
  Egrad=w0*theGrid->gradient(E,y)+w1*theGrid->gradient(E,y+1);
}

inline void Fields::get_Egradient(Scalar x, Vector3 &Egrad)
{
  int y =int(x);
  
  if (is_field_continuous(y))
  {
    if (is_field_continuous(y+1))
    {
      get_Egradient_interior(x, Egrad);
    }
    else
    {
      get_Egradient_minus(x, Egrad);
    }
  }
  else 
  {
    get_Egradient_plus(x, Egrad);
  }
}

// mindgame: Egradient in the positive direction
inline void Fields::get_Egradient_plus(Scalar x, Vector3 &Egrad)
{
  int y =int(x);
  Scalar w1= x-y;
  Scalar w0=1.-w1;
  
  Egrad=w0*theGrid->gradient_positive(E,y)+w1*theGrid->gradient(E,y+1);
}

// mindgame: Egradient in the negative direction
inline void Fields::get_Egradient_minus(Scalar x, Vector3 &Egrad)
{
  int y =int(x);
  Scalar w1= x-y;
  Scalar w0=1.-w1;
  
  if (w1 == 0.) get_Egradient_minus(y, Egrad);
  else 
  {
    Egrad=w0*theGrid->gradient(E,y)+w1*theGrid->gradient_negative(E,y+1);
  }
}

inline void Fields::get_Egradient_minus(int i, Vector3 &Egrad)
{
  Egrad=theGrid->gradient_negative(E, i);
}

inline void Fields::get_Bgradient_interior(Scalar x, Vector3 &Bgrad)
{
  int y=int(x);
  Scalar w1= x-y;
  Scalar w0=1.-w1;
  
  Bgrad=w0*theGrid->gradient(B,y)+w1*theGrid->gradient(B,y+1);
} 

inline void Fields::get_EBgradient_interior(Scalar x,
					    Vector3 &Egrad, Vector3 &Bgrad)
{
  int y=int(x);
  Scalar w1= x-y;
  Scalar w0=1.-w1;
  
  Egrad=w0*theGrid->gradient(E,y)+w1*theGrid->gradient(E,y+1);
  Bgrad=w0*theGrid->gradient(B,y)+w1*theGrid->gradient(B,y+1);
} 

inline void Fields::get_Bgradient(Scalar x, Vector3 &Bgrad)
{
  int y =int(x);
  
  if (is_field_continuous(y))
  {
    if (is_field_continuous(y+1))
    {
      get_Bgradient_interior(x, Bgrad);
    }
    else
    {
      get_Bgradient_minus(x, Bgrad);
    }
  }
  else 
  {
    get_Bgradient_plus(x, Bgrad);
  }
}

// mindgame: Bgradient in the positive direction
inline void Fields::get_Bgradient_plus(Scalar x, Vector3 &Bgrad)
{
  int y=int(x);
  Scalar w1= x-y;
  Scalar w0=1.-w1;
  
  Bgrad=w0*theGrid->gradient_positive(B,y)+w1*theGrid->gradient(B,y+1);
}

inline void Fields::get_EBgradient_plus(Scalar x,
					Vector3 &Egrad, Vector3 &Bgrad)
{
  int y=int(x);
  Scalar w1= x-y;
  Scalar w0=1.-w1;
  
  Egrad=w0*theGrid->gradient_positive(E,y)+w1*theGrid->gradient(E,y+1);
  Bgrad=w0*theGrid->gradient_positive(B,y)+w1*theGrid->gradient(B,y+1);
}

inline void Fields::get_EBgradient_plus(int i,
					Vector3 &Egrad, Vector3 &Bgrad)
{
  Egrad=theGrid->gradient_positive(E,i);
  Bgrad=theGrid->gradient_positive(B,i);
}

// mindgame: Bgradient in the negative direction
inline void Fields::get_Bgradient_minus(Scalar x, Vector3 &Bgrad)
{
  int y=int(x);
  Scalar w1= x-y;
  Scalar w0=1.-w1;
  
  if (w1 == 0.) get_Bgradient_minus(y, Bgrad);
  else 
  {
    Bgrad=w0*theGrid->gradient(B,y)+w1*theGrid->gradient_negative(B,y+1);
  }
}

inline void Fields::get_Bgradient_minus(int i, Vector3 &Bgrad)
{
  Bgrad=theGrid->gradient_negative(B,i);
}

inline void Fields::get_EBgradient_minus(Scalar x,
					 Vector3 &Egrad, Vector3 &Bgrad)
{
  int y=int(x);
  Scalar w1= x-y;
  Scalar w0=1.-w1;
  
  if (w1 == 0.) get_EBgradient_minus(y, Egrad, Bgrad);
  else 
  {
    Egrad=w0*theGrid->gradient(E,y)+w1*theGrid->gradient_negative(E,y+1);
    Bgrad=w0*theGrid->gradient(B,y)+w1*theGrid->gradient_negative(B,y+1);
  }
}

inline void Fields::get_EBgradient_minus(int i,
					 Vector3 &Egrad, Vector3 &Bgrad)
{
  Egrad=theGrid->gradient_negative(E,i);
  Bgrad=theGrid->gradient_negative(B,i);
}

inline void Fields::get_EBgradient(Scalar x, Vector3 &Egrad, Vector3 &Bgrad)
{
  int y =int(x);
  
  if (is_field_continuous(y))
  {
    if (is_field_continuous(y+1))
    {
      get_EBgradient_interior(x, Egrad, Bgrad);
    }
    else
    {
      get_EBgradient_minus(x, Egrad, Bgrad);
    }
  }
  else 
  {
    get_EBgradient_plus(x, Egrad, Bgrad);
  }
}

inline void Fields::get_EB(Scalar x, Vector3 &E, Vector3 &B)
{
  get_E(x, E);
  get_B(x, B);
}
