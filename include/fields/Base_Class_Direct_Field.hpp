//-------------------------------------------------------
//Title:   Direct Solve Base Class
//purpos:  c++ everthing fundimental to DS and TC here!
//         1) Basic Physics
//         2) list of all elemetns (FOR DS AND TC)
//By:      Andrew J. Christlieb
//Date:    5/18/04
//Contact: Andrew J. Christlieb, Ph. D.
//         Assistant Professor
//         Department of MathematicsField
//         University of Michigan
//         2470 East Hall
//         Ann Arbor, MI 48109.
//
//         Office: 4851 East Hall
//         E-mail: christli@umich.edu
//         Tel:    (734) 763-5725
//------------------------------------------------------
#ifndef BaseClass
#define BaseClass

#include "fields/fields.hpp"
#include "main/particlegroup.hpp"
#include "main/oopiclist.hpp"

// SortElement provides a pointer to a single particle identified by
// ParticleGroup and index within that group. Also provides a key for
// sorting.

class SortElement
{
  ParticleGroup *pg; // points to the ParticleGroup
  int index; // index within the ParticleGroup for each particle
public:
  SortElement(void) {
    pg=NULL; index=0;
  };
  SortElement(ParticleGroup *_pg, int _index) {pg=_pg; index=_index;};
  void set(ParticleGroup* _pg, int _index) {
    pg=_pg;
    index=_index;
  };
  inline Scalar key(void) {return pg->get_x(index);}; // in grid units!
  inline Scalar get_q(void) {return pg->get_q(index);};
  inline Scalar get_qx(void) {return get_q()*key();};
  inline void set_E(Scalar E) {pg->set_E(index, E);};
  inline Scalar get_E(void) {return pg->get_E(index);};
};

//Makes sort key and initshalxzes the rest:
class Base_Class_Direct_Field : public Fields
{
public:
  //
  //Variales
  //
  int nParticles; // total number of particles in system
  SortElement *sortKeys; // array for sorting all particles by x
  //
  //Functions
  //
  Base_Class_Direct_Field(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables, SpatialRegion *sr);
  ~Base_Class_Direct_Field(void);

  // Functions passed down from parse (must maintain virtual chain)
  virtual void setdefaults(void) {setdefaults_fields();}
  virtual void setparamgroup(void) {setparamgroup_fields();}
  virtual void check(void){check_fields();}
  virtual void init(void){init_fields();}

  virtual Parse * special(ostring s) { return 0;}

};


#endif // Base Calss
