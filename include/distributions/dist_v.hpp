// VDistribution class
//
// Original by HyunChul Kim, 2005

#ifndef	DIST_V_H
#define	DIST_V_H

template <typename Type> class Gaussian;
template <typename Type> class Arbitrary;
class Species;

#include "utils/ovector.hpp"
#include "utils/message.hpp"
#include "distributions/dist_xv.hpp"

class VDistribution : public XVDistribution
{
  Gaussian<Vector3> *gaussian_type;
  Arbitrary<Vector3> *arbitrary_type;
  int dist_type;
  LoadType load_type;
  Equation *func1, *func2, *func3;
  Scalar gamma0;
  bool force_nr_flag;
  bool flux_flag;
  int mom_dir;
  Vector3 mul_vec;

  Vector3 v0; // drift velocity
  Vector3 vt; // thermal velocity
  Vector3 vcl; // lower cutoff velocity
  Vector3 vcu; // upper cutoff velocity
  Vector3 u0;  // ?
  Scalar beta;
  Scalar iv0;
  Vector3 u0normal;
  Vector3 v0normal;

  Vector3 get_relativistic_addition(Vector3 v) const;
  int get_moment_direction(Vector3 norm);
  void check();
  void init();
  inline void flag_check(Direction direction) const
  {
    if ((flux_flag == false && direction != NONE)
        || (flux_flag == true && direction == NONE))
    {
      terminate_run("flag_check: You are doing something wrong!");
    }
  }

 public:
  VDistribution(Species *_species, Vector3 _vt, Vector3 _v0=Vector3(0.,0.,0.),
		bool _flux_flag=false, Vector3 _vcl=Vector3(0.,0.,0.),
		Vector3 _vcu=Vector3(0.,0.,0.), LoadType _load_type=RANDOM,
		Vector3 _norm=Vector3(1.,0.,0.));

  VDistribution(Species *_species, Scalar _vt, bool _flux_flag,
		Vector3 _vcl=Vector3(0.,0.,0.),
		Vector3 _vcu=Vector3(0.,0.,0.), LoadType _load_type=RANDOM,
		Vector3 _norm=Vector3(1.,0.,0.));

  VDistribution(Species *_species, Equation *func,
		bool _flux_flag, Vector3 _vcl, Vector3 _vcu,
		LoadType _load_type, Vector3 _norm=Vector3(1.,0.,0.));

  VDistribution(Species *_species, Equation *func1, Equation *func2,
		Equation *func3, bool _flux_flag, Vector3 _vcl, Vector3 _vcu,
		LoadType _load_type, Vector3 _norm=Vector3(1.,0.,0.));

  VDistribution(Vector3 _vt, bool _flux_flag=false,
		bool _force_nr_flag=true, LoadType _load_type=RANDOM,
		Vector3 _norm=Vector3(1.,0.,0.));

  VDistribution(Scalar _vt, bool _flux_flag=false,
		bool _force_nr_flag=true, LoadType _load_type=RANDOM,
		Vector3 _norm=Vector3(1.,0.,0.));

  ~VDistribution();

  // return new velocity (in units of MKS)
  Vector3 get_V(Direction direction=NONE) const;
  void get_V(int n, Vector3 *v, Direction direction=NONE) const;

  // return new gamma*velocity (in units of MKS)
  Vector3 get_U(Direction direction=NONE) const;
  void get_U(int n, Vector3 *v, Direction direction=NONE) const;

  void get_U(int n, Scalar *v1,Scalar *v2, Scalar *v3,
	     Direction direction=NONE) const;

  // e is the component in coordinate vector
  // e.g. for planar Vector3, 1=x, 2=y, and 3=z
  Scalar ave_1st_mnt_on_boundary(int e, Direction direction) const;

  // return flux of moment in appropriate direction
  Scalar flux(Scalar _v0, Direction direction, int moment=0) const;
};

#endif
