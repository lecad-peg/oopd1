// Distribution function class
//
// Original by HyunChul Kim, 2005

#ifndef DIST_FUNCTION_H
#define DIST_FUNCTION_H

#ifndef DIST_TYPE
#ifndef DIST_TYPE_DECLARED
enum {UNIFORM_DIST, ARBITRARY_DIST, GAUSSIAN_DIST};
#define DIST_TYPE_DECLARED
#endif
#endif

#include "utils/message.hpp"
#include "utils/ovector.hpp"

template <typename Type>

class Dist_Function : public Message
{
protected:
  LoadType load_type;
  int moments;
  int moments_direction;
  int index;
  int dimension;

  Type start_cdf, length_cdf;
  Type lcutoff, ucutoff;
  int scale_flag;
  Type scale, shift;
  virtual Scalar cdf(Scalar x, int cases) const=0 ;
  virtual Scalar inv_cdf(Scalar x, int cases, Scalar x0=0., Scalar x1=0.) const=0;
  void generate_anyMoments(int n, Type *x, bool all_dir=true, int direction=1);
  void generate_anyMoments_random(int n, Type *x, bool all_dir, int direction) const;
  void generate_anyMoments_quiet(int n, Type *x, bool all_dir, int direction) const;
  void generate_anyMoments_uniform(int n, Type *x, bool all_dir,
				   int direction);
  Type generate_anyMoments() const;
  Type generate_anyMoments_random() const;
  Type generate_anyMoments_quiet() const;
  Scalar generate_anyMoments(int direction) const;
  Scalar generate_anyMoments_random(int direction) const;
  Scalar generate_anyMoments_quiet(int direction) const;
  void init_anyMoments();
  void set_dimension();

public:
  Dist_Function();
  virtual ~Dist_Function() { }

  virtual Type generate() const = 0;
  virtual void generate(int n, Type *x) = 0;
};

#endif
