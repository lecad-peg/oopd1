// XDistribution class
//
// Original by HyunChul Kim, 2005

#ifndef DIST_X_H
#define DIST_X_H

// X Distribution function class --
// used to generate X distribution
template <typename Type> class Uniform;
template <typename Type> class Arbitrary;

#include "utils/ovector.hpp"
#include "utils/message.hpp"
#include "distributions/dist_xv.hpp"

class Grid;

class XDistribution : public XVDistribution
{
  Scalar xstart1;
  Scalar xend1;
  Vector2 xstart2;
  Vector2 xend2;
  Vector3 xstart3;
  Vector3 xend3;
  int dimension;
  LoadType load_type;
//  bool endpoints;
  int geom_type;

  Uniform<Vector1> *uniform_type1;
  Arbitrary<Vector1> *arbitrary_type1;
  Uniform<Vector2> *uniform_type2;
  Arbitrary<Vector2> *arbitrary_type2;
  Uniform<Vector3> *uniform_type3;
  Arbitrary<Vector3> *arbitrary_type3;
  int dist_type;

public:
  XDistribution(Grid *_grid, Scalar _xstart, Scalar _xend,
		LoadType _load_type=UNIFORM);

  XDistribution(Grid *_grid, Vector3 _xstart, Vector3 _xend,
		LoadType _load_type=UNIFORM);

  XDistribution(Grid *_grid, Vector2 _xstart, Vector2 _xend,
		LoadType _load_type=UNIFORM);

  XDistribution(Grid *_grid, Scalar _xstart, Scalar _xend,
		Equation *_func, LoadType _load_type=UNIFORM);

  XDistribution(Grid *_grid, Vector3 _xstart, Vector3 _xend,
		Equation *_func1, Equation *_func2, Equation *_func3,
		LoadType _load_type=UNIFORM);

  XDistribution(Grid *_grid, Vector2 _xstart, Vector2 _xend,
		Equation *_func1, Equation *_func2,
		LoadType _load_type=UNIFORM);

  ~XDistribution();

  void get_X(int n, Scalar *x) const;
  void get_X(int n, Vector2 *x) const;
  void get_X(int n, Vector3 *x) const;
// mindgame: support?
//  void get_X(int n, Scalar *x1, Scalar *x2, Scalar *x3);

  void check();
};

#endif
