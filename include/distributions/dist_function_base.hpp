// A part of Distribution Function class
//   : to avoid the code duplication
//
// Original by HyunChul Kim, 2005

#ifdef NO_ARGUMENT
// For generate_anyMoments_quiet() and generate_anyMoments_random()
  Type x;
  int cases;

  if (scale_flag)
  {
    for (int e=1; e<=dimension; e++)
    {
      if (e == moments_direction) cases = index;
      else cases = e;
      x.set(e, inv_cdf(start_cdf.e(e)+length_cdf.e(e)*RAND_FUNC(), cases, lcutoff.e(e), ucutoff.e(e))*scale.e(e)+shift.e(e));
    }
  }
  else
  {
    for (int e=1; e<=dimension; e++)
    {
      if (e == moments_direction) cases = index;
      else cases = e;
      x.set(e, inv_cdf(start_cdf.e(e)+length_cdf.e(e)*RAND_FUNC(), cases, lcutoff.e(e), ucutoff.e(e)));
    }
  }
  return x;
#endif

#ifdef ONE_ARGUMENT
// For generate_anyMoments_quiet(int e) and generate_anyMoments_random(int e)

  if (scale_flag)
  {
    if (e==moments_direction)
      return inv_cdf(start_cdf.e(e)+length_cdf.e(e)*RAND_FUNC(), index, lcutoff.e(e), ucutoff.e(e))*scale.e(e)+shift.e(e);
    else
      return inv_cdf(start_cdf.e(e)+length_cdf.e(e)*RAND_FUNC(), e, lcutoff.e(e), ucutoff.e(e))*scale.e(e)+shift.e(e);
  }
  else
  {
    if (e==moments_direction)
      return inv_cdf(start_cdf.e(e)+length_cdf.e(e)*RAND_FUNC(), index, lcutoff.e(e), ucutoff.e(e));
    else
      return inv_cdf(start_cdf.e(e)+length_cdf.e(e)*RAND_FUNC(), e, lcutoff.e(e), ucutoff.e(e));
  }
#endif

#ifdef FOUR_ARGUMENTS
// For generate_anyMoments_quiet(int n, Type *x, bool all_dir, int e)
//    and generate_anyMoments_random(int n, Type *x, bool all_dir, int e)
  if (all_dir)
  {
    int cases;
    if (scale_flag)
      for (int e=1; e<=dimension; e++)
      {
	if (e == moments_direction) cases = index;
	else cases = e;
	for (int i=0; i < n; i++)
	{
	  x[i].set(e, inv_cdf(start_cdf.e(e)+length_cdf.e(e)*RAND_FUNC(), cases, lcutoff.e(e), ucutoff.e(e))*scale.e(e)+shift.e(e));
        }
      }
    else
      for (int e=1; e<=dimension; e++)
      {
	if (e == moments_direction) cases = index;
	else cases = e;
	for (int i=0; i < n; i++)
	{
	  x[i].set(e, inv_cdf(start_cdf.e(e)+length_cdf.e(e)*RAND_FUNC(), cases, lcutoff.e(e), ucutoff.e(e)));
        }
      }
  }
  else if (e == moments_direction)
  {
    if (scale_flag)
      for (int i=0; i < n; i++)
      {
        x[i].set(e, inv_cdf(start_cdf.e(e)+length_cdf.e(e)*RAND_FUNC(), index, lcutoff.e(e), ucutoff.e(e))*scale.e(e)+shift.e(e));
      }
    else
      for (int i=0; i < n; i++)
      {
        x[i].set(e, inv_cdf(start_cdf.e(e)+length_cdf.e(e)*RAND_FUNC(), index, lcutoff.e(e), ucutoff.e(e)));
      }
  }
  else
  {
    if (scale_flag)
      for (int i=0; i < n; i++)
      {
        x[i].set(e, inv_cdf(start_cdf.e(e)+length_cdf.e(e)*RAND_FUNC(), e, lcutoff.e(e), ucutoff.e(e))*scale.e(e)+shift.e(e));
      }
    else
      for (int i=0; i < n; i++)
      {
        x[i].set(e, inv_cdf(start_cdf.e(e)+length_cdf.e(e)*RAND_FUNC(), e, lcutoff.e(e), ucutoff.e(e)));
      }
  }
#endif
