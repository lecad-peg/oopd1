// Distribution function class
#ifndef DIST_XV_H
#define DIST_XV_H

// types of distribution functions:
// -> Maxwellian, cut-off and full
// -> functional, f(x/r, Vx/r, ...)
// -> Known at discrete points

#include "utils/message.hpp"
// forward declarations
class Species;
#if 0
class Boundary;
#endif

class XVDistribution : public Message
{
protected:
  Species *species;

public:
  XVDistribution();
  ~XVDistribution() {}
};

#endif
