// Gaussian distribution class
//
// Rewritten by HyunChul Kim, 2005, based on XOOPIC code
//
// This distribution is not explicitly related to velocity itself.
//

#ifndef	GAUSSIAN_H
#define	GAUSSIAN_H

#include "utils/ovector.hpp"
#include "distributions/dist_function.hpp"

template <typename Type = Vector3>
class Gaussian : public Dist_Function<Type>
{
  bool cutoffFlag;   // cutoffFlag = true cutoff distribution,
                    // cutoffFlag = false full gaussian
  bool *ucutoffFlag, *lcutoffFlag;
  bool *meanFlag;
  Type mean, mean_org;
  Type lcutoff_org, ucutoff_org;
  Type deviation, deviation_org;
  Type erfvu, erfvl;
  int nvel;
  Type* array;
  int nvelF;
  Type* moments_array;
  Type expvu2, expvl2, Vsqr, vcu2;
  Scalar vc2;
  void set_cutoff();
  void set_array();  //sets up array
  void set_moments_array();  //sets up moments_array

  Scalar rtbis_fn(Scalar value, int moments,
		  Scalar x1, Scalar x2, int dimension) const;

  // mindgame:
  //  Eventually, relativisic vector addition for velocity is necessary.
  inline Scalar fn(Scalar x, int e) const
  {
    return exp(x)-sqrt(M_PI)*mean.e(e)*erf(x);
  }

  inline Scalar cdf(Scalar x, int cases) const
	 { switch (cases)
	   {case 1: case 2: case 3: return Scalar(erf(x));
	    case 4:
		return fn(x, 1);
	    case 5:
		return fn(x, 2);
	    case 6:
		return fn(x, 3);
	    default:
		return (Scalar)this->terminate_run("cdf: Cannot be reached.");
	   }
	 }

  inline Scalar inv_cdf(Scalar x, int cases, Scalar x0, Scalar x1) const
	 { switch (cases)
	   {case 1: case 2: case 3: return rtbis(erf, x, 0, x0, x1);
	    case 4:
		return rtbis_fn(x, 0, x0, x1, 1);
	    case 5:
		return rtbis_fn(x, 0, x0, x1, 2);
	    case 6:
		return rtbis_fn(x, 0, x0, x1, 3);
	    default:
		return (Scalar)this->terminate_run("inv_cdf: Cannot be reached.");
	   }
	 }

  void init();
  void check();
  inline Scalar normal_lc_zero_with_ucutoff(int e) const
  {
    static int iset=0;
    static float gset;
    float fac,r,v1,v2;

    if(iset==0)
      {
	do
	{
	  v1=2.0*frand()-1.0;
	  v2=2.0*frand()-1.0;
	  r = v1*v1+v2*v2;
	} while (r>=1.0);
	fac=sqrt(2*(vcu2.e(e)-log(r+(1.-r)*expvu2.e(e))))/r;
	gset=v1*fac;
	iset =1;
	r=v2*fac;
      }
      else
      {
	iset=0;
	r = gset;
      }
    return r;
  }

  inline Scalar revers_normal_lc_zero_with_ucutoff(int e) const
  {

    static int iset=0;
    static int count=0;
    static float gset;
    float fac,r,v1,v2;

    if(iset==0)
      {
	do
	{
	  v1=2.0*revers(count++,2)-1.0;
	  v2=2.0*revers(count,5)-1.0;
	  r = v1*v1+v2*v2;
	} while (r>=1.0);
	fac=sqrt(2*(vcu2.e(e)-log(r+(1.-r)*expvu2.e(e))))/r;
	gset=v1*fac;
	iset =1;
	r=v2*fac;
      }
      else
      {
	iset=0;
	r = gset;
      }
    return r;
  }

 public:
  Gaussian(Type _deviation, Type _mean, int _moments, int _moments_direction,
	   Type _lcutoff, Type _ucutoff, LoadType _load_type);

  ~Gaussian();

  Type get_mean() const; // return mean in units specified
  Type get_deviation() const; // return deviation in units specified

  // return generated Gaussian or Gaussian-Moment distribution
  Type generate() const;
  void generate(int n, Type *x);

  Scalar ave_1st_mnt_on_boundary(int i, Direction direction) const;

 };

#endif	//	ifndef GAUSSIAN
