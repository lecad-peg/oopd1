// Uniform distribution class
//
// Rewritten by HyunChul Kim, 2005, separated from grid.cpp
//

#ifndef	UNIFORM_H
#define	UNIFORM_H

#ifdef _STANDALONE_LOADLIB
#define ONETHIRD 0.33333333333333333333333333333333333333333333333
#endif

#include "utils/ovector.hpp"
#include "distributions/dist_function.hpp"

//--------------------------------------------------------------------
// Uniform:  This class describes a Uniform distribution.
// This distribution is not explicitly related to position itself.

template <typename Type = Vector1>
class Uniform : public Dist_Function<Type>
{
  inline Scalar cdf(Scalar x, int cases) const
	 { switch (cases)
	   {case 1: case 2: case 3: return x;
	    case 4: case 5: case 6: return x*x;
	    case 7: case 8: case 9: return x*x*x;
	    default:
		return (Scalar)this->terminate_run("cdf: Cannot be reached.");
	   }
	 }

  inline Scalar inv_cdf(Scalar x, int cases, Scalar x0=0., Scalar x1=0.) const
	 { switch (cases)
	   {case 1: case 2: case 3: return x;
	    case 4: case 5: case 6: return sqrt(x);
	    case 7: case 8: case 9: return pow(x, Scalar(ONETHIRD));
	    default:
		return (Scalar)this->terminate_run("inv_cdf: Cannot be reached.");
	   }
	 }

  void check();

 public:
  Uniform(Type _start, Type _end, int _moments, int _moments_direction,
	  LoadType _load_type);

  ~Uniform();

 // return generated Uniform or Uniform-Moment distribution
  void generate(int n, Type *x);
  Type generate() const;
};

#endif	//	ifndef Uniform
