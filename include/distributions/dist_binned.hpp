#ifndef DIST_BINNED
#define DIST_BINNED

// Binned Distribution function class --
// used to accumulate particles to a distribution function
//
// Types of distribution functions:
// 1) Energy and/or Angular (decoupled)
// 2) Energy and Angular (coupled)
// 3) Velocity (1v)
// 4) Velocity (3v)

#include "main/parse.hpp"
#include "utils/ovector.hpp"
#include "utils/Array.hpp"
#include "main/uniformmesh.hpp"
// forward declarations
class Array2D;
class Species;
class DataArray;

class BinnedDistribution : public Parse
{
private:
	Species *species;
	Boundary *bd;
	SpeciesList * theSpeciesList;

	Vector3 norm; // normal vector for Angular distribution

    // 2d coupled distribution, MAL 12/18/09
    Array2D * enangle;
    int iE, iA; // for 2d weighting
    Scalar fracE, fracA; // for 2d linear weighting, not used now
    bool coupled2d; // whether to do the 2d calculations and the 3d plot

	// 1d distributions
	DataArray *energydist;
	DataArray *angledist;
	DataArray *velocitydist; // 1d velocity distribution

	UniformMesh energymesh;
	UniformMesh anglemesh;
	UniformMesh velocitymesh;

	// variables for Parse
	ostring spname;
	Scalar Emin, Emax;
	Scalar anglemax;
	int nEnergy, nAngular;
	bool coupled;

	// variables for diagnostics
	DiagList tds;

public:
	BinnedDistribution();
	BinnedDistribution(int _nEnergy, Scalar _Emax, int _nAngular=0, bool _coupled=true);
	BinnedDistribution(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
			oopicList<Parameter<int> > int_variables, Boundary * _bd);
	~BinnedDistribution();

	// accessor functions
	inline void set_species(Species *_sp) { species = _sp; }
	bool is_species(int tag);

	// top-level particle accumulation functions
	void add_particle(ParticleGroup *p, int i);
	void add_particles(ParticleGroup *p);

    void weight2d(Scalar _en, Scalar _ang, Scalar _wt); // weighting on the 2d grid, MAL 12/19/09

	// lower-level particle accumulation functions
	void add_velocity1d(ParticleGroup *p, int i);

	// virtual functions from Parse
	void setdefaults(void);
	void setparamgroup(void);
	void check(void);
	void init(void);

	// functions for diagnostics
	void init_diagnostics(void);

	// Accessor functions for IED's, HH 02/19/16
	Scalar get_nEnergyBins(void);
	ostring get_BoundaryPosition(void);
	inline DataArray *get_energydist() {return energydist;}
	inline DataArray *get_xarray_energy() {return energymesh.get_xarray();}
	// Accessor functions for IAD's, HH 06/02/16
	Scalar get_nAngleBins(void);
	inline DataArray *get_angledist() {return angledist;}
	inline DataArray *get_xarray_angle() {return anglemesh.get_xarray();}
};
#endif
