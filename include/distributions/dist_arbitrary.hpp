#ifndef	ARBITRARY_H
#define	ARBITRARY_H

//--------------------------------------------------------------------
// Arbitrary:  This class describes a Arbitrary distribution.
// This distribution is not explicitly related to position itself.

#include "distributions/dist_function.hpp"
#include "utils/ovector.hpp"

template <typename Type = Vector1>
class Arbitrary : public Dist_Function<Type>
{
  Scalar x0, x1;
  Equation *func1, *func2, *func3;
  inline Scalar cdf(Scalar x, int cases) const
	 { switch (cases)
	   {case 1: return qsimp(func1,x,0);
	    case 2: return qsimp(func2,x,0);
	    case 3: return qsimp(func3,x,0);
	    case 4: return qsimp(func1,x,1);
	    case 5: return qsimp(func2,x,1);
	    case 6: return qsimp(func3,x,1);
	    case 7: return qsimp(func1,x,2);
	    case 8: return qsimp(func2,x,2);
	    case 9: return qsimp(func3,x,2);
	    default:
		return (Scalar)this->terminate_run("cdf: Cannot be reached.");
	   }
	 }
  inline Scalar inv_cdf(Scalar x, int cases, Scalar x0, Scalar x1) const
	 { switch (cases)
	   {case 1: return rtbis2(func1,x,0,x0,x1);
	    case 2: return rtbis2(func2,x,0,x0,x1);
	    case 3: return rtbis2(func3,x,0,x0,x1);
	    case 4: return rtbis2(func1,x,1,x0,x1);
	    case 5: return rtbis2(func2,x,1,x0,x1);
	    case 6: return rtbis2(func3,x,1,x0,x1);
	    case 7: return rtbis2(func1,x,2,x0,x1);
	    case 8: return rtbis2(func2,x,2,x0,x1);
	    case 9: return rtbis2(func3,x,2,x0,x1);
	    default:
		return (Scalar)this->terminate_run("inv_cdf: Cannot be reached.");
	   }
	 }

  void check();

 public:
  Arbitrary(Type _start, Type _end, Equation *_func, int _moments,
	    int _moments_direction, LoadType _load_type);

  Arbitrary(Type _start, Type _end, Equation *_func1,
	    Equation *_func2, Equation *_func3, int _moments,
	    int _moments_direction, LoadType _load_type);

  Arbitrary(Type _start, Type _end, Equation *_func1,
	    Equation *_func2, int _moments,
	    int _moments_direction, LoadType _load_type);

  ~Arbitrary();

 // return generated Arbitrary or Arbitrary-Moment distribution
  void generate(int n, Type *x);
  Type generate() const;

  // mindgame: Not implemented yet
  Scalar ave_1st_mnt_on_boundary(int e) const { return 0; }
};

#endif	//	ifndef Arbitrary
