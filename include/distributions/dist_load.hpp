// Sample file for Loading part
//
// Original by HyunChul Kim, 2005

#ifndef LOADING_H
#define LOADING_H

#include <math.h>
#include "utils/ovector.hpp"

/*
#ifndef SPEED_OF_LIGHT
const double  SPEED_OF_LIGHT = 299792458.;
const double iSPEED_OF_LIGHT = 1. / SPEED_OF_LIGHT;
const double  SPEED_OF_LIGHT_SQ = SPEED_OF_LIGHT * SPEED_OF_LIGHT;
const double iSPEED_OF_LIGHT_SQ = 1. / SPEED_OF_LIGHT_SQ;
#endif
*/

#ifndef GEOM_TYPE_DECLARED
enum {PLANAR=0, CYLINDRICAL=1, SPHERICAL=2};
#define GEOM_TYPE_DECLARED
#endif

#ifndef LoadType
#ifndef LOAD_TYPE_DECLARED
enum LoadType {UNIFORM, UNIFORM_NO_EP, RANDOM, QUIET};
#define LOAD_TYPE_DECLARED
#endif
#endif

#ifndef DIRECTION
#ifndef DIRECTION_DECLARED
#define DIRECTION_DECLARED
enum Direction {POSITIVE, NEGATIVE, NONE};
#endif
#endif

class Species
{

   bool force_nr_flag;
   double nr_v2_limit;

  public:

   Species() { force_nr_flag = false; }

   inline bool is_nr_forced() { return force_nr_flag; }

   inline Scalar get_gamma2(Scalar v2) {
   // if (force_nr_flag || v2 < nr_v2_limit)
    if (force_nr_flag)
      return 1.;
    else
      return 1/sqrt(1.- iSPEED_OF_LIGHT_SQ*v2);
   }

   inline Scalar get_gamma2(Vector3 v) {
     Scalar v2 = v*v;
   // if (force_nr_flag || v2 < nr_v2_limit)
    if (force_nr_flag)
      return 1.;
    else
      return 1/sqrt(1.- iSPEED_OF_LIGHT_SQ*v2);
   }

};

class Grid
{
   int type;

  public:
   Grid() { type = PLANAR; }
   inline int geometry() { return type; }

};

/*class Equation
{
  public:
   Equation() {}
   Scalar get_value(Scalar a) {return a;}
};
*/
#endif
