#ifndef CROSSX_SAMPLES
#define CROSSX_SAMPLES
#ifndef CROSSX
#ifndef CROSSX_DECLARED
class CrossX
#define CROSSX_DECLARED
#endif
#endif

// Those crossections are independent on the species type. 
class CrossX_type1 : public CrossX 
{
  Scalar energy0;
  Scalar energy1;
  Scalar energy2;
  Scalar sigmaMax;
  Scalar const1;
  Scalar const3;

 public:

  CrossX_type1(Scalar e0, Scalar e1, Scalar e2, Scalar sMax, Type type);
  virtual ~CrossX_type1(){}

  virtual Scalar sigma(Scalar energy) const;
  virtual Scalar get_threshold() const {return energy0;}
  virtual Scalar value(Scalar energy) const {return 0.;}
};

class CrossX_type2 : public CrossX ////t.
{
	Scalar a;
	Scalar b;
	
 public:
	CrossX_type2(Scalar a, Scalar b, Type type);
	virtual ~CrossX_type2(){}
	virtual Scalar sigma(Scalar energy) const ;
	virtual Scalar value(Scalar energy) const {return 0.;}
};

//type 1 methods
CrossX_type1::CrossX_type1(Scalar e0, Scalar e1, Scalar e2, Scalar sMax, Type _type)//:CrossX(T)
{ 
  energy0 = e0;
  energy1 = e1;
  energy2 = e2;
  sigmaMax = sMax;
  const1 = sigmaMax/(energy1 - energy0);
  const3 = sigmaMax * energy2 / log(energy2);
  type = _type;
}

Scalar CrossX_type1::sigma(Scalar energy) const
{
  if (energy <= energy0) return 0;
  if (energy <= energy1) return (energy - energy0) * const1;
  if (energy <= energy2) return sigmaMax;
  return const3 * log(energy) / energy;
}


//type 2 methods
CrossX_type2::CrossX_type2(Scalar _a, Scalar _b, Type _type)//:CrossX(T)
{
  a = _a;
  b = _b;
  type = _type;
}

Scalar CrossX_type2::sigma(Scalar energy) const
{
  return a + b / (sqrt(energy) + 1E-30);
}

#endif
