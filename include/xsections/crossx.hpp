#ifndef CROSSX
#define CROSSX

/*
  (07 / 06)
  For the moment CrossX::type should not only be understood as a type of reaction, but also as a "model".
  For example, we distinguish electron impact elastic reactions, and elastic collisions of species of equivalent masses...even if they both correspond to the same physical process (but we don't model them the same way). 
It is important to keep in mind that the default reactions included in mcc.cpp have been developped in a special context (they are based on the argon behavior). But it is possible to rewrite a more appropriate model for reactions involving other species.
  
  If a collision type name starts with E_, it is an Electron impact collision, on a species of much higher mass. (in the collision we will neglect the dynamics of the species of much higher massin comparison with the the eletron dynamics). P_ means proton impact. SAMEMASS means the colliding species have similar masses.
  - ELASTIC: colliding species are conserved.
  - EXCITATION: colliding species are conserved, a loss of energy is assumed to account for the energy lost in the excitation and radiative desexcitation.
  - "e" and "h" refer to the quantity exchanged in charge EXCHANGE processes.
  - The word IONIZATION alone means an electron is taken off the target. The impacting species is conserved. 
  - DISSIONIZ = dissociative ionization: ex: E + CH4 -> CH3+ H+ 2E
  - E_DISS = dissociative excitation. ex: E + CH4 -> CH3 + H + E
  - DBLEDISSO = double dissociation due to excitation: ex: E + CH4 -> CH2 + 2H + E
  - E_DISSRECOMB (dissociative recombination): ex: E + CH4+ -> CH3 + H 
  - DBLEDISSRECOMB (double dissociation recomination): ex: E+ Ch4+ -> CH2 + H2
  - FAKE is used to recognize the fake cross sections that ca be created, as will, in fake.hpp.

*/

class CrossX
{
 public:
  enum Type {E_ELASTIC,  SAMEMASS_ELASTIC, E_EXCITATION,  E_DISS, E_DBLEDISS, E_IONIZATION, E_DISSIONIZ, P_IONIZATION, E_DISSRECOMB, E_DBLEDISSRECOMB, E_TPLEDISSRECOMB, eEXCHANGE, P_eEXCHANGE, P_hEXCHANGE, FAKE} type; 

  CrossX(){}

  virtual ~CrossX(){}
  
  Type get_type  (void) const { return type; }
  virtual Scalar get_threshold() const {return 0.;}
  virtual Scalar sigma(Scalar energy) const = 0;
  virtual Scalar value(Scalar energy) const  = 0; // returns value in m^2
};

#endif

