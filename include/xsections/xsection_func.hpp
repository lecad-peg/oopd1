#ifndef XSECTIONFUNC_H
#define XSECTIONFUNC_H

#include "utils/ovector.hpp"
#include "xsections/xsection.hpp"
#include "utils/arbitrary_func.hpp"

class Xsection_Func : public Xsection
{
  Arbitrary_Func *arbitrary_func;
  Scalar energy0;

public:
  Xsection_Func()
   {
     arbitrary_func = NULL;
   }
  Xsection_Func(Scalar (*g)(Scalar), Scalar _threshold, ReactionType _name=NO_TYPE)
   {
     arbitrary_func = new Internal_Func(g);
     name = _name;
     energy0 = _threshold;
   }
  Xsection_Func(Equation *_x_func, Scalar _threshold, ReactionType _name=NO_TYPE)
   {
     arbitrary_func = new Equation_Func(_x_func);
     name = _name;
     energy0 = _threshold;
   }
  Xsection_Func(PairArray<> _pairarray, Scalar _threshold, ReactionType _name=NO_TYPE)
   {
     arbitrary_func = new Pairarray_Func(_pairarray, true);
     name = _name;
     energy0 = _threshold;
   }

//  Scalar value(Scalar energy) const;
  Scalar get_threshold() const {return energy0;}
//  inline Scalar function(Scalar energy, Scalar (*g)(Scalar)=NULL) const;
  Scalar sigma(Scalar energy) const
  {
    if (energy <= energy0) return 0;
   else return arbitrary_func->get_value(energy);
  }
};
#endif
