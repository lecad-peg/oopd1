// mindgame: I think fakecrossXsection won't be necessary any more.
#ifndef FAKE_H
#define FAKE_H

class Xsection;
class Fake : public MCC
{
 public:
  Fake(SpeciesList * slist, Species* sp, bool _useSecDefaultWeight, ostring sec_e, ostring sec_i):MCC(slist, sp, _useSecDefaultWeight, sec_e, sec_i) 
    {
      pkgname = "Fake";
      u_gas = &un;
      eFlag_gas = false;

      add_collider(E);
      add_fakeXsection( 1.0e-14, 1.0e-14, FAKE);       
      // add_fakeXsection(1., 0.70E-23, E_IONIZATION);
      // add_fakiXsection(2.e-15, SAMEMASS_ELASTIC);
    }
  virtual ~Fake(){}

  void dynamic(const Xsection& cx)
  {
    switch(cx.get_type())
      {
      case FAKE:
	fakeCollision(cx);
	break;
      default: 
	fprintf(stderr,"Unrecognized collision type!\n");
      }
  }

  void fakeCollision(const Xsection& cx)
  {
    ue = 0.5*ue;
    // ue = -ue;
    // ue = DELETE_PARTICLE;
  }
};

#endif
