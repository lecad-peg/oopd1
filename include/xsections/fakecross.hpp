// mindgame: I think fakecrossXsection won't be necessary any more.
#ifndef FAKECROSS
#define FAKECROSS

class Xsection;

class fakeXsection : public Xsection
//Contains cross section types relevant to an impacting electron
{
  Scalar energy0;
  Scalar energy1;
 public:
  fakeXsection(Scalar e0, Scalar e1, ReactionType name);
  virtual ~fakeXsection(){}
  virtual Scalar sigma(Scalar energy) const;
  virtual Scalar get_threshold() const {return energy0;}
  virtual Scalar value(Scalar energy) const {return 0.;}
};


fakeXsection::fakeXsection(Scalar e0, Scalar e1, ReactionType _name)  //:Xsection(T)
{ 
  energy0 = e0;
  energy1 = e1;
  name = _name;
}

Scalar fakeXsection::sigma(Scalar energy)const
{
  //return energy1;
  return energy0 / sqrt( 2 * energy * 1.6e-19 / ELECTRON_MASS);
  //return energy0 / SPEED_OF_LIGHT /sqrt(1 - 1/((energy / ELECTRON_MASS_EV+1)*(energy  /ELECTRON_MASS_EV+1)));
}

//------------------------------------------------------------------------------------------------------------

class fakiXsection : public Xsection
//Contains cross section types relevant to a particle of mass the proton mass, and no threshold.
{
  Scalar a0;
public:
  fakiXsection(Scalar e0, ReactionType name);
  virtual ~fakiXsection(){}
  virtual Scalar sigma(Scalar energy) const;
  virtual Scalar get_threshold() const {return a0;}
  virtual Scalar value(Scalar energy) const {return 0.;}
};


fakiXsection::fakiXsection(Scalar _a0, ReactionType _name)//:Xsection(T)
{ 
  a0 = _a0;
  name = _name;
} 

Scalar fakiXsection::sigma(Scalar energy) const
{
  return a0 / sqrt( 2 * energy / 6.242e18 / PROTON_MASS);
}

#endif
