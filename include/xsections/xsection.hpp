#ifndef XSECTION_H
#define XSECTION_H

#include "utils/ovector.hpp"
#include "reactions/reactiontype.hpp"

// mindgame: base class for x-section
class Xsection
{
 protected:
  ReactionType name;

 public:
  Xsection(){}
  virtual ~Xsection(){}

  ReactionType get_type() const { return name; }
  virtual Scalar get_threshold() const {return 0.;}
  virtual Scalar sigma(Scalar energy) const = 0;
  //virtual Scalar value(Scalar energy) const  = 0; // returns value in m^2
};

// mindgame: array of x-sections
class XsectionSet
{
  private:
  // mindgame: use STL to simplify the implementation
    vector<Xsection *> cxs;
  public:
  ~XsectionSet()
  {
    int num = cxs.size();
    if (num)
    {
      for(int i=0; i<num; i++)
        delete cxs[i];
    }
  }

  // Add cross sections
  void add_xsection(Xsection* cx)
  {
    cxs.push_back(cx);
  }
  inline int get_numberofxsection () const { return cxs.size();}
  inline const Xsection* get_xsection(int i) const { return cxs[i];}
};
#endif
