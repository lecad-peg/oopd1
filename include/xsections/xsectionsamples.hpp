#ifndef XSECTION_SAMPLES_H
#define XSECTION_SAMPLES_H

// Those crossections are independent on the species name. 
class Xsection_type1 : public Xsection 
{
  Scalar energy0;
  Scalar energy1;
  Scalar energy2;
  Scalar sigmaMax;
  Scalar const1;
  Scalar const3;

 public:

//type 1 methods
  Xsection_type1(Scalar e0, Scalar e1, Scalar e2, Scalar sMax, ReactionType _name=NO_TYPE)//:Xsection(T)
  { 
    energy0 = e0;
    energy1 = e1;
    energy2 = e2;
    sigmaMax = sMax;
    const1 = sigmaMax/(energy1 - energy0);
    const3 = sigmaMax * energy2 / log(energy2);
    name = _name;
  }

  Scalar sigma(Scalar energy) const
  {
    if (energy <= energy0) return 0;
    if (energy <= energy1) return (energy - energy0) * const1;
    if (energy <= energy2) return sigmaMax;
    return const3 * log(energy) / energy;
  }
  Scalar get_threshold() const {return energy0;}
//  Scalar value(Scalar energy) const {return 0.;}
};

class Xsection_type2 : public Xsection ////t.
{
  Scalar a;
  Scalar b;
	
 public:
  //type 2 methods
  Xsection_type2(Scalar _a, Scalar _b, ReactionType _name=NO_TYPE)//:Xsection(T)
  {
    a = _a;
    b = _b;
    name = _name;
  }
  ~Xsection_type2(){}
  Scalar sigma(Scalar energy) const
  {
    return a + b / (sqrt(energy) + 1E-30);
  }
 // Scalar value(Scalar energy) const {return 0.;}
};



#endif
