/////////////////
// mcc.hpp     //
/////////////////

 /*
 # Revision history:
 JTG and MAL melded file, 10/12/09
 CH Lim (17Dec2004) : Original code.
 CH Lim (25May2005) : Revise the excitation for the relativistic calculation
 C. Nguyen & CH Lim (21July2006) :
 - General structure: an MCC is characterized by a set of cross sections,
 with a flexible number of colliders of desired type / multiple MCC can be loaded,
 only characterized by a name given in the input file
 - MCC method: all colliders (including the gas) are from the species class.
 They might be represented as fluids or/and particles. Particle/particle,
 and fluid/particle collisions are taken into account. Variable weight is allowed.
 - Kinetics: dynamics for the second particle is also encountered. The possibility
 to delete a particle has been added.
 C. Nguyen (15thSep2006): New structure. MCC derives from the CollisionPackage
 (protected) class (previously the class gas). And the peculiar collision packages
 (argon, helium, CH4_gas...) derive from MCC.
 This allows to modify the reaction kinetics, according to the particular species we
 are interested in. Default functions in mcc work for argon-like basis atoms colliding
 with electrons, or their associated cation.
 CH Lim (15thSep2006): Sorting process for particle-particle collisions
 JK, DQW (25thSep2019): getting data for reaction/position diagnostics (plotting spatial distribution of reactions)
--------------------------------------------------------------------------------------
 # Some issues that might be discussed (or challenged, or might require some more work):

 - An MCC class is characterized by a set of cross sections and a basis species present
 in all the reactions. This species is refered to as gas, the other one as collider. Gas
 and collider are different from the duality projectile/target.
 - Our current applications for the mcc class only encounter collisions between exactly two
 of the three following types of species: neutral, ion or electron (we don't have electron-electron
 collisions for example). Therefore we choose to recognize them by eSpecies, iSpecies, or nSpecies.
 The drawback is that reactions, elastic() for example, only seem to work for electrons (eSpecies
 is for example used in elastic()), and that we can apparently not do collisions between species of
 the same category. The problem is simply solved by recognizing the species by Species1 and Species2
 instead...but with a careful understanding of who is who in the reactions (example: 1 is allways the
 elctron if there is one...) since we often need the identity of a species for our reactions (and not
 only kinetic properties).
 - subcycle: for the moment, two colliding particles have to have the same subcycle. Should be later
 developed
 - extra[id] is allways negative: it is  (-) the excess of collision, kept in memory for next time step
 (Eventually we may proceed the opposite way, by keeping in memory a positive number, or we can use
 a probability...which nay be better).
--------------------------------------------------------------------------------------
 # To be done:

 - There is a need for work in the kinetics (particularly for the modeling of molecules).
 - Excitation: more accurate formula should be used? (approximate  energy /
 momentum conservation for the moment).
 - Fluid: form a collision operator for fluids, determine a process for
 fluid-fluid collisions (idea: sample a certain number of particles...like before).
 -------------------------------------------------------------------------------------
*/
 // mindgame: restructuring, cleanup, and bug fixes (Dec. 9, 2006)
 //           to replace mcc.hpp eventually

#ifndef MCC_H
#define MCC_H
// #define DEBUGmcc
// #define DEBUGmccdynamic //debug calls to the various reactions in MCC::dynamic

#include "utils/Array.hpp"
#include "utils/ovector.hpp"
#include "reactions/reaction.hpp"
#include "utils/message.hpp"
#include "main/parse.hpp"
#include "main/particlegroup.hpp"

// forward declarations
class Fluid;
class Xsection;
class XsectionSet;
class Reaction;
class ReactionList;
class Sort;
class DataArray;
class Species;

const Vector3 DELETE_PARTICLE = 3.24E+22;  // Artificial value. If returned, it indicates a particle has to be deleted.
const Scalar MAX_ENERGY = 1000;  // in eV

class Sort
{
	ParticleGroup *pg; // points to the ParticleGroup
	int index; // index within the ParticleGroup for each particle
public:
	Sort() {pg=NULL; index=0;};
	Sort(ParticleGroup *_pg, int _index) {pg=_pg; index=_index;};
	void set(ParticleGroup* _pg, int _index) {pg=_pg; index=_index;};
	inline Scalar key() {return pg->get_x(index);}; // in grid units!
	inline Vector3 vel() {return pg->get_v(index);};
	inline void set_v(Vector3 u) {pg->set_v(index, u);};
	inline void del() {pg->delete_particle(index);};
	inline Scalar get_q() {return pg->get_q(index);};
	inline Scalar get_qx() {return get_q()*key();};
	inline void set_E(Scalar E) {pg->set_E(index, E);};
	inline Scalar get_E() {return pg->get_E(index);};
};


// mindgame: generic mcc class
class MCC : public Parse, public Message // Added ": public Parse, public Message", MAL 11/21/09
{
public:
	MCC(SpeciesList * slist, ReactionList _reactionlist, Scalar _dt, Fields *tf);

	virtual ~MCC()
	{
		for( unsigned int i=0 ; i < collDistrib.size() ; i++) {
			delete collDistrib[i];
		}
	}

	void init_mcc(Species *_reactant1, Species *_reactant2);
  int set_subcycle_ff(Species *_reactant1, Species *_reactant2); // add, MAL200220
	void setdefaults_mcc();
	virtual void init(Species *_reactant1, Species *_reactant2)
	{
	init_mcc(_reactant1, _reactant2);
	}
	virtual void setdefaults()
	{
	setdefaults_mcc();
	}

	void particle_particle(Species *const targetspecies, Species *const projspecies);
	// mindgame: interaction between fluid part of targetspecies
	//          and particle part of projspecies.
	void fluid_particle(Species *const fluid_species, Species *const particle_species);
  void fluid_fluid(Species *const fspecies1, Species *const fspecies2); // add, MAL200214
	inline Scalar get_extra() const { return extra; } // needed for dumpfile, MAL 8/11/10
	inline void set_extra(Scalar _extra) { extra = _extra; } // needed for dumpfile, MAL 8/11/10

	// functions to find average number of collisions in one timestep, for icp heating and dumpfile, MAL 1/22/11
	inline long int get_totNcoll() { return totNcoll; } // for icp, MAL 1/8/11
	void time_average(); // for time average number of collisions in one timestep, MAL 1/9/11
	inline Scalar get_aveNcoll_per_step() { return aveNcoll_per_step; } // time-average number of collisions per timestep, MAL 1/8/11
	inline void set_aveNcoll_per_step(Scalar _ave) { aveNcoll_per_step = _ave; } // for dumpfile 1.02

	// return array for requested reaction; JK 2019-11-25
	DataArray * get_collision_distribution(int reaction) {
		return collDistrib[reaction];
	}
	// initialize the array for counting; JK 2019-11-25
	void init_collision_distribution(void){
		for( unsigned int i=0 ; i < collDistrib.size() ; i++) {
			collDistrib[i]->zero();
		}
	}

	// enable/disable collecting reaction data for diagnostics
	// JK 2019-11-27
	void set_collect_4_diagnostics(bool flag) { collect_4_diag = flag; }
	void allocate_diagostics_arrays(void);				// allocate arrays

	// add for access to max_sigmaV in ReactionGroup::update, MAL200116
	inline Scalar get_maxSigmaV() const { return maxSigmaV; }
	void init_sumSigmaV(Species *sp); // modified for radiation trapping, MAL 1/1/11; move from private to public access MAL200116

protected:

#ifdef DEBUGmcc
	int stepnumber;
  int totalnumberofcollisions;
#endif
	int Nelastic; //for debug, MAL 9/6/12
	int Nionization;
	int Nchargetransfer;
	int Nsteps;
	int Nmutneut;
	int Ndissattach;

	// variables to find average number of collisions in one timestep, for icp heating, MAL 1/9/11
	long int totNcoll; // for icp calculation, MAL 1/8/11
	long int totNcoll_old; // at previous timestep
	Scalar Ncoll; // number of collisions in one timestep
	Scalar tempNcoll;
	Scalar aveNcoll_per_step; // used in fields.cpp to calculate electron-neutral collision frequency num

	Fields *thefield;
	SpeciesList *specieslist;

	// Main functions for the calculation of kinematics
	// virtual void dynamic(const Xsection& cx, ReactionType reactiontype); // obsolete, MAL 1/4/11
	// add to reduce number of kinematic functions needed; gives access to ALL parameters of a reaction (e.g., product species), MAL 12/27/10
	virtual void dynamic(Reaction * _reaction);
  virtual void dynamic_fluids(Reaction * _reaction); // NOT USED; USE dynamic instead; MAL200303
	virtual void eElastic(const Xsection& cx); // electron + much higher mass species elastic collision
	virtual void eExcitation(const Xsection& cx); // electron + much higher mass species excitation collision
	virtual void samemassPenningIz(const Xsection& cx, Scalar ReleasedEnergy); // Penning ionization due to collisions of excited argon species, added MAL 11/26/10
	virtual void anymassPenningIz(const Xsection& cx, Species* si, Species* sn, Scalar ReleasedEnergy); // Penning ionization due to collisions of excited atoms with other neutrals, add SAS July 7 2014
	virtual void photonEmission(const Xsection& cx); // add MAL 12/27/10
	virtual void samemassExcitation(const Xsection& cx); // add MAL 12/27/10
	virtual void samemassElastic(const Xsection& cx); // elastic collision for species of similar mass
	virtual void halfmassElastic(const Xsection& cx); // elastic collision for species with m1/m2 = 0.5  added by JTG May 20 2009
	virtual void anymassElastic(const Xsection& cx, Species* si, Species* sn); // elastic collision for any ratio of mi/mn, MAL 11/20/09
	virtual void BackwardsAnymassElastic(const Xsection& cx, Species* si, Species* sn); // backwards cx see phelps94 (SAS July 9 2014)
	virtual void BackwardsSamemassElastic(const Xsection& cx); // backwards cx see phelps94 (SAS July 9 2014)
	virtual void eIonization(const Xsection& cx); // electron ionization
	virtual void ionization (const Scalar threshold);
	virtual void ElectronExchange(const Xsection& cx); // an electron is exchanged between a neutral and a daughter ion
	// modify this to pass products and ReleasedEnergy, MAL 10/7/12
//	virtual void nonresElectronExchange(const Xsection& cx); // an electron is exchanged between a neutral and a non-daughter ion, MAL 11/20/11
	virtual void nonresElectronExchange(Species * ProductIon, Species * ProductNeutral, Scalar ReleasedEnergy = 0.); // an electron is exchanged between a neutral and a non-daughter ion, MAL 11/20/11
	void eScattering_NR();
	void eScattering_R();

	// Subfunctions for the calculation of kinematics
	virtual Vector3 newMomentum(const Vector3& U_intial, const Scalar& energy,
			  const Scalar& theta, const Scalar& phi);
	virtual Scalar elasticEnergy(const Scalar& eImpact, Scalar& thetaScatter);
	virtual Vector3 newcoordiante(Vector3 vel , Scalar theta, Scalar phi);
	virtual Scalar ejectedEnergy(const Scalar& thres, const Scalar& impactEnergy);
	virtual void primarySecondaryAngles(const Scalar& thres,
				  const Scalar& impactEnergy,
				  const Scalar& ejectedEnergy,
				  Scalar& thetaPrimary, Scalar& phiPrimary,
				  Scalar& thetaSecondary, Scalar& phiSecondary);
	virtual void elasticScatteringAngles(const Scalar& eImpact,
				   Scalar& thetaScatter, Scalar& phiScatter);
	//  Vector3 elasticMomentum(const Vector3& U_intial, const Scalar& energy); What is this?

	void add_particles(Species* sp, const Vector3 & vel);
	void add_product(Species* sp, const Vector3 & vel); // adds fluids as well as particles, MAL200208
	Vector3 add_or_delete_neutral(Species& speciesNeutral, Vector3 uNeutral);  // Adds or deletes PIC neutral if above or below energyThrToAddAsPIC, MAL 11/20/11
	// Variables for the calculation of kinematics
	// - Used to identify the species in the collision (refer to the colliders)
	Species* eSpecies;
	Species* iSpecies;
	Species* nSpecies;
	Species* fSpecies; // set to NULL in particle_particle; set to the fluid species in fluid_particle, MAL200211
	bool is_fluidfluid;
	int subcycle_ff; // the smallest subcycle value for the evolving fluids, used in fluid_fluid, MAL200220
	// Possible product species, MAL 12/27/10
	Species* sec_e;
	Species* sec_i; // for example, Ar_ion
	Species* sec_n; // for example, Ar, Arm, Arr, Ar4p

	// Data about kinematics
	Scalar x;
	Vector3 ue, ui, un;
	Scalar v;
	Scalar energy;
	Scalar w_ref; // Reference weight of a collision
	// mindgame: the better way is to get nc2p from the inputfile
	bool useSecDefaultWeight; // secondary species are created with their default weight

	void qsort(Species & targetspecies); // Could those variables and functions be private?
	void indexsort(Sort *data);
	int partition(Sort *data, int l, int r);
	Sort *sortKeys;
	int *indexsorts;
	int nParticles;
	bool sortflag;
	Species *reactant_for_sigmaV;
	ReactionList reactionlist;

	// JK, DQW - 2019-11-25
	// vector of DataArray lists to store collision distributions
	vector<DataArray *> collDistrib;
	bool collect_4_diag;		// do we collect data for diagnostics or not; default is not. JK, 2019-11-27

private:
	// Partners Storage:
	// Functions:
	void setRadiationEscapeFactor(Reaction * _reaction); // add MAL 12/30/10
	// for radiation trapping, gets product of cross section and radiation escape factor;
	// returns cross section for all other reactions, MAL 1/1/11
	inline Scalar get_crosssection(Reaction * _reaction, Scalar _energy);
	void particle_particle(Species *const targetspecies, Species *const projspecies, Scalar Ntot_particles);
	// mindgame: assign velocity pointers appropriately.
	void set_velocity_pointers(Vector3 **u, const Species *const sp);
	// for ion-ion and neutral-neutral collisions, MAL 4/17/09
	void set_velocity_pointers(Vector3 **uone, const Species *const spone, Vector3 **utwo, const Species *const sptwo);

	// Constants and Variables used in the functions:
	Scalar dt;
	Scalar maxSigmaV; // initialized to 0; set in ReactionGroup::update, MAL200117
	Scalar extra; // negative number indicating the number of physical collisions done in excess in the previous time step
};
#endif
