#ifndef HYDROCARBONS_MCC_H
#define HYDROCARBONS_MCC_H

#include "MCC/mcc.hpp"
#include "main/oopiclist.hpp"
#include "utils/ovector.hpp"

class ReactionList;
class Species;
class Xsection;

class Hydrocarbons_MCC: public MCC
{
protected:
  Species* sec_iminusH;  // CH{y-1}+ , where the main species is of the form CHy or CHy+
  Species* sec_iminusH2; // CH{y-2}+
  Species* sec_nminusH;  // CH{y-1}
  Species* sec_nminusH2; // CH{y-2}
  Species* sec_nminusH3; // CH{y-3}
  Species* sec_H;  // Hydrogen
  Species* sec_H2; // Dihydrogen
  Species* sec_p;  // Proton H+
  // Released kinetic energy
  Scalar DIeEnergyloss, DIenergy, CXhenergy;
  Scalar DE1energy, DE2energy, DE3energy, DE4energy, DE5energy;
  Scalar DR1energy, DR2energy, DR3energy, DR4energy, DR5energy;

public:
  Hydrocarbons_MCC(oopicList<Species>* slist, ReactionList _reactionlist, Scalar _dt, Fields *tf);
  void setdefaults();
//  void dynamic(const Xsection& cx, ReactionType reactiontype); // obsolete, MAL 1/4/11
  void dynamic(Reaction * _reaction); // add for compatibility to mcc.cpp, 1/4/11
  void init(Species *species1, Species *species2);

  // Reactions:
  /////////////

  // Reactions on neutral species:
  void eElastic(const Xsection& cx);
  void eExcitation(const Xsection& cx);
  void pElastic(const Xsection& cx); // no data found
  void eDissExcitation1(const Xsection& cx);
  void eDissExcitation2(const Xsection& cx);
  // virtual void eIonization(const Xsection& cx); // No data found. Default model is used.
  void eDissIonization(const Xsection & cx);
  void pElectronExchange(const Xsection& cx);
  void pHydrogenExchange(const Xsection& cx);

  // Reactions on ion species:
  void eDissExcitation3(const Xsection& cx);
  void eDissExcitation4(const Xsection& cx);
  void eDissExcitation5(const Xsection& cx);
  void eDissRecombination1(const Xsection& cx);
  void eDissRecombination2(const Xsection& cx);
  void eDissRecombination3(const Xsection& cx);

  // Functions used in common:
  void Any_eExcitation(const Xsection& cx, Scalar targetmass);
  void Any_DissociationInTwo(Vector3 & target_velocity, Species* s1,
			     Species* s2, Scalar ReleasedEnergy);

  void Any_DissociationInThree(Vector3 & target_velocity, Species* s1,
			  Species* s2, Species* s3, Scalar ReleasedEnergy);

};

#endif
