#ifndef COLLISIONTYPE
#define COLLISIONTYPE

/* 
If a neutral collision type is created it should be added in mcc.cpp in the function is_neutral()
*/

enum Collisiontype
{
  //
  NOTYPE,
  //electrons
  E,
  //ions: for the moment it simply means a cation (of order one: X+)
  Ar_ions, He_ions, H_ions, Ne_ions, CH4_ions,  CH3_ions, CH2_ions, CH_ions, C_ions, Air_ions,  
  // neutrals: atoms and methan derivatives.
  Ar, He, H, H2, Ne, CH4,  CH3,  CH2, CH,  C, Air,
  // Fake
  Fake_type
}; 

#endif
