// JTG and MAL melded file, 10/12/09
#ifndef OXYGEN_MCC_H
#define OXYGEN_MCC_H

#include "utils/ovector.hpp"
#include "MCC/mcc.hpp"
#include "main/oopiclist.hpp"

class Species;
class Xsection;
class Reaction;

class OXYGEN_MCC: public MCC
{
protected:
  Species* sec_O2_sing_delta;  // Revised by JTG February 10 2015
  Species* sec_O2_sing_sigma;  // Revised by JTG February 10 2015
  Species* sec_O2_rot;
  Species* sec_O2_vib1;
  Species* sec_O2_vib2;
  Species* sec_O2_vib3;
  Species* sec_O2_vib4;
  Species* sec_O2_ion;
  Species* sec_O_ion; // Added by JTG June 23 2009
  Species* sec_O_1D; // Added by JTG July 20 2009
  Species* sec_O_1S; // Added by JTG July 20 2009
  Species* sec_O2;
  Species* sec_O;
  Species* sec_O_neg;
  Species* sec_O2_lexc;
  // Released kinetic energy
  Scalar eEnergyloss, DIenergy, CXhenergy;
  Scalar E1energy, E2energy, E3energy, E4energy, LEenergy;
  Scalar ELenergy1, ELenergy2, ELenergy3, ELenergy4;
  Scalar DR1energy, DR2energy, DR3energy, DR4energy, DR5energy;

public:
  OXYGEN_MCC(oopicList<Species>* slist, ReactionList _reactionlist, Scalar _dt, Fields *tf);
  void setdefaults();
//  void dynamic(const Xsection& cx, ReactionType reactiontype); // obsolete, MAL 1/4/11
  void dynamic(Reaction * _reaction);
  void init(Species *species1, Species *species2);

  // Reactions:
  /////////////

  // Reactions on neutral species:
  //  void eElastic(const Xsection& cx);
   //////////////////////////////////////////////////////////
  //Every reaction with loss and excitation goes with eExcitation
  void erotational(const Xsection& cx);
  void eExcitation1(const Xsection& cx);
  void eExcitation2(const Xsection& cx);
  void eExcitation3(const Xsection& cx);
  void eExcitation4(const Xsection& cx);
  void elExcitation(const Xsection& cx);
  void esingdelta(const Xsection& cx);
  void esingdeltadex(const Xsection& cx);    // Added by JTG January 9 2013
  void esingsigmadex(const Xsection& cx);    // Added by HH February 9 2016
  void esingsigma(const Xsection& cx);
  void eloss(const Xsection& cx);
  void eloss2(const Xsection& cx);  // Added by JTG July 26 2009
  void eloss3(const Xsection& cx);  // Added by JTG July 26 2009
  void eloss4(const Xsection& cx);  // Added by JTG July 26 2009
  void eexc1d(const Xsection& cx); // Added by JTG July 20 2009
  void eexc1ddex(const Xsection& cx); // Added by JTG January 10 2013
  void eexc1s(const Xsection& cx); // Added by JTG July 20 2009
  //void eloss2(const Xsection& cx);
  //void eloss3(const Xsection& cx);
  //void eloss4(const Xsection& cx);
  //////////////////////////////////////////////////
  void eattachment(const Xsection& cx);
  void pElectronExchange(const Xsection& cx); //Added by JTG June 24 2009
  void p2ElectronExchange(const Xsection& cx); //Added by JTG July 26 2009
  void negelastic(const Xsection& cx);
  void negdetachment(const Xsection& cx);
  void negodetachment(const Xsection& cx);  //Added by JTG July 17 2009
  void fragmentation(const Xsection& cx);  //Added by JTG August 17 2009
  void dissionization(const Xsection& cx);  //Added by JTG August 18 2009
  void eIonizationo(const Xsection& cx);  //Added by JTG September 3 2009
  void eIonizationo2(const Xsection& cx);  //Added by JTG September 3 2009
  void edissattach(const Xsection& cx);
  //reaction on ion species
  void mutualneu(const Xsection& cx);
  void mutualneuo(const Xsection& cx);  // Added by JTG July 21 2009
  void o1dtoo2b(const Xsection& cx);  // Added by JTG February 10 2015
  void o2bo2quench(const Xsection& cx);  // Added by JTG February 13 2015
  void polardisso(const Xsection& cx);  // Added by JTG July 28 2009
  void chargeexchange(const Xsection& cx); // chaege -> charge, MAL, 4-13/09
  void edissrecombination(const Xsection& cx);

  //reaction on neg O
  void edetachment(const Xsection& cx);

   // Functions used in common:
  void Any_eExcitation(const Xsection& cx, Scalar targetmass);
  void Any_DissociationInTwo(Vector3 & target_velocity, Species* s1,
			     Species* s2, Scalar ReleasedEnergy);
  void Any_DissociationInTwo(Vector3 & Vr1, Vector3 & Vr2, Scalar Mr1, Scalar Mr2, Species* s1, Species* s2, Scalar Ereleased); //Ethr->Ereleased
  void Any_DissociationInThree(Vector3 & target_velocity, Species* s1,
			  Species* s2, Species* s3, Scalar ReleasedEnergy);
  void Any_DissociationInThree(Vector3 & Vr1, Vector3 & Vr2, Scalar Mr1, Scalar Mr2, Species* s1, Species* s2, Species* s3,
			       Scalar Edissociation); // Ethr -> Ereleased, just for the meaning of this energy, MAL 10/2/12
  void Any_eIonization(const Xsection& cx, Species* s1);
  void Any_eIonization2(const Xsection& cx, Species* s1);
  void Any_eDetachment(const Xsection& cx, Species* s1);
};

#endif
