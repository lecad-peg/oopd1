// Aug, 10, 2012
#ifndef CHLORINE_MCC_H
#define CHLORINE_MCC_H

#include "MCC/mcc.hpp"
#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"

class Species;
class Xsection;

class CHLORINE_MCC: public MCC
{
protected:

  Species* sec_Cl;
  Species* sec_Cl_neg_ion;
  Species* sec_Cl2;
  Species* sec_Cl2_ion;
  Species* sec_Cl_ion;


  // Released kinetic energy
  Scalar eEnergyloss, DIenergy, CXhenergy;
  Scalar E1energy, E2energy, E3energy, E4energy, LEenergy;
  Scalar ELenergy1, ELenergy2, ELenergy3, ELenergy4;
  Scalar DR1energy, DR2energy, DR3energy, DR4energy, DR5energy;

public:
  CHLORINE_MCC(oopicList<Species>* slist, ReactionList _reactionlist, Scalar _dt, Fields *tf);
  void setdefaults();
//  void dynamic(const Xsection& cx, ReactionType reactiontype); // obsolete, MAL 1/4/11
  void dynamic(Reaction * _reaction);
  void init(Species *species1, Species *species2);

  // Reactions:
  /////////////

  // Reactions on neutral species:
  //  void eElastic(const Xsection& cx);
   //////////////////////////////////////////////////////////
  //Every reaction with loss and excitation goes with eExcitation

	void eIonizationcl2(const Xsection& cx); 	// ionization: e + Cl2 -> Cl2+ + 2e
void edisssingleizcl2(const Xsection& cx);
void edissattachcl2(const Xsection& cx);
void polardisscl2(const Xsection& cx);
void eIonizationcl(const Xsection& cx);
  void edetachmentnegcl(const Xsection& cx);
  void negcl2detch(const Xsection& cx);
  void negcldetachment(const Xsection& cx);
  void edissrecposcl2(const Xsection& cx);
  void mnposcl2negcl(const Xsection& cx);
  void mnposclnegcl(const Xsection& cx);
  void eloss_cl2(const Xsection& cx);
  void eloss_cl(const Xsection& cx);
  void eloss_cl2_3pu(const Xsection& cx);
  void eloss_cl2_1pu(const Xsection& cx);
  void eloss_cl2_3pg(const Xsection& cx);
  void eloss_cl2_1pg(const Xsection& cx);
  void eloss_cl2_3su(const Xsection& cx);
  void eloss_cl2_r1pu(const Xsection& cx);
  void eloss_cl2_r1su(const Xsection& cx);
  void fragmentation_cl2(const Xsection& cx);
  void edissdoubleizcl2(const Xsection& cx);
  void edoubledetnegcl(const Xsection& cx);
///////////////////////////////////////////////////////////////



   // Functions used in common:
void Any_eExcitation(const Xsection& cx, Scalar targetmass);
  void Any_DissociationInTwo(Vector3 & target_velocity, Species* s1,
			     Species* s2, Scalar ReleasedEnergy);
  void Any_DissociationInTwo(Vector3 & Vr1, Vector3 & Vr2, Scalar Mr1, Scalar Mr2, Species* s1, Species* s2, Scalar Ereleased); //Ethr->Ereleased
  void Any_DissociationInThree(Vector3 & target_velocity, Species* s1,
			  Species* s2, Species* s3, Scalar ReleasedEnergy);
  void Any_DissociationInThree(Vector3 & Vr1, Vector3 & Vr2, Scalar Mr1, Scalar Mr2, Species* s1, Species* s2, Species* s3,
			       Scalar Edissociation); // Ethr -> Ereleased, just for the meaning of this energy, MAL 10/2/12
  void Any_eIonization(const Xsection& cx, Species* s1);
  void Any_eIonization2(const Xsection& cx, Species* s1);
  void Any_eDetachment(const Xsection& cx, Species* s1);
};

#endif
