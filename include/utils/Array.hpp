#ifndef ARRAY_H
#define ARRAY_H

#include "utils/ostring.hpp"
#include <stdlib.h>


template<class Type>

class Array{

public:
	Array()
{
		name = "";
		size = 0;
		array = 0;
		arrayAllocated = true;
}

	/************Constructor to take another array *************/
	/*                                                         */
	/*                    WARNING                              */
	/*                    WARNING                              */
	/*                    WARNING                              */
	/*  Can corrupt original data.  DO NOT assign values if you*/
	/*  use this constructor to point to another array         */
	/*  Will work on security fix.                             */
	/*                                                         */
	/***********************************************************/

	Array(ostring _name, int _size, Type* p_type)
	{
		name = _name;
		size = _size;
		array = p_type;
		arrayAllocated = false;
	}

	Array(ostring _name, int _size)
	{
		name = _name;
		size = _size;
		array = new Type[_size];
		arrayAllocated = true;
	}

	virtual ~Array()
	{
		if(arrayAllocated) { delete [] array;}
	}

	inline Type* operator()(void){ return array; }

	// sets first a.getSize() elements in array to values of a
	inline Array<Type> & operator= (Array<Type> & a)
	{
		Type *ptr = a();
		if(arrayAllocated)
		{
			if(getSize() < a.getSize())
			{
				if(array) { delete [] array; }
				array = new Type[a.getSize()];
			}
		}
		for(int i=0; i < a.getSize(); i++)
		{
			array[i] = ptr[i];
		}

		return *this;
	}

	inline Type* get_array(void) { return array; }

	inline Type& operator[](int index)
	{
		if(arrayAllocated)
		{
			if(index<size && index>=0)
			{
				return array[index];
			}
			else
			{
				exit(1);
			}
		} else return array+index;
	}

	inline Type  getValue(int index) { return array[index]; }
	virtual int  getSize() {return size;}
	virtual int* getSizePointer() {return &size;}
	virtual void setSize(int _size) {size = _size;}

	inline ostring getName() {return name;}
	inline void setName(ostring newName) { name = newName; }
	inline bool isArrayAllocated() {return arrayAllocated;}
	inline void setValue(Type value, int index)
	{
		if(arrayAllocated)
		{
			array[index] = value;
		}
		else
		{
			// FIX ME!!! WHY IS THIS HERE?!?!
			// JH, 5/9/2005
		}
	}

	void resize(int newsize){
		if(arrayAllocated){
			if(array) { delete [] array; }
			array = new Type[newsize];
		}
		setSize(newsize);
	}

	void resize_cp(int newsize)
	{
		if(!arrayAllocated)
		{
			resize(newsize);
			return;
		}
		Array<Type> a;
		a = *this;
		resize(newsize);
		*this = a;
	}

protected:
	Type* array;
	ostring name;
	int size;
	bool arrayAllocated;

};

#endif
