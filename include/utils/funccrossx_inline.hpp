inline Scalar FuncCrossX::function(Scalar energy, Scalar (*g)(Scalar))
{
  static Scalar (*f)(Scalar) = NULL;
  if(f == NULL)
    {
      if(g == NULL)
	{
	  fprintf(stderr, "\nError FuncCrossX:");
	  fprintf(stderr, "\n function() called before initialized\n");
	  exit(1);
	}
      else
	{
	  f = g;
	  return 0.;
	}
    }
  return f(energy);
}
