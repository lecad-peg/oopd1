#ifndef FUNCXSECTION_H
#define FUNCXSECTION_H

#include "utils/ovector.hpp"
#include "xsections/xsection.hpp"
#include "reactions/reactiontype.hpp"

class FuncXsection : public Xsection
{
protected:

  Scalar (*sigmaPtr)(Scalar energy);
  Scalar energy0;
  Equation *x_func;

public:
  //  FuncXsection(Type type){}//:Xsection(t){}
  FuncXsection(Scalar (*g)(Scalar), Scalar _threshold, ReactionType _name=NO_TYPE)//:Xsection(t)
   {
     function(0., g);
     sigmaPtr = g;
     name = _name;
     energy0 = _threshold;
     x_func = NULL;
   }
  FuncXsection(Equation *_x_func, Scalar _threshold, ReactionType _name=NO_TYPE)//:Xsection(t)
   {
     x_func = _x_func;
     name = _name;
     energy0 = _threshold;
   }

  Scalar value(Scalar energy) const;
  Scalar get_threshold() const {return energy0;}
  // get_atomicnumber(){return atomicnumber;}
  inline Scalar function(Scalar energy, Scalar (*g)(Scalar)=NULL) const;
  Scalar sigma(Scalar energy) const {if (x_func) return x_func->eval(energy); else return (*sigmaPtr)(energy);}
};

 inline Scalar FuncXsection::function(Scalar energy, Scalar (*g)(Scalar)) const
{
  static Scalar (*f)(Scalar) = NULL;
  if(f == NULL)
    {
      if(g == NULL)
	{
	  fprintf(stderr, "\nError FuncXsection:");
	  fprintf(stderr, "\n function() called before initialized\n");
	  exit(1);
	}
      else
	{
	  f = g;
	  return 0.;
	}
    }
  return f(energy);
}
#endif
