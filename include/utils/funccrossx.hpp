#ifndef FUNCCROSSX
#define FUNCCROSSX

#ifndef CROSSX
#ifndef CROSSX_DECLARED
class CrossX
#define CROSSX_DECLARED
#endif
#endif

#include "utils/ovector.hpp"
#include "xsections/crossx.hpp"

class FuncCrossX : public CrossX
{
protected:

  Scalar (*sigmaPtr)(Scalar energy);
  Scalar energy0;

public:
  //  FuncCrossX(Type type){}//:CrossX(t){}
  FuncCrossX(Scalar (*g)(Scalar), Scalar _threshold, Type _type)//:CrossX(t)
   {
     function(0., g);
     sigmaPtr = g;
     type = _type;
     energy0 = _threshold;
   }

  Scalar value(Scalar energy) const;
  virtual Scalar get_threshold() const {return energy0;}
  // get_atomicnumber(){return atomicnumber;}
  inline Scalar function(Scalar energy, Scalar (*g)(Scalar)=NULL) const;
  //  virtual Scalar sigma(Scalar energy) {return 0.;}
  virtual Scalar sigma(Scalar energy) const {return (*sigmaPtr)(energy);}
};

 inline Scalar FuncCrossX::function(Scalar energy, Scalar (*g)(Scalar)) const
{
  static Scalar (*f)(Scalar) = NULL;
  if(f == NULL)
    {
      if(g == NULL)
	{
	  fprintf(stderr, "\nError FuncCrossX:");
	  fprintf(stderr, "\n function() called before initialized\n");
	  exit(1);
	}
      else
	{
	  f = g;
	  return 0.;
	}
    }
  return f(energy);
}
#endif
