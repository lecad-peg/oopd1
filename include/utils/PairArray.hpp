// PairArray
// by HyunChul Kim, 2006

#ifndef PAIRARRAY_H
#define PAIRARRAY_H
#include "utils/ovector.hpp"
#include "main/parse.hpp"

template <typename Type1 = Scalar, typename Type2 = Scalar>
class PairArray
{
  vector<pair<Type1, Type2> > array;
public:
  int num() const {return array.size();}
  Type1 get_first(int i) const {return array[i].first;}
  Type2 get_second(int i) const {return array[i].second;}
  void add(Type1 x, Type2 y)
  {
    array.push_back(make_pair(x,y));
  }
  void add(pair<Type1, Type2> one_pair)
  {
    array.push_back(one_pair);
  }
  void print_scalar_array() const
  {
    for (unsigned int i=0; i<array.size(); i++)
      printf("\n%g %g", array[i].first, array[i].second);
  }
  Type2 get_second(Type1 x, bool _linear_outside = true) const
  {
    int num = array.size();
    if (num) {
      if (array[num-1].first <= x)
      {
	if (_linear_outside && num >= 2)
	  return linear_interpolation(array[num-2], array[num-1], x);
	else
	  return Type2(array[num-1].second);
      }
      else if (array[0].first >= x)
      {
	if (_linear_outside && num >= 2)
	  return linear_interpolation(array[0], array[1], x);
	else
          return Type2(array[0].second);
      }
      else
      {
        for (int i=num-1; i>0; i--)
        {
          if (array[i].first >= x && array[i-1].first < x)
          {
	    return linear_interpolation(array[i-1], array[i], x);
          }
        }
      }
    }
    return Type2(-1);
  }
  pair<Type1, Type2> get_pair(Type1 x) const
  {
    return make_pair(x, get_second(x));
  }
  PairArray make_array(vector<Type1> const &vector1) const
  {
    PairArray new_array;
    if (array.size())
      for (unsigned int i=0; i<vector1.size(); i++)
      {
        new_array.add(get_pair(vector1[i]));
      }
    return new_array;
  }
  void sort() {}
};

#endif
