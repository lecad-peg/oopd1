#ifndef VECTORARRAY_H
#define VECTORARRAY_H

#include "utils/ovector.hpp"
#include "utils/Array.hpp"
#include "main/pd1.hpp"

//template
class VectorArray:public Array<Vector3>
{
private:
  int vectdim; // dimension of the Vector3 (from 1 to 3)

public:
  VectorArray();
  VectorArray(ostring, int, Vector3*, int vdim);
  VectorArray(ostring, int);
  ~VectorArray(){}

  // accessor functions
  inline int    get_vdim() { return vectdim; }

  void   doDivision();
  Vector3 getAverageOfArray();
  void   doFourierTransform();
  void   resetValues(VectorArray data);
  void   allocateAccumulateArray();
  void   allocateFourierTransformArray();

  inline Vector3& operator[](int index){
    if(index<getSize() && index>=0){
      return array[index];
    }
    else{
      exit(1);
    }
  }
};

#endif
