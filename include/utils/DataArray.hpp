#ifndef DATAARRAY_H
#define DATAARRAY_H

#include "utils/ovector.hpp"
#include "utils/Array.hpp"
#include "main/pd1.hpp"

//template
class DataArray:public Array<Scalar>
{
public:
	DataArray();
	DataArray(ostring, int, Scalar*);
	DataArray(ostring, int, int);
	DataArray(ostring, int);
	~DataArray(){  }

	int label;		// additional label (e.g. dimension) used in some diagnostics
	void   zero(void);
	Scalar getAverageOfArray(void);

	inline Scalar& operator[](int index)
	{
		if(index<getSize() && index>=0) { return array[index]; }
		else { exit(1); }
	}
};
#endif
