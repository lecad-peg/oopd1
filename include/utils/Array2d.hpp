#ifndef ARRAY2D
#define ARRAY2D

#include "utils/ovector.hpp"
#include "utils/DataArray.hpp"

class Array2D : public DataArray
{
  int nrow; // number in a row
  int ncol; // number in a column
  Scalar **row;
public:
  Array2D(ostring &n, int nr, int nc);
  ~Array2D();

  inline int index(int i, int j)  { return j*nrow + i; }
  inline Scalar val(int i, int j) { return array[index(i,j)]; }
  inline Scalar operator()(int i, int j) { return val(i,j); }

  inline Scalar ** get_ptrptr(void) { return row; }
  inline Scalar *  get_row(int i) { return row[i]; }
};
#endif
