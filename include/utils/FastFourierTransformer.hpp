#ifndef __FASTFOURIERTRANSFORMER_HPP
#define __FASTFOURIERTRANSFORMER_HPP

#include <cmath>
#include <iostream>
using namespace std;
typedef float scalar;
const scalar PI = 3.1416;
const scalar TWOPI = 6.2832;


class FastFourierTransformer{

public:

  /* Default Constructor */
  FastFourierTransformer();

  /* Default Destructor */
  ~FastFourierTransformer();

  /* Pre:   Takes a real scalar data array of size size            */
  /* Post:  Returns the forward Fourier transformed array in place */
  /*        Since the data is real, lowest frequency, f0 and       */
  /*        highest frequency, f(size/2) components must be real.  */
  /*        data[0] stores f0, and data[1] stores f(size/2)        */
  /*        data[2] stores f1(real), data[3] stores f1(imag.)      */
  /*        data[4] stores f2(real), data[5] stores f2(imag.)      */
  /*        and so on...                                           */
  /*        see also functions returnMagnitude(scalar, int) and    */
  /*        returnPhase(scalar,int)                                */
  scalar* doForwardTransform(scalar* data, int size);

  /* Pre:   Takes a data array of size size                        */
  /* Post:  Returns the backward transformed array in place, which */
  /*        must be a real array.  This function also normalizes   */
  /*        the algorithm.                                         */
  scalar* doReverseTransform(scalar* data, int size);

  /* Returns magnitude of a data array stored in the order given in doForwardTransform(scalar, int)*/
  scalar* returnMagnitude(scalar* data, int size);

  /* Returns phase of a data array stored in the order given in doForwardTransform(scalar, int)*/
  scalar* returnPhase(scalar* data, int size);

private:

  /* scalar array storing magnitude */
  scalar* magnitude;

  /* scalar array storing phase */
  scalar* phase;

  /* true if magnitude array has been allocated */
  bool magnitudeAllocated;

  /* true if phase array has been allocated */
  bool phaseAllocated;

  /*  Swap scalar values held in a and b */
  void swap(scalar* a, scalar* b);

  /*********************************************************************/
  /* Replace `data' by its discrete Fourier transform, if `isign'is    */
  /* input as 1, or by its inverse discrete Fourier transform, if      */
  /* `isign' is input as -1. `data' is a complex array of length `nn', */
  /* input as a real array data[1..2*nn]. `nn' MUST be an integer      */
  /* power of 2 (this is checked for in pdc1)                          */
  /*********************************************************************/
  void four1 (float data[], int nn, int isign);
  
  /*******************************************************************/
  /* Calculates the Fourier transform of a set of 2n real-valued     */
  /* data points. Replaces `data' by the positive frequency half of  */
  /* its complex Fourier transform. The real-valued first and last   */
  /* components of the complex transform are returned as elements    */
  /* data[0] and data[1] respectively. `n' MUST be a power of 2.     */
  /* This routine also calculates the inverse transform of a complex */
  /* data array if it is the transform of real data. (Result in      */
  /* This case MUST be divided by `n'.)                              */
  /*******************************************************************/
  /* Modified from Fortran 1-based array to 0-based array indexing.  */
  /* Earlier, realft must be called in C++ using data-1.  However,   */
  /* all the array data has been subtracted 1 inside the algorithm.  */
  /* 6/03                                                            */
  /*******************************************************************/
  void realft(float data[], int nn, int isign);
  
};

#endif
