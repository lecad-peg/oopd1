/* Known Issues: */
/* 1. Does typedef float scalar really use float?  test by uncommenting main  */
/*    It seems that it outputs more than 7 digits.                            */
/* 2. Take out #include <iostream>, using namespace std, and main()           */
/*    in final copy.  Only included for cout.  No need for them in the class. */
/* 3. Have not tested the intervals (0,1)                                     */


#include <ctime>
typedef float scalar;
using namespace std;

/************************************************************************/
/* From "Random number generators: good ones are hard to find", S. Park */
/* and K. Miller, Communications of ACM, October 1988, pp 1192-1201.    */
/* This is from page 1195, and is to work on any system for which       */
/* maxint is 2**31-1 or larger. Due earlier to Schrage, as cited by P&M.*/
/*                                                                      */
/* Note: OK values for iseed are 1 through 2**31-2. Give it 0 or 2*31-1 */
/* and it will return the same values thereafter!                       */
/*                                                                      */
/* C version 6/91, Bruce Langdon.                                       */
/* C++ version 6/03, Alan Wu.                                           */
/*                                                                      */
/* Algorithm replaces seed by mod(a*seed,m). First represent            */
/* seed = q*hi + lo.  Then                                              */
/* a*seed = a*q*hi + lo = (m - r)*hi + a*lo = (a*lo - r*hi) + m*hi,     */
/* and new seed = a*lo - r*hi unless negative; if so, then add m.       */
/*                                                                      */
/* Generates a scalar number between [0,1].                             */
/* Can use randomFlag to choose between initializing seed with          */
/* either system time or same value each time.                          */
/* Default constructor uses chooses system time.                        */
/************************************************************************/

class RandomScalarGenerator{

public:

  /* Default Constructor         */
  /* System Time as Initial Seed */
  RandomScalarGenerator();

  /* Constructor Allowing Choice for Initial Seed */
  RandomScalarGenerator(bool);

  /* Method returning a random scalar in the interval [0,1] */
  scalar makeRandomScalar();

  /* Method allowing choice for returning a scalar in the intervals: */
  /* [0,1] - open0 = false, open1 = false                            */
  /* [0,1) - open0 = false, open1 = true                             */
  /* (0,1] - open0 = true , open1 = false                            */
  /* (0,1) - open0 = true , open1 = true                             */
  scalar makeRandomScalar(bool, bool);

private:
  /* Flag to indicate whether seed should */
  /* be system time or the same each time */
  bool randomFlag;

  /* Seed used to initialize the random number generator */
  long seed;

  /* Initialize seed either using system time or a default number */
  long initSeed();
};

