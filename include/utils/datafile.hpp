// ScalarDataFile class
// by HyunChul Kim, 2006
#ifndef DATAFILE_H
#define DATAFILE_H

#include "utils/ovector.hpp"
#include "utils/PairArray.hpp"

class ScalarDataFile
{
  PairArray<> pairarray;
public:
  ScalarDataFile(char const *_filename)
  {
    readfile(_filename);
  }
  void readfile(char const *filename)
  {
    double x,y;
    FILE *fp;
    if ((fp = fopen(filename, "r")) != 0)
    {
      char buffer[STR_SIZE];
      while (fgets(buffer, STR_SIZE-1, fp) != 0)
      {
        if (buffer[0] != '#')
        {
	  if (sscanf(buffer, "%lf %lf", &x, &y) >= 2)
	  {
	     pairarray.add(x, y);
	  }
	}
      }
      fclose(fp);
    }
    else
      fprintf(stderr, "\nfailed to read the file '%s'\n", filename);
  }
  PairArray<> get_pairarray() const
  {
    return pairarray;
  }
};
#endif
