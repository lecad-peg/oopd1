#ifndef EQARRAY_H
#define EQARRAY_H

#include "main/oopiclist.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"

// EqArray
class Equation;

class EqArray
{
private:
  oopicList<DataArray> darrays;
  oopicList<VectorArray> varrays;
  Equation *theq; //

public:

  ~EqArray();

  void setdefaults();

  // functions for Array (Data, Vector) manipulation
  void add_array(DataArray *thed);

  // parsing functions
  void parse_rhs(ostring rhs);
};
#endif
