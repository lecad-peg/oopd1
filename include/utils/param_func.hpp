#ifndef PARAM_FUNC
#define PARAM_FUNC

#include<stdio.h>
#include<stdarg.h>

#include "main/pd1.hpp"
#include "utils/ostring.hpp"
#include "main/oopiclist.hpp"
#include "utils/equation.hpp"

// functional parameter
class Func
{
 private:
  ostring name;
  ostring desc;
  bool def;
  Equation *theq;

  Scalar *theval;
  int nvar;

 public:
  oopicList<ostring> varnames;
  oopicList<ostring> orig_vars;
  Func(ostring nname, Equation *teqp, oopicList<ostring> & vn)
    {
      desc = "";
      theq = teqp;
      name = nname;
      nvar = vn.nItems();
      varnames = vn; 

      set_function();
    }

  Func(ostring nname, Equation *teqp, int nc, ...)
    {
      desc = "";

      char *place;
      va_list ap;
      
      theq = teqp;
      // mindgame: I doubt that this is necessary.
    //  varnames.destructive = true;
      
      name = nname;
      nvar = nc;

      va_start(ap, nc);
      while(nc--)
	{
	  place = va_arg(ap, char *);
	  varnames.add(new ostring(place));
	}
      va_end(ap);
      set_function();
    }

  ~Func()
    {
      varnames.deleteAll();
      orig_vars.deleteAll();
    }
  

  // accessor functions
  inline Equation * get_equation() const  { return theq; }
  inline void set_equation(Equation *te) { theq = te; }
  inline ostring get_name() const { return name; }
  inline ostring get_description() const { return desc; }
  inline void set_description(const ostring & _desc) { desc = _desc; }

  bool is_dependent(ostring testvar)
    {
      oopicListIter<ostring> thei(orig_vars);

      for(thei.restart(); !thei.Done(); thei++)
	{
	  if((*thei()) == testvar)
	    {
	      return true;
	    }
	}
      return false;
    }

  // fparse related functions
  void set_orig_variables(Equation *testeq)
    {
      orig_vars.deleteAll();

      int n = testeq->nvar();
      for(int i=0; i < n; i++)
	{
	  orig_vars.add(new ostring(testeq->get_variable(i)));
	}
    }

  ostring varstring(void)
    {
      ostring retval;
      bool flag;
      oopicListIter<ostring> thei(varnames);

      flag = false;
      for(thei.restart(); !thei.Done(); thei++)
	{
	  if(flag) 
	    {
	      retval += ostring(",");
	    }
	  flag = true;
	  retval += (*thei());
	}
      return retval;
    }

  ostring lhs(void)
    {
      ostring retval;

      retval = name;
      retval += "(";
      retval += varstring();
      retval += ")";

      return retval;
    }

  ostring lhseq(void)
    {
      ostring retval;
      retval = lhs();
      retval += "=";
      return retval;
    }

  ostring get_rhs(void)
  {
    if(theq)
      {
	return theq->get_rhs();
      }
    return ostring("[uninitialized]");
  }

  ostring reparse(Equation *oldeq)
    {
      ostring eqstring;
      set_orig_variables(oldeq);

      eqstring = lhseq();
      eqstring += oldeq->get_rhs();         

      return eqstring;
    }

  void set_function()
    {
      oopicListIter<ostring> thei(varnames);

      theq->reset_variables();

      theq->set_equation_name(name);
      for(thei.restart(); !thei.Done(); thei++)
	{
	  theq->set_variable(*(thei()));
	}    
    }

  void set_constant(Scalar xyz)
    {
      oopicListIter<ostring> thei(varnames);

      theq->reset_variables();
      theq->setconstant(xyz);

      theq->set_equation_name(name);
      for(thei.restart(); !thei.Done(); thei++)
	{
	  theq->set_variable(*(thei()));
	}    
    }

};
#endif
