#ifndef __EQUATION_H
#define __EQUATION_H

#include "utils/ovector.hpp"
#include "utils/ostring.hpp"
#include "utils/fparser.hh"
#include<vector>
using namespace std;

#define CONSTTYPE double

class Constant
{
 public:
  CONSTTYPE c;
  ostring name;
  Constant(CONSTTYPE ccc, ostring nnn) { c = ccc; name = nnn; }
};


class Equation
{
 private:
  FunctionParser * fparse;
  vector<ostring> ind;
  ostring therhs;
  ostring dep;
  ostring wholeeq;
  bool fatal;

  bool constflag;
  Scalar* kon;
  bool konallocflag;
  bool initfparseflag; // mindgame: check whether fparse is allocated or not

  vector<Equation *> globaleqs;
  vector<Constant> constants;

  double *buff;  // fparser uses doubles by default

 public:
  Equation();
  Equation(ostring eq);
  Equation(Scalar C);
  Equation(Scalar *C);
  Equation(const Equation&);
  ~Equation();
  Equation &operator = (const Equation&);
  void copy(const Equation &);

  // accessor functions
  inline  int nvar() {       return ind.size();     }
  inline  bool is_constant() { return constflag;     }
  inline  bool is_fparsed() { return initfparseflag;     }
  inline  ostring get_full_name() { return wholeeq; }
  inline  ostring get_equation_name() { return dep; }
  inline  void set_equation_name(ostring a) { dep = a; }
  inline  ostring get_variable(int i)
    {
      if((i >= int(ind.size())) || (i < 0)) { return ostring(""); }
      return ind[i];
    }
  inline vector<ostring> get_variables() { return ind; }
  inline void set_variable(ostring newvar)
    {
      ind.push_back(newvar);
      if(buff) { delete [] buff; }
      buff = new double[ind.size()];
    }
  inline void reset_variables() { ind.clear(); }

  inline ostring get_rhs()
  {
    ostring retval = therhs;
    if(kon)
      {
	if(*kon)
	  {
	    char tmpstr[16];
	    // mindgame: use snprintf instead of sprintf
	    //          to prevent buffer overflow
	    snprintf(tmpstr, 16*sizeof(char), "%g", *kon);
	    if(retval.length() && (*kon > 0.)) { retval += "+"; }
	    retval += tmpstr;
	  }
	else
	  {
	    if(!retval.length()) { retval = "0"; }
	  }
      }

    if(!retval.length()) { return ostring("."); }
    return retval;
  }

  inline void Optimize()
  {
    if(fparse) { fparse->Optimize(); }
  }

  bool is_independent(ostring testvar);

  // toggle fatal errors
  inline void set_fatal()    { fatal = true;  }
  inline void set_nonfatal() { fatal = false; }

  // only one FunctionParser per Equation (for now)
  inline FunctionParser * get_fparser()
    {
      return fparse;
    }

  int setconstant(Scalar C);

  // evaluation functions and front ends --
  // NOT type safe!
  Scalar eval(double *args);
  Scalar eval(float  *args);
  Scalar eval(); // for constant functions (zero independent vars)
  Scalar eval(Scalar a, ...); // for more than zero independent vars
  Scalar get_value(Scalar a);

  ostring getparsestring();

  // add global Constants and Equations
  void add_Constant(ostring nn, Scalar vv);
  void add_Equation(Equation *eeqq);

  int initfparser();

  int parseequation(ostring eqn);
  void parseall(ostring theq);

  void on_error(const char *msg="");
  void printdebuginfo(); // print information for debugging

};
#endif
