#ifndef VARIABLEARRAY_H
#define VARIABLEARRAY_H
#include "utils/ovector.hpp"
#include "utils/DataArray.hpp"
// macros for defaults
// add default for naverage of 1 billion, MAL 6/12/09
#define NCOMB 4
#define NSTEPS 8192
#define NAVERAGE 1000000000
#define REALLOC_MULT 2

// class VariableArray : variable size array
class VariableArray : public DataArray
{
protected:
	int locsize;  // number of active elements
	int *ls;      // pointer to localsize (FIX ME!!!, JH, 12/04)
	int ncomb;    // inverse to fineness of comb
	int update_interval;
	int interval;
	int realloc_mult;
	bool cp_on_realloc;

	// pointer to the function to be called  when the array is full
	void (VariableArray::*maxsizefunc)(void);

public:
	VariableArray();
	VariableArray(ostring _n, int _s);
	VariableArray(ostring _n, int _s, int * _ls);
	VariableArray(ostring _n, int _s, int _label);

	void setdefaults(void);
	void update(Scalar tnew);

	// accessor functions
	void set_ncomb(int n) { ncomb = n; }

	// functions to call when the array is full
	void comb(void);
	void realloc(void);

	void set_realloc(bool to_copy = true);

	// virtual functions from Array
	int  getSize() {return locsize;}
	int* getSizePointer() {return &locsize;}
	void setSize(int _size) {locsize = _size;}
};
#endif
