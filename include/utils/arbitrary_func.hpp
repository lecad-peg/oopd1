// Arbitrary_Func class
// by HyunChul Kim, 2006
#ifndef ARBITRARYFUNC_H
#define ARBITRARYFUNC_H
#include "utils/ovector.hpp"
#include "utils/PairArray.hpp"
class Equation;
class Arbitrary_Func
{
 public:
   virtual ~Arbitrary_Func() {}
   virtual Scalar get_value(Scalar x) const = 0;
};

class Equation_Func: public Arbitrary_Func
{
  Equation *func;
public:
  Equation_Func(Equation *_func)
   {
     func = _func;
   }
  Scalar get_value(Scalar x) const
  {
     return func->get_value(x);
  }
};

class Internal_Func: public Arbitrary_Func
{
  Scalar (*g)(Scalar);
 public:
  Internal_Func(Scalar (*_g)(Scalar))
   {
     g = _g;
   }
  Scalar get_value(Scalar x) const
  {
    return (*g)(x);
  }
};

class Pairarray_Func: public Arbitrary_Func
{
  PairArray<> pairarray;
  bool linear_outside;
 public:
  Pairarray_Func(PairArray<> _pairarray, bool _linear_outside)
  {
    pairarray = _pairarray;
    linear_outside = _linear_outside;
  }
  Scalar get_value(Scalar x) const
  {
    return pairarray.get_second(x, linear_outside);
  }
};

#endif
