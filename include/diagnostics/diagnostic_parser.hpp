#ifndef DIAGNOSTICPARSER
#define DIAGNOSTICPARSER

#include <vector>
#include "main/parse.hpp"
#include "main/oopiclist.hpp"

// forward declarations
class DiagnosticPhase;
class TimeDiagnostic;

class DiagnosticParser : public Parse
{
private:
	ostring name;

	oopicList<DiagnosticParser> dps; //children parsers
	bool toplevel;

	DiagList *thediagnostics; // pointer to list of all diagnostics

	// variables to parse
	Equation function;
	bool initopen;  // whether the Diagnostic should be open initially
	bool display;
	bool bulkv;

	// variables for time diagnostics; naverage used for grid time averages, MAL 6/26/09
	int ncomb, histmax, naverage;

	// variables for sheath diagnostic
	Scalar Lparam, Rparam; // for left and right sheath diagnostics, MAL 6/25/09

	// variables for phase diagnostics
	int nsamppart;
	bool display_material_flag;

	// variables added for control of species gridded diagnostics, MAL 6/26/09
	bool flux1, flux2, flux3, J1dotE1, J2dotE2, J3dotE3, enden1, enden2, enden3;
	bool vel1, vel2, vel3, JdotE, enden, T1, T2, T3, Tperp, Temp, alloff, allon1D, allon; // add allon1D and allon, MAL 9/13/10
	int ngdu; // update grid diagnostics calculations every ngdu timesteps

	// variables for EEDF diagnostic
	bool eedf_diag, eedf_diags_enabled;
	int eedf_energybins, eedf_update_interval;
	int eedf_j0, eedf_j1;
	Scalar Emin, Emax;							// min/max energy to accumulate for EEDF
	oopicList<DiagnosticParser> eedf_dps; 		// EEDF children parsers
	ostring outfile;							// filename to write diagnostics into (it can have C format %d for time step)
	int write_start, write_step, write_end;		// start, step and end step for writing diagnostics into file
	int eedf_jbins;								// number of intervals in which define/compute EEDF - intervals are the same in size
	// support for list of coordinates and times in which to create diagnostics
//	ostring list_j0, list_j1, list_write_start;	// lists for start/end cells (list in space) and beginning of writes (list in time)
//	std::vector<int> vec_j0, vec_j1;			// vector of start/end positions for intervals for EEDFs
//	std::vector<double> vec_write_start;		// vector of start times for writing diagnostics
	bool reaction_diag;							// include reaction diagnostics (collision); JK, 2019-11-27
public:
	DiagnosticParser(bool tl=false);
	DiagnosticParser(FILE *fp, oopicList<Parameter<Scalar> > float_variables, oopicList<Parameter<int> > int_variables, bool tl);
	DiagnosticParser(FILE *fp, oopicList<Parameter<Scalar> > float_variables, oopicList<Parameter<int> > int_variables, DiagList *t,
						bool tl, ostring inClassName);
	~DiagnosticParser();

	// virtual functions from Parse
	void setdefaults();
	void setparamgroup();
	void check();
	void init();
	Parse * special(ostring s);

	// accessor functions
	inline ostring get_name() const { return name; }
	inline bool is_bulkvenabled() const { return bulkv; }
	// added MAL 6/26/09
	inline bool is_flux1enabled() const { return flux1; }
	inline bool is_flux2enabled() const { return flux2; }
	inline bool is_flux3enabled() const { return flux3; }
	inline bool is_J1dotE1enabled() const { return J1dotE1; }
	inline bool is_J2dotE2enabled() const { return J2dotE2; }
	inline bool is_J3dotE3enabled() const { return J3dotE3; }
	inline bool is_enden1enabled() const { return enden1; }
	inline bool is_enden2enabled() const { return enden2; }
	inline bool is_enden3enabled() const { return enden3; }
	inline bool is_vel1enabled() const { return vel1; }
	inline bool is_vel2enabled() const { return vel2; }
	inline bool is_vel3enabled() const { return vel3; }
	inline bool is_T1enabled() const { return T1; }
	inline bool is_T2enabled() const { return T2; }
	inline bool is_T3enabled() const { return T3; }
	inline bool is_Tperpenabled() const { return Tperp; }
	inline bool is_Tempenabled() const { return Temp; }
	inline bool is_JdotEenabled() const { return JdotE; }
	inline bool is_endenenabled() const { return enden; }
	inline bool is_alloffenabled() const { return alloff; }
	inline bool is_allon1Denabled() const { return allon1D; } // MAL 9/13/10
	inline bool is_allonenabled() const { return allon; } // MAL 9/13/10
	inline bool collect_reaction_diag() const { return reaction_diag; }		// JK 2019-11-27

	inline int get_ngdu() const { return ngdu; }
	inline int get_naverage() const { return naverage; } // added for grid time averages, MAL 6/7/09
	inline Scalar get_Lparam() const { return Lparam; } // for sheath diagnostic, MAL 6/25/09
	inline Scalar get_Rparam() const { return Rparam; } // for sheath diagnostic, MAL 6/25/09
	inline int get_eedf_energybins() const { return eedf_energybins; }				// JK, 2018-01-16
	inline int get_eedf_update_interval() const { return eedf_update_interval; }			// JK, 2017-11-01
	inline int get_eedf_j0(void) const { return eedf_j0; }						// JK, 2018-01-16
	inline int get_eedf_j1(void) const { return eedf_j1; }						// JK, 2018-01-16
	inline void set_eedf(bool _in) { eedf_diag = _in; classname = "DiagnosticEEDF";}
	inline bool is_eedf(void) const { return eedf_diag; }
	inline bool is_eedf_enabled(void) const { return eedf_diags_enabled; }				// JK, 2018-01-16
	inline oopicList<DiagnosticParser>* get_eedf_diag_parsers(void) { return &eedf_dps; }		// JK, 2018-01-16
	inline ostring get_file(void) const { return outfile; }						// JK, 2018-01-22
	inline int get_write_start(void) const { return write_start;}					// JK, 2018-01-22
	inline int get_write_step(void) const { return write_step;}					// JK, 2018-01-22
	inline int get_write_end(void) const { return write_end;}					// JK, 2018-01-22
	inline Scalar get_Emin(void) const { return Emin; }						// JK, 2018-01-25
	inline Scalar get_Emax(void) const { return Emax; }						// JK, 2018-01-25
	inline int get_eedf_jbins(void) { return eedf_jbins; }						// JK, 2018-01-27
//	inline int get_position_list_length(void) { return vec_j0.size(); }				// JK, 2018-01-27
//	inline int get_time_list_length(void) { return vec_write_start.size(); }			// JK, 2018-01-27

//
	inline void set_diagnostics(DiagList *td)
	{
		thediagnostics = td;
	}
	bool is_phasediagnostic_enabled(Species *thesp) const;

	// functions to initialize diagnostics
	int  init_Diagnostic(Diagnostic *thed);
	void init_TimeDiagnostic(TimeDiagnostic *thetd);
	void init_PhaseDiagnostic(DiagnosticPhase *thepd);

	void init_space_time_lists();					// JK, 2018-01-26
	vector<int> list_to_vector(ostring in_list) ;			// JK, 2018-01-26
	vector<double> list_to_vector_double(ostring in_list);		// JK, 2018-01-26
};

#endif
