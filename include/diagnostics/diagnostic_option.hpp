#ifndef DIAGNOSTIC_OPTION_H
#define DIAGNOSTIC_OPTION_H

#include "utils/ovector.hpp"

class XGoption
{
	// variables for XGrafix
	Vector3 scale;
	intVector3 rescale;
	Vector3 offset;
	Vector3 min, max;
public:
	XGoption()
	{
		scale=Vector3(1.0, 1.0, 1.0);
		offset=Vector3(0.0, 0.0, 0.0);
		rescale=intVector3(1, 1, 1);
		min=Vector3(0.0, 0.0, 0.0);
		max=Vector3(0.0, 0.0, 0.0);
	}
	void set_xrescale(int _xrescale)
	{
		rescale.set_e1(_xrescale);
	}
	void set_xscale(Scalar _xscale)
	{
		scale.set_e1(_xscale);
	}
	void set_yscale(Scalar _yscale)
	{
		scale.set_e2(_yscale);
	}
	void set_xmin(Scalar _xmin)
	{
		min.set_e1(_xmin);
	}
	void set_xmax(Scalar _xmax)
	{
		max.set_e1(_xmax);
	}
	void set_xoffset(Scalar _xoffset)
	{
		offset.set_e1(_xoffset);
	}
	Scalar xscale() const
	{
		return scale.e1();
	}
	Scalar yscale() const
	{
		return scale.e2();
	}
	Scalar zscale() const
	{
		return scale.e3();
	}
	Scalar xoffset() const
	{
		return offset.e1();
	}
	Scalar yoffset() const
	{
		return offset.e2();
	}
	Scalar zoffset() const
	{
		return offset.e3();
	}
	int xrescale() const
	{
		return rescale.e1();
	}
	int yrescale() const
	{
		return rescale.e2();
	}
	int zrescale() const
	{
		return rescale.e3();
	}
	Scalar xmin() const
	{
		return min.e1();
	}
	Scalar ymin() const
	{
		return min.e2();
	}
	Scalar zmin() const
	{
		return min.e3();
	}
	Scalar xmax() const
	{
		return max.e1();
	}
	Scalar ymax() const
	{
		return max.e2();
	}
	Scalar zmax() const
	{
		return max.e3();
	}
};
#endif
