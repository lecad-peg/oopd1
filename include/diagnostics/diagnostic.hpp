/* class Diagnostic -- generic diagnostic class */
#ifndef DIAGNOSTIC
#define DIAGNOSTIC

#include "xgrafix.h"
#include "utils/ostring.hpp"
#include "main/oopiclist.hpp"

// types of Diagnostics
#define DIAG_UNDEFINED -1
#define GRIDDIAGNOSTICTYPE  0
#define TIMEDIAGNOSTICTYPE  1				// 2D diagnostic with time as X-axis (constantly changing)
#define PHASEDIAGNOSTICTYPE 2				// 2D diagnostics with points for plotting data (species)
#define EEDFDIAGNOSTICTYPE	3				// 2D diagnostics with lines for computed values (curves)

class Diagnostic
{
protected:
	double *time;
	ostring name;
	ostring filename;
	ostring srname;  // SpatialRegion name for multiple SpatialRegions
	int type;
	int output_index, noutput, output_num;		// control of writing to file: current index, next output and current file written.
	int output_start, output_end;				// when to start/end writing to file

	// variables related directly to XGrafix
	oopicList<int> xgopenlist; // whether Diagnostic is open, according to XGrafix
	bool xginited;
	int ulx, uly;  // requested position of window frame's upper left corner

	bool openflag; // whether Diagnostic is INITIALLY open (setable)
	bool increment, binary;
	bool embedded; // whether Diagnostic is embedded in another
public:
	Diagnostic()
	{
		name = "";
		srname = "";
		filename = "";
		type = DIAG_UNDEFINED;
		output_index = 0;
		noutput = output_end = output_num = -1;
		output_start = 0;
		increment = binary = false;
		embedded = false;
		openflag = false;
		xginited = false;
		ulx = 100;
		uly = 400;
	}
	virtual ~Diagnostic()
	{
		xgopenlist.deleteAll();
	}

	// accessor functions
	inline  int get_type() const { return type; }
	inline void set_type(int i) { type = i; }
	inline void set_binary() { binary = true; }
	inline ostring get_name() const { return name; }
	inline bool get_openflag() const { return openflag; }
	inline void set_openflag(bool a) { openflag = a; }

	void write_data();
	void set_write_params(ostring f, int in_start, int in_step, int in_end) {
		set_file_name(f);
		output_start = in_start;
		noutput = in_step;
		output_end = 1 + (int) (in_end-in_start)/in_step;			// keep relative end step in terms of number of outputs we have to do
	}
	void set_file_name(ostring f) { filename = f; }
	void set_srname(ostring srn)  { srname = srn; }
	ostring get_file_name() const;
	ostring get_file_mode() const;

	bool is_xgopen();

	// virtual function for child classes
	virtual void update() { }
	virtual void initialize() { }
	virtual ostring get_full_name() const { return name; }
	virtual void output(FILE *fp) {}
	// added by JK, 2018-10-23
	void set_timepointer( double *tp ) { time = tp; };
};

#endif
