#ifndef TIMEDIAGNOSTIC_H
#define TIMEDIAGNOSTIC_H

#include "utils/ovector.hpp"
#include "diagnostics/diagnostic2d.hpp"

// type of the pointer to current time
#define TIMETYPE Scalar

// types for TimeDiagnostic:
#define TD_UNDEFINED -1
#define SPINTFUNC 0
#define SPSCAFUNC 1
#define SPVECFUNC 2
#define SCALARRAY 3

class TimeArray;
class Species;

class TimeDiagnostic : public Diagnostic2d
{
private:
  bool sum;          // whether to sum over multiple arrays
  int nnum;          // max number of data points in time arrays
  TIMETYPE *tpointer;  // pointer to the time value
  TimeArray *tarray; // time array -- really a "copy" of xarray
  Scalar scalefactor;

  int nnyyarr; // temporary variable to hold # of yarrays

  SpeciesList *splist; // specieslist
  int td_type;  // type of the TimeDiagnostic

  /////////////////////////////////////////////
  // data for different types of TimeDiagnostic
  /////////////////////////////////////////////

  // pointers to species functions used for updating
  int (Species::*spintfunc)();
  Scalar (Species::*spscfunc)();
  Vector3 (Species::*spvecfunc)();
  int nvcomp;

  // pointer to Scalar array for updating
  Scalar *udarray;

public:
  TimeDiagnostic();
  TimeDiagnostic(TIMETYPE *tp);
  TimeDiagnostic(TIMETYPE *tp, SpeciesList *spl,
		 int (Species::*spif)(), ostring nnn, bool tosum=true);
  TimeDiagnostic(TIMETYPE *tp, SpeciesList *spl,
		 Scalar (Species::*spsf)(), ostring nnn, bool tosum=true);
  TimeDiagnostic(TIMETYPE *tp, SpeciesList *spl,
		 Vector3 (Species::*spsf)(), ostring nnn);
  TimeDiagnostic(TIMETYPE *tp, SpeciesList *spl,
		 Vector3 (Species::*spsf)(), ostring nnn, XGoption xg_option);
  TimeDiagnostic(TIMETYPE *tp, Scalar *thearray, int nitms,
		 ostring nnn, bool tosum=false);
  TimeDiagnostic(TIMETYPE *tp, SpeciesList *spl, Scalar *thearray, int nitms,
		  ostring nnn, bool tosum=false);										// species list; JK 2019-01-19
  ~TimeDiagnostic();

  void init(TIMETYPE *tp, SpeciesList *spl,
	    Vector3 (Species::*spsf)(), ostring nnn);
  void setdefaults();

  void set_timepointer(TIMETYPE *tp) { tpointer = tp; }

  // inherited from Diagnostic2d (-> from Diagnostic)
  void update();
  void initialize();
  ostring get_full_name() const;

  void allocate_arrays(int number);
  void allocate_tarray();
  void allocate_yarrays(int number);

  void set_ntimesteps(int n);
  void set_ncomb(int n);

};

#endif
