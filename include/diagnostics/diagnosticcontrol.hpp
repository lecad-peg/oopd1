// DiagnosticControl -- class to manage Diagnostics

#ifndef DIAGNOSTICCONTROL_H
#define DIAGNOSTICCONTROL_H

#include "utils/ostring.hpp"
#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "diagnostics/diagnostic_time.hpp"

class TimeDiagnostic;
class DiagnosticPhase;
class DiagnosticParser;
class DiagnosticEEDF;
class DiagnosticReaction;
class Grid;
class DataArray;
class Fields;
class ReactionGroup;
class Species;

class DiagnosticControl
{
private:
	TIMETYPE *timepointer;
	SpeciesList *thesplist;
	Grid *thegrid;
	Fields *thefield;
	DataArray *gridarray;
	Scalar *sptr; // pointer to grid diagnostic arrays created in diagnosticcontrol.cpp

	static DiagList thediagnostics; // master list of diagnostics

	static oopicList<DataArray> grid_arrays;     // gridded scalar arrays
	static oopicList<VectorArray> grid_vectors;  // gridded vector arrays
	static oopicList<std::pair<ostring, Scalar * (Species::*)() const> > sp_grid_arrays;

	// lists of pointers to different types of diagnostics
	oopicList<Diagnostic2d> grid_diagnostics;
	static oopicList<TimeDiagnostic> time_diagnostics;
	oopicList<DiagnosticPhase> phase_diagnostics;

	// name to append to Diagnostics (SpatialRegion name)
	ostring srname;

	// master parser for this DiagnosticControl
	DiagnosticParser *theparser;

public:
	DiagnosticControl();
	~DiagnosticControl();

//	void  initialize(TIMETYPE *ttime, Grid *tg, Fields *tf,
//			SpeciesList *spl, BoundaryList *tbl);
	void  initialize(TIMETYPE *ttime, Grid *tg, Fields *tf,
			SpeciesList *spl, BoundaryList *tbl, oopicList<ReactionGroup> *rgl);			// added by JK, DQW 2019-11-25
	void  init_sp_grid_diag();
	void  init_sheathwidth_diag();
	void  update();
	void  parsediagnostics(FILE *fp);

	// accessor functions
	inline void set_diagnostic_parser(DiagnosticParser *tp) { theparser = tp; }
	inline void set_srname(ostring srn) { srname = srn; }
	inline DiagnosticParser* get_theparser() {return theparser;}

	// generic function to add a new Diagnostic
	void  add_diagnostic(Diagnostic *thenewdiag);
//#include "diagnostic_fe.hpp"
	void  add_phasediagnostic(Species *s, BoundaryList * theb);
	void  add_griddiagnostic(DataArray *a);
	void  add_griddiagnostic(VectorArray *a);
	void  add_timediagnostic(Scalar *val, ostring nnn);
	void  add_timediagnostic(int (Species::*spif)(), ostring nnn,
				 bool tosum=true);
	void  add_timediagnostic(Scalar (Species::*spif)(), ostring nnn,
				 bool tosum=true); // For integral of time-average J1dotE1 diagnostic, MAL 12/11/09
	void  add_timediagnostic(Vector3 (Species::*spvf)(), ostring nnn);
	void  add_timediagnostic(Vector3 (Species::*spvf)(), ostring nnn, XGoption xg_option);
	void  add_timediagnostic(Scalar *arr, int nitms, ostring nnn, bool tosum=false);
	void  add_timediagnostic(SpeciesList *spl, Scalar *arr, int nitms, ostring nnn, bool tosum);		// JK 2019-01-19

	void  add_gridarray(const char *name, Scalar *array, int n=0);
	void  add_gridvectors(const char *name, Vector3 *array, int vdim, int n=0);
	void  add_sp_grid_array(const char *name, Scalar * (Species::*spga)() const);
	void  add_eedfdiagnostic(Species *s, int, int, bool);			// added JK, 2017-11-01

	void  display_string(Diagnostic *td);
};

#endif
