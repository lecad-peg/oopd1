#ifndef DIAGNOSTIC2D_H
#define DIAGNOSTIC2D_H

#include "diagnostics/diagnostic_option.hpp"
#include "diagnostics/diagnostic.hpp"
#include "main/oopiclist.hpp"
#include "utils/DataArray.hpp"
class VectorArray;

// macros for types of plots
#define CURVE 0
#define SCATTER 1
#define VECCURVE 2

// macros for colors
#define PREVIOUSTYPE -2
#define NEXTTYPE -1

static const int BLUE    = 0;
static const int BEIGE   = 1;
static const int GREEN   = 2;
static const int YELLOW  = 3;
static const int ORANGE  = 4;
static const int PINK    = 5;
static const int GRAY    = 6;
static const int MAGENTA = 7;
static const int RED     = 8;




class Diagnostic2d : public Diagnostic
{ // 2d diagnostic
protected:
	oopicList<DataArray> yarray;
	oopicList<VectorArray> vyarray;
	oopicList<int> plottype;  // one of these for each yarray -- CURVE or SCATTER
	oopicList<int> vplottype;
	DataArray * xarray;
	char scaletype[7];
	XGoption option;

	bool v_sep_plots; // should VectorArrays have separate plots for each entry?
	ostring vecstring[3]; // strings for vectors

	ostring dimstr;
	bool vecdisplay[3];

	int col;

	void (*f)(); // the updating function


public:
	Diagnostic2d(DataArray *xrr, ostring nm);
	Diagnostic2d(DataArray *xrr, DataArray *yrr);
	Diagnostic2d(DataArray *xrr, VectorArray *yrr);
	Diagnostic2d();
	Diagnostic2d(void (*g)());
	~Diagnostic2d();

	void add_y_array(Scalar *yrr, int type = CURVE);
	void add_y_array(Scalar *yrr, ostring label, int type = CURVE);			// Y-values label, JK 2019-01-13

	void setdefaults();
	void set_func(void (*g)());

	// virtual functions from Diagnostic
	virtual void update();
	virtual void initialize();   // finishes initialization
	virtual ostring get_full_name() const;
	void output(FILE *fp);

	void set_vecstring(ostring a, ostring b, ostring c);

	int currentcolor();
	int nextcolor(bool increment = true);

	void diagnostic_error(const char *str="");

	void openWindow(const ostring & yname);
	virtual void initXGrafix();  // sets up the XGrafix windows
};

#endif
