#ifndef DIAGNOSTIC_EEDF_H
#define DIAGNOSTIC_EEDF_H

#include<vector>
#include "utils/ovector.hpp"
#include "diagnostics/diagnostic2d.hpp"
using namespace std;

#define SAMPLESIZE 10000
#define STRIDESIZE 1
#define FILLCOLOR PINK

// forward declaration
class DiagnosticParser;
class Grid;

class DiagnosticEEDF : public Diagnostic2d
{
private:
	int nsamppart;    // # of sample particles
	int stridesize;   // # of particles to sample per stride
	vector<vector<int> > indices;  // random numbers
	vector<Species *> speciesList;			// list of species to be considered and plotted
	bool v_sample[3];
	int maxdim;
	int linecolor, fillcolor;
	bool display_material;
	bool eepf;						// compute EEPF or EEDF
	Scalar energy_min, energy_max;	// xAxis data: min, max;
	int num_energybins;				// always have given number of intervals
	int j0, j1;						// min, max grid number between which we compute EEDF - given in input file
	int lim_j0, lim_j1;				// limits for x cell position between which we compute EEDF (could be different than j0 & j1)
	Scalar deV;						// grid size for energy
	int update_interval;			// update EEDF every "update_intervals" step (call) to update
	int update_skip_count;			// number of skipped updates
	Scalar *countArray;				// array used for counting particles
	DataArray * eV_sqrt;			// square roots of energies
	bool evaluatedOnce;
	bool autoEnergyLimits;			// auto defining energy limits for EEDF (true) or use defined (false)
	int pos_bins;					// number of intervals (position bins); JK 2018-01-27
	Scalar pos_xmin, pos_xmax;		// min/max positions for EEDF (for all position intervals)
	Scalar pos_dx;					// width of position interval
	vector<Scalar *> bins_countArray;	// vector for counting array for all bins
	vector<Scalar *> bins_data;			// vector of data for each interval

public:
	DiagnosticEEDF(int _n, Species * _thes);
	DiagnosticEEDF(Species * _thes);
	DiagnosticEEDF(int _eV, int _eV_update, bool _eedf);
	DiagnosticEEDF(DiagnosticParser *in_thep, Grid *thegrid, bool _eepf);
//	DiagnosticEEDF(DiagnosticParser *in_thep, Grid *thegrid, bool _eepf) {
//		DiagnosticEEDF(in_thep, thegrid, _eepf, -1);
//	}
//	DiagnosticEEDF(DiagnosticParser *in_thep, Grid *thegrid, bool _eepf, int list_pos);
	DiagnosticEEDF();
	~DiagnosticEEDF();

	void setdefaults();

	// virtual functions from Diagnostic2d -> Diagnostic
	void update();
	void initialize();   // finishes initialization
	void initXGrafix();
	ostring get_full_name() const;
	void output(FILE *fp);

	// getter/setter functions
	inline void set_nsamppart(int nnn) { nsamppart = nnn; }
	inline void set_display_material(bool flag) { display_material = flag; }
//	inline void set_species(Species *ts) { thespecies = ts; }
	void set_num_energybins(int _intervals);
	inline void set_update_interval(int _intervals) { update_interval = _intervals; }
	inline bool get_eepf(void) { return eepf; }
	inline void set_eepf(bool _eepf) {
		eepf = _eepf;

		if( eepf ) {
			name = ostring("EEPF");
		} else {
			name = ostring("EEDF");
		}
	}
	void set_x_limits(Grid *thegrid);
	inline bool is_inside_limits(Scalar x) { return (x >= lim_j0 && x < lim_j1); }

	void allocate_arrays();
	void deallocate_arrays();
	int get_yarr_dim(DataArray *tyarr);

	int compute_num_energy_bins(void);			// JK, 2017-10-31
	/* Add another species to current diagnostics; simulation could have multiple species representing electrons (initial, secondary, ...)
	 * and all of them have to be included in EEDF/EEPF. */
	inline void addSpecies(Species *ts) { speciesList.push_back(ts); }

};

#endif
