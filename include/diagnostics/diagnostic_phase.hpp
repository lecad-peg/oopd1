#ifndef PHASEDIAGNOSTIC_H
#define PHASEDIAGNOSTIC_H

#include<vector>
#include "utils/ovector.hpp"
#include "diagnostics/diagnostic2d.hpp"
using namespace std;

#define SAMPLESIZE 10000
#define STRIDESIZE 1
#define FILLCOLOR PINK

class Species;


class DiagnosticPhase : public Diagnostic2d
{
private:
  int nsamppart;    // # of sample particles
  int stridesize;   // # of particles to sample per stride
  vector<vector<int> > indices;  // random numbers
  Species *thespecies;  // species to be sampled
  BoundaryList *boundarylist;
  bool v_sample[3];
  int maxdim;
  int linecolor, fillcolor;
  bool display_material;

  // pointer to function for sampling
  void (DiagnosticPhase::*samplefunc)();

public:
  DiagnosticPhase();
  DiagnosticPhase(Species * _thes, BoundaryList * _theb);
  DiagnosticPhase(int _n, Species * _thes,  BoundaryList * _theb);
  ~DiagnosticPhase();

  void setdefaults();

  // virtual functions from Diagnostic2d -> Diagnostic
  void update();
  void initialize();   // finishes initialization
  void initXGrafix();
  ostring get_full_name() const;

  // accessor functions
  inline void set_nsamppart(int nnn) { nsamppart = nnn; }
  inline void set_display_material(bool flag) { display_material = flag; }
  inline void set_species(Species *ts) { thespecies = ts; }

  void set_array(int dim);
  void allocate_arrays();
  void setSampleSize();

  // master function for sampling phase space
  void SamplePhaseSpace();

  // child functions for sampling phase space using different methods
  void StrideSample();
  void FrontSample();

  void InitRandomNumbers();

  void sample_all_particles();

  int get_yarr_dim(DataArray *tyarr);
};

#endif
