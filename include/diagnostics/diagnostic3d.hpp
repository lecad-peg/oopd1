#ifndef DIAGNOSTIC3D_H
#define DIAGNOSTIC3D_H

#include "diagnostics/diagnostic_option.hpp"
#include "diagnostics/diagnostic.hpp"
#include "main/oopiclist.hpp"
#include "utils/DataArray.hpp"
#include "utils/Array2d.hpp"

// macros for colors 0-8 in diagnostic2d.hpp, MAL 12/26/09

class Diagnostic3d : public Diagnostic
{
private:
  DataArray *xarray;
  DataArray *yarray;
  Array2D  *zarray;
  Scalar theta, phi;

  XGoption option;

public:
  Diagnostic3d(DataArray *xa, DataArray *ya, Array2D *za);

  void setdefaults();

  ostring get_type(int i) const;

  // virtual functions from Diagnostic
  ostring get_full_name() const;
  void initialize();

  void openWindow();
  void initXGrafix();  // sets up the XGrafix windows

};
#endif
