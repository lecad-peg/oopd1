#ifndef DIAGNOSTIC_REACTION_H
#define DIAGNOSTIC_REACTION_H

#include<vector>
#include "utils/ovector.hpp"
#include "utils/ostring.hpp"
#include "diagnostics/diagnostic2d.hpp"
using namespace std;

#define SAMPLESIZE 10000
#define STRIDESIZE 1
#define FILLCOLOR PINK

// forward declaration
class DiagnosticParser;
class Grid;
class Reaction;
class ReactionGroup;
class DataArray;

class DiagnosticReaction : public Diagnostic2d
{
private:
	ostring reactionType;
	ostring chemical_equation;
	Scalar threshold_energy;			// reaction threshold energy - for adding to the diagnostics name; JK 2019-12-02
	int reacIndex;

public:
	DiagnosticReaction(DataArray *gridarray, ReactionGroup *group, int _reacIndex);
//	DiagnosticReaction(Species * _thes);
//	DiagnosticReaction(int _eV, int _eV_update, bool _eedf);
//	DiagnosticReaction(DiagnosticParser *in_thep, Grid *thegrid, bool _eepf);
//	DiagnosticReaction();
//	~DiagnosticReaction();

	void update();
	ostring get_full_name();
	// use Diagnostics2d methods here as they are defined as virtual ad we don't want to supply new ones
	void initialize() {	Diagnostic2d::initialize(); }
	void initXGrafix() { Diagnostic2d::initXGrafix(); }
	void output(FILE *fp) { Diagnostic2d::output(fp); }
};

#endif
