-- multipactor.inp
   : input file for testing oblique electric field addition in oopd1
   : Vacuum Multipactor Discharge
{

Variables
{
  melec = 9.1E-31
  echarge = -1.6E-19
  pi = 3.14159265358979323846
}

Species
{
  name = electrons
  q = echarge
  m = melec
  ForceNonRelativistic
  np2c = 5E9
  Force3V
}


SpatialRegion 
{
  dt = 3E-14
  Grid
  {
    x0 = 0.
    x1 = 0.0014
    ng = 8000
  }

  Variables
  {
    frequency = 2.85e9
  }
  PoissonSolver
  {
    Variables
    {
      //E_amp = 3e6
      E_ampx=0
      E_ampy=0
      E_ampz=3e6
      phi_x=0
      phi_y=0
      phi_z=0
      //frequency=2.85e9
      T=1e-9             //  T=1/frequency  //For Guassian form      
      tau_max=0.25e-9    //0.25*T
      tau_min=0.75e-9    //0.75*T
      DeltaTau=0.05e-9   //0.05*T
      Delta_sqr=2.5e-21  //DeltaTau*DeltaTau
      //beta=4.0*log(2)/2.5e-21   beta=4*log(2)/Delta_sqr
      beta=1.109035489e21
      theta=0.05		// pi is added in time function
    }

      efield_x(x,t) = sin(theta*pi)*E_ampz*sin(2.*pi*frequency*t+phi_z)
      efield_y(x,t) = E_ampy*sin(2.*pi*frequency*t)
      efield_z(x,t) = cos(theta*pi)*E_ampz*sin(2.*pi*frequency*t+phi_z)

  }
  Dielectric
  {
    j0 = 0
    j1 = 10
    epsilonr = 10
    Distribution
    {
      //decoupled 
      species = electrons
      Emin = 0.
      Emax = 2000.
      anglemax=1.5708
      anglebins=100
      energybins = 1000
    }
    Secondary
    {
      method = Vaughan
      energy_emit = 2
      secondary = 2.0
      impact_Species = electrons
      secSpecies = electrons
      roughness = 1 
      threshold = 0.
      energy_max = 400. 
      meth = 1
    }
    BeamEmitter
    {
      inject_to_right
      T = 2
      I(t) = -1.3e3*(t < 1/frequency)
      species = electrons
      methods = 1
    }
  }
  Dielectric
  {
    j0 = 7989
    j1 = 7999
    epsilonr = 1
  }
  Conductor
  {
    j0 = 7999
    Circuit
    {
      Drive
      {
	V
	DC=0
      }
    }
  }
} //ends SpatialRegion

DiagnosticControl
{
  allon
  Lsheath = 0.5 // n-(t) = Lsheath*n+(t)
  Rsheath = 0.5 // n-(t) = Rsheath*n+(t)
  particleflux1
  particleflux2
  particleflux3
  J1dotE1
  energydensity1
  energydensity2
  energydensity3
  averagevelocity1
  temperature1
  temperature2
  temperature3
  temperature
  J2dotE2
  J3dotE3
  JdotE
//  alloff
//  ngriddiagupdate = 10000// update diagnostics every 10 timesteps
    ngriddiagupdate = 10 // update diagnostics every 10 timestep
//  nsamppart = 200000 // sample 200000 particles from each species
    naverage=100 // number of timesteps for grid time averages
//	naverage=1 //don't calculate time-average grid diagnostics
    ncomb=4 // what is this for
    histmax=200000
  //*******************************
  bulkvelocity
  //histmax = 100  // global histmax for all diagnostics
  nsamppart = 1000000 // sample a million particles from each species
  Diagnostic
  {
    name = number(t)
    open // open number(t) initially
  }
 Diagnostic
  {
    name = phi(x)
    open
  }
	Diagnostic
	{
		name = Ex(x)
		open
	}
	Diagnostic
	{
		name = Ey(x)
		open
	}
	Diagnostic
	{
		name = Ez(x)
		open
	}
}

}
