#!/bin/sh
#PBS -N IED_Cl
#PBS -l nodes=1:ppn=1
#PBS -l walltime=250:00:00
#PBS -M tumi@hi.is
#PBS -m abe
cd ptsg/28JUL2016/
export LD_LIBRARY_PATH=/users/home/tumi/instdir/lib
./pd1 -i inp/chlorine_10mTorr_DF_phase45.inp -nox -dp 550000 -s 5500000 > Commands/Clph45.txt
./pd1 -i inp/chlorine_10mTorr_DF_phase60.inp -nox -dp 550000 -s 5500000 > Commands/Clph60.txt
./pd1 -i inp/chlorine_10mTorr_DF_phase75.inp -nox -dp 550000 -s 5500000 > Commands/Clph75.txt