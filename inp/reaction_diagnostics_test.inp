-- reaction_diagnostic_test.inp
   : Input file for displaying reaction diagnostics; 
   : Based on HPM (high power microwave) breakdown discharge in Argon; case from CPC 2009 paper; Nam+Verboncoeur
{

Variables
{
    melec = 9.1E-31
    mproton = 1.68E-27
    pcharge = 1.6E-19
}

Species
{
    name = Argon
    np2c = 1e09
    q=0.
    m = 40*mproton
    reaction_type = Ar

    DiffusionFluid 
    {
        n = 3.1759E+23       // p = 10 Torr
        T = 0.026
    }
//    useSecDefaultWeight
//    energyThrToAddAsPIC = 300.0
}

Species
{
    name = electrons
    reaction_type = E-
    q = -pcharge
    m = melec
    ForceNonRelativistic
    np2c = 1E09
}

Species // the ion species
{
    name = Ar_ions
    reaction_type = Ar+
    q = pcharge
    m = 40*mproton
    ForceNonRelativistic
    np2c = 1E09
}

SpatialRegion 
{
    dt = 1e-13 // 1E-14
    Grid
    {
        x0 = 0.
        x1 = 0.14
        ng = 8000
    }

    Variables
    {
        frequency = 2.85e9      // E-field driving frequency
    }

    PoissonSolver
    {
        Variables
        {
            E_amp = 2.82e6             // E-field amplitude
        }
        efield_y(x,t) = E_amp*cos(2.*3.141592653*frequency*t)
    }

    Dielectric
    {
        j0 = 0
        j1 = 10
    }

    Dielectric
    {
        j0 = 7989
        j1 = 7999
    }

    Conductor
    {
        j0 = 7999
        Circuit
        {
            Drive
            {
                V
                DC=0
            }
        }
    } 

    Reactions
    {
        gas = Argon
        Reaction
        {
            debug
            type = e_ionization
            threshold = 15.76
//            x_type1_e1 = 30
//            x_type1_e2 = 100
//            x_type1_sigmamax = 3e-20
        //   x_function(e) = 1e-19/e
            reactant1 = electron
            reactant2 = Argon
        }

        Reaction
        {
            debug
            type = ch_exchange
//            x_type2_e1 = 2.e-19
//            x_type2_e2 = 5.5e-19
//            reactant1 = Ar_ions
//            reactant2 = Argon
        }

        Reaction
        {
            debug
            type = e_excitation
            reactant1 = electron
            reactant2 = Argon
        }
    }

    Load
    {
        species = electrons
        x0 = 0
        x1 = 0.10
        n = 1e13
        densityfunction = 1
        T_load = 2.0

    }

    Load
    {
        species = Ar_ions
        x0 = 0
        x1 = 0.10
        n = 1e13
        densityfunction = 1
        T_load = 0.026
    }

} //ends SpatialRegion

DiagnosticControl
{
//    allon
//  alloff
//  ngriddiagupdate = 10000// update diagnostics every 10 timesteps
    ngriddiagupdate = 100 // update diagnostics every 10 timestep
    naverage=100 // number of timesteps for grid time averages
//	naverage=1 //don't calculate time-average grid diagnostics
    ncomb=4 // what is this for
    histmax=200000
    //*******************************
    bulkvelocity
    //histmax = 100  // global histmax for all diagnostics
    nsamppart = 10000000 // sample a million particles from each species

    // enable/disable re3action diasgnostics
    reaction_diagnostics
}

}
