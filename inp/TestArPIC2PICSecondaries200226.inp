
{  // you can have comments after braces

  	Variables
    	{
      	// physical data
	melec = 9.11E-31
	mAr = 6.64E-26   	
        e=1.6E-19 		//positive charge
    	} 

  	Species 
    	{
        name = E-
	q = -e
	m = melec
	reaction_type = E-
        np2c = 1E7 
	Force3V  		// For angular distributions
	useSecDefaultWeight
	maxEnergyForSigmaV = 100.
    	}

  	Species
    	{
      	name = Ar_ions
	q = e
	m = mAr
	reaction_type = Ar_ion 
	np2c = 1E7
	Force3V
	maxEnergyForSigmaV = 500.
    	}


  	Species
    	{
      	name = Argon
	q = 0
	m = mAr
	reaction_type = Ar
	DiffusionFluid
	{
	fluidname = feed
	n = 3.25E20 // 10 mTorr
	T = 0.026
	diff_vol = 16.1
	}
	np2c = 1E7
	Force3V
	useSecDefaultWeight
 	energyThrToAddAsPIC = 0 // add as PIC species above 10000 eV
     	}

	Species
    	{
       	name = Arm
	q = 0.0
	m = mAr
	reaction_type = Arm
	DiffusionFluid
	 {
	   fluidname = diff
	   evolve_fluid_in_time
	   // n = 2E15 // initialize same as loaded PIC Arm's
	   n = 0 // initialize
	   T=26 // with this T, we get nwall = 0.5 ncenter
	   gammaL = 1
	   gammaR = 1
// Arm diffusion volume below gives Smirnov Arm/Ar diffusion cofficient at STP of D=7.1E-6 m^2/s
	   diff_vol = 183.2 // Fuller (1966) diffusion volume [cm^3/mole]
	 }
	np2c = 1E7
	Force3V
   	energyThrToAddAsPIC = 0.0
	}

	Species 
	{
	name = Arr
   	q = 0.0 
   	m = mAr
   	reaction_type = Arr
	DiffusionFluid
	 {
	   fluidname = diff
	   evolve_fluid_in_time
	   n = 0 // initialize
	   T=0.026 // with this T, we get nwall = 0.5 ncenter
	   gammaL = 1
	   gammaR = 1
	   diff_vol = 183.2 // use Fuller (1966) diffusion volume [cm^3/mole] for Arm
	 }
   	np2c = 1.0E7
   	Force3V
   	energyThrToAddAsPIC = 0.0
   	// energyThrToAddAsPIC = 10000
	}


	Species 
	{
  	 name = Ar4p
   	q = 0.0 
   	m = mAr 
   	reaction_type = Ar4p
	DiffusionFluid
	 {
	   fluidname = diff
	   evolve_fluid_in_time
	   n = 0 // initialize
	   T=0.026 // with this T, we get nwall = 0.5 ncenter
	   gammaL = 1
	   gammaR = 1
	   diff_vol = 183.2 // use Fuller (1966) diffusion volume [cm^3/mole] for Arm
	 }
   	np2c = 1.0E7
   	Force3V
   	energyThrToAddAsPIC = 0.0
   	// energyThrToAddAsPIC = 10000
}


	SpatialRegion 
	{
  	dt = 3.687315E-11; 	// 2000 timesteps/period at 13.56 MHz 

        // spatiotemporal

  	Grid
  	{
	x0 = -0.0127 		// gap length = 0.0254 m
	x1 = 0.0127
	area= 0.00817 		// diameter = 0.102 m 
	ng = 1001
  	}

  	PoissonSolver
  	{	
	rhoback=0.0; 	
  	}
  

  	Conductor
  	{
   	j0 = 0

   	Circuit
    	{
	//C=1
	Drive
	{
	I
	AC = 40*0.00817
        frequency = 27.12e6 	// 13.56e6
        phase = 0.0
	}
    	}

  Secondary 		// Charged particle/neutral emission, Ar_ion hitting wall generates Ar4p
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Ar_ions
	secSpecies = Ar4p
	threshold = 0.0
        np2c0 = 1E7
	}

  Secondary 		// Neutral electron emission, Arm hitting wall generates electron
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Arm
	secSpecies = E-
	threshold = 0.0
        np2c0 = 1E7
	}

 Secondary 		// Neutral/neutral emission, Arm hitting wall generates Arr
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Arm
	secSpecies = Arr
	threshold = 0.0
        np2c0 = 1E7
	}
  	}

	Boundary
	{
    	j0 = 500 		// for midpotential
    	j1 = 500
	}

	Conductor
  	{
   	j0 = 1000

  Secondary 		// Charged particle/neutral emission, Ar_ion hitting wall generates Ar4p
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Ar_ions
	secSpecies = Ar4p
	threshold = 0.0
        np2c0 = 1E7
	}

  Secondary 		// Neutral electron emission, Arm hitting wall generates electron
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Arm
	secSpecies = E-
	threshold = 0.0
        np2c0 = 1E7
	}

 Secondary 		// Neutral/neutral emission, Arm hitting wall generates Arr
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Arm
	secSpecies = Arr
	threshold = 0.0
        np2c0 = 1E7
	}
	}
  
  	Load
  	{
	species = E-
        T_load = 3
	n = 2E15 					//5E14 is peak density
  	}

  	Load
  	{
	species = Ar_ions
        T_load = 0.026
	n = 2E15
  	}

	Load
	{
	species = Arm
	n = 2E15
	T_load = 26 // note high temp
	}

  	Load
  	{
	species = Argon
  	}


  	Reactions
  	{
    	gas = Argon
  	}

} 		//ends SpatialRegion


DiagnosticControl
{
	Lsheath = 0.6 		// n-(t) = Lsheath*n+(t)
	Rsheath = 0.6 		// n-(t) = Rsheath*n+(t)
	naverage= 271200	// 200000
	ngriddiagupdate = 1 	// update diagnostics every timestep
	allon
	nsamppart = 1000000 	// sample a million particles from each species
	histmax=100000
   }

}
