
{  // you can have comments after braces

  	Variables
    	{
      	// physical data
	melec = 9.11E-31
	mCl = 5.885E-26   	//35.45*1.66E-27
        mCl2= 1.177E-25  	//2*35.45*1.66E-27
        e=1.6E-19 		//positive charge
    	} 

  	Species 
    	{
        name = E-
	q = -e
	m = melec
	reaction_type = E-
        np2c = 2E7 
	Force3V  		// For angular distributions
	useSecDefaultWeight
	maxEnergyForSigmaV = 100.
    	}

  	Species
    	{
      	name = Cl2+
	q = e
	m = mCl2
	reaction_type = Cl2_ion
	np2c = 5E7
	Force3V
	useSecDefaultWeight
	maxEnergyForSigmaV = 500.
	subcycle=16
    	}

	Species
	{
      	name = Cl+
	q = e
	m = mCl
	reaction_type = Cl_ion
	np2c = 1E6  		// np2c for Cl+. The np2c0 in the SEE should also be changed. (4 np2c0 in total)
	Force3V
	useSecDefaultWeight
	subcycle=16
    	}

  	Species
    	{
      	name = Cl2
	q = 0
	m = mCl2
	reaction_type = Cl2
	DiffusionFluid
	 	{
	   	n = 0.819*3.2E20 	//
	  	// n = 3.2E21 	// 100 mTorr
	   	T=0.026
	 	}
	np2c = 5E7
	Force3V
	useSecDefaultWeight
	subcycle=16
 	energyThrToAddAsPIC = 1 // add as PIC species above 10000 eV
     	}

	Species
    	{
      	name = Cl
	q = 0
	m = mCl
	reaction_type = Cl
	DiffusionFluid
	 	{
	   	n = 0.181*3.2E20  		
	  	// n = 3.2E21 	// 100 mTorr
	  	T=0.026
	 	}
	np2c = 5E7
	Force3V
	useSecDefaultWeight
	subcycle=16
 	energyThrToAddAsPIC = 1 // add as PIC species above 10000 eV
     	}

   	Species
    	{
       	name = Cl-
	q = -e
	m = mCl
	reaction_type = Cl_neg_ion
	np2c = 5E7
	Force3V
	useSecDefaultWeight
	maxEnergyForSigmaV = 10.
	subcycle=16
	}

	SpatialRegion 
	{
  	dt = 3.687315E-11/2; 	// 2000 timesteps/period at 13.56 MHz 

  	Grid
  	{
	x0 = -0.0127 		// gap length = 0.0254 m
	x1 = 0.0127
	area= 0.00817 		// diameter = 0.102 m 
	ng = 1001
  	}

  	PoissonSolver
  	{	
	rhoback=0.0; 	
  	}
  

  	Conductor
  	{
   	j0 = 0

   	Circuit
    	{
	//C=1
	Drive
	{
	I
	AC = 60*0.00817
        frequency = 27.12e6 	// 13.56e6
        phase = 0.0
	}

	Drive
	{
	I
	AC = 0*0.008107
        frequency = 2e6 	// low frequency
        phase = 0.0
	}
    	}

        Secondary 		// Recombination, PIC Cl hitting wall generates thermal PIC Cl2
	{
	method = Basic
	energy_emit = 0.026
	secondary =  0.25
	impact_Species = Cl
	secSpecies = Cl2
	threshold = 0.0
        np2c0 = 5E7
	}
       
        Secondary 		// Recombination, PIC Cl hitting wall generates thermal PIC Cl
	{
	method = Basic
	energy_emit = 0.026
	secondary =  0.5
	impact_Species = Cl
	secSpecies = Cl
	threshold = 0.0
        np2c0 = 5E7
	}

        Secondary 		// Reflection, PIC Cl2 hitting wall generates thermal PIC Cl2
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Cl2
	secSpecies = Cl2
	threshold = 0.0
        np2c0 = 5E7
	}

        Secondary 		// 
	{
	method = Basic
	energy_emit = 0.026  
	secondary = 0.0 	// sec_e coefficient
	impact_Species = Cl2+
	secSpecies = E-
	threshold = 0.0
        np2c0 = 5E7
	}

        Secondary 		// 
	{
	method = Basic
	energy_emit = 0.026
	secondary = 0.0 	// sec_e coefficient
	impact_Species = Cl+
	secSpecies = E-
	threshold = 0.0
        np2c0 = 1E6		// np2c0 for Cl+
	}
 
    	Distribution
    	{
      	decoupled 
      	species = Cl2+
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 

    	Distribution
    	{
      	decoupled 
      	species = Cl+
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 

    	Distribution
    	{
      	decoupled 
      	species = Cl
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 

    	Distribution
    	{
      	decoupled 
      	species = Cl2
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 

    	Distribution
    	{
      	decoupled 
      	species = Cl-
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 

  	}

	Boundary
	{
    	j0 = 500 		// for midpotential
    	j1 = 500

    	Distribution
    	{
      	decoupled 
      	species = E-
      	Emin = 0.0
      	Emax = 200.0
      	energybins = 800
    	}

    	Distribution
    	{
      	decoupled 
      	species = Cl-
      	Emin = 0.0
      	Emax = 200.0
      	energybins = 800
    	}

	}

	Conductor
  	{
   	j0 = 1000

        Secondary 		// Recombination, PIC Cl hitting wall generates thermal PIC Cl2
	{
	method = Basic
	energy_emit = 0.026
	secondary =  0.25
	impact_Species = Cl
	secSpecies = Cl2
	threshold = 0.0
        np2c0 = 5E7
	}
       
        Secondary 		// Recombination, PIC Cl hitting wall generates thermal PIC Cl
	{
	method = Basic
	energy_emit = 0.026
	secondary =  0.5
	impact_Species = Cl
	secSpecies = Cl
	threshold = 0.0
        np2c0 = 5E7
	}

        Secondary 		// Reflection, PIC Cl2 hitting wall generates thermal PIC Cl2
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Cl2
	secSpecies = Cl2
	threshold = 0.0
        np2c0 = 5E7
	}

        Secondary 		// 
	{
	method = Basic
	energy_emit = 0.026  
	secondary = 0.0 	// sec_e coefficient
	impact_Species = Cl2+
	secSpecies = E-
	threshold = 0.0
        np2c0 = 5E7
	}

        Secondary 		// 
	{
	method = Basic
	energy_emit = 0.026
	secondary = 0.0 	// sec_e coefficient
	impact_Species = Cl+
	secSpecies = E-
	threshold = 0.0
        np2c0 = 1E6		// np2c0 for Cl+
	}
 
    	Distribution
    	{
      	decoupled 
      	species = Cl2+
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 

    	Distribution
    	{
      	decoupled 
      	species = Cl+
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 

    	Distribution
    	{
      	decoupled 
      	species = Cl
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 

    	Distribution
    	{
      	decoupled 
      	species = Cl2
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 

    	Distribution
    	{
      	decoupled 
      	species = Cl-
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 


  	}

  	Load
  	{
	species = E-
        T_load = 3
	n = 0.2E15 					//5E14 is peak density
	densityfunction(x)=max(1-x^2/1E-4,0) 	//average is 2/3
  	}

  	Load
  	{
	species = Cl2+
        T_load = 0.026
	n = 20E15
	densityfunction(x)=max(1-x^2/1E-4,0)       
  	}

  	Load
  	{
	species = Cl+
        // T_load = 0.026
  	}

  	Load
  	{
	species = Cl2
        // T_load = 0.026
  	}

  	Load
  	{
	species = Cl
        // T_load = 0.026
  	}

  	Load
  	{
	species = Cl-
        T_load = 0.026
	n = 19.8E15
	densityfunction(x)=max(1-x^2/1E-4,0)
  	}

  	Reactions
  	{
    	gas = Chlorine
  	}

} 		//ends SpatialRegion


DiagnosticControl
{
	Lsheath = 0.6 		// n-(t) = Lsheath*n+(t)
	Rsheath = 0.6 		// n-(t) = Rsheath*n+(t)
	naverage= 271200	// 200000
	ngriddiagupdate = 1 	// update diagnostics every timestep
	allon
	nsamppart = 1000000 	// sample a million particles from each species
	histmax=100000
   }

}
