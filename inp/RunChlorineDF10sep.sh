#!/bin/sh
#PBS -N IED_Cl
#PBS -l nodes=1:ppn=1
#PBS -l walltime=250:00:00
#PBS -M tumi@hi.is
#PBS -m abe
cd ptsg/28JUL2016/
export LD_LIBRARY_PATH=/users/home/tumi/instdir/lib
./pd1 -i inp/chlorine_10mTorr_DF_phase0.inp -nox -dp 550000 -s 5500000 > Commands/Clph0.txt
./pd1 -i inp/chlorine_10mTorr_DF_phase15.inp -nox -dp 550000 -s 5500000 > Commands/Clph15.txt
./pd1 -i inp/chlorine_10mTorr_DF_phase30.inp -nox -dp 550000 -s 5500000 > Commands/Clph30.txt
./pd1 -i inp/chlorine_10mTorr_DF_phase90.inp -nox -dp 550000 -s 5500000 > Commands/Clph90.txt