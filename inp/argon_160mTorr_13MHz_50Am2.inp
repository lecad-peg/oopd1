
{  // Argon at intermediate pressure, Kawamura et al JVSTA submitted 2019
   // 160 mTorr, 2.5 cm gap, 50 A/m2

  	Variables
    	{
      	// physical data
	melec = 9.11E-31
	mAr = 6.64E-26   	
        e=1.6E-19 		//positive charge
    	} 

  	Species 
    	{
        name = E-
	q = -e
	m = melec
	reaction_type = E-
        np2c = 1E7 
	Force3V  		// For angular distributions
	useSecDefaultWeight
	maxEnergyForSigmaV = 100.
    	}

  	Species
    	{
      	name = Ar_ions
	q = e
	m = mAr
	reaction_type = Ar+
	np2c = 1E7
	Force3V
    	}


  	Species
    	{
      	name = Argon
	q = 0
	m = mAr
	reaction_type = Ar
	DiffusionFluid
	 	{
	   	n = 16.0*3.2E20 	//
	  	// n = 3.2E21 	// 100 mTorr
	   	T=0.026
	 	}
	np2c = 5E7
	Force3V
	useSecDefaultWeight
	subcycle=16
 	energyThrToAddAsPIC = 1 // add as PIC species above 10000 eV
     	}

Species 
{
   name = Unit
   q = 0.0 
   m = mAr 
   reaction_type = Unit
   DiffusionFluid
  {
   n = 1 // unit density fluid
   T = 1E-6 // very low temperature
  }
   np2c = 1.0E9
   Force3V
   energyThrToAddAsPIC = 0.0
}
Species 
{
   name = Arm
   q = 0.0 
   m = mAr 
   reaction_type = Arm
   np2c = 1.0E9
   Force3V
   energyThrToAddAsPIC = 0.0
}
Species 
{
   name = Arr
   q = 0.0 
   m = mAr 
   reaction_type = Arr
   np2c = 1.0E9
   Force3V
   energyThrToAddAsPIC = 0.0
}
Species 
{
   name = Ar4p
   q = 0.0 
   m = mAr 
   reaction_type = Ar4p
   np2c = 1.0E6
   Force3V
   energyThrToAddAsPIC = 0.0
}



	SpatialRegion 
	{
  	dt = 3.687315E-12*2; 	// 

        spatiotemporal

  	Grid
  	{
	x0 = -0.0125 		// gap length = 0.025 m
	x1 = 0.0125
	area= 0.00817 		// diameter = 0.102 m 
	ng = 1001
  	}

  	PoissonSolver
  	{	
	rhoback=0.0; 	
  	}
  

  	Conductor
  	{
   	j0 = 0

   	Circuit
    	{
	C=1
	Drive
	{
	I
	AC = 50 * 0.00817
        frequency = 13.56e6 	// 
        phase = 0.0
	}
    	}


  Secondary 		// Reflection, PIC Ar hitting wall generates thermal PIC Ar
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Argon
	secSpecies = Argon
	threshold = 0.0
        np2c0 = 5E7
	}

   Secondary 		// Reflection, PIC Arm hitting wall generates thermal PIC Arm
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Arm
	secSpecies = Arm
	threshold = 0.0
        np2c0 = 5E7
	}

Secondary 		// Reflection, PIC Arr hitting wall generates thermal PIC Arr
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Arr
	secSpecies = Arr
	threshold = 0.0
        np2c0 = 5E7
	}
Secondary 		// Reflection, PIC Ar4p hitting wall generates thermal PIC Ar4p
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Ar4p
	secSpecies = Ar4p
	threshold = 0.0
        np2c0 = 5E7
	}


    Secondary               // 
        {
        method = Basic
        energy_emit = 0.026  
        secondary = 0.1         // sec_e coefficient
        impact_Species = Ar_ions
        secSpecies = E-
        threshold = 0.0
        np2c0 = 5E7
        }

 
    	Distribution
    	{
      	decoupled 
      	species = Ar_ions
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 

    

    	Distribution
    	{
      	decoupled 
      	species = Argon
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 
      	}


	Boundary
	{
    	j0 = 500 		// for center EEDF
    	j1 = 500

    	Distribution
    	{
      	decoupled 
      	species = E-
      	Emin = 0.0
      	Emax = 200.0
      	energybins = 800
    	}
	}
	
	Boundary
	{
    	j0 = 880 		// for sheath EEDF
    	j1 = 880

    	Distribution
    	{
      	decoupled 
      	species = E-
      	Emin = 0.0
      	Emax = 200.0
      	energybins = 800
    	}

	}


	Conductor
  	{
   	j0 = 1000


 Secondary 		// Reflection, PIC Ar hitting wall generates thermal PIC Ar
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Argon
	secSpecies = Argon
	threshold = 0.0
        np2c0 = 5E7
	}

Secondary 		// Reflection, PIC Arm hitting wall generates thermal PIC Arm
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Arm
	secSpecies = Arm
	threshold = 0.0
        np2c0 = 5E7
	}

Secondary 		// Reflection, PIC Arr hitting wall generates thermal PIC Arr
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Arr
	secSpecies = Arr
	threshold = 0.0
        np2c0 = 5E7
	}

Secondary 		// Reflection, PIC Ar4p hitting wall generates thermal PIC Ar4p
	{
	method = Basic
	energy_emit = 0.026
	secondary = 1.0
	impact_Species = Ar4p
	secSpecies = Ar4p
	threshold = 0.0
        np2c0 = 5E7
	}

   Secondary               // 
        {
        method = Basic
        energy_emit = 0.026  
        secondary = 0.1         // sec_e coefficient
        impact_Species = Ar_ions
        secSpecies = E-
        threshold = 0.0
        np2c0 = 5E7
        }


    	Distribution
    	{
      	decoupled 
      	species = Ar_ions
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 

    
    	Distribution
    	{
      	decoupled 
      	species = Argon
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 
   }

  
  	Load
  	{
	species = E-
        T_load = 3
	n = 2E15 					//5E14 is peak density
	densityfunction(x)=max(1-x^2/1E-4,0) 	//average is 2/3
  	}

  	Load
  	{
	species = Ar_ions
        T_load = 0.026
	n = 2E15
	densityfunction(x)=max(1-x^2/1E-4,0)       
  	}


  	Load
  	{
	species = Argon
        // T_load = 0.026
  	}


  	Reactions
  	{
    	gas = Argon
  	}

} 		//ends SpatialRegion


DiagnosticControl
{
	Lsheath = 0.6 		// n-(t) = Lsheath*n+(t)
	Rsheath = 0.6 		// n-(t) = Rsheath*n+(t)
	naverage= 27120	// 20000
	ngriddiagupdate = 1 	// update diagnostics every timestep
	allon
	nsamppart = 1000000 	// sample a million particles from each species
	histmax=100000
   }

}
