Test full argon set with Arm, Arr, and Ar4p modelled as fluids
Includes fluid_fluid collisions and diffusion of fluids to the walls
MAL200220
{  // you can have comments after braces
   //constant secondary coefficient 

Variables
{
	melec = 9.11E-31
	mion = 6.64E-26
}
Species 
{
   name = e-
   q = -1.6E-19 
   m = melec 
   reaction_type = E-
   Force3V
   ForceNonRelativistic
   np2c = 1.0E9
   useSecDefaultWeight
   maxEnergyForSigmaV = 100.

}
Species 
{
   name = Ar
   q = 0.0 
   m = mion 
   reaction_type = Ar
   DiffusionFluid
  {
   fluidname = feed
   n = 3.25E20 // 10 mTorr
   T = 0.026
	diff_vol = 16.1
  }
   np2c = 1.0E9
   Force3V
   useSecDefaultWeight
   energyThrToAddAsPIC = 10000
}
Species 
{
   name = U
   q = 0.0 
   m = mion 
   reaction_type = Unit
   DiffusionFluid
  {
   n = 1 // unit density fluid
   T = 1E-6 // very low temperature
  }
   np2c = 1.0E9
   Force3V
   useSecDefaultWeight
   energyThrToAddAsPIC = 10000
}
Species 
{
   name = Arm
   q = 0.0 
   m = mion 
   reaction_type = Arm
	DiffusionFluid
	 {
	   fluidname = diff
	   evolve_fluid_in_time
//	   subcycle = 100 // Note the default value of subcycle for fluids is 100
	   // n = 1.5E18 // initialize
	   n = 0 // initialize
	   T=0.026
	  // T=26 // with this T, we get nwall = 0.5 ncenter
//	   Dcoef(x)=100-100*x*x/4E-3 // the spatially-varying case
	   // Dcoef(x)=1-1*x*x/4E-3 // the spatially-varying case
//	   Dcoef(x)=300-300*x*x/4E-3 // for substepping due to von Neumann stability condition
//	   Dcoef = 100
	   gammaL = 1
	   gammaR = 1
//	   outflux // diffusing species
// Arm diffusion volume below gives Smirnov Arm/Ar diffusion cofficient at STP of D=7.1E-6 m^2/s
	   diff_vol = 183.2 // Fuller (1966) diffusion volume [cm^3/mole]
	 }
   np2c = 1.0E9
   Force3V
   useSecDefaultWeight
   energyThrToAddAsPIC = 10000
}
   Species 
   {
      name = Arr
      q = 0.0 
      m = mion 
      reaction_type = Arr
	DiffusionFluid
	 {
	   fluidname = diff
	   evolve_fluid_in_time
//	   subcycle = 100 // Note the default value of subcycle for fluids is 100
	   n = 0
	   T=0.026 // with this T, we get nwall = 0.5 ncenter
//	   Dcoef(x)=100-100*x*x/4E-3 // the spatially-varying case
	   gammaL = 1
	   gammaR = 1
	   diff_vol = 183.2 // Use Fuller (1966) diffusion volume [cm^3/mole] for Arm
	 }
      np2c = 1.0E9
      Force3V
      useSecDefaultWeight
      energyThrToAddAsPIC = 10000
   }
  Species 
  {
     name = Ar4p
     q = 0.0 
     m = mion 
     reaction_type = Ar4p
	DiffusionFluid
	 {
	   fluidname = diff
	   evolve_fluid_in_time
	   subcycle = 20 // here we use (test) a smaller subcycle for Ar4p
	   n = 0
	   T=0.026 // with this T, we get nwall = 0.5 ncenter
	  // Dcoef(x)=100-100*x*x/4E-3 // the spatially-varying case
	   gammaL = 1
	   gammaR = 1
	   diff_vol = 183.2 // Use Fuller (1966) diffusion volume [cm^3/mole] for Arm
	 }
     np2c = 1.0E9
     Force3V
     useSecDefaultWeight
     energyThrToAddAsPIC = 10000
  }
Species 
{
   name = Ar+
   q = 1.6E-19 
   m = mion 
   reaction_type = Ar+
   np2c = 1.0E9
   Force3V
   useSecDefaultWeight
   maxEnergyForSigmaV = 500.
}
SpatialRegion 
{
  dt = 3.687315E-11; // 2000 timesteps/period at 13.56 MHz 
  Grid
  {
	x0 = -0.05
	x1 = 0.05
	ng = 501
	area=0.1 //area in sq meters
  }
  PoissonSolver
  {	
	rhoback=0.0; 	
  }
  Conductor
  {
   j0 = 0
   Circuit
    {
	Drive
	{
	V
	AC = 500
//	I
//	AC = 2.5
        frequency = 13.56e6
	  phase = 180.
	}
    }
  }
Boundary
{
 j0 = 250 //for midpotential
 j1 = 250
}
Conductor
  {
   j0 = 500
  }
   Load
   {
 	species = e-
         T_load = 3.3
 	// n = 3.0E15
 	n = 3.0E15
   }
   Load
   {
 	species = Ar+
         T_load = 0.026
 	// n = 3.0E15
 	n = 3.0E15
   }
//  Load
//  {
//	species = Ar4p
//        T_load = 3.3
//	n = 1E16
//  }
//  Load
//  {
//	species = Arm
//        T_load = 3.3
//	n = 1E16
//  }
//  Load
//  {
//	species = Arr
//        T_load = 3.3
//	n = 1E16
//  }
//  Load
//  {
//	species = Ar
//        T_load = 3.3
//	n = 1E16
//  }
  Reactions
  {
    gas = Argon
  }
} //ends SpatialRegion
  DiagnosticControl
  {
//	debug
	Lsheath = 0.5 // n-(t) = Lsheath*n+(t)
//	Lsheath = 0. // turn off left hand sheath diagnostic
	Rsheath = 0.5 // n-(t) = Rsheath*n+(t)
//	Rsheath = 0. // turn off right hand sheath diagnostic
//	bulkvelocity
//	particleflux1
//	particleflux2
//	particleflux3
//	J1dotE1
//	energydensity1
//	averagevelocity1
//	temperature1
//	temperature
//	J2dotE2
	alloff
//	allon
//	ngriddiagupdate = 10 // update diagnostics every 10 timesteps
	ngriddiagupdate = 1 // update diagnostics every 1 timestep
	nsamppart = 200000 // sample 200000 particles from each species
	naverage=2000 // number of timesteps for grid time averages
//	naverage=1 //don't calculate time-average grid diagnostics
	ncomb=4 // what is this for
	histmax=100000
// here we add a switch for diagnostics of reaction rates
        reaction_diagnostics

  }
}
