{  // you can have comments after braces

  	Variables
    	{
      	// physical data
	melec = 9.11E-31
	mHe = 6.695E-27   	//4.002602*1.6726E-27
        e=1.6E-19 		//positive charge
    	} 

  	Species 
    	{
        name = E-
	q = -e
	m = melec
	reaction_type = E-
        np2c = 2E8 
	Force3V  		// For angular distributions
	useSecDefaultWeight
	maxEnergyForSigmaV = 100.
    	}


  	Species
    	{
      	name = He+
	q = e
	m = mHe
	reaction_type = He+
	np2c = 4E8
    	}


  	Species
    	{
      	name = He
	q = 0
	m = mHe
	reaction_type = He
	DiffusionFluid
	 	{
//	   	n = 1.0*3.2E20 	//
	  	n = 3.2E21 	// 100 mTorr
	   	T=0.026
	 	}
	np2c = 2E8
	Force3V
	useSecDefaultWeight
	subcycle=16
 	energyThrToAddAsPIC = 1 // add as PIC species above 10000 eV
     	}

	

Species 
{
   name = Hem1
   q = 0.0 
   m = mHe
   reaction_type = Hem1
	DiffusionFluid
	 	{
	   	n = 1.0*3.2E20
	   	T=0.026
	 	}
   np2c = 2E8
	subcycle=16
   energyThrToAddAsPIC = 0.5
}


Species 
{
   name = Hem3
   q = 0.0 
   m = mHe 
   reaction_type = Hem3
	DiffusionFluid
	 	{
	   	n = 1.0*3.2E20
	   	T=0.026
	 	}
   np2c = 2E8
 	subcycle=16
  energyThrToAddAsPIC = 0.5
}

Species 
{
   name = Hep1
   q = 0.0 
   m = mHe
   reaction_type = Hep1
   np2c = 2E8
	subcycle=16
   energyThrToAddAsPIC = 0.5
}


Species 
{
   name = Hep3
   q = 0.0 
   m = mHe 
   reaction_type = Hep3
   np2c = 2E8
	subcycle=16
   energyThrToAddAsPIC = 0.5
}

Species 
{
   name = U
   q = 0.0 
   m = mHe 
   reaction_type = Unit
   DiffusionFluid
  {
   n = 1 // unit density fluid
   T = 1E-6 // very low temperature
  }
   np2c = 2E8
   Force3V
   useSecDefaultWeight
   energyThrToAddAsPIC = 0.0
}


	SpatialRegion 
	{
  	dt = 3.687315E-11/2; 	// 2000 timesteps/period at 13.56 MHz 

  	Grid
  	{
	x0 = -0.0127 		// gap length = 0.0254 m
	x1 = 0.0127
	area= 0.00817 		// diameter = 0.102 m 
	ng = 1001
  	}

  	PoissonSolver
  	{	
	rhoback=0.0; 	
  	}
  

  	Conductor
  	{
   	j0 = 0

   	Circuit
    	{
	//C=1
	Drive
	{
	V
	AC = 400
        frequency = 13.56e6 	// 13.56e6
        phase = 0.0
	}

    	}

        Secondary 	// Recombination, PIC He hitting wall generates thermal PIC He
	{
	method = Basic
	energy_emit = 0.026
	secondary =  1.0
	impact_Species = He
	secSpecies = He
	threshold = 0.0
        np2c0 = 5E7
	}
       

        Secondary 		// 
	{
	method = Basic
	energy_emit = 0.026  
	secondary = 0.0 	// sec_e coefficient
	impact_Species = He+
	secSpecies = E-
	threshold = 0.0
        np2c0 = 5E7
	}

 
    	Distribution
    	{
      	decoupled 
      	species = He+
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 


    	Distribution
    	{
      	decoupled 
      	species = He
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 


  	}

	Boundary
	{
    	j0 = 500 		// for midpotential
    	j1 = 500

    	Distribution
    	{
      	decoupled 
      	species = E-
      	Emin = 0.0
      	Emax = 200.0
      	energybins = 800
    	}
	}

	Conductor
  	{
   	j0 = 1000

        Secondary 	// Recombination, PIC He hitting wall generates thermal PIC He
	{
	method = Basic
	energy_emit = 0.026
	secondary =  1.0
	impact_Species = He
	secSpecies = He
	threshold = 0.0
        np2c0 = 5E7
	}
       
       
        Secondary 		// 
	{
	method = Basic
	energy_emit = 0.026  
	secondary = 0.0 	// sec_e coefficient
	impact_Species = He+
	secSpecies = E-
	threshold = 0.0
        np2c0 = 5E7
	}


    	Distribution
    	{
      	decoupled 
      	species = He
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 

    	Distribution
    	{
      	decoupled 
      	species = He+
      	Emin = 0.0
      	Emax = 1000 
      	energybins = 1000 
      	anglebins = 1000
      	anglemax = 2
    	} 	

  	}

  	Load
  	{
	species = E-
        T_load = 3
	n = 20E15 					//5E14 is peak density
	densityfunction(x)=max(1-x^2/1E-4,0) 	//average is 2/3
  	}

  	Load
  	{
	species = He+
        T_load = 0.026
	n = 20E15
	densityfunction(x)=max(1-x^2/1E-4,0)       
  	}

  
  	Load
  	{
	species = He
        // T_load = 0.026
  	}



  	Reactions
  	{
    	gas = Helium
  	}

} 		//ends SpatialRegion


DiagnosticControl
{
	Lsheath = 0.6 		// n-(t) = Lsheath*n+(t)
	Rsheath = 0.6 		// n-(t) = Rsheath*n+(t)
	naverage= 271200	// 200000
	ngriddiagupdate = 1 	// update diagnostics every timestep
	allon
	nsamppart = 1000000 	// sample a million particles from each species
	histmax=100000
	reaction_diagnostics
   }

}
