#!/bin/bash
#SBATCH --job-name=JTG_03
#SBATCH --nodes=1 --exclusive
#SBATCH --mail-type=ALL
#SBATCH --mail-user=tumi@hi.is
#SBATCH --time=350:00:00
#SBATCH --partition=omnip


export OMP_NUM_THREADS=32
export MKL_NUM_THREADS=32
export KMP_DETERMINISTIC_REDUCTION=no
#export KMP_AFFINITY=granularity=fine,scatter

module load intel/compiler/2017.2

#The program to be run.
PROG=/users/home/tumi/ptsg/28JUL2016/pd1 -i inp/argon_1600mTorr_13MHz_50Am2_withmeta.inp -nox -dp 550000 -s 5500000 > Commands/Ar1600.txt

echo "Job started at: `date`"

${PROG}

echo "Job finished at: `date`"