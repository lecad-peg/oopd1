EmiNLSRCyl100612.inp -- 
Cylindrical model in Lieberman et al, PoP 15, 063505 (2008) with mobile ions.
{
Variables
{
	melec = 9.11E-31
	mion = 6.64E-26
}

Species 
{
   name = electrons
   q = -1.6E-19 
   m = melec 
   reaction_type = E-
   Force3V
   ForceNonRelativistic
   np2c=5E7
}

Species 
{
   name = Argon
   q = 0.0 
   m = mion 
   reaction_type = Ar
   np2c=2E10
   energyThrToAddAsPIC = 10000
   
   DiffusionFluid
  {
   n = 9.6E19 // 3 mTorr
   T = 0.026
  }
}

Species 
{
   name = Ar_ions
   q = 1.6E-19 
   m = mion 
   reaction_type = Ar+
   Force3V
   ForceNonRelativistic
   np2c=5E7
//   subcycle=10 //for dumpfile generation
}
SpatialRegion 
{
  dt = 3.125E-11; // 1000 timesteps per 32 MHz rf period
  Grid
  {
        Cylindrical
	r0 = 0.04
	r1 = 0.20
        height = 0.05
	ng = 2001
  }
  PoissonSolver
  {	
	rhoback=0.0; 	
  }
  

  Conductor
  {
   j0 = 0

   Circuit
    {
	C=1e-9
	Drive
	{
	V
	AC = 50.
        frequency = 32e6
        phase = 0.0
	}
    }
  }
Boundary
{
    j0 = 1000
    j1 = 1000
}

Conductor
  {
   j0 = 2000

   Circuit
    {
	R=0 // rhs voltage = I*R
	C=1
	Drive
	{
	V
	DC = 0.0
	}
    }
  }

  Load
  {
	species = electrons
        T_load = 5.09
	n=1.5E15
//	n = 1.6E15/1.6
//	densityfunction(x)=max(1-(x-0.13)*(x-0.13)/0.0049,0)
  }

  Load
  {
	species = Ar_ions
        T_load = 0.026
	n=1.5E15
//	n = 1.6E15/1.6
//	densityfunction(x)=max(1-(x-0.13)*(x-0.13)/0.0049,0)
  }

  Reactions
  {
    gas = Argon
  }

} //ends SpatialRegion


DiagnosticControl
{
	Lsheath = 0.5 // n-(t) = Lsheath*n+(t)
	Rsheath = 0.5 // n-(t) = Rsheath*n+(t)
	particleflux1
	particleflux2
	particleflux3
	J1dotE1
	energydensity1
	energydensity2
	energydensity3
//	averagevelocity1
	temperature1
	temperature2
	temperature3
	temperature
	J2dotE2
	J3dotE3
	JdotE
//	alloff
//	ngriddiagupdate = 10000// update diagnostics every 10 timesteps
	ngriddiagupdate = 1 // update diagnostics every 1 timestep
	nsamppart = 200000 // sample 200000 particles from each species
	naverage=16000 // number of timesteps for grid time averages
//	naverage=1 //don't calculate time-average grid diagnostics
	ncomb=4 // what is this for
	histmax=100000
	Diagnostic
	{
		name = Number(t)
		open
	}

}

}
