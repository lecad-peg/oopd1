-- multipactor.inp
   : Collisional Multipactor Discharge
{

Variables
{
  melec = 9.1E-31
  mproton = 1.68E-27
  pcharge = 1.6E-19
}

Species
{
  name = Argon
  np2c = 5e09
  q=0.
  m = 40*mproton
  reaction_type = Ar
  
  DiffusionFluid 
  {
   n = 1.92048E25
   T = 0.026
   }
}

Species
{
  name = el
  reaction_type = E-
  q = -pcharge
  m = melec
  ForceNonRelativistic
  np2c = 5E09
}

Species
{
  name = se_el
  reaction_type = E-
  q = -pcharge
  m = melec
  ForceNonRelativistic
  np2c = 5E09
}

Species // the ion species
{
  name = Ar_ions
  reaction_type = Ar+
  q = pcharge
  m = 40*mproton
  ForceNonRelativistic
  np2c = 5E09
}

SpatialRegion 
{
  dt = 1E-14
  Grid
  {
    x0 = 0.
    x1 = 0.0007
    ng = 4000
  }

  Variables
  {
    frequency = 2.85e9
  }
  
  PoissonSolver
  {
    Variables
    {
      E_amp = 2.82e6
    }
    efield_y(x,t) = E_amp*sin(2.*3.14*frequency*t)
  }

  Dielectric
  {
    j0 = 0
    j1 = 10
    epsilonr = 1
  }

  Dielectric
  {
    j0 = 3989
    j1 = 3999
    epsilonr = 10
    Distribution
    {
      decoupled 
      species = el
      Emin = 100.
      Emax = 100000.
      energybins = 1000
    }

    Secondary
    {
      method = Vaughan
      energy_emit = 2
      secondary = 2.0
      impact_Species = el, se_el
      secSpecies = se_el
      roughness = 1 
      threshold = 0.
      energy_max = 400. 
      meth = 1
    }

    BeamEmitter
    {
      inject_to_left
      T = 2
      I(t) = -1.3e3*(t < 1/frequency)
      species = el
      methods = 1
    }

  }

  Conductor
  {
    j0 = 0
    Circuit
    {
      Drive
      {
	V
	DC=0
      }
    }
  } 

  Reactions
  {
    gas = Argon
  }
} //ends SpatialRegion

DiagnosticControl
{
  bulkvelocity
  histmax = 100  // global histmax for all diagnostics
  nsamppart = 1000000 // sample a million particles from each species
  Diagnostic
  {
    name = number(t)
    open // open number(t) initially
  }
 Diagnostic
  {
    name = phi(x)
    open
  }
}

}
