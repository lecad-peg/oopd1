#!/bin/sh
#PBS -N IED_Ar
#PBS -l nodes=1:ppn=1
#PBS -l walltime=250:00:00
#PBS -M tumi@hi.is
#PBS -m abe
cd ptsg/28JUL2016/
export LD_LIBRARY_PATH=/users/home/tumi/instdir/lib
./pd1 -i inp/argon_16mTorr_13MHz_50Am2.inp -nox -dp 550000 -s 5500000 > Commands/Ar16.txt
./pd1 -i inp/argon_160mTorr_13MHz_50Am2.inp -nox -dp 550000 -s 5500000 > Commands/Ar160.txt
./pd1 -i inp/argon_1600mTorr_13MHz_50Am2.inp -nox -dp 550000 -s 5500000 > Commands/Ar1600.txt