Ex1rfdisArXeFastPICNeutrals.inp -- 
A simple 1D model of an rf discharge.
{  // you can have comments after braces
   //constant secondary coefficient 

Variables
{
	melec = 9.11E-31
	mion = 6.64E-26
        mionx = 2.1961e-25
}

Species 
{
   name = electrons
   q = -1.6E-19 
   m = melec 
   reaction_type = E-
   np2c = 1.0E9
}

Species 
{
   name = Argon
   q = 0.0 
   m = mion 
   reaction_type = Ar
   
   DiffusionFluid
  {
   n = 9.75E19*0.5 // 0.5*3 mTorr
   T = 0.026
  }
  np2c = 1.0E9
  Force3V
  useSecDefaultWeight
  energyThrToAddAsPIC = 1.0 // add as PIC particles if energy >1eV
}

Species 
{
   name = Ar_ions
   q = 1.6E-19 
   m = mion 
   reaction_type = Ar+
   np2c = 1.0E9
   Force3V
}

Species 
{
   name = Xenon
   q = 0.0 
   m = mionx 
   reaction_type = Xe
   
   DiffusionFluid
  {
   n = 9.75E19*0.5 // 0.5*3 mTorr
   T = 0.026
  }
  np2c = 1.0E9
  Force3V
  useSecDefaultWeight
  energyThrToAddAsPIC = 1.0 // add as PIC particles if energy >1eV
}

Species 
{
   name = Xe_ions
   q = 1.6E-19 
   m = mionx 
   reaction_type = Xe+
   np2c = 1.0E9
   Force3V
}

SpatialRegion 
{
  dt = 3.687315E-11; 
  Grid
  {
	x0 = 0.
	x1 = 0.1
	ng = 501
	area = 0.1
  }
  PoissonSolver
  {	
	rhoback=0.0; 	
  }
  

  Conductor
  {
   j0 = 0

   Circuit
    {
	Drive
	{
	V
	AC = 500.0
        frequency = 13.56e6
        phase = 180.0
	}
    }
  }


Boundary
{
    j0 = 250
    j1 = 250
    Distribution
    {
      decoupled 
      species = electrons
      Emin = 0.0
      Emax = 60.0
      energybins = 60
    }
}

Conductor
  {
   j0 = 500

    Distribution
    {
      decoupled 
      species = Ar_ions
      Emin = 0.0
      Emax = 250.0
      energybins = 250
      anglebins = 250
      anglemax = 0.2
    }

    Distribution
    {
      decoupled 
      species = Xe_ions
      Emin = 0.0
      Emax = 250.0
      energybins = 250
      anglebins = 250
      anglemax = 0.2
    }

    Distribution
    {
      decoupled 
      species = Argon
      Emin = 0.0
      Emax = 250.0
      energybins = 250
      anglebins = 250
      anglemax = 0.2
    }

    Distribution
    {
      decoupled 
      species = Xenon
      Emin = 0.0
      Emax = 250.0
      energybins = 250
      anglebins = 250
      anglemax = 0.2
    }

   Circuit
    {
	C = 1.0 //add huge capacitor to rhs
	R = 1.0 //add small resistor to rhs to track discharge current
    }
  }

  Load
  {
	species = electrons
        T_load = 3.3
	n = 3E15
//        XRandomStart
  }

  Load
  {
	species = Ar_ions
        T_load = 0.026
	n = 1E15 // load 1/3 argon ions
//        XRandomStart
  }

  Load
  {
	species = Xe_ions
        T_load = 0.026
	n = 2E15 // load 2/3 xenon ions
//        XRandomStart
  }


  Reactions
  {
    gas = Argon
}  
Reactions
  {
    gas = Xenon
}

Reactions
  {
   gas = Nobelgasxterms
  }

} //ends SpatialRegion


DiagnosticControl
{
//	debug
	Lsheath = 0.5 // n-(t) = Lsheath*n+(t)
//	Lsheath = 0. // turn off left hand sheath diagnostic
	Rsheath = 0.5 // n-(t) = Rsheath*n+(t)
//	Rsheath = 0. // turn off right hand sheath diagnostic
//	bulkvelocity
//	particleflux1
//	particleflux2
//	particleflux3
//	J1dotE1
//	energydensity1
//	averagevelocity1
//	temperature1
	temperature
//	J2dotE2
//	alloff
	ngriddiagupdate = 10 // update diagnostics every 10 timesteps
//	ngriddiagupdate = 1 // update diagnostics every 1 timestep
	nsamppart = 200000 // sample 200000 particles from each species
	naverage=2000 // number of timesteps for grid time averages
//	naverage=1 //don't calculate time-average grid diagnostics
	ncomb=4 // what is this for
	histmax=100000
	Diagnostic
	{
		name = number(t)
		open
	}

  }

}
