-- multipactor.inp
   : Vacuum Multipactor Discharge
{

Variables
{
  melec = 9.1E-31
  echarge = -1.6E-19
}

Species
{
  name = electrons
  q = echarge
  m = melec
  ForceNonRelativistic
  np2c = 5E9
}

Species
{
  name = secondary_electrons
  q = echarge
  m = melec
  ForceNonRelativistic
  np2c = 5E9
}

SpatialRegion 
{
  dt = 3E-14
  Grid
  {
    x0 = 0.
    x1 = 0.0007
    ng = 4000
  }

  Variables
  {
    frequency = 1e9
  }
  DirectSolver
  {
    Variables
    {
      E_amp = 3e6
    }
    efield_y(x,t) = E_amp*sin(2.*3.14*frequency*t)
  }
  Dielectric
  {
    j0 = 0
    j1 = 10
    epsilonr = 10
    Distribution
    {
      decoupled 
      species = electrons
      Emin = 100.
      Emax = 100000.
      energybins = 1000
    }
    Secondary
    {
      method = Vaughan
      energy_emit = 2
      secondary = 2.0
      impact_Species = electrons, secondary_electrons
      secSpecies = secondary_electrons
      roughness = 1 
      threshold = 0.
      energy_max = 400. 
      meth = 1
    }
    BeamEmitter
    {
      inject_to_right
      T = 2
      I(t) = -1.3e3*(t < 1/frequency)
      species = electrons
      methods = 1
    }
  }
  Dielectric
  {
    j0 = 3989
    j1 = 3999
    epsilonr = 1
  }
  Conductor
  {
    j0 = 0
    Circuit
    {
      Drive
      {
	V
	DC=0
      }
    }
  }
} //ends SpatialRegion

DiagnosticControl
{
  bulkvelocity
  histmax = 100  // global histmax for all diagnostics
  nsamppart = 1000000 // sample a million particles from each species
  Diagnostic
  {
    name = number(t)
    open // open number(t) initially
  }
 Diagnostic
  {
    name = phi(x)
    open
  }
}

}
