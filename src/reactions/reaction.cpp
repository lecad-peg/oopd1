// Reaction class
// by HyunChul Kim, 2006
// JTG and MAL melded file, 10/12/09
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "reactions/reaction.hpp"
#include "main/particlegroup.hpp"
#include "MCC/mcc.hpp"
#include "MCC/hydrocarbons_mcc.hpp"
#include "MCC/oxygen_mcc.hpp"
#include "MCC/chlorine_mcc.hpp"//20130604,Huang

ostring Reaction::get_speciesname(Species *sp) const
{
	ostring name;
	if (sp)
		name = sp->get_name()+"("+sp->get_reactionname()+")";
	else name = "(Empty)";
	return name;
}

ostring Reaction::get_speciesname_short(Species *sp) const
{
	ostring name;
	if (sp)
		name = sp->get_reactionname();
	else name = "Empty";
	return name;
}


/*
 * Print chemical equations by using get_chemical_equation.
 * JK 2019-12-02
 */
void Reaction::print_chemical_equation(bool _debug) const
{
	ostring name = get_reactiontypename();
	printf("\n  %s", name.c_str());			// JK DQW, 2019-11-25 - correctly printing reaction type
	name = get_chemical_equation(_debug);
	printf("\n   : %s", name.c_str());
}

/*
 * Get full name of chemical reaction - the reactants and products are fill species names
 */
ostring Reaction::get_chemical_equation(bool _debug) const
{
	ostring fancy_out = ostring();
	int num = reactants.size();
	if (num) {
		ostring name = get_reactiontypename();

		if (_debug) {
			fancy_out += ReactionTypeName(reactiontype);
			fancy_out += ostring(" (") + ostring(mcctype()) + ostring(")");
			fancy_out += ostring(": ");
		}

		for (int i=0; i<num; i++)
		{
			name = get_speciesname(reactants[i]);
			fancy_out += name();
			if (i<num-1) fancy_out += " + ";
		}
		fancy_out += " -> ";
		vector<ostring> names;
		switch (reactiontype)
		{
		case E_IONIZATION:
		case E_OIONIZATION:
		case E_O2IONIZATION:


		case E_ClIONIZATION://20130604,Huang, this part should be put before case SAMEMASS_EXCITATION
		case E_Cl2IONIZATION:
		case E_DETACHMENT_negCl:
		case E_Double_DET_negCl:
		case EDISSSingleIzCl2:
		case EDISSDoubleIzCl2:


		case E_DETACHMENT:
		case EDISSIZ:
			//     case POLARDISS:
		case E_DISS_IONIZATION:
		case E_EXCITATION:
		case E_DISS_EXCITATION1:
		case E_DISS_EXCITATION2:
		case E_DISS_EXCITATION3:
		case E_DISS_EXCITATION4:
		case E_DISS_EXCITATION5:
		case E_ROTATIONAL:
		case E_EXCITATION1:
		case E_EXCITATION2:
		case E_EXCITATION3:
		case E_EXCITATION4:
		case E_LEXCITATION:
		case E_SINGLET_DELTA:
		case E_SINGLET_SIGMA:
		case SAMEMASS_EXCITATION: // add MAL 12/27/10
			names.push_back(get_speciesname(reactants[0]));
			break;
		//case E_ELASTIC:
		//case EO_ELASTIC:   // Added JTG July 28 2009
		case SAMEMASS_ELASTIC:  // Added JTG November 20 2009
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;
		case B_SAMEMASS_ELASTIC:  // HH 05/17/16
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;
		case ANYMASS_ELASTIC:  // Added JTG November 23 2009
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;
		case E_EXC1D:  // Added JTG July 20 2009
			names.push_back(get_speciesname(reactants[0]));
			break;
		case E_EXC1S:  // Added JTG July 20 2009
			names.push_back(get_speciesname(reactants[0]));
			break;
		case E_EXC3P0:  // Added JTG July 30 2009
		case E_EXC5S0:  // Added JTG July 30 2009
		case E_EXC3S0:  // Added JTG July 30 2009
			//      case HALFMASS_ELASTIC:  // Added JTG May 20 2009
		case NEG_ELASTIC:
		case POS_ELASTIC:   // Added JTG July 31 2009
			//     case O2FRAG:   // Added JTG  August 17 2009
			//     case EDISSIZ  // Added by JTG August 18 2009
		case POSO_ELASTIC:   // Added JTG July 31 2009
		case NEUO_ELASTIC:  // Added JTG May 26 2009
			//      case P_eEXCHANGE:   // Added JTG July 26 2009
			//     case P2_eEXCHANGE:  // Added JTG July 26 2009
			//case NEGO_DETACHMENT:  // Added JTG July 26 2009
		case E_LOSS1:
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
		case E_LOSS2:
			names.push_back(get_speciesname(reactants[0]));
			break;
		case E_LOSS3:
			names.push_back(get_speciesname(reactants[0]));
			break;
		case E_LOSS4:
			names.push_back(get_speciesname(reactants[0]));
			break;
		case E_ELASTIC:  // Added by JTG november 20 2009
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;
		case Ch_EXCHANGE:
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;
		case Ch_EXCHANGEO:   // Added JTG July 31 2009
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;
		case O2O2_ELASTIC:   // Added MAL 1/5/10
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;
		case OO_ELASTIC:   // Added MAL 1/5/10
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;

		case E_LOSS_Cl://20130604,Huang this part should be put before case E_LOSS_Cl2 and case E_LOSS_Cl2_3Pu
		case El_negClCl2:
		case El_posCl2Cl2:
		case El_posClCl2:
		case El_ClCl2:
		case E_LOSS_Cl2:
			if (num > 1) names.push_back(get_speciesname(reactants[1]));// without break

		case E_LOSS_Cl2_3Pu:
			names.push_back(get_speciesname(reactants[0]));
			break;
		case E_LOSS_Cl2_1Pu:
			names.push_back(get_speciesname(reactants[0]));
			break;
		case E_LOSS_Cl2_3Pg:
			names.push_back(get_speciesname(reactants[0]));
			break;
		case E_LOSS_Cl2_1Pg:
			names.push_back(get_speciesname(reactants[0]));
			break;
		case E_LOSS_Cl2_3Su:
			names.push_back(get_speciesname(reactants[0]));
			break;
		case E_LOSS_Cl2_R1Pu:
			names.push_back(get_speciesname(reactants[0]));
			break;
		case E_LOSS_Cl2_R1Su:
			names.push_back(get_speciesname(reactants[0]));
			break;

		case CEposClCl:
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;
		case CEposCl2Cl2:
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;
		case CEnegClCl:
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;
		case Cl2Cl2_ELASTIC:
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;
		case ClCl_ELASTIC:
			if (num > 1) names.push_back(get_speciesname(reactants[1]));
			names.push_back(get_speciesname(reactants[0]));
			break;

		default:
			break;
		}

		int num_p = products_new.size();
		for (int i=0; i<num_p; i++)
		{
			names.push_back(get_speciesname(products_new[i]));
		}
		num_p = names.size();
		for (int i=0; i<num_p; i++)
		{
			if (names[i] != "")
			{
				int k=1;
				if (i > 0) fancy_out += " + ";
				for (int j=i+1; j<num_p; j++)
				{
					if (names[j] == names[i])
					{
						k++;
						names[j] = "";
					}
				}
				if (k > 1) fancy_out += ostring(k) + ostring(" ");
				fancy_out += names[i]();
			}
		}
	}

	return fancy_out;
}

/*
 * added by JK, DQW, 2019-11-25
 * Get chemical equation of current reaction; used for printing or diagnostics.
 * Methods is called from print_chemical_equation() to keep only one method to create fancy output of reactions.
 */
ostring Reaction::get_chemical_equation_short(bool _debug) const
{
	ostring fancy_out = ostring();
	int num = reactants.size();
	if (num) {
		ostring name = get_reactiontypename();
//		fancy_out += ReactionTypeName(reactiontype);

		if (_debug) {
			fancy_out += ReactionTypeName(reactiontype);
			fancy_out += ostring(" (") + ostring(mcctype()) + ostring(")");
			fancy_out += ostring(": ");
		}
//		fancy_out += ostring(": ");

		for (int i=0; i<num; i++)
		{
			name = get_speciesname_short(reactants[i]);
			fancy_out += name();
			if (i<num-1) fancy_out += " + ";
		}
		fancy_out += " -> ";
		vector<ostring> names;
		switch (reactiontype)
		{
		case E_IONIZATION:
		case E_OIONIZATION:
		case E_O2IONIZATION:
		case E_DETACHMENT:
		case EDISSIZ:
			//     case POLARDISS:
		case E_DISS_IONIZATION:
		case E_EXCITATION:
		case E_DISS_EXCITATION1:
		case E_DISS_EXCITATION2:
		case E_DISS_EXCITATION3:
		case E_DISS_EXCITATION4:
		case E_DISS_EXCITATION5:
		case E_ROTATIONAL:
		case E_EXCITATION1:
		case E_EXCITATION2:
		case E_EXCITATION3:
		case E_EXCITATION4:
		case E_LEXCITATION:
		case E_SINGLET_DELTA:
		case E_SINGLET_SIGMA:
		case SAMEMASS_EXCITATION: // add MAL 12/27/10
		names.push_back(get_speciesname_short(reactants[0]));
		break;
		//case E_ELASTIC:
		//case EO_ELASTIC:   // Added JTG July 28 2009
		case SAMEMASS_ELASTIC:  // Added JTG November 20 2009
			if (num > 1) names.push_back(get_speciesname_short(reactants[1]));
			names.push_back(get_speciesname_short(reactants[0]));
			break;
		case ANYMASS_ELASTIC:  // Added JTG November 23 2009
			if (num > 1) names.push_back(get_speciesname_short(reactants[1]));
			names.push_back(get_speciesname_short(reactants[0]));
			break;
		case E_EXC1D:  // Added JTG July 20 2009
			names.push_back(get_speciesname_short(reactants[0]));
			break;
		case E_EXC1S:  // Added JTG July 20 2009
			names.push_back(get_speciesname_short(reactants[0]));
			break;
		case E_EXC3P0:  // Added JTG July 30 2009
		case E_EXC5S0:  // Added JTG July 30 2009
		case E_EXC3S0:  // Added JTG July 30 2009
			//      case HALFMASS_ELASTIC:  // Added JTG May 20 2009
		case NEG_ELASTIC:
		case POS_ELASTIC:   // Added JTG July 31 2009
			//     case O2FRAG:   // Added JTG  August 17 2009
			//     case EDISSIZ  // Added by JTG August 18 2009
		case POSO_ELASTIC:   // Added JTG July 31 2009
		case NEUO_ELASTIC:  // Added JTG May 26 2009
			//      case P_eEXCHANGE:   // Added JTG July 26 2009
			//     case P2_eEXCHANGE:  // Added JTG July 26 2009
			//case NEGO_DETACHMENT:  // Added JTG July 26 2009
		case E_LOSS1:
			if (num > 1) names.push_back(get_speciesname_short(reactants[1]));
		case E_LOSS2:
			names.push_back(get_speciesname_short(reactants[0]));
			break;
		case E_LOSS3:
			names.push_back(get_speciesname_short(reactants[0]));
			break;
		case E_LOSS4:
			names.push_back(get_speciesname_short(reactants[0]));
			break;
		case E_ELASTIC:  // Added by JTG november 20 2009
			if (num > 1) names.push_back(get_speciesname_short(reactants[1]));
			names.push_back(get_speciesname_short(reactants[0]));
			break;
		case Ch_EXCHANGE:
			if (num > 1) names.push_back(get_speciesname_short(reactants[1]));
			names.push_back(get_speciesname_short(reactants[0]));
			break;
		case Ch_EXCHANGEO:   // Added JTG July 31 2009
			if (num > 1) names.push_back(get_speciesname_short(reactants[1]));
			names.push_back(get_speciesname_short(reactants[0]));
			break;
		case O2O2_ELASTIC:   // Added MAL 1/5/10
			if (num > 1) names.push_back(get_speciesname_short(reactants[1]));
			names.push_back(get_speciesname_short(reactants[0]));
			break;
		case OO_ELASTIC:   // Added MAL 1/5/10
			if (num > 1) names.push_back(get_speciesname_short(reactants[1]));
			names.push_back(get_speciesname_short(reactants[0]));
			break;
		default:
			break;
		}
		int num_p = products_new.size();
		for (int i=0; i<num_p; i++)
		{
			names.push_back(get_speciesname_short(products_new[i]));
		}
		num_p = names.size();
		for (int i=0; i<num_p; i++)
		{
			if (names[i] != "")
			{
				int k=1;
				if (i > 0) fancy_out += " + ";
				for (int j=i+1; j<num_p; j++)
				{
					if (names[j] == names[i])
					{
						k++;
						names[j] = "";
					}
				}
				if (k > 1) fancy_out += ostring(k) + ostring(" ");
				fancy_out += names[i]();
			}
		}
	}

	return fancy_out;
}


ReactionGroup::ReactionGroup(ReactionList _reactionlist,
		SpeciesList *specieslist,
		Scalar dt, Fields *thefield)
{
	reactionlist = _reactionlist;
	// debug
	// print_reactionlist(1); // MAL200115
	set_reactants();

#ifdef debugReactionGroup
	if (!(reactionlist.in_order_reactionlist())) // add to detect out-of-order reaction lists, MAL200116
	{
		printf("\n* WARNING: OUT-OF-ORDER REACTANTS IN THIS GROUP:\n* ******* the first reactant of the first reaction determines the mass used for the cross section energy"); // add, MAL200116
	}
	print_reactionlist(1); //add, MAL200116
#endif

	if (reactionlist.data()->get_mcctype() == "hc_mcc")
		themcc = new Hydrocarbons_MCC(specieslist, _reactionlist, dt, thefield);
	if (reactionlist.data()->get_mcctype() == "oxygen_mcc")
		themcc = new OXYGEN_MCC(specieslist, _reactionlist, dt, thefield);
	if (reactionlist.data()->get_mcctype() == "chlorine_mcc") 
		themcc = new CHLORINE_MCC(specieslist, _reactionlist, dt, thefield);//20130604,Huang
	else if (reactionlist.data()->get_mcctype() == "generic_mcc") 
		themcc = new MCC(specieslist, _reactionlist, dt, thefield);

	themcc->init(reactant1, reactant2);
}

void ReactionGroup::set_reactants()
{
	reactant1 = reactionlist.data()->get_reactants()[0];
	reactant2 = reactionlist.data()->get_reactants()[1];
	reactant1->set_3v_flag();
	reactant2->set_3v_flag();

#ifdef debugReactionGroup
	print_reactants(); // MAL200115
#endif
  
}

void  ReactionGroup::print_reactants() const
{
	printf("\n  ReactionGroup Reactant1 (ReactionType): %s (%s)\n", reactant1->get_name().c_str(), reactant1->get_reactionname().c_str());
	printf("  ReactionGroup Reactant2 (ReactionType): %s (%s)\n", reactant2->get_name().c_str(), reactant2->get_reactionname().c_str());
	printf("  NOTE: energy=0.5*mass(%s)*(relative velocity)^2 for all cross sections in this group of reactions", reactant1->get_name().c_str());
}

/*
 * Returning pointer to reaction's data array; collision (spatial) distribution.
 * JK 2019-11-25
 */
DataArray * ReactionGroup::get_collision_distribution(int reaction){
	return themcc->get_collision_distribution(reaction);
}

ReactionGroup::~ReactionGroup()
{
	delete themcc;
}

void ReactionGroup::update() const
{
#ifdef DEBUGmcc
	fprintf(stderr, "\n -- projectile is :");
	fprintf(stderr, "%s;",reactant1->get_name().c_str());
#endif

	// Do initialization of maxSigmaV and radiation trapping here for both fluid_particle and particle_particle
	// always use mass of reactant1 (reactant[0]) for calculation of cross section (energy), MAL200116
	if (themcc->get_maxSigmaV() == 0.) themcc->init_sumSigmaV(reactant1); // initialize maxSigmaV here; init_sumSigmaV also initializes the radiation escape factor g, MAL191219, MAL200112, JK200113

	if(reactant1->includes_particles() && !is_thesame(reactant1, reactant2)) // add "&& !is_thesame(reactant1, reactant2)" for like-particle collisions, MAL 11/23/09
	{
		// particle-particle collisions
		if(reactant2->includes_particles()){
			themcc->particle_particle(reactant1, reactant2);}
		if(reactant2->includes_fluids()){
			themcc->fluid_particle(reactant2, reactant1);}
	}
	if(reactant1->includes_fluids() && !is_thesame(reactant1, reactant2)) // add "&& !is_thesame(reactant1, reactant2)" for like-particle collisions, MAL 11/23/09
	{
		if(reactant2->includes_particles()){
			themcc->fluid_particle(reactant1, reactant2);}
		// fluid-fluid collisions
//    if(reactant2->includes_fluids()) fluid_fluid(reactant1, reactant2);
    if(reactant2->includes_fluids())
      themcc -> fluid_fluid(reactant1, reactant2); // MAL200214
	}
	if(is_thesame(reactant1, reactant2)) // add if statement for like-particle collisions, MAL 11/23/09
	{
			if(reactant1->includes_particles() && reactant1->includes_fluids()){
			themcc->fluid_particle(reactant1,reactant2);}
			if(reactant1->includes_particles()){ // for like-particle collisions, uncomment this statement to add particle-particle collisions, MAL 11/24/09
			themcc->particle_particle(reactant1,reactant2);} // for like-particle collisions, uncomment this statement to add particle-particle collisions, MAL 11/24/09
//    if(reactant1->includes_fluids()) fluid_fluid(reactant1,reactant2); // note: fluid-fluid collisions not implemented in oopd1, MAL 11/23/09
    if(reactant1->includes_fluids())
      themcc -> fluid_fluid(reactant1,reactant2); // MAL200214
	}
	themcc->time_average(); // for icp heating, calculate time-average number of collisions, averaged over naverage, MAL 1/8/11
}

// mindgame: This is not general merging, but special kind of merging.
void ReactionList::merge_reactionlist(ReactionList _default_reactionlist,
		// oopicList <Reaction> &resultant_reactions) const
		ReactionList &resultant_reactions) const
{
	// mindgame: add some default reactions which reactiontype is not
	//          included in reactionlist.
	int num = _default_reactionlist.num();
	for (int i=0; i<num; i++)
	{
		if (!include(_default_reactionlist.data(i)->get_reactiontype()))
			resultant_reactions.add(new Reaction(_default_reactionlist.data(i)));
	}
	Reaction *reaction, *parse_reaction;
	ReactionList searched_reaction;
	num = reactions.size();
	for (int j=0; j<num; j++)
	{
		parse_reaction=reactions[j];
		if (!parse_reaction->is_empty())
		{
			searched_reaction = _default_reactionlist.find(parse_reaction->get_reactiontype());
			int reaction_num;
			if (parse_reaction->is_reactant_empty())
				reaction_num = searched_reaction.num();
			else
				reaction_num = 1;
			for (int i=0; i<reaction_num; i++)
			{
				reaction = new Reaction(parse_reaction);
				if (parse_reaction->is_reactant_empty())
					reaction->set_reactant(searched_reaction.data(i)->get_reactants());
				if (parse_reaction->is_product_empty())
				{
					if (!searched_reaction.data(i)->is_product_empty2())
						reaction->set_product(searched_reaction.data(i)->get_products());
				}
				else
				{
					reaction->pop_products(reaction->get_products().size()-searched_reaction.data(i)->get_products().size());
				}
				if (parse_reaction->is_xsection_empty() &&
						!searched_reaction.data(i)->is_xsection_empty())
					reaction->set_xsection(searched_reaction.data(i)->get_xsection());
				if (parse_reaction->is_characteristic_value_empty() &&
						!searched_reaction.data(i)->is_characteristic_value_empty())
					reaction->set_characteristic_values(searched_reaction.data(i)->get_characteristic_values());
				resultant_reactions.add(reaction);
			}
		}
	}
}
