// ReactionSubParse class
// by HyunChul Kim, 2006
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "xsections/xsection.hpp"
#include "xsections/xsectionsamples.hpp"
#include "utils/PairArray.hpp"
#include "utils/arbitrary_func.hpp"
#include "xsections/xsection_func.hpp"
#include "reactions/reaction.hpp"
#include "main/particlegroup.hpp"
#include "reactions/reactionSubParse.hpp"
#include "utils/PairArray.hpp"
#include "utils/datafile.hpp"

ReactionSubParse::ReactionSubParse(FILE *fp,
		   oopicList<Parameter<Scalar> > float_variables, 
		   oopicList<Parameter<int> > int_variables,
		   SpeciesList* slist, const ostring *_gasname)
{
  specieslist = slist;
  gasname = _gasname;
  species_iter.restart(*specieslist);
  init_parse(fp, float_variables, int_variables);
}

void ReactionSubParse::setparamgroup()
{
  pg.add_string("type", &reactionname);
  pg.add_fparameter("x_type2_e1", &b0);
  pg.add_fparameter("x_type2_e2", &b1);
  pg.add_fparameter("threshold", &e_threshold);
  pg.add_fparameter("x_type1_e1", &c1);
  pg.add_fparameter("x_type1_e2", &c2);
  pg.add_fparameter("x_type1_sigmamax", &sMax);
  pg.add_function("x_function", "", &x_func, 1, "e");
  pg.add_string("x_file", &x_file);
  pg.add_strings("reactant1", &reactant1_names);
  pg.add_strings("reactant2", &reactant2_names);
}

void ReactionSubParse::build_xsection()
{
// mindgame: create x-section from parameters
  if (c1 >= 0 && c2 >= 0 && sMax >= 0) 
  {
    if (xsection)
      terminate_run("Cross-section types are specified more than once.");
    else
      xsection = new Xsection_type1(e_threshold, c1, c2, sMax, reactiontype);
  }
  if (b0 >= 0 && b1 >= 0)
  {
    if (xsection)
      terminate_run("Cross-section types are specified more than once.");
    else
      xsection = new Xsection_type2(b0, b1, reactiontype);
  }
  if (x_func.is_fparsed())
  {
    if (xsection)
      terminate_run("Cross-section types are specified more than once.");
    else
      xsection = new Xsection_Func(&x_func, e_threshold, reactiontype);
  }
  if (x_file != "")
  {
    if (xsection)
      terminate_run("Cross-section types are specified more than once.");
    else
    {
      ScalarDataFile sdf(x_file());
      xsection = new Xsection_Func(sdf.get_pairarray(), e_threshold, reactiontype);
//      pairarray.print_scalar_array();
    }
  }
}

void ReactionSubParse::convert_ostrings_to_species()
{
  for (int i=reactant1_names.size()-1;i>=0;i--)
    reactant1.push_back(Species::get_species_named_with(species_iter, reactant1_names[i]));
  for (int i=reactant2_names.size()-1;i>=0;i--)
    reactant2.push_back(Species::get_species_named_with(species_iter, reactant2_names[i]));
}

void ReactionSubParse::init()
{
  reactiontype = ReactionTypeName(reactionname);
  build_xsection();
  convert_ostrings_to_species();
  build_reactionlist();
}

void ReactionSubParse::setdefaults()
{
  classname = "Reaction";
  set_msg(classname);

  xsection = NULL;
  e_threshold = 0;
  c1 = -1;
  c2 = -1;
  sMax = -1;
  b0 = -1;
  b1 = -1;
  reactiontype = NO_TYPE;
  x_func.setconstant(0.0);
  x_file = "";
}

void ReactionSubParse::build_reactionlist()
{
  Reaction *reaction;
  ostring mcctype;
  GasType gastype = GasTypeName(*gasname);
  if (gastype == CH4_GAS)
    mcctype="hc_mcc";
  else
    mcctype="generic_mcc";
  if (gastype == O2_GAS)
    mcctype="oxygen_mcc";
  if (gastype == Cl2_GAS)//20130604,Huang
    mcctype="chlorine_mcc";
  if (debug()) printf("\n* Reactions from SubParse");
  if (!reactant1.size() && !reactant2.size())
  {
    reaction =new Reaction(gastype, reactiontype, NULL, NULL, xsection, mcctype());
    reactionlist.add(reaction);
    if (debug())
    { 
      printf("\n  %s:", ReactionTypeName(reactiontype).c_str());
      if (reaction->is_empty())
        printf(" excluded");
      else
        printf(" not finished building yet");
    }
  }
  else {
    for (int i=reactant1.size()-1; i>=0; i--)
    {
      for (int j=reactant2.size()-1; j>=0; j--)
      {
        reaction =new Reaction(gastype, reactiontype, reactant1[i], reactant2[j],
		               xsection, mcctype());
        reactionlist.add(reaction);
        if (debug()) reaction->print_chemical_equation(debug());
      }
    }
  }
  if (debug()) printf("\n");
}

