// ReactionType
// by HyunChul Kim, 2006
// JTG and MAL melded file, 10/12/09
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "reactions/reaction.hpp"

ostring GasTypeName(GasType gastype)
{
	switch (gastype)
	{
	case ARGON_GAS:
		return "Argon";
		break;
	case HYDROGEN_GAS:
		return "Hydrogen";
		break;
	case XENON_GAS:
		return "Xenon";
		break;
	case HELIUM_GAS:
		return "Helium";
		break;
	case FAKE_GAS:
		return "Fake";
		break;
	case CH4_GAS:
		return "CH4";
		break;
	case CH3_GAS:
		return "CH3";
		break;
	case CH2_GAS:
		return "CH2";
		break;
	case CH_GAS:
		return "CH";
		break;
	case CARBON_GAS:
		return "Carbon";
		break;
	case O2_GAS:
		return "Oxygen";
		break;
	case Cl2_GAS://20130604,Huang
		return "Chlorine";
		break;
	case NOBELGASXTERMS_GAS: // Added by JTG November 21 2009
		return "Nobelgasxterms";
		break;
	}
	return "";
}

GasType GasTypeName(ostring name)
{
	if (name == "Argon") return ARGON_GAS;
	else if (name == "Hydrogen") return HYDROGEN_GAS;
	else if (name == "Xenon") return XENON_GAS;
	else if (name == "Helium") return HELIUM_GAS;
	else if (name == "Fake") return FAKE_GAS;
	else if (name == "CH4") return CH4_GAS;
	else if (name == "CH3") return CH3_GAS;
	else if (name == "CH2") return CH2_GAS;
	else if (name == "CH") return CH_GAS;
	else if (name == "Carbon") return CARBON_GAS;
	else if (name == "Oxygen") return O2_GAS;
	else if (name == "Chlorine") return Cl2_GAS;//20130604,Huang
	else if (name == "Nobelgasxterms") return NOBELGASXTERMS_GAS; // Added by JTG November 21 2009
	else
	{
		fprintf(stderr, "%s: unknown Gas Type\n", name());
		return FAKE_GAS;
	}
}

ostring ReactionTypeName(ReactionType reactiontype)
{
	switch (reactiontype)
	{
	case E_ELASTIC:
		return "e_elastic";
		break;
	case EO_ELASTIC:
		return "eo_elastic";
		break;
	case P_ELASTIC:
		return "p_elastic";
		break;
	case E_IONIZATION:
		return "e_ionization";
		break;
	case E_OIONIZATION:
		return "eIonizationo";
		break;
	case E_O2IONIZATION:
		return "eIonizationo2";
		break;
	case E_Cl2IONIZATION: //20130604,Huang
		return "eIonizationcl2";
		break;
	case EDISSSingleIzCl2:
		return "edisssingleizcl2";
		break;
	case EDISSDoubleIzCl2:
		return "edissdoubleizcl2";
		break;
	case E_DISS_ATTACHMENT_Cl2:
		return "edissattachcl2";
		break;
	case POLARDISSCl2:
		return "polardisscl2";
		break;
	case E_ClIONIZATION:
		return "eIonizationcl";
		break;
	case E_DETACHMENT_negCl:
		return "e_detachment_negcl";
		break;
	case E_Double_DET_negCl:
		return "e_double_det_negcl";
		break;
	case NEGCl2_DETCH:
		return "negcl2detch";
		break;
	case NEGCl_DETACHMENT:
		return "negcldetachment";
		break;
	case E_DISS_REC_posCl2:
		return "edissrecposcl2";
		break;
	case MN_posClnegCl:
		return "mnposclnegcl";
		break;
	case MN_posCl2negCl:
		return "mnposcl2negcl";
		break;
	case CEposClCl:
		return "ce_posclcl";
		break;
	case CEposCl2Cl2:
		return "ce_poscl2cl2";
		break;
	case CEposClCl2:
		return "ce_posclcl2";
		break;
	case CEposCl2Cl:
		return "ce_poscl2cl";
		break;
	case CEnegClCl:
		return "ce_negclcl";
		break;
	case E_LOSS_Cl2:
		return "eloss_cl2";
		break;
	case E_LOSS_Cl:
		return "eloss_cl";
		break;
	case E_LOSS_Cl2_3Pu:
		return "eloss_cl2_3pu";
		break;
	case E_LOSS_Cl2_1Pu:
		return "eloss_cl2_1pu";
		break;
	case E_LOSS_Cl2_3Pg:
		return "eloss_cl2_3pg";
		break;
	case E_LOSS_Cl2_1Pg:
		return "eloss_cl2_1pg";
		break;
	case E_LOSS_Cl2_3Su:
		return "eloss_cl2_3su";
		break;
	case E_LOSS_Cl2_R1Pu:
		return "eloss_cl2_r1pu";
		break;
	case E_LOSS_Cl2_R1Su:
		return "eloss_cl2_r1su";
		break;
	case Cl2Cl2_ELASTIC:
		return "cl2cl2_elastic";
		break;
	case ClCl_ELASTIC:
		return "clcl_elastic";
		break;
	case El_ClCl2:
		return "elclcl2";
		break;
	case El_posCl2Cl2:
		return "elposcl2cl2";
		break;
	case El_posClCl2:
		return "elposclcl2";
		break;
	case El_negClCl2:
		return "elnegclcl2";
		break;
	case Cl2FRAG:
		return "fragmentation_cl2";
		break;

	case E_DISS_IONIZATION:
		return "e_diss_ionization";
		break;
	case E_EXCITATION:
		return "e_excitation";
		break;
	case PHOTON_EMISSION: // add MAL 12/27/10
		return "photon_emission";
		break;
	case PENNING_IONIZATION: // add MAL 12/27/10
		return "Penning_ionization";
		break;
     case AM_PENNING_IONIZATION: // add MAL 12/27/10
      return "AM_Penning_ionization";
      break;
	case SAMEMASS_EXCITATION: // add MAL 12/27/10
		return "samemass_excitation";
		break;
	case NONRES_Ch_EXCHANGE: // add MAL 11/20/11
		return "nonres_ch_exchange";
		break;
	case Ch_EXCHANGE:
		return "ch_exchange";
		break;
	case Ch_EXCHANGEDM: // added by JTG November 21 2009
		return "ch_exchange";
		break;
	case Ch_EXCHANGEO:  // added by JTG July 31 2009
		return "ch_exchangeo";
		break;
	case P_eEXCHANGE:
		return "p_e_exchange";
		break;
    case P_e2EXCHANGE:    // added by JTG October 15 2014
      return "p_e2_exchange";
      break;
	case P2_eEXCHANGE:   // added by JTG July 26 2009
		return "p2_e_exchange";
		break;
	case POLARDISS:   // added by JTG July 28 2009
		return "polar_diss";
		break;
	case P_hEXCHANGE:
		return "p_h_exchange";
		break;
	case SAMEMASS_ELASTIC:
		return "samemass_elastic";
		break;
	case ANYMASS_ELASTIC:
		return "anymass_elastic";
		break;
	case B_ANYMASS_ELASTIC: //add SAS July 9 2014
		return "b_anymass_elastic";
		break;
    case B_SAMEMASS_ELASTIC: //add SAS July 9 2014
		return "b_samemass_elastic";
		break;
	case HALFMASS_ELASTIC:    // added by JTG MAy 20 2009
		return "halfmass_elastic";
		break;
	case E_DISS_RECOMBINATION1:
		return "e_diss_recombination1";
		break;
	case E_DISS_RECOMBINATION2:
		return "e_diss_recombination2";
		break;
	case E_DISS_RECOMBINATION3:
		return "e_diss_recombination3";
		break;
	case E_DISS_EXCITATION1:
		return "e_diss_excitation1";
		break;
	case E_DISS_EXCITATION2:
		return "e_diss_excitation2";
		break;
	case E_DISS_EXCITATION3:
		return "e_diss_excitation3";
		break;
	case E_DISS_EXCITATION4:
		return "e_diss_excitation4";
		break;
	case E_DISS_EXCITATION5:
		return "e_diss_excitation5";
		break;
	case E_EXCITATION1:
		return "e_excitation1";
		break;
	case E_EXCITATION2:
		return "e_excitation2";
		break;
	case E_EXCITATION3:
		return "e_excitation3";
		break;
	case E_EXCITATION4:
		return "e_excitation4";
		break;
	case E_LEXCITATION:
		return "e_lexcitation";
		break;
	case E_SINGLET_DELTA:
		return "e_singlet_delta";
		break;
    case E_SINGLET_DELTAdex:
		return "e_singlet_deltadex"; // Added JTG January 9 2013
		break;
    case E_SINGLET_SIGMAdex:
		return "e_singlet_sigmadex"; // Added HH February 9 2016
		break;
	case E_SINGLET_SIGMA:
		return "e_singlet_sigma";  // Corrected by JTG July 26 2009
		break;
	case E_LOSS1:
		return "e_loss1";
		break;
	case E_LOSS2:
		return "e_loss2";
		break;
	case E_LOSS3:
		return "e_loss3";
		break;
	case E_LOSS4:
		return "e_loss4";
		break;
	case E_DISS_RECOMBINATION:
		return "e_diss_recombination";
		break;
	case E_ROTATIONAL:
		return "e_rotation";
		break;
	case E_DISS_ATTACHMENT:
		return "e_diss_attachment"; // attatchmen -> attachment, MAL 4/13/09
		break;
	case EDISSIZ:
		return "e_diss_ionization"; // Added by JTG August 18 2009
		break;
	case NEG_ELASTIC:
		return "neg_elastic";
		break;
	case POS_ELASTIC:    // Added by JTG July 31 2009
		return "pos_elastic";
		break;
	case O2FRAG:    // Added by JTG August 17 2009
		return "fragmentation";
		break;
	case POSO_ELASTIC:    // Added by JTG July 31 2009
		return "poso_elastic";
		break;
	case NEUO_ELASTIC:  // Added by JTG May 26 2009
		return "neuo_elastic";
		break;
	case NEG_DETCH:
		return "neg_detch";
		break;
	case NEGO_DETACHMENT: // Added by JTG July 17 2009
		return "nego_detch";
		break;
	case MUTUAL_NEUTRALIZATION:
		return "mutual_neutralization";
		break;
	case MUTUAL_NEUTRALIZATIONO:
		return "mutual_neutralization";
		break;
	case O1DTOO2b: // Added by JTG February 10 2015
		return "o1dtoo2b";
		break;
	case O2bO2quench: // Added by JTG February 13 2015
		return "o2bo2quench";
		break;
	case E_DETACHMENT:
		return "e_detachment";
		break;
	case E_EXC1D: // Added by JTG July 20 2009
		return "e_excitation_1D";
		break;
	case E_EXC1Ddex: // Added by JTG January 10 2013
		return "e_excitation_1Ddex";
		break;
	case E_EXC1S:   // Added by JTG July 20 2009
		return "e_excitation_1S";
		break;
	case E_EXC3P0:   // Added by JTG July 30 2009
		return "e_excitation_3P0";
		break;
	case E_EXC5S0:   // Added by JTG July 30 2009
		return "e_excitation_5S0";
		break;
	case E_EXC3S0:   // Added by JTG July 30 2009
		return "e_excitation_3S0";
		break;
	case O2O2_ELASTIC: // Added by MAL 1/5/10
		return "o2o2_elastic";
		break;
	case OO_ELASTIC: // Added by MAL 1/5/10
		return "oo_elastic";
		break;

	case NO_TYPE:
		return "unknown type";
		break;
	}
	return "";
}   

ReactionType ReactionTypeName(ostring name)
{
	if (name == "e_elastic") return E_ELASTIC;
	else if (name == "eo_elastic") return EO_ELASTIC;
	else if (name == "p_elastic") return P_ELASTIC;
	else if (name == "e_ionization") return E_IONIZATION;
	else if (name == "eIonizationo") return E_OIONIZATION;
	else if (name == "eIonizationo2") return E_O2IONIZATION;

	else if (name == "eIonizationcl2") return E_Cl2IONIZATION;//20130604,Huang
	else if (name == "edisssingleizcl2") return EDISSSingleIzCl2;
	else if (name == "edissdoubleizcl2") return EDISSDoubleIzCl2;
	else if (name == "edissattachcl2") return E_DISS_ATTACHMENT_Cl2;
	else if (name == "polardisscl2") return POLARDISSCl2;
	else if (name == "eIonizationcl") return E_ClIONIZATION;
	else if (name == "e_detachment_negcl") return E_DETACHMENT_negCl;
	else if (name == "e_double_det_negcl") return E_Double_DET_negCl;
	else if (name == "negcl2detch") return NEGCl2_DETCH;
	else if (name == "negcldetachment") return NEGCl_DETACHMENT;
	else if (name == "edissrecposcl2") return E_DISS_REC_posCl2;
	else if (name == "mnposcl2negcl") return MN_posCl2negCl;
	else if (name == "mnposclnegcl") return MN_posClnegCl;
	else if (name == "ceposclcl") return CEposClCl;
	else if (name == "ceposcl2cl2") return CEposCl2Cl2;
	else if (name == "ceposclcl2") return CEposClCl2;
	else if (name == "ceposcl2cl") return CEposCl2Cl;
	else if (name == "cenegclcl") return CEnegClCl;
	else if (name == "eloss_cl2") return E_LOSS_Cl2;
	else if (name == "eloss_cl") return E_LOSS_Cl;
	else if (name == "eloss_cl2_3pu") return E_LOSS_Cl2_3Pu;
	else if (name == "eloss_cl2_1pu") return E_LOSS_Cl2_1Pu;
	else if (name == "eloss_cl2_3pg") return E_LOSS_Cl2_3Pg;
	else if (name == "eloss_cl2_1pg") return E_LOSS_Cl2_1Pg;
	else if (name == "eloss_cl2_3su") return E_LOSS_Cl2_3Su;
	else if (name == "eloss_cl2_r1pu") return E_LOSS_Cl2_R1Pu;
	else if (name == "eloss_cl2_1su") return E_LOSS_Cl2_R1Su;
	else if (name == "cl2cl2_elastic") return Cl2Cl2_ELASTIC;
	else if (name == "clcl_elastic") return ClCl_ELASTIC;
	else if (name == "elclcl2") return El_ClCl2;
	else if (name == "elposcl2cl2") return El_posCl2Cl2;
	else if (name == "elposclcl2") return El_posClCl2;
	else if (name == "elnegclcl2") return El_negClCl2;
	else if (name == "fragmentation_cl2") return Cl2FRAG;

	else if (name == "e_diss_ionization") return E_DISS_IONIZATION;
	else if (name == "e_excitation") return E_EXCITATION;
	else if (name == "photon_emission") return PHOTON_EMISSION; // add MAL 12/27/10
	else if (name == "Penning_ionization") return PENNING_IONIZATION; // add MAL 12/27/10
	else if (name == "AM_Penning_ionization") return AM_PENNING_IONIZATION; // add MAL 12/27/10
	else if (name == "samemass_excitation") return SAMEMASS_EXCITATION; // add MAL 12/27/10
	else if (name == "nonres_ch_exchange") return NONRES_Ch_EXCHANGE; // add MAL 11/20/11
	else if (name == "ch_exchange") return Ch_EXCHANGE;
	else if (name == "ch_exchangedm") return Ch_EXCHANGEDM;
	else if (name == "ch_exchangeo") return Ch_EXCHANGEO;
	else if (name == "p_e_exchange") return P_eEXCHANGE;
	else if (name == "p_e2_exchange") return P_e2EXCHANGE;
	else if (name == "p2_e_exchange") return P2_eEXCHANGE;
	else if (name == "p_h_exchange") return P_hEXCHANGE;
	else if (name == "samemass_elastic") return SAMEMASS_ELASTIC;
	else if (name == "anymass_elastic") return ANYMASS_ELASTIC;
	else if (name == "b_anymass_elastic") return B_ANYMASS_ELASTIC; //add SAS July 9 2014
	else if (name == "b_samemass_elastic") return B_SAMEMASS_ELASTIC; //add SAS July 9 2014
	else if (name == "halfmass_elastic") return HALFMASS_ELASTIC;
	else if (name == "e_diss_recombination1") return E_DISS_RECOMBINATION1;
	else if (name == "e_diss_recombination2") return E_DISS_RECOMBINATION2;
	else if (name == "e_diss_recombination3") return E_DISS_RECOMBINATION3;
	else if (name == "e_diss_excitation1") return E_DISS_EXCITATION1;
	else if (name == "e_diss_excitation2") return E_DISS_EXCITATION2;
	else if (name == "e_diss_excitation3") return E_DISS_EXCITATION3;
	else if (name == "e_diss_excitation4") return E_DISS_EXCITATION4;
	else if (name == "e_diss_excitation5") return E_DISS_EXCITATION5;
	else if (name == "e_excitation1") return E_EXCITATION1;
	else if (name == "e_excitation2") return E_EXCITATION2;
	else if (name == "e_excitation3") return E_EXCITATION3;
	else if (name == "e_excitation4") return E_EXCITATION4;
	else if (name == "e_lexcitation") return E_LEXCITATION;
	else if (name == "e_singlet_delta") return E_SINGLET_DELTA;
	else if (name == "e_singlet_deltadex") return E_SINGLET_DELTAdex;
	else if (name == "e_singlet_sigmadex") return E_SINGLET_SIGMAdex;
	else if (name == "e_singlet_sigma") return E_SINGLET_SIGMA;
	else if (name == "e_loss1") return E_LOSS1;
	else if (name == "e_loss2") return E_LOSS2;
	else if (name == "e_loss3") return E_LOSS3;
	else if (name == "e_loss4") return E_LOSS4;
	else if (name == "e_rotational") return E_ROTATIONAL;
	else if (name == "e_diss_recombination") return E_DISS_RECOMBINATION;
	else if (name == "e_diss_attachment") return E_DISS_ATTACHMENT;
	else if (name == "e_diss_ionization") return EDISSIZ; // added by JTG August 18 2009
	else if (name == "neg_elastic") return NEG_ELASTIC;
	else if (name == "pos_elastic") return POS_ELASTIC; // added by JTG July 31 2009
	else if (name == "fragmentation") return O2FRAG; // added by JTG August 17 2009
	else if (name == "pos_elastic") return POSO_ELASTIC; // added by JTG July 31 2009
	else if (name == "neuo_elastic") return NEUO_ELASTIC; // added by JTG May 26 2009
	else if (name == "neg_detch") return NEG_DETCH;
	else if (name == "nego_detch") return NEGO_DETACHMENT;  // added by JTG July 17 2009
	else if (name == "polar_diss") return POLARDISS;  // added by JTG July 28 2009
	else if (name == "e_1D") return E_EXC1D;   // added by JTG July 20 2009
	else if (name == "e_1Ddex") return E_EXC1Ddex;   // added by JTG January 10 2013
	else if (name == "e_1S") return E_EXC1S;   // added by JTG July 20 2009
	else if (name == "e_3P0") return E_EXC3P0;   // added by JTG July 30 2009
	else if (name == "e_5S0") return E_EXC5S0;   // added by JTG July 30 2009
	else if (name == "e_3S0") return E_EXC3S0;   // added by JTG July 30 2009
	else if (name == "mutual_neutralization") return MUTUAL_NEUTRALIZATION;
	else if (name == "mutual_neutralization") return MUTUAL_NEUTRALIZATIONO;   // added by JTG July 21 2009
	else if (name == "o1dtoo2b") return O1DTOO2b; // JTG February 10 2015
	else if (name == "o2bo2quench") return O2bO2quench; // JTG February 13 2015
	else if (name == "e_detachment") return E_DETACHMENT; //detchament -> detachment, MAL, 4/13/09
	else if (name == "o2o2_elastic") return O2O2_ELASTIC; // added by MAL 1/5/10
	else if (name == "oo_elastic") return OO_ELASTIC; // added by MAL 1/5/10
	else
	{
		fprintf(stderr, "%s: unknown Reaction Type\n", name());
		return NO_TYPE;
	}
}   

ostring ReactionSpeciesTypeName(ReactionSpeciesType rst)
{
	switch (rst)
	{
	case E:
		return "E-";
		break;
	case Ar:
		return "Ar";
		break;
	case Ar4p: // add MAL 11/15/10
		return "Ar4p";
		break;
	case Arm: // add MAL 11/17/10
		return "Arm";
		break;
	case Arr: // add MAL 11/17/10
		return "Arr";
		break;
	case Unit: // add MAL 11/16/10
		return "Unit";
		break;
	case Ar_ion:
		// return "Ar+";
		return "Ar_ion"; // MAL200226
		break;
	case Xe:
		return "Xe";
		break;
	case Xe_ion:
		return "Xe+";
		break;
	case He:
		return "He";
		break;
	case Hep3: // add MAL 11/15/10 (SAS July 2 2014)
		return "Hep3";
		break;
	case Hep1: // add MAL 11/15/10 (SAS July 2 2014)
		return "Hep1";
		break;
	case Hem3: // add MAL 11/17/10 (SAS July 2 2014)
		return "Hem3";
		break;
	case Hem1: // add MAL 11/17/10 (SAS July 2 2014)
		return "Hem1";
		break;
	case He_ion:
		return "He+";
		break;
	case H:
		return "H";
		break;
	case H2:
		return "H2";
		break;
	case H_ion:
		return "H+";
		break;
	case CH4:
		return "CH4";
		break;
	case CH4_ion:
		return "CH4+";
		break;
	case CH3:
		return "CH3";
		break;
	case CH3_ion:
		return "CH3+";
		break;
	case CH2:
		return "CH2";
		break;
	case CH2_ion:
		return "CH2+";
		break;
	case CH:
		return "CH";
		break;
	case CH_ion:
		return "CH+";
		break;
	case O2_ion:
		return "O2_ion"; // "O2+" -> "O2_ion", MAL 4/14/09
		break;
	case O2:
		return "O2";
		break;
	case O:
		return "O";
		break;
	case O_ion:
		return "O_ion";
		break;
	case O2_sing_delta:
		return "O2_sing_delta";
		break;
	case O2_sing_sigma:
		return "O2_sing_sigma";
		break;
	case O_neg_ion:
		return "O_neg_ion";
		break;
	case O2_lexc:
		return "O2_lexc";
		break;
	case O2_rot:
		return "O2_rot";
		break;
	case O2_vib1:
		return "O2_vib1";
		break;
	case O2_vib2:
		return "O2_vib2";
		break;
	case O2_vib3:
		return "O2_vib3";
		break;
	case O2_vib4:
		return "O2_vib4";
		break;
	case O_1D:
		return "O_1D";
		break;
	case O_1S:
		return "O_1S";
		break;

	case Cl2_ion://20130604,Huang
		return "Cl2_ion";
		break;
	case Cl2:
		return "Cl2";
		break;
	case Cl:
		return "Cl";
		break;
	case Cl_ion:
		return "Cl_ion";
		break;
	case Cl_neg_ion:
		return "Cl_neg_ion";
		break;

	case NO_SPECIES:
		return "unknown type";
	default:
		break;
	}
	return "";
}

ReactionSpeciesType ReactionSpeciesTypeName(ostring name)
{
	if (name == "E-") return E;
	else if (name == "Ar") return Ar;
	else if (name == "Ar4p") return Ar4p; // add MAL 11/15/10
	else if (name == "Arm") return Arm; // add MAL 11/17/10
	else if (name == "Arr") return Arr; // add MAL 11/17/10
	else if (name == "Unit") return Unit; // add MAL 11/16/10
	// else if (name == "Ar+") return Ar_ion;
	else if (name == "Ar_ion") return Ar_ion; // MAL200226
	else if (name == "Xe") return Xe;
	else if (name == "Xe+") return Xe_ion;
	else if (name == "He") return He;
	else if (name == "Hep3") return Hep3; // add MAL 11/15/10 (SAS July 2 2014)
	else if (name == "Hep1") return Hep1; // add MAL 11/15/10 (SAS July 2 2014)
	else if (name == "Hem3") return Hem3; // add MAL 11/17/10 (SAS July 2 2014)
	else if (name == "Hem1") return Hem1; // add MAL 11/17/10 (SAS July 2 2014)
	else if (name == "He+") return He_ion;
	else if (name == "H") return H;
	else if (name == "H2") return H2;
	else if (name == "H+") return H_ion;
	else if (name == "CH4") return CH4;
	else if (name == "CH4+") return CH4_ion;
	else if (name == "CH3") return CH3;
	else if (name == "CH3+") return CH3_ion;
	else if (name == "CH2") return CH2;
	else if (name == "CH2+") return CH2_ion;
	else if (name == "CH") return CH;
	else if (name == "CH+") return CH_ion;
	else if (name == "O2") return O2;
	else if (name == "O") return O;
	else if (name == "O2_ion") return O2_ion; // "O2+" -> "O2_ion", MAL, 4/14/09
	else if (name == "O_neg_ion") return O_neg_ion;
	else if (name == "O_ion") return O_ion;
	else if (name == "O2_lexc") return O2_lexc;
	else if (name == "O2_sing_delta") return O2_sing_delta;
	else if (name == "O2_sing_sigma") return O2_sing_sigma;
	else if (name == "O_1D") return O_1D;
	else if (name == "O2_1S") return O_1S;
	else if (name == "O2_rot") return O2_rot;
	else if (name == "O2_vib1") return O2_vib1;
	else if (name == "O2_vib2") return O2_vib2;
	else if (name == "O2_vib3") return O2_vib3;
	else if (name == "O2_vib4") return O2_vib4;

	else if (name == "Cl2") return Cl2; //20130604,Huang
	else if (name == "Cl") return Cl;
	else if (name == "Cl2_ion") return Cl2_ion;
	else if (name == "Cl_neg_ion") return Cl_neg_ion;
	else if (name == "Cl_ion") return Cl_ion;

	else
	{
		fprintf(stderr, "%s: unknown Reaction Species Type\n", name());
		return NO_SPECIES;
	}
}

bool is_neutral(ReactionSpeciesType rst)
{
	return (rst <= Air && rst >= Ar);
}

bool is_ion(ReactionSpeciesType rst)
{
	return (rst <= Air_ion && rst >= Ar_ion);
}

bool is_before(ReactionSpeciesType rstone, ReactionSpeciesType rsttwo)
{
	return (rstone < rsttwo);
}

bool is_after(ReactionSpeciesType rstone, ReactionSpeciesType rsttwo)
{
	return (rstone > rsttwo);
}

bool is_thesame(ReactionSpeciesType rstone, ReactionSpeciesType rsttwo) // for like-particle collisions, MAL 11/23/09
{
	return (rstone == rsttwo);
}

bool is_neutral(const Species *const rst)
{
	return (is_neutral(rst->get_reactiontype()));
}

bool is_electron(const Species *const rst)
{
	return (rst->get_reactiontype() == E);
}

bool is_ion(const Species *const rst)
{
	return (is_ion(rst->get_reactiontype()));
}

bool is_before(const Species *const rstone, const Species *const rsttwo) //for ion-ion and neutral-neutral collisions, MAL 4/17/09
{
	return (is_before(rstone->get_reactiontype(), rsttwo->get_reactiontype()));
}

bool is_after(const Species *const rstone, const Species *const rsttwo) //MAL 4/17/09
{
	return (is_after(rstone->get_reactiontype(), rsttwo->get_reactiontype()));
}

bool is_thesame(const Species *const rstone, const Species *const rsttwo) //for like-particle collisions, MAL 11/23/09
{
	return (is_thesame(rstone->get_reactiontype(), rsttwo->get_reactiontype()));
}
