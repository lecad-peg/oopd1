// ReactionParse class
// by HyunChul Kim, 2006
// JTG and MAL melded file, 10/12/09
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "fields/fields.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "reactions/reaction.hpp"
#include "xsections/xsection.hpp"
#include "xsections/xsectionsamples.hpp"
#include "utils/PairArray.hpp"
#include "utils/arbitrary_func.hpp"
#include "xsections/xsection_func.hpp"
#include "reactions/reactionSubParse.hpp"
#include "MCC/mcc.hpp"
#include "reactions/reactionParse.hpp"
#include "atomic_properties/gas.hpp"
#include "atomic_properties/argon_gas.hpp"
#include "atomic_properties/helium_gas.hpp"
#include "atomic_properties/hydrogen_gas.hpp"
#include "atomic_properties/hydrocarbons_gas.hpp"
#include "atomic_properties/oxygen_gas.hpp"
#include "atomic_properties/chlorine_gas.hpp" // 20130604,Huang
#include "atomic_properties/xenon_gas.hpp"
#include "atomic_properties/nobelgasxterms_gas.hpp"  // Added by JTG November 21 2009

ReactionParse::ReactionParse (FILE *fp,
		oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables,
		SpeciesList *slist,
		Scalar _dt,  Fields *tf)
{
	specieslist = slist;
	dt = _dt;
	thefield = tf;
	init_parse(fp, float_variables, int_variables);
}

ReactionParse::~ReactionParse()
{
	reactionlist.deleteAll();
	reactionparselist.deleteAll();
	reactiongrouplist.deleteAll();
	if (gas) delete gas;
}

void ReactionParse::setdefaults()
{
	classname = "Reactions";
	set_msg(classname);

	sec_e_name = "";
	sec_i_name = "";
	sec_n_name = ""; // add MAL 11/15/10
	sec_Ar4p_name = ""; // add MAL 11/15/10
	sec_Arm_name = ""; // add MAL 11/17/10
	sec_Arr_name = ""; // add MAL 11/17/10
	sec_Ar_name = ""; // add MAL 11/16/10
	sec_Ar_ion_name = ""; // MAL191210
	sec_Unit_name = ""; // MAL191211

	sec_Hep3_name = ""; // add SAS July 1 2014
	sec_Hep1_name = ""; // add SAS July 1 2014
	sec_Hem3_name = ""; // add SAS July 1 2014
	sec_Hem1_name = ""; // add SAS July 1 201
	sec_He_name = ""; // add SAS July 1 2014
	sec_H_name = "";
	sec_H_ion_name = "";
	sec_H2_name = "";
	sec_C_name = "";
	sec_C_ion_name = "";
	sec_CH_name = "";
	sec_CH_ion_name = "";
	sec_CH2_name = "";
	sec_CH2_ion_name = "";
	sec_CH3_name = "";
	sec_CH3_ion_name = "";

	sec_O2_rot_name = "";
	sec_O2_vib1_name = "";
	sec_O2_vib2_name = "";
	sec_O2_vib3_name = "";
	sec_O2_vib4_name = "";
	sec_O2_lexc_name = "";
	sec_O2_sing_delta_name = "";
	sec_O2_sing_sigma_name = "";
	sec_O_name = "";
	sec_O_neg_name = "";
	sec_O2_name = "";
	sec_O2_ion_name = "";
	sec_O_ion_name = ""; // added by JTG June 23  2009
	sec_O_1D_name = ""; // added by JTG July 20  2009
	sec_O_1S_name = ""; // added by JTG July 20  2009
	sec_Ar_ion_name = ""; // added MAL, 11/20/11
	sec_Xe_name = ""; // added MAL, 11/20/11
	sec_He_ion_name = ""; // added SAS July 1 2014
	sec_Xe_ion_name = ""; // added MAL, 11/20/11
	sec_Cl2_name = ""; // 20130604,Huang
	sec_Cl2_ion_name = "";  
	sec_Cl_name = ""; 
	sec_Cl_ion_name = "";  
	sec_Cl_neg_ion_name = "";

	sec_O2_rot = NULL;
	sec_O2_vib1 = NULL;
	sec_O2_vib2 = NULL;
	sec_O2_vib3 = NULL;
	sec_O2_vib4 = NULL;
	sec_O2_lexc = NULL;
	sec_O2_sing_delta = NULL;
	sec_O2_sing_sigma = NULL;
	sec_O = NULL;
	sec_O_neg = NULL;
	sec_O2 = NULL;
	sec_O2_ion = NULL;
	sec_O_ion = NULL; // added by JTG June 23  2009
	sec_O_1D = NULL; // added by JTG July 20  2009
	sec_O_1S = NULL; // added by JTG July 20  2009

	sec_Cl = NULL; //20130604,Huang
	sec_Cl_ion = NULL;  
	sec_Cl2 = NULL;  
	sec_Cl2_ion = NULL;   
	sec_Cl_neg_ion = NULL;   

	sec_Xe = NULL;  //  Added by JTG November 21 2009
	sec_Xe_ion = NULL;
  
	sec_Ar = NULL;
	sec_Ar4p = NULL; // add MAL 11/15/10
	sec_Arm = NULL; // add MAL 11/17/10
	sec_Arr = NULL; // add MAL 11/17/10
	sec_Ar_ion = NULL; // note this was already added, MAL191210
	sec_Unit = NULL; // add MAL191211
  
	sec_He = NULL;
	sec_Hep3 = NULL; // add SAS July 1 2014
	sec_Hep1 = NULL; // add SAS July 1 2014
	sec_Hem3 = NULL; // add SAS July 1 2014
	sec_Hem1 = NULL; // add SAS July 1 2014
	sec_He_ion = NULL;  // add JTG January 26 2015
	sec_e = NULL;
	sec_i = NULL;
	sec_n = NULL; // add MAL 11/15/10
	sec_H = NULL;
	sec_H_ion = NULL;
	sec_H2 = NULL;
	sec_C = NULL;
	sec_C_ion = NULL;
	sec_CH = NULL;
	sec_CH_ion = NULL;
	sec_CH = NULL;
	sec_CH2 = NULL;
	sec_CH2_ion = NULL;
	sec_CH3 = NULL;
	sec_CH3_ion = NULL;

	gasname = "";
	gas = NULL;

	no_default_secondary_species_flag = false;
}

void ReactionParse::setparamgroup()
{
	pg.add_string("gas", &gasname, "Gas which reactions are included");
	pg.add_bflag("no_default_secondary_species", &no_default_secondary_species_flag, true);
	pg.add_string("sec_e", &sec_e_name, "Species name for creation of secondary species");
	pg.add_string("sec_i", &sec_i_name, "Species name for creation of secondary species");
	pg.add_string("sec_Ar", &sec_Ar_name, "Species name for creation of secondary species"); // MAL191210
	pg.add_string("sec_Arm", &sec_Arm_name, "Species name for creation of secondary species"); // MAL191210
	pg.add_string("sec_Arr", &sec_Arr_name, "Species name for creation of secondary species"); // MAL191210
	pg.add_string("sec_Ar4p", &sec_Ar4p_name, "Species name for creation of secondary species"); // MAL191210
	pg.add_string("sec_Ar_ion", &sec_Ar_ion_name, "Species name for creation of secondary species"); // MAL191210
	pg.add_string("sec_Unit", &sec_Unit_name, "Species name for creation of secondary species"); // MAL191211
	pg.add_string("sec_Xe", &sec_Xe_name, "Species name for creation of secondary species"); // MAL191211
	pg.add_string("sec_Xe_ion", &sec_Xe_ion_name, "Species name for creation of secondary species"); // MAL191211
	pg.add_string("sec_H", &sec_H_name, "Species name for creation of secondary species"); // MAL191211
	pg.add_string("sec_H_ion", &sec_H_ion_name, "Species name for creation of secondary species");
	pg.add_string("sec_H2", &sec_H2_name, "Species name for creation of secondary species");
	pg.add_string("sec_C", &sec_C_name, "Species name for creation of secondary species");
	pg.add_string("sec_C_ion", &sec_C_ion_name, "Species name for creation of secondary species");
	pg.add_string("sec_CH", &sec_CH_name, "Species name for creation of secondary species");
	pg.add_string("sec_CH_ion", &sec_CH_ion_name, "Species name for creation of secondary species");
	pg.add_string("sec_CH2", &sec_CH2_name, "Species name for creation of secondary species");
	pg.add_string("sec_CH2_ion", &sec_CH2_ion_name, "Species name for creation of secondary species");
	pg.add_string("sec_CH3", &sec_CH3_name, "Species name for creation of secondary species");
	pg.add_string("sec_CH3_ion", &sec_CH3_ion_name, "Species name for creation of secondary species");
	pg.add_string("sec_He_ion", &sec_He_ion_name, "Species name for creation of secondary species");
	pg.add_string("sec_Cl", &sec_Cl_name, "Species name for creation of secondary species");//20130604,Huang
	pg.add_string("sec_Cl_ion", &sec_Cl_ion_name, "Species name for creation of secondary species");
	pg.add_string("sec_Cl2", &sec_Cl2_name, "Species name for creation of secondary species");
	pg.add_string("sec_Cl2_ion", &sec_Cl2_ion_name, "Species name for creation of secondary species");
	pg.add_string("sec_Cl_neg_ion", &sec_Cl_neg_ion_name, "Species name for creation of secondary species");
	pg.add_string("sec_O2_rot", &sec_O2_rot_name, "Species name for creation of secondary species");
	pg.add_string("sec_O2_vib1", &sec_O2_vib1_name, "Species name for creation of secondary species");
	pg.add_string("sec_O2_vib2", &sec_O2_vib2_name, "Species name for creation of secondary species");
	pg.add_string("sec_O2_vib3", &sec_O2_vib3_name, "Species name for creation of secondary species");
	pg.add_string("sec_O2_vib4", &sec_O2_vib4_name, "Species name for creation of secondary species");
	pg.add_string("sec_O2_lexc", &sec_O2_lexc_name, "Species name for creation of secondary species");
	pg.add_string("sec_O2_sing_delta", &sec_O2_sing_delta_name, "Species name for creation of secondary species");
	pg.add_string("sec_O2_sigma_delta", &sec_O2_sing_sigma_name, "Species name for creation of secondary species");
	pg.add_string("sec_O", &sec_O_name, "Species name for creation of secondary species");
	pg.add_string("sec_O_neg", &sec_O_neg_name, "Species name for creation of secondary species");
	pg.add_string("sec_O2", &sec_O2_name, "Species name for creation of secondary species");
	pg.add_string("sec_O2_ion", &sec_O2_ion_name, "Species name for creation of secondary species");
	pg.add_string("sec_O_ion", &sec_O_ion_name, "Species name for creation of secondary species");  // added by JTG June 23  2009
	pg.add_string("sec_O_1D", &sec_O_1D_name, "Species name for creation of secondary species");  // added by JTG July 20  2009
	pg.add_string("sec_O_1S", &sec_O_1S_name, "Species name for creation of secondary species");  // added by JTG July 20  2009
	//  pg.add_string("sec_Xe", &sec_Xe_name, "Species name for creation of secondary species");  // added by JTG November 19  2009
	// pg.add_fparameter("np2c_product1", &np2c_product1);
	//  pg.add_fparameter("np2c_product2", &np2c_product2);
	add_child_name("Reaction");
}

void ReactionParse::check()
{
}

void ReactionParse::init()
{
	species_iter.restart(*specieslist);
	if (sec_e_name != "")
		sec_e = Species::get_species_named_with(species_iter, sec_e_name);
	if (sec_i_name != "")
		sec_i = Species::get_species_named_with(species_iter, sec_i_name);
	GasType gastype = GasTypeName(gasname);
	switch (gastype)
	{
	case CH4_GAS:
		if (sec_CH3_ion_name != "")
			sec_CH3_ion = Species::get_species_named_with(species_iter, sec_CH3_ion_name);
		if (sec_CH2_ion_name != "")
			sec_CH2_ion = Species::get_species_named_with(species_iter, sec_CH2_ion_name);
		if (sec_CH3_name != "")
			sec_CH3 = Species::get_species_named_with(species_iter, sec_CH3_ion_name);
		if (sec_CH2_name != "")
			sec_CH2 = Species::get_species_named_with(species_iter, sec_CH2_name);
		if (sec_CH_name != "")
			sec_CH = Species::get_species_named_with(species_iter, sec_CH_name);
		if (sec_H_name != "")
			sec_H = Species::get_species_named_with(species_iter, sec_H_name);
		if (sec_H2_name != "")
			sec_H2 = Species::get_species_named_with(species_iter, sec_H2_name);
		if (sec_H_ion_name != "")
			sec_H_ion = Species::get_species_named_with(species_iter, sec_H_ion_name);
		gas = new CH4_Gas(species_iter, sec_e, sec_i, sec_H, sec_H_ion, sec_H2, sec_CH, sec_CH2, sec_CH2_ion, sec_CH3, sec_CH3_ion, no_default_secondary_species_flag);
		break;
	case O2_GAS:
		if (sec_O2_rot_name != "")
			sec_O2_rot = Species::get_species_named_with(species_iter, sec_O2_rot_name);
		if (sec_O2_vib1_name != "")
			sec_O2_vib1 = Species::get_species_named_with(species_iter, sec_O2_vib1_name);
		if (sec_O2_vib2_name != "")
			sec_O2_vib2 = Species::get_species_named_with(species_iter, sec_O2_vib2_name);
		if (sec_O2_vib3_name != "")
			sec_O2_vib3 = Species::get_species_named_with(species_iter, sec_O2_vib3_name);
		if (sec_O2_vib4_name != "")
			sec_O2_vib4 = Species::get_species_named_with(species_iter, sec_O2_vib4_name);
		if (sec_O2_lexc_name != "")
			sec_O2_lexc = Species::get_species_named_with(species_iter, sec_O2_lexc_name);
		if (sec_O2_sing_delta_name != "")
			sec_O2_sing_delta = Species::get_species_named_with(species_iter, sec_O2_sing_delta_name);
		if (sec_O2_sing_sigma_name != "")
			sec_O2_sing_sigma = Species::get_species_named_with(species_iter, sec_O2_sing_sigma_name);
		if (sec_O_name != "")
			sec_O = Species::get_species_named_with(species_iter, sec_O_name);
		if (sec_O_neg_name != "")
			sec_O_neg = Species::get_species_named_with(species_iter, sec_O_neg_name);
		if (sec_O2_name != "")
			sec_O2 = Species::get_species_named_with(species_iter, sec_O2_name);
		if (sec_O2_ion_name != "")
			sec_O2_ion = Species::get_species_named_with(species_iter, sec_O2_ion_name);
		if (sec_O_ion_name != "")
			sec_O_ion = Species::get_species_named_with(species_iter, sec_O_ion_name);  // added by JTG June 23  2009
		if (sec_O_1D_name != "")
			sec_O_1D = Species::get_species_named_with(species_iter, sec_O_1D_name);  // added by JTG July 20  2009
		if (sec_O_1S_name != "")
			sec_O_1S = Species::get_species_named_with(species_iter, sec_O_1S_name);  // added by JTG July 20  2009, also added below
		gas = new O2_Gas(species_iter, sec_e, sec_i, sec_O2_rot, sec_O2_vib1, sec_O2_vib2, sec_O2_vib3, sec_O2_vib4, sec_O2_lexc, sec_O2_sing_delta, sec_O2_sing_sigma, sec_O, sec_O_neg, sec_O2, sec_O2_ion, sec_O_ion, sec_O_1D, sec_O_1S,  no_default_secondary_species_flag);
		break;

	case Cl2_GAS:  //20130604,Huang
		if (sec_Cl_name != "")
			sec_Cl = Species::get_species_named_with(species_iter, sec_Cl_name);
		if (sec_Cl_neg_ion_name != "")
			sec_Cl_neg_ion = Species::get_species_named_with(species_iter, sec_Cl_neg_ion_name);
		if (sec_Cl2_name != "")
			sec_Cl2 = Species::get_species_named_with(species_iter, sec_Cl2_name);
		if (sec_Cl2_ion_name != "")
			sec_Cl2_ion = Species::get_species_named_with(species_iter, sec_Cl2_ion_name);
		if (sec_Cl_ion_name != "")  
			sec_Cl_ion = Species::get_species_named_with(species_iter, sec_Cl_ion_name);  
		gas = new Chlorine_Gas(species_iter, sec_e, sec_i, sec_Cl, sec_Cl_neg_ion, sec_Cl2, sec_Cl2_ion, sec_Cl_ion,   no_default_secondary_species_flag);
		break;
	case CH3_GAS:
		if (sec_H_name != "")
			sec_H = Species::get_species_named_with(species_iter, sec_H_name);
		if (sec_H_ion_name != "")
			sec_H_ion = Species::get_species_named_with(species_iter, sec_H_ion_name);
		if (sec_H2_name != "")
			sec_H2 = Species::get_species_named_with(species_iter, sec_H2_name);
		if (sec_C_name != "")
			sec_C = Species::get_species_named_with(species_iter, sec_C_name);
		if (sec_CH_name != "")
			sec_CH = Species::get_species_named_with(species_iter, sec_CH_name);
		if (sec_CH_ion_name != "")
			sec_CH_ion = Species::get_species_named_with(species_iter, sec_CH_ion_name);
		if (sec_CH2_name != "")
			sec_CH2 = Species::get_species_named_with(species_iter, sec_CH2_name);
		if (sec_CH2_ion_name != "")
			sec_CH2_ion = Species::get_species_named_with(species_iter, sec_CH2_ion_name);
		gas = new CH3_Gas(species_iter, sec_e, sec_i, sec_H, sec_H_ion, sec_H2, sec_C, sec_CH, sec_CH_ion, sec_CH2, sec_CH2_ion, no_default_secondary_species_flag);
		break;
	case CH2_GAS:
		if (sec_H_name != "")
			sec_H = Species::get_species_named_with(species_iter, sec_H_name);
		if (sec_H_ion_name != "")
			sec_H_ion = Species::get_species_named_with(species_iter, sec_H_ion_name);
		if (sec_H2_name != "")
			sec_H2 = Species::get_species_named_with(species_iter, sec_H2_name);
		if (sec_C_name != "")
			sec_C = Species::get_species_named_with(species_iter, sec_C_name);
		if (sec_CH_name != "")
			sec_CH = Species::get_species_named_with(species_iter, sec_CH_name);
		if (sec_CH_ion_name != "")
			sec_CH_ion = Species::get_species_named_with(species_iter, sec_CH_ion_name);
		gas = new CH2_Gas(species_iter, sec_e, sec_i, sec_H, sec_H_ion, sec_H2, sec_C, sec_CH, sec_CH_ion, no_default_secondary_species_flag);
		break;
	case CH_GAS:
		if (sec_H_name != "")
			sec_H = Species::get_species_named_with(species_iter, sec_H_name);
		if (sec_H_ion_name != "")
			sec_H_ion = Species::get_species_named_with(species_iter, sec_H_ion_name);
		if (sec_H2_name != "")
			sec_H2 = Species::get_species_named_with(species_iter, sec_H2_name);
		if (sec_C_name != "")
			sec_C = Species::get_species_named_with(species_iter, sec_C_name);
		if (sec_C_ion_name != "")
			sec_C_ion = Species::get_species_named_with(species_iter, sec_C_ion_name);
		gas = new CH_Gas(species_iter, sec_e, sec_i, sec_H, sec_H_ion, sec_H2, sec_C, sec_C_ion, no_default_secondary_species_flag);
		break;
	case CARBON_GAS:
		if (sec_H_name != "")
			sec_H = Species::get_species_named_with(species_iter, sec_H_name);
		gas = new Carbon_Gas(species_iter, sec_e, sec_i, sec_H, no_default_secondary_species_flag);
		break;
	case ARGON_GAS:
		if (sec_Ar4p_name != "") // add MAL 11/15/10
			sec_Ar4p = Species::get_species_named_with(species_iter, sec_Ar4p_name);
		if (sec_Arm_name != "") // add MAL 11/17/10
			sec_Arm = Species::get_species_named_with(species_iter, sec_Arm_name);
		if (sec_Arr_name != "") // add MAL 11/17/10
			sec_Arr = Species::get_species_named_with(species_iter, sec_Arr_name);
		if (sec_Ar_name != "") // add MAL 11/16/10
			sec_Ar = Species::get_species_named_with(species_iter, sec_Ar_name);
		if (sec_Ar_ion_name != "") // add MAL191210
			sec_Ar_ion = Species::get_species_named_with(species_iter, sec_Ar_ion_name);
		if (sec_Unit_name != "") // add MAL191210
			sec_Unit = Species::get_species_named_with(species_iter, sec_Unit_name);
		gas = new Argon_Gas(species_iter, sec_e, sec_i, sec_Ar_ion, sec_Ar, sec_Arm, sec_Arr, sec_Ar4p, sec_Unit, no_default_secondary_species_flag); // add sec_Ar_ion and sec_Unit to the list, MAL191210, MAL191211
		break;
	case XENON_GAS:
		gas = new Xenon_Gas(species_iter, sec_e, sec_i, sec_Xe, sec_Xe_ion, no_default_secondary_species_flag);
		break;
	case HELIUM_GAS:
		if (sec_He_ion_name != "") // add JTG January 26 2015
			sec_He_ion = Species::get_species_named_with(species_iter, sec_He_ion_name);
		if (sec_Hep3_name != "") // add SAS July 1 2014
			sec_Hep1 = Species::get_species_named_with(species_iter, sec_Hep3_name);
		if (sec_Hep1_name != "") // add SAS July 1 2014
			sec_Hep1 = Species::get_species_named_with(species_iter, sec_Hep1_name);
		if (sec_Hem3_name != "") // add SAS July 1 2014
			sec_Hem3 = Species::get_species_named_with(species_iter, sec_Hem3_name);
		if (sec_Hem1_name != "") // add SAS July 1 2014
			sec_Hem1 = Species::get_species_named_with(species_iter, sec_Hem1_name);
		if (sec_He_name != "") // add SAS July 1 2014
			sec_He = Species::get_species_named_with(species_iter, sec_He_name);
		// add sec_He_ion and sec_Unit to Helium_Gas constructor, MAL191211
		gas = new Helium_Gas(species_iter, sec_e, sec_i, sec_He_ion, sec_Unit, sec_He, sec_Hem3, sec_Hem1, sec_Hep3, sec_Hep1, no_default_secondary_species_flag);
		break;
	case HYDROGEN_GAS:
		gas = new Hydrogen_Gas(species_iter, sec_e, sec_i, no_default_secondary_species_flag);
		break;
	case NOBELGASXTERMS_GAS:  // Added by JTG November 21 2009
		// uncomment if statements and add sec_Xe, sec_Xe_ion, sec_Ar, sec_Ar_ion to gas statement, MAL 11/20/11
		if (sec_Xe_name != "")
			sec_Xe = Species::get_species_named_with(species_iter, sec_Xe_name);
		if (sec_Xe_ion_name != "")
			sec_Xe_ion = Species::get_species_named_with(species_iter, sec_Xe_ion_name);
		if (sec_Ar_name != "")
			sec_Ar = Species::get_species_named_with(species_iter, sec_Ar_name);
		if (sec_Ar_ion_name != "")
			sec_Ar_ion = Species::get_species_named_with(species_iter, sec_Ar_ion_name);
		//gas = new Nobelgasxterms_Gas(species_iter, sec_e, sec_i, no_default_secondary_species_flag);
		gas = new Nobelgasxterms_Gas(species_iter, sec_e, sec_i, sec_He, sec_Hem3, sec_Hem1, sec_Ar_ion, sec_Xe, sec_Xe_ion, sec_Ar, sec_He_ion, no_default_secondary_species_flag); // add SAS July 1 2014
		break;
	case FAKE_GAS:
		break;
	default:
		terminate_run("Unsupported gasname '%s' !", gasname());
		break;
	}

	ReactionList default_reaction;
	if (gas)
	{
		//  gas->print_reactionlist();
		default_reaction.add(gas->get_reactionlist());
	}

	if (debug())
	{
		printf("\n* Default Reactions from %s", gasname());
		default_reaction.print_reactionlist(debug());
	}

	parse_reactionlist.merge_reactionlist(default_reaction, reactionlist);

	printf("\n* Resultant Reactions regarding %s", gasname());
	// print_reactionlist(reactionlist, debug());
	reactionlist.print_reactionlist(debug());

	ReactionList resultant_reactionlist, reactionlist_tmp;
	// mindgame: duplicate reactionlist to resultant_reactionlist;
	resultant_reactionlist.add(reactionlist);
	int num = resultant_reactionlist.num();
	for (int i=0; i<num; i++)
	{
		if (resultant_reactionlist.data(i))
		{
			reactionlist_tmp=resultant_reactionlist.find_n_erase(resultant_reactionlist.data(i)->get_reactants());
			ReactionGroup *reaction_group=new ReactionGroup(reactionlist_tmp, specieslist, dt, thefield);
			reactiongrouplist.add(reaction_group);
		}
	}
}

Parse *ReactionParse::special(ostring s)
{
	if (s == "Reaction")
	{
		ReactionSubParse *reactionparse = new ReactionSubParse(f, pg.float_variables, pg.int_variables, specieslist, &gasname);
		parse_reactionlist.add(reactionparse->get_reactionlist());
		reactionparselist.add(reactionparse);
		return reactionparse;
	}
	return 0;
}
