// Sample file for Loading part
//
// Original by HyunChul Kim, 2005

#include <stdio.h>
#include <math.h>
#include "utils/ovector.hpp"
#include "distributions/dist_load.hpp"
#include "utils/message.hpp"
#include "distributions/dist_xv.hpp"
#include "distributions/dist_x.hpp"
#include "distributions/dist_v.hpp"

int main()
{
  Scalar x0=0, x1=1;
  LoadType xload_type = UNIFORM;
  Grid *thegrid=new Grid();
  XDistribution *xdist;
  xdist=new XDistribution(thegrid,x0,x1,xload_type);

  Scalar vt=1, v0=1;
  LoadType vload_type = RANDOM;
  Vector3 vcl=Vector3(0.,0.,0.);
  Vector3 vcu=Vector3(0.,0.,0.);
  Species *species;
  species=new Species();
  VDistribution *vdist;
  vdist=new VDistribution(species,vt,v0,false,vcl,vcu,vload_type);

  delete species;
  delete vdist;
  delete xdist;
  delete thegrid;
  return 0;
}
