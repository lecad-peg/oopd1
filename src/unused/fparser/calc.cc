#include <stdio.h>
//#include "ostring.h"
#include "fparser.cc"

FunctionParser * get_function(ostring A)
{
  return NULL;
}

main()
{
  char buf[266];
  ostring rhs, vars;
  double val;
  FunctionParser fp;
  
  printf("\nEnter the Right hand side of a function: ");
  gets(buf);
  rhs = buf;

  printf("\nEnter the variables: ");
  gets(buf);
  vars = buf;
 
  fp.Parse(rhs, vars);

  // works only for single valued functions
  while(1)
    {
      printf("\nEnter value: ");
      scanf("%lf", &val);
      printf("\nvalue = %g, ans = %g\n", val, fp.Eval(&val));
    }

}
