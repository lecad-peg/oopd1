// mindgame: this file is depreciated
#include <stdio.h>
#include <stdlib.h>
#include "main/pd1.hpp"
#include "reactions/reactiontype.hpp"
#include "collisionpkg.hpp"
#include "utils/functions.hpp"
#include "utils/ostring.hpp"
#include "utils/equation.hpp"
#include "xsections/xsection.hpp"
#include "xsections/xsectionsamples.hpp"   // Contains certain paterns to create cross - sections.
#include "utils/funcxsection.hpp"
#include "xsections/fakecross.hpp"
#include "main/oopiclist.hpp"

CollisionPkg::CollisionPkg(void)
{
  currentxsectionset = NULL;
  pkgname = "non specified collision package name";
}

CollisionPkg::~CollisionPkg()
{
  if(xsectionarray.size())
  {
    XsectionSet * xsectionsref;
    unsigned int i=0;
    while(i<xsectionarray.size())
      {
	xsectionsref = xsectionarray[i];
	while (++i <xsectionarray.size() && xsectionarray[i]== xsectionsref){}
	delete xsectionsref;
      }
  }
}

void CollisionPkg::add_collider(ReactionSpeciesType ct)
{
  colliders_types.push_back(ct);
  currentxsectionset = new XsectionSet;
  xsectionarray.push_back(currentxsectionset);
}

// Add cross sections
void CollisionPkg::add_xsection(Scalar (*f)(Scalar), Scalar _threshold, ReactionType _type)
{
  currentxsectionset->add_xsection (new FuncXsection(f, _threshold, _type));
}

void CollisionPkg::add_Xsection_type1(Scalar e0, Scalar e1, Scalar e2, Scalar sMax,
		      ReactionType _type)
{
  currentxsectionset->add_xsection (new Xsection_type1(e0, e1, e2, sMax, _type));
}

void CollisionPkg::add_Xsection_type2(Scalar _a, Scalar _b, ReactionType _type)
{
   currentxsectionset->add_xsection(new Xsection_type2(_a, _b,_type));
}

void CollisionPkg::add_fakeXsection(Scalar e0, Scalar e1, ReactionType _type)
{
  currentxsectionset->add_xsection (new fakeXsection(e0,e1, _type));
}

void CollisionPkg::add_fakiXsection(Scalar _a, ReactionType _type)
{
  currentxsectionset->add_xsection ( new fakiXsection(_a, _type));
}

// Printing
void CollisionPkg::print_xsections(Scalar *a, int n, FILE *fp)
{
  unsigned int j;
  int i, k;
    for(i=0;i<n;i++)
    {
      fprintf(fp, "%g", a[i]);
      for (j=0; j<xsectionarray.size() ; j++)
	{
	  const XsectionSet* thecxs = xsectionarray[j];
	  for(k=0; k < thecxs->get_numberofxsection(); k++)
	    {
	       const Xsection* thecx = thecxs->get_xsection(k);
	      fprintf(fp, "%g ", thecx->value(a[i]));
	    }
	}
      fprintf(fp, "\n");
    }
}

void CollisionPkg::print_xsections(Scalar Emin, Scalar Emax, int n, bool logscale)
{
  ostring fn = pkgname + ".dat";
  Scalar *f;
  FILE *fp = fopen(fn(), "w");

  if(logscale)
    {
      f = logspacing(Emin, Emax, n-1);
    }
  else
    {
      f = linearspacing(Emin, Emax, n-1);
    }

  print_xsections(f, n, fp);
  delete [] f;
  fclose(fp);
}

void CollisionPkg::print_xsections(int idx, Scalar *a, int n, FILE *fp)
{
  int i, k;
  XsectionSet *thecxs = xsectionarray[idx];
  for(i=0;i<n;i++)
    {
      fprintf(fp, "%g", a[i]);
      for(k=0; k<thecxs->get_numberofxsection()  ; k++)
	{
	  const Xsection *thecx = thecxs->get_xsection(k);
	  fprintf(fp, "%g ", thecx->value(a[i]));
	}
      fprintf(fp, "\n");
    }
}

void CollisionPkg::print_xsections(int idx, Scalar Emin, Scalar Emax, int n, bool logscale)
{
  ostring fn = pkgname + ".dat";
  Scalar *f;
  FILE *fp = fopen(fn(), "w");

  if(logscale)
    {
      f = logspacing(Emin, Emax, n-1);
    }
  else
    {
      f = linearspacing(Emin, Emax, n-1);
    }

  print_xsections(idx , f, n, fp);
  delete [] f;
  fclose(fp);
}
