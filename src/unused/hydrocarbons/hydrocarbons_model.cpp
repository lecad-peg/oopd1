#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "collisionpkg.hpp"
#include "xsections/xsection.hpp"
#include "reactions/reaction.hpp"
#include "MCC/mcc_new.hpp"
#include "hydrocarbons_model.hpp"

Hydrocarbons_model::Hydrocarbons_model(oopicList<Species>* slist, Species* sp, bool _useSecDefaultWeight, 
				       ostring sec_e, ostring sec_i, 
				       ostring sec_iminusH, ostring sec_iminusH2,
				       ostring sec_nminusH, ostring sec_nminusH2, ostring sec_nminusH3, 
				       ostring sec_H, ostring sec_H2, ostring sec_p)
  : MCC(slist, sp, _useSecDefaultWeight, sec_e, sec_i)

{
  sec_iminusHSpecies = sec_iminusH2Species = NULL;
  sec_nminusHSpecies = sec_nminusH2Species = sec_nminusH3Species = NULL;
  sec_HSpecies = sec_H2Species = sec_pSpecies = NULL;

  oopicListIter<Species> thei (*specieslist);
  for(thei.restart(); !thei.Done(); thei++)
    {
      ostring speciesname = thei()->get_name();
     
      if (speciesname == sec_iminusH)
	{
	  if ( useSecDefaultWeight && thei()->get_np2c() < 0.0 ) 
	    fprintf(stderr, "\nMCC: Warning. Found MCC Package, with 'UseSecDefaultWeight' flag and secondary species of non-initialized weight.\n");
	  else sec_iminusHSpecies = thei();
	}
      else if (speciesname == sec_iminusH2)
	{
	  if ( useSecDefaultWeight && thei()->get_np2c() < 0.0 ) 
	    fprintf(stderr, "\nMCC: Warning. Found MCC Package, with 'UseSecDefaultWeight' flag and secondary species of non-initialized weight.\n");
	  else sec_iminusH2Species = thei();
	}
      else if (speciesname == sec_nminusH)
	{
	  if ( useSecDefaultWeight && thei()->get_np2c() < 0.0 ) 
	    fprintf(stderr, "\nMCC: Warning. Found MCC Package, with 'UseSecDefaultWeight' flag and secondary species of non-initialized weight.\n");
	  else sec_nminusHSpecies = thei();
	}  
      else if (speciesname == sec_nminusH2)
	{
	  if ( useSecDefaultWeight && thei()->get_np2c() < 0.0 ) 
	    fprintf(stderr, "\nMCC: Warning. Found MCC Package, with 'UseSecDefaultWeight' flag and secondary species of non-initialized weight.\n");
	  else sec_nminusH2Species = thei();
	}  
       else if (speciesname == sec_nminusH3)
	{ 
	  if ( useSecDefaultWeight && thei()->get_np2c() < 0.0 ) 
	    fprintf(stderr, "\nMCC: Warning. Found MCC Package, with 'UseSecDefaultWeight' flag and secondary species of non-initialized weight.\n");
	  else sec_nminusH3Species = thei();
	}  
       else if (speciesname == sec_H )
	{
	  if ( useSecDefaultWeight && thei()->get_np2c() < 0.0 ) 
	    fprintf(stderr, "\nMCC: Warning. Found MCC Package, with 'UseSecDefaultWeight' flag and secondary species of non-initialized weight.\n");
	  else sec_HSpecies = thei();
	}  
       else if (speciesname == sec_H2 )
	{
	  if ( useSecDefaultWeight && thei()->get_np2c() < 0.0 ) 
	    fprintf(stderr, "\nMCC: Warning. Found MCC Package, with 'UseSecDefaultWeight' flag and secondary species of non-initialized weight.\n");
	  else sec_H2Species = thei();
	}
      else if (speciesname == sec_p )
	{
	  if ( useSecDefaultWeight && thei()->get_np2c() < 0.0 ) 
	    fprintf(stderr, "\nMCC: Warning. Found MCC Package, with 'UseSecDefaultWeight' flag and secondary species of non-initialized weight.\n");
	  else sec_pSpecies = thei();
	}
    }
}

void Hydrocarbons_model::dynamic(const Xsection& cx)
{
# ifdef DEBUGmcc
  totalnumberofcollisions++;
# endif

  switch(cx.get_type())
    {
      // --- Scattering processes:
    case E_ELASTIC:                 
      eElastic(cx);
      break;
    case P_ELASTIC: 
      pElastic(cx);
      break;
    case E_EXCITATION:
      eExcitation(cx);
      break;
    case E_DISSEXCITATION1:
      eDissExcitation1(cx);
      break;
    case E_DISSEXCITATION2:
      eDissExcitation2(cx);
      break;
    case E_DISSEXCITATION3:
      eDissExcitation3(cx);
      break;
    case E_DISSEXCITATION4:
      eDissExcitation4(cx);
      break;
    case E_DISSEXCITATION5:
      eDissExcitation5(cx);
      break;

      // --- Ionizing processes:
    case E_IONIZATION:
      eIonization(cx);
      break;
    case E_DISSIONIZATION:
      eDissIonization(cx);
      break;
   
      // --- Charge exchange processes:    
    case P_eEXCHANGE:
      pElectronExchange(cx);
      break;   
    case P_hEXCHANGE:
      pHydrogenExchange(cx);
      break;
    
      // --- Recombinations:
    case E_DISSRECOMBINATION1:
      eDissRecombination1(cx);
      break;
    case E_DISSRECOMBINATION2:
      eDissRecombination2(cx);
      break;
    case E_DISSRECOMBINATION3:
      eDissRecombination3(cx);
      break;
    default: 
      fprintf(stderr,"Unrecognized collision type!\n");
    }
}

/////////////////////////////
// Reaction dynamics       // 
/////////////////////////////
// Simple models are used: 
// non-relativistic processes, isotropic behaviors, ion/neutral velocity not modified by a single electron.

void Hydrocarbons_model::eElastic (const Xsection &) 
{
}

void Hydrocarbons_model::pElastic (const Xsection &) 
{
  
}

void Hydrocarbons_model::eExcitation (const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
}

void Hydrocarbons_model::Any_eExcitation (const Xsection& cx, Scalar targetmass) 
{
  //
  energy -= cx.get_threshold();
  v = eSpecies->get_speed2(energy); 
  v /= eSpecies->get_dxdt();
  
  // Isotropic scattering with the reduced energy.
  Scalar coschi = 1 - 2*frand();
  Scalar chi = acos(coschi);
  Scalar phi = TWOPI*frand();
  Scalar me = eSpecies -> get_m();
  v *= sqrt(1. - 2. * me * targetmass / (me + targetmass) / (me + targetmass) * (1. - coschi)); 
  ue = v*  newcoordiante(ue , chi, phi);  
}
void Hydrocarbons_model::Any_DissociationInTwo(Vector3 & target_velocity, Species* s1, 
					     Species* s2, Scalar m1, Scalar m2, Scalar ReleasedEnergy) 
{
  if (s1||s2)
    { 
      Scalar mu = m1*m2/(m1+m2);
      Scalar temp  = sqrt(2*ReleasedEnergy*mu*PROTON_CHARGE); 
      Scalar phi = frand()*TWOPI; // isotropic in the frame of the excited species
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi) ; 
      
      if (s1) 
	{ 
	  Vector3 newvel = target_velocity + ((temp/m1)*uProducts)/s1->get_dxdt(); 
	  add_particles(s1, newvel); // lab frame	  
	}
      if (s2)
	{ 
	  Vector3 newvel = target_velocity - ((temp/m2)*uProducts)/s2->get_dxdt();
	  add_particles(s2, newvel);	    
	}
    }
  target_velocity = DELETE_PARTICLE;
}

void Hydrocarbons_model::Any_DissociationInThree(Vector3 & target_velocity, Species* s1, Species* s2, 
						Species* s3, Scalar m1, Scalar m2, Scalar m3, Scalar ReleasedEnergy)
{
  if(s1||s2||s3)
    {
      Scalar mu_inv = 1/m1+ 1/m2 + 1/m3; //  mu_inv = 1/mu, mu is the reduced mass.
      Scalar temp  = sqrt(2*(ReleasedEnergy)*PROTON_CHARGE/mu_inv);  
      Scalar phi = frand()*TWOPI; // isotropic
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi);   
      phi = frand()*TWOPI; 
      
      if (s1)
	{ 
	  Vector3 newvel = target_velocity + ((temp/m1)*uProducts)/s1->get_dxdt();
	  add_particles(s1, newvel);	  
#ifdef DEBUGmcc  
	  fprintf(stderr, "\n New particle velocity: %g", newvel.magnitude(), ui.magnitude());
#endif
	}
      if (s2)
	{ 
	  Vector3 newvel = target_velocity + ((temp/m2)*newcoordiante(uProducts, (2/3)*PI, phi))
	    / s2->get_dxdt();  
	  add_particles(s2, newvel);	  
	}  
      if (s3)
	{ 
	  Vector3 newvel = target_velocity + (temp/m3)*newcoordiante(uProducts, (2/3)*PI, phi + PI)
	    /s3->get_dxdt();  
	  add_particles(s3, newvel);	  
	}
    }
  target_velocity = DELETE_PARTICLE;
}


void Hydrocarbons_model::eDissExcitation1(const Xsection& cx)  
{
  // Calculations valid in the  neutral frame (ue >> un => negligible modifications).   
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_nminusHSpecies, 
			sec_HSpecies, getm_nminusH(), getm_H(), get_DE1energy());  
}
void Hydrocarbons_model::eDissExcitation2(const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_nminusH2Species, 
			sec_H2Species, getm_nminusH2(), getm_H2(), get_DE2energy());
}
void Hydrocarbons_model::eDissExcitation3(const Xsection& cx)
{
  Any_eExcitation(cx, iSpecies->get_m());
  Any_DissociationInTwo(ui, sec_iminusHSpecies, 
			sec_HSpecies, getm_iminusH(), getm_H(), get_DE3energy());
}
void Hydrocarbons_model::eDissExcitation4(const Xsection& cx)
{
  Any_eExcitation(cx, iSpecies->get_m());
  Any_DissociationInTwo(ui, sec_iminusH2Species, 
			sec_H2Species, getm_iminusH2(), getm_H2(), get_DE4energy());
}
void Hydrocarbons_model::eDissExcitation5(const Xsection& cx)
{
  Any_eExcitation(cx, iSpecies->get_m());
  Any_DissociationInTwo(ui, sec_nminusHSpecies, 
			sec_pSpecies, getm_nminusH(), getm_p(), get_DE5energy());
}

void Hydrocarbons_model::eDissIonization(const Xsection& cx)
{
  // Calculations valid in the  neutral frame (ue >> un => negligible modifications).   
  
  // Approximate solution to allow collisions with energies <= average_energyloss 
  Scalar average_energyloss = get_DIeEnergyloss();
  if (average_energyloss >= energy) 
    { 
      ionization (energy);
      Any_DissociationInTwo(un, sec_iminusHSpecies, sec_HSpecies, 
			    getm_iminusH(), getm_H(), get_DIenergy()- (average_energyloss-energy));
      }
  else
    {
	ionization (average_energyloss);
	Any_DissociationInTwo(un, sec_iminusHSpecies, sec_HSpecies, 
			    getm_iminusH(), getm_H(), get_DIenergy());
    }
}

void Hydrocarbons_model::eDissRecombination1(const Xsection& cx) 
  // Calculations valid in center of center of mass frame ~ ion frame (ue >> ui => negligible modifications)
{
  Any_DissociationInTwo(ui, sec_nminusHSpecies, 
			sec_HSpecies, getm_nminusH(), getm_H(), get_DR1energy());
  ue = DELETE_PARTICLE;
}

void Hydrocarbons_model::eDissRecombination2(const Xsection& cx) 
{
  Any_DissociationInThree(ui, sec_nminusH2Species, 
			sec_HSpecies, sec_HSpecies, getm_nminusH2(), getm_H(), getm_H(), get_DR2energy()+energy);  
  ue = DELETE_PARTICLE;
}

void Hydrocarbons_model::eDissRecombination3(const Xsection& cx)
{
  Any_DissociationInThree(ui, sec_nminusH3Species, 
			sec_H2Species, sec_HSpecies, getm_nminusH3(), getm_H2(), getm_H(), get_DR3energy()+energy);  
  ue = DELETE_PARTICLE;
}

void Hydrocarbons_model::pElectronExchange(const Xsection & cx)
{ // Pb: exothermicities not taken into account...error?
  if (sec_HSpecies)
    { 
      add_particles(sec_HSpecies, ui);
    }
  if (sec_iSpecies)
    { 
      add_particles(sec_iSpecies, un);
    } 
  ui = DELETE_PARTICLE;
  un = DELETE_PARTICLE;
}

void Hydrocarbons_model::pHydrogenExchange(const Xsection& cx) 
  // Data could be found: isotropy here is very questionable.
{
  // Move to the center of mass frame, cmf. 
  Scalar mp = iSpecies->get_m(); 
  Scalar mn = nSpecies->get_m();
  Scalar mass_sum = mn+mp;
  un = (mn*un+mp*ui)/mass_sum; // cmf velocity
  energy = energy*mn/mass_sum; 
  Any_DissociationInTwo(un, sec_iminusHSpecies, 
			sec_H2Species, getm_iminusH(), getm_H2(), get_CXhenergy()+energy);  
  ui = DELETE_PARTICLE;
}
