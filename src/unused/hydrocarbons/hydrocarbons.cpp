#include "hydrocarbons.hpp"

void Hydrocarbons::init_secondaries(void) // need not to forget the species 
{
  sec_eSpecies = sec_iSpecies = NULL;
  sec_iminusHSpecies = sec_nminusHSpecies = sec_nminusH2Species = NULL;
  sec_nminusH3Species = sec_HSpecies = sec_H2Species = NULL;

  oopicListIter<Species> thei (*specieslist);
  for(thei.restart(); !thei.Done(); thei++)
    {
      Species_type s = thei()->get_stype();
      if (thei()->undergoes_mcc())
	{
	  for (int i =0; i< get_numberofsecondaries(); i++)
	    { 
	      if(s== get_secondary(i))
		if(thei()->get_np2c()<0.0) 
		  {
		    fprintf(stderr, "\n MCC:init_secondaries: Found secondary species, with a default weight non-initialized. Creations due to collisions won't be possible for this species. \n");
		  }
		else 
		  {
		    switch(get_secondarynature(i))
		      {
		      case sec_especies:
			sec_eSpecies = thei();
			thei()->set_3v_flag();
			break;
		      case sec_ispecies:
			sec_iSpecies = thei();
			thei()->set_3v_flag();
			break;
		      case sec_iminusHspecies:		  
			sec_iminusHSpecies = thei();
			thei()->set_3v_flag();
			break;	
		      case sec_Hspecies:
			sec_HSpecies = thei();
			thei()->set_3v_flag();
			break;
		      case sec_H2species:
			sec_H2Species = thei();
			thei()->set_3v_flag();
			break;
		      case sec_nminusHspecies:
			sec_nminusHSpecies = thei();
			thei()->set_3v_flag();
			break;
		      case sec_nminusH2species:
			sec_nminusH2Species = thei();
			thei()->set_3v_flag();
			break;
		      case sec_nminusH3species:
			sec_nminusH3Species = thei();
			break;
		      default:
			fprintf(stderr, "MCC::init(), warning, nature of secondary not recognized");   	   
		      }
		  }
	    }
	}
    }
}
/*
void Hydrocarbons::dynamic(const CrossX& cx)
{
# ifdef DEBUGmcc
  totalnumberofcollisions++;
# endif
}
*/

void Hydrocarbons::dynamic(const CrossX& cx) // All non-relativistic
{
# ifdef DEBUGmcc
  totalnumberofcollisions++;
# endif

  switch(cx.get_type())
    {
      // --- Scattering processes:
    case CrossX::E_ELASTIC:                 
      // fprintf(stderr,"elastic\n");
      eElastic(cx);
      break;
    case CrossX::E_EXCITATION:
      //fprintf(stderr,"excitaition\n");
      eExcitation(cx);
      break;
    case CrossX::E_DISS:
      eDissExcitation(cx);
      break;
    case CrossX::E_DBLEDISS:
      eDbleDissExcitation(cx);
      break;
    case CrossX::SAMEMASS_ELASTIC: 
      // fprintf(stderr, "ionelastic\n");
      samemassElastic(cx);
      break;
   
      // --- Ionizing processes:
    case CrossX::E_IONIZATION:
      // fprintf(stderr,"ionization\n");
      eIonization(cx);
      break;
    case CrossX::E_DISSIONIZ:
      DissIonization(cx);
      break;
    case CrossX::P_IONIZATION:
      pIonization(cx);
      break;
   
      // --- Charge exchange processes:    
    case CrossX::eEXCHANGE:
      ElectronExchange(cx);
      break;   
    case CrossX::P_hEXCHANGE:
      HydrogenExchange(cx);
      break;
    
      // --- Recombinations:
    case CrossX::E_DISSRECOMB:
      eDissRecombination(cx);
      break;
    case CrossX::E_DBLEDISSRECOMB:
      eDbleDissRecombination(cx);
      break;
    case CrossX::E_TPLEDISSRECOMB:
      eTpleDissRecombination(cx);
      break;
    default: 
      fprintf(stderr,"Unrecognized collision type!\n");
    }
}


void Hydrocarbons::eElastic (const CrossX &)
{

}

void Hydrocarbons::eExcitation (const CrossX& cx)  //  non-relativistic
{
  energy -= cx.get_threshold();
  v = eSpecies->get_speed2(energy); 
  v /= eSpecies->get_dxdt();
  
  // Then, isotropic scattering, following the algorithm for elastic scattering with the reduced energy.
  Scalar coschi = 1 - 2*frand();
  Scalar chi = acos(coschi);
  Scalar phi = TWOPI*frand();
  Scalar me = eSpecies -> get_m();
  Scalar mn = nSpecies -> get_m(); 
  v *= sqrt(1. - 2. * me * mn / (me + mn) / (me + mn) * (1. - coschi)); 
  ue = v*  newcoordiante(ue , chi, phi);  
}

void Hydrocarbons::eDissExcitation(const CrossX& cx)  //  non-relativistic
{
  eExcitation(cx);

  if (sec_nminusHSpecies|| sec_HSpecies)
    {
      Scalar mn = getm_nminusH();
      Scalar mH = getm_H();
      Scalar mu = mn*mH/(mn+mH);
      Scalar temp  = sqrt(2*get_DissEnergy()*mu*PROTON_CHARGE); 
      Scalar phi = frand()*TWOPI; 
      Scalar coschi = 1 - 2*frand(); // isotropic
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi) ; 
      
      if (sec_nminusHSpecies) 
	{ 
	  Vector3 newvel = un + ((temp/mn)*uProducts)/sec_nminusHSpecies->get_dxdt(); // We come back to the lab frame.  
	  add_particles(sec_nminusHSpecies, newvel);
	}
      
      if (sec_HSpecies)
	{ 
	  Vector3 newvel = un - ((temp/mH)*uProducts)/sec_HSpecies->get_dxdt();  
	  add_particles(sec_HSpecies, newvel);
	}
    }
  un = DELETE_PARTICLE;
}

void Hydrocarbons::eDbleDissExcitation(const CrossX& cx)
{
  eExcitation(cx);

  if (sec_nminusH2Species|| sec_H2Species)
    { 
      Scalar mn = getm_nminusH2();
      Scalar mH2 = getm_H2();
      Scalar mu = mn*mH2/(mn+mH2);
      Scalar temp  = sqrt(2*get_DbleDissEnergy()*mu*PROTON_CHARGE); 
      Scalar phi = frand()*TWOPI; // isotropic
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi) ; 
      
      if (sec_nminusH2Species) 
	{ 
	  Vector3 newvel = un + ((temp/mn)*uProducts)/sec_nminusH2Species->get_dxdt(); // We come back to the lab frame.  
	  add_particles(sec_nminusH2Species, newvel);	  
	}
      
      if (sec_H2Species)
	{ 
	  Vector3 newvel = un - ((temp/mH2)*uProducts)/sec_H2Species->get_dxdt();
	  add_particles(sec_H2Species, newvel);	    
	}
    }
  un = DELETE_PARTICLE;
}

void Hydrocarbons::DissIonization(const CrossX& cx)
{
  //We don't substract the velocity of the neutral: assumed the electron has a much higher velocity: energy, ue are valid.  
  Scalar thres = cx.get_threshold();
  ionization(thres);

  if (sec_iminusHSpecies || sec_HSpecies)
    {
      Scalar mi = getm_nminusH();
      Scalar mH = getm_H();
      Scalar mu = mi*mH/(mi+mH);
      Scalar temp  = sqrt(2*get_DissIzEnergy()*mu*PROTON_CHARGE);
      Scalar phi = frand()*TWOPI; // isotropic
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi) ; 

      if (sec_iminusHSpecies)
	{ 
	  Vector3 newvel = un + ((temp/mi)*uProducts)/sec_iminusHSpecies->get_dxdt(); // We come back to the lab frame.  	 
	  add_particles(sec_iminusHSpecies, newvel); // 8 ordres de magnitude de difference AIE!!
	}
      
      if (sec_HSpecies)
	{ 
	  Vector3 newvel = un - ((temp/mH)*uProducts)/sec_HSpecies->get_dxdt();  
	  add_particles(sec_HSpecies, newvel);	  
	}
    } 
  //Delete the neutral:
  un = DELETE_PARTICLE;
}

void Hydrocarbons::pIonization(const CrossX & cx)
{
  // To be completed
}

void Hydrocarbons::HydrogenExchange(const CrossX& cx)
{
  // To be completed //
}

void Hydrocarbons::eDissRecombination(const CrossX& cx) 
  // We have to move to the center of mass frame...it is almost the ion location: we consider, it is like removing ui.
  // We would have to take the energy in the center of mass frame 0.5 mu (v_rel)^2, it is almost "energy".
  // Check the relevance of those approximations !!!
{
  if (sec_nminusHSpecies || sec_HSpecies)
    {
      Scalar mn = getm_nminusH();
      Scalar mH = getm_H();
      Scalar mu= (mn*mH)/(mn+mH);
      Scalar temp  = sqrt(2*(get_DREnergy() + energy)*mu*PROTON_CHARGE);  
      // Warning: in the case of recombination, it is not the kinetic energy... it is only the total released energy. Has to be renamed, or corrected.    
      Scalar phi = frand()*TWOPI; // isotropic
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi) ;    
   
      if (sec_nminusHSpecies)
	{ 
	  Vector3 newvel = ui + ((temp/mn)*uProducts)/sec_nminusHSpecies->get_dxdt(); 
	  add_particles(sec_nminusHSpecies, newvel);
	}
      if (sec_HSpecies)
	{ 
	  Vector3 newvel = ui - ((temp/mH)*uProducts)/sec_HSpecies->get_dxdt();  
	  add_particles(sec_HSpecies, newvel);	  
	}
    }
  ue = DELETE_PARTICLE;
  ui = DELETE_PARTICLE;
}

void Hydrocarbons::eDbleDissRecombination(const CrossX& cx) //Similar comments as for recombination.
{
  if (sec_nminusH2Species || sec_HSpecies)
    {
      Scalar mn = getm_nminusH2(); 
      Scalar mH = getm_H(); 
      Scalar mu_inv = 2/mH + 1/mn;      
      Scalar temp  = sqrt(2*(get_DDREnergy() + energy)*PROTON_CHARGE /mu_inv);  
      // Warning: in the case of recombination, using the approximation that the released energy is fully kinetic is bad: there are a lot of excited states. There is the need to improve the model.
      Scalar phi = frand()*TWOPI; 
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi) ;   
          
      if (sec_nminusH2Species)
	{
	  Vector3 newvel = ui + ((temp/mn)*uProducts)/sec_nminusH2Species->get_dxdt();
	  add_particles(sec_nminusH2Species, newvel);	   
#ifdef DEBUGmcc  
	  fprintf(stderr, "\n New particle velocity: %g", newvel.magnitude(), ui.magnitude());
#endif
	}
      if (sec_HSpecies)
	{ 
	  phi = frand()*PI; // Only Pi since the created particles are the same: 2H.
	  Scalar temp_over_mH= temp/mH/sec_HSpecies->get_dxdt();
	  Vector3 newvel1 = ui + temp_over_mH*newcoordiante(uProducts, (2/3)*PI, phi);  
	  Vector3 newvel2 = ui + temp_over_mH*temp*newcoordiante(uProducts, (2/3)*PI, phi+PI);  
	  add_particles(sec_HSpecies, newvel1);	  
	  add_particles(sec_HSpecies, newvel2);	  
	}
    }
  ue = DELETE_PARTICLE;
  ui = DELETE_PARTICLE;
}

void Hydrocarbons::eTpleDissRecombination(const CrossX& cx)
{
  Scalar mn = getm_nminusH3();
  Scalar mH = getm_H();
  Scalar mH2 = getm_H2();
  Scalar mu_inv = 1/mn+ 1/mH + 1/mH2; // mu is the reduced mass, mu_inv = 1/mu
  Scalar temp  = sqrt(2*(get_TDREnergy() + energy)*PROTON_CHARGE/mu_inv);  // muE  
  // Warning: in the case of recombination, it is not the kinetic energy... it is only the total released energy. Has to be renamed, or corrected.
  Scalar phi = frand()*TWOPI; // isotropic
  Scalar coschi = 1 - 2*frand();
  Scalar sinchi =  sqrt(1 - pow(coschi,2));  
  Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi);   

  phi = frand()*TWOPI; 
  if (sec_nminusH3Species)
    { 
      Vector3 newvel = ui + ((temp/mn)*uProducts)/sec_nminusH3Species->get_dxdt();
      add_particles(  sec_nminusH3Species, newvel);	  
    }
  if (sec_H2Species)
    { 
      Vector3 newvel = ui + ((temp/mH2)*newcoordiante(uProducts, PI/6, phi))
	/ sec_H2Species->get_dxdt();  
      add_particles(sec_H2Species, newvel);	  
    }  
  if (sec_HSpecies)
    { 
      Vector3 newvel = ui + (temp/mH)*newcoordiante(uProducts, PI/6, phi + PI)
	/sec_HSpecies->get_dxdt();  
      add_particles(sec_HSpecies, newvel);	  
    }
  ue = DELETE_PARTICLE;
  ui = DELETE_PARTICLE;
}
