// test file for ndarray.hpp
#include <stdio.h>

#include "main/pd1.hpp"
#include "utils/ostring.hpp"
#include "main/ndarray.hpp"
#include "main/uniformmesh.hpp"
#include "main/binned.hpp"
#include "utils/equation.hpp"
#include "main/binned_recurse.hpp"

main()
{
  FILE *fp = fopen("mybinned.txt", "w");
  FILE *fp2 = fopen("mybinned2.txt", "w");

  Scalar testpoint[2] = { 1.0, 1.0 };

  Equation myeq("f(x,y)=sin(x)+y");
  Equation myeq2("f(x,y)=x+y");

  ndarray<Scalar> myarray;
  Binned<2, Scalar> mybinned(20, 20);
  Binned<2, Scalar> mybinned2(50, 40);
  
  //BinnedRecurse<2, Scalar, Equation, Scalar, double> testcase;

  mybinned.set_limits(0, -5.0, 5.0);
  mybinned.set_limits(1, 0., 4.);
  mybinned2.set_limits(0, -5.0, 5.0);
  mybinned2.set_limits(1, 0., 4.);

  // these effectively do the same thing to mybinned and mybinned2
  mybinned.set_equal(&myeq);
  //  testcase.recurse(mybinned2, myeq, &Equation::eval);

  fprintf(stderr, "answer=%g\n", mybinned.eval(testpoint));

  mybinned2 = mybinned;

  mybinned.print(fp);
  mybinned2.print(fp2);

  fclose(fp);
  fclose(fp2);
}
