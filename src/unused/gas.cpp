#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "main/pd1.hpp"
#include "utils/functions.hpp"
#include "utils/ostring.hpp"
#include "utils/equation.hpp"
#include "xsections/crossx.hpp" // General structure of a cross section.
#include "xsections/crossxsamples.hpp" // Contains certain patents to create cross sections.
#include "xsections/fakecross.hpp"
#include "utils/funccrossx.hpp"
#include "atomic_properties/gas.hpp"

#include "utils/funccrossx_inline.hpp"

Gas::Gas()
{
  nc = ns = 0; 
  ncx = 0;
  incx = NULL;
  cx = NULL;
  collider_types = NULL;
  secSpecies_types = NULL;
  crossXId = NULL; 
}  

Gas::Gas(int ncol, int nsec, int ncrossx)
{
  allocate_arrays(ncol, nsec, ncrossx);
}

Gas::~Gas()
{
  int i, j;
  if(cx != NULL)
  {
    for (i=0; i<icx ; i++)
      {
	if (cx[i] != NULL)
	  {
	    for (j = 0; j<incx[i]; j++) delete[] cx[i][j];
	    delete[] cx[i];
	  }
      }
    delete[] cx;
    delete[] incx;
  }

  if(collider_types!=NULL)  
    {
      delete[] collider_types; 
      delete[] crossXId;
    }
  if (secSpecies_types != NULL) 
    {
      delete[] secSpecies_types[0]; 
      delete[] secSpecies_types[1];
      delete[] secSpecies_types;
    }
}

void Gas::allocate_arrays(int ncol, int nsec, int ncrossx) 
{
  ic = is = icx = 0; 
  ncx = ncrossx;
  nc = ncol;
  ns = nsec;
  cx = NULL;
  incx = NULL;
  collider_types = NULL;
  secSpecies_types = NULL;
  crossXId = NULL;
 
  if (nc) 
    {
      collider_types = new Species_type[nc]; 
      crossXId = new int [nc];
    }
  if (ns) 
    {
      secSpecies_types = new Species_type*[2];
      secSpecies_types[0] = new Species_type[ns];
      secSpecies_types[1] = new Species_type[ns];
    }
  if(ncx) 
    { 
      cx = new CrossX **[ncx];
      incx = new int[ncx];
    } 
}

void Gas::allocate_crosssections(int idx, int inx)
{
  incx[idx] = 0; 
  cx[idx] = new CrossX*[inx];
}

void Gas::add_collider(Species_type st, int i) 
{
  collider_types[ic] = st;
  crossXId[ic] = i; 
  ic++;
}

void Gas::add_secspecies(Species_type st1, Species_type st2) 
{
  secSpecies_types[0][is] = st1; 
  secSpecies_types[1][is] = st2; 
  is++; 
}

// Add cross sections
void Gas::add_crosssection(int idx, Scalar (*f)(Scalar), Scalar _threshold, CrossX::Type _type)
{
  cx[idx][incx[idx]] = new FuncCrossX(f, _threshold, _type);
  incx[idx] ++;
}

void Gas::add_crossX_type1(int idx, Scalar e0, Scalar e1, Scalar e2, Scalar sMax, 
		      CrossX::Type _type)
{
  cx[idx][incx[idx]] = new CrossX_type1(e0, e1, e2, sMax, _type);
  incx[idx]++;
}

void Gas::add_crossX_type2(int idx, Scalar _a, Scalar _b,CrossX:: Type _type)
{
  cx[idx][incx[idx]] = new CrossX_type2(_a, _b,_type);
  incx[idx] ++;
}

void Gas::add_fakecrossX(int idx, Scalar e0, Scalar e1, CrossX::Type _type)
{
  cx[idx][incx[idx]] = new fakeCrossX(e0,e1, _type);
  incx[idx]++;
}

void Gas::add_fakicrossX(int idx, Scalar _a,CrossX:: Type _type)
{ 
  cx[idx][incx[idx]] = new fakiCrossX(_a, _type);
  incx[idx]++;
}

// Printing
void Gas::print_crosssections(Scalar *a, int n, FILE *fp)
{
  int i, j, k;
  CrossX *thecx;
  for(i=0;i<n;i++)
    {
      fprintf(fp, "%g", a[i]);
      for (j=0; j<ncx ; j++)
	{
	  for(k=0; k<incx[j]  ; k++)
	    {
	      thecx = cx[j][k];
	      fprintf(fp, "%g ", thecx->value(a[i]));
	    }
	}
      fprintf(fp, "\n");
    }
}

void Gas::print_crosssections(Scalar Emin, Scalar Emax, int n, bool logscale)
{
  ostring fn = name + ".dat";
  Scalar *f;
  FILE *fp = fopen(fn(), "w");

  if(logscale)
    {
      f = logspacing(Emin, Emax, n-1);
    }
  else
    {
      f = linearspacing(Emin, Emax, n-1);
    }

  print_crosssections(f, n, fp);
  delete [] f;
  fclose(fp);
}

void Gas::print_crosssections(int idx, Scalar *a, int n, FILE *fp)
{
  int i, k;
  CrossX *thecx;
  for(i=0;i<n;i++)
    {
      fprintf(fp, "%g", a[i]);
      for(k=0; k<incx[idx]  ; k++)
	{
	  thecx = cx[idx][k];
	  fprintf(fp, "%g ", thecx->value(a[i]));
	}
      fprintf(fp, "\n");
    }
}

void Gas::print_crosssections(int idx, Scalar Emin, Scalar Emax, int n, bool logscale)
{
  ostring fn = name + ".dat";
  Scalar *f;
  FILE *fp = fopen(fn(), "w");

  if(logscale)
    {
      f = logspacing(Emin, Emax, n-1);
    }
  else
    {
      f = linearspacing(Emin, Emax, n-1);
    }

  print_crosssections(idx , f, n, fp);
  delete [] f;
  fclose(fp);
}


