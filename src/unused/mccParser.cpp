#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "collisionpkg.hpp"
#include "reactions/reaction.hpp"
#include "MCC/mcc_new.hpp"
#include "MCC/mcc.hpp"
#include "xsections/xsection.hpp"
#include "mccParser.hpp"
//#include "argon.hpp"
//#include "helium.hpp"
#include "xsections/fake.hpp"
//#include "hydrogen.hpp"
#include "air_gas.hpp"
#include "hydrocarbons.hpp"

MCCParser::MCCParser (FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		      oopicList<Parameter<int> > int_variables, 
		      oopicList<Species> *slist, bool _initcoltypes)
{
  init_coltypes = _initcoltypes;
  specieslist = slist;
  init_parse(fp, float_variables, int_variables);
}

void MCCParser::setdefaults(void)
{
  classname = "MCC";  
  mccPackage = "";
  createInInitialDefaultNames = false;
  createInSecondaryDefaultNames = false;
  useSecDefaultWeight = false;

  sec_eSpecies= "";
  sec_iSpecies = "";
  sec_iminusHSpecies = "";
  sec_iminusH2Species = "";
  sec_nminusHSpecies = "";
  sec_nminusH2Species = "";
  sec_nminusH3Species = "";  
  sec_HSpecies = "";
  sec_H2Species = "";
  sec_pSpecies = "";
}

void MCCParser::setparamgroup(void)
{
  // MCC collision Package
  pg.add_string("mccPackage", &mccPackage, "Collision Package we want to use");
  
  // Secondary Species recognition
  pg.add_bflag("createInInitialDefaultNames", &createInInitialDefaultNames, true, "Creation of secondary species with initial default names");  
  pg.add_bflag("createInSecondaryDefaultNames", &createInSecondaryDefaultNames,  true,"Creation of secondary species with secondary default names");
  pg.add_bflag("useSecDefaultWeight", &useSecDefaultWeight, true, "Create of secondary species with default weight"); 
  
  pg.add_string("sec_eSpecies", &sec_eSpecies   , "Species name for creation of secondary species"); 
  pg.add_string("sec_iSpecies", &sec_iSpecies   , "Species name for creation of secondary species");
  pg.add_string("sec_iminusHSpecies", &sec_iminusHSpecies   , "Species name for creation of secondary species");
  pg.add_string("sec_iminusH2Species", &sec_iminusH2Species   , "Species name for creation of secondary species");  
  pg.add_string("sec_nminusHSpecies", &sec_nminusHSpecies   , "Species name for creation of secondary species");
  pg.add_string("sec_nminusH2Species", &sec_nminusH2Species   , "Species name for creation of secondary species");
  pg.add_string("sec_nminusH3Species", &sec_nminusH3Species   , "Species name for creation of secondary species");   
  pg.add_string("sec_HSpecies", &sec_HSpecies   , "Species name for creation of secondary species");
  pg.add_string("sec_H2Species", &sec_H2Species   , "Species name for creation of secondary species");
  pg.add_string("sec_pSpecies", &sec_pSpecies   , "Species name for creation of secondary species");
}

void MCCParser::check(void)
{
  if (createInInitialDefaultNames && createInSecondaryDefaultNames) 
    error ("\nMCC::check(): choose betwen creating secondary species with initial or secondary names: not both!\n");
  
  if (init_coltypes)  init_collisiontypes();

  // Recognize mccPackage
  if (mccPackage.length() == 0) { error("\nMCC::chek: MCC Package not specified \n"); }
//  else if (mccPackage == "Argon")     { create_Argon(); }
//  else if (mccPackage == "Helium")    { create_Helium(); }
//  else if (mccPackage == "Hydrogen")  { create_Hydrogen(); } 
  // else if (mccPackage == "Neon")      { mcc = create_Neon (); }  //  fix me!  JH, Sept. 12, 2005, to be found in xpdp1.
  else if (mccPackage == "Air")       { create_Air_gas(); }
  else if (mccPackage == "CH4")       { create_CH4_gas(); }
  else if (mccPackage == "CH3")       { create_CH3_gas(); }
  else if (mccPackage == "CH2")       { create_CH2_gas(); }
  else if (mccPackage == "CH")        { create_CH_gas(); }
  else if (mccPackage == ostring ("Carbon"))    { create_Carbon(); }
  else if (mccPackage == "CH4_ions")  { create_CH4_ions_gas(); }
  else if (mccPackage == "CH3_ions")  { create_CH3_ions_gas(); }
  else if (mccPackage == "CH2_ions")  { create_CH2_ions_gas(); }
  else if (mccPackage == "CH_ions")   { create_CH_ions_gas(); }
  else if (mccPackage == "Fake")  { create_Fake(); }
  else error ("\nMCC::check(): mccPackage not recognized\n");
}

void MCCParser::init_collisiontypes(void)
{
  oopicListIter<Species> thei(*specieslist);
  for(thei.restart(); !thei.Done(); thei++)
    {
      init_collisiontype(thei());
    }
}

void MCCParser::init_collisiontype(Species *sp)
{
  ostring colmodel = sp->get_collisionmodel();
  if (colmodel ==  "E") sp->set_collisiontype(E);
  else if (colmodel ==  "Ar") sp->set_collisiontype(Ar);
  else if (colmodel ==  "He") sp->set_collisiontype(He);
  else if (colmodel ==  "H") sp->set_collisiontype(H);
  else if (colmodel ==  "Ne") sp->set_collisiontype(Ne);  
  else if (colmodel ==  "H2") sp->set_collisiontype(H2);
  else if (colmodel ==  "CH4") sp->set_collisiontype(CH4);
  else if (colmodel ==  "CH3") sp->set_collisiontype(CH3);
  else if (colmodel ==  "CH2") sp->set_collisiontype(CH2);
  else if (colmodel ==  "CH") sp->set_collisiontype(CH);
  else if (colmodel ==  "C") sp->set_collisiontype(C);
  else if (colmodel ==  "Ar+") sp->set_collisiontype(Ar_ions);
  else if (colmodel ==  "He+") sp->set_collisiontype(He_ions);
  else if (colmodel ==  "H+") sp->set_collisiontype(H_ions);
  else if (colmodel ==  "Air") sp->set_collisiontype(Air);
  else if (colmodel ==  "CH4+") sp->set_collisiontype(CH4_ions);
  else if (colmodel ==  "CH3+") sp->set_collisiontype(CH3_ions);
  else if (colmodel ==  "CH2+") sp->set_collisiontype(CH2_ions);
  else if (colmodel ==  "CH+") sp->set_collisiontype(CH_ions);
  else if (colmodel ==  "C+") sp->set_collisiontype(C_ions);
  else if (colmodel ==  "Air+") sp->set_collisiontype(Air_ions); 
  else if (colmodel ==  "Fake_type") sp->set_collisiontype(Fake_type);
  else if (colmodel ==  "") sp->set_collisiontype(NO_SPECIES);
  else { sp->set_collisiontype(NO_SPECIES); fprintf(stderr, "\nMCC: unrecognized collision type\n");};
}

void MCCParser::init_mcc(Scalar _dt, Fields *tf)
{
  oopicListIter<MCC> mccIter(mcclist);
  for (mccIter.restart(); ! mccIter.Done(); mccIter ++)
    {
      mccIter()->init(_dt, tf);
    }
}

///////////////////////////////////
//  Collision Packages creation  //
///////////////////////////////////
// The default names for secondary species are to be set here

#if 0
void MCCParser::create_Argon(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == Ar)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) themcc = new Argon(specieslist, thei(), useSecDefaultWeight, "electrons", "Argon_ions"); 
	  else if(createInSecondaryDefaultNames) themcc = new Argon(specieslist, thei(), useSecDefaultWeight, "secondary_electrons", "secondary_Argon_ions");
	  else themcc = new Argon(specieslist, thei(), useSecDefaultWeight, sec_eSpecies, sec_iSpecies);
	  mcclist.add(themcc); 
	}
    }
  if (!found) fprintf(stderr, "\nMCC: No species with collision type Argon. Argon collision Package inactive. \n");
} 

    
void MCCParser::create_Helium(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == He)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) themcc = new Helium(specieslist, thei(), useSecDefaultWeight, "electrons", "Helium_ions"); 
	  else if(createInSecondaryDefaultNames) themcc = new Helium(specieslist, thei(), useSecDefaultWeight, "secondary_electrons", "secondary_Helium_ions");
	  else themcc = new Helium(specieslist, thei(), useSecDefaultWeight, sec_eSpecies, sec_iSpecies);
	  mcclist.add(themcc); 
	}
    }
  if (!found) fprintf(stderr, "\nMCC: No species with collision type Helium. Helium collision Package inactive.\n");
}

void MCCParser::create_Hydrogen(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == H)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) themcc = new Hydrogen(specieslist, thei(), useSecDefaultWeight, "electrons", "Hydrogen_ions"); 
	  else if(createInSecondaryDefaultNames) themcc = new Hydrogen(specieslist, thei(), useSecDefaultWeight, "secondary_electrons", "secondary_Hydrogen_ions");
	  else themcc = new Hydrogen(specieslist, thei(), useSecDefaultWeight, sec_eSpecies, sec_iSpecies);
	  mcclist.add(themcc); 
	}
    }
  if (!found) fprintf(stderr, "\nMCC: No species with collision type Hydrogen. Hydrogen collision Package inactive.  \n");
}
#endif

void MCCParser::create_Air_gas(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == Air)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) themcc = new Air_gas(specieslist, thei(), useSecDefaultWeight, "electrons", "Air_ions"); 
	  else if(createInSecondaryDefaultNames) themcc = new Air_gas(specieslist, thei(), useSecDefaultWeight, "secondary_electrons", "secondary_Air_ions");
	  else themcc = new Air_gas(specieslist, thei(), useSecDefaultWeight, sec_eSpecies, sec_iSpecies);
	  mcclist.add(themcc); 
	}
      if (!found) fprintf(stderr, "\nMCC: No species with collision type Air. Air collision Package inactive. \n");
    }
}
void MCCParser::create_CH4_gas(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == CH4)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) 
	    themcc = new CH4_gas(specieslist, thei(), useSecDefaultWeight, 
				 "electrons", "CH4_ions", "CH3_ions", "CH2_ions",  
				 "CH3", "CH2", "CH", "Hydrogen", "DiHydrogen", "Hydrogen_ions"); 
	  else if(createInSecondaryDefaultNames) 
	    themcc = new CH4_gas(specieslist, thei(), useSecDefaultWeight, 
				 "secondary_electrons", "secondary_CH4_ions", "secondary_CH3_ions", "secondary_CH2_ions", 
				 "secondary_CH3", "secondary_CH2", "secondary_CH", "secondary_Hydrogen", 
				 "secondary_DiHydrogen",  "secondary_Hydrogen_ions");
	  else themcc = new CH4_gas(specieslist, thei(), useSecDefaultWeight, 
				    sec_eSpecies, sec_iSpecies, sec_iminusHSpecies, sec_iminusH2Species, 
				    sec_nminusHSpecies, sec_nminusH2Species, sec_nminusH3Species, 
				    sec_HSpecies, sec_H2Species, sec_pSpecies);
	  mcclist.add(themcc); 
	}
    }
  if (!found) fprintf(stderr, "\nMCC: No species with collision type CH4. CH4 collision Package inactive. \n");
}

void MCCParser::create_CH3_gas(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == CH3)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) 
	        themcc = new CH3_gas(specieslist, thei(), useSecDefaultWeight, 
				     "electrons", "CH3_ions", "CH2_ions", "CH_ions", 
				     "CH2", "CH", "Carbon", "Hydrogen", "DiHydrogen", "Hydrogen_ions"); 
	  else if(createInSecondaryDefaultNames) 
	    themcc = new CH3_gas(specieslist, thei(), useSecDefaultWeight, 
				 "secondary_electrons", "secondary_CH3_ions", "secondary_CH2_ions", "secondary_CH_ions",
				 "secondary_CH2", "secondary_CH", "secondary_Carbon", "secondary_Hydrogen", 
				 "secondary_DiHydrogen",  "secondary_Hydrogen_ions"); 
	  else themcc = new CH3_gas(specieslist, thei(), useSecDefaultWeight, 
				    sec_eSpecies, sec_iSpecies, sec_iminusHSpecies, sec_iminusH2Species,
				    sec_nminusHSpecies, sec_nminusH2Species, sec_nminusH3Species, 
				    sec_HSpecies, sec_H2Species, sec_pSpecies);
	  mcclist.add(themcc); 
	}
     }
  if (!found) fprintf(stderr, "\nMCC: No species with collision type CH3. CH3 collision Package inactive. \n");
}

void MCCParser::create_CH2_gas(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == CH2)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) 
	    themcc = new CH2_gas(specieslist, thei(), useSecDefaultWeight, 
				 "electrons", "CH2_ions", "CH_ions", "Carbon_ions", 
				 "CH", "Carbon", "", "Hydrogen", "DiHydrogen", "Hydrogen_ions"); 
	  else if(createInSecondaryDefaultNames) 
	    themcc = new CH2_gas(specieslist, thei(), useSecDefaultWeight, 
				 "secondary_electrons", "secondary_CH2_ions", "secondary_CH_ions", "secondary_Carbon_ions",
				 "secondary_CH", "secondary_Carbon", "", "secondary_Hydrogen", "secondary_DiHydrogen",
				 "secondary_Hydrogen_ions"); 
	  else themcc = new CH2_gas(specieslist, thei(), useSecDefaultWeight, 
				    sec_eSpecies, sec_iSpecies, sec_iminusHSpecies, sec_iminusH2Species,
				    sec_nminusHSpecies, sec_nminusH2Species, sec_nminusH3Species, 
				    sec_HSpecies, sec_H2Species, sec_pSpecies);
	  mcclist.add(themcc); 
	}
    }
 if (!found) fprintf(stderr, "\nMCC: No species with collision type CH2. CH2 collision Package inactive. \n");
}

void MCCParser::create_CH_gas(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == CH)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) 
	    themcc = new CH_gas(specieslist, thei(), useSecDefaultWeight, 
				"electrons", "CH_ions", "Carbon_ions", "", "Carbon", 
				"", "", "Hydrogen", "DiHydrogen", "Hydrogen_ions"); 
	  else if(createInSecondaryDefaultNames) 
	    themcc = new CH_gas(specieslist, thei(), useSecDefaultWeight, 
				"secondary_electrons", "secondary_CH_ions", "secondary_Carbon_ions", "", 
				"secondary_Carbon", "", "", "secondary_Hydrogen", "secondary_DiHydrogen", 
				"secondary_Hydrogen_ions"); 
	  else themcc = new CH_gas(specieslist, thei(), useSecDefaultWeight, 
				   sec_eSpecies, sec_iSpecies, sec_iminusHSpecies, sec_iminusH2Species,
				   sec_nminusHSpecies, sec_nminusH2Species, sec_nminusH3Species, 
				   sec_HSpecies, sec_H2Species, sec_pSpecies);
	  mcclist.add(themcc); 
	}
    }
  if (!found) fprintf(stderr, "\nMCC: No species with collision type CH. CH collision Package inactive. \n");
}

void MCCParser::create_Carbon(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == C)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) 
	    themcc = new Carbon(specieslist, thei(), useSecDefaultWeight, 
				"electrons", "Carbon_ions", "", "", "", 
				"", "", "Hydrogen", "DiHydrogen", "Hydrogen_ions"); 
	  else if(createInSecondaryDefaultNames) 
	    themcc = new Carbon(specieslist, thei(), useSecDefaultWeight, 
				"secondary_electrons", "secondary_Carbon_ions", "", "", 
				"", "", "", "secondary_Hydrogen", "secondary_DiHydrogen", 
				"secondary_Hydrogen_ions"); 
	  else themcc = new Carbon(specieslist, thei(), useSecDefaultWeight, 
				   sec_eSpecies, sec_iSpecies, sec_iminusHSpecies, sec_iminusH2Species,
				   sec_nminusHSpecies, sec_nminusH2Species, sec_nminusH3Species, 
				   sec_HSpecies, sec_H2Species, sec_pSpecies);
	  mcclist.add(themcc); 
	}
    }
  if (!found) fprintf(stderr, "\nMCC: No species with collision type C. Carbon collision Package inactive. \n");
}

void MCCParser::create_CH4_ions_gas(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == CH4_ions)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) 
	    themcc = new CH4_ions_gas(specieslist, thei(), useSecDefaultWeight, 
				      "electrons", "CH4_ions", "CH3_ions", "CH2_ions",
				      "CH3", "CH2", "CH", "Hydrogen", "DiHydrogen", "Hydrogen_ions"); 
	  else if(createInSecondaryDefaultNames) 
	    themcc = new CH4_ions_gas(specieslist, thei(), useSecDefaultWeight, 
				      "secondary_electrons", "secondary_CH4_ions", "secondary_CH3_ions", "secondary_CH2_ions",  
				      "secondary_CH3", "secondary_CH2", "secondary_CH", "secondary_Hydrogen", 
				      "secondary_DiHydrogen", "secondary_Hydrogen_ions"); 
	  else themcc = new CH4_ions_gas(specieslist, thei(), useSecDefaultWeight, 
					 sec_eSpecies, sec_iSpecies, sec_iminusHSpecies, sec_iminusH2Species,
					 sec_nminusHSpecies, sec_nminusH2Species, sec_nminusH3Species, 
					 sec_HSpecies, sec_H2Species, sec_pSpecies);
	  mcclist.add(themcc); 
	}
    }
  if (!found) fprintf(stderr, "\nMCC: No species with collision type CH4_ions. CH4_ions collision Package inactive. \n");
}

void MCCParser::create_CH3_ions_gas(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == CH3_ions)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) 
	    themcc = new CH3_ions_gas(specieslist, thei(), useSecDefaultWeight, 
				      "electrons", "CH3_ions", "CH2_ions", "CH_ions",
				      "CH2", "CH", "Carbon", "Hydrogen", "DiHydrogen", "Hydrogen_ions"); 
	  else if(createInSecondaryDefaultNames) 
	    themcc = new CH3_ions_gas(specieslist, thei(), useSecDefaultWeight,
				      "secondary_electrons", "secondary_CH3_ions", "secondary_CH2_ions", "secondary_CH_ions" ,
				      "secondary_CH2", "secondary_CH", "secondary_Carbon", "secondary_Hydrogen", 
				      "secondary_DiHydrogen", "secondary_Hydrogen_ions"); 
	  else themcc = new CH3_ions_gas(specieslist, thei(), useSecDefaultWeight, 
					 sec_eSpecies, sec_iSpecies, sec_iminusHSpecies, sec_iminusH2Species,
					 sec_nminusHSpecies, sec_nminusH2Species, sec_nminusH3Species, 
					 sec_HSpecies, sec_H2Species, sec_pSpecies);
	  mcclist.add(themcc); 
	}
    }
  if (!found) fprintf(stderr, "\nMCC: No species with collision type CH3_ions. CH3_ions collision Package inactive. \n");
}

void MCCParser::create_CH2_ions_gas(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == CH2_ions)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) 
	    themcc = new CH2_ions_gas(specieslist, thei(), useSecDefaultWeight, 
				      "electrons", "CH2_ions", "CH_ions", "Carbon_ions",
				      "CH", "Carbon", "", "Hydrogen", "DiHydrogen", "Hydrogen_ions"); 
	  else if(createInSecondaryDefaultNames) 
	    themcc = new CH2_ions_gas(specieslist, thei(), useSecDefaultWeight,
				      "secondary_electrons", "secondary_CH2_ions", "secondary_CH_ions", "secondary_Carbon_ions",
				      "secondary_CH", "secondary_Carbon", "", "secondary_Hydrogen", "secondary_DiHydrogen",
				      "secondary_Hydrogen_ions"); 
	  else themcc = new CH2_ions_gas(specieslist, thei(), useSecDefaultWeight, 
					 sec_eSpecies, sec_iSpecies, sec_iminusHSpecies, sec_iminusH2Species,
					 sec_nminusHSpecies, sec_nminusH2Species, sec_nminusH3Species, 
					 sec_HSpecies, sec_H2Species, sec_pSpecies);
	  mcclist.add(themcc); 
	}
    }
  if (!found) fprintf(stderr, "\nMCC: No species with collision type CH2_ions. CH2_ions collision Package inactive. \n");
}	

void MCCParser::create_CH_ions_gas(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_collisiontype() == CH_ions)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) 
	    themcc = new CH_ions_gas(specieslist, thei(), useSecDefaultWeight, 
				     "electrons", "CH_ions", "Carbon_ions", "",
				     "Carbon", "", "", "Hydrogen", "DiHydrogen", "Hydrogen_ions");  
	  else if(createInSecondaryDefaultNames) 
	    themcc = new CH_ions_gas(specieslist, thei(), useSecDefaultWeight, 
				     "secondary_electrons", "secondary_CH_ions", "secondary_Carbon_ions", "",
				     "secondary_Carbon", "", "", "secondary_Hydrogen", "secondary_DiHydrogen",
				     "secondary_Hydrogen_ions");  
	  else themcc = new CH_ions_gas(specieslist, thei(), useSecDefaultWeight, 
					sec_eSpecies, sec_iSpecies, sec_iminusHSpecies, sec_iminusH2Species,
					sec_nminusHSpecies, sec_nminusH2Species, sec_nminusH3Species, 
					sec_HSpecies, sec_H2Species, sec_pSpecies);
	  mcclist.add(themcc); 
	}
    }
  if (!found) fprintf(stderr, "\nMCC: No species with collision type CH_ions. CH_ions collision Package inactive. \n");
}		

void MCCParser::create_Fake(void)
{
  oopicListIter<Species> thei(*specieslist);
  bool found = false;
  for(thei.restart(); !thei.Done();thei++)
    {
      if(thei()->get_collisiontype() == Fake_type)
	{
	  found = true;
	  MCC* themcc = NULL;
	  if(createInInitialDefaultNames) themcc = new Fake(specieslist, thei(), useSecDefaultWeight, "", ""); 
	  else if(createInSecondaryDefaultNames) themcc = new Fake(specieslist, thei(), useSecDefaultWeight, "", "");
	  else themcc = new Fake(specieslist, thei(), useSecDefaultWeight, sec_eSpecies, sec_iSpecies);
	  mcclist.add(themcc); 
	}
    }
  if (!found) fprintf(stderr, "\nMCC: No species with collision type Fake_type. Fake collision Package inactive. \n");
} 

