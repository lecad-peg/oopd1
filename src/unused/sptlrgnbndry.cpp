#include "sptlrgnbndry.hpp"

SpatialRegionBoundary::~SpatialRegionBoundary()
{
  if(sendbuffer != NULL)
    {
      delete [] sendbuffer;
      delete [] recvbuffer;
    }
}

void SpatialRegionBoundary::setdefaults(void)
{
  buffermult = 2;
  sizeperparticle = 3; // x, vx, weight
  nv = 1;
  if(thefield->is_3v())
    {
      nv += 2;
      sizeperparticle += 2;
    }
  nw = sizeperparticle -1;
}

void SpatialRegionBoundary::check()
{
  check_boundary();
}

void SpatialRegionBoundary::init()
{
  init_boundary();

  nsp = theSpeciesList->nItems();
  allocatebuffers();
}

int SpatialRegionBoundary::particleBC(ParticleGroup *p, int i, Scalar frac)
{
  static int i;
  static Vector3 vvec;

  if((++particlecount) == buffersize)
    {
      buffersize *= buffermult;
      allocatebuffers();
    }
  sendbuffer[pindex] = p->get_x(i);
  vvec = p->get_v();
  sendbuffer[pindex + w] = p->get_w(i);
  sendbuffer[++pindex] = vvec.e1();
  for(i=1; i < nv; i++)
    {
      sendbuffer[pindex + i] = vvec.e(i)
    }
}

void SpatialRegionBoundary::allocatebuffers(void)
{
  Scalar *tmpr;
  Scalar *tmps;
  int asize;

  asize = nsp + sizeperparticle*buffersize;

  if(sendbuffer != NULL) // reallocate buffers
    {
      tmpr = new Scalar[asize];
      tmps = new Scalar[asize];
      memcpy(tmps, sendbuffer, oldbuffersize*sizeof(Scalar));
      delete [] sendbuffer;
      delete [] recvbuffer;
      sendbuffer = tmps;
      recvbuffer = tmpr;
    }
  else
    {
      sendbuffer = new Scalar[asize];
      recvbuffer = new Scalar[asize];
      
      sendbuffer[0] = 0.1;
      for(int i=0; i < nsp; i++)
	{
	  sendbuffer[i+1] = 0.1; // truncates to 0;
	}
      pindex = nsp+1; // start indexing here
      particlecount = 0.0;
    }

  oldbuffersize = asize;
}

void SpatialRegionBoundary::sendparticles(void)
{
  sendbuffer[0] = particlecount + 0.1;  // for truncating

  // communicate particles using MPI

}

void SpatialRegionBoundary::recvparticles(void)
{
  // reset indexing array
  for(int i=0; i <= nsp; i++)
    {
      sendbuffer[i] = 0.1;
    }
}
