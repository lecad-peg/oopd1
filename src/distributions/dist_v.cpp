// VDistribution class
//
// Original by HyunChul Kim, 2005
//
// Assumption for this Gaussian Distribution
//  1. The drift velocity can be relativistic,
//    but the thermal speed cannot be.
//  2. Thermal velocity is inputed in a drifting frame.
//  3. In cutoff velocities, zero is considered as +-infinity.
//   (It is because I don't find any way to express infinity inside the code.
//    I believe that the case with infinity cutoff vel. is more common than
//    that with zero one.)
//    It implies that the case where cutoff velocities are really zero
//    cannot be dealt for now.

#include "utils/ovector.hpp"

#ifndef _STANDALONE_LOADLIB

#include "utils/equation.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/grid.hpp"

#else

#include "distributions/dist_load.hpp"
#include "utils/functions.hpp"

#endif

#include "utils/message.hpp"
#include "distributions/dist_xv.hpp"
#include "distributions/dist_v.hpp"
#include "distributions/dist_function.hpp"
#include "distributions/dist_gaussian.hpp"
#include "distributions/dist_arbitrary.hpp"

VDistribution::VDistribution(Vector3 _vt, bool _flux_flag,
		bool _force_nr_flag, LoadType _load_type,
		Vector3 _norm)
{
	set_msg("VDistribution");
	dist_type = GAUSSIAN_DIST;
	load_type = _load_type;
	species = NULL;
	flux_flag = _flux_flag;
	force_nr_flag = _force_nr_flag;
	v0 = 0;
	vt = _vt;
	vcl = 0;
	vcu = 0;
	if (flux_flag)
	{
		mom_dir=get_moment_direction(_norm);
		gaussian_type = new Gaussian<Vector3>(_vt,0,1,mom_dir,0,0,_load_type);
	}
	else
	{
		gaussian_type = new Gaussian<Vector3>(_vt,0,0,1,0,0,_load_type);
	}
	check();
	init();
}

VDistribution::VDistribution(Scalar _vt, bool _flux_flag,
		bool _force_nr_flag, LoadType _load_type,
		Vector3 _norm)
{
	set_msg("VDistribution");
	dist_type = GAUSSIAN_DIST;
	load_type = _load_type;
	species = NULL;
	flux_flag = _flux_flag;
	force_nr_flag = _force_nr_flag;
	v0 = 0;
	vt = Vector3(_vt, _vt, _vt);
	vcl = 0;
	vcu = 0;
	if (flux_flag)
	{
		mom_dir=get_moment_direction(_norm);
		gaussian_type = new Gaussian<Vector3>(vt,0,1,mom_dir,0,0,_load_type);
	}
	else
	{
		gaussian_type = new Gaussian<Vector3>(vt,0,0,1,0,0,_load_type);
	}
	check();
	init();
}

VDistribution::VDistribution(Species *_species, Equation *_func1,
		Equation *_func2, Equation *_func3,
		bool _flux_flag, Vector3 _vcl, Vector3 _vcu,
		LoadType _load_type, Vector3 _norm)
{
	set_msg("VDistribution");
	dist_type = ARBITRARY_DIST;
	species = _species;
	load_type = _load_type;
	flux_flag = _flux_flag;
	func1 = _func1;
	func2 = _func2;
	func3 = _func3;
	vcl = _vcl;
	vcu = _vcu;
	force_nr_flag = species->is_nr_forced();
	if (flux_flag)
	{
		mom_dir=get_moment_direction(_norm);
		arbitrary_type = new Arbitrary<Vector3>(_vcl,_vcu,
				_func1,_func2,_func3,
				1,mom_dir,_load_type);
	}
	else
	{
		arbitrary_type = new Arbitrary<Vector3>(_vcl,_vcu,
				_func1,_func2,_func3,
				0,1,_load_type);
	}
	check();
	init();
}

VDistribution::VDistribution(Species *_species, Equation *_func,
		bool _flux_flag, Vector3 _vcl,Vector3 _vcu,
		LoadType _load_type, Vector3 _norm)
{
	set_msg("VDistribution");
	dist_type = ARBITRARY_DIST;
	species = _species;
	load_type = _load_type;
	flux_flag = _flux_flag;
	func1 = _func;
	vcl = _vcl;
	vcu = _vcu;
	force_nr_flag = species->is_nr_forced();
	if (flux_flag)
	{
		mom_dir=get_moment_direction(_norm);
		arbitrary_type = new Arbitrary<Vector3>(_vcl,_vcu,
				_func,1,mom_dir,_load_type);
	}
	else
	{
		arbitrary_type = new Arbitrary<Vector3>(_vcl,_vcu,_func,0,1,_load_type);
	}
	check();
	init();
}

VDistribution::VDistribution(Species *_species, Vector3 _vt, Vector3 _v0,
		bool _flux_flag, Vector3 _vcl, Vector3 _vcu,
		LoadType _load_type, Vector3 _norm)
{
	set_msg("VDistribution");
	dist_type = GAUSSIAN_DIST;
	species = _species;
	load_type = _load_type;
	flux_flag = _flux_flag;
	v0 = _v0;
	vt = _vt;
	vcl = _vcl;
	vcu = _vcu;
	force_nr_flag = species->is_nr_forced();
	if (flux_flag)
	{
		mom_dir=get_moment_direction(_norm);
		gaussian_type = new Gaussian<Vector3>(_vt,_v0,1,
				mom_dir,_vcl,_vcu,_load_type);
	}
	else
		gaussian_type = new Gaussian<Vector3>(_vt,_v0,0,1,_vcl,_vcu,_load_type);
	check();
	init();
}

VDistribution::VDistribution(Species *_species, Scalar _vt,
		bool _flux_flag, Vector3 _vcl, Vector3 _vcu,
		LoadType _load_type, Vector3 _norm)
{
	set_msg("VDistribution");
	dist_type = GAUSSIAN_DIST;
	species = _species;
	load_type = _load_type;
	flux_flag = _flux_flag;
	v0 = Vector3(0.,0.,0.);
	vt = Vector3(_vt,_vt,_vt);
	vcl = _vcl;
	vcu = _vcu;
	force_nr_flag = species->is_nr_forced();
	if (flux_flag)
	{
		mom_dir=get_moment_direction(_norm);
		gaussian_type = new Gaussian<Vector3>(vt,v0,1,mom_dir,
				_vcl,_vcu,_load_type);
	}
	else {
		gaussian_type = new Gaussian<Vector3>(vt,v0,0,1,_vcl,_vcu,_load_type);
	}
	check();
	init();
}


VDistribution::~VDistribution()
{
	switch (dist_type)
	{
	case GAUSSIAN_DIST:
		delete gaussian_type;
		break;
	case ARBITRARY_DIST:
		delete arbitrary_type;
		break;
	}
}

int VDistribution::get_moment_direction(Vector3 norm)
{
	mom_dir=0;
	for (int i=1; i<=3; i++)
	{
		if (norm.e(i))
		{
			if (mom_dir == 0)
			{
				mom_dir=i;
			}
			else
			{
				terminate_run("moment_direction: Not supported.");
			}
			mul_vec.set(i, -1.);
		}
		else
			mul_vec.set(i, 1.);
	}
	if (mom_dir == 0)
	{
		terminate_run("moment_direction: Error.");
	}
	return mom_dir;
}

void VDistribution::check(void)
{
	if (dist_type == GAUSSIAN_DIST)
	{
		if (vt.magnitude() > SPEED_OF_LIGHT)
		{
			terminate_run("check: Thermal velocity is ultra-relativistic, inversion in gaussian failed.");
		}
		else if (vt.magnitude() > 0.7*SPEED_OF_LIGHT)
		{
			error_msg("check: Thermal velocity is relativistic, inversion will be inaccurate.");
		}
	}

	for (int e=1; e<=3; e++)
	{
		if (dist_type != GAUSSIAN_DIST || vcu.e(e))
		{
			if (vcu.e(e) < vcl.e(e))
				terminate_run("check: vcu < vcl");
			if (vcu.e(e) < v0.e(e))
				terminate_run("check: vcu < v0");
		}
	}

	switch (dist_type)
	{
	case GAUSSIAN_DIST:
		print_msg(MSG_LOW, "GAUSSIAN_DIST...");
		break;
	case ARBITRARY_DIST:
		print_msg(MSG_LOW, "ARBITRARY_DIST...");
		break;
	case UNIFORM_DIST:
		terminate_run("check: Not Implemented.");
		break;
	default:
		terminate_run("check: Wrong Dist_Type");
		break;
	}
}

void VDistribution::init(void)
{
	if (force_nr_flag) gamma0 = 1.;
	else if (dist_type == GAUSSIAN_DIST)
	{
		Scalar vmag = v0.magnitude();
		if (vmag != 0){
			v0normal = v0.unit();
			beta = vmag*iSPEED_OF_LIGHT;
			gamma0 = 1/sqrt(1-beta*beta);
			u0 = gamma0 * v0;
			iv0 = 1/vmag;
			u0normal = u0.unit();
		}
		else {
			u0normal = 0.;
			v0normal = 0.;
			beta = 0.;
			gamma0 = 1.;
			u0 = 0.;
			v0 = 0.;
			iv0 = 0.;
		}
	}
}

Vector3 VDistribution::get_relativistic_addition(Vector3 v) const
{
	v -= v0;
	Vector3 v_parallel_prime
	= v0*(1+v0normal*v*iv0)/(1 + v0*v*iSPEED_OF_LIGHT_SQ);
	Vector3 v_pert = v-(v0normal*v)*v0normal;
	Scalar v_pert_mag = v_pert.magnitude();
	Scalar v_pert_mag_prime =0;
	Vector3 v_pert_prime;
	if (v_pert_mag){
		v_pert_mag_prime
		= v_pert_mag/(gamma0*(1 + beta*v_pert_mag*iSPEED_OF_LIGHT));
		v_pert_prime = v_pert*v_pert_mag_prime/v_pert_mag;
	}
	return (v_parallel_prime+v_pert_prime);
}

Vector3 VDistribution::get_U(Direction direction) const
{
	Vector3 v;

	flag_check(direction);
	switch (dist_type)
	{
	case GAUSSIAN_DIST:
		v = gaussian_type->generate();
		if (direction == NEGATIVE)
		{
			v *= mul_vec;
		}
		else if (direction == NONE && gamma0 >= 1.4)
		{
			v = get_relativistic_addition(v)*gamma0;
		}
		else
		{
			v *= gamma0;
		}
		break;
	case ARBITRARY_DIST:
		v = arbitrary_type->generate();
		v = species->get_gamma2(v)*v;
		if (direction == NEGATIVE) v *= mul_vec;
		break;
	}
	return v;
}

Vector3 VDistribution::get_V(Direction direction) const
{
	Vector3 v;

	flag_check(direction);
	switch (dist_type)
	{
	case GAUSSIAN_DIST:
		v = gaussian_type->generate();
		if (direction == NEGATIVE)
		{
			v *= mul_vec;
		}
		else if (direction == NONE && gamma0 >= 1.4)
		{
			v =  get_relativistic_addition(v);
		}
		break;
	case ARBITRARY_DIST:
		v = arbitrary_type->generate();
		if (direction == NEGATIVE) v *= mul_vec;
		break;
	}
	return v;
}

//Return a new velocity
void VDistribution::get_U(int n, Vector3 *v, Direction direction) const
{
	flag_check(direction);
	switch (dist_type)
	{
	case GAUSSIAN_DIST:
		gaussian_type->generate(n, v);
		if (direction == NEGATIVE)
		{
			Vector3 gamma = gamma0*mul_vec;
			for (int i=0;i<n;i++)
			{
				v[i] *= gamma;
			}
		}
		else if (direction == NONE && gamma0 >= 1.4)
		{
			for (int i=0;i<n;i++)
			{
				v[i] = get_relativistic_addition(v[i])*gamma0;
			}
		}
		else
		{
			for (int i=0;i<n;i++)
			{
				v[i] *= gamma0;
			}
		}
		break;
	case ARBITRARY_DIST:
		arbitrary_type->generate(n, v);
		if (direction == NEGATIVE)
		{
			for (int i=0;i<n;i++)
			{
				v[i] = species->get_gamma2(v[i])*v[i]*mul_vec;
			}
		}
		else
		{
			for (int i=0;i<n;i++)
			{
				v[i] = species->get_gamma2(v[i])*v[i];
			}
		}
		break;
	}
}

//Return a new velocity
void VDistribution::get_V(int n, Vector3 *v, Direction direction) const
{
	flag_check(direction);
	switch (dist_type)
	{
	case GAUSSIAN_DIST:
		gaussian_type->generate(n, v);
		if (direction == NEGATIVE)
		{
			for (int i=0;i<n;i++)
			{
				v[i] *= mul_vec;
			}
		}
		else if (direction == NONE && gamma0 >= 1.4)
		{
			for (int i=0;i<n;i++)
			{
				v[i] = get_relativistic_addition(v[i]);
			}
		}
		break;
	case ARBITRARY_DIST:
		arbitrary_type->generate(n, v);
		if (direction == NEGATIVE)
		{
			for (int i=0;i<n;i++)
			{
				v[i] *= mul_vec;
			}
		}
		break;
	}
}

void VDistribution::get_U(int n, Scalar *v1, Scalar *v2, Scalar *v3,
		Direction direction) const
{
	Vector3 *v = new Vector3 [n];
	get_U(n, v, direction);
	for (int i=0;i<n; i++)
	{
		v1[i]=v[i].e1();
		v2[i]=v[i].e2();
		v3[i]=v[i].e3();
	}
	delete [] v;
}

Scalar VDistribution::ave_1st_mnt_on_boundary(int e, Direction direction) const
{
	switch (dist_type)
	{
	case GAUSSIAN_DIST:
		return gaussian_type->ave_1st_mnt_on_boundary(e, direction);
		break;
	case ARBITRARY_DIST:
		return (Scalar)terminate_run("ave_1st_mnt: Not supported yet.");
		break;
	default:
		return (Scalar)terminate_run("ave_1st_mnt: Not supported.");
		break;
	}
}

// return flux of moment in appropriate direction
Scalar VDistribution::flux(Scalar _v0, Direction direction, int moment) const
{
	Scalar val = 0.;



	return val;
}
