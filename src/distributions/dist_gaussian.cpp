// Gaussian distribution class
//
// Rewritten by HyunChul Kim, 2005, based on XOOPIC code
//
// This distribution is not explicitly related to velocity itself.
//
// TODO: frand vs frand2 , etc... decoupling between velocity and position?
//
// f(x) = exp(-(x-mean)^2/(2*deviation^2))
// integration from x = lcutoff to x = ucutoff
//
//  In 0th gaussian, lcutoff and ucutoff can be in [-infinity, infinity].
//  In 1st gaussian, lcutoff and ucutoff can be in [0, infinity].
//                   v < 0 is not dealt.

#include "utils/ovector.hpp"
#ifndef _STANDALONE_LOADLIB
#include "utils/equation.hpp"
#else
#include "distributions/dist_load.hpp"
#include "utils/functions.hpp"
#endif
#include "utils/message.hpp"
#include "distributions/dist_function.hpp"
#include "distributions/dist_gaussian.hpp"
#ifndef _STANDALONE_LOADLIB
#include "utils/functions.hpp"
#endif
#include <math.h>

template<typename Type>
Gaussian<Type>::Gaussian(Type _deviation, Type _mean, int _moments,
			 int _moments_direction, Type _lcutoff,
			 Type _ucutoff, LoadType _load_type)
{
  this->set_msg("Dist_Gaussian");
  this->load_type = _load_type;
  this->moments = _moments;
  this->moments_direction = _moments_direction;
  deviation_org = deviation = _deviation;
  mean_org = mean = _mean;
  lcutoff_org = this->lcutoff = _lcutoff;
  ucutoff_org = this->ucutoff = _ucutoff;
  this->set_dimension();
  init();
  check();
  this->init_anyMoments();
}

//--------------------------------------------------------------------
template<typename Type>
Gaussian<Type>::~Gaussian()
{
  if(array) { delete[] array; }
  if(moments_array) { delete[] moments_array; }
  if (ucutoffFlag) delete[] ucutoffFlag;
  if (lcutoffFlag) delete[] lcutoffFlag;
  delete[] meanFlag;
}

template<typename Type>
void Gaussian<Type>::init()
{
  array = moments_array = NULL;
  meanFlag = new bool[this->dimension+1];
  for (int e=1; e<= this->dimension; e++)
  {
    if (mean_org.e(e)) meanFlag[e] = true;
    else meanFlag[e] = false;
    if (deviation.e(e))
    {
      // deviation = sqrt(2)*deviation_org
      deviation.set(e, deviation.e(e)*M_SQRT2);
      // mean = mean_org/deviation
      mean.set(e, mean_org.e(e)/deviation.e(e));
    }
  }
  if (this->moments == 1
      && fabs(mean_org.e(this->moments_direction)) >=
      (50*deviation.e(this->moments_direction)))
    this->moments = 0;
  // The values of lcutoff and ucutoff are normalized.
  if (deviation.isNonZero()) set_cutoff();
  else
  {
    lcutoffFlag = ucutoffFlag = NULL;
  }
  this->scale_flag = true;
  this->scale = deviation;
  this->shift = mean_org;
}

template<typename Type>
void Gaussian<Type>::check()
{
  if (this->moments >= 1)
    for (int e=1; e<this->dimension; e++)
    {
//      fprintf(stderr,"lcutoff line 99 = %g\n",this->lcutoff.e(e)); // don't need; MAL200227
//      fprintf(stderr,"ucutoff line 100 = %g\n",this->ucutoff.e(e)); // don't need; MAL200227
      if (this->lcutoff.e(e) < 0 || this->ucutoff.e(e) < 0)
      {
        this->terminate_run("check: Cutoff should be zero or positive.");
      }
    }
}

template<typename Type>
void Gaussian<Type>::set_cutoff()
{
  ucutoffFlag = new bool[this->dimension+1];
  lcutoffFlag = new bool[this->dimension+1];
  for(int e=1; e <= this->dimension; e++)
  {
    if (ucutoff_org.e(e) == 0.) ucutoffFlag[e] = false;
    else ucutoffFlag[e] = true;
    if (lcutoff_org.e(e) == 0.) lcutoffFlag[e] = false;
    else lcutoffFlag[e] = true;
    if (meanFlag[e])
    {
      // lcutoff = (lcutoff_org-mean_org)/deviation
      if (lcutoffFlag[e])
	this->lcutoff.set(e, this->lcutoff.e(e)-mean_org.e(e));
      if (ucutoffFlag[e])
	this->ucutoff.set(e, this->ucutoff.e(e)-mean_org.e(e));
    }
    if (deviation.e(e))
    {
      if (lcutoffFlag[e])
	this->lcutoff.set(e, this->lcutoff.e(e)/deviation.e(e));
      if (ucutoffFlag[e])
	this->ucutoff.set(e, this->ucutoff.e(e)/deviation.e(e));
      if ((this->load_type != QUIET && this->load_type != RANDOM)
	    || (e == this->moments_direction
		&& (lcutoffFlag[e] != ucutoffFlag[e]
		|| meanFlag[e] == true))
	    || (e != this->moments_direction
		&& lcutoffFlag[e] != ucutoffFlag[e]))
      {
// For inv_cdf, (mean+3.3*sqrt(2)*deviation) is considered to be infinity.
	if (!ucutoffFlag[e])
	{
	  this->ucutoff.set(e, 3.3);
	  ucutoffFlag[e] = true;
	}
	if (!lcutoffFlag[e])
	{
	  this->lcutoff.set(e, -3.3);
	  lcutoffFlag[e] = true;
	}
      }
    }

  }

  if (this->moments == 1)
  {
    int e = this->moments_direction;
    if (meanFlag[e] == false && ucutoffFlag[e] && lcutoffFlag[e])
    {
      expvu2.set(e, exp(sqr(this->ucutoff.e(e))));
      expvl2.set(e, exp(sqr(this->lcutoff.e(e))));
      vc2 = sqr(this->ucutoff.e(e))+sqr(this->lcutoff.e(e));
    } 
  }
  else if (this->moments == 0)
  {
    for (int e=1; e<=this->dimension; e++)
    {
      if (lcutoffFlag[e] == false && ucutoffFlag[e])
      {
	expvu2.set(e, exp(sqr(this->ucutoff.e(e))));
	vcu2.set(e, sqr(this->ucutoff.e(e)));
      }

    }
  }

}

template<typename Type>
Type Gaussian<Type>::get_mean() const
{   
  return mean_org;
}

template<typename Type>
Type Gaussian<Type>::get_deviation() const
{
  return deviation_org;
}

#define ITMAX 50
#define EPS 5.0e-5
template<typename Type>
Scalar Gaussian<Type>::rtbis_fn(Scalar value, int moments,
                    Scalar x1, Scalar x2, int e) const
{
  int j;
  Scalar dx,f,fmid,xmid,rtb;

  f=fn(x1, e)-value;
  fmid=fn(x2, e)-value;
  if (f*fmid > 0.0) this->terminate_run("rtbis: Root must be bracketed.");
  rtb = f < 0.0 ? (dx=x2-x1,x1) : (dx=x1-x2,x2);
  for (j=1;j<=ITMAX;j++) {
    fmid=fn(xmid=rtb+(dx *= 0.5), e)-value;
    if (fmid <= 0.0) rtb=xmid;
    if (fabs(dx) < EPS || fmid == 0.0) {
      return rtb;
    }
    f=fmid;
  }
  return (Scalar)this->terminate_run("rtbis: Maximum number of iterations exceeded %e.", value/fn(x2, e));
}
#undef ITMAX
#undef EPS

template<typename Type>
Type Gaussian<Type>::generate() const
{
  Scalar value=0;
  Type x;
  Scalar temp=0;

  for (int e=1; e<=this->dimension; e++)
  {
    if (deviation.e(e))
    {
      if (lcutoffFlag[e] && ucutoffFlag[e])
      {
        if (e == this->moments_direction
	    && meanFlag[e] == false
	    && (this->load_type == RANDOM || this->load_type == QUIET))
        {
	  // Case: ucutoffFlag == true && lcutoffFlag == true && meanFlag == false && (RANDOM || QUIET)
	  if (this->load_type == RANDOM) temp = frand();
	  else if (this->load_type == QUIET) temp = base2();
	  value = sqrt(vc2-log(temp*expvl2.e(e) + 
		  (1-temp)*expvu2.e(e)))*deviation.e(e) + mean_org.e(e);
	}
	else value = this->generate_anyMoments(e);
      }
      else if (lcutoffFlag[e] == false)
      {
	if (e == this->moments_direction)
	{
	  if (ucutoffFlag[e] == false && meanFlag[e] == false)
	  {
	    // Case: lcutoffFlag == false && ucutoffFlag == false && meanFlag == false && (RANDOM || QUIET)
	    if (this->load_type == RANDOM)
	      value = half_normal()*deviation_org.e(e);
	    else if (this->load_type == QUIET)
	      value = half_revers_normal()*deviation_org.e(e);
	    else 
	    {
	      this->terminate_run("generate2: Cannot be dealt.");
	    }
	  }
	  else if (ucutoffFlag[e] == true)
	    value = this->generate_anyMoments(e);
	  else
	  {
	    this->terminate_run("generate3: Cannot be reached.");
	  }
	}
	// e != this->moments_direction
	else
	{
	  if (ucutoffFlag[e] == false)
	  {
	  // Case: ucutoffFlag = lcutoffFlag == false && (RANDOM || QUIET)
	    if (this->load_type == QUIET)
	    {
	      value = deviation_org.e(e)*revers_normal() + mean_org.e(e);
	    }
	    else if (this->load_type == RANDOM)
	    {
	      value = deviation_org.e(e)*normal() + mean_org.e(e);
	    }
	    else 
	    {
	      this->terminate_run("generate4: Cannot be dealt.");
	    }
	  }
	  else 
	  {
	    this->terminate_run("generate5: Cannot be dealt.");
	  }
	}
      }
      else if (lcutoffFlag[e] == true && this->lcutoff.e(e) == 0
		&& e != this->moments_direction)
      {
	// Case: lcutoffFlag == true && ucutoffFlag == false
	//       && (RANDOM || QUIET)
	if (this->load_type == QUIET)
	{
	  value = deviation_org.e(e)*revers_normal_lc_zero_with_ucutoff(e) + mean_org.e(e);
	}
	else if (this->load_type == RANDOM)
	{
	  value = deviation_org.e(e)*normal_lc_zero_with_ucutoff(e) + mean_org.e(e);
	}
	else 
	{
	  this->terminate_run("generate6: Cannot be dealt.");
	}
      }
      else
      {
	this->terminate_run("generate7: Cannot be reached.");
      }
    }
    else value = mean_org.e(e);
    x.set(e, value);
  }
  return x;
}

template<typename Type>
void Gaussian<Type>::generate(int n, Type *x)
{
  if (n <= 0)
  {
    this->terminate_run("generate: n <= 0");
  }
  Scalar temp;

  for (int e=1; e<=this->dimension; e++)
  {
    if (deviation.e(e))
    {
      if (lcutoffFlag[e] && ucutoffFlag[e])
      {
        if (e == this->moments_direction
	    && meanFlag[e] == false
	    && (this->load_type == RANDOM || this->load_type == QUIET))
        {
	  if (this->load_type == RANDOM)
	    for (int i=0; i<n; i++)
	    {
	      temp = frand();
	      x[i].set(e, sqrt(vc2-log(temp*expvl2.e(e)+(1-temp)*expvu2.e(e)))*deviation.e(e));
	    }
	  else if (this->load_type == QUIET)
	    for (int i=0; i<n; i++)
	    {
	      temp = base2();
	      x[i].set(e, sqrt(vc2-log(temp*expvl2.e(e)+(1-temp)*expvu2.e(e)))*deviation.e(e));
	    }
	}
	else
	{
	  for (int i=0; i<n; i++)
	  {
 	    x[i].set(e, this->generate_anyMoments(e));
	  }
	}
      }
      else if (lcutoffFlag[e] == false)
      {
	if (e == this->moments_direction)
	{
	  if (ucutoffFlag[e] == false && meanFlag[e] == false)
	  {
	    if (this->load_type == RANDOM)
	      for (int i=0; i<n; i++)
	      {
		x[i].set(e, half_normal()*deviation_org.e(e));
	      }
	    else if (this->load_type == QUIET)
	      for (int i=0; i<n; i++)
	      {
		x[i].set(e, half_revers_normal()*deviation_org.e(e));
	      }
	    else 
	    {
	      this->terminate_run("generate8: Cannot be dealt.");
	    }
	  }
	  else if (ucutoffFlag[e] == true)
	    for (int i=0; i<n; i++)
	    {
	      // with lcutoff value = 0
	      x[i].set(e, this->generate_anyMoments(e));
	    }
	  else
	  {
	    // ucutoffFlag[e] == false && meanFlag == true
	    this->terminate_run("generate9: Cannot be dealt.");
	  }
	}
	// e != this->moments_direction
	else
	{
	  if (ucutoffFlag[e] == false)
	  {
  	    if (this->load_type == QUIET)
	    {
	      for (int i=0; i<n; i++)
	      {
	        x[i].set(e, deviation_org.e(e)*revers_normal() + mean_org.e(e));
	      }
	    }
	    else if (this->load_type == RANDOM)
	    {
	      for (int i=0; i<n; i++)
	      {
	        x[i].set(e, deviation_org.e(e)*normal() + mean_org.e(e));
	      }
	    }
	    else 
	    {
	      this->terminate_run("generate10: Cannot be dealt.");
	    }
	  }
	  else 
	  {
	    this->terminate_run("generate11: Cannot be dealt.");
	  }
	}
      }
      else if (lcutoffFlag[e] == true &&  this->lcutoff.e(e) == 0
		&& e != this->moments_direction)
      {
	if (this->load_type == QUIET)
	{
	  for (int i=0; i<n; i++)
	  {
	    x[i].set(e, deviation_org.e(e)*revers_normal_lc_zero_with_ucutoff(e) + mean_org.e(e));
	  }
	}
	else if (this->load_type == RANDOM)
	{
	  for (int i=0; i<n; i++)
	  {
	    x[i].set(e, deviation_org.e(e)*normal_lc_zero_with_ucutoff(e) + mean_org.e(e));
	  }
	}
	else 
	{
	  this->terminate_run("generate12: Cannot be dealt.");
	}
      }
      else this->terminate_run("generate13: Cannot be dealt.");
    }
    else
    {
      for (int i=0; i<n; i++)
      {
	x[i].set(e, mean_org.e(e));
      }
    }
  }
}

template<typename Type>
Scalar Gaussian<Type>::ave_1st_mnt_on_boundary(int e, Direction direction) const
{
  Scalar value;
  if (deviation.e(e))
  {
    if (lcutoffFlag[e] && ucutoffFlag[e])
    {
      value = (fn(this->lcutoff.e(e),e)-fn(this->ucutoff.e(e),e))
		*deviation.e(e)
	/(sqrt(M_PI)*(oopd1::erf(this->ucutoff.e(e))-oopd1::erf(this->lcutoff.e(e)))); 
    }
    else if (ucutoffFlag[e] == false && lcutoffFlag[e] == false)
    {
      if (mean_org.e(e))
	value = 0.5*(exp(-sqr(mean.e(e)))/sqrt(M_PI)*deviation.e(e)
			+mean_org.e(e)*(1+erf(mean.e(e))));
      else
	// same formula but for faster computation
	// 'value' becomes deviation_org/sqrt(2*M_PI)
	value = 0.5/sqrt(M_PI)*deviation.e(e);
    }
    else
    {
      return (Scalar)this->terminate_run("get_ave_1st_mnt: Not supported.");
    }
  }
  else
  {
    value = mean_org.e(e);
  }
  if (direction == POSITIVE) return value;
  else return -value;
}

// mindgame: Necessary to avoid linking error. Is there any better idea?
template class Gaussian<Vector1>;
template class Gaussian<Vector2>;
template class Gaussian<Vector3>;
