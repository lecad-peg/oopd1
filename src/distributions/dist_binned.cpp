#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/Array2d.hpp" // MAL 12/14/09
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic3d.hpp" // MAL 12/14/09
#include "diagnostics/diagnostic_time.hpp"
#include "diagnostics/diagnosticcontrol.hpp"
#include "utils/ostring.hpp" // MAL 12/15/09
#include "main/uniformmesh.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/boundary.hpp"
#include "distributions/dist_binned.hpp"

// default constructor
BinnedDistribution::BinnedDistribution()
{
	setdefaults();
}

// constructor for energy/angular distribution function mode
BinnedDistribution::BinnedDistribution(int _nEnergy, Scalar _Emax, int _nAngular, bool _coupled)
{
	setdefaults();
    
	nEnergy = _nEnergy;
	nAngular = _nAngular;
	coupled = _coupled;
	Emax = _Emax;
    
	check();
}

// constructor for class Parse
BinnedDistribution::BinnedDistribution(FILE *fp,
			oopicList<Parameter<Scalar> > float_variables,
			oopicList<Parameter<int> > int_variables,
			Boundary * _bd)
{
	setdefaults();
	setparamgroup();
	bd = _bd;
	theSpeciesList = bd->get_SpeciesList();
    
	if(finish_construction)
	{
		parse(fp, float_variables, int_variables);
		check();
		init();
	}
}

BinnedDistribution::~BinnedDistribution()
{
	if(energydist) { delete energydist; }
	if(angledist)  { delete angledist; }
	if(velocitydist) { delete velocitydist; }
    if(enangle) { delete enangle; } // 2d distribution, MAL 12/19/09
	//  tds.deleteAll();
}

void BinnedDistribution::setdefaults(void)
{
	classname = "Distribution";

	// zero pointers
	energydist = 0;
	angledist = 0;
	velocitydist = 0;
    enangle = 0; // MAL 12/19/09
	theSpeciesList = 0;
    
	// set a positive unit normal vector by default
	norm.set(1., 0., 0.);
    
	// by default energy and angle in the input file are coupled
	coupled = true;

    // by default we don't do the 3d plot unless energybins > 2, anglebins > 2, and coupled = true, MAL 12/19/09
    coupled2d = false;
    
	spname = "";
	Emin = 0.;
	Emax = -1.;
	nEnergy = -1;
	nAngular = -1;
	anglemax = 0.5*M_PI;
	species = NULL;
}

void BinnedDistribution::setparamgroup(void)
{
	pg.add_string("species", &spname, "name of species to accumulate");
	pg.add_fparameter("Emin", &Emin, "[eV] minimum energy to accumulate");
	pg.add_fparameter("Emax", &Emax);
	pg.add_fparameter("anglemax", &anglemax);
	pg.add_iparameter("energybins", &nEnergy);
	pg.add_iparameter("anglebins", &nAngular);
	pg.add_bflag("decoupled", &coupled, false); 	//Changed by Deqi Wen 08/05/2018 to check the coupled case
}

void BinnedDistribution::check(void)
{
	ostring errstring;
    
	// check for invalid nEnergy values
	if(nEnergy <= 2)
	{
		fprintf(stderr, "\nBinnedDistribution: energybins <= 2\n");
		exit(1);
	}
    
	// check for invalid nAngular values, MAL 12/19/09
	if(nAngular >= 0 && nAngular <= 2)
	{
		fprintf(stderr, "\nBinnedDistribution: anglebins <= 2\n");
		exit(1);
	}
    
	// initialize the species from string
	if(!species)
	{
		if(spname == "")
		{
			error("BinnedDistribution", "species undeclared");
		}
        
		oopicListIter<Species> thes(*theSpeciesList);
		for(thes.restart(); !thes.Done(); thes++)
		{
			if(thes()->is(spname))
			{
				species = thes();
				break;
			}
		}
		if(!species)
		{
			errstring = "Species '" + spname + "' not found";
			error("BinnedDistribution", errstring());
		}
	}
}

void BinnedDistribution::init(void)
{

	int j0=bd->get_j0(); //  add to keep all diagnostic names distinct, MAL 9/23/10
	int j1=bd->get_j1();
    if(j0 < 0 && j1 >=0) j0 = j1;
    if(j1 < 0 && j0 >=0) j1 = j0;
    ostring j0str = j0;
    ostring j1str = j1;
	ostring diagname = species->get_name() + "(" + j0str + "," + j1str + ")_f"; // MAL 9/23/10
    
	if(nEnergy > 2)
	{
		// set up uniform mesh for energy
		energydist = new DataArray(diagname, nEnergy);
		energymesh.init_mesh(energydist, Emin, Emax);
	}
    
	if(nAngular > 2)
	{
		// set up angular distribution
		angledist = new DataArray(diagname, nAngular); // "angle"->diagname, MAL 9/14/09
		anglemesh.init_mesh(angledist, 0., anglemax);
	}

    coupled2d = nAngular > 2 && nEnergy > 2 && coupled;
	if(coupled2d) // add the coupled (2d) distribution, MAL 12/18/09
	{
		enangle = new Array2D(diagname, nAngular, nEnergy);
		memset(enangle->get_array(), 0, nEnergy*nAngular*sizeof(Scalar));
	}
    
	init_diagnostics();
}

bool BinnedDistribution::is_species(int tag)
{
	if(!species) { return false; }
	if(species->get_ident() == tag) { return true; }
	return false;
}

// add_particle:  adds particle with index i from
// ParticleGroup *p to the existing distribution function
void BinnedDistribution::add_particle(ParticleGroup *p, int i)
{
	if(velocitydist)
	{
		add_velocity1d(p, i);
		return;
	}
    
	Scalar angle, energy;
    
    if(energydist)
	{
		energy = p->get_energy_eV(i);
		energymesh.weight(energy, p->get_w(i));
	}
    
	if(angledist)
	{
		angle = norm.angle(p->get_v(i)); // see angle in ovector.hpp, MAL 11/26/11
		anglemesh.weight(angle, p->get_w(i));
	}
    
    if(coupled2d)
	{
		weight2d(energy, angle, p->get_w(i));
	}
}

// add_particles:  adds a ParticleGroup to the distribution function
void BinnedDistribution::add_particles(ParticleGroup *p)
{
    
	// 1d velocity distribution
	if(velocitydist)
	{
		for(int i=0; i < p->get_n(); i++)
		{
			add_velocity1d(p, i);
		}
		return;
	}
    
	Scalar angle, energy;
	//	int iE, iA; // for 2d coupled distr
	//	Scalar fracE, fracA; // for linear weighting of 2d coupled distr, not used, MAL 12/19/09
    
    if(energydist)
    {
        for(int i=0; i < p->get_n(); i++)
		{
			energy = p->get_energy_eV(i);
			energymesh.weight(energy, p->get_w(i));
		}
    }

    if(angledist)
    {
        for(int i=0; i < p->get_n(); i++)
        {
            angle = norm.angle(p->get_v(i)); // see angle in ovector.hpp, MAL 11/27/11
            anglemesh.weight(angle, p->get_w(i));
        }
    }

    if(coupled2d)
    {
        for(int i=0; i<p->get_n(); i++)
        {
            weight2d(energy, angle, p->get_w(i));
        }
    }
}

// adds velocity to 1d distribution
void BinnedDistribution::add_velocity1d(ParticleGroup *p, int i)
{
	// SHOULD BE FIXED or DEPRECATED, JH, 6/21/2005
	error("BinnedDistribution::add_velocity1d", "not yet implemented");
}

void BinnedDistribution::weight2d(Scalar _en, Scalar _ang, Scalar _wt)
{
    iE = energymesh.floor_index(_en, &fracE);
    iA = anglemesh.floor_index(_ang, &fracA);
    if(iE>=0 && iA >=0)
    {
        enangle->get_ptrptr()[iE][iA] += _wt; // simple floor NGP; linear weighting not needed? MAL 12/19/09
    }
}

// For IED diagnostics, HH 02/22/16
Scalar BinnedDistribution::get_nEnergyBins(void)
{
    return nEnergy;
}

// For IAD diagnostics, HH 06/02/16
Scalar BinnedDistribution::get_nAngleBins(void)
{
    return nAngular;
}

// For IED diagnostics, HH 02/22/16
ostring BinnedDistribution::get_BoundaryPosition(void)
{
    int j0=bd->get_j0(); //  add to keep all diagnostic names distinct, MAL 9/23/10
    int j1=bd->get_j1();
    if(j0 < 0 && j1 >=0) j0 = j1;
    if(j1 < 0 && j0 >=0) j1 = j0;
    ostring j0str = j0;
    ostring j1str = j1;
    ostring diagname = species->get_name() + "(" + j0str + "," + j1str + ")"; // MAL 9/23/10
    return diagname;
}

// initialize diagnostics
void BinnedDistribution::init_diagnostics(void)
{
	DiagnosticControl dc;

	if(energydist)
	{
		tds.add(new Diagnostic2d(energymesh.get_xarray("energy"), energydist));
	}

	if(angledist)
	{
		tds.add(new Diagnostic2d(anglemesh.get_xarray("angle"), angledist)); // added, MAL 9/14/09
	}

	if(coupled2d) // add, MAL 12/17/09
	{
		tds.add(new Diagnostic3d(energymesh.get_xarray("energy"), anglemesh.get_xarray("angle"), enangle));
	}

	// add diagnostics to the master list
	oopicListIter<Diagnostic> thed(tds);
	for(thed.restart(); !thed.Done(); thed++)
	{
		dc.add_diagnostic(thed());
	}
}
