#include "utils/ostring.hpp"
#include "utils/message.hpp"
#include "distributions/dist_xv.hpp"
#if 0
#include "utils/ovector.hpp"
#include "main/species.hpp"
#include "main/boundary.hpp"
#include "distributions/dist_function.hpp"
#endif

XVDistribution::XVDistribution()
{
  set_msg("XVDistribution");
}

#if 0
bool XVDistribution::is_species(int tag)
{
  if(!species) { return false; }
  if(species->get_ident() == tag) { return true; }
}
#endif
