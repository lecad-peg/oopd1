// Distribution function class
//
// Original by HyunChul Kim, 2005
//
// TODO: In quiet loading, different ordinal numbers for different components
//      of dimension.

#include "utils/ovector.hpp"
#ifndef _STANDALONE_LOADLIB
#include "utils/equation.hpp"
#else
#include "distributions/dist_load.hpp"
#include "utils/functions.hpp"
#endif
#include "utils/message.hpp"
#include "distributions/dist_function.hpp"

template<typename Type>
Dist_Function<Type>::Dist_Function()
{ 
  set_msg("Dist_Function");
}

template<typename Type>
void Dist_Function<Type>::set_dimension()
{ 
  Type test;
  dimension = test.dim();
  if (moments == 0) moments_direction = 0;
}

template<typename Type>
void Dist_Function<Type>::init_anyMoments()
{
  int cases;
  index = 3*moments+moments_direction;
  for (int e=1; e<=dimension; e++)
  {
    if (e == moments_direction) cases = index;
    else cases = e;
    start_cdf.set(e,cdf(lcutoff.e(e),cases));
    length_cdf.set(e,cdf(ucutoff.e(e),cases)-start_cdf.e(e));
  }
}

template<typename Type>
void Dist_Function<Type>::generate_anyMoments(int n, Type *x, bool all_dir, int direction)
{
  switch (load_type)
  {
    case UNIFORM: case UNIFORM_NO_EP:
      generate_anyMoments_uniform(n, x, all_dir, direction);
      break;
    case RANDOM:
      generate_anyMoments_random(n, x, all_dir, direction);
      break;
    case QUIET:
      generate_anyMoments_quiet(n, x, all_dir, direction);
      break;
    default:
      terminate_run("anyMoments: Cannot be reached.");
      break;
  }
}

template<typename Type>
Type Dist_Function<Type>::generate_anyMoments() const
{
  switch (load_type)
  {
    case UNIFORM: case UNIFORM_NO_EP:
      return (Type)terminate_run("anyMoments: Cannot be supported.");
      break;
    case RANDOM:
      return generate_anyMoments_random();
      break;
    case QUIET:
      return generate_anyMoments_quiet();
      break;
    default:
      return (Type)terminate_run("anyMoments: Cannot be reached.");
      break;
  }
}

template<typename Type>
Scalar Dist_Function<Type>::generate_anyMoments(int direction) const
{
  switch (load_type)
  {
    case UNIFORM: case UNIFORM_NO_EP:
      return (Scalar)terminate_run("anyMoments: Cannot be supported.");
      break;
    case RANDOM:
      return generate_anyMoments_random(direction);
      break;
    case QUIET:
      return generate_anyMoments_quiet(direction);
      break;
    default:
      return (Scalar)terminate_run("anymoments: Cannot be reached.");
      break;
  }
}

template<typename Type>
void Dist_Function<Type>::generate_anyMoments_uniform(int n, Type *x, bool all_dir, int e)
{
  Type del_cdf, temp_cdf, temp_cdf0; 

  if (load_type == UNIFORM)
  {
    del_cdf = length_cdf/Scalar(n);
    temp_cdf0 = start_cdf + 0.5*del_cdf; // johnv: must retain original start_cdf for PlasmaSource
  }
  else
  {
    del_cdf = length_cdf/Scalar(n+1);
    temp_cdf0 = start_cdf + del_cdf; // johnv: must retain original start_cdf for PlasmaSource
  }
  if (all_dir)
  {
    int cases;
    if (scale_flag)
      for (int e=1; e<=dimension; e++)
      {
	if (e == moments_direction) cases = index;
	else cases = e;
	for (int i=0; i < n; i++)
	{
	  temp_cdf = temp_cdf0+i*del_cdf;
	  x[i].set(e, inv_cdf(temp_cdf.e(e), cases, lcutoff.e(e), ucutoff.e(e))*scale.e(e)+shift.e(e));
	}
      }
    else
      for (int e=1; e<=dimension; e++)
      {
	if (e == moments_direction) cases = index;
	else cases = e;
	for (int i=0; i < n; i++)
	{
	  temp_cdf = temp_cdf0+i*del_cdf;
	  x[i].set(e, inv_cdf(temp_cdf.e(e), cases, lcutoff.e(e), ucutoff.e(e)));
	}
      }
  }
  else if (e == moments_direction)
  {
    if (scale_flag)
      for (int i=0; i < n; i++)
      {
	x[i].set(e, inv_cdf(temp_cdf.e(e), index, lcutoff.e(e), ucutoff.e(e))*scale.e(e)+shift.e(e));
      }
    else
      for (int i=0; i < n; i++)
      {
	x[i].set(e, inv_cdf(temp_cdf.e(e), index, lcutoff.e(e), ucutoff.e(e)));
      }
  }
  else {
    if (scale_flag)
      for (int i=0; i < n; i++)
      {
	temp_cdf = temp_cdf0+i*del_cdf;
	x[i].set(e, inv_cdf(temp_cdf.e(e), e, lcutoff.e(e), ucutoff.e(e))*scale.e(e)+shift.e(e));
      }
    else
      for (int i=0; i < n; i++)
      {
	temp_cdf = temp_cdf0+i*del_cdf;
	x[i].set(e, inv_cdf(temp_cdf.e(e), e, lcutoff.e(e), ucutoff.e(e)));
      }
  }
}

#define RAND_FUNC base2

template<typename Type>
Type Dist_Function<Type>::generate_anyMoments_quiet() const
{
#define NO_ARGUMENT
#include "distributions/dist_function_base.hpp"
#undef NO_ARGUMENT
}

template<typename Type>
Scalar Dist_Function<Type>::generate_anyMoments_quiet(int e) const
{
#define ONE_ARGUMENT
#include "distributions/dist_function_base.hpp"
#undef ONE_ARGUMENT
}

template<typename Type>
void Dist_Function<Type>::generate_anyMoments_quiet(int n, Type *x, bool all_dir, int e) const
{
#define FOUR_ARGUMENTS
#include "distributions/dist_function_base.hpp"
#undef FOUR_ARGUMENTS
}

#undef RAND_FUNC
#define RAND_FUNC frand

template<typename Type>
Type Dist_Function<Type>::generate_anyMoments_random() const
{
#define NO_ARGUMENT
#include "distributions/dist_function_base.hpp"
#undef NO_ARGUMENT
}

template<typename Type>
Scalar Dist_Function<Type>::generate_anyMoments_random(int e) const
{
#define ONE_ARGUMENT
#include "distributions/dist_function_base.hpp"
#undef ONE_ARGUMENT
}

template<typename Type>
void Dist_Function<Type>::generate_anyMoments_random(int n, Type *x, bool all_dir, int e) const
{
#define FOUR_ARGUMENTS
#include "distributions/dist_function_base.hpp"
#undef FOUR_ARGUMENTS
}

#undef RAND_FUNC

template class Dist_Function<Vector1>;
template class Dist_Function<Vector2>;
template class Dist_Function<Vector3>;
