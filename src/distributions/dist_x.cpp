// XDistribution class
//
// Original by HyunChul Kim, 2005

#ifndef _STANDALONE_LOADLIB

#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "utils/equation.hpp"
#include "main/parse.hpp"
#include "main/grid.hpp"

#else

#include "utils/ovector.hpp"
#include "distributions/dist_load.hpp"
#include "utils/functions.hpp"
#include "utils/message.hpp"

#endif

#include "distributions/dist_xv.hpp"
#include "distributions/dist_function.hpp"
#include "distributions/dist_uniform.hpp"
#include "distributions/dist_arbitrary.hpp"
#include "distributions/dist_x.hpp"

// mindgame:
// Assume that e1() is radius in cylindrical and spherical coordinates

// TODO: mix up the different dist_types for each component

XDistribution::XDistribution(Grid *_grid, Scalar _xstart, Scalar _xend,
		LoadType _load_type)
{
	set_msg("XDistribution");
	geom_type = _grid->geometry();
	dimension = 1;
	xstart1 = _xstart;
	xend1 = _xend;
	load_type = _load_type;
	dist_type = UNIFORM_DIST;
	check();
	uniform_type1 = new Uniform<Vector1>(Vector1(_xstart), Vector1(_xend),
			geom_type, 1, _load_type);
}

XDistribution::XDistribution(Grid *_grid, Vector3 _xstart, Vector3 _xend,
		LoadType _load_type)
{
	set_msg("XDistribution");
	geom_type = _grid->geometry();
	dimension = 3;
	xstart3 = _xstart;
	xend3 = _xend;
	load_type = _load_type;
	dist_type = UNIFORM_DIST;
	check();
	uniform_type3 = new Uniform<Vector3>(_xstart, _xend, geom_type, 1,
			_load_type);
}

XDistribution::XDistribution(Grid *_grid, Vector2 _xstart, Vector2 _xend,
		LoadType _load_type)
{
	set_msg("XDistribution");
	geom_type = _grid->geometry();
	dimension = 2;
	xstart2 = _xstart;
	xend2 = _xend;
	load_type = _load_type;
	dist_type = UNIFORM_DIST;
	check();
	uniform_type2 = new Uniform<Vector2>(_xstart, _xend, geom_type, 1,
			_load_type);
}

XDistribution::XDistribution(Grid *_grid, Scalar _xstart, Scalar _xend,
		Equation *_func, LoadType _load_type)
{
	set_msg("XDistribution");
	geom_type = _grid->geometry();
	dimension = 1;
	xstart1 = _xstart;
	xend1 = _xend;
	load_type = _load_type;
	dist_type = ARBITRARY_DIST;
	arbitrary_type1 = new Arbitrary<Vector1>(Vector1(_xstart), Vector1(_xend),
			_func, geom_type, 1,
			_load_type);
	check();
}

XDistribution::XDistribution(Grid *_grid, Vector2 _xstart, Vector2 _xend,
		Equation *_func1, Equation *_func2,
		LoadType _load_type)
{
	set_msg("XDistribution");
	geom_type = _grid->geometry();
	dimension = 2;
	xstart2 = _xstart;
	xend2 = _xend;
	load_type = _load_type;
	dist_type = ARBITRARY_DIST;
	arbitrary_type2 = new Arbitrary<Vector2>(_xstart, _xend, _func1, _func2,
			geom_type, 1, _load_type);
	check();
}

XDistribution::XDistribution(Grid *_grid, Vector3 _xstart, Vector3 _xend,
		Equation *_func1, Equation *_func2,
		Equation *_func3, LoadType _load_type)
{
	set_msg("XDistribution");
	geom_type = _grid->geometry();
	dimension = 3;
	xstart3 = _xstart;
	xend3 = _xend;
	load_type = _load_type;
	dist_type = ARBITRARY_DIST;
	arbitrary_type3 = new Arbitrary<Vector3>(_xstart, _xend, _func1, _func2,
			_func3, geom_type, 1, _load_type);
	check();
}

XDistribution::~XDistribution()
{
	switch (dist_type)
	{
	case ARBITRARY_DIST:
		switch (dimension)
		{
		case 1:
			delete arbitrary_type1;
			break;
		case 2:
			delete arbitrary_type2;
			break;
		case 3:
			delete arbitrary_type3;
			break;
		}
		break;
		case UNIFORM_DIST:
			switch (dimension)
			{
			case 1:
				delete uniform_type1;
				break;
			case 2:
				delete uniform_type2;
				break;
			case 3:
				delete uniform_type3;
				break;
			}
			break;
	}
}

void XDistribution::get_X(int n, Scalar *x) const
{
	if (n <= 0)
	{
		terminate_run("get_X: n <= 0");
	}
	Vector1 *x_tmp = new Vector1[n];
	switch (dist_type)
	{
	case UNIFORM_DIST:
		uniform_type1->generate(n, x_tmp);
		break;
	case ARBITRARY_DIST:
		arbitrary_type1->generate(n, x_tmp);
		break;
	}
	for (int i=0; i<n; i++)
		x[i] = x_tmp[i].e1();
	delete [] x_tmp;
}

void XDistribution::get_X(int n, Vector3 *x) const
{
	if (n <= 0)
	{
		terminate_run("get_X: n <= 0");
	}
	switch (dist_type)
	{
	case UNIFORM_DIST:
		uniform_type3->generate(n, x);
		break;
	case ARBITRARY_DIST:
		arbitrary_type3->generate(n, x);
		break;
	}
}

void XDistribution::get_X(int n, Vector2 *x) const
{
	if (n <= 0)
	{
		terminate_run("get_X: n <= 0");
	}
	switch (dist_type)
	{
	case UNIFORM_DIST:
		uniform_type2->generate(n, x);
		break;
	case ARBITRARY_DIST:
		arbitrary_type2->generate(n, x);
		break;
	}
}

void XDistribution::check()
{
	if (dimension == 1)
	{
		if (xend1 < xstart1)
		{
			terminate_run("check: xend < xstart");
		}
		if (geom_type != PLANAR && xstart1 < 0.)
		{
			terminate_run("check: xstart < 0");
		}
	}
	else
	{
		for (int e=1; e<=dimension; e++)
		{
			if (xend3.e(e) < xstart3.e(e))
			{
				terminate_run("check: xend < xstart");
			}
		}
		if (geom_type != PLANAR && xstart3.e1() < 0.)
		{
			terminate_run("check: xstart < 0");
		}
	}

	switch (geom_type)
	{
	case PLANAR: case CYLINDRICAL: case SPHERICAL:
		break;
	default:
		terminate_run("check: Wrong Geometry");
		break;
	}

	switch (dist_type)
	{
	case UNIFORM_DIST:
		print_msg(MSG_LOW, "UNIFORM_DIST...");
		break;
	case ARBITRARY_DIST:
		print_msg(MSG_LOW, "ARBITRARY_DIST...");
		break;
	case GAUSSIAN_DIST:
		terminate_run("check: Not Implemented.");
		break;
	default:
		terminate_run("check: Wrong Geometry");
		break;
	}
}
