// Uniform distribution class
//
// Rewritten by HyunChul Kim, 2005, separated from grid.cpp
//

#include "utils/ovector.hpp"
#ifdef _STANDALONE_LOADLIB
#include "distributions/dist_load.hpp"
#endif
#include "utils/message.hpp"
#include "distributions/dist_function.hpp"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "distributions/dist_uniform.hpp"

template<typename Type>
Uniform<Type>::Uniform(Type _start, Type _end, int _moments,
		       int _moments_direction, LoadType _load_type)
{
  this->set_msg("Dist_Uniform");
  this->lcutoff = _start;
  this->ucutoff = _end;
  this->moments = _moments;
  this->moments_direction = _moments_direction;
  this->load_type = _load_type;
  this->scale_flag = false;
  this->set_dimension();
  check();
  this->init_anyMoments();
}

template<typename Type>
Uniform<Type>::~Uniform()
{
}

template<typename Type>
void Uniform<Type>::check()
{
}

template<typename Type>
void Uniform<Type>::generate(int n, Type *x)
{
  if (n <= 0)
  {
    this->terminate_run("generate: n <= 0");
  }
  this->generate_anyMoments(n, x); // Fixed by Kristjan Jonsson 11 Feb 2014
}

template<typename Type>
Type Uniform<Type>::generate() const
{
  return this->generate_anyMoments();
}

template class Uniform<Vector1>;
template class Uniform<Vector2>;
template class Uniform<Vector3>;
