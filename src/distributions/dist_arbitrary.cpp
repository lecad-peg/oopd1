// by HyunChul Kim, 2005
#include <math.h>

#include "utils/ovector.hpp"
#ifndef _STANDALONE_LOADLIB
#include "utils/equation.hpp"
#else
#include "distributions/dist_load.hpp"
#include "utils/functions.hpp"
#endif
#include "utils/message.hpp"
#include "distributions/dist_function.hpp"
#include "distributions/dist_arbitrary.hpp"

// mindgame: what should be done for infinite cutoffs ?

template<typename Type>
Arbitrary<Type>::Arbitrary(Type _start, Type _end, Equation *_func,
			   int _moments, int _moments_direction,
			   LoadType _load_type)
{
  this->set_msg("Dist_Arbitrary");
  this->lcutoff = _start;
  this->ucutoff = _end;
  this->moments = _moments;
  this->moments_direction = _moments_direction;
  func1 = _func;
  func2 = NULL;
  func3 = NULL;
  this->load_type = _load_type;
  this->scale_flag = false;
  this->set_dimension();
  check();
  this->init_anyMoments(); 
}

template<typename Type>
Arbitrary<Type>::Arbitrary(Type _start, Type _end, Equation *_func1,
			   Equation *_func2, Equation *_func3,
			   int _moments, int _moments_direction,
			   LoadType _load_type)
{
  this->set_msg("Dist_Arbitrary");
  this->lcutoff = _start;
  this->ucutoff = _end;
  this->moments = _moments;
  this->moments_direction = _moments_direction;
  func1 = _func1;
  func2 = _func2;
  func3 = _func3;
  this->load_type = _load_type;
  this->scale_flag = false;
  this->set_dimension();
  check();
  this->init_anyMoments();  
}

template<typename Type>
Arbitrary<Type>::Arbitrary(Type _start, Type _end, Equation *_func1,
			   Equation *_func2, int _moments,
			   int _moments_direction, LoadType _load_type)
{
  this->set_msg("Dist_Arbitrary");
  this->lcutoff = _start;
  this->ucutoff = _end;
  this->moments = _moments;
  this->moments_direction = _moments_direction;
  func1 = _func1;
  func2 = _func2;
  this->load_type = _load_type;
  this->scale_flag = false;
  this->set_dimension();
  check();
  this->init_anyMoments();
}

template<typename Type>
Arbitrary<Type>::~Arbitrary()
{
}

template<typename Type>
void Arbitrary<Type>::check()
{
}

template<typename Type>
void Arbitrary<Type>::generate(int n, Type *x)
{
  if (n <= 0)
  {
    this->terminate_run("generate: n <= 0");
  }
  this->generate_anyMoments(n, x);  // Fixed by Kristjan Jonsson 11 Feb 2014
}

template<typename Type>
Type Arbitrary<Type>::generate() const
{
  return this->generate_anyMoments();
}

template class Arbitrary<Vector1>;
template class Arbitrary<Vector2>;
template class Arbitrary<Vector3>;
