#ifdef MPIDEFINED
#include <mpi.h>
#endif

#include "main/oopiclist.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "spatial_region/sptlrgn.hpp"
#include "spatial_region/sptlrgngrp.hpp"
#include "main/species.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_time.hpp"
#include "diagnostics/diagnostic_parser.hpp"
#include "diagnostics/diagnosticcontrol.hpp"
#include "main/timing.hpp"
#include "main/plsmdev.hpp"
#include "xgio.h" // for access to xgread and xgwrite for dumpfile, MAL 6/11/10

PlsmDev::PlsmDev()
{
	t = 0.;
	ttemp = 0.;
	thediagparser = 0;
	parent = 0;
	classname = "PLASMA DEVICE";
}

PlsmDev::~PlsmDev()
{
	specieslist.deleteAll();
	sptlrgnlist.deleteAll();

	if(thediagparser) { delete thediagparser; }
}

void PlsmDev::setfilename(char *c)
{
	filename = c;
}

void PlsmDev::update(void)
{
	oopicListIter<SpatialRegion> thei(sptlrgnlist);
	ttemp += dt;
	t = ttemp;

	for(thei.restart(); !thei.Done(); thei++)
	{
		while(thei()->get_t() < ttemp)
		{
			thei()->update();
		}
	}
}

void PlsmDev::setdefaults(void) 
{
	t = 0;
	particlegrouptype = PARTICLEGROUPARRAYTYPE;

	// obtain rank, # of processors
#ifdef MPIDEFINED
	// initialize MPI parameters
	MPI_Comm_size (MPI_COMM_WORLD, &nproc);
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
#else
	// initialize non-MPI (serial) case
	rank = 0;
	nproc = 1;
#endif

	add_child_name("Species");
	add_child_name("DiagnosticControl");
	add_child_name("SpatialRegion");  // add SpatialRegionGroup when/if finished
}

void PlsmDev::setparamgroup(void)
{
	pg.add_fparameter("t", &ttemp, "[s] beginning simulation time");
}

void PlsmDev::init(void)
{
	int i;

	if(WasDumpFileGiven) //test dumpfile number and restore t
	{
		reload_dumpfile();
	}
	else
	{
		t = ttemp;
	}

	oopicListIter<SpatialRegion> thei(sptlrgnlist);

	if(!thediagparser) { thediagparser = new DiagnosticParser(true); }

	for(thei.restart(), dt=0., i=0; !thei.Done(); thei++, i++)
	{
#ifdef MPIDEFINED
		// JK
		printf("\nSpatialRegion %d, MPI rank: %d -> set time to %f\n", i, rank, t);
#endif
		thei()->set_t(t);  // start all SpatialRegions at the same time

		thei()->set_nsteps_total(nsteps_total); // HH 03/03/16
      
		if(thei()->get_dt() > dt)
		{
			dt = thei()->get_dt();
		}
		ostring id="";
		if(sptlrgnlist.nItems() > 1)
		{
			id = i;
		}
		thei()->set_name(id);
		thei()->set_diagnostic_parser(thediagparser);
	}
}

Parse * PlsmDev::special(ostring s)
{
	if(s == "Species")
	{
		Species *thes;
		if(sptlrgnlist.nItems() > 0)
		{
			error("All species must be initialized before the SpatialRegions");
		}


		thes = new Species(f,pg.float_variables, pg.int_variables, specieslist.nItems());
		specieslist.add(thes);
		return thes;
	}
	if(s == "DiagnosticControl")
	{
		if(thediagparser)
		{
			error("DiagnosticControl already initialized");
		}

		thediagparser = new DiagnosticParser(f, pg.float_variables, pg.int_variables, true);
		return thediagparser;
	}
	if(s == "SpatialRegion")
	{
		SpatialRegion *thesr;

		thesr = new SpatialRegion(f, pg.float_variables, pg.int_variables, &specieslist, particlegrouptype);
		sptlrgnlist.add(thesr);
		return thesr;
	}
	if(s == "SpatialRegionGroup")
	{
		SpatialRegionGroup * thesrg;
		fprintf(stderr, "\nplsmdev, parsing SpatialRegionGroup:");

		if(sptlrgnlist.nItems() > 0)
		{
			error("`SpatialRegionGroup' cannot be used with SpatialRegions already defined");
		}

		thesrg = new SpatialRegionGroup(f, pg.float_variables, pg.int_variables, &specieslist, particlegrouptype, rank, nproc, this);
		sptlrgnlist.add(thesrg);
		return thesrg;
	}

	return 0;
}

// add a new SpatialRegion from grid data
void PlsmDev::add_SpatialRegion(int nn, Scalar *newgrid, 
		int geomtype, Scalar dt)
{
	sptlrgnlist.add(new SpatialRegion(nn, newgrid, geomtype,
			&specieslist, particlegrouptype));
}


// obtain SpatialRegion data from remote processor
void PlsmDev::get_SpatialRegions_fromremote(void)
{  
	fprintf(stderr, "\nEntered get_SpatialRegions_fromremote(), rank %d\n", rank);

#ifdef MPIDEFINED
	//temporary variables  to receive remote data
	int nsp; // # species
	Scalar sdata[NSPDATA]; // species data: q, m, np2c
	char name[STR_SIZE]; // species name
	ostring ntmp;
	Scalar dttmp;

	MPI_Status stat;

	// create new SpatialRegion
	sptlrgnlist.add(new SpatialRegion());
	oopicListIter<SpatialRegion> thei(sptlrgnlist);
	thei.restart();

	// receive species data:
	// Scalars: q, m, np2c;
	// also, name string
	MPI_Recv(&nsp, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &stat);  //recv # of species

	for(int i=0; i < nsp; i++)
	{
		MPI_Recv(sdata, NSPDATA, MPI_SCALAR, 0, 0, MPI_COMM_WORLD, &stat);
		MPI_Recv(name, STR_SIZE, MPI_CHAR, 0, 0, MPI_COMM_WORLD, &stat);

		ntmp = name;
		specieslist.add(new Species(sdata[0], sdata[1], sdata[2], ntmp));
	}


	// receive generic SpatialRegion data
	MPI_Recv(&dttmp, 1, MPI_SCALAR, 0, 0, MPI_COMM_WORLD, &stat);
	thei()->set_dt(dttmp);

	// JH, 9/10/2004 - print outs dt from remote for debugging MPI
#ifdef DEBUG
	fprintf(stderr, "\nPlsmDev::get_SpatialRegions_fromremote: dt=%g\n", thei()->get_dt());
#endif

	// recv grid data
	// ngg[0] = # of grid points;
	int ngg[2]; // ngg[1] = geometry type
	Scalar *xtemp;
	MPI_Recv(ngg, 2, MPI_INT, 0, 0, MPI_COMM_WORLD, &stat);
	xtemp = new Scalar[ngg[0]];
	MPI_Recv(xtemp, ngg[0], MPI_SCALAR, 0, 0, MPI_COMM_WORLD, &stat);

	// recv line to start parsing file from
	MPI_Recv(&loadline, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &stat);

	// wait for all processors to complete task
	MPI_Barrier(MPI_COMM_WORLD);

	// cleaup
	delete [] xtemp;
#endif
}

// top level function to reload dumpfile, MAL 5/16/10
void PlsmDev::reload_dumpfile()
{
	FILE * DMPFile;
	if((DMPFile = fopen(theDumpFile,"r")) == NULL) {
		printf("\nPlsmDev::reload_dumpfile(): open dumpfile failed\n"); return;
	}
	int i, nsr, nsp;
	long int nsteps;
	char rev[5];
	char Revision [] = {'1','.','0','0'}; //dump and restore revision number
	char Revision1 [] = {'1','.','0','1'}; //revision number for dump and restore of ReactionGroup
	char Revision2 [] = {'1','.','0','2'}; //revision number for dump and restore of aveNcoll_per_step
	char Revision3 [] = {'1','.','0','3'}; //revision number for dump and restore of evolving fluids
	XGRead(rev,sizeof(char),4,DMPFile,"char");
	for (i=0; i<4; i++)
	{
		if (!(rev[i]==Revision[i] || rev[i]==Revision1[i] || rev[i]==Revision2[i] || rev[i]==Revision3[i]))
		{
			printf("\nIncompatible dump file version\n");
			fclose(DMPFile);
			exit(1);
		}
	}
	XGRead(&ttemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
	XGRead(&nsr,sizeof(int),1,DMPFile,"int");
	if(nsr != 1) {
		printf("\nPlsmDev::reload_dumpfile(), more than one spatial region not implemented in dumpfile\n"); exit(1);
	}
	XGRead(&nsteps,sizeof(long int),1,DMPFile,"int");
	XGRead(&nsp,sizeof(int),1,DMPFile,"int");
	//		printf("\nFrom PlsmDev::init, nsp=%i\n", nsp); //for debug
	if (nsp <=0 ) {
		printf("\nFrom PlsmDev::reload_dumpfile(), nsp=%i\n",nsp); exit(1);
	}
	t = ttemp;
	//		printf("\nPlsmDev::reload_dumpfile(), t=%g\n",t); // for debug dumpfile
	fclose(DMPFile);
}

// print dumpfile to stdout for debug, MAL 5/13/10
void PlsmDev::print_dumpfile(char * dumpfile) // to debug dumpfile, MAL 5/13/10
{
	FILE * DMPFile;
	if((DMPFile = fopen(dumpfile,"r")) == NULL) {
		printf("\nPlsmDev::print_dumpfile: open failed\n"); return;
	}
	char rev[5];
	char Revision [] = {'1','.','0','0'}; //original dump and restore revision number
	char Revision1 [] = {'1','.','0','1'}; //original dump and restore revision number
	char Revision2 [] = {'1','.','0','2'}; //revision number for dump and restore of aveNcoll_per_step, MAL200229
	char Revision3 [] = {'1','.','0','3'}; //revision number for dump and restore of evolving fluids, MAL200229
	Scalar t, r0, r1, sourcestrength, circq, circq1, circq2, circq3, sourceq, sourceq1, sourceq2, sourceq3;
	Scalar sigma, Q, theta0, Iplushalfdt, dQcirc, dQcirc1, sumQ;
	Scalar wt;
	Scalar x, v1, v2, v3, w;
	Scalar extra, aveNcoll_per_step, fluiddensity;
	bool rev0; // true if dumpfile revision number = 1.00
	bool rev1; // true if dumpfile revision number = 1.01
	bool rev2; // true if dumpfile revision number = 1.02, MAL200229
	bool rev3; // true if dumpfile revision number = 1.03, MAL200229
	int nsr, ncond, ndrive, nbound, nsp, id, np, is3v, isvw, npts, nrg, id1, id2, nfluids, ng;
	long int nsteps;
	int isr, i, j, k, l, m, irg;
	XGRead(rev,sizeof(char),4,DMPFile,"char");
	rev0 = true;
	rev1 = true;
	rev2 = true; // MAL200229
	rev3 = true; // MAL200229
	for (i=0; i<4; i++)
	{
		if (!(rev[i]==Revision[i])) rev0 = false;
		if (!(rev[i]==Revision1[i])) rev1 = false;
		if (!(rev[i]==Revision2[i])) rev1 = false; // MAL200229
		if (!(rev[i]==Revision3[i])) rev1 = false; // MAL200229
	}
	XGRead(&t,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
	XGRead(&nsr,sizeof(int),1,DMPFile,"int"); //nsr > 0
	printf("\nPlsmDev::print_dumpfile: revision=%c%c%c%c, t=%g, nsr=%i",rev[0],rev[1],rev[2],rev[3],t,nsr);
	for (isr = 0; isr < nsr; isr++)
	{
		XGRead(&nsteps,sizeof(long int),1,DMPFile,"int"); //nsteps
		XGRead(&nsp,sizeof(int),1,DMPFile,"int"); //nsp > 0
		printf("\nnsteps=%li, nsp=%i",nsteps,nsp);
		for (i = 0; i < nsp; i++)
		{
			XGRead(&id,sizeof(int),1,DMPFile,"int"); // 0, 1, 2, ...
			species_iter.restart(specieslist); // add to print species names in dumpfile, MAL 11/17/10
			ostring spname = species_iter()->get_species_name_with_ident(species_iter,id);
			XGRead(&is3v,sizeof(int),1,DMPFile,"int"); // 0 or 1
			XGRead(&np,sizeof(int),1,DMPFile,"int"); // np >= 0
			printf("\n\tspid=%i (%s), is3v=%i, np=%i",id ,spname.c_str(), is3v, np);
		}

		XGRead(&nbound,sizeof(int),1,DMPFile,"int");
		printf("\nnbound=%i",nbound); // nbound >= 0
		if (nbound > 0) //there are boundaries
		{
			for (l = 0; l < nbound; l++)
			{
				XGRead(&r0,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&r1,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&sumQ,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&npts,sizeof(int),1,DMPFile,"int");
				printf("\nr0=%g, r1=%g, sumQ=%g, npts=%i",r0,r1,sumQ,npts);
				// print weights
				int i, j;
				for (i = 0; i < nsp; i++)
				{
					species_iter.restart(specieslist); // print species names, MAL 1/21/11
					ostring spname = species_iter()->get_species_name_with_ident(species_iter,i);
					printf("\n\tspid=%i (%s)",i, spname.c_str());
					for (j = 0; j < npts; j++)
					{
						XGRead(&wt,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
						printf(", weights=%g",wt);
					}
				}
			}
		}

		XGRead(&ncond,sizeof(int),1,DMPFile,"int");
		printf("\nncond=%i",ncond); // ncond >= 0
		if (ncond > 0) //there are conductors
		{
			for (j = 0; j < ncond; j++)
			{
				XGRead(&r0,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&r1,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&sourcestrength,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&circq,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&circq1,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&circq2,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&circq3,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&sourceq,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&sourceq1,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&sourceq2,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&sourceq3,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&sigma,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&Q,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&Iplushalfdt,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&dQcirc,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				XGRead(&dQcirc1,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				printf("\nr0=%g, r1=%g",r0,r1);
				printf("\n\tsourcestrength=%g, circq=%g, circq1=%g, circq2=%g, circq3=%g",sourcestrength,circq,circq1,circq2,circq3);
				printf("\n\tsourceq=%g, sourceq1=%g, sourceq2=%g, sourceq3=%g (for 2nd current loop, not implemented)",
						sourceq,sourceq1,sourceq2,sourceq3);
				printf("\n\tsigma=%g, Q=%g, Iplushalfdt=%g, dQcirc=%g, dQcirc1=%g",sigma, Q, Iplushalfdt, dQcirc, dQcirc1);
				// add 5/22/10
				XGRead(&ndrive,sizeof(int),1,DMPFile,"int");
				printf("\n\tndrive=%i",ndrive); // ndrive >= 0
				if (ndrive > 0) //there are circuit drives
				{
					for (m = 0; m < ndrive; m++)
					{
						XGRead(&theta0,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
						printf(", theta0=%g",theta0);
					}
				}
				// end add
			} // end loop on j
		}
		XGRead(&nsp,sizeof(int),1,DMPFile,"int"); //nsp > 0
		printf("\nnsp=%i",nsp);
		for (k = 0; k < nsp; k++)
		{
			XGRead(&id,sizeof(int),1,DMPFile,"int"); // 0, 1, 2, ...
			species_iter.restart(specieslist); // print species names, MAL 1/21/11
			ostring spname = species_iter()->get_species_name_with_ident(species_iter,id);
			XGRead(&is3v,sizeof(int),1,DMPFile,"int");
			XGRead(&isvw,sizeof(int),1,DMPFile,"int");
			XGRead(&np,sizeof(int),1,DMPFile,"int"); // np >= 0
			printf("\nspid=%i (%s), is3v=%i, isvw=%i, npart=%i", id, spname.c_str(), is3v, isvw, np);
			if (np > 0) //there are particles
			{
				printf(", first ten particles are:");
				for (l = 0; l < np; l++)
				{
					XGRead(&x,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
					XGRead(&v1,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
					if (is3v)
					{
						XGRead(&v2,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
						XGRead(&v3,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
					}
					XGRead(&w,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
					//print first 10 particles for debug
					if (l <= 9 && is3v) printf("\n\tx=%g, v1=%g, v2=%g, v3=%g, w=%g",x,v1,v2,v3,w);
					if (l <= 9 && !is3v) printf("\n\tx=%g, v1=%g, w=%g",x,v1,w);
				}
			}
		}
		if (rev0 == false)
		{
			XGRead(&nrg,sizeof(int),1,DMPFile,"int"); // number of reaction groups, nrg >= 0
			printf("\nnumber of reaction groups nrg=%i",nrg); // nrg >= 0
			if (nrg > 0) //there are reaction groups
			{
				for (irg = 0; irg < nrg; irg++)
				{
					XGRead(&id1,sizeof(int),1,DMPFile,"int"); // 0, 1, 2, ..., id of reactant1
					species_iter.restart(specieslist); // print species names, MAL 1/21/11
					ostring spname1 = species_iter()->get_species_name_with_ident(species_iter,id1);
					XGRead(&id2,sizeof(int),1,DMPFile,"int"); // 0, 1, 2, ..., id of reactant2
					species_iter.restart(specieslist); // print species names, MAL 1/21/11
					ostring spname2 = species_iter()->get_species_name_with_ident(species_iter,id2);
					// extra: negative (or zero) number indicating the number of physical collisions done in excess in the previous time step
					XGRead(&extra,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // extra, <= 0
					if (rev1 == true)
					{
						printf("\n\treactant1=%i (%s), reactant2=%i (%s), extra=%g",id1,spname1.c_str(),id2,spname2.c_str(),extra);
					}
					else
					{
						XGRead(&aveNcoll_per_step,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
						printf("\n\treactant1=%i (%s), reactant2=%i (%s), extra=%g, aveNcoll_per_step=%g",
								id1,spname1.c_str(),id2,spname2.c_str(),extra,aveNcoll_per_step);
					}
				}
			}
		}
    if(rev3) //Version 1.03 restore evolving fluid densities; MAL200229
    {
//      oopicListIter<Species> thes(specieslist);
      XGRead(&nsp,sizeof(int),1,DMPFile,"int"); // number of species in dumpfile
      printf("\nnsp=%i",nsp);
      for (k = 0; k < nsp; k++)
      {
        XGRead(&id,sizeof(int),1,DMPFile,"int"); // 0, 1, 2, ...
        species_iter.restart(specieslist); // print species names, MAL 1/21/11
        ostring spname = species_iter()->get_species_name_with_ident(species_iter,id);
        XGRead(&nfluids,sizeof(int),1,DMPFile,"int");
        printf("\nspid=%i (%s), nfluids=%i", id, spname.c_str(), nfluids);
        if (nfluids > 0) //there are fluids
        {
          printf(", first five grid densities for each fluid are:");
          for (int m=0; m<nfluids; m++)
          {
            printf("\n\t");
            XGRead(&ng,sizeof(int),1,DMPFile,"int");
            for (l = 0; l < ng; l++)
            {
              XGRead(&fluiddensity,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
              if (l <= 3) printf("n=%g, ",fluiddensity);
              if (l == 4) printf("n=%g",fluiddensity);
            }
          }
        }
      }
    }
	}
	fclose(DMPFile);
	printf("\n");
}

