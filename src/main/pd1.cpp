#include<stdio.h>
#include<math.h>
#include<iostream>
#include<fstream>

#ifdef MPIDEFINED
#include<mpi.h>
#endif

#include<vector>
using namespace std;
#include<iomanip> // For std::setprecision

#include<utility>

extern "C" void XGMainLoop();
extern "C" void Dump(char *);
extern "C" void Quit(void);

#include "main/pd1.hpp"
#include "xgio.h" // add for dumpfile, MAL 5/4/10

#include "main/timing.hpp"

#include "utils/ostring.hpp"
#include "utils/equation.hpp"
#include "utils/functions.hpp"
#include "main/oopiclist.hpp"
#include "utils/ovector.hpp"
#include "main/paramgrp.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/particlegrouparray.hpp"
#include "main/boundary.hpp"
#include "main/conductor.hpp" // add for dumpfile, MAL 5/9/10
#include "main/circuit.hpp" // add for dumpfile, MAL 5/9/10
#include "main/drive.hpp" // add for dumpfile, MAL 5/22/10
#include "main/dielectric.hpp" // add for dumpfile, MAL 5/11/10
#include "main/grid.hpp"
#include "main/load.hpp"
#include "main/drive.hpp"
#include "main/grid.hpp"
#include "main/grid_inline.hpp"
#include "fields/fields.hpp"
#include "spatial_region/sptlrgn.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_time.hpp"
#include "diagnostics/diagnosticcontrol.hpp"
#include "main/plsmdev.hpp"
#include "xsections/xsection.hpp" // add for dumpfile, MAL 8/12/10
#include "reactions/reaction.hpp" // add for dumpfile, MAL 8/12/10
#include "reactions/reactionSubParse.hpp" // add for dumpfile, MAL 8/12/10
#include "MCC/mcc.hpp" // add for dumpfile, MAL 8/12/10
#include "reactions/reactionParse.hpp" // add for dumpfile, MAL 8/12/10
#include "atomic_properties/fluid.hpp" // add for fluid dumpfile, MAL200228
#include "atomic_properties/diffusion_fluid.hpp" // add for fluid dumpfile, MAL200228
PlsmDev theplasmadevice; // the essential global variable
#include "main/parseforpd1.hpp"

// other global variables
bool generate_docs = false;
ostring docfile = "";

// set global line counter variable
int Parse::globallinenumber = 0;

ostring display_title(void)
{
	return ostring("oopd1 -- Object Oriented Plasma Device 1-d");
}

// parse command line arguments --
// returns arguments that cannot be parse internally
vector<char *> parse_args(int argc, char **argv)
{
	ParamGroup pg;
	vector<char *> unparsed_args;

	// add flags to parse
	pg.add_bflag("generate-docs", &generate_docs, true);
	pg.add_string("generate-docs", &docfile);

	pg.setchecking(false);
	ostring tmpstr;
	for(int i=0; i < argc; i++)
	{
		// kill the '-' for options
		if(argv[i][0] == '-') { tmpstr = argv[i] + 1; }
		else { tmpstr = argv[i]; }

		if(!pg.parse(tmpstr)) { unparsed_args.push_back(argv[i]); }
	}
	return unparsed_args;
}

void init(int argc, char **argv)
{
	Time thetimes;
	vector<char *> newargs;

	char *fn;
	// char defaultfilename[] = "test.inp"; // default input file

#ifdef MPIDEFINED
	int ver, subver;
	MPI_Init (&argc, &argv);
	MPI_Get_version(&ver, &subver);
#endif

	thetimes.start_time(INITIALIZATION);

	// parse command line arguments
	newargs = parse_args(argc, argv);

    // Add spacetime diagnostics, HH 03/03/16

    char s[3] = "-s";

    int nsteps_total=0; // Total number of steps as specified in terminal

    for(int index=0; index<argc; index++)
    {
		if (strcmp(s,*(argv+index))==0)
		{
			nsteps_total = atoi(*(argv+index+1));
		}
    }

    theplasmadevice.set_nsteps_total(nsteps_total);

	if(!(generate_docs || docfile.length()))
	{
		printf("\n%s\n", display_title().c_str()); // Display oopd1 title
	}

	// Initialize XGrafix
	XGInit(newargs.size(), &newargs[0], theplasmadevice.get_t_ptr());

	theplasmadevice.setdefaults();
	theplasmadevice.setparamgroup();

	// if in generate-docs mode, print the docs
	FILE *dfp = stdout;
	if(docfile.length())
	{
		dfp = fopen(docfile(), "w");
		generate_docs = true;
		int length = docfile.length();
		char htmstr[]=".html";
		for(int i=5; i >= 4; i--)
		{
			if(length > i)
			{
				ostring tmp = docfile.substr(length-i);

				if(tmp == ostring(htmstr))
				{
					theplasmadevice.set_htmlmode();
				}
			}
			htmstr[i] = '\0';
		}
	}
	if(generate_docs)
	{
		theplasmadevice.set_program_description(display_title());
		theplasmadevice.print_open(dfp);
		theplasmadevice.print_parameters(dfp);
		theplasmadevice.print_close(dfp);

		if(docfile.length())fclose(dfp);
		exit(0);
	}

	// select input file to parse
	if(WasInputFileGiven)
	{
		fn = theInputFile;
	}
	else
	{
		// mindgame: to avoid segfault when the default inputfile is not found.
		printf("Syntax:\n\t%s -i <inputfile> -d [dumpfile]\n", argv[0]);
		printf("\tor `%s -generate-docs' for more information.\n", argv[0]);
		exit(1);
		// fn = defaultfilename;
	}

#ifdef MPIDEFINED
	// print out data (to stdout) for MPI runs
	//	if(theplasmadevice.get_rank() == 0)
	//	{
	fprintf(stdout, "\nMPI oopd1: rank %d of %d processors",
			theplasmadevice.get_rank(),
			theplasmadevice.get_nproc());
	fprintf(stdout, "\nMPI version: %d.%d\n", ver, subver);

	fflush(stdout);
	//	}
#endif

	if(theplasmadevice.get_rank() == 0)
	{
		parse(fn);
	}
	else // for cases of multiple processors
		// use "root" (rank == 0) processor to
		// initially set up and distribute work
	{
		theplasmadevice.get_SpatialRegions_fromremote();
	}

	theplasmadevice.check();
	theplasmadevice.init();

	thetimes.end_time(INITIALIZATION);

}

int main(int argc, char *argv[])
{
	init(argc, argv);

	XGStart();
	return 0;
}

/////////////////////////
// functions for XGrafix
/////////////////////////

void XGMainLoop(void)
{
	theplasmadevice.update();
}

// Write dumpfile, MAL 5/9/10
// version 1.01, add dump and restore of mcc variable "extra", MAL 8/12/10
// version 1.02, add dump and restore of mcc aveNcoll_per_step, MAL 1/22/11
void Dump(char *filename) // write dumpfile, MAL 5/4/10
{
//  char Revision [] = {'1','.','0','2'}; //dumpfile and revert revision number
    char Revision [] = {'1','.','0','3'}; //dumpfile and revert revision number, including fluids; MAL200228
    int ntemp;
    long int nsteps;
    Scalar ftemp;
    int nsp, npts, nbound, nfluids; // add nfluids for fluids dump; MAL200228
    FILE * DMPFile;
    SpatialRegionList sptlrgnlist;
    SpeciesList specieslist;
    BoundaryList boundarylist;
    oopicList<Conductor> conductorlist;
    oopicList<Drive> drivelist; // add for dumpfile, MAL 5/22/10
    oopicList<Dielectric> dielectriclist;
    oopicList<ReactionGroup> reactiongrouplist; // add for dumpfile, MAL 8/12/10
    oopicList<Fluid> fluidlist; // add for fluids dump; MAL200228


    if((DMPFile = fopen(filename,"w")) == NULL) {
        printf("\npd1::Dump(), open failed\n"); return;
    }
    XGWrite(Revision,sizeof(char),4,DMPFile,"char"); //write 4 char revision number
    ftemp = theplasmadevice.get_t();
    XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //write dump time
    sptlrgnlist=*(theplasmadevice.get_sptlrgnlist());
    ntemp = sptlrgnlist.nItems();
    if(ntemp <= 0) {
        printf("\npd1::Dump(), nsr<=0, abort\n"); fclose(DMPFile); return;
    }
    XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write number of spatial regions
    oopicListIter<SpatialRegion> thei(sptlrgnlist);
    for(thei.restart(); !thei.Done(); thei++)
    {
        nsteps = thei()->get_field()->get_nsteps();
		//		printf("\nFrom pd1::Dump(), nsteps=%li\n",nsteps);
        XGWrite(&nsteps,sizeof(long int),1,DMPFile,"int"); // write nsteps
        specieslist=*(thei()->get_specieslist());
        nsp = specieslist.nItems();
        if (nsp <= 0) {
			printf("\nDump: nsp<=0, abort\n"); fclose(DMPFile); return;
        }
        XGWrite(&nsp,sizeof(int),1,DMPFile,"int"); // write number of species
        oopicListIter<Species> thek(specieslist);
        for(thek.restart(); !thek.Done(); thek++)
        {
            ntemp = thek()->get_ident(); //unique species id number
            XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write unique id number for this species
            ntemp = thek()->is_3v();
            XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write is_v3 for this species
            ntemp = thek()->number_particles(); //number of computer particles (of all weights) for this species
            XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write total number of computer particles for this species
        }

        boundarylist=*(thei()->get_boundarylist());
        nbound = boundarylist.nItems();
        XGWrite(&nbound,sizeof(int),1,DMPFile,"int"); // write number of boundaries
        if(nbound > 0) // there is at least one boundary
        {
			oopicListIter<Boundary> thel(boundarylist);
            for(thel.restart(); !thel.Done(); thel++)
            {
                ftemp = thel()->getr0();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write left position of boundary
                ftemp = thel()->getr1();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write right position of boundary
                ftemp = thel()->get_sumQ();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write sum_Qconv on the boundary
                npts = thel()->get_surface_npoints();
                XGWrite(&npts,sizeof(int),1,DMPFile,"int"); // write number of surface points
                int i, j;
                for (i = 0; i < nsp; i++)
                {
                    for (j = 0; j < npts; j++)
                    {
                        ftemp = thel()->get_weights(j,i);
                        XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write weights[j][i]
                    }
                }
            }
        }

        conductorlist = *(thei()->get_conductorlist());
        ntemp = conductorlist.nItems();
        XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write number of conductors
        if(ntemp > 0) // there is at least one conductor
        {
            oopicListIter<Conductor> thej(conductorlist);
            for(thej.restart(); !thej.Done(); thej++)
            {
                ftemp = thej()->getr0();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write left position of conductor
                ftemp = thej()->getr1();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write right position of conductor
                ftemp = thej()->get_thecircuit()->get_sourcestrength();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write sourcestrength
                ftemp = thej()->get_thecircuit()->get_circ_q();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write circ_q
                ftemp = thej()->get_thecircuit()->get_circ_q1();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write circ_q1
                ftemp = thej()->get_thecircuit()->get_circ_q2();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write circ_q2
                ftemp = thej()->get_thecircuit()->get_circ_q3();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write circ_q3
                ftemp = thej()->get_thecircuit()->get_source_q();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write source_q, placeholder for 2nd source current loop
                ftemp = thej()->get_thecircuit()->get_source_q1();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write source_q1
                ftemp = thej()->get_thecircuit()->get_source_q2();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write source_q2
                ftemp = thej()->get_thecircuit()->get_source_q3();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write source_q3
                ftemp = thej()->get_thecircuit()->get_sigma();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write sigma
                ftemp = thej()->get_thecircuit()->get_Q();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write Q for current source
                ftemp = thej()->get_I_plushalfdt();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write I_plushalfdt for current source
                ftemp = thej()->get_dQ_circ();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write dQ_circ for voltage source
                ftemp = thej()->get_dQ_circ1();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write dQ_circ1 for voltage source
                // added 5/22/10
                drivelist = *(thej()->get_thecircuit()->get_drivelist());
                oopicListIter<Drive> them(drivelist);
                ntemp = drivelist.nItems();
                XGWrite(&ntemp,sizeof(int),1,DMPFile,"int");
                if (drivelist.nItems() > 0)
                {
                    for (them.restart(); !them.Done(); them++)
                    {
                        ftemp = them()->getphase();
                        XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
						//						printf("\npd1::Dump(), theta0=%g\n",ftemp); // for debug dumpfile
                    }
                }
				// end addition
            }
        }

//		NOT NEEDED; charge=sumQ_conv on the surface was written in boundary, above
//		dielectriclist = *(thei()->get_dielectriclist());
//		if(dielectriclist.nItems() > 0) {
//			printf("\nDump: dielectric boundaries not implemented\n"); return;
//		}

        specieslist=*(thei()->get_specieslist());
        ntemp = specieslist.nItems();
/*
        if(ntemp <= 0) //there must be at least one species //previously tested, MAL200228
        {
            printf("\nDump: nsp<=0, abort\n");
            // add remove DMPfile here?
            fclose(DMPFile);
            return;
        }
 */
        XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write total number of species (incl fluid species)

        for(thek.restart(); !thek.Done(); thek++)
        {
            ntemp = thek()->get_ident(); //unique species id number
            XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write unique id number for this species
            ntemp = thek()->is_3v();
            XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write is_v3 for this species
            ntemp = thek()->is_variableWeight();
            XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write is_variableWeight for this species
            ntemp = thek()->number_particles(); //number of computer particles (of all weights) for this species
            XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write total number of computer particles for this species
            if (ntemp > 0) thek()->dump_particles(DMPFile); // in species.cpp, which calls dump_particles(DMPFile) in particlegroup.cpp
        }
        reactiongrouplist=*(thei()->get_reactiongrouplist());
        ntemp = reactiongrouplist.nItems();
		//    printf("\nreactiongrouplist items = %i\n",ntemp); // for debug
        XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write number of reaction groups
        if(ntemp > 0) // there is at least one reaction group
        {
            oopicListIter<ReactionGroup> then(reactiongrouplist);
            for(then.restart(); !then.Done(); then++)
            {
                ntemp = then()->get_reactant1()->get_ident();
				XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write unique id number for reactant 1
                ntemp = then()->get_reactant2()->get_ident();
				XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write unique id number for reactant 2
                ftemp = then()->get_mcc()->get_extra();
                // extra: negative (or zero) number indicating the number of physical collisions done in excess in the previous time step
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write extra for this reaction group
                ftemp = then()->get_mcc()->get_aveNcoll_per_step();
                XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // write aveNcoll_per_step for this reaction group
                // for debug of ReactionGroup dumpfile
                //				ostring r1name = then()->get_reactant1()->get_name();
                //				int n1temp = then()->get_reactant1()->get_ident();
                //				ostring r2name = then()->get_reactant2()->get_name();
                //				int n2temp = then()->get_reactant2()->get_ident();
                //				printf("\nreactant1=%s, ident1=%i, reactant2=%s, ident2=%i, extra=%g",r1name.c_str(), n1temp, r2name.c_str(), n2temp, ftemp);
            }
        }

		// add dump of fluid densities; MAL200228
		XGWrite(&nsp,sizeof(int),1,DMPFile,"int"); // write total number of species (incl fluid species)
		for(thek.restart(); !thek.Done(); thek++)
		{
			ntemp = thek()->get_ident(); //unique species id number
			oopicList<Fluid> fluidList;
			fluidList = *(thek()->get_fluidList());
			nfluids = fluidList.nItems();
			XGWrite(&ntemp,sizeof(int),1,DMPFile,"int"); // write unique id number for this species
			XGWrite(&nfluids,sizeof(int),1,DMPFile,"int"); // write number of fluids for this species
			if (nfluids) thek()->dump_fluids(DMPFile);
		}
    }
    fclose(DMPFile);

    // ***************************************************************************
    // Writing results into textfiles each time we write into a dumpfile.
    // Added by HH 02/22/16
    //
    // All the variables needed so far are written into text files which start
    // with the same name as the input file, and are saved into the same folder.
    // Note that this increases greatly the number of files save into the input
    // folder. More variables can be added as needed.
    // ***************************************************************************

    ostring filename_results, grid_file, JdotE_file, nAve_file, flux1Ave_file, vel1Ave_file, TempAve_file,
    phiAve_file, IEDs_file, IADs_file, pwrAve_file, Ickt_file, Iconv_file, n_array_file, Temp_array_file,
    JdotE_array_file, phi_array_file, Ex_array_file, V_DC_file, nFluidAve_file, master_file; // MAL200301

    char *grid_string, *JdotE_string, *nAve_string, *flux1Ave_string, *vel1Ave_string, *TempAve_string,
    *phiAve_string, *nFluidAve_string, *master_string, *IEDs_string, *IADs_string, *pwrAve_string, *Ickt_string, *Iconv_string,*n_array_string, *Temp_array_string,
    *JdotE_array_string, *phi_array_string, *Ex_array_string; // MAL200301

    char *V_DC_string;

    int filename_length;

    // Make filename same as name of input file
    filename_results = theplasmadevice.get_filename();
    filename_length = filename_results.length();
    filename_results = filename_results.substr(0,filename_length-4);

    // Specify files for each diagnostics
    // -----------------------------------------------------------

    ofstream resultsfile_master;
    master_file = filename_results + "_master.txt";
    master_string = master_file.c_str2();
    resultsfile_master.open(master_string);

    ofstream resultsfile_grid;
    grid_file = filename_results + "_grid.txt";
    grid_string = grid_file.c_str2();
    resultsfile_grid.open(grid_string);

    ofstream resultsfile_JdotE;
    JdotE_file = filename_results + "_JdotEAve.txt";
    JdotE_string = JdotE_file.c_str2();
    resultsfile_JdotE.open(JdotE_string);
    Scalar *JdotEtemp;

    ofstream resultsfile_nAve;
    nAve_file = filename_results + "_nAve.txt";
    nAve_string = nAve_file.c_str2();
    resultsfile_nAve.open(nAve_string);
    Scalar *nAvetemp;

    ofstream resultsfile_flux1Ave;
    flux1Ave_file = filename_results + "_flux1Ave.txt";
    flux1Ave_string = flux1Ave_file.c_str2();
    resultsfile_flux1Ave.open(flux1Ave_string);
    Scalar *flux1Avetemp;

    ofstream resultsfile_vel1Ave;
    vel1Ave_file = filename_results + "_vel1Ave.txt";
    vel1Ave_string = vel1Ave_file.c_str2();
    resultsfile_vel1Ave.open(vel1Ave_string);
    Scalar *vel1Avetemp;

    ofstream resultsfile_TempAve;
    TempAve_file = filename_results + "_TempAve.txt";
    TempAve_string = TempAve_file.c_str2();
    resultsfile_TempAve.open(TempAve_string);
    Scalar *TempAvetemp;

    ofstream resultsfile_phiAve;
    phiAve_file = filename_results + "_phiAve.txt";
    phiAve_string = phiAve_file.c_str2();
    resultsfile_phiAve.open(phiAve_string);
    Scalar *phiAvetemp;

    ofstream resultsfile_nFluidAve; // MAL200301
    nFluidAve_file = filename_results + "_nFluidAve.txt";
    nFluidAve_string = nFluidAve_file.c_str2();
    resultsfile_nFluidAve.open(nFluidAve_string);
    Scalar *nFluidAvetemp;

    //Scalar *nIEDstemp;
    //Scalar *nIADstemp;

    ofstream resultsfile_n_array, resultsfile_Temp_array,  resultsfile_JdotE_array,
    resultsfile_phi_array, resultsfile_Ex_array;

    // -----------------------------------------------------------


    // Specify other variables needed
    long int nsteps_results;
    int nsteps_total_results, nsp_results, ngridpts_results, steps_rf_results, naverage_results, nfluids_results, count; // MAL200301
    string species_name, fluid_name; // MAL200301
    Scalar x0_results, x1_results, step_results;

    SpatialRegionList sptlrgnlist_results;
    sptlrgnlist_results=*(theplasmadevice.get_sptlrgnlist());
    oopicListIter<SpatialRegion> thei_results(sptlrgnlist_results);

    SpeciesList specieslist_results;

    // For IED's
    BoundaryList boundarylist_results;
    DataArray *energydist_results, *energybins_results, *angledist_results, *anglebins_results;

    count = 0;

    // Loop over all SpatialRegions
    for(thei_results.restart(); !thei_results.Done(); thei_results++)
    {
		// Check whether we should have spatiotemporal or V_DC diagnostics
		bool spatiotemporal_flag = thei_results()->get_spatiotemporal_flag();
		bool V_DC_flag = thei_results()->get_V_DC_flag();

		// Total number of steps executed already, and total number of steps
		nsteps_results = thei_results()->get_field()->get_nsteps();
		nsteps_total_results = theplasmadevice.get_nsteps_total();

		// Get number of species
		specieslist_results=*(thei_results()->get_specieslist());
		nsp_results = specieslist_results.nItems();

		// Number of timesteps in an rf cycle
		steps_rf_results = thei_results()->get_steps_rf();
		// Number of steps to average
		naverage_results = thei_results()->get_naverage();

		// Print info to terminal
		fprintf(stdout,"\nAdding results to textfile...\n");
		fprintf(stdout,"Number of steps total: %d \n",nsteps_total_results);
		fprintf(stdout,"Number of steps executed: %li \n",nsteps_results);
		fprintf(stdout,"Number of steps in an rf cycle: %d \n",steps_rf_results);
		fprintf(stdout,"Number of steps to average: %d \n",naverage_results);
		fprintf(stdout,"Number of species added to textfile: %d \n",nsp_results);
		fprintf(stdout,"List of species added to textfile: \n");

		// Print info to masterfile
		resultsfile_master << "Adding results to textfile..." << endl;
		resultsfile_master << "Number of steps total: " << nsteps_total_results << endl;
		resultsfile_master << "Number of steps executed: " << nsteps_results << endl;
		resultsfile_master << "Number of steps in an rf cycle: " << steps_rf_results << endl;
		resultsfile_master << "Number of steps to average: " << naverage_results << endl;
		resultsfile_master << "Number of species added to textfile: " << nsp_results << endl;
		resultsfile_master << "List of species added to textfile: " << endl;

		// Loop over all Species
		oopicListIter<Species> thek_results(specieslist_results);
		for(thek_results.restart(); !thek_results.Done(); thek_results++)
		{
			// Get number of grid points and end points
			ngridpts_results = thek_results() -> get_grid() -> getng();
			x0_results = thek_results() -> get_grid() -> getx0();
			x1_results = thek_results() -> get_grid() -> getx1();
			step_results = (x1_results-x0_results)/(ngridpts_results-1);

			// Get species name
			species_name = thek_results()->get_name();

			// Print species name to terminal and masterfile
			fprintf(stdout,"%s \n",species_name.c_str());
			resultsfile_master << species_name.c_str() << endl;

			// Get quantities
			JdotEtemp = thek_results()->get_species_ave_JdotE();
			nAvetemp = thek_results()->get_species_ave_density();
			flux1Avetemp = thek_results()->get_species_ave_flux1();
			vel1Avetemp = thek_results()->get_species_ave_vel1();
			TempAvetemp = thek_results()->get_species_ave_Temp();

			// Loop over grid points and write quantities into file
			if (count==0)
			{
				for (int i=0; i<ngridpts_results; i++)
				{
					resultsfile_grid << std::setprecision(17) << x0_results+i*step_results;
					if (i!=ngridpts_results-1)
					{
						resultsfile_grid << ",";
					}
				}
				count = 1;
			}

			for (int i=0; i<ngridpts_results; i++)
			{
				resultsfile_JdotE << std::setprecision(17) << *(JdotEtemp+i);
				if (i!=ngridpts_results-1)
				{
					resultsfile_JdotE << ",";
				}
				resultsfile_nAve << std::setprecision(17) << *(nAvetemp+i);
				if (i!=ngridpts_results-1)
				{
					resultsfile_nAve << ",";
				}
				resultsfile_flux1Ave << std::setprecision(17) << *(flux1Avetemp+i);
				if (i!=ngridpts_results-1)
				{
					resultsfile_flux1Ave << ",";
				}
				resultsfile_vel1Ave << std::setprecision(17) << *(vel1Avetemp+i);
				if (i!=ngridpts_results-1)
				{
					resultsfile_vel1Ave << ",";
				}
				resultsfile_TempAve << std::setprecision(17) << *(TempAvetemp+i);
				if (i!=ngridpts_results-1)
				{
					resultsfile_TempAve << ",";
				}
			}

			resultsfile_JdotE << endl;
			resultsfile_nAve << endl;
			resultsfile_flux1Ave << endl;
			resultsfile_vel1Ave << endl;
			resultsfile_TempAve << endl;
		}

		// Add for fluid species textfile; MAL200301
		fprintf(stdout,"List of fluid species added to textfile: \n");
		resultsfile_master << "List of species added to textfile: " << endl;
		for ( thek_results.restart(); !thek_results.Done(); thek_results++ )
		{
			// Get species name
			species_name = thek_results()->get_name();
			fluidlist = *(thek_results()->get_fluidList());
			nfluids_results = fluidlist.nItems();
			if (nfluids_results > 0)
			{
				int nfl = 0;
				oopicListIter<Fluid> thefl_results(fluidlist);
				for (thefl_results.restart(); !thefl_results.Done(); thefl_results++)
				{
					// Print species and fluid name to terminal and masterfile
					fprintf(stdout,"Fluid %s(%s)\n",species_name.c_str(), thefl_results()->get_fluidname().c_str());
					nfl++ ;
					nFluidAvetemp = thefl_results()->get_nfluidave();
					for (int i=0; i<ngridpts_results; i++)
					{
						resultsfile_nFluidAve << std::setprecision(17) << *(nFluidAvetemp+i);
						if (i!=ngridpts_results-1 || nfl != nfluids_results)
						{
							resultsfile_nFluidAve << ",";
						}
					}
				}
				resultsfile_nFluidAve << endl;
			}
		}

		// For spatiotemporal diagnostics, HH 04/18/16
		// Only exectued if we are on the last timestep and flag is true

		if (spatiotemporal_flag)
		{

			if (nsteps_results==nsteps_total_results)
			{
				ofstream resultsfile_n_array;
				n_array_file = filename_results + "_n_array.txt";
				n_array_string = n_array_file.c_str2();
				resultsfile_n_array.open(n_array_string);
				Scalar *n_arraytemp;

				ofstream resultsfile_Temp_array;
				Temp_array_file = filename_results + "_Temp_array.txt";
				Temp_array_string = Temp_array_file.c_str2();
				resultsfile_Temp_array.open(Temp_array_string);
				Scalar *Temp_arraytemp;

				ofstream resultsfile_JdotE_array;
				JdotE_array_file = filename_results + "_JdotE_array.txt";
				JdotE_array_string = JdotE_array_file.c_str2();
				resultsfile_JdotE_array.open(JdotE_array_string);
				Scalar *JdotE_arraytemp;

				// Loop over all Species
				for(thek_results.restart(); !thek_results.Done(); thek_results++)
				{
					n_arraytemp = thek_results()->get_n_array();
					Temp_arraytemp = thek_results()->get_Temp_array();
					JdotE_arraytemp = thek_results()->get_JdotE_array();

					for (int k=0; k<steps_rf_results; k++)
					{
						for (int i=0; i<ngridpts_results; i++)
						{
							resultsfile_n_array << std::setprecision(17) << (n_arraytemp[steps_rf_results*i+k])/((double) (naverage_results/steps_rf_results));
							resultsfile_Temp_array << std::setprecision(17) << (Temp_arraytemp[steps_rf_results*i+k])/((double) (naverage_results/steps_rf_results));
							resultsfile_JdotE_array << std::setprecision(17) << (JdotE_arraytemp[steps_rf_results*i+k])/((double) (naverage_results/steps_rf_results));

							if (i!=ngridpts_results-1)
							{
								resultsfile_n_array << ",";
								resultsfile_Temp_array << ",";
								resultsfile_JdotE_array << ",";
							}
						}

						resultsfile_n_array << endl;
						resultsfile_Temp_array << endl;
						resultsfile_JdotE_array << endl;
					}

					resultsfile_n_array << endl;
					resultsfile_Temp_array << endl;
					resultsfile_JdotE_array << endl;
				}

			}
		}

		// For DC bias diagnostics, HH 05/04/16.
		// The DC bias is calculated once at the end of each rf cycle
		// and the bias at each point in time is written into the textfile.
		// Normally, we will only use the last value as the DC bias, but
		// the time evolution is saved so that it is possible to check
		// whether the bias has converged yet.

		if (V_DC_flag)
		{
			ofstream resultsfile_V_DC;
			V_DC_file = filename_results + "_V_DC_timearray.txt";
			V_DC_string = V_DC_file.c_str2();
			resultsfile_V_DC.open(V_DC_string);
			Scalar *V_DC;

			V_DC = thei_results()->get_V_DC();

			for (int k=0; k <= nsteps_total_results/steps_rf_results; k++)
			{

			  resultsfile_V_DC << std::setprecision(17) << *(V_DC+k);

			  if (k != nsteps_total_results/steps_rf_results)
			  {
				resultsfile_V_DC << ",";
			  }
			}
		}



		// Get phiAve
		phiAvetemp = (thei_results()->get_field()->get_phiAve());

		// Loop over grid points and write phiAve into file
		for (int i=0; i<ngridpts_results; i++)
		{
			resultsfile_phiAve << std::setprecision(17) << *(phiAvetemp+i);
			if (i!=ngridpts_results-1)
			{
				resultsfile_phiAve << ",";
			}
		}

		resultsfile_phiAve << endl;

		if (spatiotemporal_flag)
		{
			if (nsteps_results==nsteps_total_results)
			{
				ofstream resultsfile_phi_array;
				phi_array_file = filename_results + "_phi_array.txt";
				phi_array_string = phi_array_file.c_str2();
				resultsfile_phi_array.open(phi_array_string);
				Scalar *phi_arraytemp;

				ofstream resultsfile_Ex_array;
				Ex_array_file = filename_results + "_Ex_array.txt";
				Ex_array_string = Ex_array_file.c_str2();
				resultsfile_Ex_array.open(Ex_array_string);
				Scalar *Ex_arraytemp;

				// For spatiotemporal diagnostics for phi, HH 04/19/16
				// Only exectued if we are on the last timestep
				// Added electric field (E) diagnostics, HH 05/13/16

				phi_arraytemp = thei_results()->get_field()->get_phi_array();

				Ex_arraytemp = thei_results()->get_field()->get_E_array();

				for (int k=0; k<steps_rf_results; k++)
				{
					for (int i=0; i<ngridpts_results; i++)
					{
						resultsfile_phi_array << std::setprecision(17) << (phi_arraytemp[steps_rf_results*i+k])/((double) (naverage_results/steps_rf_results));
						resultsfile_Ex_array << std::setprecision(17) << (Ex_arraytemp[steps_rf_results*i+k])/((double) (naverage_results/steps_rf_results));

						if (i!=ngridpts_results-1)
						{
							resultsfile_phi_array << ",";
							resultsfile_Ex_array << ",";
						}
					}

					resultsfile_phi_array << endl;
					resultsfile_Ex_array << endl;
				}

				resultsfile_phi_array.close();
				resultsfile_Ex_array.close();
			}
		}

		// For IED's and IAD's
		boundarylist_results=*(thei_results()->get_boundarylist());
		oopicListIter<Boundary> thel_results(boundarylist_results);

		// Loop over all boundaries specified in input file
		for(thel_results.restart(); !thel_results.Done(); thel_results++)
		{
			// Loop over number of IED's at that boundary, as specified in input file
			for (int k=0; k<thel_results()->get_nBoundaries(); k++)
			{
				int nEnergyBins_results;
				nEnergyBins_results = thel_results()->get_nEnergyBins(k);
				if (nEnergyBins_results>-1)
				{
					ofstream resultsfile_IEDs;
					// Get energy bins and energy distributions
					energybins_results = (thel_results()->get_energybins(k));
					energydist_results = (thel_results()->get_energydist(k));

					// Name of files
					IEDs_file = filename_results + "_" + thel_results()->get_BoundaryPosition(k) + "_f(energy).txt";
					IEDs_string = IEDs_file.c_str2();
					resultsfile_IEDs.open(IEDs_string);

					// Loop over energy bins and write energy into file
					for (int i=0; i<nEnergyBins_results; i++)
					{
						resultsfile_IEDs << std::setprecision(17) << (*energybins_results)[i];

						if (i!=nEnergyBins_results-1)
						{
							resultsfile_IEDs << ",";
						}
					}

					resultsfile_IEDs << endl;

					// Loop over energy bins and write distribution into file
					for (int i=0; i<nEnergyBins_results; i++)
					{
						resultsfile_IEDs << std::setprecision(17) << (*energydist_results)[i];
						if (i!=nEnergyBins_results-1)
						{
							resultsfile_IEDs << ",";
						}
					}

					resultsfile_IEDs.close();
				}

				int nAngleBins_results;
				nAngleBins_results = thel_results()->get_nAngleBins(k);
				if (nAngleBins_results>-1)
				{
					ofstream resultsfile_IADs;
					// Get angle bins and angle distributions
					anglebins_results = (thel_results()->get_anglebins(k));
					angledist_results = (thel_results()->get_angledist(k));

					// Name of files
					IADs_file = filename_results + "_" + thel_results()->get_BoundaryPosition(k) + "_f(angle).txt";
					IADs_string = IADs_file.c_str2();
					resultsfile_IADs.open(IADs_string);

					// Loop over angle bins and write angle into file
					for (int i=0; i<nAngleBins_results; i++)
					{
						resultsfile_IADs << std::setprecision(17) << (*anglebins_results)[i];

						if (i!=nAngleBins_results-1)
						{
							resultsfile_IADs << ",";
						}
					}

					resultsfile_IADs << endl;

					// Loop over angle bins and write distribution into file
					for (int i=0; i<nAngleBins_results; i++)
					{
						resultsfile_IADs << std::setprecision(17) << (*angledist_results)[i];
						if (i!=nAngleBins_results-1)
						{
							resultsfile_IADs << ",";
						}
					}

					resultsfile_IADs.close();
				}
			}

			// Power diagnostics
			ofstream resultsfile_pwrAve;

			pwrAve_file = filename_results + "_" + thel_results()->get_BoundaryName() + "_pwrAve.txt";
			pwrAve_string = pwrAve_file.c_str2();
			resultsfile_pwrAve.open(pwrAve_string);

			resultsfile_pwrAve << std::setprecision(17) << thel_results()-> get_pwrAve ();
			resultsfile_pwrAve.close();

			// Current diagnostics, HH 05/09/16
			if(spatiotemporal_flag)
			{
				ofstream resultsfile_Ickt;
				ofstream resultsfile_Iconv;

				Ickt_file = filename_results + "_" + thel_results()->get_BoundaryName() + "_Ickt.txt";
				Iconv_file = filename_results + "_" + thel_results()->get_BoundaryName() + "_Iconv.txt";
				Ickt_string = Ickt_file.c_str2();
				Iconv_string = Iconv_file.c_str2();
				resultsfile_Ickt.open(Ickt_string);
				resultsfile_Iconv.open(Iconv_string);

				Scalar *Ickt_arraytemp = thel_results()->get_Ickt_array();
				Scalar *Iconv_arraytemp = thel_results()->get_Iconv_array();

				for (int k=0; k<steps_rf_results; k++)
				{
					resultsfile_Ickt << std::setprecision(17) << Ickt_arraytemp[k]/((double) (naverage_results/steps_rf_results));
					resultsfile_Ickt << endl;

					for (int i=0; i<nsp_results; i++)
					{
						resultsfile_Iconv << std::setprecision(17) << Iconv_arraytemp[nsp_results*k+i]/((double) (naverage_results/steps_rf_results));
						resultsfile_Iconv << endl;
					}

					resultsfile_Iconv << endl;
				}

				resultsfile_Ickt.close();
				resultsfile_Iconv.close();
			}
		}

		if (spatiotemporal_flag)
		{
			resultsfile_n_array.close();
			resultsfile_Temp_array.close();
			resultsfile_JdotE_array.close();
		}

    }

    // Close all files
    resultsfile_JdotE.close();
    resultsfile_nAve.close();
    resultsfile_nFluidAve.close(); // MAL200301
    resultsfile_TempAve.close();
    resultsfile_phiAve.close();
    resultsfile_master.close();

}

void Quit(void)
{
	printf("Dumpfile: %s\n", theDumpFile);
	theplasmadevice.print_dumpfile(theDumpFile); // for debugging dumpfile
	Time thetimes;

	fprintf(stdout, "\nExiting... ID: %d\n", theplasmadevice.get_rank());

#ifdef MPIDEFINED
	MPI_Finalize();
#endif

	thetimes.print_times();
}
