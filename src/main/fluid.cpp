#include<stdio.h>

#include "main/pd1.hpp"
#include "utils/ostring.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/grid.hpp"
#include "fields/fields.hpp"
#include "main/boundary.hpp"
#include "distributions/dist_v.hpp"
#include "atomic_properties/fluid.hpp"

#include "atomic_properties/fluid_inline.hpp"

///////////////////
// ctors and dtor
///////////////////

Fluid::Fluid(Species * _thespecies){
  thefield = 0;
  thespecies = _thespecies;
  phi_small = 0.1;  // 1/10 V by default
  N_tot = 0.; 
  n_fluid = 0;
  n_fluid_old = 0;
  n_fluid_temp = 0; // MAL200301
  n_fluid_ave = 0; // MAL200301
  n_source = 0;
  phi_tmp = 0;
  nonlinear = false;
}

Fluid::Fluid(Fields * _thefield, Species * _thespecies){
  thefield = _thefield;
  thespecies = _thespecies;
  phi_small = 0.1;  // 1/10 V by default
  n_fluid = 0;
  n_fluid_old = 0;
  n_fluid_temp = 0; // MAL200301
  n_fluid_ave = 0; // MAL200301
  n_source = 0;
  phi_tmp = 0;
  nonlinear = false;
}

Fluid::~Fluid(){
  if(n_fluid) { delete [] n_fluid; }
  if(n_fluid_old) { delete [] n_fluid_old;}
  if(n_fluid_temp) { delete [] n_fluid_temp;} // MAL200301
  if(n_fluid_ave) { delete [] n_fluid_ave;} // MAL200301
  if(n_source) {delete [] n_source;}
}

Scalar Fluid::electrostatic_density_grid(int i){
  if(phi_tmp)    {
    return electrostatic_density(phi_tmp[i]);
  }  else  {
    return electrostatic_density(thefield->get_phi(i));
  }
} 

Scalar Fluid::density_grid(int i){
  return electrostatic_density_grid(i)/thespecies->get_q();
}

// electrostatic_derivative() : derivative of 
// the electrostatic_density function with respect to potential
// By default, use the center difference.
Scalar Fluid::electrostatic_derivative(Scalar potential){
  Scalar val = electrostatic_density(potential + phi_small);
  val -= electrostatic_density(potential - phi_small);
  return val/(2.0*phi_small);
}

void Fluid::set_thefield(Fields * _thefield){
  thefield = _thefield; 

  // if(n_fluid) { [should do something here, JH, June 1, 2006] }
  n_fluid = new Scalar[thefield->get_theGrid()->getng()];
  n_fluid_old = new Scalar[thefield->get_theGrid()->getng()];
  n_fluid_temp = new Scalar[thefield->get_theGrid()->getng()]; // MAL200301
  n_fluid_ave = new Scalar[thefield->get_theGrid()->getng()]; // MAL200301
  n_source = new Scalar[thefield->get_theGrid()->getng()];
}

// This method NO LONGER USED; replaced with DiffusionFluid::interact_evolvingfluid_with_boundaries; MAL200303
// Fluid::interact_with_boundaries interacts fluid with boundaries
// requires n_fluid to be calculated; calls Boundary::fluid_BC
// NOTE: Boundary::fluid_BC: interacts a velocity distribution with the boundary
// v: the distribtion function of the fluid
// returns total number of particles absorbed by boundary
// returns number lost in units of [1/s]
// how does the sign of number depend on dir? JH, June 15, 2006;
Scalar Fluid::interact_with_boundaries(void){
  Scalar total = 0.;
  oopicListIter<Boundary> thei(*(thefield->get_boundarylist()));

  VDistribution * thedist = 0;

  for(thei.restart(); !thei.Done(); thei++)    {
    bool thin = true;
    Scalar density = 0.;
      
    thin = (thei()->get_j0() == thei()->get_j1());
    if(thin)	{
      thedist = velocity_distribution(thei()->get_j0());
      density = n_fluid[thei()->get_j0()];	  		  
    }
      
    if(thei()->is_usable(NEGATIVE))	{
      if(!thin)   {
	thedist = velocity_distribution(thei()->get_j0());
	density = n_fluid[thei()->get_j0()];
      }
      total -= thei()->fluid_BC(density, thedist, NEGATIVE);
    }

    if(thei()->is_usable(POSITIVE))	{
      if(!thin)  {
	thedist = velocity_distribution(thei()->get_j1());
	density = n_fluid[thei()->get_j1()];
      }
      total += thei()->fluid_BC(density, thedist, POSITIVE);
    }

    // seeing if density is calculated correctly, JH, June 12, 2006
#ifdef DEBUG
    fprintf(stderr, "\nname=%s(%s), material=%d, thin=%d, isusableNEG=%d, j0=%d; isusablePOS=%d, j1=%d, density=%g, total=%g\n",this->get_thespecies()->get_name().c_str(), this->get_fluidname().c_str(), thei()->get_material(), thin, thei()->is_usable(NEGATIVE), thei()->get_j0(),thei()->is_usable(POSITIVE), thei()->get_j1(),
	    density, total); // for debug, MAL200130
#endif

    if(thedist){
      delete thedist;
      thedist = 0;
    }
  }

  return total;
}

void Fluid::update(Scalar dt){
  if(!thefield)  {
    fprintf(stderr, "BoltzmannModel::update -- thefield is not initialized");
    exit(1);
  }
}
