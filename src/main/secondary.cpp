//********************************************************
// secondary.cpp
//
// Revision history
//
// CH Lim (July/4/2005) Original code.
//
//********************************************************

#include "main/secondary.hpp"
#include "main/grid.hpp"
#include "fields/fields.hpp"
#include "distributions/dist_xv.hpp"
#include "distributions/dist_v.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/boundary.hpp"
#include "main/boundary_inline.hpp"
#include "emmiter/emitter.hpp"
#include "emmiter/secondaryemitter.hpp"
#include "main/particlegrouparray.hpp"

#include "main/grid_inline.hpp"
#include "atomic_properties/fluid.hpp" // add for boundary-fluid interaction, MAL200222
#include "atomic_properties/diffusion_fluid.hpp" // add for boundary-fluid interaction, MAL200222
#include "utils/message.hpp"

// #define DEBUG // uncomment to see PIC-Boundary and Fluid-Boundary secondary creation, MAL200315

Secondary::Secondary (FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables, Fields *t, Boundary *theb)
{
	specieslist = t -> get_specieslist();
	theboundary = theb;
	thegrid = t->get_theGrid();
	init_parse(fp, float_variables, int_variables);
	dt = theboundary->get_thefield()->get_dt();
}

Secondary::~Secondary()
{
	if(dist) delete dist;
	if(semitter) delete semitter;
}

void Secondary::setdefaults(void)
{
	classname = "Secondary";
	secspecies_name = "";
	secSpecies = NULL;
	impact_Species = NULL; // Edit HH 07/16/15
	dist = NULL;
	semitter = NULL;
	np2c0 = -1.;
	constant_np2c = false;
	method = "Vaughan";
	threshold = 0.;
	secondary = 0.;
	Eemit = 1.;
	f_ref = 0.;
	f_sca = 0.;
	energy_max = 0.;
	ks = 1.;
	pic_angle = 0;
	angular_slope = 1.0;
	wall_temp = 0.026;
	Edes = Edam = 0.0;
	flux = 0.0;
	csp = 0.0;
	// meth is the particle push method used in emitter.cpp!!, MAL 9/17/09
	meth = 4; // use for default mover
	//  meth = 5; // bug -> seg fault; occasionally gives dX of wrong sign -> x+dX not within the grid, MAL 12/26/09
	nmax = NMAX;
}

void Secondary::setparamgroup(void)
{
	pg.add_string("secSpecies", &secspecies_name);
	// mindgame: allow multiple impact names
	pg.add_strings("impact_Species", &impact_names);
	// mindgame: np2c of emitted secondaries
	pg.add_fparameter("np2c0", &np2c0);
	pg.add_iparameter("meth",&meth);
	pg.add_iparameter("nmax",&nmax);

	pg.add_string("method",&method);
	pg.add_fparameter("fraction_ref",&f_ref);
	pg.add_fparameter("fraction_scat" , &f_sca);
	pg.add_fparameter("energy_max" , &energy_max);
	pg.add_fparameter("energy_emit" , &Eemit);
	pg.add_fparameter("secondary" , &secondary);
	pg.add_fparameter("threshold" , &threshold);
	pg.add_fparameter("roughness",&ks);
	pg.add_fparameter("angular_slope", &angular_slope, "[/deg] Slope (Yamamura's angular model)");
	pg.add_fparameter ("pic_angle", &pic_angle, "[deg] Maximum emission angle (Yamamura's angular model)");
	pg.add_fparameter ("wall_temperature", &wall_temp, "[eV] Wall temperature (Roth model)");
	pg.add_fparameter ("Edes", &Edes, "[eV] Threshold for surface desorption (Roth model)");
	pg.add_fparameter ("Edam", &Edam, "[eV] Threshold for radiation damage (Roth model)");
	pg.add_fparameter ("flux", &flux, "[m^-2s^-1] Average flux (Roth model)");
}

void Secondary::check(void)
{
	set_msg(classname);

	oopicListIter<Species> thei(*specieslist);

	if (secspecies_name != "")
		for(thei.restart(); !thei.Done(); thei++)
		{
			if(thei()->get_name() == secspecies_name)
			{
				secSpecies = thei();
			}
		}
	int num_names;
	if ((num_names=impact_names.size()))
		for(thei.restart(); !thei.Done(); thei++)
		{
			for(int i=num_names-1; i>=0; i--)
			{
				if(thei()->get_name() == impact_names[i])
				{
					iSpeciesList.add(thei());
					impact_Species = thei(); // Edit HH 07/16/15
				}
			}
		}

	if(secSpecies == NULL) { terminate_run("secSpecies not found"); }
	if(iSpeciesList.isEmpty()) { terminate_run("impact_Species not found"); }
	ispecies_iter.restart(iSpeciesList);

	// mindgame: in case that np2c0 is not given, use np2c0 of secSpecies
	if (np2c0 <= 0) np2c0 = secSpecies->get_np2c();
	if (np2c0 <= 0)
	{
		error_msg("check: np2c is not specified. The weight of impacting species will be used.");
	}
	else constant_np2c = true;

	if(nmax <= 0) { terminate_run("nmax <= 0"); }
}

void Secondary::init()
{
	secSpecies->set_3v_flag(); //for make velocity field 3-dimensional

	variable_weight = secSpecies->is_variableWeight();

	v_emit = sqrt(2*PROTON_CHARGE*Eemit/secSpecies->get_m());

	// CHANGE true to false in the line below, don't need flux, MAL 9/16/09
	// Changed from false to true by Deqi Wen 08/05/2018 to check the coupled case
	// true needed for multipactor_right.inp; otherwise get flag_check error; MAL200227
	dist = new VDistribution(secSpecies, v_emit, true); // for true, the upper and lower cutoff limits should be set; MAL200226

	if (constant_np2c)
		semitter = new SecondaryEmitter(theboundary->get_thefield(),
				theboundary, secSpecies, meth, nmax,
				np2c0, variable_weight);
	else
		semitter = new SecondaryEmitter(theboundary->get_thefield(),
				theboundary, secSpecies, meth, nmax,
				-1, true);
}


void Secondary::init_roth()
{
	// Used parameters:
	Scalar Erel = 1.8; // pure Carbon
	Scalar Etherm = 1.7; // should be Gauss dist around 1.7 with sig=0.3.
	Scalar tempratio  = - Etherm/wall_temp;
	csp = (1/(1+ 3e-7*exp(-1.4/wall_temp)))*(2e-32*flux + exp(tempratio))
    		/ (2e-32*flux + (1 + (2e29*exp(-Erel/wall_temp)/flux))*exp(tempratio));
	secondary =  csp*0.033*exp(tempratio)/(2e-32*flux + exp(tempratio));
}

Scalar Secondary::number(ParticleGroup *p, int i, Species * ispecies, Vector3 v_incident) // add args ispecies and v_incident, MAL200225
{

	Scalar theta, costheta, delta_ratio, E, Emax, w, k, y, sn_q, m0; // add m0, MAL200225
	Vector3 vel; // declare vel here, MAL200225
	// add if and else if to handle both particles and fluid incident species, MAL200225
	if(p)
	{ vel = p ->get_v(i); E = p->get_energy_eV(i); m0 = p->get_m0(); }
	else if (ispecies) { vel = v_incident; E = ispecies->get_energy_eV(v_incident); m0 = ispecies->get_m();}
	else terminate_run("either p or ispecies must be specified in Secondary::number");

	switch (get_method())
	{
	case BASIC:
		return secondary;
		break;
	case NEUTRALAR: //Added by JTG 06/12/19
		// For neutral Ar on a dirty surface
		// Eq. (B17) Phelps et al, PSST 8(3) (1999) R21
//		E = p->get_energy_eV(i); // MAL200225
		secondary=  0.0001*pow((E-90),1.2)/(1+pow((E/8000),1.5)) + 0.00007*pow((E-32),1.2)/(1+pow((E/2800),1.5));
		if (E<90)
		  {secondary=0.00007*pow((E-32),1.2)/(1+pow((E/2800),1.5));}
				if (E<32)
		{secondary=0.0;}
		return secondary;
		break;
	case DAR: //Added by JTG 06/12/19
		// For Ar ion on a dirty surface: DAR refers to dirty surface bombarded  by argon ion
		// Use correction given in Phelps et al, PSST 8 (1999) B1
//		E = p->get_energy_eV(i); // MAL200225
		secondary= (0.006*E/(1+(E/10))) + 0.000105*pow((E-80),1.2)/pow((1+E/8000),1.5);
		if (E<80)
		{secondary=0.006*E/(1+(E/10));}
		return secondary;
		break;
	case NEUTRALO: //Added by HH 01/10/15
		// For neutral O
		// Hannesdottir and Gudmundsson, PSST 25(5) (2016) 055002
//		E = p->get_energy_eV(i); // MAL200225
		E=log(2*E);
		secondary=1/2*exp(0.139551922739642*pow(E,3)-2.903151232578513*pow(E,2)+21.126692604667177*E-53.398134458366961);
		if (secondary<1e-14)
		{secondary=0;}
		return secondary;
		break;
	case NEUTRALO2: //Added by HH 01/10/15
		// For neutral O2
		// Hannesdottir and Gudmundsson, PSST 25(5) (2016) 055002
//		E = p->get_energy_eV(i); // MAL200225
		E=log(E);
		secondary=exp(0.139551922739642*pow(E,3)-2.903151232578513*pow(E,2)+21.126692604667177*E-53.398134458366961);
		if (secondary<1e-14){
		  secondary=0;}
		return secondary;
		break;
	case OXO: //Added by HH 01/10/15
		// For O+
		// Hannesdottir and Gudmundsson, PSST 25(5) (2016) 055002
//		E = p->get_energy_eV(i); // MAL200225
		if (E>250){
//      secondary=1/2*(2.936535345122867e-05*sqrt(2*E*PROTON_CHARGE/p->get_m0())-8.829449251629568e-01-1.318138751561906e-01)+1.318138751561906e-01; // MAL200225
      secondary=1/2*(2.936535345122867e-05*sqrt(2*E*PROTON_CHARGE/m0)-8.829449251629568e-01-1.318138751561906e-01)+1.318138751561906e-01;
		}
		else if (E<=(106.05/2)){
			secondary=1.318138751561906e-01;
		}
		else{
			secondary = 1/2*(-1.143206617232685e-09*pow(2*E,3)+1.931729502724217e-06*pow(2*E,2)+ 7.058712041143481e-04*2*E+3.659096821574526e-02-1.318138751561906e-01)+1.318138751561906e-01;
		}
		if (secondary<1e-10){
			secondary=0;
		}
		return secondary;
		break;
	case OXO2: //Added by HH 01/10/15
		// For O2+
		// Hannesdottir and Gudmundsson, PSST 25(5) (2016) 055002
//		E = p->get_energy_eV(i); // MAL200225
		if (E>500){
//      secondary=2.936535345122867e-05*sqrt(2*E*PROTON_CHARGE/p->get_m0())-8.829449251629568e-01; // MAL200225
			secondary=2.936535345122867e-05*sqrt(2*E*PROTON_CHARGE/m0)-8.829449251629568e-01;
		}
		else if (E<=106.05){
			secondary=1.318138751561906e-01;
		}
		else{
			secondary = -1.143206617232685e-09*pow(E,3)+1.931729502724217e-06*pow(E,2)+ 7.058712041143481e-04*E+3.659096821574526e-02;
		}
		if (secondary<1e-10){
			secondary=0;
		}
		return secondary;
		break;
	case BRUINING:
		break;
	case VAUGHAN: case VAUGHAN_VERTICAL_EMISSION:
		if (secondary <= 0) return 0;
		theta = acos(-(normal_vector*vel.unit()));
		Emax = energy_max * (1+ 0.5 *ks*theta*theta/PI);
		//    w = (p->get_energy_eV(i) - threshold)/(Emax-threshold); // MAL200225
		w = (E - threshold)/(Emax-threshold); // MAL200225
		k = (w > 1) ? 0.25 : 0.56;
		if (w < 3.6)
			delta_ratio = pow ( w * exp(1-w),k);
		else
			delta_ratio = 1.125/pow(w, Scalar(0.35));
		return secondary * (1 + 0.5*ks*theta*theta/PI)*delta_ratio;
		break;
	case YAMAMURA:
		if (secondary <= 0) return 0;
		costheta = -(normal_vector*vel.unit());
		if (costheta == 0) return 0;
		return secondary*exp(angular_slope*(1-(1/costheta))*sin(PI*(0.5 -(pic_angle/180) )))/pow(costheta, angular_slope);
		break;
	case ROTH:
		// Chemical sputtering from deuterium impact only, without influence of hydrogenation time.
		// J. Nucl. Materials 266-269 (1999) 51-57.
		// Etf = 447 eV, Eth = 27 eV,  Q = 0.1, D = 125. Manageable thresholds: Edes, Edam
//		E = p->get_energy_eV(i); // MAL200225
		y = secondary;
		if (E > Edes)
		{
			w = E/447.;
			sn_q = 0.05*log(1+1.2288*w)/(w + 0.1728*sqrt(w) + 0.008*pow(w, 0.1504));
			if (E > Edam) { k = Edam/E; y *= 1. + 125*sn_q*(1 - pow(k, 2./3.))*pow((1-k), 2.);}
			k = Edes/E;
			y += csp*sn_q*(1. - pow(k, 2./3.))*pow((1-k), 2.)/(1.0 + exp((E-65)/40) );
		}
		return y;
	case ERROR:
		return 0.;
	}

	// by default, return none, JH, Aug. 22, 2005
	return 0.;
}

// mindgame: 'dir' is the vel direction of ejected particles from the boundary
//         , not impacting ones.  called by Secondary::createSecondaries, MAL 9/15/09
void Secondary::createSecondary(ParticleGroup *p, int i, Scalar frac, 
		Direction dir, Vector3 v_incident) // add v_incident as arg for incident fluid species, MAL200225
{

	Scalar xtmp;
	if (dir == NEGATIVE)
	{
		xtmp = Scalar(theboundary -> get_j0());
	}
	else if (dir == POSITIVE)
	{
		xtmp = Scalar(theboundary -> get_j1());
	}
	//	printf("\ndir=%i, xtmp=%g", dir, xtmp);
	Scalar sintheta = frand();
	Scalar costheta = sqrt(1 - sintheta*sintheta);
	Scalar phi = 2 * PI * frand();

	Vector3 v = costheta * normal_vector + sintheta*cos(phi)*tan;
	v.set_e3(sin(phi)*sintheta);

	if ( p && !constant_np2c) np2c0 = p->get_w(i); // only for incident particle case, add if (p), MAL200225

	Scalar R;
	switch (get_method())
	{
	case ERROR:
		break;
	case BASIC: case NEUTRALAR: case DAR: case NEUTRALO: case NEUTRALO2: case OXO: case OXO2: // Edit HH 07/15/15 JTG 06/12/19
		v *= frand()*v_emit; // MKS Velocity
		emit_secondary_product(xtmp, v, frac, dir); // add for particle and fluid boundary interaction, MAL200222
/*		if (secSpecies->get_q() != 0 || 0.5*v.magnitude_squared()*secSpecies->get_m()/PROTON_CHARGE > secSpecies->get_energyThrToAddAsPIC()) // add, MAL 7/7/13
		{
			semitter -> emit(xtmp, v, frac, dt, dir, np2c0);
		} // emit() is in emitter.cpp, MAL 9/16/09
 */
		break;
	case BRUINING:
		break;
	case VAUGHAN: case YAMAMURA: case ROTH:
		R = frand();
		if (R < f_ref + f_sca )
		{
			// mindgame: this vel should be in units of MKS.
			//          since the argument of emit() is assumed so.
			if (p) v = p -> get_vMKS(i); // incident particle case, MAL200225
			else v = v_incident; // incident fluid case, MAL200225
			v.set_e1(-v.e1());

			if ( R > f_ref )
			{
				// mindgame: This below seems to be mistake.
				// v *= frand()*v.magnitude();
				v *= frand();
			}
			//  mindgame: Even in case of reflection,
			//         emit() is necessary because all incident particles will
			//         be absorbed in class 'boundary'.
			emit_secondary_product(xtmp, v, frac, dir); // add for particle and fluid boundary interaction, MAL200222
/*
			if (secSpecies->get_q() != 0 || 0.5*v.magnitude_squared()*secSpecies->get_m()/PROTON_CHARGE > secSpecies->get_energyThrToAddAsPIC()) // add, MAL 7/7/13
			{
				semitter -> emit(xtmp, v, frac, dt, dir, np2c0);
			}
 */
		}
		else
		{
			v = dist -> get_U(dir);
			emit_secondary_product(xtmp, v, frac, dir); // add for particle and fluid boundary interaction, MAL200222
/*
			if (secSpecies->get_q() != 0 || 0.5*v.magnitude_squared()*secSpecies->get_m()/PROTON_CHARGE > secSpecies->get_energyThrToAddAsPIC()) // add, MAL 7/7/13
			{
				semitter -> emit(xtmp, v, frac, dt, dir, np2c0);
			}
 */
		}
		break;
	case VAUGHAN_VERTICAL_EMISSION:
		// Vaughan model with vertical emission
		v = v_emit * normal_vector;			// velocity normal to the surface
		emit_secondary_product(xtmp, v, frac, dir); // add for particle and fluid boundary interaction, MAL200222
//		semitter -> emit(xtmp, v, frac, dt, dir, np2c0); // emit() is in emitter.cpp, MAL 9/16/09
		break;
	}
}

// emit secondary product (particles or fluid) into simulation region, MAL200222
void Secondary::emit_secondary_product(Scalar xtmp, Vector3 v, Scalar frac, Direction dir)
{
	if (secSpecies->get_q() != 0 || 0.5*v.magnitude_squared()*secSpecies->get_m()/PROTON_CHARGE > secSpecies->get_energyThrToAddAsPIC()) // add, MAL 7/7/13
	{
		semitter -> emit(xtmp, v, frac, dt, dir, np2c0);
	} // emit() is in emitter.cpp, MAL 9/16/09
	else // emit into fluid, MAL200222
	{
		oopicList<Fluid> *flist = secSpecies->get_fluidList();
		int nFluids = flist->nItems();
		if (nFluids)
		{
			// there are fluids
			oopicListIter<Fluid> fIter(*flist);
			for (fIter.restart(); !fIter.Done(); fIter ++)
			{
				// A work in progress; add density to the first evolving fluid encountered
				// There will usually be only one such fluid
				// To be done: if more than one evolving fluid, then choose the one best matching vel?
				{
					if (fIter()->get_evolvefluid())
					{
						fIter()->add_or_subtract_density(np2c0, xtmp);
						break;
					}
				}
			}
		}
	}
}

// particles incident on the boundary, called by secondary_emission in boundary.cpp, MAL 9/16/09
int Secondary::createSecondaries(ParticleGroup *p, int i, Scalar frac, 
		Direction dir)
{
	for(ispecies_iter.restart(); !ispecies_iter.Done(); ispecies_iter++)
	{
		if (p->get_species() == ispecies_iter())  break;
	}
	if (ispecies_iter.Done()) return 0;
	if (p->get_energy_eV(i) < threshold) return 0;

	///////////////////////////////////////////////////////
	//normal_vector is normal of surface
	//t is the tangent of surface
	if (p->get_v(i).e1()<0.)
	{
		normal_vector.set_e1(1.);
	}
	else
	{
		normal_vector.set_e1(-1.);
	}

	tan.set_e2(1.);
	/////////////////////////////////////////////////////////

	Scalar yield = number(p,i);
  
	if (constant_np2c) yield *= p->get_w(i)/np2c0;
  
#ifdef DEBUG
	Scalar yield0 = yield; // for debug printf below, MAL200314
#endif
  
	int n = (int)yield;
	yield -= n;
	if (frand() <= yield) n++;
  
#ifdef DEBUG
  if (n) printf("\nSecondary::createSecondaries: isp=%s, iwt=%g, secsp=%s, secwt=%g, yield=%g, nsec=%i",ispecies_iter()->get_name().c_str(), ispecies_iter()->get_np2c(), secSpecies->get_name().c_str(), np2c0, yield0, n); // for debug, MAL200313
#endif

	for (int j=0; j<n ; j++) createSecondary(p,i,frac,dir);
	return n;
}

// evolving fluid incident on the boundary, called by Boundary::evolvingfluid_secondaryemission, MAL 9/16/09
// num_incident is the number of computer particles incident on the boundary
int Secondary::evolvingfluid_createSecondaries(Species * ispecies, Scalar num_incident, VDistribution *v, Direction dir)
{
//  printf("\nSecondary::evolvingfluid_createSecondaries: ispecies=%s",ispecies->get_name().c_str());
	for(ispecies_iter.restart(); !ispecies_iter.Done(); ispecies_iter++)
	{
//    printf("\nSecondary::evolvingfluid_createSecondaries: ispecies_iter()=%s",ispecies_iter()->get_name().c_str());
		if (ispecies->get_ident() == ispecies_iter()->get_ident())  break; // needs get_ident() here!!! MAL200225
	}
//  printf("\nSecondary::evolvingfluid_createSecondaries: ispecies_iter.Done()=%i",ispecies_iter.Done());
	if (ispecies_iter.Done()) return 0;
	// dir is the normal vector direction of the boundary, pointing in to the plasma
	// for dir=POSITIVE, get a random incident velocity from the distribution with a negative x-component;
	// for dir=NEGATIVE, get a random incident velocity from the distribution with a positive x-component;
	Vector3 v_incident = v->get_V();
	if (dir == POSITIVE) v_incident.set_e1(-fabs(v_incident.e1()));
	else v_incident.set_e1(fabs(v_incident.e1()));
//  printf("\nvx=%g, energy=%g",v_incident.e1(), ispecies->get_energy_eV(v_incident));
	if (ispecies->get_energy_eV(v_incident) < threshold) return 0;

	///////////////////////////////////////////////////////
	//normal_vector is normal of surface
	//t is the tangent of surface
	// if (p->get_v(i).e1()<0.)
	if (dir == POSITIVE)
	{
		normal_vector.set_e1(1.);
	}
	else
	{
		normal_vector.set_e1(-1.);
	}

	tan.set_e2(1.);
	/////////////////////////////////////////////////////////

	//  Scalar yield = number(nullptr,0,ispecies,v_incident); // not correct; needs mult by num_incident, MAL200314
	Scalar yield = num_incident*number(nullptr,0,ispecies,v_incident); // add num_incident*, MAL200315
	if (constant_np2c) yield *= ispecies->get_np2c()/np2c0;
  
#ifdef DEBUG
	Scalar yield0 = yield; // for debug printf below, MAL200314
#endif
  
	int n = (int)yield;
	yield -= n;
	if (frand() <= yield) n++;
  
#ifdef DEBUG
	if (n) printf("\nSecondary::evolvingfluid_createSecondaries: isp=%s, iwt=%g, numincident=%g, secSpecies=%s, secwt=%g, yield=%g, nsec=%i", ispecies->get_name().c_str(), ispecies->get_np2c(), num_incident, secSpecies->get_name().c_str(), np2c0, yield0, n); // for debug, MAL200314
#endif
  
	for (int j=0; j<n ; j++) createSecondary(nullptr,0,frand(),dir,v_incident);
	return n;
}

