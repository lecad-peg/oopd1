// Dielectric class
//
// Original by HyunChul Kim, 2005

#include <stdio.h>

#include<vector>
using namespace std;

#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "utils/ostring.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/particlegrouparray.hpp"
#include "fields/fields.hpp"
#include "main/boundary.hpp"
#include "main/secondary.hpp"
#include "main/drive.hpp"
#include "utils/trimatrix.hpp"
#include "main/grid.hpp"
#include "main/dielectric.hpp"
#include "fields/poissonfield.hpp"

#include "main/boundary_inline.hpp"
#include "main/grid_inline.hpp"
#include "fields/fields_inline.hpp"

Dielectric::Dielectric(Fields *t, int i) : Boundary(t)
{
  rho_old = 0;
  material = DIELECTRIC;
  property = ABSORBED;
  setdefaults();  // from parse
  setparamgroup();
  j0 = j1 = i;
  check();
  init();
}

Dielectric::Dielectric(FILE *fp, 
		     oopicList<Parameter<Scalar> > float_variables,
		     oopicList<Parameter<int> > int_variables, Fields *t) :
  Boundary(t)
{
  rho_old = 0;
  material = DIELECTRIC;
  property = ABSORBED;
  init_parse(fp, float_variables, int_variables);
}

Dielectric::~Dielectric() 
{
  if(rho_old) { delete [] rho_old; }
}


// mindgame: 'dir' is the direction in the boundary, not of impacting particles.
Property Dielectric::particle_BC(ParticleGroup *p, int i, Scalar frac, 
				 Direction dir)
{
  Scalar frac2;

  frac2 = (this->*advance_v_for_synchro)(p, i, frac, dir);

  if(accumulate_particles) { accumulate_particle(p, i, frac2, dir, false); }

  secondary_emission(p, i, frac2, dir);
  radiation(p, i);

#ifdef DEBUG 
  fprintf(stderr, "\nAbsorbed on dielectric in %d direction", dir);
#endif

  absorb(p, i, dir);
  return property;
}

void Dielectric::modify_rho(Scalar *rho, Scalar *space_rho) //not called as of 5/25/10, MAL
{
  PoissonSolver *theps;

  // assume this is called by the Poisson Solver
  theps = (PoissonSolver *)thefield;
  
  if(theps->getlaplaceflag())
    {
      for(int i=0, j=j0; j <= j1; j++, i++)
	{
	  rho_old[i] = rho[j];
	  rho[j] = 0.;
	}
       return;
    }

    if (is_usable(NEGATIVE))
    {
      rho[j0] = 0.5*space_rho[j0]+calculate_Q(NEGATIVE)/(thegrid->getV(j0));
#ifdef DEBUG
      print_msg(" NEGATIVE %e %s", rho[j0] - 0.5*space_rho[j0], (char *)name);
#endif
    }

    if (is_usable(POSITIVE))
    {
      rho[j1] = 0.5*space_rho[j1]+calculate_Q(POSITIVE)/(thegrid->getV(j1));
#ifdef DEBUG
      print_msg(" POSITIVE %e %e %e %s", space_rho[j1], space_rho[j1+1], rho[j1] - 0.5*space_rho[j1], (char *)name);
#endif
    }
}

void Dielectric::restore_rho(Scalar * rho) //not called as of 5/25/10, MAL
{ 
  int i, j;

  for(i=0, j=j0; j <= j1; j++, i++)
    {
      rho[j] = rho_old[i];
    }
}

// For Poisson Solver
void Dielectric::modify_coeffs(PoissonSolver * theps)
{

  if(theps->getlaplaceflag())
    {
      error("Dielectric::modify_coeffs", 
	    "Laplace solution not yet implemented");
    }

  ////////////////////////////////////////
  // add surface charge to rho [rhs] array
  ////////////////////////////////////////

  if (is_usable(POSITIVE))
    {
      theps->add_charge(j1, calculate_Q(POSITIVE));
    }

  if (is_usable(NEGATIVE))
    {
      //      rho[j1] = 0.5*space_rho[j1]+calculate_Q(NEGATIVE)/(thegrid->getV(j1));
      theps->add_charge(j0, calculate_Q(NEGATIVE));
    }

  /////////////////////////////////////////////////
  // modify field solve to solve for Efield at edge
  /////////////////////////////////////////////////
  
  if(thegrid->is_lhs(j0))
    {
#ifdef DEBUG
      fprintf(stderr, "\nlhs: A=%g; D0=%g\n",thegrid->lhs_area(), D0);
#endif

      theps->add_charge(j0, D0*thegrid->lhs_area());
    }

  if(thegrid->is_rhs(j1))
    {
#ifdef DEBUG
      fprintf(stderr, "\nrhs: A=%g; D0=%g\n",thegrid->lhs_area(), D0);
#endif 

      theps->add_charge(j1, D0*thegrid->rhs_area());
    }

}

void Dielectric::setdefaults()
{
  classname = "Dielectric";
  diel_epsilon_r = 1.;
  diel_epsilon = EPSILON0;

  E0 = D0 = 0.;
}

void Dielectric::setparamgroup()
{
  pg.add_fparameter("epsilonr", &diel_epsilon_r, "relative permitivity");
  
  pg.add_fparameter("E0", &E0, "[V/m] electric field (E) outside the system");
  pg.add_fparameter("D0", &D0, 
		    "[C/m^2] electric displacement (D) outside the system");
  
}

void Dielectric::check_dielectric()
{
  if(j1 == -1) 
    { 
      if((thegrid->is_rhs(j0) || thegrid->is_lhs(j0)))
	{
	  j1 = j0;
	}
      else
	{
	  j1 = j0 + 1; 
	  print_msg("WARNING: j1 not specified, using 1 cell (j0+1)");
	}
    }
  
  if(j0 == j1) 
    {
      if(!(thegrid->is_rhs(j0) || thegrid->is_lhs(j0)))
	{
	  error("j0 = j1"); 
	}
    }

  if(D0 != 0.)
    {
      if(E0 != 0.) // check to see if E0 and D0 matched
	{
	  print_msg("Dielectric::WARNING: both E0 and D0 specific, using D0");
	} 
      
      E0 = D0/(diel_epsilon*diel_epsilon_r); // init_dielectric not yet called 
    }
  else
    {
      D0 = E0*diel_epsilon*diel_epsilon_r; // init_dielectric not yet called
    }
}

void Dielectric::init_dielectric()
{
  surface_npoints = NONE; // NONE == 2
  npoints = j1 - j0 + 1;

  diel_epsilon *= diel_epsilon_r;

  rho_old = new Scalar[npoints];
}

void Dielectric::check()
{
  // mindgame: check_dielectric() should be done first.
  check_dielectric();
  check_boundary();
}

void Dielectric::init()
{
  set_msg(classname);
  // mindgame: init_dielectric() should be done first.
  init_dielectric();
  init_boundary();
}

Parse * Dielectric::special(ostring s)
{
  return special_boundary(s);
}
