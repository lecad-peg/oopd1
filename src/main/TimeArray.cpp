#include <stdlib.h>
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VariableArray.hpp"
#include "main/TimeArray.hpp"

TimeArray::TimeArray() : VariableArray(ostring("t"), NSTEPS)
{

}

TimeArray::TimeArray(int n) : VariableArray(ostring("t"), n)
{

}

