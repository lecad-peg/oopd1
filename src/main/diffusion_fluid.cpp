#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/grid.hpp"
#include "fields/fields.hpp"
#include "main/boundary.hpp" // MAL200131
#include "distributions/dist_xv.hpp"
#include "distributions/dist_v.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_time.hpp"
#include "diagnostics/diagnosticcontrol.hpp"
#include "atomic_properties/fluid.hpp"
#include "atomic_properties/diffusion_fluid.hpp"
#include "xgio.h" // add for fluids dump; MAL200228

///////////////////
// ctors and dtors
///////////////////

DiffusionFluid::DiffusionFluid(Species * _thespecies, bool _parse):
  Fluid(_thespecies),
  Parse(_thespecies)
{
  N_tot = 0.;
  if(_parse) { init_parse(); }
  D_coef=0; // initialize pointer, MAL200129
  n_fixed=0; // initialize pointer, MAL200201
  molefrac_old = 0; // MAL200204
}

////////////////////////////////
// virtual functions from Parse
////////////////////////////////

void DiffusionFluid::setdefaults(){
  classname = "DiffusionFluid";
  T = 0.;
  gammaL = 0; // MAL200128
  gammaR = 0; // MAL200128
  flux_out = false; // NOT USED; MAL200303
  evolve_fluid = false; // MAL200127
  leftflux = 0; // Alan Wu add
  rightflux = 0; // Alan Wu add
  diff_vol = 0; // MAL200201
  // subcycle = 1; // Alan Wu add
  subcycle = 100; // used for subcycling fluid-fluid collisions, MAL200220
  fluidname = ""; // MAL200127
  density_diff_flag = false;
  molefrac_diff_flag = false;
  ng = 0;
  dx = 0;
  vbar = 0;
  vonNeumannstability = 0.4; // set to just below the real stability condition of 0.5, MAL200218
}

void DiffusionFluid::setparamgroup(){
  pg.add_fparameter("T", &T, "temperature");
  pg.add_function("Dcoef", "[m^2/s] diffusion coefficient", &Dcoefficient, 1, "x"); // MAL200127
  pg.add_fparameter("gammaL", &gammaL, "absorption coefficient on left boundary"); // MAL200128
  pg.add_fparameter("gammaR", &gammaR, "absorption coefficient on right boundary"); // MAL200128
  pg.add_fparameter("leftflux", &leftflux, "flux from left"); // Alan Wu add
  pg.add_fparameter("rightflux", &rightflux, "flux from right"); // Alan Wu add
//   pg.add_fparameter("subcycle", &subcycle, "update every subcycle timesteps"); // Alan Wu add
  pg.add_iparameter("subcycle", &subcycle, "update every subcycle timesteps"); // adaptively set, MAL200220
  pg.add_fparameter("diff_vol", &diff_vol, "Fuller 1966 diffusion volume for this fluid"); // MAL200201
  pg.add_bflag("outflux", &flux_out, true, "fluxes to the wall should decrease fluid density"); // NOT USED, MAL200303
  pg.add_bflag("evolve_fluid_in_time", &evolve_fluid, true, "evolve fluid in time due to diffusion and collisions"); // MAL200127
  pg.add_function("n", "[m^{-3}] density", &density, 1, "x");
  pg.add_function("source", "[particles/s/m^3]", &source, 2, "x", "t"); // Alan Wu add
  pg.add_string("fluidname", &fluidname, "optional, unless > 1 fluids for the same species"); // MAL200127
}
vector<DiffusionFluid * > DiffusionFluid::get_fixedfluids() const { return fixedfluids; } // MAL200124
DiffusionFluid * DiffusionFluid::get_fixedfluid(int i) const { return fixedfluids[i]; } // MAL200124
void DiffusionFluid::addto_fixedfluids(DiffusionFluid * _pdf) { fixedfluids.push_back(_pdf); } // MAL200124


void DiffusionFluid::init()
{
  if (evolve_fluid && thespecies -> get_name() == "Unit") terminate_run("The Unit species cannot evolve in time");
  if (!evolve_fluid && thespecies->get_name() != "Unit") addto_fixedfluids(this); // add to set static vector
// of fixed fluids, MAL200127; exclude the "Unit" species, which is used only for radiative decay reactions
}

////////////////////////////////
// virtual functions from Fluid
////////////////////////////////

VDistribution * DiffusionFluid::velocity_distribution(int node)
{
  VDistribution *thegaussian;

  thegaussian = new VDistribution(thespecies, thespecies->vthermal(T),
                                  Vector3(0.,0.,0), 
				  false);
  return thegaussian;
}

Vector3 DiffusionFluid::get_v_from_distribution(int node){
  VDistribution thegaussian(thespecies, thespecies->vthermal(T), Vector3(0.,0.,0), false);
  return thegaussian.get_V();
}


Scalar DiffusionFluid::electrostatic_density(Scalar potential){
  if(density.is_constant()) { return density.eval(); }
  error("DiffusionFluid::electrostatic_density", "density is not dependent on potential");
  return 0.;
}

void DiffusionFluid::set_thefield(Fields * _thefield){
  Fluid::set_thefield(_thefield);
  Grid * thegrid = thefield->get_theGrid();
  ng = thegrid -> getng();
//  Vector3 v1 = get_v_from_distribution(0); // for test, MAL200205
//  Vector3 v2 = get_v_from_distribution(0); // for test, MAL200205
//  printf("\nv1x=%g, v1y=%g, v1z=%g, v2x=%g, v2y=%g, v2z=%g\n",v1.e1(), v1.e2(), v1.e3(), v2.e1(), v2.e2(), v2.e3()); // for test, MAL200205
  // printf("\ndiff_vol = %g\n",diff_vol); // for debug, MAL200201
  // printf("\nDcoefficient=%s, isconst%i\n",Dcoefficient.get_full_name().c_str(), Dcoefficient.is_constant()); // for debug, MAL200201
  D_coef = new Scalar [ng]; // MAL200129
  n_fixed = new Scalar [ng]; // MAL200129
  molefrac_old = new Scalar [ng]; // MAL200204
  for (int i=0; i<ng; i++)
  {
    n_fluid[i] = density.eval(thegrid->getX(i));
    n_source[i] = source.eval(thegrid->getX(i));
    n_fluid_temp[i] = 0; // MAL200301
    n_fluid_ave[i] = 0; // MAL200301
//    n_fluid_old[i] = 0; // NO!  MAKES n_fluid=0 every other timestep
    n_fluid_old[i] = n_fluid[i]; // fix bug that makes n_fluid=0 every other timestep, MAL 9/27/12
    N_tot = thegrid->integrate(n_fluid);
  }
  if (evolve_fluid) // MAL200202
  {
    is_evolve_fluid_in_time_implemented(); // test validity, grid must be uniform MAL200131
    dx = thegrid -> getdx();
    vbar = sqrt(8/PI)*thespecies -> vt(T);
    if (diff_vol > 0 && Dcoefficient.get_full_name() != "")
      terminate_run("Dcoef and diff_vol cannot both be defined in the diffusion fluid");
    else if (diff_vol == 0 && Dcoefficient.get_full_name() != "")
      density_diff_flag = set_density_diff();
    else if (diff_vol > 0 && Dcoefficient.get_full_name() == "")
      molefrac_diff_flag = set_molefrac_diff();
//    printf("\n\nDiffusionFluid::set_thefield, timestep=%li, evolving fluid=%s(%s), get_Ntot=%g particles\n", thefield->get_nsteps(), thespecies->get_name().c_str(), get_fluidname().c_str(), get_Ntot());
  }
  
  if(debug()) {  fprintf(stderr, "\nN_tot = %g\n", N_tot); }

  DiagnosticControl dc;
  // mindgame: print species_name in the diagnostics
  ostring speciesname = thespecies->get_name()+" Fluid";
  // > 1 fluids for the same species must have distinct names in the diagnostics name list, MAL200127
  if (fluidname != "") speciesname = speciesname + "(" + fluidname + ")"; // MAL200127
  dc.add_gridarray(speciesname + " n", n_fluid);
  if (evolve_fluid) dc.add_gridarray(speciesname + "Ave n", n_fluid_ave); // MAL200301
  if (evolve_fluid) dc.add_timediagnostic(&N_tot, speciesname + " Ntot"); // add, MAL200219
  ostring speciessource = thespecies->get_name()+" Source"; // Alan Wu add
  if (fluidname != "") speciessource = speciessource + "(" + fluidname + ") n"; // MAL200127
  dc.add_gridarray(speciessource, n_source);
}

// test whether evolve_fluid_in_time is implemented for the input grid and boundary configuration, MAL200131
void DiffusionFluid::is_evolve_fluid_in_time_implemented()
{
  Grid * thegrid = thefield->get_theGrid();
  if (!thegrid->isuniform()) {terminate_run("evolve_fluid_in_time not implemented for nonuniform grids");}
  if (thegrid->geometry() != PLANAR) {terminate_run("evolve_fluid_in_time not implemented for cylindrical or spherical grids");}
  bool conductorL = false; bool conductorR = false; bool emptycentralboundaries = true;
  oopicListIter<Boundary> thei(*(thefield->get_boundarylist()));
  for(thei.restart(); !thei.Done(); thei++)
  {
    if (thei()->get_j0() == 0 && thei()->get_j1() == 0 && thei()->get_material() == CONDUCTOR) {conductorL = true; continue;}
    if (thei()->get_j0() == ng-1 && thei()->get_j1() == ng-1 && thei()->get_material() == CONDUCTOR) {conductorR = true; continue;}
    if (thei()->get_material() != EMPTY) {emptycentralboundaries = false; break;}
  }
  if (!(conductorL && conductorR && emptycentralboundaries)) {terminate_run("evolve_fluid_in_time not implemented unless boundaries j0=j1==0 and j0=j1=ng-1 are conductors, and all central boundaries are empty" );}
}

bool DiffusionFluid::set_density_diff()
{
  Grid * thegrid = thefield->get_theGrid();
  bool nonzeroflag = false;
  Scalar maxD = 0;
  for (int i=0; i<ng; i++)
  {
    D_coef[i] = Dcoefficient.eval(thegrid->getX(i)); // the spatially-varying case, e.g., Dcoef(x) = fct(x), MAL200127
    if (D_coef[i] > 0) { maxD = MAX(maxD,D_coef[i]); nonzeroflag = true; }
  }
  if (nonzeroflag)
  {
    vonNeumannstability = maxD*thefield->get_dt()/(dx*dx);
    int nsubsteps = (int)(vonNeumannstability/0.4) + 1;
    if(nsubsteps > 100) { printf("\nvonNeumannstability parameter for fluid %s(%s)=%g, and nsubsteps=%i > 100\n", thespecies->get_name().c_str(), fluidname.c_str(), vonNeumannstability, nsubsteps); terminate_run("terminate run"); }
    if (vonNeumannstability >= 0.8*0.5 && nsubsteps <= 100) // stability condition is really 0.5
      printf("\nvonNeumannstability parameter for fluid %s(%s)=%g; taking nsubsteps=%i in time for advance_density\n", thespecies->get_name().c_str(), fluidname.c_str(), vonNeumannstability, nsubsteps);
    return true;
  }
  return false;
}

bool DiffusionFluid::set_molefrac_diff()
{
  Grid * thegrid = thefield->get_theGrid();
  for (int k=0; k<ng; k++) { n_fixed[k] = 0; D_coef[k] = 0; }
  int Nfixed = fixedfluids.size();
  if (Nfixed <= 0) terminate_run("There must be at least one fixed (nondiffusing) fluid for the Fuller (1966) diffusion calculation");
  for (int i=0; i<Nfixed; i++)
  {
    // Fuller (1966) diffusion coefficient scriptDj for diffusion of the mole fraction
    // chi(x,t) = nj(x,t)/nfixed(x), against a set of fixed fluids of density ni(x).
    // Letting nfixed(x) = sumi ni(x),
    //        scriptDj = nfixed(x)*Dj = 1/[sumi Cij*ni(x)]
    // Here Dj is the ordinary diffusion coefficient (m^2/s) for diffusion of the density nj(x,t).
    // Here scriptDj in 1/m-s, Dj in m2/s, mij in kg, T in eV, diff_vol in cm^3/mole, and nfixed in /m^3
    // The flux Gammaj = - scriptDj*(d/dx)chi(x,t), with flux in #/m^2-s
    // E.N Fuller, P.D. Schettler, and J. Calvin Giddings, Indus.Engin. Chem. vol. 58, no. 5, May 1966, pp. 19-27
    //
    // A similar calculation could be done for the Chapman-Enskog diffusion coefficient,
    // inputting the Lennard-Jones parameters for each species. MAL200204
    Scalar mij = (fixedfluids[i]->get_thespecies()->get_m() * get_thespecies()->get_m())/(fixedfluids[i]->get_thespecies()->get_m() + get_thespecies()->get_m());
    Scalar numer = (pow(fixedfluids[i]->get_diffvol(),ONETHIRD) + pow(diff_vol,ONETHIRD)) *
    (pow(fixedfluids[i]->get_diffvol(),ONETHIRD) + pow(diff_vol,ONETHIRD));
    Scalar denom = pow(0.5*(fixedfluids[i]->get_T() + T),0.75);
    Scalar Cij = 2.9906E-11*numer*sqrt(mij)/denom; // units are m-s
//    printf("\nmij=%g, numer=%g, denom=%g, Cij=%g\n",mij,numer,denom,Cij); // debug, MAL200204
    for (int k=0; k<ng; k++)
    {
      D_coef[k] += Cij*fixedfluids[i]->density.eval(thegrid->getX(k)); // really the intermediate result 1/Dij = Cij*ni (s/m^2); see below
      n_fixed[k] += fixedfluids[i]->density.eval(thegrid->getX(k));
    }
  }
//  printf("\nn_fixed[0]=%g, D_coef[0]=%g\n", n_fixed[0], D_coef[0]); // debug, MAL200204
  Scalar maxD = 0.; // accumulate maximum of density diffusion coef Dj
  for (int k=0; k<ng; k++)
  {
    if (n_fixed[k] <= 0) terminate_run("n_fixed must be > 0 at all grid points for the Fuller (1966) diffusion calculation");
    maxD = MAX(maxD, 1/D_coef[k]);
    D_coef[k] = n_fixed[k]/D_coef[k]; // this is scriptDj [1/m-s] for the mole fraction nj/n_fixed
  }
//  printf("\nn_fixed[0]=%g, scriptD[0]=%g /m-s, D[0]=%g m^2/s\n", n_fixed[0], D_coef[0], D_coef[0]/n_fixed[0]); // debug, MAL200204
  vonNeumannstability = maxD*thefield->get_dt()/(dx*dx);
  int nsubsteps = (int)(vonNeumannstability/0.4) + 1;
  if(nsubsteps > 100) { printf("\nvonNeumannstability parameter for fluid %s(%s)=%g, and nsubsteps=%i > 100\n", thespecies->get_name().c_str(), fluidname.c_str(), vonNeumannstability, nsubsteps); terminate_run("terminate run"); }
  if (vonNeumannstability >= 0.8*0.5 && nsubsteps <= 100) // stability condition is really 0.5
    printf("\nvonNeumannstability parameter for fluid %s(%s)=%g; taking nsubsteps=%i in time for advance_molefrac\n", thespecies->get_name().c_str(), fluidname.c_str(), vonNeumannstability, nsubsteps);
  return true;
}

void DiffusionFluid::advance_density(Scalar _dt)
{
  for (int i=0; i<ng; i++) n_fluid_old[i] = n_fluid[i]; // Add: copy existing n_fluid into n_fluid_old, MAL200208
  for(int i=1; i<ng-1; i++)
  {
    // Use explicit forward time, center space (FTCS) scheme, since dt must be very small anyway, with radian plasma frequency omega_pe*dt < 0.2; MAL200128
    // Von Neuman stability condition for FTCS is: (1/8)*(D[i+1]-D[i-1])^2*(dt)^2/(dx)^4 < D[i]*dt/(dx)^2 < (1/2)
    // References: M. Dehghan, Appl. Math. Comput. vol 147, pp. 307-319 (2004), but note misprint in Eq (16): i-1 -> i+1 in the first term on the rhs
    // References: Abolfazl Mohammadi, M. Manteghian, and Ali Mohammadi, Aust. J. Basic Appl. Sci. vol 5, 1536-1543 (2011)
    n_fluid[i] = MAX(0, n_fluid_old[i] + (_dt/(dx*dx)) * (D_coef[i]*(n_fluid_old[i-1]-2*n_fluid_old[i]+n_fluid_old[i+1]) + 0.25*(D_coef[i+1]-D_coef[i-1])*(n_fluid_old[i+1]-n_fluid_old[i-1])) + n_source[i]*_dt); // MAL200128; keep positive, MAL200211
    Scalar boundaryL = dx*gammaL*vbar/(2*(2-gammaL)/D_coef[0]);
    Scalar boundaryR = dx*gammaR*vbar/(2*(2-gammaR)/D_coef[ng-1]);
    //    printf("\nD_coef[0]=%g, Dcoef[ng/2]=%g, D_coef[ng-1]=%g",D_coef[0], D_coef[ng/2], D_coef[ng-1]); // for debug, MAL200128
    n_fluid[0] = MAX(0, (n_fluid[1]+leftflux*dx/D_coef[0])/(1+boundaryL));
    n_fluid[ng-1] = MAX(0, (n_fluid[ng-2] + rightflux*dx/D_coef[ng-1])/(1+boundaryR));
  }
}

void DiffusionFluid::advance_molefrac(Scalar _dt)
{
//  for (int i=0; i<ng; i++) { molefrac_old[i] = n_fluid_old[i]/n_fixed[i]; } // must copy n_fluid into n_fluid_old
  for (int i=0; i<ng; i++) { n_fluid_old[i] = n_fluid[i]; molefrac_old[i] = n_fluid_old[i]/n_fixed[i]; }

  for(int i=1; i<ng-1; i++)
  {
    // Use explicit forward time, center space (FTCS) scheme, since dt must be very small anyway, with radian plasma frequency omega_pe*dt < 0.2; MAL200128
    // Von Neuman stability condition for FTCS is: (1/8)*(D[i+1]-D[i-1])^2*(dt)^2/(dx)^4 < D[i]*dt/(dx)^2 < (1/2)
    // References: M. Dehghan, Appl. Math. Comput. vol 147, pp. 307-319 (2004), but note misprint in Eq (16): i-1 -> i+1 in the first term on the rhs
    // References: Abolfazl Mohammadi, M. Manteghian, and Ali Mohammadi, Aust. J. Basic Appl. Sci. vol 5, 1536-1543 (2011)
    n_fluid[i] = MAX(0, n_fluid_old[i] + (_dt/(dx*dx)) * (D_coef[i]*(molefrac_old[i-1]-2*molefrac_old[i]+molefrac_old[i+1]) + 0.25*(D_coef[i+1]-D_coef[i-1])*(molefrac_old[i+1]-molefrac_old[i-1])) + n_source[i]*_dt); // MAL200128
    Scalar boundaryL = dx*gammaL*vbar*n_fixed[0]/(2*(2-gammaL)*D_coef[0]);
    Scalar boundaryR = dx*gammaR*vbar*n_fixed[ng-1]/(2*(2-gammaR)*D_coef[ng-1]);
    //    printf("\nD_coef[0]=%g, Dcoef[ng/2]=%g, D_coef[ng-1]=%g",D_coef[0], D_coef[ng/2], D_coef[ng-1]); // for debug, MAL200128
    n_fluid[0] = MAX(0, (n_fluid[1]*n_fixed[0]/n_fixed[1]+leftflux*dx*n_fixed[0]/D_coef[0])/(1+boundaryL));
    n_fluid[ng-1] = MAX(0, (n_fluid[ng-2]*n_fixed[ng-1]/n_fixed[ng-2] + rightflux*dx*n_fixed[ng-1]/D_coef[ng-1])/(1+boundaryR));
  }
}
void DiffusionFluid::update_N_tot() // MAL200212
{
//  printf("\nDiffusionFluid::update_N_tot: nsteps=%li, N_tot=%g",thefield->get_nsteps(), N_tot);
  if (thefield->get_nsteps() % subcycle != 0) return;
  if (!evolve_fluid) return;
  Grid * thegrid = thefield->get_theGrid();
  N_tot = 0;
  for (int i=0; i<ng; i++)
    N_tot += n_fluid[i]*thegrid->getV(i);
}

void DiffusionFluid::update(Scalar dt)
{
// Next 4 lines: n_fluid and n_fluid_old exchange addresses every timestep;
// DEBUG n_fluid vs n_fluid_old, MAL200208
  // TOO CLEVER BY HALF; oopicList<Fluid> only picks up the initial address of n_fluid; it does not know
  // about the exchange of addresses
  // SOLUTION: n_fluid into n_fluid_old in advance_density(dt) and advance_molefrac(dt); MAL200208
//  Scalar *temp; // REMOVE
//  temp = n_fluid_old; // REMOVE
//  n_fluid_old = n_fluid; // REMOVE
//  n_fluid = temp; // REMOVE
  
  // printf("\naddr_temp=%p, addr_nfluidold=%p, addr_getnfluidold=%p, addr_nfluid=%p\n", temp, n_fluid_old, get_n_old(), n_fluid );
// Add subcycling for diffusion calculation, MAL200218
  if (vonNeumannstability <= 0.4) // FTCS fluid diffusion mover needs vonNeumannstability < 0.5
  {
    int subcyclediff = MIN((int)(0.4/vonNeumannstability),100); // MAL200303
    Scalar dtsubcycle = subcyclediff*dt;
 //   printf("\nnsteps=%li, subcycle=%i", thefield->get_nsteps(), subcycle); // for debug
    if (thefield->get_nsteps() % subcyclediff == 0 && density_diff_flag) advance_density(dtsubcycle);
    if (thefield->get_nsteps() % subcyclediff == 0 && molefrac_diff_flag) advance_molefrac(dtsubcycle);
  }
  else // reduce dt and take nsubsteps to meet vonNeumann stability condition
  {
    int nsubsteps = (int)(vonNeumannstability/0.4) + 1;
    Scalar dtstep = dt/((float)nsubsteps);
//    printf("\nnsubsteps=%i, dt=%g, dtstep=%g\n",nsubsteps,dt,dtstep);
    for (int i=1; i<=nsubsteps; i++)
    {
      if (density_diff_flag) advance_density(dtstep);
      if (molefrac_diff_flag) advance_molefrac(dtstep);
    }
  }
//  if (evolve_fluid && thefield->get_nsteps() % 10000 == 0)
//    printf("DiffusionFluid::update, timestep=%li, evolving fluid=%s(%s), get_Ntot=%g particles\n", thefield->get_nsteps(), thespecies->get_name().c_str(), get_fluidname().c_str(), get_Ntot());

  //  Scalar tmp = interact_with_boundaries(); NO LONGER USED; MAL200303

  if (evolve_fluid) interact_evolvingfluid_with_boundaries();
  
  if (evolve_fluid && thefield->get_diagupdate()) thefield->grid_time_average(n_fluid, n_fluid_temp, n_fluid_ave); // MAL200301
  
/* Not sure what use this is; MAL200205
 if(flux_out)
  {
    Scalar tmp = interact_with_boundaries();
    tmp = (1. - tmp/N_tot);
    for(int i=0; i < ng; i++)
    {
      n_fluid[i] *= tmp;
    }
  }
*/
  
}

// Used to determine the effect of the evolving fluid outgoing fluxes at the boundaries, on secondary particle/fluid creation
void DiffusionFluid::interact_evolvingfluid_with_boundaries() // Add, MAL200223
{
//  printf("\nDF::interact_evolvingfluid_with_boundaries");
  oopicListIter<Boundary> thei(*(thefield->get_boundarylist()));
  
  VDistribution * thedist = 0;

  for(thei.restart(); !thei.Done(); thei++)    {
    bool thin = true;
    Scalar density = 0.;
    
    thin = (thei()->get_j0() == thei()->get_j1());
    if(thin)  {
      thedist = velocity_distribution(thei()->get_j0());
      density = n_fluid[thei()->get_j0()];
    }
    
    if(thei()->is_usable(NEGATIVE))  {
      if(!thin)   {
        thedist = velocity_distribution(thei()->get_j0());
        density = n_fluid[thei()->get_j0()];
      }
      // Conductor::evolvingfluid_BC, to calculate the outgoing flux at the right hand boundary
      thei()->evolvingfluid_BC(thespecies, density, gammaR, thedist, NEGATIVE);
    }
    
    if(thei()->is_usable(POSITIVE))  {
      if(!thin)  {
        thedist = velocity_distribution(thei()->get_j1());
        density = n_fluid[thei()->get_j1()];
      }
      // Conductor::evolvingfluid_BC, to calculate the outgoing flux at the left hand boundary
      thei()->evolvingfluid_BC(thespecies, density, gammaL, thedist, POSITIVE);
    }
    
    // seeing if density is calculated correctly, JH, June 12, 2006
 #ifdef DEBUG
    fprintf(stderr, "\nname=%s(%s), material=%d, thin=%d, isusableNEG=%d, j0=%d; isusablePOS=%d, j1=%d, density=%g\n",thespecies->get_name().c_str(), get_fluidname().c_str(), thei()->get_material(), thin, thei()->is_usable(NEGATIVE), thei()->get_j0(),thei()->is_usable(POSITIVE), thei()->get_j1(), density); // for debug, MAL200130
 #endif
    
    if(thedist){
      delete thedist;
      thedist = 0;
    }
  }
}

void DiffusionFluid::dump_fluid(FILE * DMPFile) // add for fluids dump; MAL200228
{
  Scalar ftemp;
  XGWrite(&ng,sizeof(int),1,DMPFile,"int"); // write ng for this fluid
  for (int i=0; i<ng; i++)
  {
    ftemp = n_fluid[i];
    XGWrite(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
    //    if (i < 5 || i > ng-5) printf("\nn[i]=%g",ftemp); // for debug
  }
}

void DiffusionFluid::reload_fluid(FILE * DMPFile) // add for fluids reload from dumpfile; MAL200229
{
  int ntemp;
  XGRead(&ntemp,sizeof(int),1,DMPFile,"int"); //read ng
  if (ntemp != ng) terminate_run("DiffusionFluid::reload_fluid: ng from dumpfile is not equal to ng from the input file!");
  Scalar ftemp;
  for (int i=0; i<ng; i++)
  {
    XGRead(&ftemp,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
    if (evolve_fluid) n_fluid[i] = ftemp;
    if (i < 5 || i > ng-5) printf("\nn[i]=%g",ftemp); // for debug
  }
}

void DiffusionFluid::add_or_subtract_density(Scalar num_phys_part, Scalar gridx) // MAL200209
{
  // Linearly weight to grid; num-phys_part can be negative
  Grid * thegrid = thefield->get_theGrid();
  int i = (int) gridx;
  Scalar delta = gridx - i;
    n_fluid[i] = MAX(0, n_fluid[i]+num_phys_part*(1.-delta)/thegrid->getV(i)); // keep positive
    n_fluid[i+1] = MAX(0, n_fluid[i+1]+num_phys_part*delta/thegrid->getV(i)); // keep positive
}

Fluid * DiffusionFluid::get_copy(){
  DiffusionFluid *tdf;

  tdf = new DiffusionFluid(thespecies, false);
  tdf->setdefaults();

  tdf->T = T;
  tdf->Dcoefficient = Dcoefficient; // MAL200127
  tdf->subcycle = subcycle; // MAL200220
  tdf->gammaL = gammaL; // MAL200128
  tdf->gammaR = gammaR; // MAL200128
  tdf->flux_out = flux_out;
  tdf->leftflux = leftflux;
  tdf->rightflux = rightflux;
  tdf->density = density;
  tdf->source = source;
  tdf->diff_vol = diff_vol;
  tdf->set_debug(pg.debug);
  tdf->fluidname = fluidname; // MAL200127
  tdf->evolve_fluid = evolve_fluid; // MAL200127
  tdf->D_coef = D_coef;
  tdf->n_fixed = n_fixed;
  tdf->molefrac_old = molefrac_old; // MAL200204
  tdf->density_diff_flag = density_diff_flag; // MAL200204
  tdf->molefrac_diff_flag = molefrac_diff_flag; // MAL200204
  tdf->ng = ng;
  tdf->dx = dx;
  tdf->vbar = vbar;
  tdf->vonNeumannstability = vonNeumannstability;
  if(thefield) { tdf->set_thefield(thefield); }
  
  return tdf;
}
