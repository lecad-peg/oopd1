#include <stdio.h>

#include "main/pd1.hpp"
#include "main/parse.hpp"
#include "utils/ovector.hpp"
#include "main/species.hpp"
#include "utils/ostring.hpp"
#include "main/boundary.hpp"
#include "main/grid.hpp"
#include "main/particlegroup.hpp"
#include "main/grid_inline.hpp"
#include "fields/fields.hpp"
#include "fields/fields_inline.hpp"
#include "distributions/dist_xv.hpp"
#include "distributions/dist_x.hpp"
#include "distributions/dist_v.hpp"

#include "main/load.hpp"
#include "xgio.h" // for access to XGRead() for reload, MAL 5/14/10
#include "xgrafix.h" // for access to WasDumpFileGiven variable, MAL 6/11/10

Load::Load(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
	   oopicList<Parameter<int> > int_variables, Grid *t,
	   SpeciesList *thespecieslist)
{
  specieslist = thespecieslist;
  thegrid = t;
  init_parse(fp, float_variables, int_variables);      
}

Load::~Load()
{
  if (xdist) delete xdist;
  if (vdist) delete vdist;
}

void Load::setdefaults()
{
  classname = "Load";
  n = 0.0;
  x0 = x1 = -1.E38;
  np2c = -1;
  speciesname = "";
  T_load=0.;
  v0_x=v0_y=v0_z=0;
  vt_x=vt_y=vt_z=0;
  vcu_x=vcu_y=vcu_z=0;
  vcl_x=vcl_y=vcl_z=0;
  vload_type = RANDOM;
  xload_type = UNIFORM;
  species = NULL;
  xdist = NULL;
  vdist = NULL;
  density_func.setconstant(1.0);
}

void Load::setparamgroup()
{
  pg.add_fparameter("n", &n, "[m^{-3}] initial density averaged over given volume");
  pg.add_fparameter("np2c", &np2c);
  pg.add_fparameter("T_load",&T_load, "[V] initial effective temperature");
  pg.add_fparameter("x0", &x0, "[m] position of left boundary (for planar geometry)");
  pg.add_fparameter("r0", &x0, "[m] position of left boundary (for non-planar geometry)");
  pg.add_fparameter("x1", &x1, "[m] position of right boundary (for planar geometry)");
  pg.add_fparameter("r1", &x1, "[m] position of right boundary (for non-planar geometry)");
  pg.add_fparameter("v0_x", &v0_x, "[m/s] 1st component of initial drift velocity");
  pg.add_fparameter("v0_y", &v0_y, "[m/s] 2nd component of initial drift velocity");
  pg.add_fparameter("v0_z", &v0_z, "[m/s] 3rd component of initial drift velocity");
  pg.add_fparameter("vt_x", &vt_x, "[m/s] 1st component of initial thermal velocity");
  pg.add_fparameter("vt_y", &vt_y, "[m/s] 2nd component of initial thermal velocity");
  pg.add_fparameter("vt_z", &vt_z, "[m/s] 3rd component of initial thermal velocity");
  pg.add_fparameter("vcu_x", &vcu_x, "[m/s] 1st component of initial upper cutoff velocity");
  pg.add_fparameter("vcu_y", &vcu_y, "[m/s] 2nd component of initial upper cutoff velocity");
  pg.add_fparameter("vcu_z", &vcu_z, "[m/s] 3rd component of initial upper cutoff velocity");
  pg.add_fparameter("vcl_x", &vcl_x, "[m/s] 1st component of initial lower cutoff velocity");
  pg.add_fparameter("vcl_y", &vcl_y, "[m/s] 2nd component of initial lower cutoff velocity");
  pg.add_fparameter("vcl_z", &vcl_z, "[m/s] 3rd component of initial lower cutoff velocity");
  pg.add_string("species", &speciesname, "species name to be loaded");
  pg.add_flag("VUniformStart_NO_EP", &vload_type, UNIFORM_NO_EP);
  pg.add_flag("VUniformStart", &vload_type, UNIFORM);
  pg.add_flag("VRandomStart", &vload_type, RANDOM);
  pg.add_flag("VQuietStart", &vload_type, QUIET);
  pg.add_flag("XRandomStart", &xload_type, RANDOM);
  pg.add_flag("XUniformStart", &xload_type, UNIFORM);
  pg.add_flag("XUniformStart_NO_EP", &xload_type, UNIFORM_NO_EP);
  pg.add_flag("XQuietStart", &xload_type, QUIET);
  pg.add_function("densityfunction", "initial spatial profile of density", &density_func, 1, "x");
}


void Load::check()
{
  char err[STR_SIZE];
  set_msg(classname);

  if((x1 <= x0) && (x1 > -1.E37)) 
    { 
      sprintf(err, "x1 <= x0 (x1=%g, x0=%g)", x1, x0);
      terminate_run(err); 
    }
  if(x0 >= thegrid->getx1()) { terminate_run("x0 >= Grid->getx1()"); }
  if(x1 > -1.E37)
    {
      if(x1 <= thegrid->getx0())
	{
	  terminate_run("x1 <= Grid->getx0()");
	}
    }
  if (T_load && (vt_x || vt_y || vt_z))
    {
      terminate_run("T and VT can not be set at the same time. Choose one of them only!!");

    }

  if (speciesname != "")
  {
    oopicListIter<Species> thei(*specieslist);

    for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_name() == speciesname)
      {
	species = thei();
	break;
      }
    }
  }

#ifdef DEBUG
  print_msg(MSG_LOW, "%s", speciesname());
#endif
  if(species == NULL) { terminate_run("species not found"); }
  if (np2c <= 0.) np2c = species->get_np2c();
  if (np2c <= 0.)
  {
    error("np2c must be specified in either Load or Species!");
  }
}

void Load::init()
{
  if(n <= 0.) { n = -1.; }
  if(x0 < thegrid->getx0()) { x0 = thegrid->getx0(); }
  if((x1 > thegrid->getx1()) || (x1 < -1.E37))
    { x1 = thegrid->getx1(); }

  // mindgame : +0.5
	if (!WasDumpFileGiven)
	{
		np = int((n*thegrid->getV(x0, x1))/np2c+0.5);
	}
//	printf("\nFrom Load::init(), WasDumpFileGiven=%i, np=%i\n",WasDumpFileGiven,np);

  if (np > 0)
  {
//    print_msg("Position ");
    switch (xload_type)
    {
      case QUIET:
				print_msg("Position ");
        print_shr_msg("Quiet start...");
        break;
      case RANDOM:
				print_msg("Position ");
        print_shr_msg("Random start...");
        break;
      case UNIFORM: case UNIFORM_NO_EP:
				print_msg("Position ");
        print_shr_msg("Uniform start...");
        break;
      case FROM_DUMPFILE:
        print_msg("Dumpfile start...");
        break;
      default:
        terminate_run("check: Cannot be reached.");
        break;
    }
//    print_shr_msg(" Velocity ");
    switch (vload_type)
      {
      case QUIET:
    print_shr_msg(" Velocity ");
	print_shr_msg("Quiet start...");
	break;
      case RANDOM:
    print_shr_msg(" Velocity ");
	print_shr_msg("Random start...");
	break;
      case UNIFORM: case UNIFORM_NO_EP:
    print_shr_msg(" Velocity ");
	print_shr_msg("Uniform start...");
	break;
      case FROM_DUMPFILE:
	break;
      default:
	terminate_run("check: Cannot be reached.");
	break;
      }
    init_dist(); // johnv: moved common code for PlasmaSource to init_dist()
  }
}

void Load::init_dist()
{
  if (density_func.is_fparsed() && !density_func.is_constant()){
    xdist=new XDistribution(thegrid, x0, x1, &density_func,(LoadType)xload_type);
  }
  else {
    xdist=new XDistribution(thegrid,x0,x1,(LoadType)xload_type);
  }

  if(T_load)
    {
      // mindgame: do not use get_qm()
      // 0.5 T_x = 0.5 m sqr(vt_x)
      // 1.5 T = Tx + Ty + Tz
      vt_x=vt_y=vt_z=species->vt(T_load);
    }
  vt = Vector3(vt_x,vt_y,vt_z);
  v0 = Vector3(v0_x,v0_y,v0_z);
  vcu = Vector3(vcu_x,vcu_y,vcu_z);
  vcl = Vector3(vcl_x,vcl_y,vcl_z);
  vdist=new VDistribution(species,vt,v0,false,vcl,vcu,(LoadType)vload_type);
}

void Load::load(Scalar dt)
{
  if(np <= 0) { return; }

  print_shr_msg("\n*loading species %s...", species->get_name()());
  Scalar *x = new Scalar[np];  

  xdist->get_X(np,x);
  thegrid->XtogridX(np, x);

  //load velocity distribution
  Vector3 *v = new Vector3 [np];

  vdist->get_U(np,v);

  //--------------------------------------------------
  
  species->add_particles(np, np2c, x, v);
  print_shr_msg("%d particles loaded.", np);

  delete [] x;
  delete [] v;
}

void Load::reload(Scalar dt, int is3v, int npart, FILE * DMPFile)
{
	np = npart;
//	printf("\nLoad::reload, np=%i\n",np);
  if(np <= 0) { return; }

  print_shr_msg("\n*reloading species %s...", species->get_name()());
	if(is3v) species->set_3v_flag();
	Scalar x, v1, v2, v3, w; 
	Scalar x0, x1; // spatial region limits
	x0 = species->get_grid()->getx0();
	x1 = species->get_grid()->getx1();
//	printf("\nLoad::reload(), x0=%g, x1=%g",x0,x1); // debug dumpfile
 	for (int i = 0; i < np; i++)
	{
		XGRead(&x,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
		XGRead(&v1,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
		if(is3v)
		{
			XGRead(&v2,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
			XGRead(&v3,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
		}
		else
		{
			v2 = 0;
			v3 = 0;
		}
		Vector3 v = Vector3(v1,v2,v3);
		XGRead(&w,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
		if (x > x0 && x < x1)  // reload only if within the spatial region
		{
		thegrid->XtogridX(1, &x);
		species->add_particles(1, w, &x, &v);
		}
	}
  print_shr_msg("%d particles loaded.", np);
}

