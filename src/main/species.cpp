#include <stdio.h>
#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "utils/ostring.hpp"
#include "main/parse.hpp"
#include "main/grid.hpp"
#include "fields/fields.hpp"
#include "reactions/reaction.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/particlegrouparray.hpp"
#include "atomic_properties/fluid.hpp"
#include "main/boltz.hpp"
#include "atomic_properties/diffusion_fluid.hpp"
#include "xgio.h" // for access to XGWrite() for dump_particles, MAL 5/12/10

Species::Species()
{
	setdefaults();
	setparamgroup();
}

// minimal data-based constructor
Species::Species(Scalar qq, Scalar mm, Scalar n_np2c, ostring nname)
{
	setdefaults();
	setparamgroup();
	q = qq;
	m = mm;
	np2c = n_np2c;
	name = nname;
	init();
}

Species::Species(Parse *_parent, int i) : Parse(_parent)
{
	ident = i;
	init_parse();
}

Species::Species(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables, int i)
{
	ident = i;
	init_parse(fp, float_variables, int_variables);
}

Species::~Species()
{
	// mindgame: change from removeAll to deleteAll to avoid memory leaks
	particleGroupList.deleteAll();
	fluidList.deleteAll();
	// species_density[4*ng] remains allocated in species.cpp only if d0_flag is true (no species grid diagnostics are calculated or displayed), MAL 10/10/09
	// otherwise, it is allocated and deallocated in diagnosticcontrol.cpp
	if(species_density && get_field()->get_d0flag()) { delete [] species_density; } // holds species_density, species_scratch_density, species_ave_density, and species_temp_density, MAL 10/10/09

	if(moment)
	{
		for(int i=0; i < nmoment; i++)
		{
			delete [] moment[i];
		}
		delete [] moment;
	}

	delete [] species_density_array; // for spatiotemporal diagnostics, HH 04/19/16
	delete [] species_JdotE_array; // for spatiotemporal diagnostics, HH 04/19/16
	delete [] species_Temp_array; // for spatiotemporal diagnostics, HH 04/19/16
}

Species::Species(Species *s)
{
	copy(s);
}

Species::Species(Species &s)
{
	copy(&s);
}

void Species::copy(Species *s)
{
	setdefaults();

	// copy data
	name = s->get_name();
	reaction_name = s->get_reaction_name();
	q = s->get_q();
	m = s->get_m();
	m0c_sq_e = s->get_m0c_sq_e();
	half_m0_e = s->get_half_m0_e();  //this needs here C.H. Lim Feb/2006
	qm = s->get_qm();
	np2c = s->get_np2c();
	subcycle = s->get_subcycle();
	v3_flag = s->is_3v();
	nmax = s->nmax;
	nmult = s->nmult;
	pg_type = s->pg_type;
	variable_weight = s->variable_weight;
	useSecDefaultWeight = s->useSecDefaultWeight; // MAL 10/15/09
	energyThrToAddAsPIC = s->energyThrToAddAsPIC; // MAL 11/21/09
	maxEnergyForSigmaV = s->maxEnergyForSigmaV; //for null collision method in MCC::init_maxSigmaV(), MAL 9/22/12
	ident = s->ident;
	nr_u_limit = s->get_nr_u_limit();
	force_nr_flag = s->is_nr_forced();
	set_nr_limit();
	convert_ostrings_to_speciestypes();

	// add list of Fluids
	oopicList<Fluid> *list_copy = s->get_fluidList();
	oopicListIter<Fluid> thei(*list_copy);
	for(thei.restart(); !thei.Done(); thei++)
	{
		Fluid * copy = thei()->get_copy();
		fluidList.add(copy);
		if(thei()->is_nonlinear()) { nonlinearfluids.add(copy); }
	}
	nonlinearflag = s->is_nonlinear();
}

void Species::setdefaults(void)
{
	// m0c_sq_e and half_m0_e are defined in conv_engy_vel.hpp
	q = m = qm = m0c_sq_e = half_m0_e = 0.;
	atomicnumber = 0;
	subcycle = 1;
	classname = "Species";
	name = "";
	reaction_name = "";
	reaction_type = NO_SPECIES;
	theGrid = 0;
	theField = 0;
	nmax = NMAX;
	np2c = DEFAULT_NP2C;
	variable_weight = false;
	useSecDefaultWeight = false; // MAL 10/15/09
	energyThrToAddAsPIC = 0;  // JTG November 22 2009, default is to always add neutrals created by created by collisions as PIC particles
	maxEnergyForSigmaV = MAX_ENERGY_FOR_SIGMA_V; //for null collision method in MCC::init_maxSigmaV(), MAL 9/22/12
	v3_flag = false;
	pg_type = PARTICLEGROUPARRAYTYPE;
	nmult = 2; // assume particlegroups start with double the
	// # of particles given
	nmoment = 0;
	moment = 0;
	species_density = 0;
	species_scratch_density = 0; // Add for fast calculation of density, MAL 9/12/09
	species_ave_density = 0; // added for grid time averages, MAL 4/25/09
	species_temp_density = 0; // added for grid time averages, MAL 6/12/09
	species_density_array = 0; // Spatiotemporal diagnostics, HH 04/18/16
	species_Temp_array = 0; // Spatiotemporal diagnostics, HH 04/18/16
	species_JdotE_array = 0; // Spatiotemporal diagnostics, HH 04/18/16
	species_flux1 = 0; species_scratch_flux1 = 0; species_ave_flux1 = 0; species_temp_flux1 = 0;
	species_flux2 =	species_scratch_flux2 = species_ave_flux2 = species_temp_flux2 = 0;
	species_flux3 =	species_scratch_flux3 = species_ave_flux3 = species_temp_flux3 = 0;
	species_J1dotE1 = 0; species_ave_J1dotE1 = 0; species_temp_J1dotE1 = 0;
	species_J2dotE2 = species_ave_J2dotE2 = species_temp_J2dotE2 = 0;
	species_J3dotE3 = species_ave_J3dotE3 = species_temp_J3dotE3 = 0;
	species_enden1 = species_scratch_enden1 = species_ave_enden1 = species_temp_enden1 = 0;
	species_enden2 = species_scratch_enden2 = species_ave_enden2 = species_temp_enden2 = 0;
	species_enden3 = species_scratch_enden3 = species_ave_enden3 = species_temp_enden3 = 0;
	species_vel1 = species_ave_vel1 = species_temp_vel1 = 0;
	species_vel2 = species_ave_vel2 = species_temp_vel2 = 0;
	species_vel3 = species_ave_vel3 = species_temp_vel3 = 0;
	species_JdotE = species_ave_JdotE = species_temp_JdotE = 0;
	species_enden = species_ave_enden = species_temp_enden = 0;
	species_T1 = species_ave_T1 = species_temp_T1 = 0;
	species_T2 = species_ave_T2 = species_temp_T2 = 0;
	species_T3 = species_ave_T3 = species_temp_T3 = 0;
	species_Tperp = species_ave_Tperp = species_temp_Tperp = 0;
	species_Temp = species_ave_Temp = species_temp_Temp = 0;
//  printf("\nSpecies::setdefaults: species=%s, Ntot_particles=%g\n",name.c_str(), Ntot_particles);
	T1tot = T2tot = T3tot = Tperptot = Temptot = 0.; // zero temperatures as a function of time, MAL 10/16/12

	accumulate_moment = false;

	force_nr_flag = false;
	nr_u_limit = 1.e8;
	nonlinearflag = false;

	add_child_name("BoltzmannModel");
	add_child_name("DiffusionFluid");
}

void Species::setparamgroup(void)
{
	pg.add_string("name", &name, "species name");
	// mindgame: namelist for reactions
	pg.add_string("reaction_type", &reaction_name, "species type for reaction");
	pg.add_fparameter("q", &q, "[C] charge of a single particle");
	pg.add_fparameter("m", &m, "[kg] mass of a single particle");
	pg.add_fparameter("np2c", &np2c);
	pg.add_fparameter("energyThrToAddAsPIC", &energyThrToAddAsPIC, "[eV] threshold energy for adding (neutral) PIC particles"); // MAL 11/21/09
	pg.add_iparameter("subcycle", &subcycle, "number of steps to subcycle"); 
	pg.add_iparameter("nmax", &nmax);
	pg.add_bflag("useSecDefaultWeight", &useSecDefaultWeight, true, "Create secondary species with default weight"); //local to species, MAL 10/15/09
	pg.add_fparameter("maxEnergyForSigmaV", &maxEnergyForSigmaV, "[eV] maximum energy for calculating max(sigmaV) for null collisions"); //used in MCC::init_maxSigmaV(), MAL 9/22/12
	pg.add_bflag("VariableWeight", &variable_weight, true);
	pg.add_bflag("Force3V", &v3_flag, true, "force the particle velocity to have three components");
	pg.add_bflag("ForceNonRelativistic", &force_nr_flag, true, "force the equation of motion to be solved in a non-relativistic way");
}

void Species::check(void)
{
	if(nmax <= 0) { error("nmax <= 0"); }
	if(np2c <= 0) { error("np2c <= 0"); }  // MAL 11/21/09
	if(m < 0.) { error("m < 0."); }
	if(subcycle <= 0) { error("subcycle <= 0"); }
	if(energyThrToAddAsPIC < 0.) { error("energyThrToAddAsPIC < 0."); }
	if(maxEnergyForSigmaV <= 0.) { error("maxEnergyForSigmaV <= 0."); }
//    printf("\nSpecies::check: species=%s, Ntot_particles=%g\n",name.c_str(), Ntot_particles);
}

void Species::init(void) 
{
	if(m == 0.) qm = 0.; // avoid division by zero for massless particles
	else qm = q/m;
	m0c_sq_e = m*SPEED_OF_LIGHT_SQ/PROTON_CHARGE;
	half_m0_e = 0.5*m/PROTON_CHARGE;
	set_nr_limit();
	convert_ostrings_to_speciestypes();
//  printf("\nSpecies::init: species=%s, Ntot_particles=%g\n",name.c_str(), Ntot_particles);
}

Parse * Species::special(ostring s)
{
	if(s == "BoltzmannModel")
	{
		BoltzmannModel *tbm;

		tbm = new BoltzmannModel(this, true);
		fluidList.add(tbm);
		nonlinearfluids.add(tbm);
		nonlinearflag = true;

		return tbm;
	}
	if(s == "DiffusionFluid")
	{
		DiffusionFluid *tbm;

		tbm = new DiffusionFluid(this, true);
		fluidList.add(tbm);

		return tbm;
	}
	return 0;
}

void Species::set_theGrid(Grid *_theGrid) 
{
	theGrid = _theGrid;

	// allocate species density array
	if(species_density) { delete [] species_density; }
	// Create room for species_density, species_scratch_density, species_ave_density, and species_temp_density arrays, MAL 9/30/09
	species_density = new Scalar[4*(theGrid->getng())];
	species_scratch_density = &species_density[theGrid->getng()];
	species_ave_density = &species_density[2*theGrid->getng()];
	species_temp_density = &species_density[3*theGrid->getng()];
}

void Species::init_arrays(int size_array) // allocate and initialize Spatiotemporal arrays, HH 04/18/16
// Called only once in Spatialregion::update, so that everything else is already initialized.
{
  
	if (species_density_array) { delete [] species_density_array;}
	species_density_array = new Scalar[size_array];
	for (int m=0; m<size_array; m++)
	{
		species_density_array[m] = 0;
	}

	if (species_Temp_array) { delete [] species_Temp_array;}
	species_Temp_array = new Scalar[size_array];
	for (int m=0; m<size_array; m++)
	{
		species_Temp_array[m] = 0;
	}

	if (species_JdotE_array) { delete [] species_JdotE_array;}
	species_JdotE_array = new Scalar[size_array];
	for (int m=0; m<size_array; m++)
	{
		species_JdotE_array[m] = 0;
	}
        
}

void Species::set_arrays(int ng, int steps_rf, int m) // set Spatiotemporal arrays, HH 04/18/16
{
  
    for(int n=0; n<ng; n++)
    {
	species_density_array[n*steps_rf+m] += *(get_species_density()+n);
	species_Temp_array[n*steps_rf+m] += *(get_species_Temp()+n);
	species_JdotE_array[n*steps_rf+m] += *(get_species_JdotE()+n);
    }
    
}

void Species::set_theField(Fields *t) 
{
	theField = t;
	if(theGrid == NULL) { set_theGrid(t->get_theGrid()); }
	if(theField->is_transverseE() || theField->is_magnetized())
	{ set_3v_flag(); }

	oopicListIter<Fluid> thei(fluidList);
	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->set_thefield(t);
	}
}

void Species::add_ParticleGroup(ParticleGroup *pg)
{
	particleGroupList.add(pg, false);
}

// adds constant weight particle group and adds particles
ParticleGroup * Species::add_particles(int n, Scalar w0, Scalar *X,
		Vector3 *V, bool vnorm,
		bool update_field)
{
	ParticleGroup *pg;

	pg = get_ParticleGroup(w0, false, n);
	if (pg) pg->add_particles(n, w0, X, V, vnorm, update_field);

	return pg;
}

// adds constant weight particvariableWeightle group and adds particles
// mindgame: new X = X + dX and it can collect the particles at the boundaries.
ParticleGroup * Species::add_particles(int n, Scalar w0, Scalar *X,
		Scalar *dX, Vector3 *V, bool vnorm,
		bool update_field)
{
	ParticleGroup *pg;

	pg = get_ParticleGroup(w0, false, n);
	if (pg) pg->add_particles(n, w0, X, dX, V, vnorm, update_field);
	return pg;
}

// mindgame: add particles of _pg into particlegrouplist in species class
void Species::add_particles(ParticleGroup *_pg,
		bool vnorm, bool update_field)
{
	bool vw = _pg->is_variableWeight();
	for (int i=0;i<_pg->get_n();i++)
	{
		add_particle(_pg->get_x(i), _pg->get_v(i),
				vnorm, update_field, _pg->get_w(i), vw);
	}
}

ParticleGroup * Species::add_ParticleGroup(Scalar w0, int nm)
{
	return add_ParticleGroup(w0, variable_weight, nm);
}

ParticleGroup * Species::add_ParticleGroup(Scalar w0, bool vw, int nm)
{
	ParticleGroup *pg;
	// mindgame : to avoid compilation warning
	pg = NULL;

	if(nm < 1) { nm = nmax; }
	switch(pg_type)
	{
	case PARTICLEGROUPARRAYTYPE:
		pg = new ParticleGroupArray(nm, this, theField, w0, vw);
		particleGroupList.add(pg);
		break;
	default:
		error("NOT YET IMPLEMENTED!!!");
	}
	return pg;
}

void Species::add_particle(Scalar x, Vector3 v,
		bool vnorm, bool update_field, Scalar w, bool vw)
{
	ParticleGroup *pg;

	pg = get_ParticleGroup(w, vw);
	if (pg) pg->add_particle(x, v, vnorm, update_field, w);
}

void Species::add_particle(Scalar x, Scalar v1, Scalar v2, Scalar v3,
		bool vnorm, bool update_field, Scalar w, bool vw)
{
	ParticleGroup *pg;

	pg = get_ParticleGroup(w, vw);
	if (pg) pg->add_particle(x, v1, v2, v3, vnorm, update_field, w);
}

// mindgame: not to create new particlegroup all the time
//           do it only when necessary
ParticleGroup * Species::get_ParticleGroup(Scalar w, bool vw, int n)
{
	oopicListIter<ParticleGroup> thei(particleGroupList);

	for(thei.restart(); !thei.Done(); thei++)
	{
		if(!vw && !thei()->is_variableWeight() && w == thei()->get_w0())
		{
			if(thei()->is_capable_of_adding_particles(n))
			{ return thei(); }
		}
		else
			if(vw && thei()->is_variableWeight())
			{
				if(thei()->is_capable_of_adding_particles(n))
				{ return thei(); }
			}
	}

	if(w <= 0.)
	{
		if(np2c <= 0.)
		{
			ostring tmp = "weight of Species "+name+" is undefined";
			error("get_ParticleGroup()", tmp());
		}
		else
		{
			w = np2c;
		}
	}
	return add_ParticleGroup(w, vw, n);
}

bool Species::is(ostring _name) const
{
	if(_name == get_name()) { return true; }
	return false;
}

// modified for grid time averages, MAL 6/27/09
void Species::accumulate_density()
{
//  int i; //seg debug, comment out, MAL 9/27/12
	int n = theGrid->getng();

	oopicListIter<ParticleGroup> thei(particleGroupList);
	if (get_field()->get_nsteps() % subcycle == 0 || get_field()->get_nsteps() < 0) // add for subcycle, MAL 1/1/10
	{
		memset(get_species_density(), 0, n*sizeof(Scalar)); // accumulate species_density, MAL 9/25/09

		for(thei.restart(); !thei.Done(); thei++) // remove i=0 in 1st and i++ in 3rd arg, MAL 04/24/09
		{
			thei()->accumulate_density(); // in particlegrouparray.cpp, to accumulate density, MAL 10/2/09
		}

		Ntot_particles = 0.0; 
		for(int i=0; i < n; i++) //seg debug, add int, MAL 9/27/12
		{
			Ntot_particles += get_species_density()[i]; // species_density -> get_species_density(), MAL 9/25/09
			get_species_density()[i] /= theGrid->getV(i); // ditto, MAL 9/25/09
		}
	}
	// Do species density grid time average, MAL 6/27/09
	//	if (get_field()->get_diagupdate())
	//	{
	//		get_field()->grid_time_average(get_species_density(), get_species_temp_density(), get_species_ave_density());
	//	}
}

// accumulate 1D and 3D velocity moments for diagnostics, MAL 7/8/10
void Species::accumulate_vel_moments()
{
	int i;
	int n = theGrid->getng();
	Vector3 Efield;
	if (get_field()->get_diagupdate()) // add, MAL 11/3/10
	{ // begin if get_diagupdate, MAL, 11/3/10

		bool update_subcycle = get_field()->get_nsteps() % subcycle == 0 || get_field()->get_nsteps() < 0; // add for subcycle, MAL 1/3/10
		get_field()->grid_time_average(get_species_density(), get_species_temp_density(), get_species_ave_density()); // add, MAL 11/3/10

		// accumulate 1D velocity moments, MAL 7/8/10
		if (get_field()->get_f1flag())
		{
			if (update_subcycle) // add for subcycle, MAL 1/3/10
			{
				for(i=0; i < n; i++)
				{
					get_species_flux1()[i] /= theGrid->getV(i);
				}
			}
			if(get_field()->is_flux1enabled())
			{
				get_field()->grid_time_average(get_species_flux1(), get_species_temp_flux1(), get_species_ave_flux1(), subcycle);
			}
		}
		if (get_field()->is_J1dotE1enabled())
		{
			memset(get_species_J1dotE1(), 0, n*sizeof(Scalar));
			for(i=0; i < n; i++)
			{
				get_field()->getE(i, Efield);
				get_species_J1dotE1()[i] =get_q()*get_species_flux1()[i]*Efield.e1(); // added, MAL 4/25/09
			}
			get_field()->grid_time_average(get_species_J1dotE1(), get_species_temp_J1dotE1(), get_species_ave_J1dotE1(), subcycle);

			// integral of JdotE, MAL 12/13/09
			species_ave_J1dotE1dV = theGrid->integrate(get_species_ave_J1dotE1(),0,theGrid->getng());	// MAL 7/1/10
		}
		if (get_field()->get_e1flag())
		{
			if (update_subcycle) // add for subcycle, MAL 1/3/10
			{
				Scalar temp1 = 0.5*get_m();
				for(i=0; i < n; i++)
				{
					get_species_enden1()[i] *= (temp1/theGrid->getV(i));
				}
			}
			if(get_field()->is_enden1enabled())
			{
				get_field()->grid_time_average(get_species_enden1(), get_species_temp_enden1(), get_species_ave_enden1(), subcycle);
			}
		}
		if (get_field()->is_vel1enabled())
		{
			memset(get_species_vel1(), 0, n*sizeof(Scalar));
			for(i=0; i < n; i++)
			{
				if (get_species_density()[i] <= 0.)
				{
					get_species_vel1()[i] = 0.;
				}
				else
				{
					get_species_vel1()[i] = get_species_flux1()[i]/get_species_density()[i];
				}
			}

			get_field()->grid_time_ave(get_species_ave_vel1(), get_species_ave_density(), get_species_ave_flux1()); // MAL 9/11/10
		}
		if (get_field()->is_T1enabled())
		{
			memset(get_species_T1(), 0, n*sizeof(Scalar));
			Scalar T1tottemp = 0.0; //time-varying temperature averaged over all physical particles, MAL 8/4/10
			Scalar temp4 = 2./PROTON_CHARGE;
			Scalar temp5 = get_m()/PROTON_CHARGE;
			for(i=0; i < n; i++)
			{
				if (get_species_density()[i] <= 0.)
				{
					get_species_T1()[i] = 0.;
				}
				else
				{
					Scalar temp1 = (get_species_flux1()[i]/get_species_density()[i]);
					get_species_T1()[i] = temp4*get_species_enden1()[i]/get_species_density()[i]
																							  - temp5*temp1*temp1;
					if (get_species_T1()[i] < 0) { get_species_T1()[i] = 0.; } // kill tiny negative T's (due to round-off error), MAL 1/10/11

					T1tottemp += get_species_T1()[i]*get_species_density()[i]*theGrid->getV(i); // MAL 8/4/10
				}
			}

			if (gett_Ntot_particles() > 0) {T1tottemp /= gett_Ntot_particles();} // MAL 8/4/10, add if, MAL 11/3/10
			if(update_subcycle) T1tot = T1tottemp;
			get_field()->grid_time_ave(get_m(), get_np2c()/theField->get_naverage(), get_species_ave_T1(), get_species_ave_density(), get_species_ave_flux1(), get_species_ave_enden1()); // MAL 9/11/10
		}

		// accumulate 3D velocity moments for diagnostics, MAL 7/8/10
		if(get_field()->get_d3flag())
		{
			if (get_field()->get_f2flag())
			{
				if (update_subcycle) // add for subcycle, MAL 1/3/10
				{
					for(i=0; i < n; i++)
					{
						get_species_flux2()[i] /= theGrid->getV(i);
					}
				}
				if(get_field()->is_flux2enabled())
				{
					get_field()->grid_time_average(get_species_flux2(), get_species_temp_flux2(), get_species_ave_flux2(), subcycle);
				}
			}
			if (get_field()->is_J2dotE2enabled())
			{
				memset(get_species_J2dotE2(), 0, n*sizeof(Scalar));
				for(i=0; i < n; i++)
				{
					get_field()->getE(i, Efield);
					get_species_J2dotE2()[i] =get_q()*get_species_flux2()[i]*Efield.e2();
				}
				get_field()->grid_time_average(get_species_J2dotE2(), get_species_temp_J2dotE2(), get_species_ave_J2dotE2(), subcycle);
				// integral of JdotE, MAL 12/13/09
				species_ave_J2dotE2dV = theGrid->integrate(get_species_ave_J2dotE2(),0,theGrid->getng());	// MAL 7/1/10
			}
			if (get_field()->get_e2flag())
			{
				if (update_subcycle) // add for subcycle, MAL 1/3/10
				{
					Scalar temp1 = 0.5*get_m();
					for(i=0; i < n; i++)
					{
						get_species_enden2()[i] *= (temp1/theGrid->getV(i));
					}
				}
				if(get_field()->is_enden2enabled())
				{
					get_field()->grid_time_average(get_species_enden2(), get_species_temp_enden2(), get_species_ave_enden2(), subcycle);
				}
			}
			if (get_field()->is_vel2enabled())
			{
				memset(get_species_vel2(), 0, n*sizeof(Scalar));
				for(i=0; i < n; i++)
				{
					if (get_species_density()[i] <= 0.)
					{
						get_species_vel2()[i] = 0.;
					}
					else
					{
						get_species_vel2()[i] = get_species_flux2()[i]/get_species_density()[i];
					}
				}
				get_field()->grid_time_ave(get_species_ave_vel2(), get_species_ave_density(), get_species_ave_flux2()); // MAL 9/11/10
			}
			if (get_field()->is_T2enabled())
			{
				memset(get_species_T2(), 0, n*sizeof(Scalar));
				Scalar T2tottemp = 0.0; //time-varying temperature averaged over all physical particles, MAL 8/4/10
				Scalar temp4 = 2./PROTON_CHARGE;
				Scalar temp5 = get_m()/PROTON_CHARGE;
				for(i=0; i < n; i++)
				{
					if (get_species_density()[i] <= 0.)
					{
						get_species_T2()[i] = 0.;
					}
					else
					{
						Scalar temp2 = (get_species_flux2()[i]/get_species_density()[i]);
						get_species_T2()[i] = temp4*get_species_enden2()[i]/get_species_density()[i]
																								  - temp5*temp2*temp2;
						if (get_species_T2()[i] < 0) { get_species_T2()[i] = 0.; } // kill tiny negative T's (due to round-off error), MAL 1/10/11

						T2tottemp += get_species_T2()[i]*get_species_density()[i]*theGrid->getV(i); // MAL 8/4/10
					}
				}

				if (gett_Ntot_particles() > 0) {T2tottemp /= gett_Ntot_particles();} // MAL 8/4/10, add if, MAL 11/3/10
				if(update_subcycle) T2tot = T2tottemp;
				get_field()->grid_time_ave(get_m(), get_np2c()/theField->get_naverage(), get_species_ave_T2(), get_species_ave_density(), get_species_ave_flux2(), get_species_ave_enden2()); // MAL 9/11/10
			}
			if (get_field()->get_f3flag())
			{
				if (update_subcycle) // add for subcycle, MAL 1/3/10
				{
					for(i=0; i < n; i++)
					{
						get_species_flux3()[i] /= theGrid->getV(i);
					}
				}
				if(get_field()->is_flux3enabled())
				{
					get_field()->grid_time_average(get_species_flux3(), get_species_temp_flux3(), get_species_ave_flux3(), subcycle);
				}
			}
			if (get_field()->is_J3dotE3enabled())
			{
				memset(get_species_J3dotE3(), 0, n*sizeof(Scalar));
				for(i=0; i < n; i++)
				{
					get_field()->getE(i, Efield);
					get_species_J3dotE3()[i] =get_q()*get_species_flux3()[i]*Efield.e3();
				}
				get_field()->grid_time_average(get_species_J3dotE3(), get_species_temp_J3dotE3(), get_species_ave_J3dotE3(), subcycle);
				// integral of JdotE, MAL 12/13/09
				species_ave_J3dotE3dV = theGrid->integrate(get_species_ave_J3dotE3(),0,theGrid->getng());	// MAL, 7/1/10
			}
			if (get_field()->get_e3flag())
			{
				if (update_subcycle) // add for subcycle, MAL 1/3/10
				{
					Scalar temp1 = 0.5*get_m();
					for(i=0; i < n; i++)
					{
						get_species_enden3()[i] *= (temp1/theGrid->getV(i));
					}
				}
				if(get_field()->is_enden3enabled())
				{
					get_field()->grid_time_average(get_species_enden3(), get_species_temp_enden3(), get_species_ave_enden3(), subcycle);
				}
			}
			if (get_field()->is_vel3enabled())
			{
				memset(get_species_vel3(), 0, n*sizeof(Scalar));
				for(i=0; i < n; i++)
				{
					if (get_species_density()[i] <= 0.)
					{
						get_species_vel3()[i] = 0.;
					}
					else
					{
						get_species_vel3()[i] = get_species_flux3()[i]/get_species_density()[i];
					}
				}
				get_field()->grid_time_ave(get_species_ave_vel3(), get_species_ave_density(), get_species_ave_flux3()); // MAL 9/11/10
			}
			if (get_field()->is_T3enabled())
			{
				memset(get_species_T3(), 0, n*sizeof(Scalar));
				Scalar T3tottemp = 0.0; //time-varying temperature averaged over all physical particles, MAL 8/4/10
				Scalar temp4 = 2./PROTON_CHARGE;
				Scalar temp5 = get_m()/PROTON_CHARGE;
				for(i=0; i < n; i++)
				{
					if (get_species_density()[i] <= 0.)
					{
						get_species_T3()[i] = 0.;
					}
					else
					{
						Scalar temp3 = (get_species_flux3()[i]/get_species_density()[i]);
						get_species_T3()[i] = temp4*get_species_enden3()[i]/get_species_density()[i]
																								  - temp5*temp3*temp3;
						if (get_species_T3()[i] < 0) { get_species_T3()[i] = 0.; } // kill tiny negative T's (due to round-off error), MAL 1/10/11

						T3tottemp += get_species_T3()[i]*get_species_density()[i]*theGrid->getV(i); // MAL 8/4/10
					}
				}
				if (gett_Ntot_particles() > 0) {T3tottemp /= gett_Ntot_particles();} // MAL 8/4/10, add if, MAL 11/3/10
				if(update_subcycle) T3tot = T3tottemp;
				get_field()->grid_time_ave(get_m(), get_np2c()/theField->get_naverage(), get_species_ave_T3(), get_species_ave_density(), get_species_ave_flux3(), get_species_ave_enden3()); // MAL 9/11/10
			}
			if (get_field()->is_JdotEenabled())
			{
				memset(get_species_JdotE(), 0, n*sizeof(Scalar));
				for(i=0; i < n; i++)
				{
					get_field()->getE(i, Efield);
					get_species_JdotE()[i] =get_q()*(get_species_flux1()[i]*Efield.e1() +
							get_species_flux2()[i]*Efield.e2() + get_species_flux3()[i]*Efield.e3());
				}
				get_field()->grid_time_average(get_species_JdotE(), get_species_temp_JdotE(), get_species_ave_JdotE(), subcycle);
				// integral of JdotE, MAL 12/13/09
				species_ave_JdotEdV = theGrid->integrate(get_species_ave_JdotE(),0,theGrid->getng());
			}
			if (get_field()->is_endenenabled())
			{
				memset(get_species_enden(), 0, n*sizeof(Scalar));
				for(i=0; i < n; i++)
				{
					get_species_enden()[i] = get_species_enden1()[i]+get_species_enden2()[i]+get_species_enden3()[i];
				}
				get_field()->grid_time_average(get_species_enden(), get_species_temp_enden(), get_species_ave_enden(), subcycle);
			}
			if (get_field()->is_Tperpenabled())
			{
				memset(get_species_Tperp(), 0, n*sizeof(Scalar));
				Scalar Tperptottemp = 0.0; //time-varying temperature averaged over all physical particles, MAL 8/4/10
				Scalar temp6 = 0.5*get_m()/PROTON_CHARGE;
				for(i=0; i < n; i++)
				{
					if (get_species_density()[i] <= 0.)
					{
						get_species_Tperp()[i] = 0.;
					}
					else
					{
						Scalar temp3 = (get_species_flux3()[i]/get_species_density()[i]);
						Scalar temp2 = (get_species_flux2()[i]/get_species_density()[i]);
						get_species_Tperp()[i] = (get_species_enden2()[i]+get_species_enden3()[i])/(PROTON_CHARGE*get_species_density()[i])
							- temp6*(temp2*temp2+temp3*temp3);
						if (get_species_Tperp()[i] < 0) { get_species_Tperp()[i] = 0.; } // kill tiny negative T's (due to round-off error), MAL 1/10/11

						Tperptottemp += get_species_Tperp()[i]*get_species_density()[i]*theGrid->getV(i); // MAL 8/4/10
					}
				}
				if (gett_Ntot_particles() > 0) {Tperptottemp /= gett_Ntot_particles();} // MAL 8/4/10, add if, MAL 11/3/10
				if(update_subcycle) Tperptot = Tperptottemp;
				get_field()->grid_time_ave(get_m(), get_np2c()/theField->get_naverage(), get_species_ave_Tperp(), get_species_ave_density(), get_species_ave_flux2(), get_species_ave_enden2(),
				get_species_ave_flux3(), get_species_ave_enden3()); // MAL 9/11/10
			}
			if (get_field()->is_Tempenabled())
			{
				memset(get_species_Temp(), 0, n*sizeof(Scalar));
				Scalar Temptottemp = 0.0; //time-varying temperature averaged over all physical particles, MAL 8/4/10
				Scalar temp7 = TWOTHIRDS/PROTON_CHARGE;
				Scalar temp8 = ONETHIRD*get_m()/PROTON_CHARGE;
				for(i=0; i < n; i++)
				{
					if (get_species_density()[i] <= 0.)
					{
						get_species_Temp()[i] = 0.;
					}
					else
					{
						Scalar temp3 = (get_species_flux3()[i]/get_species_density()[i]);
						Scalar temp2 = (get_species_flux2()[i]/get_species_density()[i]);
						Scalar temp1 = (get_species_flux1()[i]/get_species_density()[i]);
						get_species_Temp()[i] = temp7*(get_species_enden1()[i]+get_species_enden2()[i]+get_species_enden3()[i])/get_species_density()[i]
																																					  - temp8*(temp1*temp1+temp2*temp2+temp3*temp3);
						if (get_species_Temp()[i] < 0) { get_species_Temp()[i] = 0.; } // kill tiny negative T's (due to round-off error), MAL 1/10/11

						Temptottemp += get_species_Temp()[i]*get_species_density()[i]*theGrid->getV(i); // MAL 8/4/10
					}
				}
				if (gett_Ntot_particles() > 0) {Temptottemp /= gett_Ntot_particles();} // MAL 8/4/10, add if, MAL 11/3/10
				if(update_subcycle) Temptot = Temptottemp;
				get_field()->grid_time_ave(get_m(), get_np2c()/theField->get_naverage(), get_species_ave_Temp(), get_species_ave_density(), get_species_ave_flux1(), get_species_ave_enden1(),
				get_species_ave_flux2(), get_species_ave_enden2(), get_species_ave_flux3(), get_species_ave_enden3()); // MAL 9/11/10
			}
		} // end accumulate 3D velocity moments, MAL 7/8/10
	} // end if get_diagupdate, MAL 11/3/10
}

// For time-variation of the integrals of species time-average J1dotE1 dV, etc, MAL 12/11/09
Scalar Species:: get_species_ave_J1dotE1dV ()
{
	if (get_field()->get_nsteps() > get_field()->get_naverage())
		return species_ave_J1dotE1dV;
	else return 0;
}

Scalar Species:: get_species_ave_J2dotE2dV ()
{
	if (get_field()->get_nsteps() > get_field()->get_naverage())
		return species_ave_J2dotE2dV;
	else return 0;
}

Scalar Species:: get_species_ave_J3dotE3dV ()
{
	if (get_field()->get_nsteps() > get_field()->get_naverage())
		return species_ave_J3dotE3dV;
	else return 0;
}

Scalar Species:: get_species_ave_JdotEdV ()
{
	if (get_field()->get_nsteps() > get_field()->get_naverage())
		return species_ave_JdotEdV;
	else return 0;
}

int Species::number_particles ()  
{
	int N = 0;
	oopicListIter<ParticleGroup> pgIter(particleGroupList);
	for (pgIter.restart(); !pgIter.Done(); pgIter++)
	{
		N += pgIter()->get_n();
	}
	return N; // MAL 9/11/09
}

Scalar Species::number_fluidparticles () 
{
	Scalar N = 0;
	oopicListIter<Fluid> collider_fIter(fluidList);
	for (collider_fIter.restart(); !collider_fIter.Done();  collider_fIter ++)
	{
		N +=  collider_fIter()->get_Ntot();
	}
	return N;
}

void Species::turn_on_dual_index()
{
	oopicListIter<ParticleGroup> pgIter(particleGroupList);
	for (pgIter.restart(); !pgIter.Done(); pgIter++)
	{
		pgIter()->init_dual_index();
	}
}

void Species::turn_off_dual_index()
{
	oopicListIter<ParticleGroup> pgIter(particleGroupList);
	for (pgIter.restart(); !pgIter.Done(); pgIter++)
	{
		pgIter()->turn_off_dual_index();
	}
}

// print_particles:  print all particles to a file
// -> FILE *fp: file to print to
void Species::print_particles(FILE *fp)
{

	oopicListIter<ParticleGroup> thei(particleGroupList);

	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->print_particles(fp);
	}
}

// dump_particles:  dump all particles to a file, MAL 5/4/10
// -> FILE *fp: dumpfile
void Species::dump_particles(FILE *fp)
{
	oopicListIter<ParticleGroup> thei(particleGroupList);
	int ntemp = particleGroupList.nItems();
	if (ntemp > 0) // if there are particle groups for this species
	{
		for (thei.restart(); !thei.Done(); thei++)
		{
			thei()->dump_particles(fp);
		}
	}
}

////////////////////////
// functions for fluids
////////////////////////

// dump_fluids:  dump all fluids to a file, MAL200228
// -> FILE *fp: dumpfile
void Species::dump_fluids(FILE *fp)
{
  oopicListIter<Fluid> thei(fluidList);
  int nfluids = fluidList.nItems();
  if (nfluids > 0) // there are fluids for this species
  {
    for (thei.restart(); !thei.Done(); thei++)
    {
      thei()->dump_fluid(fp);
    }
  }
}

// reload_fluids:  reload evolving fluids from a dumpfile, MAL200229
// -> FILE *fp: dumpfile
void Species::reload_fluids(FILE *fp)
{
  oopicListIter<Fluid> thei(fluidList);
  int nfluids = fluidList.nItems();
  if (nfluids > 0) // there are fluids for this species
  {
    for (thei.restart(); !thei.Done(); thei++)
    {
      thei()->reload_fluid(fp);
    }
  }
}


// return potential-dependent (non-linear) electrostatic density [C/m^3]
Scalar Species::electrostatic_density(Scalar potential)
{
	Scalar val = 0.;
	oopicListIter<Fluid> thei(nonlinearfluids);

	for(thei.restart(); !thei.Done(); thei++)
	{
		val += thei()->electrostatic_density(potential);
	}

	return val;
}

// return derivative of
// potential-dependent (non-linear) electrostatic density [C/m^3 V]
Scalar Species::electrostatic_derivative(Scalar potential)
{
	Scalar val = 0.;
	oopicListIter<Fluid> thei(nonlinearfluids);

	for(thei.restart(); !thei.Done(); thei++)
	{
		val += thei()->electrostatic_derivative(potential);
	}

	return val;
}

void Species::update_N_tot()
{
	ApplyToList(update_N_tot(), fluidList, Fluid);
}

void Species::update_fluids_from_fields(Scalar * _phi)
{
	oopicListIter<Fluid> thei(nonlinearfluids);
	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->update_fields(_phi);
	}

}

// integrate_fluids:  advance fluids by timestep dt
void Species::integrate_fluids(Scalar dt)
{
	oopicListIter<Fluid> thei(fluidList);

	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->update(dt);
	}
}

bool Species::is_nonlinear(void)
{
	if(debug()) { fprintf(stderr, "Fluid:  %s ", name()); }
	oopicListIter<Fluid> thei(nonlinearfluids);
	for(thei.restart(); !thei.Done(); thei++)
	{
		Scalar Ntot = thei()->get_Ntot();
		if(debug()) { fprintf(stderr, "ntot:  %g\n", Ntot); }
		if(Ntot > 0.) { return true; }
	}
	return false;
}

// mindgame: only in the case of [0.5 T = 0.5 m sqr(vt)]
Scalar Species::vt(Scalar T)
{
	return sqrt(PROTON_CHARGE * T / m);
}

// vthermal: return a 3-vector of the thermal velocity given a temperature
// T : temperature
// volts : true if temperature is in volts, false if in K
// returns a 
Vector3 Species::vthermal(Scalar T, bool volts)
{
	if(!volts) { T /= EV_IN_K; }

	Vector3 retval;
	return (retval = sqrt(PROTON_CHARGE * T / m));
}

// add weighting of v and v^2 to grid for particle and energy flux and JdotE
// diagnostics, MAL 6/20/10
void Species::advance_v(Scalar dt) 
{
	oopicListIter<ParticleGroup> thei(particleGroupList);
	if (theField->get_nodiagupdate() || theField->get_d0flag()) { // just accumulate density
		for(thei.restart(); !thei.Done(); thei++)
		{
			thei()->advance_v(dt);
		}
	}
	else if (theField->get_d1flag()) { // accumulate density and weight v1 and v1^2 to grid
		int n = theGrid->getng();
		memset(get_species_flux1(), 0, n*sizeof(Scalar));
		memset(get_species_enden1(), 0, n*sizeof(Scalar));
		for(thei.restart(); !thei.Done(); thei++)
		{
			thei()->advance_v1D(dt);
		}
	}
	else { // accumulate density and weight v1, v2, v3 and v1^2, v2^2, and v3^2 to grid, MAL 7/1/10
		int n = theGrid->getng();
		memset(get_species_flux1(), 0, n*sizeof(Scalar));
		memset(get_species_enden1(), 0, n*sizeof(Scalar));
		memset(get_species_flux2(), 0, n*sizeof(Scalar));
		memset(get_species_enden2(), 0, n*sizeof(Scalar));
		memset(get_species_flux3(), 0, n*sizeof(Scalar));
		memset(get_species_enden3(), 0, n*sizeof(Scalar));
		for(thei.restart(); !thei.Done(); thei++)
		{
			thei()->advance_v3D(dt);
		}
	}
}

void Species::advance_x(Scalar dt)
{
	oopicListIter<ParticleGroup> thei(particleGroupList);

	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->advance_x(dt);
	}
}

void Species::Gather_E(void)
{
	oopicListIter<ParticleGroup> thei(particleGroupList);

	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->Gather_E();
	}
}

Scalar Species::get_dt() const
{
	return theField->get_dt();
}

void Species::advance_vload(Scalar dt)
{

	switch(get_particlegrouptype())
	{
	case PARTICLEGROUPARRAYTYPE:
		break;
	default:
		error("NOT YET IMPLEMENTED!!!");
	}

	oopicListIter<ParticleGroup> thei(particleGroupList);

	for(thei.restart(); !thei.Done(); thei++)
	{
		ParticleGroupArray *pga = (ParticleGroupArray*)(thei());

		pga->advance_vload(dt);
	}
}

Vector3 Species::average_velocity()
{
	Vector3 retval;
	Scalar www = 0.;

	oopicListIter<ParticleGroup> thei(particleGroupList);
	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->average_velocity(retval, www);
	}

	if(www == 0.) { return 0.; }

	retval /= www;
	return retval;

}


Scalar Species::max_position()
{
	Scalar retval = 0.;

	oopicListIter<ParticleGroup> thei(particleGroupList);
	for(thei.restart(); !thei.Done(); thei++)
	{
		retval = thei()->max_position();
	}

	return retval;

}

Vector3 Species::max_velocity()
{
	Vector3 retval = 0.;

	oopicListIter<ParticleGroup> thei(particleGroupList);
	for(thei.restart(); !thei.Done(); thei++)
	{
		retval = thei()->max_velocity();
	}

	return retval;

}

Vector3 Species::front_velocity()
{
	Vector3 retval = 0.;

	oopicListIter<ParticleGroup> thei(particleGroupList);
	for(thei.restart(); !thei.Done(); thei++)
	{
		retval = thei()->front_velocity();
	}

	return retval;

}

// calculate moments of the distribution function
void Species::calculate_moments()
{
	int i;

	// allocate moments if the first time called
	if(!moment)
	{
		if(nmoment <= 0) { return; }
		moment = new Scalar * [nmoment];
		for(i=0; i < nmoment; i++)
		{
			moment[i] = new Scalar [theGrid->getng()];
		}
	}

	// if moments aren't accumulated, reset to zero
	if(!accumulate_moment)
	{
		for(i=0; i < nmoment; i++)
		{
			memset(moment[i], 0, theGrid->getng()*sizeof(Scalar));
		}
	}

	// calculate the moments
	for(i=0; i < nmoment; i++)
	{

	}

}

Scalar Species::get_dxdt() const
{
	return theGrid->getdx()/theField->get_dt();
}

// mindgame: Assuming there is no duplicated name in specieslist
Species * Species::get_species_named_with(oopicListIter<Species> species_iter, ostring name)
{
	for(species_iter.restart(); !species_iter.Done(); species_iter++)
	{
		if (name == species_iter()->get_name())
		{
			return species_iter();
		}
	}
	return NULL;
}

// add to print dumpfile, MAL 11/17/10
ostring Species::get_species_name_with_ident(oopicListIter<Species> species_iter, int _ident)
{
	for(species_iter.restart(); !species_iter.Done(); species_iter++)
	{
		if (_ident == species_iter()->get_ident())
		{
			return species_iter()->get_name();
		}
	}
	return "name not found";
}

Species * Species::get_species_named_with(oopicListIter<Species> species_iter, ReactionSpeciesType _name)
{
	for(species_iter.restart(); !species_iter.Done(); species_iter++)
	{
		if (_name == species_iter()->get_reactiontype())
		{
			return species_iter();
		}
	}
	// error("unknow species");
	return NULL;
}

// mindgame: Assuming there is no duplicated name in specieslist
vector<Species *> Species::get_reaction_species_named_with(oopicListIter<Species> species_iter, ReactionSpeciesType _name)
{
	vector<Species *> species_vector;
	for(species_iter.restart(); !species_iter.Done(); species_iter++)
	{
		if (_name == species_iter()->get_reactiontype())
		{
			species_vector.push_back(species_iter());
		}
	}
	return species_vector;
}

// mindgame: Assuming there is no duplicated name in specieslist
vector<Species *> Species::get_reaction_species_named_with(oopicListIter<Species> species_iter, ReactionSpeciesType _name1, ReactionSpeciesType _name2)
{
	vector<Species *> species_vector;
	ReactionSpeciesType name;
	for(species_iter.restart(); !species_iter.Done(); species_iter++)
	{
		name = species_iter()->get_reactiontype();
		if (_name1 == name || _name2 == name)
		{
			species_vector.push_back(species_iter());
		}
	}
	return species_vector;
}

void Species::convert_ostrings_to_speciestypes()
{
	if (reaction_name != "")
		reaction_type=ReactionSpeciesTypeName(reaction_name);
}
