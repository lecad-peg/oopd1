#include <stdio.h>

#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "fields/fields.hpp"
#include "main/grid.hpp"
#include "main/drive.hpp"
#include "main/boundary.hpp"
#include "main/particlegroup.hpp"
#include "main/circuit.hpp"

#include "main/drive_inline.hpp"
#include "fields/fields_inline.hpp"
#include "main/grid_inline.hpp"


Circuit::Circuit(Fields * _thefield)
{
  thefield = _thefield;
  setdefaults();
  setparamgroup();
  check();
  init();
}

Circuit::Circuit(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		 oopicList<Parameter<int> > int_variables, Fields *_thefield)
{
  thefield = _thefield;
  init_parse(fp, float_variables, int_variables);
}

Circuit::~Circuit()
{
  drivelist.deleteAll();
}

void Circuit::setdefaults(void)
{
  Q = Q0 = 0.;
  R = L = 0.;
  C = -1.;
  sourcetype = -1;
  idealvsource = 0;

  add_child_name("Drive");

  sigma = circ_q = circ_q1 = circ_q2 = circ_q3 = k = 0.0;
	source_q = source_q1 = source_q2 = source_q3 = 0.0;
	circ_I = 0.0; // Added for current diagnostic, MAL 12/4/09
}

void Circuit::setparamgroup(void)
{
  pg.add_fparameter("R", &R, "[Ohms] series resistance of circuit");
  pg.add_fparameter("L", &L, "[Henrys] series inductance of circuit");
  pg.add_fparameter("C", &C, "[Fahrads] series capacitance of circuit");
  pg.add_fparameter("Q0", &Q0, "[C] initial charge on capacitor");
}

void Circuit::check(void)
{
  if((R !=0) || (L != 0))
    {
      if(C == -1.)
	{
	  C = 0.;
	}
    }

  check_drive();
}

void Circuit::init(void)
{

  Q = Q0;
  epsilon = thefield->get_epsilon();
  dt = thefield->get_dt();

  /* Deciding which circuit solver to use.. */
  if((C < 1e-30) && (C >= 0.))
    {   /* when C -> 0.0, OPEN CIRCUIT */      
      puts("Circuit: Active external source is ignored since C < 1E-30\n");
      drivelist.deleteAll();
      sourcetype = CURRENTSOURCE;
      circuitptr = &Circuit::currentsource;
    }
  else if(sourcetype == CURRENTSOURCE) 
    {  /* When current source is applied */
      fprintf(stderr, "\nCurrent Souce...");
      circuitptr = &Circuit::currentsource;
    }
  else if(L < 1e-30 && R < 1e-30 && C <= 0) 
    {
      /* When L=R=0.0 and C -> infinity, SHORT CIRCUIT */
      circuitptr = &Circuit::shortcircuit;
      idealvsource = 1;
    }
  else 
    {             /* The general case with external voltage source */
     //Set coefficients for BDF.
      a0 = 2.25*L/dt/dt + 1.5*R/dt + 1/C;
      a1 = -6*L/dt/dt -2*R/dt;
      a2 = 5.5*L/dt/dt + 0.5*R/dt;
      a3 = -2*L/dt/dt;
      a4 = 0.25*L/dt/dt;

      circuitptr = &Circuit::RLC_circuit;
    } 
}

Parse * Circuit::special(ostring s)
{
  if(s == "Drive")
    {
      Drive *thedrive;
      
      thedrive = new Drive(f, pg.float_variables, pg.int_variables);
      drivelist.add(thedrive);
      return thedrive;
    }
  return 0;
}

void Circuit::check_drive(void)
{  
  bool iflag, vflag;
  oopicListIter<Drive> thei(drivelist);
  
  if(drivelist.nItems() == 0)
    {
      fprintf(stderr, "\n*Circuit::no drive found, adding V=0 source");
      drivelist.add(new Drive());
    }

  iflag = vflag = false;
  for(thei.restart(); !thei.Done(); thei++)
    {	  
      // set dt, needed by class Drive
      thei()->set_dt(thefield->get_dt());

      if(thei()->vflag) { vflag = true; }
      if(thei()->iflag) { iflag = true; }
      if(vflag && iflag)
	{
	  fprintf(stderr, "\nMultiple drives must be of the same type");
	  fprintf(stderr, 
		  "\nEither voltage sources in series or current sources in parallel\n");
	  exit(1);
	}
    }
  if(vflag)
    {
      sourcetype = VOLTAGESOURCE;      
    }
  else if(iflag)
    {
      sourcetype = CURRENTSOURCE;
    }
  else
    {
      error("Drive:", "No current or voltage source!");
    }
}

bool Circuit::voltagesource(void) 
{ 
  if(sourcetype == VOLTAGESOURCE) 
    { return true; }
  else { return false; }
}

int Circuit::idealvoltagesource(void)
{
  return idealvsource;
}

void Circuit::update(bool update_circuitptr)
{
//	if (thefield->get_nsteps() < 0) return;
  Scalar val = thefield->get_t() + 0.5*thefield->get_dt();
//	printf("\nCircuit::update(), thefield->get_t()=%g, nsteps=%li",thefield->get_t(),thefield->get_nsteps()); // debug dumpfile
  oopicListIter<Drive> thei(drivelist);
		
  for(thei.restart(), sourcestrength=0.; !thei.Done(); thei++)
    {
      sourcestrength += thei()->value(val);
    }
//	printf("\nCircuit::update(), t=%g, sourcestrength=%g\n",val,sourcestrength); // for debug dumpfile, MAL 5/22/10
		
  if(update_circuitptr)
    {
      (this->*circuitptr)();
    }
}

// functions for different types of circuits

void Circuit::currentsource(void)
{
  static int init=1;
  if(init) { init = 0; printf("\nCircuit::currentsource(), Didn't do 1st timestep\n"); return; } // don't do first time step, debug dumpfile
  Q += sourcestrength*thefield->get_dt();
//	printf("\nCircuit::currentsource(), updated Q=%g",Q);
}

void Circuit::shortcircuit(void)
{
}

void Circuit::RLC_circuit(void)
{
}

// RLC circuit stuff.

Scalar Circuit::get_rhs_coeff(Scalar qconv, Scalar A)
{
//	printf("\nCircuit::get_rhs_coeff(), nsteps=%li, sourcestrength=%g, circ_q=%g, circ_q1=%g, circ_q2=%g, circ_q3=%g\n",
//		thefield->get_nsteps(),get_sourcestrength(),circ_q,circ_q1,circ_q2,circ_q3); // for debug dumpfile, MAL 5/12/10
  k = a1*circ_q + a2*circ_q1 + a3*circ_q2 + a4*circ_q3;
  return A*sigma + qconv - circ_q + (get_sourcestrength()-k)/a0;
  if(debug()) { 
     fprintf(stderr, "get_rhs_coeff: t = %g, circ_q = %g, a0 = %g, A = %g\n", thefield->get_t(), circ_q, a0, A); }
}

void Circuit::update_RLC(Scalar phi, Scalar qconv, Scalar A)
{
	circ_q3 = circ_q2;
  circ_q2 = circ_q1;
  circ_q1 = circ_q;
  circ_q = (get_sourcestrength() - phi - k)/a0;
	
	circ_I = (2*(circ_q-circ_q1) + 0.5*(circ_q2-circ_q))/dt; // circuit current, Eq. (17) from Verboncoeur et al, J.Comp.Phys 104, 321 (1993), MAL 12/4/09

  sigma += ((circ_q - circ_q1) + qconv)/A;
//     printf("\nCircuit::update_RLC: t = %g, nsteps=%li, sourcestrength=%g, k=%g, a0=%g", // for debug dumpfile
//			thefield->get_t(), thefield->get_nsteps(), get_sourcestrength(), k, a0);
//     printf("\n\t circ_q = %g, circ_q1=%g, circ_q2=%g, circ_q3=%g,  sigma = %g, phi = %g\n",
//			circ_q, circ_q1, circ_q2, circ_q3, sigma, phi);
  if(debug())
	{ 
     fprintf(stderr, "update_RLC: t = %g, sourcestrength = %g, sigma = %g, circ_q = %g, a0 = %g, phi = %g, k = %g\n",
			thefield->get_t(), get_sourcestrength(), sigma, circ_q, a0, phi, k);
	}

  update(false);
}

