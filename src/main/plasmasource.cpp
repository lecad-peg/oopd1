#include <stdio.h>

#include "main/pd1.hpp"
#include "main/parse.hpp"
#include "utils/ovector.hpp"
#include "main/species.hpp"
#include "utils/ostring.hpp"
#include "main/boundary.hpp"
#include "main/grid.hpp"
#include "main/particlegroup.hpp"
#include "main/grid_inline.hpp"
#include "fields/fields.hpp"
#include "fields/fields_inline.hpp"
#include "distributions/dist_xv.hpp"
#include "distributions/dist_x.hpp"
#include "distributions/dist_v.hpp"

#include "main/plasmasource.hpp"

PlasmaSource::PlasmaSource(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
			   oopicList<Parameter<int> > int_variables, Grid *t,
			   SpeciesList *thespecieslist)
{
  specieslist = thespecieslist;
  thegrid = t;
  init_parse(fp, float_variables, int_variables);
}

PlasmaSource::~PlasmaSource()
{
}

void PlasmaSource::setdefaults()
{
  Load::setdefaults();
  classname = "PlasmaSource";
  rate = 0.0;
  xload_type = RANDOM;
}

void PlasmaSource::setparamgroup()
{
  Load::setparamgroup();
  pg.add_fparameter("rate", &rate, "[m^{-3}s^{-1}] peak source rate");
}

void PlasmaSource::init()
{
  Load::init_dist();
}

// load() is called each timestep to load the plasma for that step
void PlasmaSource::load(Scalar t, Scalar dt)
{ 
  // compute the number of particles to load
  // frand() gives statistical representation even for frsctional rates
  np = int(dt*rate*thegrid->getV(x0, x1)/np2c + frand());

  if(np <= 0) return; // no particles to load

  //print_shr_msg("\n*loading species %s...np = %d\n", species->get_name()(),np);
  Scalar *x = new Scalar[np];  

  xdist->get_X(np,x);
  thegrid->XtogridX(np, x);

  //load velocity distribution
  Vector3 *v = new Vector3 [np];

  vdist->get_U(np,v);

  //--------------------------------------------------
  
  // for (int i=0; i<np; i++) printf("i=%d\tx=%G\tvx=%G\n", i, x[i], v[i].e1());
  species->add_particles(np, np2c, x, v);
  //print_shr_msg("%d particles loaded.", np);

  delete [] x;
  delete [] v;
}

