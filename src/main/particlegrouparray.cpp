//********************************************************
// particlegrouparray.cpp
//
// Revision history
//
// (JohnV 09Jul2003) Original code.
//
//********************************************************
#include <stdio.h>
#include "main/pd1.hpp"
#include "main/oopiclist.hpp"
#include "utils/ostring.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/grid.hpp"
#include "fields/fields.hpp"
#include "spatial_region/sptlrgn.hpp"
#include "main/boundary.hpp"
#include "main/particlegroup.hpp"
#include "main/load.hpp"
#include "main/particlegrouparray.hpp"

#include "main/grid_inline.hpp"
#include "fields/fields_inline.hpp"

ParticleGroupArray::ParticleGroupArray(int nmax, Species *theSpecies,
		Fields *theFields, Scalar np2c0,
		bool variableWeight,
		bool fractional, bool directional)
: ParticleGroup(nmax, theSpecies, theFields, np2c0, variableWeight)
{
	dir = 0;
	allocate_arrays(theSpecies, theFields, fractional, directional);
}

ParticleGroupArray::~ParticleGroupArray()
{
	if (is_variableWeight()) delete[] w;
	delete[] x;
	delete[] x2;
	delete[] v1;
	if(v2) delete[] v2;
	if(v3) delete[] v3;
	delete[] E1;
	if (E2) delete[] E2;
	if (E3) delete[] E3;
	// mindgame: maybe someone forgot to unalloc ?
	if(frac) delete[] frac;
	delete[] arrays;
	if(dir) { delete [] dir; }
}

void ParticleGroupArray::allocate_arrays(Species *theSpecies,
		Fields *theFields, bool fractime,
		bool directional)
{
	int i = 0;

	// allocate arrays for this group:
	if (is_variableWeight()) w = new Scalar[nmax];
	else w = NULL;
	x = new Scalar[nmax];
	x2 = new Scalar[nmax];
	narrays = 3;
	// mindgame: is_transverseE() is also necessary
	//          since is_3v() is not defined as early as the field is defined.
	if(theSpecies->is_3v() || theFields->is_transverseE()) narrays += 2;
	if(is_variableWeight()) narrays += 1;
	if(theFields->is_transverseE()) narrays += 2;
	if(fractime) { narrays += 1; }

	arrays = new Scalar * [narrays];
	arrays[0] = x;
	i = 2;

	if(is_variableWeight()) { arrays[i] = w; i++; }

	v[0] = new Scalar[nmax];
	arrays[1] = v[0];
	// allocate these components only if needed
	// mindgame: is_transverseE() is also necessary
	//          since is_3v() is not defined as early as the field is defined.
	if (theSpecies->is_3v() || theFields->is_transverseE())
	{
		v[1] = new Scalar[nmax];
		v[2] = new Scalar[nmax];
		arrays[i] = v[1];
		arrays[i+1] = v[2];
		i += 2;
	}
	else
	{
		v[1] = NULL;
		v[2] = NULL;
	}
	v1 = v[0];
	v2 = v[1];
	v3 = v[2];

	E[0] = new Scalar[nmax];
	arrays[i] = E[0];
	i += 1;

	// allocate the space for E as needed
	if (theFields->is_transverseE())
	{
		E[1] = new Scalar[nmax];
		E[2] = new Scalar[nmax];
		arrays[i] = E[1];
		arrays[i+1] = E[2];
		i += 2;
	}
	else
	{
		E[1] = NULL;
		E[2] = NULL;
	}
	E1 = E[0];
	E2 = E[1];
	E3 = E[2];

	if(fractime) // allocate arrays for fractional timestep
	{
		frac = new Scalar[nmax];
		arrays[i] = frac;
	}
	else
	{
		frac = 0;
	}

	if(directional) // allocate arrays for direction
	{
		dir = new Direction[nmax];
	}
	else
	{
		dir = 0;
	}
}


int ParticleGroupArray::add_particle(Scalar X, Scalar V1, Scalar V2,
		Scalar V3, bool vnorm, bool update_field,
		Scalar W)
{
	// mindgame: not (n <= nmax) but (n+1 <= nmax)
	if(n == nmax-1)
	{
		if(reallocflag)
		{
			realloc(reallocmult*nmax + 1);
		}
		else
		{
			return 1;
		}
	}

	x[n] = X;
	if(vnorm) v1[n] = V1/dxdt;
	else v1[n] = V1;

	if(v2 && v3)
	{
		if(vnorm)
		{
			v2[n] = V2/dxdt;
			v3[n] = V3/dxdt;
		}
		else
		{
			v2[n] = V2;
			v3[n] = V3;
		}
	}

	if(update_field)
	{
		if(E2 && E3)
			theFields->get_E(x[n], E1[n], E2[n], E3[n]);
		else
			theFields->get_E(x[n], E1[n]);
	}

	if(is_variableWeight())
	{
		if(W < 0.) { w[n] = w0; }
		else { w[n] = W; }
	}
	n++;
	return 0;

}

// mindgame: only for this class to avoid code duplication
void ParticleGroupArray::save_into_array(int N, Scalar *X, Scalar *V1,
		Scalar *V2, Scalar *V3, bool vnorm,
		Scalar *W)
{
	int size = N*sizeof(Scalar);
	memcpy(x+n, X, size);
	if(V1 != NULL)
	{
		if (vnorm)
		{
			for (int i=0; i<N; i++)
				v1[n+i] = V1[i]/dxdt;
		}
		else memcpy(v1+n, V1, size);
	}
	else { memset(v1+n, 0, size); }
	if(v2 && v3)
	{
		if(V2 != NULL)
		{
			if (vnorm)
			{
				for (int i=0; i<N; i++)
					v2[n+i] = V2[i]/dxdt;
			}
			else memcpy(v2+n, V2, size);
		}
		else
			memset(v2+n, 0, size);

		if(V3 != NULL)
		{
			if (vnorm)
			{
				for (int i=0; i<N; i++)
					v3[n+i] = V3[i]/dxdt;
			}
			else
				memcpy(v3+n, V3, size);
		}
		else
			memset(v3+n, 0, size);
	}

	int top = n+N;

	if(is_variableWeight())
	{
		if(W == NULL)
		{
			for(int i=n; i < top; i++)
			{
				w[i] = w0;
			}
		}
		else
		{
			memcpy(w+n, W, size);
		}
	}
}

int ParticleGroupArray::add_particles(int N, Scalar *X, Scalar *V1,
		Scalar *V2, Scalar *V3, bool vnorm,
		bool update_field, Scalar *W)
{
	if(n+N >= nmax)
	{
		if(reallocflag)
		{
			realloc((n+N)*reallocmult + 1);
		}
		else
		{
			return 1;
		}
	}

	save_into_array(N, X, V1, V2, V3, vnorm, W);

	int top = n+N;
	if (update_field)
		for(int i=n; i < top; i++)
		{
			if(E2 && E3)
				theFields->get_E(x[i], E1[i], E2[i], E3[i]);
			else
				theFields->get_E(x[i], E1[i]);
		}
	n += N;

	return 0;

}

// mindgame: only for this class to avoid code duplication
void ParticleGroupArray::save_into_array(int N, Scalar W, Scalar *X,
		Vector3 *V, bool vnorm)
{

	int size = N*sizeof(Scalar);
	memcpy(x+n, X, size);
	if(V != NULL)
	{
		if (vnorm)
		{
			for (int i=0; i<N; i++)
				v1[n+i] = V[i].e1()/dxdt;
		}
		else
		{
			for (int i=0; i<N; i++)
				v1[n+i] = V[i].e1();
		}
	}
	else { memset(v1+n, 0, size); }
	if(v2 && v3)
	{
		if(V != NULL)
		{
			if (vnorm)
			{
				for (int i=0; i<N; i++)
					v2[n+i] = V[i].e2()/dxdt;
			}
			else
			{
				for (int i=0; i<N; i++)
					v2[n+i] = V[i].e2();
			}
		}
		else
			memset(v2+n, 0, size);

		if(V != NULL)
		{
			if (vnorm)
			{
				for (int i=0; i<N; i++)
					v3[n+i] = V[i].e3()/dxdt;
			}
			else
			{
				for (int i=0; i<N; i++)
					v3[n+i] = V[i].e3();
			}
		}
		else
			memset(v3+n, 0, size);
	}

	int top = n+N;
	if(is_variableWeight())
	{
		if(W < 0.)
		{
			for(int i=n; i < top; i++)
			{
				w[i] = w0;
			}
		}
		else
		{
			for(int i=n; i < top; i++)
			{
				w[i] = W;
			}
		}
	}
}

int ParticleGroupArray::add_particles(int N, Scalar W, Scalar *X, Vector3 *V,
		bool vnorm, bool update_field)
{
	if(n+N >= nmax)
	{
		if(reallocflag)
		{
			realloc((n+N)*reallocmult + 1);
		}
		else
		{
			return 1;
		}
	}

	save_into_array(N, W, X, V, vnorm);

	int top = n+N;
	if (update_field)
		for(int i=n; i < top; i++)
		{
			if(E2 && E3)
				theFields->get_E(x[i], E1[i], E2[i], E3[i]);
			else
				theFields->get_E(x[i], E1[i]);
		}
	n += N;

	return 0;
}

// mindgame: new X = X + dX and it can collect the particles at the boundaries.
int ParticleGroupArray::add_particles(int N, Scalar W, Scalar *X, Scalar *dX,
		Vector3 *V, bool vnorm, bool update_field)
{
	int n_sum = n+N;

	if(n_sum >= nmax)
	{
		if(reallocflag)
		{
			realloc(n_sum*reallocmult + 1);
		}
		else
		{
			return 1;
		}
	}

	save_into_array(N, W, X, V, vnorm);

	int past_n = n;
	n = n_sum;

	for(int i=past_n, j=0, N_tmp=N, old_n; i < n;)
	{
		old_n = n;
		theGrid->move(x[i], dX[j], this, i);
		if (n < old_n)
		{
			N_tmp--;
			dX[j] = dX[N_tmp];
		}
		else j++;
	}

	if (update_field)
		for(int i=past_n; i < n; i++)
		{
			if(E2 && E3)
				theFields->get_E(x[i], E1[i], E2[i], E3[i]);
			else
				theFields->get_E(x[i], E1[i]);
		}

	return 0;
}

void ParticleGroupArray::delete_particle(int i)
{
	int j;
	n--;
	if (dual_index_flag)
	{
		n_old--;

		if (i != n_old)
			for(j=0; j < narrays; j++)
			{
				arrays[j][i] = arrays[j][n_old];
			}

		// mindgame: extra computation in dual_index scheme.
		if (n_old != n)
			for(j=0; j < narrays; j++)
			{
				arrays[j][n_old] = arrays[j][n];
			}
	}
	else
	{
		if (i != n)
			for(j=0; j < narrays; j++)
			{
				arrays[j][i] = arrays[j][n];
			}
	}
}

void ParticleGroupArray::realloc(int i)
{
	int offset;
	int size = n*sizeof(Scalar);

	Scalar *xtmp;
	Scalar *wtmp;
	Scalar *vtmp[3];
	Scalar *etmp[3];
	Scalar *ftmp;

	if(i < n)
	{
		fprintf(stderr, "\nParticleGroupArray::realloc(int i) -- i < n");
		fprintf(stderr, "\nNot reallocating, returning....");
	}

	xtmp = new Scalar[i];
	memcpy(xtmp, x, size);
	delete [] x;
	x = xtmp;
	arrays[0] = x;

	offset = 2;
	if(is_variableWeight())
	{
		wtmp = new Scalar[i];
		memcpy(wtmp, w, size);
		delete [] w;
		w = wtmp;
		arrays[2] = w;
		offset = 3;
	}

	vtmp[0] = new Scalar[i];
	memcpy(vtmp[0], v[0], size);
	delete [] v[0];
	v[0] = vtmp[0];
	arrays[1]= v[0];                           //Yang 11/02/2004
	if(v2 && v3)
	{
		for(int j=1; j < 3; j++)
		{
			vtmp[j] = new Scalar[i];
			memcpy(vtmp[j], v[j], size);
			delete [] v[j];
			v[j] = vtmp[j];
			arrays[offset++] = v[j];
		}
	}

	etmp[0] = new Scalar[i];
	memcpy(etmp[0], E[0], size);
	delete [] E[0];
	E[0] = etmp[0];
	arrays[offset++] = E[0];

	if(E2 && E3)
	{
		for(int j=1; j < 3; j++)
		{
			etmp[j] = new Scalar[i];
			memcpy(etmp[j], E[j], size);
			delete [] E[j];
			E[j] = etmp[j];
			arrays[offset++] = E[j];
		}
	}

	if(frac)
	{
		ftmp = new Scalar[i];
		memcpy(ftmp, frac, size);
		delete [] frac;
		frac = ftmp;
		arrays[offset++] = frac;
	}

	if(dir)
	{
		Direction *dtmp;
		dtmp = new Direction[i];
		memcpy(dtmp, dir, n*sizeof(Direction));
		delete [] dir;
		dir = dtmp;
	}

	v1 = v[0];
	v2 = v[1];
	v3 = v[2];
	E1 = E[0];
	E2 = E[1];
	E3 = E[2];
	nmax = i;
}

// v_updateB3(): rotate v for general magnetic field.
// t1, t2 are the normalized field components
// itmag1 = 1/(1+t dot t);
void ParticleGroupArray::rotate_v(int i, Scalar t1, Scalar t2, Scalar itmag1)
{
	Scalar vprime1 = v1[i] - v3[i]*t2;
	Scalar vprime2 = v2[i] + v3[i]*t1;
	Scalar vprime3 = v3[i] + v1[i]*t2 - v2[i]*t1;
	Scalar a[3];
	a[0] = -vprime3*t2*itmag1;
	a[1] = vprime3*t1*itmag1;
	a[2] = (vprime1*t2 - vprime2*t1)*itmag1;
	add_v(i, a);
}

// advance_x():
// updates the particle positions using the velocities. Recall that x is in
// grid units, and v is in dx/dt units.
// currently only Cartesian!!!
void ParticleGroupArray::advance_x(Scalar dt)
{
	int subcycle = get_species()->get_subcycle(); // add for subcycle, MAL 1/1/10
	if (theFields->get_nsteps() % subcycle != 0) return; // add for subcycle, MAL 1/1/10
	if (theGrid->isuniform())
	{
		//     theGrid->VtogridV(n,v1,dt);
		theGrid->setdtdx(dt); // not used, does nothing?  MAL 1/1/10

		if (is_nr_forced())
		{
			for (int i=0; i<*n_ptr;)
			{

#ifdef DEBUG
				if(i < *n_ptr)
				{
					if(x[i] < 0) { fprintf(stderr, "before move, x[i] < 0\n"); }
				}
#endif
				theGrid->move(x[i], subcycle*v1[i], this, i); // add "subcycle*" for subcycle, MAL 1/1/10

#ifdef DEBUG
				if(i < *n_ptr)
				{
					if(x[i] < 0)
					{
						fprintf(stderr, "i=%d, x=%g, v=%g ", i, x[i], v1[i]);
						fprintf(stderr, "after  move, x[i] < 0\n");
						getchar();
					}
				}
#endif

			}
		}
		else
		{
			Scalar dx;
			for (int i=0; i<*n_ptr;)
			{
				// mindgame: relativistic correction
				dx = convert_to_nr_v(v1[i]);
				theGrid->move(x[i], subcycle*dx, this, i); // add "subcycle*" for subcycle, MAL 1/1/10
			}
		}
		//      theGrid->gridVtoV(n,v1,dt);
	}
	else
	{
		if (is_nr_forced())
		{
			for (int i=0; i<*n_ptr;)
			{ theGrid->move_nonuniform(x[i], subcycle*v1[i], this, i); } // add "subcycle*" for subcycle, MAL 1/1/10
		}
		else
		{
			Scalar dx;
			for (int i=0; i<*n_ptr;)
			{
				// mindgame: relativistic correction
				dx = convert_to_nr_v(v1[i]);
				theGrid->move_nonuniform(x[i], subcycle*dx, this, i); // add "subcycle*" for subcycle, MAL 1/1/10
			}
		}
	}
}

Vector3 ParticleGroupArray::get_v(int i) const
{
	if(v2 && v3)
	{
		return Vector3(v1[i], v2[i], v3[i]);
	}
	else
	{
		return Vector3(v1[i]);
	}
}

void ParticleGroupArray::set_v(int i, Vector3 v)
{
	v1[i] = v.e1();
	if (v2 && v3)
	{
		v2[i] = v.e2();
		v3[i] = v.e3();
	}
}

void ParticleGroupArray::add_v(int i, Scalar *v)
{
	v1[i] += v[0];
	if (v2 && v3)
	{
		v2[i] += v[1];
		v3[i] += v[2];
	}
}

void ParticleGroupArray::add_v(int i, Vector3 v)
{
	v1[i] += v.e1();
	if (v2 && v3)
	{
		v2[i] += v.e2();
		v3[i] += v.e3();
	}
}

Vector3 ParticleGroupArray::get_e(int i)
{
	if(E2 && E3)
	{
		return Vector3(E1[i], E2[i], E3[i]);
	}
	else
	{
		return Vector3(E1[i]);
	}
}

void ParticleGroupArray::get_E(int i, Scalar & e1, Scalar & e2, Scalar & e3)
{
	e1 = E1[i];
	if(E2 && E3)
	{
		e2 = E2[i];
		e3 = E3[i];
	}
}

void ParticleGroupArray::get_E(int i, Vector3 & e)
{
	e.set_e1(E1[i]);
	if (E2 && E3)
	{
		e.set_e2(E2[i]);
		e.set_e3(E3[i]);
	}
	else
	{
		e.set_e2(0);
		e.set_e3(0);
	}
}

void ParticleGroupArray::add_E(int i, Scalar e1, Scalar e2, Scalar e3)
{
	E1[i] += e1;
	if(E2 && E3)
	{
		E2[i] += e2;
		E3[i] += e3;
	}
}

void ParticleGroupArray::set_E(int i, Vector3 &E)
{
	E1[i] = E.e1();
	if (E2 && E3)
	{
		E2[i] = E.e2();
		E3[i] = E.e3();
	}
}

void ParticleGroupArray::reset_E(void)
{
	memset(E1, 0, n*sizeof(Scalar));
	if(E2 && E3)
	{
		memset(E2, 0, n*sizeof(Scalar));
		memset(E3, 0, n*sizeof(Scalar));
	}
}

void ParticleGroupArray::Gather_E(void)
{
	register int i;
	//  Scalar e1, e2, e3;

	//  reset_E();

	//  fprintf(stderr, "\nParticleGroupArray::Gather_E\n");

	if (theFields->is_gridded()){ // gridless field solves write E directly

		if(E2 && E3)
			for(i=0; i < n; i++)
				theFields->get_E(x[i], E1[i], E2[i], E3[i]);
		else
			for(i=0; i < n; i++)
				theFields->get_E(x[i], E1[i]);
	}
}

// accumulate_density(): weights all particles to the mesh to compute the
// number density on the mesh. Differs from number density in the
// normalization q0
// Modified MAL 10/2/09

void ParticleGroupArray::accumulate_density(void)
{
	register int i;
	int ng = theGrid->getng();
	Scalar *species_density = theSpecies->get_species_density();
	Scalar *species_scratch_density = theSpecies->get_species_scratch_density();
	if(theFields->get_nsteps() % 10000 == 0) // For debug, MAL 10/10/09, 7/1/10
		printf("PGA::accum_density, timestep=%li, species=%s, get_n=%i, get_w0=%g\n", theFields->get_nsteps(), theSpecies->get_name().c_str(), get_n(), get_w0()); // MAL 10/10/09
	if (is_variableWeight()) for (i=0; i<get_n(); i++)
	{
		// variable weight particles to the mesh
		theGrid->WeightLinear(x[i], w[i], species_density);
	}
	else
	{
		memset(species_scratch_density, 0, ng*sizeof(Scalar));
		for (i=0; i<get_n(); i++)
		{
			// constant weight particles to the mesh with unit weight, MAL 9/12/09
			theGrid->WeightLinear(x[i], species_scratch_density);
		}
		Scalar w0 = get_w0();
		for (int j=0; j<ng; j++) // only ng multiplies needed, MAL 9/12/09
		{
			species_density[j] += species_scratch_density[j]*w0;
		}
	}
}

// include pseudo-polymorphic code for ()
// this method allows inlining of functions that appear as if virtual
// allowing a single source to supply similar functionality to multiple
// classes without using virtual tables.

#define LOCALCLASS ParticleGroupArray
#include "main/advance_v.hpp"

// mindgame: advance_v for pushing single particle
#define SINGLE
#include "main/advance_v.hpp"
#undef SINGLE
#undef LOCALCLASS

// mindgame: second order loading push
void ParticleGroupArray::advance_vload_2nd(Scalar ddt)
{
	Scalar dttemp = -0.5*ddt;
	Vector3 e_grad, e_dt, e_t, e_v;
	//  Only temporarily.
	//  The following should be eventually generalized to use load section of
	// input file.
	e_dt = 0;
	if (theFields->is_magnetized())
	{
		Vector3 b_grad, b_dt, b_t, b_v;
		b_dt = 0;
		for (int i=0; i<n; i++)
		{
			Scalar vel = get_vMKS(i).e1();
			theFields->get_EBgradient(x[i], e_grad, b_grad);
			theFields->get_EB(x[i], e_t, b_t);
			e_v = e_t-0.25*(e_dt +vel*e_grad)*ddt;
			b_v = b_t-0.25*(b_dt +vel*b_grad)*ddt;
			advance_v(i, dttemp, e_v, b_v);
		}
	}
	else
	{
		for (int i=0; i<n; i++)
		{
			theFields->get_Egradient(x[i], e_grad);
			theFields->get_E(x[i], e_t);
			e_v = e_t-0.25*(e_dt +get_vMKS(i).e1()*e_grad)*ddt;
			advance_v(i, dttemp, e_v);
		}
	}
}

void ParticleGroupArray::advance_vload(Scalar ddt)
{
	advance_vload_2nd(ddt);
}

// mindgame: Depreciated!
//           I don't see any reason that the following should exist.
// WHY IS THIS COMMENTED OUT?????? JH, 1/16/2005
//void ParticleGroupArray::advance_vload(Scalar dt)
//{
//--------------------------------------------------
//second order of accuracy method to advance vecocity half time step
/* register int i;nction to ge
  Load *time;
  Scalar  dO_over_dt;
  Vector3 db_over_dt;
  Vector3 de_over_dt;
  time->get_timedependent(dO_over_dt,db_over_dt,de_over_dt);
  printf("time=%g\n",dO_over_dt);

  Vector3 e,b,b1,b2,grad_e,grad_b,grad_b1,bv,ev;
  Scalar O,grad_O,Ov;
  Scalar qm=theSpecies->get_q_over_m();
  for (i=0;i<n;i++)
  {
  Vector3 v= Vector3(v1[i],v2[i],v3[i]);
  printf("v=%g %g %g %g\n",v.e1(),v.e2(),v.e3(),x[i]);
  theFields->get_E(x[i],e);
  //printf("e=%g\n",e.e1());
  e=e*qm;
  theFields->get_B(x[i],b1);
	b=b1.unit();
      O=qm*b1.magnitude();
      theFields->get_Egradient(x[i],grad_e);
      grad_e=qm*grad_e;
      theFields->get_Bgradient(x[i],grad_b1);
      theFields->get_B(x[i],b2);
      if (grad_b1.isNonZero())
	{
	  grad_b=grad_b1/b2.magnitude();
          grad_O=qm*b2.unit()*grad_b1;
        }
      else
        {
          grad_b=0.*grad_b1;
          grad_O=0;
	}
      Ov=O-0.75*dO_over_dt*dt-0.25*v.e1()*grad_O*dt;
      bv=b-0.75*db_over_dt*dt-0.25*v.e1()*grad_b*dt;
      ev=e-0.75*de_over_dt*dt-0.25*v.e1()*grad_e*dt;

      Scalar tg=tan(Ov*dt/4);
      Scalar h11=1,h12=bv.e3()*tg,h13=-bv.e2()*tg,h21=-bv.e3()*tg,h22=1,h23=bv.e1()*tg,h31=bv.e2()*tg,h32=-bv.e1()*tg,h33=1;
      Scalar coef=get_determination(h11,h12,h13,h21,h22,h23,h31,h32,h33);
      Vector3 k=v-0.5*ev*dt+v%bv*tg;
      Scalar k1=k.e1(),k2=k.e2(),k3=k.e3();
      Scalar vxmatrix=get_determination(k1,h12,h13,k2,h22,h23,k3,h32,h33);
      Scalar vymatrix=get_determination(h11,k1,h13,h21,k2,h23,h31,k3,h33);
      Scalar vzmatrix=get_determination(h11,h12,k1,h21,h22,k2,h31,h32,k3);

      Vector3 V1;

      V1.set_e1(vxmatrix/coef);
      V1.set_e2(vymatrix/coef);
      V1.set_e3(vzmatrix/coef);//get Vn-f/2
      v=V1+1/24.*(v.e1()*(grad_e+grad_O*v%b+O*v%grad_b)+e%b*O/dt+((b*v)*b-v)*O*O+de_over_dt+O*v%db_over_dt+dO_over_dt*v%b)*dt*dt;
      v1[i]=v.e1();
      v2[i]=v.e2();
      v3[i]=v.e3();
      //printf("v[i]=%g %g %g\n",v1[i],v2[i],v3[i]);
  }
  //--------------------------------------------------------------------------------------------------------
theGrid->VtogridV(n,v1,dt);
theGrid->VtogridV(n,v2,dt);
theGrid->VtogridV(n,v3,dt);*/
//}


// WHY IS THIS IN PARTICLEGROUPARRAY?  THIS IS A GENERIC FUNCTION
// AND HAS NOTHING TO DO WITH PARTICLES ETC, JH, 1/16/2005
Scalar ParticleGroupArray::get_determination(Scalar a11,Scalar a12, Scalar a13, Scalar a21, Scalar a22,Scalar a23, Scalar a31, Scalar a32, Scalar a33)
{
	return a11*a22*a33+a12*a23*a31+a13*a21*a32-a31*a22*a13-a21*a12*a33-a11*a32*a23;
}

// copies num values of x (whole array for num = -1)
void ParticleGroupArray::copy_x(Scalar *xcp, int ns, int num)
{
	if(num < 0)
	{
		num = n - ns;
	}
	if(num <= 0) { return; }
	memcpy(xcp, x+ns, num*sizeof(Scalar));
}

// copies num values of x (whole array for num = -1)
// mindgame: multiplication of some value to velocity is necessary
//          when the subcycles of ParticleGroup and Species are different.
void ParticleGroupArray::copy_v(Scalar *vcp, int ns, int dim, int num)
{
	if(num < 0)
	{
		num = n - ns;
	}
	if(num <= 0) { return; }
	memcpy(vcp, v[dim]+ns, num*sizeof(Scalar));
}

void ParticleGroupArray::average_velocity(Vector3 &val, Scalar &www)
{
	int i;
	Scalar weig;
	if(v2 && v3)
	{
		for(i=0; i < n; i++)
		{
			weig = get_w(i);
			val.set_e1(val.e1() + weig * v1[i]);
			val.set_e2(val.e2() + weig * v2[i]);
			val.set_e3(val.e3() + weig * v3[i]);

			//val.set_e1(weig*(val.e1() + v1[i]));
			//val.set_e2(weig*(val.e2() + v2[i]));
			//val.set_e3(weig*(val.e3() + v3[i]));

			www += weig;
		}
	}
	else
	{
		for(i=0; i < n; i++)
		{
			weig = get_w(i);
			val.set_e1(val.e1() + weig* v1[i]);
			www += weig;
		}
	}
}


//CH Lim 11/07/04
//In order to calculate the beam front velocity.
//the function for the position of beam front
Scalar ParticleGroupArray::max_position(void)
{
	int i;
	Scalar position = 0.;
	for(i=0; i < n; i++)
	{
		position = max(position,get_x(i));
	}
	return position;
}


//CH Lim 11/07/04
//The velocity of the particle at front
Vector3 ParticleGroupArray::front_velocity(void)
{
	int i;
	int index = 0;
	Vector3 velocity;

	Scalar position = 0.;
	for(i=0; i < n; i++)
	{
		position = max(position,get_x(i));
		if (get_x(i) == position){
			index = i;
		}
		velocity.set_e1(get_v(i).e1());
		velocity.set_e1(get_v(i).e2());
		velocity.set_e1(get_v(i).e3());
	}
	return velocity;
}


//CH Lim 11/07/04
//The maximum velocity at particle group.
Vector3 ParticleGroupArray::max_velocity(void)
{
	int i;
	Vector3 velocity = 0.;
	for(i=0; i < n; i++)
	{
		velocity.set_e1(max(velocity.e1(),get_v(i).e1()));
		velocity.set_e2(max(velocity.e2(),get_v(i).e2()));
		velocity.set_e3(max(velocity.e3(),get_v(i).e3()));
	}
	return velocity;
}

// sknam: For periodic BC, need to set x when the particle passes the periodic boundary
void ParticleGroupArray::set_x(int i, Scalar xnew)
{
	x[i] = xnew;
}
