#include <stdio.h>

#include "main/pd1.hpp"
#include "main/timing.hpp"


Scalar * Time::ttimes = set_times(); 
TIME_TYPE Time::ts[NTIMES]; // start times
TIME_TYPE Time::te[NTIMES]; // end times

Scalar Time::npartpushed = 0;

int Time::current_type = -1;

Time::Time()
{
     
}

void Time::add_time(int type)
{
  ttimes[type] += (te[type] - ts[type])/TIMESCALE;
}

Scalar Time::total_time(void) 
{ 
  Scalar val = 0.;
  for(int i=0; i < NTIMES; i++) { val += ttimes[i]; }
  return val;
}

void Time::print_description(char *c, int i)
{
  switch(i)
    {
    case PARTICLE_PUSH:
      sprintf(c, "Particle push");
      break;
    case FIELD_SOLVE:
      sprintf(c, "Field solve");
      break;
    case EMISSION:
      sprintf(c, "Emission");
      break;
    case DIAGNOSTICS:
      sprintf(c, "Diagnostics");
      break;
    case INITIALIZATION:
      sprintf(c, "Initialization");
      break;
    case REACTION:
      sprintf(c, "Reaction");
      break; 
    case XGRAFIXTIME:
      sprintf(c, "XGrafix");
      break;
#ifdef MPIDEFINED
    case MPITIME:
      sprintf(c, "MPI communication");
      break;
#endif
    }
}

void Time::print_times(FILE *fp, bool open)
{
  char description[STR_SIZE];
  Scalar tot;      
  
  if(open) { fp = fopen("pd1_timings.txt", "w"); }
  
  fprintf(fp, "pd1 timings:\n");
  tot = total_time();      
  
  for(int i=0; i < NTIMES; i++)
    {
      print_description(description, i);
      fprintf(fp, "%s: %g [%.1f%%]\n", description, 
	      ttimes[i], 100.0*ttimes[i]/tot);
    }
  fprintf(fp, "--> Total time: %g\n", tot);
  fprintf(fp, "%g particles pushed total\n", npartpushed);
  // mindgame: only when ttimes[] has non-zero value
  if (ttimes[PARTICLE_PUSH])
  {
      fprintf(fp, "%g particles per second\n", 
	      npartpushed/ttimes[PARTICLE_PUSH]);
  }
  
  if(open) { fclose(fp); }
}

void Time::start_time(int type)
{
  static bool init = false;
  
  if(!init)
    {
      for(int i=0; i < NTIMES; i++)
	{
	  ttimes[i] = 0.;
	}
      current_type = -1;
      npartpushed = 0;
      init = true;
    }
  
  if(type < 0) { return; }
  
  ts[type] = TIME_GET ;
  current_type = type;
}

void Time::end_time(int type)
{
  if(type < 0) { return; }
  
  te[type] = TIME_GET;
  add_time(type);
  current_type = -1;
}

void Time::start_particle_time(int np)
{

  // testing why np is always 1, JH, 9/21/2004
#ifdef DEBUG
  fprintf(stderr, "> %d particles moved\n", np);
#endif

  npartpushed += np;
  start_time(PARTICLE_PUSH);
}
