#include <stdio.h>

#include<vector>
using namespace std;

#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "utils/ostring.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/particlegrouparray.hpp"
#include "fields/fields.hpp"
#include "main/boundary.hpp"
#include "main/secondary.hpp"
#include "main/drive.hpp"
#include "main/circuit.hpp"
#include "utils/trimatrix.hpp"
#include "main/grid.hpp"
#include "main/conductor.hpp"
#include "fields/poissonfield.hpp"
#include "distributions/dist_xv.hpp"
#include "distributions/dist_v.hpp" // add for evolvingfluid_BC, MAL200223

#include "main/boundary_inline.hpp"
#include "main/grid_inline.hpp"
#include "fields/fields_inline.hpp"

Conductor::Conductor(Fields *t, int i) : Boundary(t)
{
	material = CONDUCTOR;
	property = ABSORBED;
	Vminus = Vplus = 0.;
	setdefaults();  // from parse
	setparamgroup();
	j0 = j1 = i;
	check();
	init();
}

Conductor::Conductor(FILE *fp, 
		oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables, Fields *t) :
		  Boundary(t)
{
	material = CONDUCTOR;
	property = ABSORBED;
	Vminus = Vplus = 0.;
	init_parse(fp, float_variables, int_variables);
}

Conductor::~Conductor() 
{
	if(thecircuit != NULL) { delete thecircuit; }
}


Property Conductor::particle_BC(ParticleGroup *p, int i, Scalar frac, Direction j)
{
	Scalar frac2;

	frac2 = (this->*advance_v_for_synchro)(p, i, frac, j);

	if(accumulate_particles) { accumulate_particle(p, i, frac2, j, false); }

	secondary_emission(p, i, frac2, j);
	radiation(p, i);

	absorb(p, i, j);
	return property;
}

Scalar Conductor::fluid_BC(Scalar density, VDistribution *v, Direction dir,
                          vector<Scalar> * moments)
{
  Scalar number;  // number of particles that impact the boundary
  
  // calculate number flux (0th moment)
  
  number = v->ave_1st_mnt_on_boundary(1, dir);
  number *= get_A(dir)*density;
  
  if(debug())
  {
    fprintf(stderr, "Conductor: %s, number=%g, density=%g\n",
            get_name().c_str(), number, density);
  }
  if(!moments) { return number; }
  if(!moments->size()) { return number; }
  
  (*moments)[0] = number;
 
  // calculate higher moments -- to be done, JH, 6/2006
  return number;
}

// Conductor::evolvingfluid_BC, to calculate the outgoing fluxes at the boundaries, MAL200223
// called by DiffusionFluid::interact_evolvingfluid_with_boundaries in DiffusionFluid::update
void Conductor::evolvingfluid_BC(Species * ispecies, Scalar density, Scalar recombcoef, VDistribution *v, Direction dir)  // add, MAL200223
{
  if (density == 0) return; // add, since no fluid-boundary interaction, MAL200315
  Scalar number;  // number of particles that impact the boundary
  // calculate number flux (0th moment); fluxout = 0.25*n*vbar/(1-0.5*recombcoef); vbar=sqrt(8*e*Te/pi*M)
  number = fabs(v->ave_1st_mnt_on_boundary(1, dir)); // use fabs; number is positive at both boundaries
  number *= get_A(dir)*density/(1-0.5*recombcoef);
//  if(debug())
  {
//    printf("\n\nConductor::evolvingfluid_BC: %s, species=%s, number=%g, density=%g", get_name().c_str(),ispecies->get_name().c_str(), number, density);
  }
  evolvingfluid_secondaryemission(ispecies, number, v, dir);
}

// Add circuit current and power diagnostics, MAL 12/9/09
void Conductor::update_BC(void)
{
	//	printf("\nConductor::update_BC(), circuitQ=%g, w00=%g, w10=%g, w01=%g, w11=%g, w02=%g, w12=%g", circuit_Q,
	//		weights[0][0],weights[1][0],weights[0][1],weights[1][1],weights[0][2],weights[1][2]); //for debug dumpfile, MAL 5/24/10
	// Update RLC circuit.  This won't work for a circuit attached
	// to a conductor with finite thickness.  JDN.
	if (j0 != j1) error("In Conductor::update_BC, j0 != j1\n"); // MAL 12/6/09
	if(thecircuit->get_sourcetype() == VOLTAGESOURCE && !idealvoltagesource) {
		if(debug()) {
			fprintf(stderr, "x = %g, j0 = %i, j1 = %i\n", thegrid->getX(j0), j0, j1); }

		if (j1 == 0) { // RLC circuit attached to LH boundary.
			thecircuit->update_RLC(thefield->get_phi(j1),calculate_Q(POSITIVE),
					thegrid->get_area(j1)); // change getA(j1) to get_area(j1), MAL 4/21/09
			calculate_RLC_I_and_P(j1); // Add circuit current and power diagnostic, MAL 12/9/09
		}

		else if (j0 == thegrid->getng()-1) { // RLC circuit attached to RH boundary.
			thecircuit->update_RLC(thefield->get_phi(j0),calculate_Q(NEGATIVE),
					thegrid->get_area(j0)); // change getA(j0) to get_area(j0), MAL 4/21/09
			calculate_RLC_I_and_P(j0); // Add circuit current and power diagnostic, MAL 12/9/09
		}

		else { // Internal node.
			thecircuit->update_RLC(thefield->get_phi(j0),calculate_Q(),
					thegrid->get_area(j0)); // change getA(j0) to get_area(j0), MAL 4/21/09
			calculate_RLC_I_and_P(j0); // Add circuit current and power diagnostic, MAL 12/9/09
		}

	}
	else { // Update ideal voltage source or ideal current source
		thecircuit->update();
		if (thecircuit->get_sourcetype() == CURRENTSOURCE) { // Add circuit current and power diagnostic for a current source, MAL 12/5/09
			calculate_idealcurrent_I_and_P(j0);
		}
		if (thecircuit->get_sourcetype() == VOLTAGESOURCE && idealvoltagesource) { // Add current and power for an ideal voltage source, MAL 12/5/09
			calculate_idealvoltage_I_and_P(j0);
		}
	}
	if (thefield->get_naverage() > 1) {
		time_average(P_circ, tempP_circ, aveP_circ); // Add circuit time average power diagnostic, MAL 12/5/09
	}
	update_BC_top();
	zero_weights();
} 

// For Poisson Solver
void Conductor::modify_coeffs(PoissonSolver * theps)
{
	if(idealvoltagesource)
	{
		for(int j = j0; j <= j1; j++)
		{
			theps->set_coeff(j, 0., 1., 0.);
			theps->set_rhs(j, thecircuit->get_sourcestrength());
		}
	}
	else
	{
		if(thecircuit->get_sourcetype() == CURRENTSOURCE)
		{
			// will the Boltzmann solver work with a current source?
			// should be checked/implemented, JH, 3/8/2005
			if(theps->is_nonlinear())
			{
				fprintf(stderr,
						"\nWARNING, using Boltzmann model+current source\n");
			}

			if(j0 != j1)
			{
				error("Conductor::currentsource", "j0 != j1");
			}

			circuit_Q = thecircuit->get_Q();
			// Current source must be attached to conductor with zero thickness.
			theps->add_charge(j0, sum_Q()+circuit_Q);
		}
		else  // RLC circuit
		{
			if (j0 == thegrid->getng() - 1) { // RH boundary.
				lhs_coeff = 1/thecircuit->get_alpha_zero();
				lhs_coeff /=thegrid->getV(j0);
				theps->add_coeff(j0,0.,lhs_coeff,0.);

				rhs_coeff  = thecircuit->get_rhs_coeff(calculate_Q(NEGATIVE),
						thegrid->get_area(j0)); // change getA(j0) to get_area(j0), MAL 4/21/09
				rhs_coeff /= thegrid->getV(j0);
				theps->add_rhs(j0, rhs_coeff);
			}
			else if (j1 == 0) {  // LH boundary.
				lhs_coeff = 1/thecircuit->get_alpha_zero();
				lhs_coeff /=thegrid->getV(j1);
				theps->add_coeff(j1,0.,lhs_coeff,0.);

				rhs_coeff  = thecircuit->get_rhs_coeff(calculate_Q(POSITIVE),
						thegrid->get_area(j1)); // change getA(j0) to get_area(j0), MAL 4/21/09
				rhs_coeff /= thegrid->getV(j1);
				theps->add_rhs(j1, rhs_coeff);
				if(debug()) {
					fprintf(stderr, "modify_coeffs: lhs_coeff = %g, rhs_coeff = %g, j1 = %i, get_area = %g, getA= %g, getV = %g\n",
							lhs_coeff, rhs_coeff, j1, thegrid->get_area(j1), thegrid->getA(j1), thegrid->getV(j1)); }
			}
			else { // Interior node.
				lhs_coeff = 1/thecircuit->get_alpha_zero();
				lhs_coeff /=thegrid->getV(j0);
				theps->add_coeff(j0,0.,lhs_coeff,0.);

				rhs_coeff  = thecircuit->get_rhs_coeff(calculate_Q(),
						thegrid->get_area(j0)); // change getA(j0) to get_area(j0), MAL 4/21/09
				rhs_coeff /= thegrid->getV(j0);
				theps->add_rhs(j0, rhs_coeff);
			}
		}
	}
}

Scalar Conductor::get_sigma(unsigned int i, Scalar * phi, Scalar * rho, bool coulombs)
{  
  static Scalar Vminus = 0.;
  static Scalar Vplus = 0.;
	static Scalar area_plus = 0.;
	static Scalar area_minus = 0.;

	if(!(Vplus + Vminus))
	{
		Scalar rp = get_rplus();
		Scalar rm = get_rminus();

		Vminus = Vplus = 0.;
		if(j0 > 0)
		{
			Vminus = thegrid->getV(rm, get_r0());
		}

		if(j1 < thegrid->getnc())
		{
			Vplus = thegrid->getV(get_r1(), rp);
		}

		area_minus = thegrid->getA(rm);
		area_plus = thegrid->getA(rp);
	}
  
	Scalar sigma;
	Scalar Eminus, Eplus;
	Scalar area, volume;
	Eplus = Eminus = 0.;
	area = volume = 0.;

	if((i==(unsigned int)j0) && (i > 0))
	{
		Eminus = (phi[i] - phi[i-1])*Aminus;
		area += area_minus;
		volume += Vminus;
	}

	if((i==(unsigned int)j1) && (i < (unsigned int)thegrid->getnc()))
	{
		Eplus = (phi[i] - phi[i+1])*Aplus;
		area += area_plus;
		volume += Vplus;
	}

	// subtract off space charge
	sigma = Eplus + Eminus - rho[i]*volume;
	if(!coulombs) { sigma /= area; }

	return sigma;
}

// For RLC circuit source current and power diagnostic, MAL 12/5/09
void Conductor::calculate_RLC_I_and_P(unsigned int j)
{
	if (thefield->get_nsteps() > 2)
	{
		I_circ = thecircuit -> get_circ_I(); // circuit current
		P_circ = I_circ * thefield->get_phi(j); // circuit power
	}
}

// For ideal current source current and power diagnostic, MAL 12/6/09
void Conductor::calculate_idealcurrent_I_and_P(unsigned int j)
{
	I_minushalfdt = I_plushalfdt; // circuit current at time t - dt/2
	I_plushalfdt = thecircuit->get_sourcestrength(); // circuit current at time t + dt/2
	I_circ = 0.5*(I_minushalfdt + I_plushalfdt);
	P_circ = I_circ * thefield->get_phi(j); // assumes j0 == j1, as tested in update_BC, MAL 12/5/09
}

// For ideal voltage source current and power diagnostic, MAL 12/10/09
// Tested with single charge sheets emitted by opposite boundary
void Conductor::calculate_idealvoltage_I_and_P(unsigned int j)
{
	dQ_circ2 = dQ_circ1;
	dQ_circ1 = dQ_circ;
	// Due to finite differencing for the current, one MUST subtract the CUMULATIVE TOTAL particle charge from dQ_circ
	dQ_circ = get_sigma(j, thefield->get_phi(), thefield->get_rho(), true) - sum_Q(); // charge supplied by the source at time t
	if (dQ_circ2 == 3.24E22) return; // need at least three timesteps to evaluate derivative for I_circ
	// I_circ is noisy due to the particle current, see Verboncoeur et al, J.Comp.Phys. 104, 321 (1993)
	I_circ = (2*(dQ_circ-dQ_circ1) + 0.5*(dQ_circ2-dQ_circ))/thefield->get_dt(); // 2nd order accurate backward derivative
	//        I_circ = (dQ_circ - dQ_circ1) / thefield->get_dt();
	P_circ = I_circ * thefield->get_phi(j); // assumes j0 == j1, tested in update_BC
}

void Conductor::setdefaults(void)
{
	thecircuit = NULL;
	classname = "Conductor";
	add_child_name("Circuit");
	I_plushalfdt = 0; I_minushalfdt = 0; // For current source current diagnostic, MAL 12/5/09
	//	Artificial value, reset after three timesteps, for I_circ in calculate_idealvoltage_I_and_P, MAL 5/21/10
	dQ_circ = 3.24E22; dQ_circ1 = 3.24E22; dQ_circ2 = 3.24E22; // For voltage source current diagnostic
	tempP_circ = 0; // for time average power diagnostics
}

void Conductor::setparamgroup(void)
{
}

void Conductor::check_conductor(void)
{
	if(thecircuit == NULL)
	{
		thecircuit = new Circuit(thefield);
		fprintf(stderr, "\n*Conductor, node %d, circuit not present --",
				j0);
		fprintf(stderr, " using grounded conductor");
	}
	if(j1 == -1)  { j1 = j0; }
}

void Conductor::init_conductor(void)
{
	surface_npoints = NONE;  // NONE == 2
	npoints = j1 - j0 + 1;

	Scalar delxplus, delxminus, xplus, xminus;

	idealvoltagesource = thecircuit->idealvoltagesource();

	Aplus = Aminus = 0.;
	V = 0.;

	if(j1 < thegrid->getng()-1)
	{
		delxplus = thegrid->getX(j1+1) - thegrid->getX(j1);
		xplus = thegrid->getX(j1) + 0.5*delxplus;
		Aplus = thegrid->getA(xplus)/delxplus;
		Aplus *= thefield->get_epsilon(j1);
		V += thegrid->getV(thegrid->getX(j1), xplus);
	}
	if(j0 > 0)
	{
		delxminus = thegrid->getX(j0) - thegrid->getX(j0-1);
		xminus = thegrid->getX(j0) - 0.5*delxminus;
		Aminus = thegrid->getA(xminus)/delxminus;
		Aminus *= thefield->get_epsilon(j0-1);
		V += thegrid->getV(xminus, thegrid->getX(j0));
	}

	thecircuit->update(false);
}

void Conductor::check(void)
{
	// mindgame: check_conductor() should be done first.
	check_conductor();
	check_boundary();
}

void Conductor::init(void)
{
	set_msg(classname);
	// mindgame: init_conductor() should be done first.
	init_conductor();
	init_boundary();
}

Parse * Conductor::special(ostring s)
{
	Parse * i;
	i = special_boundary(s);
	if(!i)
	{
		return special_conductor(s);
	}
	else
	{
		return i;
	}
}

Parse * Conductor::special_conductor(ostring s)
{
	if(s == "Circuit")
	{
		if(thecircuit != NULL)
		{
			error("Circuit already initialized!");
		}
		thecircuit = new Circuit(f, pg.float_variables, pg.int_variables,
				thefield);
		return thecircuit;
	}
	return 0;
}


