#include <stdio.h>

#include "main/pd1.hpp"
#include "main/oopiclist.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/particlegrouparray.hpp"
#include "main/grid.hpp"
#include "fields/fields.hpp"
#include "fields/fields_inline.hpp"
#include "main/boundary.hpp"
#include "main/boundary_inline.hpp"
#include "emmiter/emitter.hpp"
#include "emmiter/beamemitter.hpp"
#include "fields/fieldemitter.hpp"
#include "emmiter/secondaryemitter.hpp"
#include "main/timing.hpp"
#include "main/secondary.hpp"
#include "distributions/dist_xv.hpp"
#include "main/ndarray.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "main/uniformmesh.hpp"
#include "distributions/dist_binned.hpp"
#include "distributions/dist_xv.hpp"
#include "distributions/dist_v.hpp"
#include "utils/VectorArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_time.hpp"
#include "diagnostics/diagnosticcontrol.hpp"
#include "main/radiation.hpp"
#include "main/grid_inline.hpp"
#include "xgio.h" // for access to XGRead() for reload_weights, MAL 5/16/10

Boundary::Boundary(Fields *t)
{
	classname = "Boundary";
	nspecies = 0;
	weights = 0;
	thefield = t;
	theSpeciesList = thefield->get_specieslist();
	thegrid = thefield->get_theGrid();

	setdefaults();  // from parse
	setparamgroup();

	accumulate_particles = false;
	calculate_current = true;

	material = EMPTY;
	// transmit particles by default
	property = TRANSMITTED;
}

Boundary::Boundary(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables, Fields *t)
{
	nspecies = 0;
	weights = 0;
	thefield = t;
	theSpeciesList = thefield->get_specieslist();
	thegrid = thefield->get_theGrid();
	accumulate_particles = false;
	calculate_current = true;
	material = EMPTY;
	// transmit particles by default
	property = TRANSMITTED;
	init_parse(fp, float_variables, int_variables);
}

Boundary::~Boundary()
{
  for(int i=0; i < nspecies; i++)
    {
      delete particles[i];
    }
  if(nspecies)
    {
      delete [] particles;
      delete [] I_sp;
      delete [] absorbed_particles; // HH 04/28/16
      delete [] emitted_particles; // HH 04/28/16
    }

	delete[] E;
	delete[] E_dt;
	delete[] E_grad;
	delete[] B;
	delete[] B_dt;
	delete[] B_grad;

	if(weights)
	{
		for(int i=0; i < surface_npoints; i++)
		{
			delete [] weights[i];
			delete [] weights_old[i];
		}
		delete [] weights;
		delete [] weights_old;
	}

	if (usable) delete [] usable;

  emitterlist.deleteAll();
  distlist.deleteAll();
  secondarylist.deleteAll();
  radiationlist.deleteAll();

  delete [] Ickt_array; // for spatiotemporal diagnostics, HH 05/09/16
  delete [] Iconv_array; // for spatiotemporal diagnostics, HH 05/09/16
}

Scalar Boundary::get_rminus() const
{
	Scalar rm;
	if(j0)
	{
		rm = thegrid->getX(j0) - 0.5*thegrid->get_dxcell(j0-1);
	}
	else
	{
		rm =  thegrid->getx0();
	}
	return rm;
}

Scalar Boundary::get_rplus() const
{
  Scalar rm;
  if(j1 < thegrid->getnc())
    {
      rm = thegrid->getX(j1) + 0.5*thegrid->get_dxcell(j1);
    }
  else
    {
      rm = thegrid->getx1();
    }
  return rm;
}


void Boundary::update_BC()
{
	update_BC_top();
}

// mindgame: For the 2nd order push for particle collection
void Boundary::update_BC_fields()
{
	thefield->get_E(j1, E_dt[POSITIVE]);
	thefield->get_E(j0, E_dt[NEGATIVE]);
	thefield->get_B(j1, B_dt[POSITIVE]);
	thefield->get_B(j0, B_dt[NEGATIVE]);
	for (int j=0; j<surface_npoints; j++)
	{
		// Temporarily, X_grad[] is used to save the old electric field
		E_grad[j] = E[j];
		B_grad[j] = B[j];
		E[j] = E_dt[j];
		B[j] = B_dt[j];
		E_dt[j] -= E_grad[j];
		B_dt[j] -= B_grad[j];
	}
	thefield->get_EBgradient_plus(j1, E_grad[POSITIVE], B_grad[POSITIVE]);
	thefield->get_EBgradient_minus(j0, E_grad[NEGATIVE], B_grad[NEGATIVE]);
}

void Boundary::get_E(Direction dir, Vector3 &E_n) const
{
	E_n = E[dir];
}

void Boundary::get_Egradient(Direction dir, Vector3 &E_n) const
{
	E_n = E_grad[dir];
}

void Boundary::get_dE(Direction dir, Vector3 &E_n) const
{
	E_n = E_dt[dir];
}

void Boundary::get_B(Direction dir, Vector3 &B_n) const
{
	B_n = B_dt[dir];
}

void Boundary::get_Bgradient(Direction dir, Vector3 &B_n) const
{
	B_n = B_grad[dir];
}

void Boundary::get_dB(Direction dir, Vector3 &B_n) const
{
	B_n = B_dt[dir];
}

void Boundary::get_EB(Direction dir, Vector3 &E_n, Vector3 &B_n) const
{
	E_n = E[dir];
	B_n = B[dir];
}

void Boundary::get_EBgradient(Direction dir, Vector3 &E_n, Vector3 &B_n) const
{
	E_n = E_grad[dir];
	B_n = B_grad[dir];
}

Scalar Boundary::getr0() const { return thegrid->getX(j0); }
Scalar Boundary::getr1() const { return thegrid->getX(j1); }


void Boundary::update_BC_top()
{
	Time thetimes;

	update_BC_fields();
	//  	calculate_E();

	dt = thefield->get_dt();

	// emit particles
	Scalar t1, t2;

	t1 = thefield->get_t();
	t2 = t1 + dt;

	thetimes.start_time(EMISSION);
	for(emitter_iter.restart(); !emitter_iter.Done(); emitter_iter++)
	{
		emitter_iter()->emit(t1, t2);
	}
	thetimes.end_time(EMISSION);

	// update weights (->charge)
	for (int i=0; i<surface_npoints; i++)
		memcpy(weights_old[i], weights[i], nspecies*sizeof(Scalar));
}


void Boundary::update_BC_post()
{
	process_particles();

	for(emitter_iter.restart(); !emitter_iter.Done(); emitter_iter++)
	{
		emitter_iter()->add_particles_to_list();
	}

	calculate_I();
	// Added for phi diagnostic.
	calculate_phi();
}

// particle_BC:  passes particle group and index i
// p: ParticleGroup of particle
// i: index of particle
// frac: fraction of time remaining in timestep
// mindgame: 'dir' is the direction in the boundary,
// not of impacting particles.
Property Boundary::particle_BC(ParticleGroup *p, int i, Scalar frac,
		Direction dir)
{
	if(accumulate_particles)
	{
		accumulate_particle(p, i, frac, dir, true);
	}

	if(calculate_current)
	{
		weights[dir][p->get_species()->get_ident()] += p->get_w(i);
	}

	return property;
}

// NO LONGER USED; REPLACED WITH evolvingfluid_BC, MAL200303
// interacts fluid with boundaries
// returns number lost in units of [1/s]
// how does the sign of number depend on dir? JH, June 15, 2006
Scalar Boundary::fluid_BC(Scalar density, VDistribution *v, Direction dir,
		vector<Scalar> * moments)
{
	Scalar number;  // number of particles that impact the boundary

	// calculate number flux (0th moment)

	number = v->ave_1st_mnt_on_boundary(1, dir);
	number *= get_A(dir)*density;

	if(debug())
	{
		fprintf(stderr, "%s, number=%g, density=%g\n",
				get_name().c_str(), number, density);
	}

	if(!moments) { return number; }
	if(!moments->size()) { return number; }

	(*moments)[0] = number;

	// calculate higher moments -- to be done, JH, 6/2006


	return number;
}
void Boundary::evolvingfluid_BC(Species * ispecies, Scalar density, Scalar recombcoef, VDistribution *v, Direction dir){ } // add, MAL200223

void Boundary::setdefaults()
{
	j0 = j1 = -1;
	nmax = NMAX;
	Q = 0.;
	I_circ = 0; P_circ = 0; aveP_circ = 0; // For diagnostics, MAL 12/4/09
	weights = 0;
	weights_old = 0;
	name = "";
	name2= "";
	name3= "";
	surface_npoints = 2; // 2 == NONE : number of surface points
	npoints = 1;
	usable = 0;
	E = E_dt = E_grad = 0;
	B = B_dt = B_grad = 0;
	dt = 0;
	accuracy_synchro = 2;
	advance_v_for_synchro = NULL;

  add_child_name("BeamEmitter");
  add_child_name("FieldEmitter");
  add_child_name("Distribution");
  add_child_name("Secondary");

  absorbed_particles = 0; // HH 04/28/16
  emitted_particles = 0; // HH 04/28/16
  V_DC = 0; // HH 05/06/16
  Delta_V_DC = 0.05; // HH 05/06/16
  C = 0.1; // HH 05/06/16

  Ickt_array = 0; // HH 05/08/16
  Iconv_array = 0; // HH 05/08/16
}

void Boundary::setparamgroup()
{
	pg.add_string("name", &name, "boundary name");
	pg.add_iparameter("nmax", &nmax);
	pg.add_iparameter("j0", &j0, "initial grid number of boundary");
	pg.add_iparameter("j1", &j1, "terminating grid number of boundary");
}

void Boundary::check_boundary()
{
	if(nmax <= 0) { error("nmax <= 0"); }
	if(j1 < j0)   { error("j1 < j0"); }
	if(j0 < 0)    { error("location j0 not initialized"); }
	if(j1 >= thegrid->getng())
	{
		error("location j1 outside of grid!");
	}

	if(name == "")
	{
		ostring number1 = j0, number2 = j1;
		name = "Boundary(";
		name += number1;
		name += ",";
		name += number2;
		name += ")";
	}

	if(name2 == "")
	{
		ostring number1 = j0, number2 = j0;
		name2 = "Boundary(";
		name2 += number1;
		name2 += ",";
		name2 += number2;
		name2 += ")";
	}

	if(name3 == "")
	{
		ostring number1 = j1, number2 = j1;
		name3 = "Boundary(";
		name3 += number1;
		name3 += ",";
		name3 += number2;
		name3 += ")";
	}

}

void Boundary::init()
{
	set_msg(classname);
	init_boundary();
}

void Boundary::check()
{
	check_boundary();
}

void Boundary::init_boundary()
{
	nspecies = theSpeciesList->nItems();

	usable = new bool[surface_npoints]; // always 2?, JH, Feb 10, 2006

	weights = new Scalar * [surface_npoints];
	weights_old = new Scalar * [surface_npoints];

	E = new Vector3[surface_npoints];
	E_dt = new Vector3[surface_npoints];
	E_grad = new Vector3[surface_npoints];
	B = new Vector3[surface_npoints];
	B_dt = new Vector3[surface_npoints];
	B_grad = new Vector3[surface_npoints];
	for(int j=0; j < surface_npoints; j++)
	{
		E[j] = B[j] = 0;
	}

	dt = thefield->get_dt();

	particles = new ParticleGroup * [nspecies];
	I_sp = new Scalar[nspecies];
	for(int j=0; j < surface_npoints; j++)
	{
		weights[j] = new Scalar[nspecies];
		weights_old[j] = new Scalar[nspecies];
		for(int i=0; i < nspecies; i++)
		{
			weights[j][i] = 0.;
			weights_old[j][i] = 0.;
			if (j==0)
			{
				particles[i] = NULL;
				I_sp[i] = 0.;
			}
		}
	}

	species_iter.restart(*theSpeciesList);
	for(species_iter.restart(); !species_iter.Done(); species_iter++)
	{
		if(particles[species_iter()->get_ident()] == NULL)
		{
			switch(species_iter()->get_particlegrouptype())
			{
			case PARTICLEGROUPARRAYTYPE:
				particles[species_iter()->get_ident()] =
						new ParticleGroupArray(nmax, species_iter(), thefield, 1., true,
								true, true);
				break;
			default:
				error("init_boundary", "Unrecognized particle group type");
			}
		}
		else
		{
			error("Duplicate species tags");
		}
	}
	secondary_iter.restart(secondarylist);
	dist_iter.restart(distlist);
	radiation_iter.restart(radiationlist);
	emitter_iter.restart(emitterlist);
	// set location of Emitters
	for(emitter_iter.restart(); !emitter_iter.Done(); emitter_iter++)
	{
		emitter_iter()->set_position();
	}

  switch (accuracy_synchro)
  {
    case 0:
      advance_v_for_synchro = &Boundary::advance_v_for_synchro_0th;
      break;
    case 1:
      advance_v_for_synchro = &Boundary::advance_v_for_synchro_1st;
      break;
    case 2:
      advance_v_for_synchro = &Boundary::advance_v_for_synchro_2nd;
      break;
    default:
      terminate_run("synchro_accuracy: Either Not Implemented or Wrong Accuracy Type.");
      break;
  }

    absorbed_particles = new double [nspecies]; // HH 04/28/16
    emitted_particles = new double [nspecies]; // HH 04/28/16

}

void Boundary::init_arrays(int size_array) // allocate and initialize Spatiotemporal arrays, HH 04/18/16
// Called only once in Spatialregion::update, so that everything else is already initialized.
{

if (Ickt_array) { delete [] Ickt_array;}
Ickt_array = new Scalar[size_array];
        for (int m=0; m<size_array; m++)
        {
            Ickt_array[m] = 0;
        }

if (Iconv_array) { delete [] Iconv_array;}
Iconv_array = new Scalar[size_array*nspecies];

        for (int m=0; m<(size_array); m++)
        {
            Iconv_array[m] = 0;
        }

}

void Boundary::set_arrays(int steps_rf, int m) // set Spatiotemporal arrays, HH 05/09/16
{
    for(int n_sp=0; n_sp<theSpeciesList->nItems(); n_sp++)
    {

	Iconv_array[n_sp*steps_rf+m] += *(I_sp+n_sp);
    }

    Ickt_array[m] += I_circ;

}

Parse * Boundary::special(ostring s)
{
	return special_boundary(s);
}

Parse * Boundary::special_boundary(ostring s)
{
	if(s == "BeamEmitter")
	{
		BeamEmitter *thebeamemitter;

		thebeamemitter = new BeamEmitter(f, pg.float_variables,
				pg.int_variables, thefield, this);

		emitterlist.add(thebeamemitter);

		return thebeamemitter;
	}
	if(s == "FieldEmitter")
	{
		FieldEmitter *thefieldemitter;

		thefieldemitter = new FieldEmitter(f, pg.float_variables,
				pg.int_variables, thefield, this);

		emitterlist.add(thefieldemitter);

		return thefieldemitter;
	}
	if(s == "Distribution")
	{
		BinnedDistribution *thedist;

		thedist = new BinnedDistribution(f, pg.float_variables, pg.int_variables, this);
		accumulate_particles = true;
		distlist.add(thedist);

		return thedist;
	}
	if(s == "Secondary")
	{
		Secondary *thesecondary;

		thesecondary = new Secondary(f, pg.float_variables,
				pg.int_variables,thefield, this);

		emitterlist.add(thesecondary->get_emitter(),false);

		accumulate_particles = true;
		secondarylist.add(thesecondary);

		return thesecondary;
	}
	if (s == "Radiation")
	{
		Radiation *theradiation;
		theradiation = new Radiation(f, pg.float_variables, pg.int_variables,thefield);
		accumulate_particles = true;
		radiationlist.add(theradiation);
		return theradiation;
	}

	return 0;
}

void Boundary::process_particles()
{
	accumulate_distribution();
	delete_particles();
}

// add impacted particles to binned distribution function
void Boundary::accumulate_distribution()
{
	int i;

	for(dist_iter.restart(); !dist_iter.Done(); dist_iter++)
	{
		for(i=0; i < theSpeciesList->nItems(); i++)
		{
			if(dist_iter()->is_species(i))
			{
				dist_iter()->add_particles(particles[i]);
			}
		}
	}
}

void Boundary::delete_particles()
{
	for(int i=0; i < theSpeciesList->nItems(); i++)
	{
		particles[i]->delete_all();
	}
}

// reload boundary weights from dumpfile, called by SpatialRegion::reload_dumpfile_SR, MAL 5/16/10
void Boundary::reload_weights(FILE * DMPFile)
{
	int i, j;
	Scalar wji;
	for (species_iter.restart(), i = 0; !species_iter.Done(); species_iter++, i++)
	{
		for (j = 0; j < surface_npoints; j++)
		{
			XGRead(&wji,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
			weights[j][i] = wji;
		}
	}
}

Scalar Boundary::calculate_sp_Q(Species *thes)
{
	Scalar sum = 0.;
	for (int j=0; j < surface_npoints; j++)
	{
		if (is_usable(j))
			sum += weights[j][thes->get_ident()]*thes->get_q();
	}
	return sum;
}

Scalar Boundary::calculate_sp_Q(Species *thes, Direction dir)
{
	return   weights[dir][thes->get_ident()]*thes->get_q();
}

Scalar Boundary::calculate_sp_I(Species *thes)
{
	int i = thes->get_ident();
	I_sp[i] = 0;
	for (int j=0; j < surface_npoints; j++)
	{
		if (is_usable(j))
			I_sp[i] += thes->get_q()*(weights[j][i] -
					weights_old[j][i])/dt;
	}
	return I_sp[i];
}

Scalar Boundary::calculate_Q()
{
	Scalar sum = 0.;
	for(species_iter.restart(); !species_iter.Done(); species_iter++)
	{
		for (int j=0; j < surface_npoints; j++)
		{
			if (is_usable(j))
				sum += weights[j][species_iter()->get_ident()]*species_iter()->get_q();
		}
	}
	return sum;
}

Scalar Boundary::calculate_Q(Direction dir)
{
	Scalar sum = 0.;
	for(species_iter.restart(); !species_iter.Done(); species_iter++)
	{
		sum += weights[dir][species_iter()->get_ident()]*species_iter()->get_q();
	}
	return sum;
}

// Following function added by JDN to keep a running total of charge
// deposited on a conductive boundary, convectively. "sum_convQ" is not reset
// each time step.  "weights[][]" is reset each time step.
Scalar Boundary::sum_Q()
{
	for (species_iter.restart(); !species_iter.Done(); species_iter++)
	{
		for (int j=0; j< surface_npoints; j++)
		{
			if (is_usable(j))
				sum_convQ += weights[j][species_iter()->get_ident()]*species_iter()->get_q();
		}
	}
	return sum_convQ;
}

Scalar Boundary::calculate_I()
{
	Scalar sum = 0.;
	for(species_iter.restart(); !species_iter.Done(); species_iter++)
	{
		sum += calculate_sp_I(species_iter());
	}
	return sum;
}

void Boundary::zero_weights()
{
	for(int j=0; j < surface_npoints; j++)
	{
		for(int i=0; i < theSpeciesList->nItems(); i++)
		{
			weights[j][i] = 0.;
		}
	}
}

Scalar Boundary::get_Q_and_delete()
{
	Scalar val = calculate_Q();
	zero_weights();
	return val;
}

// mindgame: 'dir' is the direction in the boundary, not of impacting particles.
void Boundary::secondary_emission(ParticleGroup *p, int i, Scalar frac,
		Direction dir)
{

  // Find the flux to the wall in each timestep. Every particle that hits the
  // wall will be absorbed, but num_emitted secondaries are emitted, HH 04/28/16.
  int num_emitted = 0;
  double np2c_sec = 0;
  int jj=0; //MAL200312
  for(secondary_iter.restart(); !secondary_iter.Done(); secondary_iter++)
    {
      jj++; //MAL200312
//      printf("\nBoundary::secondary_emission: name=%s, dir=%i, jj=%i, i_sp=%s, secondary_iter()=%p, s_ident=%i", get_BoundaryName().c_str(), dir, jj, p->get_species()->get_name().c_str(), secondary_iter(), secondary_iter()->get_secSpecies()->get_ident()); //MAL200312
      num_emitted = secondary_iter()->createSecondaries(p, i, frac, dir);
      np2c_sec = secondary_iter()->get_secSpecies_np2c(); // debug TestHeFluid2FluidSecondary.inp seg fault, MAL200310
      emitted_particles[secondary_iter()->get_secSpecies()->get_ident()] += num_emitted * np2c_sec; // debug TestHeFluid2FluidSecondary.inp seg fault, MAL200310
    }

  absorbed_particles[p->get_species()->get_ident()] += 1 * p->get_w0();
}

// for interact_evolvingfluid_with_boundaries in DiffusionFluid; called by Conductor::evolvingfluid_BC
// number is the physicsl number of evolving fluid particles per second incident on the boundary, MAL200224
void Boundary::evolvingfluid_secondaryemission(Species * ispecies, Scalar number, VDistribution *v, Direction dir)
{
  // Find the flux to the wall in each timestep. Every particle that hits the
  // wall will be absorbed, but num_emitted secondaries are emitted, HH 04/28/16.
  int num_emitted = 0;
  double np2c_sec = 0;
  Scalar num_incident = 0.;
  Scalar np2c_incident = ispecies->get_np2c(); // default np2c of incident particles
//  printf("\nBoundary::evolvingfluid_secondaryemission: ispecies=%s, np2c_incident=%g, dir=%i, number=%g",ispecies->get_name().c_str(), np2c_incident,dir, number); // for debug, MAL200224
  if (np2c_incident)
  {
    num_incident = number * thefield->get_dt() / np2c_incident; // number of computer particles incident
//    printf(", num_incident=%g", num_incident);

//    int jj=0; // for debug, MAL200312
    for(secondary_iter.restart(); !secondary_iter.Done(); secondary_iter++)
    {
//      jj++; // for debug, MAL200312
 //     printf("\nBoundary::evolvingfluid_secondaryemission: name=%s, dir=%i, jj=%i, i_sp=%s, secondary_iter()=%p, s_ident=%i", get_BoundaryName().c_str(), dir, jj, ispecies->get_name().c_str(), secondary_iter(), secondary_iter()->get_secSpecies()->get_ident() ); // for debug, MAL200312
       num_emitted = secondary_iter()->evolvingfluid_createSecondaries(ispecies, num_incident, v, dir);
        np2c_sec = secondary_iter()->get_secSpecies_np2c();// debug TestHeFluid2FluidSecondary.inp seg fault, MAL200310
        emitted_particles[secondary_iter()->get_secSpecies()->get_ident()] += num_emitted * np2c_sec;// debug TestHeFluid2FluidSecondary.inp seg fault, MAL200310
//      printf("\nBoundary::evolvingfluid_secondaryemission: especies=%s, np2c_sec=%g, num_emitted=%i, emittedparticles=%g",secondary_iter()->get_secSpecies()->get_name().c_str(),np2c_sec, num_emitted, emitted_particles[secondary_iter()->get_secSpecies()->get_ident()]); // for debug
    }

    absorbed_particles[ispecies->get_ident()] += num_incident;
//    printf(", absorbed_particles=%g", absorbed_particles[ispecies->get_ident()]); // for debug, MAL200224
  }
}


// Calculate the total positive absorbed flux to the electrode in this rf cycle, HH 05/06/16.
double Boundary::absorbed_positive_flux()
{
  double pos_flux = 0.;

  for(species_iter.restart(); !species_iter.Done(); species_iter++)
  {
    if (species_iter()->get_q()>1e-20)
    {
      pos_flux += absorbed_particles[species_iter()->get_ident()];
    }
  }

  return pos_flux;
}

// Calculate the total negative absorbed flux to the electrode in this rf cycle, HH 05/06/16.
double Boundary::absorbed_negative_flux()
{
  double neg_flux = 0.;

  for(species_iter.restart(); !species_iter.Done(); species_iter++)
  {
    if (species_iter()->get_q()<-1e-20)
    {
      neg_flux += absorbed_particles[species_iter()->get_ident()];
    }
  }

  return neg_flux;
}

// Calculate the total positive emitted flux from the electrode in this rf cycle, HH 05/06/16.
double Boundary::emitted_positive_flux()
{
  double pos_flux = 0.;

  for(species_iter.restart(); !species_iter.Done(); species_iter++)
  {
    if (species_iter()->get_q()>1e-20)
    {
      pos_flux += emitted_particles[species_iter()->get_ident()];
    }
  }

  return pos_flux;
}

// Calculate the total negative emitted flux from the electrode in this rf cycle, HH 05/06/16.
double Boundary::emitted_negative_flux()
{
  double neg_flux = 0.;

  for(species_iter.restart(); !species_iter.Done(); species_iter++)
  {
    if (species_iter()->get_q()<-1e-20)
    {
      neg_flux += emitted_particles[species_iter()->get_ident()];
    }
  }

  return neg_flux;
}


// Algorithm by Yonemura and Nanbu (2003) to calculate DC bias in the circuit.
// The DC bias is the voltage on the capacitor which is in series with the plasma
// circuit, and should be approximately zero for a symmetric discharge. HH 05/06/16.
double Boundary::calculate_V_DC()
{
  double R;

  if (absorbed_negative_flux()+emitted_positive_flux() != 0)
  {
    R = (absorbed_positive_flux()+emitted_negative_flux())/(absorbed_negative_flux()+emitted_positive_flux());
  }
  else
  {
    return V_DC; // We don't do anything if the particles are too few to update V_DC in this cycle.
  }

  if (fabs(V_DC)>1)
  {
    V_DC = V_DC*pow(R,C*(V_DC/fabs(V_DC)));
  }
  else
  {
    if (fabs(R-1.0)>1e-20) // To prevent division by 0.
    {
      V_DC = V_DC + ((float) (R-1.0))/fabs(R-1.0)*Delta_V_DC;
    }
  }
  return V_DC;
}

// Function to reset matrix of flux of species to the walls,
// called after each rf period, HH 04/28/16.
void Boundary::reset_absorbed_particles()
{
    for(int i=0; i < nspecies; i++)
    {
      absorbed_particles[i] = 0.;
    }
}

// Function to reset matrix of flux of species from the walls,
// called after each rf period, HH 04/28/16.
void Boundary::reset_emitted_particles()
{
    for(int i=0; i < nspecies; i++)
    {
      emitted_particles[i] = 0.;
    }
}

void Boundary::radiation(ParticleGroup *p, int i)
{
	//  oopicListIter<Radiation> thei(radiationlist);
	for(radiation_iter.restart(); !radiation_iter.Done(); radiation_iter++)
	{
		radiation_iter()->radiate(p, i);
	}
}

// Added for phi diagnostic.
void Boundary::calculate_phi()
{
	if (is_usable(POSITIVE))
	{
		phi1 = thefield->get_phi(j1);
	}
	if (is_usable(NEGATIVE))
	{
		phi0 = thefield->get_phi(j0);
	}
}

// For time averages (power, Iconv) diagnostics, MAL 12/5/09
void Boundary::time_average(Scalar &g, Scalar &tempg, Scalar &aveg)
{
	int nsmod = thefield->get_nsteps() % thefield->get_naverage();
	Scalar avecoef = 1.0/(nsmod+1);
	if(nsmod != 0) // accum into tempg only
	{
		tempg += (g - tempg)*avecoef;
	}
	else if(nsmod == 0 && thefield->get_nsteps() > 1) // xfer tempg to displayed aveg, and begin again accum into tempg
	{
		aveg = tempg;
		tempg = g;
	}
}

// mindgame: 'dir' is the vel direction of particles ejected
void Boundary::eject(int n, Species *species, Scalar np2c, Direction dir)
{
	weights[dir][species->get_ident()] -= n*np2c;
}

// The following functions are for IED diagnostics, to print the diagnostics into text file.
// "i" is an index to loop over in order to get every IED as specified in input file, HH 02/22/16

// Get number of Boundaries to be diagnosed, as specified in input file, HH 02/22/16
Scalar Boundary::get_nBoundaries()
{
    int count = 0;
    for(dist_iter.restart(); !dist_iter.Done(); dist_iter++)
    {
        count++;
    }
    return count;
}

// Get number of energy bins for particular IED, HH 02/22/16
Scalar Boundary::get_nEnergyBins(int i)
{
    int n = 0;
    int count = 0;
    for(dist_iter.restart(); !dist_iter.Done(); dist_iter++)
    {
        if (count == i)
        {
            n = dist_iter() -> get_nEnergyBins();
        }
        count++;
    }
    return n;
}

// Get number of angle bins for particular IED, HH 05/02/16
Scalar Boundary::get_nAngleBins(int i)
{
    int n = 0;
    int count = 0;
    for(dist_iter.restart(); !dist_iter.Done(); dist_iter++)
    {
        if (count == i)
        {
            n = dist_iter() -> get_nAngleBins();
        }
        count++;
    }
    return n;
}

// Get name of IED and IAD (i.e. species name and location in space), HH 02/22/16
ostring Boundary::get_BoundaryPosition(int i)
{
    ostring speciesname;
    int count = 0;
    for(dist_iter.restart(); !dist_iter.Done(); dist_iter++)
    {
        if (count == i)
        {
            speciesname = dist_iter() -> get_BoundaryPosition();
        }
        count++;
    }
    return speciesname;
}

// Get number of particles in each energybin for species i, HH 02/22/16
DataArray *Boundary::get_energydist(int i)
{
    DataArray *energydist;
    int count = 0;
    for(dist_iter.restart(); !dist_iter.Done(); dist_iter++)
    {
        if (count == i)
        {
            energydist = dist_iter() -> get_energydist();
        }
        count++;
    }
    return energydist;
}

// Get number of particles in each anglebin for species i, HH 05/02/16
DataArray *Boundary::get_angledist(int i)
{
    DataArray *angledist;
    int count = 0;
    for(dist_iter.restart(); !dist_iter.Done(); dist_iter++)
    {
        if (count == i)
        {
            angledist = dist_iter() -> get_angledist();
        }
        count++;
    }
    return angledist;
}

// Get energy bins, HH 02/22/16
DataArray *Boundary::get_energybins(int i)
{
    DataArray *energybins;
    int count = 0;
    for(dist_iter.restart(); !dist_iter.Done(); dist_iter++)
    {
        if (count == i)
        {
            energybins = dist_iter() -> get_xarray_energy();
        }
        count++;
    }
    return energybins;
}

// Get angle bins, HH 05/02/16
DataArray *Boundary::get_anglebins(int i)
{
    DataArray *anglebins;
    int count = 0;
    for(dist_iter.restart(); !dist_iter.Done(); dist_iter++)
    {
        if (count == i)
        {
            anglebins = dist_iter() -> get_xarray_angle();
        }
        count++;
    }
    return anglebins;
}

// Get name of boundary (i.e. location in space), HH 04/22/16
ostring Boundary::get_BoundaryName()
{
    int j0=get_j0(); //  add to keep all diagnostic names distinct, MAL 9/23/10
    int j1=get_j1();
    if(j0 < 0 && j1 >=0) j0 = j1;
    if(j1 < 0 && j0 >=0) j1 = j0;
    ostring j0str = j0;
    ostring j1str = j1;
    ostring diagname = "(" + j0str + "," + j1str + ")"; // MAL 9/23/10
    return diagname;
}

void Boundary::initialize_diagnostics(DiagnosticControl *dc)
{
	// Current diagnostic.
	ostring diag_name = name + "_Iconv"; // need underscore to fix input file bug: Diagnostic name = "diagnostic_name" (no blanks!), MAL 12/6/09
	dc->add_timediagnostic(theSpeciesList, I_sp, nspecies, diag_name, false); // do not plot sum, MAL 12/12/09; add species names, JK 2019-01-19

	//Trying to add potential diagnostic.  JDN
	//Add && !(is_usable(POSITIVE) && j0==j1) to fix bug for adding phi when j0==j1, MAL, 12/3/09
	if (is_usable(NEGATIVE) && !(is_usable(POSITIVE) && j0==j1)) {
		ostring diag_name2 = name2 + "_phi";
		dc->add_timediagnostic(&phi0, 1, diag_name2, false);
	}
	if (is_usable(POSITIVE)) {
		ostring diag_name3 = name3 + "_phi";
		dc->add_timediagnostic(&phi1, 1, diag_name3, false);
	}
	// Add conductor charge, circuit current, and circuit power, MAL 12/4/09
	if (is_usable(NEGATIVE) && get_material() == CONDUCTOR && !(is_usable(POSITIVE) && j0==j1)) {
		ostring diag_name4 = name2 + "_Ickt";
		dc->add_timediagnostic(&I_circ, 1, diag_name4, false);
		ostring diag_name8 = name2 + "_Pwr";
		dc->add_timediagnostic(&P_circ, 1, diag_name8, false);
		if (thefield->get_naverage() > 1)
		{
			ostring diag_name10 = name2 + "_PwrAve";
			dc->add_timediagnostic(&aveP_circ, 1, diag_name10, false);
		}
	}
	if (is_usable(POSITIVE) && get_material() == CONDUCTOR) {
		ostring diag_name5 = name3 + "_Ickt";
		dc->add_timediagnostic(&I_circ, 1, diag_name5, false);
		ostring diag_name9 = name3 + "_Pwr";
		dc->add_timediagnostic(&P_circ, 1, diag_name9, false);
		if (thefield->get_naverage() > 1)
		{
			ostring diag_name11 = name3 + "_PwrAve";
			dc->add_timediagnostic(&aveP_circ, 1, diag_name11, false);
		}
	}
}


Scalar Boundary::advance_v_for_synchro_0th(ParticleGroup *p, int i,
					   Scalar frac, Direction dir)
{
  return 0;
}

Scalar Boundary::advance_v_for_synchro_1st(ParticleGroup *p, int i,
					   Scalar frac, Direction dir)
{
  p->advance_v(i,(0.5-frac)*dt,E[dir],B[dir]);
  return frac;
}

Scalar Boundary::advance_v_for_synchro_2nd(ParticleGroup *p, int i,
					     Scalar frac, Direction dir)
{
  Scalar frac_2nd, d;
  Vector3 E_v;

  if (thefield->is_magnetized())
  {
    Vector3 v_plus_half = p->get_vMKS(i);
    Vector3 v_plus_tmp, B_v;
    v_plus_tmp = v_plus_half
               + 0.5*(1-frac)*dt*p->get_qm0()*(E[dir]+v_plus_half%B[dir]);

    frac_2nd = frac*v_plus_half.e1()/v_plus_tmp.e1();

    d = 0.25*(2*frac_2nd-3.0);
    E_v = E[dir]-d*E_dt[dir]+(d+0.5)*v_plus_half.e1()*E_grad[dir]*dt;
    B_v = B[dir]-d*B_dt[dir]+(d+0.5)*v_plus_half.e1()*B_grad[dir]*dt;
    p->advance_v(i,(0.5-frac_2nd)*dt,E_v,B_v);
  }
  else
  {
    Scalar v_plus_tmp;
    Scalar v_plus_half = p->get_vMKS(i).e1();
    v_plus_tmp = v_plus_half
               + 0.5*(1-frac)*dt*p->get_qm0()*E[dir].e1();

    frac_2nd = frac*v_plus_half/v_plus_tmp;

    d = 0.25*(2*frac_2nd-3.0);
    E_v = E[dir]-d*E_dt[dir]+(d+0.5)*v_plus_half*E_grad[dir]*dt;
    p->advance_v(i,(0.5-frac_2nd)*dt,E_v);
  }

  return frac_2nd;
}

//#define LOCALCLASS Boundary
//#include "advance_f_sync.hpp"
