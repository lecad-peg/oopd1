//********************************************************
// radiation.cpp
//
// Revision history
//
//
//********************************************************

#include <stdio.h>
#include "main/radiation.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/pd1.hpp"
#include "main/oopiclist.hpp"
#include "utils/ostring.hpp"
#include "main/parse.hpp"
#include "fields/fields.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_time.hpp"
#include "diagnostics/diagnosticcontrol.hpp"
#include "main/ndarray.hpp"


#include<iostream>
#include<string>
#include<fstream>
using namespace std;

Radiation::Radiation (FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		      oopicList<Parameter<int> > int_variables, Fields *tf)
{
  specieslist = tf -> get_specieslist();
  init_parse(fp, float_variables, int_variables);
}

Radiation::~Radiation()
{
  if(radiation) { delete radiation; }
}
void Radiation::setdefaults(void)
{
  target_name ="";
  impact_species = "";
  iSpecies = NULL;
  abs_mat = NULL;

  density = 0.0;
  angle = 30.0*PI/180;
  azi_angle = 0.0;
  photon_energy_min = 0.0;
  photon_energy_max = 10.0;
  photon_energy_number = 200;
  solid_angle = 1;
  energy_criteria = 1e-5;
  position_max = 0;
  radiation = 0;
  photon_energy = 0;
  //when 90% of initial energy is lost, electron stops
  energy_fraction = 0.9;
}

void Radiation::setparamgroup(void)
{
  pg.add_fparameter("density", &density);
  pg.add_fparameter("angle", &angle);
  pg.add_fparameter("azi_angle", &azi_angle);
  pg.add_fparameter("atomic_weight", &atomic_weight);
  pg.add_fparameter("atomic_number", &atomic_number);
  pg.add_fparameter("energy_min",&photon_energy_min);
  pg.add_fparameter("energy_max",&photon_energy_max);
  pg.add_fparameter("solid_angle",&solid_angle);
  pg.add_fparameter("energy_criteria",&energy_criteria);
  pg.add_fparameter("energy_fraction",&energy_fraction);
  pg.add_iparameter("photon_energy_bins",&photon_energy_number);
  pg.add_iparameter("position_max",&position_max);
  pg.add_string("impact_species", &impact_species);
  pg.add_string("target_name",&target_name);
  pg.add_string("target_file",&target_file);
}

void Radiation::check(void)
{
  char err[]="RADIATION::check():";

  if(target_name.length() == 0) { error(err, "wall not specified"); }
  

  oopicListIter<Species> thei(*specieslist);
  for(thei.restart(); !thei.Done(); thei++)
    {
      if(thei()->get_name() == impact_species)
	{
	  iSpecies = thei();
	}
    }
  if(iSpecies == NULL) { error("impact_species not found"); }
}

// initialize diagnostics
void Radiation::init_diagnostics(void)
{
  DiagnosticControl dc;

  if(radiation)
    {
      tds.add(new Diagnostic2d(photon_energy, radiation));
    }

  // add diagnostics to the master list
  oopicListIter<Diagnostic> thed(tds);
  for(thed.restart(); !thed.Done(); thed++)
    {
      dc.add_diagnostic(thed());
    }
}

void Radiation::read_fromfile(void)
{
  //define the size of the matrix
  int Size(0);
  
  //define the height and the width of the matrix
  int Height(0),Width(0);
    
  string MyFile = "tungsten.txt";
  ifstream MyReader;

  MyReader.open(MyFile.c_str(),ios::in|ios::binary);

  if(!MyReader.is_open())
    {
      cerr<<"File Open Failed::for radiation"<<endl;
      exit(1);
    }


  // while(!MyReader.eof())
  //  {
      //take the size of the matrix
      //  MyReader>>Size;
      fprintf(stderr,"size = %d\n",Size);   
      //define the height and the width of the matrix
      Height=59;
      Width=3;
      
      //create one dimitional array of size "Size*Size" as
      //we treate with it as two dimitional array
     abs_mat =new Scalar[Height*Width];
      
      //reading each elemen of the matrix
      for(int R=0;R<Height;R++)
	for(int C=0;C<Width;C++)
	  //this formate to acess one dimentional array
	  //by index of two dimentiona array

	  MyReader>>abs_mat[R*Width+C];
     
      showMatrix(abs_mat,Size);
      //  }
}

void Radiation::showMatrix(Scalar x[],int Size)
{
  int Height=59;
  int Width=3;
  
  cout<<endl<<endl;
  
  for(int R=0;R<Height;R++)
    {
      cout<<"|";
      for(int C=0;C<Width;C++)
	cout<<x[R*Width+C]<<" ";
      cout<<"|"<<endl;
    }
  cout<<endl<<endl;
}

void Radiation::init()
{
  photon_energy_interval = (photon_energy_max-photon_energy_min)/photon_energy_number;
  numberofatom = NA * density / atomic_weight;
 
  ostring yaxis = "radiation of " + iSpecies -> get_name();
  radiation = new DataArray (yaxis,photon_energy_number+1);
  photon_energy = new DataArray ("photon energy",photon_energy_number+1);
  absorption = new Scalar [photon_energy_number+1];

  radiation -> zero();
  photon_energy -> setValue(photon_energy_min, 0);

  for (int i=1;i<photon_energy_number+1;i++){
    photon_energy -> setValue(photon_energy_min+(Scalar)i*photon_energy_interval,i);
  }
  for (int i=1;i<photon_energy_number+1;i++){
    absorption[i] = absorb(photon_energy->getValue(i));
  }
  //angle from degree to radian
  angle = angle * PI /180;

  init_diagnostics();
  read_fromfile();
}

void Radiation::radiate(ParticleGroup *p, int index)
{
  /*
    in this calculation unit of all energy is keV and the assumption is
    below 0.4keV is not generating x-ray.
    limit the number of calculation of trajectory
 
    The maximum number of calculation for trajectory is 400, we can change 
    by changing max_array
  */
  int max_array = position_max;
  Scalar position_x = 0.0;
  Scalar position_y = 0.0;
  Scalar position_z = 0.0;
  Scalar step_length = 0.0;
  
  Scalar energy;
  Scalar angle_theta;

  if (p->get_species()->get_ident() != iSpecies->get_ident()) return;

  energy = p -> get_energy_eV(index) * 0.001;
  if (iSpecies -> get_name() =="electrons")  energy = 15000.0;
  Scalar energy_initial = energy;
 
  if (energy_initial < energy_criteria)
    {
      fprintf(stderr,"\n RADIATION::radiation is not generating because E < 0.4keV\n");
    }

  Vector3 vel = p -> get_v(index);

  Scalar x_dir = vel.e1() / vel.magnitude();
  Scalar y_dir = vel.e2() / vel.magnitude();
  Scalar z_dir = vel.e3() / vel.magnitude();

  x_dir = 1.0;
  y_dir = 0.0;
  z_dir = 0.0;
  
  int count = 0;
  //think how to add count option to stop the radiation....

  while (energy > energy_initial*(1.0-energy_fraction)&&count< max_array)
    {
     
      //      fprintf(stderr,"energy=%g\n",energy);
      //Calculation of electron trajectory
      scatteringangle(energy);
      Scalar energy_tmp = energy;
      step_length = energyreduction(energy_tmp);
      angle_theta = get_theta(x_dir, y_dir);
      get_direction(x_dir, y_dir, z_dir);
      //step length in cm
     
      position_x = step_length * x_dir * 1e-8+ position_x;
      position_y = step_length * y_dir * 1e-8+ position_y;
      position_z = step_length * z_dir * 1e-8+ position_z; 
     
      if(position_x < 0.0) break;
      energy = energy_tmp;
     
      for (int k=0; k<photon_energy_number+1 ;k++)
	{
	  if (photon_energy_min > energy) break;
	  
	  radiation -> setValue(brem_rad(energy, photon_energy->getValue(k), angle_theta)*step_length*1e-8*exp(-absorb_fromfile(photon_energy->getValue(k))*density*position_x)/sin(angle)+radiation->getValue(k),k);
		
	  if (photon_energy->getValue(k) == 0) radiation->setValue(0.0,k);	
	}
      //      fprintf(stderr,"count=%d \n",count);
      count++;
    }

}

Scalar Radiation::energyreduction(Scalar &energy)
{

  //Sao J Appl Phys p5062
  //avogadro_number = 6.022E22;
  //Ion = 0.00976*atomic_number+0.0585*atomic_number^(-0.19);         %keV
  //stopping_power = 2*pi*elec_charge^4*number_target*atomic_number/energy_eV*log(1.166*energy_eV/Ion);  
  //stopping_power = 7.85*10^4*density*atomic_number/atomic_weight/energy*log(1.166*energy/Ion);        %unit is keV/cm
  //stopping_power = 5;
  
  //mean_free_path = mean_free(energy_eV*1E-3, atomic_number, number_target)*1E-10;       %unit is A
  //mean_free_path = mean_free(energy_eV*1E-3, atomic_number, number_target);       %unit is cm
  //step_length = -mean_free_path * log(rand);                      %unit is A
  //bethe = energy-stopping_power*step_length*1E-3;                 
  
  //energy in keV and stopping power in eV
  
  //calculation of bethe stopping power
  //J.. Phys. D. 26(1993) 507-516
  Scalar energy_eV = energy * 1E3;                   //energy in eV
  Scalar Ion = 9.76*atomic_number+58.5*pow(atomic_number,-0.19);         //eV
  //  Scalar elec_charg = iSpecies -> get_q();
  Scalar bet = iSpecies -> get_beta2(energy_eV);
  Scalar mv_square = bet * bet * SPEED_OF_LIGHT_SQ / PROTON_CHARGE * ELECTRON_MASS;
  //stopping_power = 2*pi*elec_charge^4*number_target*atomic_number/energy_eV*log(1.166*energy_eV/Ion);  
  //Scalar stopping_power = 785.0*density*atomic_number/atomic_weight/energy_eV*log(1.166*energy_eV/Ion);        //unit is eV/A
  //stopping_power = 5;
  Scalar stopping_power = 785*density*atomic_number/atomic_weight/mv_square*(log(mv_square*energy_eV/2/(Ion*Ion)/(1-bet*bet))-(2*sqrt(1-bet*bet)-(1-bet*bet))*log(2)+1-bet*bet+1/8*pow((1-sqrt(1-bet*bet)),2.0));

  //what is proper setting?
  if(stopping_power < 0) stopping_power = 0.0;
  //  fprintf(stderr,"beta =%g \n", bet);
  //  fprintf(stderr,"stopping =%g \t length=%g\n",(mv_square*energy_eV/2/(Ion*Ion)/(1-bet*bet)),(2*sqrt(1-bet*bet)-(1-bet*bet))*log(2));

  Scalar R = frand();
  Scalar mean_free_path = mean_free(energy)*1E10;       //unit is A
  Scalar step_length = - mean_free_path * log(R);         //unit is A
 
  energy = energy - stopping_power*step_length*1e-3;   
  // fprintf(stderr,"energy reduction =%g \n",stopping_power*step_length*1e-3);
    //energy in keV and stopping power in eV
  return step_length;
  //calculation based on J. Appl Phys Vol 76 No11 p7180 Ding
}

void Radiation::scatteringangle(Scalar energy)
{
  //calculation of scattering angle from mott cross section
  //right now we are using Rutherford cross section
  Scalar alpha, beta, R;
  // alpha = 3.4E-3*pow(atomic_number,2.0/3.0)/energy;         //energy in keV

  //  R = frand();       //uniformly distributed random number

  //  mott_angle = acos(1.0-2.0*alpha*R/(1.0+alpha-R));

  //azimuthal angle is uniformly distributed
  //  azi = TWOPI*frand();
  //  fprintf(stderr,"mott=%g \t azi=%g \n",mott_angle,azi);
  /////////////////////////////////////////////////////////////
  //modified rutherford
  //calculation of alpha
  //coefficient for Cu
  Scalar a = -2.11246;
  Scalar b = -1.1456;
  Scalar c = 0.22332;
  Scalar d = -0.620971;

  Scalar log_alpha = a+b*log10(energy)+c*log10(energy)*log10(energy)+d/exp(log10(energy));         //energy in keV

  alpha = pow(10,log_alpha);

  //calculation of beta for Cu
  Scalar a1 = 0.712004;
  Scalar b1 = 0.00196709;
  Scalar c1 = -0.0218025;
  Scalar d1 = -0.059572;
  
  Scalar beta_pre = a1 + b1*sqrt(energy)*log(energy)+c1*log(energy)/energy+d1/energy;

  if (beta_pre > 1)
    {
      beta = 1;
    }
  else
    {
      beta = beta_pre;
    }
 
  Scalar angle_beta =pow(180,beta)*PI/180;
  Scalar R_max = (cos(angle_beta)+alpha*cos(angle_beta)-1-alpha)/(cos(angle_beta)-1-2*alpha);
  
  R = frand();       //uniformly distributed random number
		     
  Scalar R_star = R*R_max;

  Scalar cos_angle = 1-2*alpha*R_star/(1+alpha-R_star);
  
  mott_angle = pow((acos(cos_angle)),(1/beta));
  azi = TWOPI*frand();
}

Scalar Radiation::brem_rad(Scalar energy, Scalar photon, Scalar angle_theta)
{

  Scalar a_x,a_y,x_component,y_component;
  Scalar large_A,large_B,small_a,small_b;
  Scalar y_1,y_2,y_3;
  Scalar f_func,g_func,h_func;
  Scalar brem;

 // Scalar mc_square = 511;            //unit = keV
  Scalar beta = sqrt(1.0-pow((1.0+energy/ELECTRON_MASS_EV/1E3),-2.0));
  
  Scalar accel_vol = energy / 0.2998;            //energy in keV
  Scalar U = accel_vol / (atomic_number * atomic_number);
  Scalar Q = photon / energy + 1e-4;                 //photon_energy in keV and energy in keV
  
  if (U < 0.0604)
    {
      a_x = (-1.019040+0.1460140*Q-0.0501077*Q*Q)*log(U)-1.483110+0.9474840*Q-0.3500140*Q*Q;
      a_y = (-0.882129+0.0366934*Q-0.3396460*Q*Q)*log(U)-1.040880-1.8635500*Q-0.7230680*Q*Q;
      
      x_component = exp(a_x);
      y_component = exp(a_y);
    
      if(Q < 0.2)
	{
	  x_component = x_component * (1.0-0.567*exp(-20.0*Q));
	  y_component = y_component * (1.0-(-0.55+4.83*U)*exp(-20.0*Q));
	}
    }
  
  
  else
    {
      large_A = exp(-0.2230*U)-exp(-57.0*U);
      large_B = exp(-0.0828*U)-exp(-84.9*U);
      
      small_a = 1.47*large_B-0.507*large_A-0.833;
      small_b = 1.70*large_B-1.090*large_A-0.627;
      
      x_component = (0.252+small_a*(Q-0.135)-small_b*(Q-0.135)*(Q-0.135))/(U*photon);
      
      y_1 = 0.22*(1-0.39*exp(-26.9));
      y_2 = 0.067+0.023/(U+0.75);
      y_3 = -0.00259+0.00776/(U+0.116);
      
      f_func = (-0.214*y_1+1.21*y_2-y_3)/(1.43*y_1-2.43*y_2+y_3);
      g_func = (1.0+2.0*f_func)*y_2-2.0*(1.0+f_func)*y_3;
      h_func = (1.0+f_func)*(y_3+g_func);
      
      y_component = (-g_func + h_func/(Q+f_func))/(U*photon);
    
      if(Q < 0.2) x_component = x_component * (1.0-0.567*exp(-20.0*Q));
    }
  if (Q > 1.0){
    brem = 0.0;
  }
  else{
    brem = (x_component*(1.0-cos(angle_theta)*cos(angle_theta))+y_component*(1.0+cos(angle_theta)*cos(angle_theta)))/(1.0-beta*cos(angle_theta)*cos(angle_theta));
  }
  return brem;
//unit is cm^2/keV
}

Scalar Radiation::mean_free(Scalar energy)
{
  //unit of energy is "keV" 
  //calculation of mean free path using Rutherforn scattering 
  //in the equation of rutherford cross section, i ignore the velocity beta,
  //because velocity is much smaller than speed of light
     
  //  Scalar elec_charge = iSpecies -> get_q();
  //Scalar mc_square = ELEACTRON_MASS*SPEED_OF_LIGHT_SQ;
  //  Scalar electron_radius = 2.8129E-15;               //unit = meter
						 
  Scalar alpha = 3.4E-3*pow(atomic_number,2.0/3.0)/energy;              //energy in keV
  Scalar lamda = 3.87E-2/sqrt(energy)/sqrt(1.0+9.79E-4*energy);
  Scalar bohr = 0.529E-1;                           //unit = nm
						
  //for this calculation energy unit is joule
  //energy_joule = energy * 1.6E-16;
  //Scalar gamma = energy *1E3 / ELECTRON_MASS_EV + 1;
  // Scalar beta = sqrt(1-1/pow(gamma,2));
  
  //integration of rutherford cross section from 0 to pi
   
  //Scanning v16 67-77
  
  Scalar integration = 0.0;
  Scalar x_value = 0.0;
  Scalar x_value_before = 0.0;
  Scalar y_value =0.0;
  Scalar y_value_before = 0.0;
  int interval_int = 1000;
  Scalar del_x = PI / interval_int;
  x_value = del_x;

  for (int i=0;i<interval_int;i++)
    {
     y_value = 1.0/pow((pow(sin(x_value/2.0),2.0)+alpha),2.0)*sin(x_value);
     y_value_before = 1.0/pow((pow(sin(x_value_before/2.0),2.0)+alpha),2.0)*sin(x_value_before);
     integration += (y_value+y_value_before)*del_x/2.0;
     x_value = x_value + del_x;
     x_value_before = x_value_before+del_x;
    }
  
  Scalar total_cross = 2.0*PI*(atomic_number*atomic_number*pow(lamda,4.0)/64.0/PI/bohr)*integration;
  //Asuman Aydin
  //total_cross = int(2*3.141592*(atomic_number^2*electron_radius^2*(1-beta^2)/beta^4/(1-cos(theta)+1E-10)^2)*sin(theta),theta,0,3.141592);
  
  
  //Ding Ze-jun J. Phys. D 26 (1993) 507-516
  //total_cross = int(2*3.141592*(atomic_number^2*elec_charge^4/4/(energy/1000)^2/(1-cos(theta)+2*beta)^2)*sin(theta),theta,0,3.141592);
  
  //This is calculation of total cross section
  //mean_free = total_cross;
  
  //This is mean free path
  //Scalar mean_free = 1/numberofatom/total_cross*1E14;  
  Scalar mean_free = 1.0/numberofatom/total_cross*1E12;    //unit is cm^3 and total cross is nm^2<-check it tomorrow
  //return [m]
  return mean_free;
}

Scalar Radiation::get_theta(Scalar x_dir, Scalar y_dir)
{
  Scalar the_elec = mott_angle;
  Scalar phi_elec = azi;
  Scalar angle_theta = acos(cos(angle)*sin(the_elec)*cos(phi_elec-azi_angle)-sin(angle)*cos(the_elec));
  /* 
    FILE *fp;
    ostring ffname="angle.dat";
    fp =fopen(ffname(),"a");
    fprintf(fp,"x_dir=%g \t the_elec=%g \t phi_elec=%g \n",x_dir,the_elec,phi_elec);
    fprintf(fp,"angle=%g \t azi_angle=%g \t angle_theta=%g\n",angle,azi_angle,angle_theta);
    fclose(fp); 
  */
  return angle_theta;
}

void Radiation::get_direction(Scalar& x_dir, Scalar& y_dir, Scalar& z_dir)
{
  Scalar sinx = sin(acos(x_dir));
  Scalar sinz = sin(acos(z_dir));
  Scalar cosphi0 = x_dir;
  Scalar cosT0 = z_dir;
  
  Scalar cosphi = cos(azi);
  Scalar sinphi = sin(azi);
  Scalar cosT = cos(mott_angle);
  Scalar sinT = sin(mott_angle);

  x_dir = cosphi*sinT*cosphi0*cosT0 - sinphi*sinT*sinx + cosT*cosphi0*sinz;
  y_dir = cosphi*sinT*sinx*cosT0 + sinphi*sinT*cosphi0 + cosT*sinx*sinz;
  z_dir = -cosphi*sinT*sinz + cosT*cosT0;
}

Scalar Radiation::absorb(Scalar abs_energy)
{
  //the unit of eneryg here is eV, so convert energy from keV to eV
  abs_energy = abs_energy * 1e3;
  Scalar absorp;
 
  if (abs_energy <= 10.2) absorp = 8.71e4;
  else if(abs_energy > 10.2 && abs_energy <= 21.2){
    absorp = 8.71e4 + (9.95e4-8.71e4)/(21.2-10.2) *(abs_energy - 10.2);
  }
  else if(abs_energy > 21.2 && abs_energy <= 30.5){
    absorp = 9.95e4 + (1.07e5-9.95e4)/(30.5-21.2) *(abs_energy - 21.2);
  }
  else if (abs_energy > 30.5 && abs_energy <= 200.0){
    absorp = -7e-6*pow(abs_energy,5)+0.0046*pow(abs_energy,4)-1.1466*pow(abs_energy,3)+137.96*pow(abs_energy,2)-8109.7*abs_energy+254368;
  }
  else if (abs_energy > 200.0 && abs_energy <= 851.5){
    absorp = 1e9*pow(abs_energy,-1.9737);
  }
  else if( abs_energy > 851.5 && abs_energy <= 1011.5){
    absorp = exp(9.19835*(log(abs_energy)-6.91939)+9.2496);
  }
  else if(abs_energy>1011.5 && abs_energy <= 8047.8){
    absorp = 8e11*pow(abs_energy,-2.5986);
  }
  else if(abs_energy>8047.8 && abs_energy <= 9886.4){
    absorp = exp(7.26642*(log(abs_energy)-8.99315)+3.91202);
  }
  else {
    absorp = 3e13*pow(abs_energy,-2.7672);
  }
  //  absorp = 0.0;
  return absorp;  
}

Scalar Radiation::absorb_fromfile(Scalar abs_energy)
{
  //unit of abs_energy is keV and unit for abs is MeV
  Scalar absorption;
  abs_energy = abs_energy / 1000;

  for (int i=1 ; i<59; i++){
    if (abs_energy < abs_mat[0]) absorption = abs_mat[2];
    if (abs_energy < abs_mat[3*i]){
      absorption = abs_mat[3*i-1] + (abs_mat[3*i+2]-abs_mat[3*i-1])/(abs_mat[3*i]-abs_mat[3*(i-1)])*(abs_energy-abs_mat[3*i-3]);
      break;
    }
    //    fprintf(stderr,"abs_coeff=%g \t abs_energy=%g \n",absorption,abs_energy);
  }
  return absorption;
}
