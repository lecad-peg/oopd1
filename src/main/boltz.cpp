#include<stdio.h>

#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/grid.hpp"
#include "fields/fields.hpp"
#include "fields/poissonfield.hpp"
#include "distributions/dist_xv.hpp"
#include "distributions/dist_v.hpp"
#include "atomic_properties/fluid.hpp"
#include "main/boundary.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_time.hpp"
#include "diagnostics/diagnosticcontrol.hpp"
#include "main/boltz.hpp"

#include "main/grid_inline.hpp"

inline Scalar BoltzmannModel::get_rho0(void)
{
  return n0*thespecies->get_q();
}

BoltzmannModel::BoltzmannModel(Species * _thespecies, bool _parse) :
  Fluid(_thespecies),
  Parse(_thespecies)
{
  N_tot = -1.;
  nonlinear = true;
  if(_parse) { init_parse(); }
}

BoltzmannModel::BoltzmannModel(PoissonSolver * theps,
			       Species * _thespecies) : Fluid(theps,
							      _thespecies)
{
  N_tot = -1.;
  nonlinear = true;
}

BoltzmannModel::BoltzmannModel(FILE *fp,
			       oopicList<Parameter<Scalar> > float_variables,
			       oopicList<Parameter<int> > int_variables,
			       Species * _thespecies)
  : Fluid(_thespecies)
{
  init_parse(fp, float_variables, int_variables);
}

BoltzmannModel::BoltzmannModel(FILE *fp,
			       oopicList<Parameter<Scalar> > float_variables,
			       oopicList<Parameter<int> > int_variables,
			       PoissonSolver * theps, Species * _thespecies)
  : Fluid(theps, _thespecies)
{
  init_parse(fp, float_variables, int_variables);
}

// virtual functions from Parse
void BoltzmannModel::setdefaults(void)
{
  flux_in = 0.;
  nonlinear = true;
  classname = "BoltzmannModel";
  n0 = -1.;
  T = -1.;
  N_tot = -1.;
  quasineutral = false;
  n0const = false;
}

void BoltzmannModel::setparamgroup(void)
{
  pg.add_fparameter("n0", &n0, "[m^{-3}] number density where potential = 0");
  pg.add_fparameter("Ntot", &N_tot, "total number of particles");
  pg.add_fparameter("T", &T, "[V] initial temperature");
  pg.add_fparameter("influx", &flux_in,
		    "[s^{-1}] flux into the volume");
  pg.add_bflag("quasineutral", &quasineutral, true,
	       "Boltzmann density determined by quasineutrality");
}

void BoltzmannModel::check(void)
{
  fprintf(stderr, "N_tot = %g\n", N_tot);

  if(T <= 0.) { error("T should be greater than zero"); }

  bool flag = true;

  if(n0 >= 0.)
    {
      n0const = true;
      flag = false;
   }

 if(quasineutral) { flag = false; }
 if(N_tot >= 0) { flag = false; }

 if(flag)
   {
     error("Boltzmann density not specified");
   }

 if(n0const && quasineutral)
   {
     error("n0 cannot be used with quasineutral");
   }
 if(N_tot >= 0.)
   {
     if(n0const || quasineutral)
       {
	 error("Ntot cannot be used with n0 or quasineutral");
       }
   }
}

void BoltzmannModel::init(void)
{
  // set up variables to reduce number of operations
  qeT = thespecies->get_q()/(PROTON_CHARGE*T);
  deriv_coeff = -qeT*thespecies->get_q()*n0;
}

void BoltzmannModel::set_thefield(Fields * _thefield)
{
  if(_thefield->is_PoissonField())
    {
      Fluid::set_thefield(_thefield);
      DiagnosticControl dc;
      ostring diagname = "Boltzmann_";
      diagname += thespecies->get_name();
      diagname += "_";
      dc.add_gridarray(diagname + "n", n_fluid);
      dc.add_timediagnostic(&N_tot, diagname + "Ntot");
    }
  else
    {
      error("BoltzmannModel: must use PoissonSolver");
    }
}

VDistribution * BoltzmannModel::velocity_distribution(int node)
{
  VDistribution *thegaussian;

  thegaussian = new VDistribution(thespecies, thespecies->vthermal(T),
				  Vector3(0.,0.,0),
				  false);

  return thegaussian;
}

Scalar BoltzmannModel::normalized_density(int i)
{
  return exp(-qeT*phi_tmp[i]);
}

// electrostatic_density() :  return charge density [C/m^3]
// as a function of potential
Scalar BoltzmannModel::electrostatic_density(Scalar potential)
{
  return get_rho0()*exp(-qeT*potential);
}

Scalar BoltzmannModel::electrostatic_derivative(Scalar potential)
{
  return deriv_coeff*exp(-qeT*potential);
}

void BoltzmannModel::update(Scalar dt)
{
  Fluid::update(dt);
  calculate_density();

  if(!n0const && (N_tot >= 0))
    {
      Scalar flux_out = dt*(interact_with_boundaries() - flux_in);

      if(debug())
	{
	  fprintf(stderr, "flux_out = %g, flux_in = %g, dt=%g\n",
		  flux_out, flux_in, dt);
	}

      N_tot -= flux_out;
      if(N_tot < 0) { N_tot = 0.; }
    }
}

Scalar BoltzmannModel::get_n0(Scalar _N_tot, Scalar * _phi)
{
  phi_tmp = _phi;

  // integrate the total amount of charge in the volume
  Scalar tmp;
  tmp =  calculate_Ntot();

  if(debug())
    {
      fprintf(stderr, "get_n0: _N_tot=%g, denom=%g\n", _N_tot, tmp);
    }

  if(tmp == 0)
    {
      phi_tmp = 0;
      return 0.;
    }
  else
    {
      phi_tmp = 0;
      return _N_tot/tmp;
    }
}

void BoltzmannModel::update_N_tot(void)
{
  if(debug())
    {
      fprintf(stderr, "update_N_tot: %g\n", N_tot);
    }

  if(n0const)
    {
      if(!n0)
	{
	  N_tot = 0.;
	}
      else
	{
	  N_tot =  calculate_Ntot(n0);
	}
      return;
    }

  if(N_tot < 0) // initialize N_tot
    {
      if(quasineutral)
	{
	  // compute total charge in system
	  N_tot = thefield->get_theGrid()->integrate(thefield->get_rho());
	  N_tot = -N_tot/thespecies->get_q();
	  if(N_tot < 0.) { N_tot = 0.; }

	  set_n0(get_n0(N_tot, thefield->get_phi()));

	  if(debug())
	    {
	      fprintf(stderr, "\nBoltzmann, quasineutral-- N_tot = %g",
		      N_tot);
	      fprintf(stderr, "; n0 = %g \n", n0);
	    }
	}
    }
  else
    {
      set_n0(get_n0(N_tot, thefield->get_phi()));
    }
}

// updating routine called by PoissonSolver
void BoltzmannModel::update_fields(Scalar *phi)
{
  phi_tmp = phi;

  if(debug())
    {
      fprintf(stderr, "\nBoltzmann, update_fields-- N_tot = %g",
	      N_tot);
      fprintf(stderr, "; n0 = %g \n", n0);
    }


  if(!n0const)
    {
      Scalar n0tmp;
      n0tmp = get_n0(N_tot, phi);

      set_n0(n0tmp);
    }


  phi_tmp = 0;
}

Fluid * BoltzmannModel::get_copy(void)
{
  BoltzmannModel *tbm;

  tbm = new BoltzmannModel(thespecies, false);
  tbm->setdefaults();
  tbm->set_n0(n0);
  tbm->set_T(T);
  tbm->set_debug(pg.debug);
  tbm->quasineutral = quasineutral;
  tbm->flux_in = flux_in;
  tbm->n0const = n0const;

  if(N_tot >= 0) { tbm->N_tot = N_tot; }

  tbm->check();
  tbm->init();

  if(thefield)    {      tbm->set_thefield(thefield);    }

  return tbm;
}

void BoltzmannModel::calculate_density(void)
{
  int ng = thefield->get_theGrid()->getng();
  for(int i=0; i < ng; i++)
    {
      n_fluid[i] = density_grid(i);
    }
}

Scalar BoltzmannModel::calculate_Ntot(Scalar _n0)
{
  //  Scalar retval = thefield->get_theGrid()->integrate(thefield->get_rho());
  Scalar retval =
    thefield->get_theGrid()->integrate(&BoltzmannModel::normalized_density,
				       this);
  return retval*_n0;
}

Vector3 BoltzmannModel::get_v_from_distribution(int node)
{
  VDistribution thegaussian(thespecies, thespecies->vthermal(T),
			    Vector3(0.,0.,0),
			    false);
  return thegaussian.get_V();
}
