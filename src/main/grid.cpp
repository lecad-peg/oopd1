#include <stdio.h>

#ifdef MPIDEFINED
#include <mpi.h>
#endif

#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/boundary.hpp"
#include "main/dielectric.hpp"
#include "main/grid.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"

#include "main/grid_inline.hpp"

Grid::Grid()
{ 
	setdefaults();
	setparamgroup();
}

Grid::Grid(Parse * _parent) : Parse(_parent)
{
	init_parse();
}


Grid::Grid(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables)
{
	init_parse(fp, float_variables, int_variables);
}

Grid::Grid(int nngg, Scalar *xx, int geomtype)
{
	setdefaults();
	setparamgroup();
	creategridfromarray(nngg, xx, geomtype);

}

Grid::~Grid()
{
	if(Xsetflag)
	{
		delete [] X;
		delete [] V;
		delete [] A;
		delete [] Xcell;
		delete [] cell_volume;
	}
	if(!uniform)
	{
		delete [] dxcell;
		delete [] dxnode;
	}

	if(boundary_setflag)
	{
		for(int i=0; i < ng; i++)
		{
			delete boundary_list[i];
		}
		delete [] boundary_list;
	}
}

void Grid::setdefaults(void)
{
	classname = "Grid";
	cell_volume = 0;
	ng = nc = 0;
	L = x0 = x1 = dx = 0.;
	uniform = true;
	periodic = false;
	type = PLANAR;
	Xsetflag = 0;
	height = area = 1.; // default values
	dxcell = dxnode = 0;
	boundary_setflag = false;
}


void Grid::setparamgroup(void)
{
	pg.add_flag("Planar", &type, PLANAR, "planar geometry");
	pg.add_flag("Cylindrical", &type, CYLINDRICAL, "cylindrical geometry");
	pg.add_flag("Spherical", &type, SPHERICAL, "spherical geometry");

	pg.add_iparameter("ng", &ng, "number of grid points");
	pg.add_iparameter("nc", &nc, "number of cells in the grid");

	pg.add_fparameter("L", &L, "[m] system length");
	pg.add_fparameter("x0", &x0, "[m] position of first boundary");
	pg.add_fparameter("x1", &x1, "[m] position of last boundary");
	pg.add_fparameter("r0", &x0);
	pg.add_fparameter("r1", &x1);
	pg.add_fparameter("dx", &dx, "[m] cell size");
	pg.add_fparameter("area", &area, "[m^2] cross-sectional area (for planar geometry)");
	pg.add_fparameter("height", &height, "[m] system height (for cylindrical geometry)");
}

void Grid::check(void)
{
	if(periodic && (type != PLANAR))
	{
		error("Curvilinear periodic grid not allowed!");
	}

	if((x1 < x0) && (L <= 0.))
	{
		error("x1 < x0");
	}

	if((!x1) && (!L))
	{
		error("Length of the system is undefined!");
	}
	if(((x0 < 0) || (x1 < 0)) &&
			((type == CYLINDRICAL) || (type == SPHERICAL)))
	{
		error("Negative values of x0 or x1 not allowed for curvilinear coordinates");
	}
	switch(type)
	{
	case PLANAR:
		if(area <= 0.)
		{
			error("Electrode area must be positive");
		}
		break;
	case CYLINDRICAL:
		if(height <= 0.)
		{
			error("System height must be positive");
		}
		if(area != 1.)
		{
			fprintf(stderr, "\narea ignored for cylindrical coordinates.\n");
		}
		break;
	case SPHERICAL:
		if((area != 1.) || (height != 1.))
		{
			fprintf(stderr,
					"\narea and height ignored for spherical coordinates.\n");
		}
	}
}

void Grid::init(void)
{
	set_msg(classname);
	// set up system length
	if(x1 && L)
	{
		fprintf(stderr, "\nLength of system overspecified.");
		fprintf(stderr, "\nUsing x1 = %f\n",x1);
	}
	if(x1)
	{
		L = x1 - x0;
	}
	else
	{
		x1 = x0 + L;
	}
	if(x1 == x0) { error("Grid::init:", "System length zero"); }

	// set up grid spacing
	if(ng)
	{
		if(nc)
		{
			fprintf(stderr, "\nNumber of nodes overspecified");
			fprintf(stderr, "\nUsing nc=%d cells.\n", nc);
		}
		else
		{
			nc = ng-1;
		}
	}

	if(dx)
	{
		if(nc)
		{
			fprintf(stderr, "\nNumber of nodes overspecified");
			fprintf(stderr, "\nUsing nc=%d cells.\n", nc);
		}
		else
		{
			nc = int(L / dx);
		}
	}
	if(nc < 1)
	{
		error("Number of cells undefined!");
	}

	dx = L / Scalar(nc);
	ng = nc+1;
	if(ng < 2) { error("Too few grid points!"); }
	fprintf(stderr, "\ndx = %e", dx);
	setX();
}

Scalar Grid::getGridVolume() const
{
	Scalar tmp;
	int i;

	for(i=0, tmp = 0.; i < ng; i++)
	{
		tmp += V[i];
	}
	return tmp;
}

void Grid::setXcell(void)
{
	int i;
	Scalar tmp;

	Xcell = new Scalar[ng+1];
	Xcell[0] = X[0];
	Xcell[ng] = X[ng-1];

	switch(type)
	{
	case PLANAR:
		for(i=0; i < nc; i++)
		{
			Xcell[i+1] = 0.5*(X[i] + X[i+1]);
		}
		break;
	case CYLINDRICAL:
	case SPHERICAL:
	default:
		for(i=0, tmp=0.; i < nc; i++)
		{
			tmp += V[i];
			Xcell[i+1] = getRfromV(tmp);
		}
	}
}

void Grid::setV(void)
{
	int i;

	if(type == PLANAR) { setXcell(); }

	for(i=0; i < ng; i++)
	{
		switch(type)
		{
		case PLANAR:
			V[i] = area*(Xcell[i+1] - Xcell[i]);
			break;

			// for curvilinear coordinates, create volumes for
			// first order weighting using the method  given
			// in Verboncoeur, JCP 174, pp421-427, 2001.
		case CYLINDRICAL:
			V[i] = height*(PI/3.);
			if(i==0)
			{
				V[i] *= (X[1]-X[0])*(2.*X[0] + X[1]);
			}
			else if(i==ng-1)
			{
				V[i] *= (X[i] - X[i-1])*(X[i-1] + 2.*X[i]);
			}
			else
			{
				V[i] *= (X[i+1]*(X[i] + X[i+1]) - X[i-1]*(X[i-1] + X[i]));
			}
			break;
		case SPHERICAL:
			V[i] = (PI/3.);
			if(i==0)
			{
				V[i] *= (X[1]-X[0])*(3.*X[0]*X[0] + X[1]*X[0] + X[1]*X[1]);
			}
			else if(i==ng-1)
			{
				V[i] *= (X[i] - X[i-1])*(X[i-1]*X[i-1] +
						+ 2.*X[i-1]*X[i] + 3.*X[i]*X[i]);
			}
			else
			{
				V[i] *= (X[i+1] - X[i-1])*(X[i-1]*X[i-1] + X[i-1]*X[i] +
						X[i-1]*X[i+1] + X[i]*X[i] +
						X[i]*X[i+1] + X[i+1]*X[i+1]);
			}
			break;
		}
	}

	if(type != PLANAR) { setXcell(); }

	// calculate cell_volume array (based on cell midpoints)
	Scalar xleft, xright;
	cell_volume = new Scalar[ng];
	for(i=0; i < ng; i++)
	{

		if(i == 0)
		{
			xleft = X[i];
		}
		else
		{
			xleft = 0.5*(X[i] + X[i-1]);
		}
		if(i == (ng-1))
		{
			xright = X[i];
		}
		else
		{
			xright = 0.5*(X[i] + X[i+1]);
		}

		cell_volume[i] = getV(xleft, xright);
	}


}

void Grid::creategrid(void)
{
	int i;

	V = new Scalar[ng];
	A = new Scalar[ng+1];

	switch(type)
	{
	case PLANAR:
		area_coeff = vol_coeff = area;
		break;
	case CYLINDRICAL:
		area_coeff = 2.*PI*height;
		vol_coeff = PI*height;
		break;
	case SPHERICAL:
		area_coeff = 4.*PI;
		vol_coeff = (4.0/3.0)*PI;
		break;
	}

	setV();

	A[0] = area_coeff*ipow(X[0], type);
	for(i=0; i < ng; i++)
	{
		A[i+1] = area_coeff*ipow(Xcell[i+1], type);
	}

	Xsetflag = 1;
	if(!uniform) { setdx(); }
	dxdx = dx*dx;
	inv_dxdx = 1./dxdx;
}

void Grid::setX(void)
{
	int i;

	X = new Scalar[ng];

	if(uniform)
	{
		for(i=0; i < ng; i++)
		{
			X[i] = x0 + i*dx;
		}
	}

	creategrid();
}

void Grid::setdx(void)
{
	dx = X[1] - X[0];
	ratio = new Scalar[nc]; // DOESN"T STORE ANYTHING, CURRENTLY
	dxcell = new Scalar[nc];
	dxnode = new Scalar[ng];
	dxnode[0] = dx;
	dxnode[ng-1] = X[ng-1] - X[ng-2];

	for(int i=0; i < nc; i++)
	{
		dxcell[i] = X[i+1] - X[i];
		if(i > 0) { dxnode[i] = 0.5*(dxcell[i] + dxcell[i-1]); }
		ratio[i] = dxcell[i]/dxcell[0];
	}

}

void Grid::setdtdx(Scalar dt)
{
	dtdx = dt/dx;
}

// converts array gridX into physical units
void Grid::gridXtoX(int n, Scalar *gridX)
{
	register int i;
	if(uniform)
	{
		for(i=0; i < n; i++)
		{
			gridX[i] = gridXtoX_uniform(gridX[i]);
		}
	}
	else
	{
		error("gridXtoX(int n, Scalar *gridX)",
				"Not yet implement for nonuniform meshes");

	}
}

void Grid::XtogridX(int n, Scalar *x)
{
	register int i;
	if(uniform)
	{
		for(i=0; i < n; i++)
		{
			x[i] = XtogridX_uniform(x[i]);
		}
	}
	else
	{
		error("XtogridX(int n, Scalar *gridX)",
				"Not yet implement for nonuniform meshes");

	}
}

void Grid::dXtogriddX(int n, Scalar *_dx) // added function to fix bug in emitter.cpp, MAL 9/17/09
{
	register int i;
	if(uniform)
	{
		for(i=0; i < n; i++)
		{
			_dx[i] /= dx;
		}
	}
	else
	{
		error("dXtogriddX(int n, Scalar *griddX)",
				"Not yet implemented for nonuniform meshes");

	}
}

Scalar Grid::integrate(Scalar *f, int start, int end, int power, bool vmult)
{
	Scalar val = 0.;

	if(end < 0) { end = ng; }

	if(vmult)
	{
		for(int i=start; i < end; i++)
		{
			val += ipow(f[i]*V[i], power);
		}
	}
	else
	{
		for(int i=start; i < end; i++)
		{
			val += V[i]*ipow(f[i], power);
		}
	}

	return val;
}

// mindgame:
//  is_usable() will be used to decide whether boundaries are relevant.
//     Especially for the case that materials are overlapped to each other.
//  TODO: Decide what to do in case where materials (including inside)
//       are overlapped.
//    Rule 1: When the boundary without thickness is overlapped with 
//           the one with finite thickness, the former one has the precedence.
//
void Grid::setboundaries(BoundaryList & blist)
{
	int i;
	boundary_setflag = true;

	boundary_list = new BoundaryList * [ng];
	for(i=0; i < ng; i++)
	{
		boundary_list[i] = new BoundaryList;
	}

	oopicListIter<Boundary> thei(blist);
	oopicListIter<Boundary> thej;

	int j0, j1, j;
	bool ident;
	Direction dir1, dir2;
	bool absorbed_done;

	for(thei.restart(); !thei.Done(); thei++)
	{
		j0 = thei()->get_j0();
		j1 = thei()->get_j1();
		if (j0 == j1) ident = true;
		else ident = false;

		for (j=j0, dir1=NEGATIVE, dir2=POSITIVE;
				dir1==NEGATIVE || (!ident && j==j1 && dir1==POSITIVE);
				j+=(j1-j0), dir1=POSITIVE, dir2=NEGATIVE)
		{
			if (boundary_list[j]->isEmpty()
					|| thei()->get_property() == TRANSMITTED)
			{
				boundary_list[j]->add(thei());
				thei()->set_enable(dir1);
				if(ident)
				{
					thei()->set_enable(dir2);
				}

			}
			else
			{
				absorbed_done = false;
				for(thej.restart(*boundary_list[j]); !thej.Done(); thej++)
				{
					if (thej()->get_property() == ABSORBED)
					{
						// previous one without thickness has more priority
						if (thej()->get_j0() == thej()->get_j1())
						{
							if (!ident)
							{
								thej()->set_disable(dir2);
							}
						}
						// previous one has thickness
						else
						{
							if (ident)
								// new one has more priority.
							{
								thej()->set_disable(dir1);
								boundary_list[j]->add(thei());
								thei()->set_enable(dir2);
							}
						}
						thei()->set_disable(dir1);
						absorbed_done = true;

						break;
					}
				}
				if (!absorbed_done)
				{
					// all previous ones have the property of TRANSMITTED
					boundary_list[j]->add(thei());
					thei()->set_enable(dir1);
					if(ident)
					{
						thei()->set_enable(dir2);
					}

				}
			}
		}
	}

	if(boundary_list[0]->isEmpty())
	{
		error("boundary_list[0] not initialized");
	}
	if(boundary_list[ng-1]->isEmpty())
	{
		error("boundary_list[ng-1] not initialized");
	}

	for(thej.restart(*boundary_list[0]); !thej.Done(); thej++)
	{
		thej()->set_disable(NEGATIVE);
	}

	for(thej.restart(*boundary_list[ng-1]); !thej.Done(); thej++)
	{
		thej()->set_disable(POSITIVE);
	}
}

// initialize a grid from an array of x values
void Grid::creategridfromarray(int nngg, Scalar *xx, int geomtype)
{
	int i;
	Scalar dxtest, tolerence;
	ostring message;

	tolerence = 0.001;  // assumed uniformity tolerence constant

	// set basic grid constants
	ng = nngg;
	X = new Scalar[ng];
	memcpy(X, xx, ng*sizeof(Scalar));

	nc = ng-1;
	L = xx[nc] - xx[0];

	if(L < 0) { error("L < 0"); }

	// check for uniformity
	uniform = true;
	dxtest = xx[1] - xx[0];
	tolerence *= dxtest;

	dx = dxtest;

	for(i=1; i < nc; i++)
	{
		if(fabs(xx[i+1] - xx[i]) > tolerence)
		{
			uniform = false;
		}
	}

	if(uniform)
	{
		message = "uniform";
	}
	else
	{
		message = "nonuniform";
	}
}

// get_uniform_volume_mesh -- gets a uniform volume mesh
// x  -- holds coordinates of the mesh (assumes at least n-1 long)
// x0 -- mesh starting point
// x1 -- mesh ending point
// n  -- number of volumes
// endpoints -- should end volumes be half?
void Grid::get_uniform_volume_mesh(Scalar *x, Scalar xstart, Scalar xend, 
		int n, bool endpoints)
{
	int i, nstrt;
	if(xend <= xstart)
	{
		error("Grid::get_uniform_volume_mesh", "xend <= xstart");
	}
	if(n <= 0)
	{
		error("Grid::get_uniform_volume_mesh", "n <= 0");
	}

	nstrt = 0;
	if(endpoints)
	{
		nstrt = 1;
	}

	if(type == PLANAR)
	{
		Scalar delx = (xend - xstart)/Scalar(n - nstrt);
		// mindgame: (x[i] = x[i-1] + delx) makes core dump.
		// It makes larger cumulative error as i increases.
		// Unexpectively, it makes x[n-2] > x1.
		if(endpoints)
		{
			for(i=0; i < n-1; i++)
			{
				x[i] = xstart+(i+0.5)*delx;
			}
		}
		else
		{
			for(i=0; i < n-1; i++)
			{
				x[i] = xstart+(i+1)*delx;
			}
		}
	}
	else
	{
		if(xstart < 0.) { error("xstart < 0"); }
		Scalar vold = getV(xstart);
		Scalar vi = (getV(xend) - getV(xstart))/Scalar(n - nstrt);

		if(endpoints)
		{
			x[0] = getRfromV(vold + 0.5*vi);
			vold += 0.5*vi;
		}

		for(i=nstrt; i < n-1; i++)
		{
			x[i] = getRfromV(vold + vi);
			vold = getV(x[i]);
		}
	}
}

// mindgame: OK to delete?
#if 0
void Grid::get_uniform_weight_locations(Scalar *x, Scalar xstart, 
		Scalar xend,
		int n)
{
	int i;

	if(xend <= xstart)
	{
		error("Grid::get_uniform_weight_locations", "xend <= xstart");
	}
	if(n <= 0)
	{
		error("Grid::get_uniform_weight_locations", "n <= 0");
	}

	Scalar F;
	Scalar invtype = 1./Scalar(type+1);

	Scalar start = ipow(xstart, type+1);
	Scalar length = ipow(xend, type+1) - ipow(xstart, type+1);

	for(i = 0; i < n; i++)
	{
		F = (Scalar(i)+0.5)/Scalar(n);
		x[i] = pow(F*length + start, invtype);
	}
}
#endif

ostring Grid::lhs_string(void) const
{
	switch(type)
	{
	case PLANAR:
		return ostring("Left");
	default:
		return ostring("Inner");
	}
}

ostring Grid::rhs_string(void) const
{
	switch(type)
	{
	case PLANAR:
		return ostring("Right");
	default:
		return ostring("Outer");
	}
}

ostring Grid::geometry_string(int i) const
{
	switch(type)
	{
	case PLANAR:
		switch(i)
		{
		case 0:
			return ostring("x");
		case 1:
			return ostring("y");
		case 2:
			return ostring("z");
		default:
			error("Grid::geometry_string", "i should be 0, 1, or 2");
			break;
		}

	case CYLINDRICAL:
		switch(i)
		{
		case 0:
			return ostring("r");
		case 1:
			return ostring("theta");
		case 2:
			return ostring("z");
		default:
			error("Grid::geometry_string", "i should be 0, 1, or 2");
			break;
		}

	case SPHERICAL:
		switch(i)
		{
		case 0:
			return ostring("r");
		case 1:
			return ostring("theta");
		case 2:
			return ostring("phi");
		default:
			error("Grid::geometry_string", "i should be 0, 1, or 2");
			break;
		}
	}
	return ostring("ostring Grid::geometry_string(int i)");
}

void Grid::print_grid(FILE *fp) const
{
	int i;

	fprintf(fp, "\nGrid data: ng=%d", ng);
	for(i=0; i < ng; i++)
	{
		fprintf(fp, "\ni=%d; X=%e; Xcell=%e; A=%e; V=%e",
				i, X[i], Xcell[i], A[i], V[i]);
	}
	fprintf(fp, "\ni=%d; Xcell=%e; A=%e", i, Xcell[i], A[i]);
	fprintf(fp, "\nTotal volume: %e\n\n", getGridVolume());
}

