#include <stdio.h>
#include <math.h>
#include "main/pd1.hpp"
#include "main/parse.hpp"
#include "main/drive.hpp"

#include "main/drive_inline.hpp"

Drive::Drive()
{
  setdefaults();
  setparamgroup();
  vflag = true;  // ideal voltage source with V=0 by default
  check();
  init();
}

Drive::Drive(FILE *fp, oopicList<Parameter<Scalar> > float_variables,
	     oopicList<Parameter<int> > int_variables)
{
  init_parse(fp, float_variables, int_variables);
}

void Drive::setdefaults(void)
{
  phi = w0 = DC = AC = wt = wend = 0.0;
  theta0 = 0.;
  iflag = vflag = false;
  dcbiasflag = false;
  
  chirpstart = chirpend = 1E99;  
}

void Drive::setparamgroup(void)
{
  pg.add_bflag("I", &iflag, true, "indicates ideal current source drive");
  pg.add_bflag("V", &vflag, true, "indicates ideal voltage source drive");
  pg.add_bflag("DC_bias",&dcbiasflag, true, "indicates that DC bias should be calculated"); // HH 05/18/16
  pg.add_fparameter("DC", &DC, "[V or Amps] constant drive strength");
  pg.add_fparameter("AC", &AC, "[V or Amps] sinusoidal drive strength");
  pg.add_fparameter("phase", &phi, "[degrees] initial phase of AC drive");
  pg.add_fparameter("frequency", &w0, "[Hz] frequency of AC drive");
  pg.add_fparameter("chirpstart", &chirpstart, 
		    "[s] time to begin frequency chirp");
  pg.add_fparameter("chirpend", &chirpend,
      		    "[s] time to end frequency chirp");
  pg.add_fparameter("endfrequency", &wend, "[Hz] frequency following chirp");
  pg.add_fparameter("chirprate", &wt, 
		    "[Hz/s] rate of change of frequency in chirp");  
  pg.add_function("timefunction", "[V or I] time-dependent drive strength",
		  &timefunc, 1, "t");
}

void Drive::check(void)
{
  
  if(vflag && iflag)
    {
      error("", "Drive cannot be both a current and voltage source");
    }
  if(vflag && iflag)
    {
      error("", "Drive must be either a current or voltage source");
    }
  if(iflag && dcbiasflag)
  {
    error("","DC bias not implemented for current source");
  }

  if(pg.debug) { pg.print(); }

}

void Drive::init(void)
{
  theta0 = phi*2.*PI/360.0;
  w0 *= 2.*PI;
  wend *= 2.*PI;
  wt *= 2.*PI;

  //  theta0 -= w0*dt;
  
  if(pg.debug)
    {
      fprintf(stderr, "\ntheta0 = %g\n", theta0);
    }

  if((chirpend - chirpstart) > 0.)
    {
      if(wt != 0.)
	{
	  if(wend != 0.)
	    {
	      printf("\nDrive:  chirprate and final frequency both specified");
	      printf("\nUsing chirprate=%e\n", wt);
	    }
	  wend = wt*(chirpend - chirpstart);
	}
      else
	{
	  wt = (wend - w0)/(chirpend - chirpstart);
	}
    }      
}

void Drive::print(FILE *fp, Scalar t)
{
  fprintf(fp, "\n\nDrive:");
  if(iflag) { fprintf(fp, "\nCurrent Source"); }
  if(vflag) { fprintf(fp, "\nVoltage Source"); }
  fprintf(fp, "\nDC=%f; AC=%f; phi=%f; omega=%g", DC, AC, phi, w0);
  fprintf(fp, "\nphase=%g, sin(phase)=%g, timefunc.eval(t)=%g",
	  theta0, 
	  sin(theta0), 
	  timefunc.eval(t));
  fprintf(fp, "\ndt=%g\n", dt);
}
