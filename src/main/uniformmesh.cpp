#include "main/pd1.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "main/uniformmesh.hpp"

// default constructor
UniformMesh::UniformMesh()
{
	thearray = 0;
	xarray = 0;
	weight_order = 0;
	// mindgame: initialization
	missed_weight = 0;
}

UniformMesh::UniformMesh(DataArray * _thearray, Scalar _min, Scalar _max)
{
	weight_order = 0;
	// mindgame: initialization
	missed_weight = 0;
	init_mesh(_thearray, _min, _max);
}

UniformMesh::~UniformMesh()
{
	if(xarray) { delete xarray; }
}

void UniformMesh::init_mesh(DataArray * _thearray, Scalar _min, Scalar _max)
{
	thearray = _thearray;
	min = _min;
	max = _max;
	dx = (max - min)/Scalar(thearray->getSize() - 1);
	ng = thearray->getSize();
}

void UniformMesh::init_mesh(int _ng, Scalar _min, Scalar _max)
{
	min = _min;
	max = _max;
	ng = _ng;
	dx = (max - min)/Scalar(ng - 1);
}

DataArray * UniformMesh::get_xarray(ostring name)
{
	if(!xarray)
	{
		if(!thearray) { return 0; }
		xarray = new DataArray(name, thearray->getSize());
		Scalar xtmp = min;
		for(int i=0; i < thearray->getSize(); i++, xtmp += dx)
		{
			(*xarray)[i] = xtmp;
		}
	}
	return xarray;
}

////////////////////////
// indexing functions
////////////////////////

// int floor_index(Scalar xval): returns the minimum index
// that xval is greater than
// returns -1 for error
int UniformMesh::floor_index(Scalar xval, Scalar * frac)
{
	int ind;
	if(xval < min) { return -1; }
	if(xval > max) { return -1; }

	Scalar ftmp = (xval-min)/dx;
	ind = int(ftmp);
	if(frac) { *frac = ftmp-ind; }
	return ind;
}

////////////////////////
// weighting functions
////////////////////////

void UniformMesh::weight(Scalar xval, Scalar weight)
{
	int i;
	if((i = floor_index(xval)) < 0)
	{
		missed_weight += weight;
		return;
	}

	switch(weight_order)
	{
	case 0:
		weight_NGP(xval, weight);
		return;
	case 1:
		weight_linear(xval, weight);
		return;
	default:
		fprintf(stderr, "UniformMesh::weight -- undefined weight order=%d",
				weight_order);
		exit(1);
	}
}

/*
 * NGP = nearest-grid-point weighting
 */
void UniformMesh::weight_NGP(Scalar xval, Scalar weight)
{
	int i;
	if((i = floor_index(xval)) >= 0) // if in range
	{
		(*thearray)[i] += weight;
	}
	else
	{
		missed_weight += weight;
	}
}

/*
 * Linear weighting
 */
void UniformMesh::weight_linear(Scalar xval, Scalar weight)
{
	int i;
	Scalar frac;
	if((i = floor_index(xval, &frac)) >= 0) // if in range
	{
		(*thearray)[i] += (1.0 - frac)*weight;
		(*thearray)[i+1] += frac*weight;
	}
	else
	{
		missed_weight += weight;
	}
}

// is *this within UniformMesh a ?
bool UniformMesh::is_within(UniformMesh & a)
{
	if(get_min() < a.get_min()) { return false; }
	if(get_max() > a.get_max()) { return false; }
	return true;
}
