#include <stdio.h>
#include <stdlib.h>
#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/grid.hpp"
#include "fields/fields.hpp"
#include "fields/fields_inline.hpp"
#include "main/boundary.hpp"
#include "main/particlegroup.hpp"

#include "main/grid_inline.hpp"
#include "xgio.h" // for access to XGWrite() for dump_particles, MAL 5/5/10

ParticleGroup::ParticleGroup(int _nmax, Species *_theSpecies, Fields *_theFields, Scalar _w0, bool _variableWeight)
{
	nmax = _nmax;
	n=0;
	dual_index_flag = false;
	n_ptr = &n;
	variableWeight = _variableWeight;
	theSpecies = _theSpecies;
	theFields = _theFields;
	theGrid = theFields->get_theGrid();
	dt = theFields->get_dt();
	dxdt = theGrid->getdx()/dt;
	q0 = theSpecies->get_q();
	m0 = theSpecies->get_m();
	qm0 = theSpecies->get_qm();
	m0c_sq_e = m0 * SPEED_OF_LIGHT_SQ/PROTON_CHARGE;
	w0 = _w0;

	reallocflag = true;
	reallocmult = 2;

	set_norm_light_speed();
	half_m0_e = 0.5*m0*sqr(dxdt)/PROTON_CHARGE;
	set_nr_limit();

	checkData(); // check data consistency
} // default constructor

void ParticleGroup::init_dual_index(void)
{
	dual_index_flag = true;
	n_old = n;
	n_ptr = &n_old;
}

void ParticleGroup::turn_off_dual_index(void)
{
	dual_index_flag = false;
	n_ptr = &n;
}

// check for legal data
void ParticleGroup::checkData(void) 
{
	if (nmax <= 0)
	{
		fprintf(stderr,"\nunrecognized nmax in ParticleGroup\n");
		exit(1);
	}
}  

int ParticleGroup::add_particle(Scalar X, Vector3 Vvec, bool vnorm,
		bool update_field, Scalar W)
{
	return add_particle(X, Vvec.e1(), Vvec.e2(), Vvec.e3(),
			vnorm, update_field, W);
}


// add a particle, i, from ParticleGroup *p
int ParticleGroup::add_particle(ParticleGroup *p, int i, bool vnorm,
		bool update_field)
{
#ifdef DEBUG
	if(p->get_species() != get_species())
	{
		fprintf(stderr, "ParticleGroup::add_particle(ParticleGroup*p,int i):\n");
		fprintf(stderr, "species pointers don't match!!!\n");
		exit(1);
	}
#endif

	return add_particle(p->get_x(i), p->get_v(i),
			vnorm, update_field, p->get_w(i));
}

// MKS position of the ith particle
Scalar ParticleGroup::get_xMKS(int i) const {return theGrid->gridXtoX(get_x(i));} 

void ParticleGroup::Gather_E()
{

	register int i;
	Vector3 E_loc;

	for(i=0; i < n; i++)
	{
		theFields->get_E(get_x(i), E_loc);
		set_E(i, E_loc);
	}
}

// adds moments of the distribution function to thearray
void ParticleGroup::add_moment(int nmom, Scalar *thearray)
{
	for(int i=0; i < n; i++)
	{

	}
}

void ParticleGroup::print_particles(FILE *fp)
{
	Vector3 v;

	// JH, 9/10/2004 -- prints out number of particles for debugging
#ifdef DEBUG
	fprintf(stderr, "\nParticleGroup::print_particle=%d\n",n);
#endif
	fprintf(fp,"time=%g\n",dt);
	for(int i=0; i < n; i++)
	{
		v = get_v(i);
		if (get_xMKS(i)<0.10001 && get_xMKS(i)>0.09999)
		{
			fprintf(fp, "%g \t %g \t ", get_xMKS(i), v.e1());
			fprintf(fp, "%g \t %g \t %g\n ", v.e2(), v.e3(), get_q(i));
		}
	}


	fflush(fp);
}

void ParticleGroup::dump_particles(FILE *fp) // for dumpfile, MAL 5/4/10
{
	Scalar ftemp;
	if(n>0 && fp) // if this group has particles and file exists
	{
		for (int i=0; i < n; i++)
		{
			ftemp = get_xMKS(i);
			XGWrite(&ftemp, sizeof(Scalar), 1, fp, SCALAR_CHAR); //write xMKS to dumpfile
			ftemp = get_vMKS(i).e1();
			XGWrite(&ftemp, sizeof(Scalar), 1, fp, SCALAR_CHAR); //write v1MKS to dumpfile
			if(theSpecies->is_3v())
			{
				ftemp = get_vMKS(i).e2();
				XGWrite(&ftemp, sizeof(Scalar), 1, fp, SCALAR_CHAR); //write v2MKS to dumpfile
				ftemp = get_vMKS(i).e3();
				XGWrite(&ftemp, sizeof(Scalar), 1, fp, SCALAR_CHAR); //write v3MKS to dumpfile
			}
			ftemp = get_w(i);
			XGWrite(&ftemp, sizeof(Scalar), 1, fp, SCALAR_CHAR); //write weight to dumpfile
		}
	}
}

// mindgame: routine to check whether it is full or not
bool ParticleGroup::is_capable_of_adding_particles(int N)
{ 

	if(n+N >= nmax && !reallocflag)
	{
		return false;
	}
	else
	{
		return true;
	}
}

