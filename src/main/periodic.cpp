// sknam: For periodic BC (04/2007)
#include <stdio.h>

#include<vector>
using namespace std;

#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "utils/ostring.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/particlegrouparray.hpp"
#include "fields/fields.hpp"
#include "main/boundary.hpp"
#include "main/secondary.hpp"
#include "main/drive.hpp"
#include "main/circuit.hpp"
#include "utils/trimatrix.hpp"
#include "main/grid.hpp"
#include "main/periodic.hpp"
#include "fields/poissonfield.hpp"

#include "main/boundary_inline.hpp"
#include "main/grid_inline.hpp"
#include "fields/fields_inline.hpp"

// May not need Periodic list for sub catagories of periodic boundary.
// So may not need this constructor. 
Periodic::Periodic(Fields *t, int i) : Boundary(t)
{
  material = EMPTY;
  property = TRANSMITTED;
  setdefaults();  // from parse
  setparamgroup();
  j0 = j1 = i;
  check();
  init();
}


Periodic::Periodic(FILE *fp, 
		     oopicList<Parameter<Scalar> > float_variables,
		     oopicList<Parameter<int> > int_variables, Fields *t) :
  Boundary(t)
{
  material = EMPTY;
  property = TRANSMITTED;
  init_parse(fp, float_variables, int_variables);
}

Periodic::~Periodic() 
{
}


Property Periodic::particle_BC(ParticleGroup *p, int i, Scalar frac, 
				Direction j)
{
  
// sknam: needs to setup new positon of particles at each bounday for periodic condition.
  Scalar xnew;
  Scalar xold = p->get_x(i);
  Scalar end = thegrid->getng()-1;
  
  if(j==POSITIVE)// negative move
    {
      xnew = xold + end;
    }
  else if(j==NEGATIVE)// positive move
    {
      xnew = xold - end;
    }
  else
    {
      error("Direction should be defined at Periodic::particle_BC()");
    }

// sknam: Needed to modify the ParticleGroup and ParticleGroupArray class by adding new function set_x
// sknam: Is there any other way to modify the x value without creating new function, set_x ?
//    p->set_x(i, xnew);
 
// sknam: The following also can apply the periodic BC for particles without using creating set_x function
  Scalar w = p->get_w(i);
  Vector3 v = p->get_v(i);
  p->add_particles(1, w, &xnew, &v, false, false); 
  absorb(p, i, j);






//  if(accumulate_particles) 
//    {
//      error("Cannot be accumulated on the periodic boundary");
//      accumulate_particles=false;
//    }

//  secondary_emission(p, i, frac2, j);
//  radiation(p, i);

//  absorb(p, i, j);
  return property;
}


// For Poisson Solver
void Periodic::modify_coeffs(PoissonSolver * theps)
{
  if (thegrid->is_lhs(j0))
    {
//      theps->set_coeff(j0, 1., -2., 1.);
//      theps->set_rhs(j0, 0.0);
      theps->set_coeff(j0, 0., 1., 0.);
      theps->set_rhs(j0, 0.0);
    }
  if (thegrid->is_rhs(j1))
    {
//      theps->set_coeff(j1, 0., -1., 1.);
//      theps->set_rhs(j1, 0.0);
      theps->set_coeff(j1, 0., 1., 0.);
      theps->set_rhs(j1, 0.0);
    } 
}



void Periodic::setdefaults(void)
{
  classname = "Periodic";
//  add_child_name("Circuit");
}

void Periodic::setparamgroup(void)
{
}

void Periodic::check_periodic(void)
{
  if(j1 == -1)  { j1 = j0; }
}

void Periodic::init_periodic(void)
{
  surface_npoints = NONE;  // NONE == 2
  npoints = j1 - j0 + 1;
}

void Periodic::check(void)
{
  // mindgame: check_periodic() should be done first.
  check_periodic();
  check_boundary();
}

void Periodic::init(void)
{
  set_msg(classname);
  // mindgame: init_periodic() should be done first.
  init_periodic();
  init_boundary();
}

Parse * Periodic::special(ostring s) // Periodic doesn't need sub-block in input file. Don't need this function
{
  return 0;
}


