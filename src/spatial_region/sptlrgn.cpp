#include <stdio.h>

#ifdef MPIDEFINED
#include <mpi.h>
#endif

#include<vector>
using namespace std;

#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/load.hpp"
#include "main/plasmasource.hpp"
#include "main/grid.hpp"
#include "spatial_region/sptlrgn.hpp"
#include "fields/fields.hpp"
#include "main/boundary.hpp"
#include "main/conductor.hpp"
#include "main/drive.hpp" // add for dumpfile, MAL 5/22/10
#include "main/dielectric.hpp"
// sknam: Periodic object is created in this file
#include "main/periodic.hpp"
#include "utils/trimatrix.hpp"
#include "main/drive.hpp"
#include "main/circuit.hpp"
#include "fields/poissonfield.hpp"
#include "fields/directfield.hpp"
#include "xsections/xsection.hpp"
#include "reactions/reaction.hpp"
#include "reactions/reactionSubParse.hpp"
#include "MCC/mcc.hpp"
#include "reactions/reactionParse.hpp"
#include "main/timing.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_time.hpp"
#include "diagnostics/diagnostic_parser.hpp"
#include "diagnostics/diagnosticcontrol.hpp"
#include "xgio.h" // for dumpfile access to xgread and xgwrite, MAL 6/11/10

SpatialRegion::SpatialRegion()
{
	thediags = new DiagnosticControl;
	setdefaults();
	setparamgroup();
}

SpatialRegion::SpatialRegion(FILE *fp, oopicList<Parameter<Scalar> > float_variables, oopicList<Parameter<int> > int_variables, SpeciesList *sl, int pgtype)
{
	thediags = new DiagnosticControl;
	add_species(sl);
	set_ParticleGroupType(pgtype);

	init_parse(fp, float_variables, int_variables);
}  

// grid constructor
SpatialRegion::SpatialRegion(int nn, Scalar *newgrid, int geomtype, SpeciesList *slist, int pgtype)
{
	thediags = new DiagnosticControl;

	add_species(slist);
	set_ParticleGroupType(pgtype);

	setdefaults();
	setparamgroup();

	set_grid(nn, newgrid, geomtype);
}

SpatialRegion::~SpatialRegion()
{
	if(thegrid)  { delete thegrid; }

	reactiongrouplist.deleteAll();
	reactionparselist.deleteAll();
	if(thediags)
	{
		delete thediags;
	}

	loadlist.deleteAll();
	plasmaSourcelist.deleteAll();

	// mindgame: to avoid memory leaks
	boundarylist.deleteAll();

	// mindgame: thefield and specieslist can be deleted
	//          after (not before!!!) deleting the boundarylist.
	fieldlist.deleteAll();
	specieslist.deleteAll();

	delete [] V_DC;
}

// sets type of ParticleGroups to be used by Species
void SpatialRegion::set_ParticleGroupType(int type)
{
	oopicListIter<Species> thei(specieslist);

	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->set_ParticleGroupType(type);
	}
}

// sets fields for all species
void SpatialRegion::set_SpeciesFields()
{
	oopicListIter<Species> thei(specieslist);

	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->set_theField(thefield);
	}
}

////////////////////////////////////////
// functions for diagnostics
////////////////////////////////////////

void SpatialRegion::set_name(ostring nm)
{
	// don't allow SpatialRegions to be named twice
	if(name == "")    {      name = nm;    }
}

// set_diagnostic_parser: sets the DiagnosticParser from PlsmDev
// and initializes.  Should be called after thegrid and thefield
// are initialized.
void SpatialRegion::set_diagnostic_parser(DiagnosticParser *tdptr)
{
	fprintf(stdout, "\nInitializing diagnostics, SpatialRegion %s...\n", name());
	thedp = tdptr;
	thediags->set_diagnostic_parser(thedp);
	thediags->set_srname(name);
	// JK, DQW, 2019-11-25; adding reaction group list as a parameter for diagnostics
	thediags->initialize(&t, thegrid, thefield, &specieslist, &boundarylist, &reactiongrouplist);
	thediags->update();
}

// update -- update loop for SpatialRegion
void SpatialRegion::update()
{

	static bool init = false;
	Time thetimes;

	// JH, 9/14/2004 - print out time at each step when debugging
#ifdef DEBUG
	fprintf(stderr, "\nt=%g\n", t);
#endif
	// xgrafix timing
	if(init)
	{
		thetimes.end_time(XGRAFIXTIME);
	}
	else
	{
		init = true;
	}

	thetimes.start_time(FIELD_SOLVE);
	thefield->update_fields();
	thetimes.end_time(FIELD_SOLVE);

	oopicListIter<Boundary> thej(boundarylist);

	for(thej.restart(); !thej.Done(); thej++)
	{
		thej()->update_BC();
	}

	oopicListIter<Species> thei(specieslist);

	for(thei.restart(); !thei.Done(); thei++)
	{

#ifdef DEBUG
		fprintf(stderr, "Species %s:\n", thei()->get_name().c_str());
#endif                      
		int tp = thei()->number_particles();
		thetimes.start_particle_time(tp);
		thei()->advance_v(dt);
		thei()->advance_x(dt);
		thetimes.end_time(PARTICLE_PUSH);
	}

	// add for diagnostics velocity moments and timing, MAL 7/8/10
	for(thei.restart(); !thei.Done(); thei++)
	{
		thetimes.start_time(DIAGNOSTICS);
		thei()->accumulate_vel_moments();
		thetimes.end_time(DIAGNOSTICS);
	}

	if (reactiongrouplist.nItems())
	{
		for(thei.restart(); !thei.Done(); thei++)
		{
			thei()->turn_on_dual_index();
		}
		oopicListIter<ReactionGroup> rgIter(reactiongrouplist);
		for (rgIter.restart(); ! rgIter.Done(); rgIter ++)
		{
			thetimes.start_time(REACTION);
			rgIter()->update();
			thetimes.end_time(REACTION);
		}
		for (thei.restart(); !thei.Done(); thei++)
		{
			thei()->turn_off_dual_index();
		}
	}


	ApplyToList(integrate_fluids(dt),specieslist,Species);
	ApplyToList(update_BC_post(), boundarylist, Boundary);

	if(pg.debug)
	{
		FILE *tmpf;

		for(thei.restart(); !thei.Done(); thei++)
		{
			ostring fn = thei()->get_name();
			fn += ".dat";
			tmpf = fopen(fn(), "a");

			thei()->print_particles(tmpf);
			fclose(tmpf);
		}
	}

	// load PlasmaSource particles
	oopicListIter<PlasmaSource> psrc(plasmaSourcelist);
	for(psrc.restart(); !psrc.Done(); psrc++){
		psrc()->load(t, dt); // this does not presently time center v properly!
	}

	thetimes.start_time(DIAGNOSTICS);
	thediags->update();

	t += dt;
  
	// Diagnostics, HH 05/20/16
	nsteps = thefield->get_nsteps();
  
	// Only enter this loop once
	if (count_naverage == 0)
	{
		steps_rf = calculate_steps_rf();

		naverage = thefield->get_naverage();

		ng = thegrid->getng();


		if (spatiotemporal_flag)
		{
			init_spatiotemporal_diagnostics();
		}

		dcbiasflag = check_V_DC_diagnostics();
	
		if (dcbiasflag)
		{
			total_rf_periods = nsteps_total/steps_rf;
			V_DC = new Scalar [total_rf_periods+1]; // We start at zeroth timestep, hence the +1
			for (int k = 0; k<=total_rf_periods; k++)
			{
			  V_DC[k] = 0; // Init V_DC array
			}
		}
	
		count_naverage = 1;
	}

	// Spatiotemporal diagnostics, HH 03/03/16
	
	if (spatiotemporal_flag)
	{
		spatiotemporal_diagnostics();
	}


	// DC bias diagnostics
	if (dcbiasflag)
	{
		V_DC_diagnostics();
	}

    thetimes.end_time(DIAGNOSTICS);
    thetimes.start_time(XGRAFIXTIME);
}

// Find the lowest nonzero frequency, as specified in input file, HH 03/04/16
int SpatialRegion::calculate_steps_rf()
{
	int ntemp_conductor, ntemp_drive;
	oopicListIter<Conductor> thek(conductorlist);
	ntemp_conductor = conductorlist.nItems();
	int count = 0;
	if (ntemp_conductor>0)
	{
		for(thek.restart(); !thek.Done(); thek++)
		{
			drivelist = *(thek()->get_thecircuit()->get_drivelist());
			ntemp_drive = drivelist.nItems();
			if (ntemp_drive>0)
			{
				oopicListIter<Drive> them(drivelist);
				for (them.restart(); !them.Done(); them++)
				{
					w0_temp = them()->getw0();
					if ( fabs(them()->getDC()) > 1E-10 || fabs(them()->getAC()) > 1E-10) // We do not want to take into account frequencies when voltage in the input file has been set to zero
					{
						if (count == 0)
						{
							w0 = w0_temp;
							count = 1;
						}
						if (w0_temp>1E-10 && w0_temp < w0)
						{
							w0 = w0_temp; // Find the lowest nonzero frequency
						}
					}
				}
			}
		}
	}
	f_low = w0 / (2 * PI);

	return 1/(f_low*dt);     // Total number of steps in RF cycle
}
 
void SpatialRegion::init_spatiotemporal_diagnostics()
{
	oopicListIter<Species> thei(specieslist); 
	
	// Species
	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->init_arrays(steps_rf*ng); // Make arrays the size of (timesteps in rf cycle)x(number of gridpoints)
	}
	
	thefield->init_arrays(steps_rf*ng); // Make phi array the size of (timesteps in rf cycle)x(number of gridpoints)
	oopicListIter<Boundary> thej(boundarylist);
        
	// Boundary
	for(thej.restart(); !thej.Done(); thej++)
	{
		thej()->init_arrays(steps_rf);
	}
}
  
void SpatialRegion::spatiotemporal_diagnostics()
{
	naverage_array = naverage/steps_rf;

	if (nsteps_total>=naverage && nsteps>(nsteps_total-naverage-1) && nsteps!=nsteps_total)
	// Only executed if we are close enough for the simulation to end, to speed up
	{
		int m;
		m = nsteps % steps_rf;

		oopicListIter<Species> thei(specieslist);

		for(thei.restart(); !thei.Done(); thei++)
		{
			thei()->set_arrays(ng,steps_rf,m);
		}
    
		// Field
		thefield->set_arrays(ng,steps_rf,m);
		// Boundarys
		oopicListIter<Boundary> thej(boundarylist);

		for(thej.restart(); !thej.Done(); thej++)
		{
			thej()->set_arrays(steps_rf,m);
		}
	}
}
  
    


// Checks whether DC bias flag from input file is true
bool SpatialRegion::check_V_DC_diagnostics()
{
	
	oopicListIter<Conductor> thek(conductorlist);
	
	for(thek.restart(); !thek.Done(); thek++)
	{
		drivelist = *(thek()->get_thecircuit()->get_drivelist());
		oopicListIter<Drive> them(drivelist);
	
		for (them.restart(); !them.Done(); them++)
		{
			if (them()->get_dcbiasflag())
			{
				return true;
			}
		}
	}

	return false;
}

void SpatialRegion::V_DC_diagnostics()
{
    if (nsteps % steps_rf == 0)
    {
		oopicListIter<Conductor> thek(conductorlist);

		// Find the boundary which is powered
		for(thek.restart(); !thek.Done(); thek++)
		{
			drivelist = *(thek()->get_thecircuit()->get_drivelist());
	
			oopicListIter<Drive> them(drivelist);

			for (them.restart(); !them.Done(); them++)
			{
				// Find the driven conductor
				if (fabs(them()->getDC()) > 1E-10 || fabs(them()->getAC()) > 1E-10)
				{
					// Find and calculate V_DC
					V_DC[nsteps/steps_rf] = thek()->calculate_V_DC();
					them()->set_DC(V_DC[nsteps/steps_rf]);
					break;
				}
			}
	
			// Reset matrices for absorbed and emitted particles from the walls in each rf cycle
			thek()->reset_absorbed_particles();
			thek()->reset_emitted_particles();
		}
	}
}

void SpatialRegion::add_species(SpeciesList *slist)
{
	oopicListIter<Species> thei(*slist);

	for(thei.restart(); !thei.Done(); thei++)
	{
		specieslist.add(new Species(thei()));
	}
}

void SpatialRegion::set_grid(int nn, Scalar *newgrid, int geomtype)
{
	if(thegrid != NULL)
	{
		delete thegrid;
	}
	thegrid = new Grid(nn, newgrid, geomtype);
}

// all particles in the SpatialRegion
int  SpatialRegion::total_number_particles()
{
	int np;
	oopicListIter<Species> thei(specieslist);

	np = 0;

	for(thei.restart(); !thei.Done(); thei++)
	{
		np += thei()->number_particles();
	}
	return np;
}

void SpatialRegion::setdefaults_SR()
{  
	classname = "SpatialRegion";
	name = "";
	t = dt = 0.;
	thegrid = 0;
	thefield = 0;
	spatiotemporal_flag = false;
}

void SpatialRegion::setparamgroup_SR()
{  
	pg.add_string("name", &name, "name of the SpatialRegion");
	pg.add_fparameter("dt", &dt, "[s] time step");
	pg.add_bflag("spatiotemporal", &spatiotemporal_flag, true, "flag for spatiotemporal diagnostics");
	add_child_name("Grid");
	add_child_names("Fields", 2, "PoissonSolver", "DirectSolver");
	add_child_names("Boundary types", 3, "Dielectric", "Conductor", "Boundary");
	add_child_name("Load");
	add_child_name("PlasmaSource");
	add_child_name("MCC");
	add_child_name("Reactions");
}

void SpatialRegion::check_SR()
{  
	if(dt == 0.0) { error("dt not set."); }
	if(thegrid == NULL)
	{
		error("Grid not initialized");
	}

	if(thefield == NULL)
	{
		error("Field not initialized");
	}
	check_boundaries();
}

// check_Dirichlet:  returns true if a Dirichlet boundary
// condition is found, false otherwise
bool SpatialRegion::check_Dirichlet()
{  
	oopicListIter<Conductor> thei(conductorlist);
	for(thei.restart(); !thei.Done(); thei++)
	{
		if(thei()->is_idealvoltagesource()) { return true; }
	}
	// sknam: Periodic has Dirichlet
	return false;
}

void SpatialRegion::check_boundaries()
{
	// mindgame: this should be conductor, not boundary
	oopicListIter<Boundary> thei(boundarylist);
	//  oopicListIter<Conductor> thei(conductorlist);
	Conductor *c;
	bool l_flag = false;
	bool r_flag = false;

	fprintf(stderr, "\nChecking boundaries....");

	if(!thegrid->isperiodic())
	{
		for(thei.restart(); !thei.Done(); thei++)
		{
			if(thei()->get_j0() == 0)
			{
				l_flag = true;
			}
			if(thei()->get_j1() == thegrid->getng()-1)
			{
				r_flag = true;
			}
		}
		if(!r_flag)
		{
			c = new Conductor(thefield, thegrid->getng()-1);
			boundarylist.add(c);
			conductorlist.add(c);

		}
		if(!l_flag)
		{
			c = new Conductor(thefield, 0);
			boundarylist.add(c);
			conductorlist.add(c);
		}
	}
	else
	{
		error("SpatialRegion::check_boundaries",
				"periodic boundary conditions not yet implemented");
	}

	// check to ensure that at least one Dirichlet BC is found
	if(!check_Dirichlet())
	{
		/* sknam: Temperally close this line for periodic BC test
		error("SpatialRegion::check_boundaries", "At least one Dirichlet boundary condition required");
		 */
	}

	// after checking the boundaries,
	// set the boundaries for the classes that need them
	thegrid->setboundaries(boundarylist);
	thefield->setdielectrics(&dielectriclist);
	thefield->setconductors(&conductorlist);
	if (thefield->is_PoissonField()) thefield->setmatrixcoef();
}

void SpatialRegion::init_SR()
{

	V_DC = 0;
	dcbiasflag = false;

	set_SpeciesFields();

	if(WasDumpFileGiven)
	{
		reload_dumpfile_SR(); // reload dumpfile function in SR, MAL 5/16/10
	}

	else  // add initially loaded particles
	{
		oopicListIter<Load> thei(loadlist);
		for(thei.restart(); !thei.Done(); thei++)
		{
			thei()->load(dt);
			//			printf("\nSpatialRegion::init_SR, thei()=%p, ident=%i",(void*)thei(),thei()->get_loadspecies()->get_ident()); // for debug
		}
	}

	//	printf("\nSpatialRegion::init(), t=%g\n",get_t()); // for debug
	thefield->update_fields();

	// move v back for leap-frog integrator
	oopicListIter<Species> thej(specieslist);
	for(thej.restart(); !thej.Done(); thej++)
	{
		// mindgame: this method is just 1st-order accurate.
		// thej()->advance_v(-0.5*dt);
		// mindgame: this method is just 2nd-order accurate.
		thej()->advance_vload(dt);
	}

	count_naverage = 0; // In order to only calculate naverage once, HH 04/19/2016
}

Parse * SpatialRegion::special_SR(ostring s)
{ 
	Conductor *theconductor;
	Dielectric *thedielectric;

	if(s == "Grid")
	{
		thegrid = new Grid(f, pg.float_variables, pg.int_variables);

		// JH, 9/10/2004 -- print grid for debugging
#ifdef DEBUG
		FILE *fp = fopen("grid.dat", "w");
		thegrid->print_grid(fp);
		fclose(fp);
#endif

		return thegrid;
	}

	/////////////////
	// field solves
	/////////////////

	ostring k("PoissonSolver");
	if(s == k)
	{
		if(thegrid == NULL)
		{
			error(k(), "Grid must be initialized before the field solver");
		}
		thefield = new PoissonSolver(f, pg.float_variables,pg.int_variables,
				this);
		fieldlist.add(thefield);
		return thefield;
	}

	k = "DirectSolver";

	if (s==k){

		if (thegrid == NULL) error(k(), "Grid must be initialized previously");
		thefield = new DirectSolver(f, pg.float_variables, pg.int_variables, this);
		fieldlist.add(thefield);
		return thefield;
	}


	k = "Load";
	if(s == k)
	{
		Load *loadptr;
		if(thegrid == NULL)
		{
			error(k(), "Grid must be initialized before loading");
		}

		loadptr = new Load(f,pg.float_variables,pg.int_variables,
				thegrid, &specieslist);
		loadlist.add(loadptr);
		return loadptr;
	}

	k = "PlasmaSource";
	if(s == k)
	{
		PlasmaSource *plasmaSourceptr;
		if(thegrid == NULL)
		{
			error(k(), "Grid must be initialized before PlasmaSource");
		}

		plasmaSourceptr = new PlasmaSource(f,pg.float_variables,pg.int_variables,
				thegrid, &specieslist);

		plasmaSourcelist.add(plasmaSourceptr);
		return plasmaSourceptr;
	}

	///////////////////////
	// boundary conditions
	///////////////////////

	k = "Conductor";
	if(s == k)
	{
		if(thefield == NULL)
		{
			error(k(), "Fields must be initialized before boundary conditions");
		}
		theconductor = new Conductor(f, pg.float_variables,
				pg.int_variables, thefield);
		boundarylist.add(theconductor);
		conductorlist.add(theconductor);
		return theconductor;
	}

	k = "Dielectric";
	if(s == k)
	{
		if(thefield == NULL)
		{
			error(k(), "Fields must be initialized before boundary conditions");
		}
		thedielectric = new Dielectric(f, pg.float_variables,
				pg.int_variables, thefield);
		boundarylist.add(thedielectric);
		dielectriclist.add(thedielectric);
		return thedielectric;
	}

	k = "Boundary"; // generic boundary condition
	if(s == k)
	{
		if(!thefield)
		{
			error(k(), "Fields must be initialized before boundary conditions");
		}
		Boundary *theboundary = new Boundary(f, pg.float_variables,
				pg.int_variables, thefield);
		boundarylist.add(theboundary);
		return theboundary;
	}

	// sknam: To add periodic boundary to the boundarylist for periodic BC
	k = "Periodic";// If there is "Periodic" boundary in the input file, add it to boundarylist.
	if(s == k)
	{
		if(!thefield)
		{
			error(k(), "Field must be initialized before boundary conditions");
		}
		Boundary *theboundary = new Periodic(f, pg.float_variables,
				pg.int_variables, thefield);
		boundarylist.add(theboundary);
		// No need for periodiclist
		return theboundary;
	}

	k = "Reactions";
	if(s == k)
	{
		ReactionParse* reactionParse  = new ReactionParse(f, pg.float_variables, pg.int_variables, &specieslist, dt, thefield);
		reactionparselist.add(reactionParse);
		reactiongrouplist += *(reactionParse->get_reactiongrouplist());

		return reactionParse;
	}
	return 0;
}

// Reload dumpfile function, called by init() after call to reload_dumpfile() in PlsmDev, MAL, 5/16/10
void SpatialRegion::reload_dumpfile_SR()
{
	FILE * DMPFile;
	if((DMPFile = fopen(theDumpFile,"r")) == NULL) {
		printf("\nSpatialRegion::init_SR: open dumpfile failed\n"); return;
	}
	Scalar f, r0, r1, tt;
	Scalar sourcestrength, circq, circq1, circq2, circq3, sourceq, sourceq1, sourceq2, sourceq3;
	Scalar sigma, Q, Iplushalfdt, dQcirc, dQcirc1, sumQ, theta0;
	Scalar extra, aveNcoll_per_step;
	int n, ncond, ndrive, nbound, k;
	int nsp, id, is3v, isvw, npart, npts, nrg, irg1, irg2, nfluids;
	long int nsteps, li;
	bool loadflag, rev0, rev1, rev2, rev3; // add rev2, rev3, MAL200229
	oopicListIter<Load> thei(loadlist);
	char rev[5];
	char Revision [] = {'1','.','0','0'}; //original revision number for dump and restore
	char Revision1 [] = {'1','.','0','1'}; //revision number 1.01 for dump and restore
	char Revision2 [] = {'1','.','0','2'}; //revision number 1.02 for dump and restore; MAL200229
	char Revision3 [] = {'1','.','0','3'}; //revision number 1.03 for dump and restore; MAL200229
	XGRead(rev,sizeof(char),4,DMPFile,"char");
	rev0 = true; // Version 1.00 of dumpfile
	rev1 = true; // Version 1.01 of dumpfile
	rev2 = true; // add, MAL200229
	rev3 = true; // add, MAL200229
	for (k=0; k<4; k++)
	{
		if (!(rev[k]==Revision[k])) rev0 = false; // any higher version of dumpfile, with dump and restore of ReactionGroup
    if (!(rev[k]==Revision1[k])) rev1 = false; // any higher version of dumpfile, with dump and restore of aveNcoll_per_step
    if (!(rev[k]==Revision2[k])) rev2 = false; // any higher version of dumpfile, with dump and restore of aveNcoll_per_step, MAL200229
    if (!(rev[k]==Revision3[k])) rev3 = false; // any higher version of dumpfile, with dump and restore of aveNcoll_per_step, MAL200229
	}
	XGRead(&tt,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //read time
	set_t(tt);
	XGRead(&n,sizeof(int),1,DMPFile,"int"); //read nsr
	XGRead(&nsteps,sizeof(long int),1,DMPFile,"int");
	//		get_field()->set_nsteps(nsteps); // to restore nsteps in fields, not restored now, MAL 5/15/10
	XGRead(&nsp,sizeof(int),1,DMPFile,"int"); // get number of species
	//		printf("\nSpatialRegion::init_SR, nsp=%i\n",n);
	if (nsp <=0) {
		printf("\nSpatialRegion::init_SR, error in getting number of species");
	}
	li = 3*nsp*sizeof(int);
	n = fseek(DMPFile,li,SEEK_CUR); //skip species id's, isv3's, and npart's
	if (n) {
		printf("\nSpatialRegion::init_SR, error in fseek");
	}

	// Restore boundary values
	XGRead(&nbound,sizeof(int),1,DMPFile,"int"); // number of boundaries
	if (nbound > 0)
	{
		//		printf("\nSpatialRegion::init_SR, nbound=%i\n",nbound);
		for (k = 0; k < nbound; k++)
		{
			//				printf("\nFrom SpatialRegion::reload_dumpfile_SR(), k=%i\n",k); //for debug
			XGRead(&r0,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
			XGRead(&r1,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
			XGRead(&sumQ,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //sum_Qconv
			XGRead(&npts,sizeof(int),1,DMPFile,"int"); //npts on boundary
			oopicListIter<Boundary> thek(boundarylist);
			for(thek.restart(); !thek.Done(); thek++)
			{
				loadflag = false;
				if (r0 == thek()->getr0() && r1 == thek()->getr1())
				{
					thek()->set_sumQ(sumQ);
					thek()->reload_weights(DMPFile);
					loadflag = true;
					break;
				}
			}
			if (!loadflag) // boundary was not loaded, so skip weights in dumpfile
			{
				printf("\nSpatialRegion::reload_dumpfile_SR(), boundary %i not reload\n", k);
				int jmax = npts*nsp;
				for (int j = 0; j < jmax; j++)
				{
					XGRead(&f,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				}
			} // end skip boundary weights
		} // end loop over boundaries
	} // end no boundaries

	// Restore conductor and circuit charges
	XGRead(&ncond,sizeof(int),1,DMPFile,"int"); // number of conductors
	if (ncond > 0)
	{
		//		printf("\nSpatialRegion::init_SR, ncond=%i\n",n);
		for (k = 0; k < ncond; k++)
		{
			XGRead(&r0,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
			XGRead(&r1,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
			XGRead(&sourcestrength,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //sourcestrength
			XGRead(&circq,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //circ_q
			XGRead(&circq1,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //circ_q1
			XGRead(&circq2,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //circ_q2
			XGRead(&circq3,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //circ_q3
			XGRead(&sourceq,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //source_q (placeholder for 2nd current loop, not implemented)
			XGRead(&sourceq1,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //source_q1
			XGRead(&sourceq2,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //source_q2
			XGRead(&sourceq3,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //source_q3
			XGRead(&sigma,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //sigma
			XGRead(&Q,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //Q for current source
			XGRead(&Iplushalfdt,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); //I_plushalfdt
			XGRead(&dQcirc,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
			XGRead(&dQcirc1,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
			oopicListIter<Conductor> thek(conductorlist);
			for(thek.restart(); !thek.Done(); thek++)
			{
				if (r0 == thek()->getr0() && r1 == thek()->getr1())
				{
					thek()->get_thecircuit()->set_sourcestrength(sourcestrength);
					thek()->get_thecircuit()->set_circ_q(circq);
					thek()->get_thecircuit()->set_circ_q1(circq1);
					thek()->get_thecircuit()->set_circ_q2(circq2);
					thek()->get_thecircuit()->set_circ_q3(circq3);
					thek()->get_thecircuit()->set_source_q(sourceq);
					thek()->get_thecircuit()->set_source_q1(sourceq1);
					thek()->get_thecircuit()->set_source_q2(sourceq2);
					thek()->get_thecircuit()->set_source_q3(sourceq3);
					thek()->get_thecircuit()->set_sigma(sigma);
					thek()->get_thecircuit()->set_Q(Q);
					thek()->set_I_plushalfdt(Iplushalfdt);
					thek()->set_dQ_circ(dQcirc);
					thek()->set_dQ_circ1(dQcirc1);
					// add drive restore, 5/22/10
					XGRead(&ndrive,sizeof(int),1,DMPFile,"int"); // number of drives
					if (ndrive > 0)
					{
						drivelist = *(thek()->get_thecircuit()->get_drivelist());
						oopicListIter<Drive> them(drivelist);
						for (them.restart(); !them.Done(); them++)
						{
							XGRead(&theta0,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
							them()->setphase(theta0);
							//								printf("\nSpatialRegion::reload_dumpfile_SR(), theta0=%g\n",theta0); // for debug
						}
					}
					break;
				}
			}
		} // end loop over conductors
	} // end no conductors

	// Restore all loaded particle species (NOT all species!)
	XGRead(&nsp,sizeof(int),1,DMPFile,"int"); // number of species in dumpfile
	//		printf("\nSpatialRegion::init_SR, nsp=%i",nsp); // for debug
	for (int i = 0; i < nsp; i++)
	{
		XGRead(&id,sizeof(int),1,DMPFile,"int"); // species id number
		XGRead(&is3v,sizeof(int),1,DMPFile,"int"); // is3v
		XGRead(&isvw,sizeof(int),1,DMPFile,"int"); // isvariableWeight
		XGRead(&npart,sizeof(int),1,DMPFile,"int"); // total number of particles of all weights
		//			printf("\nisp=%i, id=%i, is3v=%i, isvw=%i, npart=%i",i,id,is3v,isvw,npart);
		if (npart > 0)
		{
			//				oopicListIter<Load> thei(loadlist);
			for(thei.restart(); !thei.Done(); thei++)
			{
				loadflag = false;
				//					printf("\ndumpfile id=%i, loadlist id=%i",id, thei()->get_loadspecies()->get_ident()); // for debug
				if (thei()->get_loadspecies()->get_ident() == id)
				{
					//						printf("\nMatch found, reloading, thei()=%p, ident=%i",(void*)thei(),id); // for debug
					thei()->reload(dt,is3v,npart,DMPFile);
					loadflag = true;
					break;
				}
			}
			if (!loadflag) // species was not loaded, so skip it
			{
				printf("\nMatch not found, not reloading species with ident=%i\n",id);
				int jmax = 3*npart;
				if (is3v) jmax += 2*npart;
				for (int j = 0; j < jmax; j++)
				{
					XGRead(&f,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
				}
			} // end skip species
		} // end no particles
	} // end loop over species

	// Version 1.01 and above: Restore mcc variable extra; negative (or zero) number of physical collisions done in excess in the previous time step
	if(!rev0) // if not Version 1.00
	{
		XGRead(&nrg,sizeof(int),1,DMPFile,"int"); // number of reaction groups in dumpfile
		if (nrg > 0)
		{
			oopicListIter<ReactionGroup> then(reactiongrouplist);
			for (int j = 0; j < nrg; j++) // at least 1 reaction group exists
			{
				XGRead(&irg1,sizeof(int),1,DMPFile,"int"); // reactant1 id number
				XGRead(&irg2,sizeof(int),1,DMPFile,"int"); // reactant2 id number
				XGRead(&extra,sizeof(Scalar),1,DMPFile,SCALAR_CHAR); // extra
				for(then.restart(); !then.Done(); then++)
				{
					int r1 = then()->get_reactant1()->get_ident();
					int r2 = then()->get_reactant2()->get_ident();
					if ((r1 == irg1 && r2 == irg2) || (r2 == irg1 && r1 == irg2)) then()->get_mcc()->set_extra(extra);
				}
				if(!rev1) // restore aveNcoll_per_step
				{
					XGRead(&aveNcoll_per_step,sizeof(Scalar),1,DMPFile,SCALAR_CHAR);
					for(then.restart(); !then.Done(); then++)
					{
						int r1 = then()->get_reactant1()->get_ident();
						int r2 = then()->get_reactant2()->get_ident();
						if ((r1 == irg1 && r2 == irg2) || (r2 == irg1 && r1 == irg2))
						{
							then()->get_mcc()->set_aveNcoll_per_step(aveNcoll_per_step);
						}
					}
				}
			}
		}
    
		if(rev3) //Version 1.03 restore evolving fluid densities; MAL200229
		{
			oopicListIter<Species> thes(specieslist);
			XGRead(&nsp,sizeof(int),1,DMPFile,"int"); // number of species in dumpfile
			if (nsp >= 1)
			{
				for(thes.restart(); !thes.Done(); thes++)
				{
					XGRead(&id,sizeof(int),1,DMPFile,"int"); // id of species in dumpfile
					if (thes()->get_ident() == id) // check for species match
					{
						XGRead(&nfluids,sizeof(int),1,DMPFile,"int"); // number of fluids for the species in dumpfile
						if (nfluids >= 1) // there are fluids
						{
							thes()->reload_fluids(DMPFile);
						}
					}
					else
					{
						printf("\nSpatialRegion::reload_dumpfile_SR: species in dumpfile does not match species in input file; ABORT RELOAD");
						return;
					}
				}
			}
		}
	}
	fclose(DMPFile);
}	
// use "_SR" functions for SpatialRegion functions
// inherited from Parse
void SpatialRegion::setdefaults()   { setdefaults_SR();   }
void SpatialRegion::setparamgroup() { setparamgroup_SR(); }
void SpatialRegion::check() { check_SR(); }
void SpatialRegion::init()  { init_SR(); }
Parse * SpatialRegion::special(ostring s) { return special_SR(s); }

#ifdef MPIDEFINED
// MPI SpatialRegion functions --
// send and recv relavent data to other processsors 
// in order to construct SpatialRegions remotely

// -- doesn't work, as of March, 2006
// should we kill this?  finish it??? JH, March, 2006

void SpatialRegion::send_classdata()
{
	// send global data first
	send_speciesdata();
}

void SpatialRegion::recv_classdata()
{
	// recv global data first --
	// particlegrouptype, dt
	recv_speciesdata();
}

void SpatialRegion::send_speciesdata() { }

void SpatialRegion::recv_speciesdata() { }


#endif // MPIDEFINED
