#include <stdio.h>

#ifdef MPIDEFINED
#include <mpi.h>
#endif

#include<vector>
using namespace std;

#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/load.hpp"
#include "main/grid.hpp"
#include "spatial_region/sptlrgn.hpp"
#include "fields/fields.hpp"
#include "main/boundary.hpp"
#include "main/conductor.hpp"
#include "utils/trimatrix.hpp"
#include "main/drive.hpp"
#include "main/circuit.hpp"
#include "fields/poissonfield.hpp"
#include "spatial_region/sptlrgn.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_time.hpp"
#include "diagnostics/diagnosticcontrol.hpp"
#include "main/plsmdev.hpp"
#include "spatial_region/sptlrgngrp.hpp"

SpatialRegionGroup::SpatialRegionGroup(FILE *fp, 
		oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables,
		SpeciesList *sl,
		int pgtype, int r, int np,
		PlsmDev *thepd)
{
	rank = r;
	nproc = np;
	theplasmadevice = thepd;
	mastergrid = NULL;		// added by JK, 2017-09-22
	inputfileline = 0;		// added by JK, 2017-09-22

	fprintf(stderr, "\nSpatialRegionGroup() constructor, rank %d\n", rank);

	add_species(sl);
	set_ParticleGroupType(pgtype);

	init_parse(fp, float_variables, int_variables);
}

// functions from Parse and SpatialRegion
void SpatialRegionGroup::setdefaults() { setdefaults_SR(); }
void SpatialRegionGroup::setparamgroup()
{
	setparamgroup_SR();
}

void SpatialRegionGroup::check()
{
	check_SR();
}

void SpatialRegionGroup::init()
{
	init_SR();
}

Parse * SpatialRegionGroup::special(ostring s)
{

	if(s == ostring("Grid"))
	{
		fprintf(stderr, "\nSpatialRegionGroup:: parsing grid....");

		mastergrid = new Grid(f, pg.float_variables, pg.int_variables);

		fprintf(stderr, "....done.\n");

#ifdef DEBUG
		fprintf(stderr, "\n...printing master grid --");
#endif

		FILE *fp = fopen("mastergrid.dat", "w");
		mastergrid->print_grid(fp);
		fclose(fp);

#ifdef DEBUG
		fprintf(stderr, "--done!\n");
#endif

		inputfileline = globallinenumber;

		// JK
		printf("globallinenumber: %d\n", globallinenumber);

		// split up SpatialRegions
		fprintf(stderr, "\ncreating SpatialRegions....");

		createSpatialRegions(theplasmadevice);

		fprintf(stderr, "...done");

		return mastergrid;
	}
	if(special_SR(s))
	{
		error("", "");
	}

	return 0;
}


// createSpatialRegions --
// split up the region into different SpatialRegions for different procs
// Communication routine:
// 1) send number of grids and geometry type
// 2) send grid data
void SpatialRegionGroup::createSpatialRegions(PlsmDev *thep)
{
	int i, ng, nend, ncurrent;
	int ngg[2];
	int *ngproc;
	Scalar **nstart;
	Scalar *gridx;
	Scalar work, rem;

#ifdef MPIDEFINED
	MPI_Status stat;
	fprintf(stderr, "\nentering SpatialRegionGroup::createSpatialRegions(), rank %d\n", rank);
#else
	fprintf(stderr, "\nentering SpatialRegionGroup::createSpatialRegions()\n");
#endif

	// variables for MPI commmunication
	Scalar *sendbuffer;

	if(mastergrid == NULL)
	{
		error("","");
	}

	ngproc = new int[nproc]; // number of grids in each processor
	nstart = new Scalar*[nproc]; // starting Scalar for each proc

	ng =  mastergrid->getng();
	work = ng/(Scalar (nproc));
	gridx = mastergrid->getX();

	// geometry type for all procs
	ngg[1] = mastergrid->geometry();

	sendbuffer = new Scalar[ng + 2];

	for(i=0, nend=0, rem = 0.; i < nproc; i++)
	{
		ncurrent = nend;

		nstart[i] = &(gridx[ncurrent]);

		if(i == nproc-1)
		{
			nend = ng-1;
		}
		else
		{
			nend = int(ncurrent + work + rem);
			rem = ncurrent + work - nend; //update remainder to even load
		}

		ngg[0] = nend - ncurrent + 1;

		if(i == PD1_MPI_ROOT) // local processor
		{
			fprintf(stderr, "\nSpatialRegionGroup::createSpatialRegions, PD1_MPI_ROOT processor\n");

			set_grid(ngg[0], gridx, ngg[1]);

			fprintf(stderr, "-- done creating grid.");
		}
		else  // send data to other processors
		{
#ifdef MPIDEFINED
			//  section modified by JK, 2017-09-22
			fprintf(stderr, "\nSpatialRegionGroup::createSpatialRegions, for rank: %d\n", i);

			// send species data
			int nsptmp = specieslist.nItems();
			oopicListIter<Species> thes(specieslist);
			Scalar sdata[NSPDATA];
			char sname[STR_SIZE];
			MPI_Send(&nsptmp, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
			for(thes.restart(); !thes.Done(); thes++)
			{
				sdata[NSPQ]    = thes()->get_q();
				sdata[NSPM]    = thes()->get_m();
				sdata[NSPNP2C] = thes()->get_np2c();
				MPI_Send(sdata, NSPDATA, MPI_SCALAR, i, 0, MPI_COMM_WORLD);
				sprintf(sname, "%s", thes()->get_name().c_str());
				MPI_Send(sname, STR_SIZE, MPI_CHAR, i, 0, MPI_COMM_WORLD);
			}

			// send generic SpatialRegion data
			int dttmp = dt;
			MPI_Send(&dttmp, 1, MPI_SCALAR, i, 0, MPI_COMM_WORLD);

			// send grid data
			MPI_Send(ngg, 2, MPI_INT, i, 0, MPI_COMM_WORLD);
			MPI_Send((gridx + ncurrent), ngg[0], MPI_SCALAR, i, 0, MPI_COMM_WORLD);
#endif

			// send line to start parsing file from
//#ifdef DEBUG
			fprintf(stderr, "\nSpatialRegionGroup::creatSpatialRegions(), line number=%d\n", inputfileline);
//#endif

#ifdef MPIDEFINED
			MPI_Send(&inputfileline, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
#endif
			fprintf(stderr, "-- done creating grid (rank: %d).", rank);
		}
	}

	// wait for other processes to finish before going on
#ifdef MPIDEFINED
	MPI_Barrier(MPI_COMM_WORLD);
#endif


	// cleanup -- deallocate memory
	delete [] ngproc;
	delete [] nstart;
	delete [] sendbuffer;
}

