#include<stdarg.h>

#include<vector>
using namespace std;

#include "main/pd1.hpp"
#include "utils/ostring.hpp"
#include "utils/equation.hpp"

Equation::Equation()
{
  fparse = 0;
  fatal = true;
  buff = 0;
  kon  = 0;
  konallocflag = true;
  initfparseflag = false;
  setconstant(0.0);
}

Equation::Equation(ostring eq)
{
  fparse = 0;
  fatal = true;
  buff = 0;
  kon  = 0;
  konallocflag = true;
  initfparseflag = false;
  setconstant(0.);
  parseall(eq);
}

Equation::Equation(Scalar C)
{
  fparse = 0;
  fatal = true;
  buff = 0;
  kon  = 0;
  konallocflag = true;
  initfparseflag = false;
  setconstant(C);
}

Equation::Equation(Scalar *C)
{
  fparse = 0;
  fatal = true;
  buff = NULL;
  kon = C;
  konallocflag = false;
  initfparseflag = false;
}

Equation::~Equation()
{
  if(initfparseflag)
    {
      delete fparse;
    }  
  if(konallocflag) 
    { 
      delete kon; 
    }
  if(buff)
    {
      delete [] buff;
    }
}

// mindgame: copy fn.
void Equation::copy(const Equation &cpy)
{
  if (cpy.initfparseflag && !initfparseflag)
  {
    fparse = new FunctionParser;
    initfparseflag = true;
  }
  else if (!cpy.initfparseflag && initfparseflag)
  {
    delete fparse;
    initfparseflag = false;
  }
  if (cpy.initfparseflag)
  {
    *fparse = *(cpy.fparse);
  }

  therhs = cpy.therhs;
  dep = cpy.dep;
  wholeeq = cpy.wholeeq;
  constflag = cpy.constflag;
  globaleqs = cpy.globaleqs;
  constants = cpy.constants;
  fatal = cpy.fatal;
  if (ind.size() != cpy.ind.size())
  {
    if (ind.size()) delete buff;
    buff = new double[cpy.ind.size()];
  }
  ind = cpy.ind;
  for (unsigned int i=0; i<ind.size(); i++)
  {
    buff[i] = cpy.buff[i];
  }
  if (cpy.konallocflag && !konallocflag)
  {
    kon = new Scalar;
    konallocflag = true;
  }
  else if (!cpy.konallocflag && konallocflag)
  {
    delete kon;
    konallocflag = false;
  }
  if (cpy.konallocflag)
  {
    *kon = *(cpy.kon);
  }
}

// mindgame: = operator overloading
Equation& Equation::operator=(const Equation &cpy)
{
  if (this != &cpy) copy(cpy);
  return *this;
}

// mindgame: copy constructor
Equation::Equation(const Equation &cpy)
{
  copy(cpy);
}

int Equation::setconstant(Scalar C)
{
  if(!konallocflag) { return 0; }
  if(!kon) 
    { 
      kon = new Scalar; 
      konallocflag = true;
    }
  *kon = C;
  return 1;
}

Scalar Equation::eval(double *args)
{
  Scalar val = (*kon);

  if (initfparseflag == false) return val;
  val += fparse->Eval(args);      
  return val;
}

Scalar Equation::eval(float *args)
{
  double ans;

  if(buff == NULL)
    {
      if(ind.size())
	{
	  buff = new double[ind.size()];
	}
    }
  for(unsigned int i=0; i < ind.size(); i++)
    {
      buff[i] = args[i];
    }
  ans = eval(buff);

  return Scalar(ans);
}

// for constant equations -- not type safe
Scalar Equation::eval(void)
{
  static double *tmp = 0;
  return eval(tmp);
}

// for equations of multiple args -- not type safe
Scalar Equation::eval(Scalar a, ...)
{
  int nc, i;
  Scalar ans;

  if (initfparseflag == false) return (*kon);
  va_list ap;
  nc = ind.size() - 1;
  if (nc > 0)
  {
    va_start(ap, a);
  }

  if(buff == NULL)
    {    
      buff = new double[ind.size()];
    }

  buff[0] = a;
  i = 0;
  while(nc--)
    {
      buff[++i] = va_arg(ap, double);     
    }
  ans = eval(buff);

  va_end(ap);

  return ans;
}

// mindgame: for equations of one arg, much faster than eval(Scalar, ...)
Scalar Equation::get_value(Scalar a)
{
  if(buff == NULL)
    {    
      buff = new double [1];
    }

  buff[0] = a;
  return (fparse->Eval(buff));
}

bool Equation::is_independent(ostring testvar)
{
  for(unsigned int i=0; i < ind.size(); i++)
    {
      if(testvar == ind[i]) { return true; }
    }
  return false;
}

ostring Equation::getparsestring(void)
{
  ostring strr = "";
  if(ind.size() == 0) { return strr; }
  
  strr = ind[0];
  for(unsigned int i=1; i < ind.size(); i++)
    {
      strr += ",";
      strr += ind[i];
    }
  
  return strr;
}


int Equation::initfparser(void)
{
  unsigned int i;
  int result;
  ostring vars;
  FunctionParser *theparser;
    
  vars = getparsestring();
  
  #ifdef DEBUG
  fprintf(stderr, "\nvars='%s'\n", vars());
  #endif

  fparse = new FunctionParser;
  
  theparser = fparse;

  // add user-functions and variables here
  // NOTE (BUG!): constants in sub-equations are ignored for now
  for(i=0; i < constants.size(); i++)
    {
      theparser->AddConstant(constants[i].name(), constants[i].c);
    }
  for(i=0; i < globaleqs.size(); i++)
    {
      theparser->AddFunction(globaleqs[i]->get_equation_name(),
			     *(globaleqs[i]->get_fparser()));
    }
  
  result = theparser->Parse(therhs, vars);
  
  if(result != -1) { on_error(theparser->ErrorMsg()); }

  // mindgame: After parsing, the default value of (*kon) should not be
  //          overrided.
  if (!initfparseflag) (*kon) = 0;

  // mindgame: moved to the right line.
  initfparseflag = true;

  // mindgame: I find that 'fatal' is disabled in parse_equation().
  //          For the case of error, 'result' needs to be checked again.
  //          Otherwise it produces segmentfault.
  if (result == -1)
  if(konallocflag && constflag)
    {
      double *dummy = NULL;
      (*kon) += theparser->Eval(dummy);
      delete fparse;
      initfparseflag = false;
    }

  return result;
}

int Equation::parseequation(ostring eqn)
{
  vector<char> ws;
  ostring rhs;
  ostring varis;
  int eqpos, lpar, rpar;
  unsigned int ncomma;
  int loc, loc2;
  unsigned int i;
  
  ws.push_back(' ');
  ws.push_back('\t');
  ws.push_back('\n');
 
  wholeeq = eqn;
 
  ostring errstring("Error, Equation `");
  errstring  +=  eqn;
  errstring += "': ";
  
  if(eqn.charcount('=') > 1)
    {
      printf("\n%smore than one =\n", errstring());
      return 0;
    }
  if(eqn.charcount('=') < 1)
    {
      printf("\n%sno = found\n", errstring());
      return 0;
    }
  eqpos = eqn.find('=');
  lpar = eqn.find('(');
  rpar = eqn.find(')');
  
  rhs = eqn.substr(eqpos+1);
  
  constflag = false;
  if((lpar < 0) || (lpar > eqpos))
    {
      int tmppos = eqn.find(',');
      if((tmppos >= 0) && (tmppos < eqpos))
	{
	  printf("\n%sparse error[1]\n", errstring());
	  on_error();
	  return 0;
	}

      constflag = true;
      if((rpar >= 0) && (rpar < eqpos))
	{
	  printf("\n%sparse error[2]\n", errstring());
	  on_error();
	  return 0;
	}
      dep = eqn.substr(0, eqpos);
      for(i=0; i < ws.size(); i++)
	{
	  dep.strip(ws[i]);
	}
      if(!dep.length())
	{
	  printf("\n%sno dependent variable name found\n", errstring());
	  return 0;
	}
    }
  else
    {
      if(rpar > eqpos)
	{
	  printf("\n%sparse error[3]\n", errstring());
	  on_error();
	  return 0;
	}
      if(rpar < eqpos-1)
	{
	  ostring ts = eqn.substr(rpar+1, eqpos-1-rpar);
	  for(i=0; i < ws.size(); i++)
	    {
	      ts.strip(ws[i]);
	    }
	  if(ts.length() > 0)
	    {
	      printf("\n%sparse error[4]\n", errstring());
	      on_error();
	      return 0;
	    }
	}
      dep = eqn.substr(0, lpar);
      
      for(i=0; i < ws.size(); i++)
	{
	  dep.strip(ws[i]);
	}
      if(!dep.length())
	{
	  printf("\n%sno dependent variable name found\n", errstring());
	  return 0;
	}
      if(rpar != lpar+1)
	{
	  varis = eqn.substr(lpar+1, rpar-lpar-1);
	}
      else
	{
	  varis = "";
	}
      
      for(i=0; i < ws.size(); i++)
	{
	  varis.strip(ws[i]);
	}

      if(varis.length() > 0)
	{
	  ncomma = varis.charcount(',');
	  loc = 0;
	  for(i=0; i < ncomma; i++)
	    {		  
	      loc2=varis.find(',', loc) + loc;
	      
	      #ifdef DEBUG
	      printf("varis=%s, loc=%d, loc2=%d, i=%d, ncomma=%d\n", 
		     varis(), loc, loc2, i, ncomma);
	      printf("lpar=%d, rpar=%d\n\n", lpar, rpar);
	      #endif
	      
	      if((loc2 < loc+1) && (i < ncomma))
		{
		  printf("\n%smissing independent variable[1]\n", errstring());
		  return 0;
		}
	      
	      ind.push_back(varis.substr(loc, loc2-loc));
	      loc = loc2+1;
	    }

	  loc2 = varis.length();
	  if(loc2 < loc+1)
	    {
	      printf("\n%smissing independent variable[2]\n", errstring());
	      printf("varis=%s, loc=%d, loc2=%d, i=%d, ncomma=%d\n", 
		     varis(), loc, loc2, i, ncomma);
	      return 0;
	    }
	  ind.push_back(varis.substr(loc, loc2));
	}
      else
	{
	  constflag=true;
	}	  
    }
  
  therhs = rhs;
  return 1;
}

void Equation::parseall(ostring theq)
{
  parseequation(theq);
  initfparser();
}


void Equation::on_error(const char *msg)
{
  fprintf(stderr, "\nError, Equation `%s':", wholeeq());
  fprintf(stderr, "\n%s\n", msg);
  if(fatal) { exit(1); }
}

void Equation::add_Constant(ostring nn, Scalar vv)
{
  constants.push_back(Constant(vv, nn));
}

void Equation::add_Equation(Equation *eeqq)
{
  globaleqs.push_back(eeqq);
}

void Equation::printdebuginfo(void) // print information for debugging
{
  fprintf(stderr, "\nEquation debugging info: \n");
  fprintf(stderr, "dep: %s\n", dep());
}

