#include <stdio.h>
#include <stdlib.h>

#include "main/pd1.hpp"
#include "xsections/crossx.hpp"
#include "utils/funccrossx.hpp"

Scalar FuncCrossX::value(Scalar energy) const
{
  return function(energy);
}
