#include <stdlib.h>
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VariableArray.hpp"

VariableArray::VariableArray()
{
	setdefaults();
}

VariableArray::VariableArray(ostring _n, int _s) : DataArray(_n, _s)
{
	setdefaults();
}

VariableArray::VariableArray(ostring _n, int _s, int * _ls) : DataArray(_n, _s)
{
	setdefaults();
	ls = _ls;
}

VariableArray::VariableArray(ostring _n, int _s, int _label) : DataArray(_n, _s)
{
	setdefaults();
	label = _label;
}

void VariableArray::setdefaults(void)
{
	locsize = 0;
	ncomb = NCOMB;
	update_interval = 1;
	interval = 0;
	realloc_mult = REALLOC_MULT;
	cp_on_realloc = true;
	maxsizefunc = &VariableArray::comb;
	ls = &locsize;
	label = 0;			// JK, 2017-10-31
}

void VariableArray::comb(void)
{
	register int cc, i;

	for(cc=0, i=0; cc < locsize; cc += ncomb, i++)
	{
		array[i] = array[cc];
	}
	locsize = i;
	update_interval *= ncomb;
}

void VariableArray::realloc(void)
{
	if(cp_on_realloc)
	{
		resize_cp(size*realloc_mult);
	}
	else
	{
		resize(size*realloc_mult);
	}
}

void VariableArray::set_realloc(bool to_copy)
{
	maxsizefunc = &VariableArray::realloc;
	cp_on_realloc = to_copy;
}

void VariableArray::update(Scalar tnew)
{
	interval++;
	if(interval%update_interval) { return; }
	interval = 0;

	if(locsize == size)
	{
		((*this).*maxsizefunc)();
	}
	array[locsize++] = tnew;
}


