#include <stdio.h>
#include <stdlib.h>

#include "main/pd1.hpp"
#include "reactions/reactiontype.hpp"
#include "xsections/xsection.hpp"
#include "utils/equation.hpp"
#include "utils/funcxsection.hpp"

Scalar FuncXsection::value(Scalar energy) const
{
  return function(energy);
}
