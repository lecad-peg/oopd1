#include <stdlib.h>
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"

DataArray::DataArray(){
	size = 0;
	label = 0;
}

DataArray::DataArray(ostring _name, int _size, Scalar* p_scalar) : Array<Scalar>(_name, _size, p_scalar)
{
	label = 0;
}

DataArray::DataArray(ostring _name, int _size, int _label) : Array<Scalar>(_name, _size)
{
	label = _label;
}

DataArray::DataArray(ostring _name, int _size) : Array<Scalar>(_name, _size)
{
	zero();
	label = 0;
}

void DataArray::zero(void)
{
	memset(array, 0, size*sizeof(Scalar));
}

Scalar DataArray::getAverageOfArray(void)
{
	Scalar average = 0.;
	for(int i=0; i<getSize(); i++){ average += array[i]; }
	average /= getSize();
	return average;
}
