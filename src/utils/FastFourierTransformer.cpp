#include "utils/FastFourierTransformer.hpp"

FastFourierTransformer::FastFourierTransformer(){
  magnitudeAllocated = false;
  phaseAllocated = false;
}



FastFourierTransformer::~FastFourierTransformer(){
  if(magnitudeAllocated) delete [] magnitude;
  if(phaseAllocated) delete [] phase;
}


scalar* FastFourierTransformer::doForwardTransform(scalar* data, int size){
  realft(data, size/2, 1);
  return data;
}

scalar* FastFourierTransformer::doReverseTransform(scalar* data, int size){
  realft(data, size/2, -1);
  for(int i=0; i<size; i++){
    data[i] = data[i] * 2/size;
  }
  return data;
}



scalar* FastFourierTransformer::returnMagnitude(scalar* data, int size){
  int i,j;
  magnitude = new scalar[size/2+1];
  magnitudeAllocated = true;
  magnitude[0] = fabs(*data);
  magnitude[size/2] = *(data+1);
  for(i=2, j=2; i<(size/2+1); i++,j+=2){
    magnitude[i-1] = sqrt(*(data+j)* *(data+j) + *(data+j+1)* *(data+j+1));
  }
  return magnitude;
}



scalar* FastFourierTransformer::returnPhase(scalar* data, int size){
  int i,j;
  phase = new scalar[size/2+1];
  phaseAllocated = true;
  phase[0] = (data[0] > 0.0) ? 0.0: 180.0;
  phase[size/2] = (data[0] > 0.0) ? 0.0: 180.0;
  for(i=2, j=2; i<(size/2+1); i++, j+=2){
    if(fabs(data[j+1]) < 1e-30 && fabs(data[j]) < 1e-30)
      phase[i-1] = 0.0;
    else
      phase[i-1] = (180.0/PI)*atan2(data[j+1], data[j]);
  }
  return phase;
}

/**********************************************************************/
/* Private Functions                                                  */
/**********************************************************************/

void FastFourierTransformer::swap(scalar* a, scalar* b){
  scalar temp;
  temp = *a;
  *a = *b;
  *b = temp;
}


void FastFourierTransformer::four1(float data[], int nn, int isign){
     int i, j, m, n, mmax, istep;
     float wtemp, wr, wpr, wpi, wi, theta;
     float tempr, tempi;

     n= nn<<1;    
     j=1;

     for(i=1; i<n; i+=2){
	 if(j>i){
	   swap(data+j, data+i);   
	   swap(data+j+1,data+i+1);
	 }
	 m = nn;  
	 while(m >= 2 && j > m){
	   j -= m; 
	   m >>= 1;   
	 }
	 j += m;
      }
      mmax = 2;
      while(n > mmax){
	istep = mmax << 1;
	 theta= isign*(TWOPI/mmax);
	 wtemp= sin(.5*theta);
	 wpr= -2.0*wtemp*wtemp;
	 wpi= sin(theta);
	 wr= 1.0;
	 wi= 0.0;
	 for(m=1; m<mmax; m+=2){
	     for(i=m; i<=n; i+=istep){
		j=i+mmax;
		tempr= wr*data[j]-wi*data[j+1];
		tempi= wr*data[j+1]+wi*data[j];
		data[j]= data[i]- tempr;
		data[j+1]= data[i+1]- tempi;
		data[i] +=tempr;
		data[i+1] +=tempi;
	     }
	     wr= (wtemp=wr)*wpr - wi*wpi+wr;
	     wi= wi*wpr + wtemp*wpi + wi;
	  }
	  mmax= istep;
      }
}

void FastFourierTransformer::realft(float data[], int n, int isign){
	int i, i1, i2, i3, i4, n2p3;
	float c1=0.5, c2, h1r, h1i, h2r, h2i;
	float wr, wi, wpr, wpi, wtemp, theta;

	theta= PI/(scalar) n;
	if(isign ==1){
	   c2= -0.5;
	   four1(data-1, n, 1);
	}
	else{
	   c2= 0.5;
	   theta= -theta;
	}
	wtemp= sin(0.5*theta);
	wpr= -2.0*wtemp*wtemp;
	wpi= sin(theta);
	wr= 1.0+wpr;
	wi= wpi;
	n2p3= 2*n+3;
	for(i=2; i<= n/2; i++){
	   i4= 1+(i3=n2p3 -(i2=1 +(i1=i+i-1)));
	   h1r= c1*(data[i1-1] +data[i3-1]);
	   h1i= c1*(data[i2-1] -data[i4-1]);
	   h2r= -c2*(data[i2-1] +data[i4-1]);
	   h2i= c2*(data[i1-1] -data[i3-1]);

	   data[i1-1]= h1r +wr*h2r -wi*h2i;
	   data[i2-1]= h1i +wr*h2i +wi*h2r;
	   data[i3-1]= h1r -wr*h2r +wi*h2i;
	   data[i4-1]= -h1i +wr*h2i +wi*h2r;

	   wr= (wtemp=wr)*wpr -wi*wpi +wr;
	   wi= wi*wpr +wtemp*wpi +wi;
	}
	if(isign ==1){
	   data[0]= (h1r= data[0]) +data[1];
	   data[1]= h1r - data[1];
	}
	else{
	   data[0]= c1*((h1r=data[0]) +data[1]);
	   data[1]= c1*(h1r -data[1]);
	   four1(data-1, n, -1);
	}
	//	for (i=1; i<=2*n; i++){
	//	   if (isign== 1) data[i-1] /= (2*n);
	//	   if (isign==-1) data[i-1] *= 2;
	//	}
}




/*int main(){
  FastFourierTransformer fft;
  scalar data2[] = {3.0f,4.0f,7.0f,-3.0f,0.0f,-8.0f,-23.0f,6.0f,
                    2.0f,3.0f,-4.0f,-4.0f,3.0f,7.0f,-10.0f,3.0f};
  scalar* datap2 = &data2[0];
  int n = 16;
  fft.doForwardTransform(datap2,n);
  for(int i=0; i<n; i++){
    cout << data2[i] << endl;}
  scalar* pha2 = fft.returnPhase(datap2,10);
  }

*/
