#include<stdlib.h>

#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/Array2d.hpp"

Array2D::Array2D(ostring &n, int nr, int nc) : DataArray(n, nr*nc)
{
  nrow = nr;
  ncol = nc;

  row = new Scalar * [ncol];

  // set the pointers
  for(int i=0; i < ncol; i++)
    {
      row[i] = array + i*nrow;
    }
  zero();
}

Array2D::~Array2D()
{
  delete [] row;
}
