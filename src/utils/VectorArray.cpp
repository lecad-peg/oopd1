#include <stdlib.h>
#include "utils/Array.hpp"
#include "utils/VectorArray.hpp"

VectorArray::VectorArray(){
  size = 0;
  vectdim = 3;
}

VectorArray::VectorArray(ostring _name, int _size, Vector3* p_scalar, int vdim)
  : Array<Vector3>(_name, _size, p_scalar)
{
  vectdim = vdim;
}

void VectorArray::resetValues(VectorArray _data){
  for (int i=0; i<_data.getSize(); i++){
    _data[i] = 0;
  }
}

void VectorArray::doFourierTransform()
{
  //  FastFourierTransformer fft;
  //= new FastFourierTransformer();
  //  array = fft.doForwardTransform(array, getSize());
}

Vector3 VectorArray::getAverageOfArray(){
  Vector3 average = 0;
  for(int i=0; i<getSize(); i++){
    average += array[i];
  }
  average /= getSize();
  return average;
}
