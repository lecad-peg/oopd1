#include "utils/RandomScalarGenerator.hpp"

RandomScalarGenerator::RandomScalarGenerator(){
  randomFlag = true;
  seed = initSeed();
}

RandomScalarGenerator::RandomScalarGenerator(bool input){
  randomFlag = input;
  seed = initSeed();
}

scalar RandomScalarGenerator::makeRandomScalar(){
  scalar randomScalar;
  randomScalar = RandomScalarGenerator::makeRandomScalar(false,false);
  return randomScalar;
}

scalar RandomScalarGenerator::makeRandomScalar(bool open0, bool open1){
  long a = 16807, q = 127773, r = 2836;
  const long MAX_LONG = 2147483647;
  long hi, lo;
  scalar randomScalar;

  hi = seed/q;
  lo = seed - q*hi;
  seed = a*lo - r*hi;

  /* "seed" will always be a legal integer of 32 bits (including sign). */
  /* long ranges from -2147483648 to 2147483647                         */
  /* add MAX_LONG + 1 to keep seed positive and between 0 to MAX_LONG   */
  if(seed < 0) seed += MAX_LONG + 1;
  randomScalar = (scalar)seed/(scalar)MAX_LONG;
  if(randomScalar == 0 && open0){
    RandomScalarGenerator::makeRandomScalar(open0, open1);
  }
  if(randomScalar == 1 && open1){
    RandomScalarGenerator::makeRandomScalar(open0, open1);
  }
  return randomScalar;
}

long RandomScalarGenerator::initSeed(){

  if(randomFlag){
    time_t timer;
    timer = time(NULL);  
    return timer;
  }
  else 
    return 31207321;  //As far as I know, any number will serve this purpose.
                      //This value might be chosen for good statistical results
                      //with this algorithm.
}
