#include <stdio.h>

#include "main/pd1.hpp"
#include "utils/trimatrix.hpp"

TriMatrix::TriMatrix(int i)
{
  // nullify pointers
  tri_a = tri_b = tri_c = gam = 0;
  fixed = 0;

  // allocate memory
  alloc(i);

  // nullify pointers for nonlinear solver
  tri_b_copy = 0;
  rhs_copy = 0;

  // use L2 norm for error checking by default
  Lnorm = 2;

  // set all elements to zero
  zero();

  // don't start in nonlinear mode
  nonlinearmode = false;
}

TriMatrix::~TriMatrix()
{
  dealloc();
}

// TriMatrix::alloc -- allocate memory for an i-row matrix
void TriMatrix::alloc(int i)
{
  n = i;
  nc = n-1;
  if(tri_a) { dealloc(); }

  tri_a = new Scalar[n];
  tri_b = new Scalar[n];
  tri_c = new Scalar[n];
  gam = new Scalar[n];

  fixed = new bool[n];
}

void TriMatrix::dealloc(void)
{
  delete [] tri_a;
  delete [] tri_b;
  delete [] tri_c;
  delete [] gam;

  delete [] fixed;

  if(tri_b_copy)
    {
      delete [] tri_b_copy;
      delete [] rhs_copy;
    }
}

// multiply: matrix-vector multiply.  multiplys *this by
// vec and the resulting vector is placed in ans
void TriMatrix::multiply(Scalar *vec, Scalar *ans)
{
  // get matrix diagonal
  Scalar *btmp = get_tri_b();

  // end conditions
  ans[0] = btmp[0]*vec[0] + tri_c[0]*vec[1];
  ans[nc] = tri_a[nc]*vec[nc-1] + btmp[nc]*vec[nc];

  for(int i=1; i < nc; i++)
    {
      ans[i] = tri_a[i]*vec[i-1] + btmp[i]*vec[i] + tri_c[i]*vec[i+1];
    }
}

// TriMatrix::solve -- solve a linear system
void TriMatrix::solve(Scalar *rhs, Scalar *ans, int nstrt)
{
  register int j;
  Scalar bet = tri_b[nstrt];

  ans[nstrt] = rhs[nstrt]/bet;

  for(j=nstrt+1; j<n; j++) 
    {
      gam[j] = tri_c[j-1]/bet;
      bet = tri_b[j] - tri_a[j]*gam[j];
      ans[j] = (rhs[j] - tri_a[j]*ans[j-1])/bet;
    }

  for(j=nc-1; j>= nstrt; j--) ans[j] -= gam[j+1]*ans[j+1];
}

TriMatrix& TriMatrix::operator = (TriMatrix * RHS)
{
  if(this == RHS)
    {
      fprintf(stderr, "THIS SHOULD NEVER HAPPEN!!!");
      getchar();
    }

  if(n != RHS->get_n())
    {
      // trying to make sure = operator works correctly, JH, Dec. 4, 2005
      #ifdef DEBUG
      fprintf(stderr, "\nTriMatrix = : reallocating\n");
      getchar();
      #endif

      dealloc();
      alloc(RHS->get_n());
    }

  
  memcpy(tri_a, RHS->get_tri_a(), n*sizeof(Scalar)); 
  memcpy(tri_b, RHS->get_tri_b(), n*sizeof(Scalar)); 
  memcpy(tri_c, RHS->get_tri_c(), n*sizeof(Scalar)); 
	   
  for(register int i=0; i < n; i++)
    {
      fixed[i] = RHS->is_fixed(i);
    }
  
  return *this;
}

void TriMatrix::operator *= (Scalar f)
{
  for(register int i=0; i < n; i++)
    {
      tri_a[i] *= f;
      tri_b[i] *= f;
      tri_c[i] *= f;
    }
}

void TriMatrix::operator += (TriMatrix & RHS)
{
  if(n != RHS.get_n())
    {
      
    }

  for(register int i=0; i < n; i++)
    {
      tri_a[i] += RHS.a(i);
      tri_b[i] += RHS.b(i);
      tri_c[i] += RHS.c(i);
    }
}

void TriMatrix::zero(void)
{
  memset(tri_a, 0, n*sizeof(Scalar));
  memset(tri_b, 0, n*sizeof(Scalar));
  memset(tri_c, 0, n*sizeof(Scalar));

  for(register int i=0; i < n; i++)
    {
      fixed[i] = false;
    }

}

void TriMatrix::print(FILE *fp)
{
  fprintf(fp, "\nMatrix: %d elements", n);
  for(int i=0; i < n; i++)
    {
      fprintf(fp, "\ni=%d; a=%g; b=%g; c=%g", 
	      i, tri_a[i], tri_b[i], tri_c[i]);
      if(fixed[i]) { fprintf(fp, ", fixed"); }
    }
  fprintf(fp, "\n");
}

void TriMatrix::print(char *fname)
{
  FILE *fp = fopen(fname, "w");
  print(fp);
  fclose(fp);
}
