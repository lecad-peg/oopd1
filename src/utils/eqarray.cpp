#include "utils/ostring.hpp"
#include "main/oopiclist.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "utils/equation.hpp"
#include "utils/eqarray.hpp"

EqArray::~EqArray()
{
  if(theq) { delete theq; }
}

void EqArray::setdefaults()
{
  theq = 0;
}

void EqArray::add_array(DataArray *thed)
{
  darrays.add(thed);
}
