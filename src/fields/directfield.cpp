//********************************************************
// directfield.cpp
//
// Revision history
//
// (JohnV 02Jun2004) Original code.
//
//********************************************************

#include <stdio.h>
#include "utils/ovector.hpp"
#include "main/ostack.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/grid.hpp"
#include "fields/directfield.hpp"
#include "main/boundary.hpp"

#include "xgrafix.h"  // THIS SHOULD BE REMOVED!!!! USE REAL DIAGNOSTICS!!!

#include "main/grid_inline.hpp"

DirectSolver::DirectSolver(FILE *fp, 
		oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables,
		SpatialRegion *sr)
: Base_Class_Direct_Field(fp, float_variables, int_variables, sr)
{
	E_shown = false;
	init_parse(fp, float_variables, int_variables);
}

DirectSolver::~DirectSolver(void){}

int DirectSolver::partition(SortElement *data, int l, int r)
{
	int i(l-1);
	int j(r);
	float v = data[r].key();
	SortElement tmp;
	while(true){
		// while (data[++i].key() < v); previous
		while (data[++i].key() < v) if (i==r) break;
		while (v < data[--j].key()) if (j==l) break;
		if (i >= j) break;
		tmp = data[i]; // swap the elements i and j
		data[i] = data[j];
		data[j] = tmp;
	}
	tmp = data[i]; // swap the elements i and r
	data[i] = data[r];
	data[r] = tmp;
	return i;
}

// provides a non-recursive quicksort of keys based on ParticleGroups

void DirectSolver::qsort(void)
{
	// calculate number of particles
	nParticles = 0;
	oopicListIter<Species> spIter(*theSpeciesList);
	for (spIter.restart(); !spIter.Done(); spIter++)
		nParticles += spIter()->number_particles();

	// create key index array structure
	if (sortKeys) delete[] sortKeys; // deallocate old array
	sortKeys = new SortElement[nParticles];
	int j=0;
	Scalar xMin, xMax;
	bool init = true;
	for (spIter.restart(); !spIter.Done(); spIter++) {
		oopicListIter<ParticleGroup> pgIter(*spIter()->get_ParticleGroupList());
		// must initialize xmin and xmax
		if (init) {
			pgIter.restart();
			xMin = pgIter()->get_x(0);
			xMax = xMin;
			init = false;
		}
		// set up sortKeys and also get xmin and xmax
		for (pgIter.restart(); !pgIter.Done(); pgIter++) {
			ParticleGroup *pg = pgIter();
			for (int i=pg->get_n(); i;) { // loop over particles
				i--; // decrement done here to reduce for loop calcs
				sortKeys[j].set(pg, i); // store index and ParticleGroup*
				float x = pg->get_x(i); // compare xmin and xmax to x
				xMin = MIN(xMin,x);
				xMax = MAX(xMax,x);
				j++;
			}
		}
	}

	// sort the data
	int l = 0;
	int r = nParticles-1;
	int i;
	oopicStack<int> s;
	s.push(l);
	s.push(r);
	while (!s.isEmpty()){
		r = s.pop();
		l = s.pop();
		if (r <= l) continue;
		i = partition(sortKeys, l, r);
		if (i-1 > r-i){
			s.push(l); s.push(i-1); s.push(i+1); s.push(r);
		} else {
			s.push(i+1); s.push(r); s.push(l); s.push(i-1);
		}
	}
}

void DirectSolver::update_fields(void)
{
	qsort(); // Sort all particles by position

	compute_E();
}

// compute_E calculates the electric field at each particle location
// Algorithm is to do this sequentially from the rightmost particle to the 
// left most.

void DirectSolver::compute_E(void)
{
	// Weight total charge in the system to the wall:
	Scalar E_r = 0;
	for (int i=nParticles; i;) { // do --i inside to optimize for loop
		E_r += sortKeys[--i].get_qx(); // could be optimized?
	}
	// compute field at right wall assuming grounded conductors

	// nc = L in grid units
	E_r /= epsilon*theGrid->getnc()*theGrid->getA(theGrid->getL());

	// compute field at each particle location and write to particles

	// scales between q and E in 1D
	Scalar scaling = 1.0/(2*epsilon*theGrid->getA(theGrid->getL()));

	Scalar q=0, qp;
	Scalar Ecur = E_r;
	for (int i=nParticles-1; i>=0; i--) {
		qp = q;
		q = sortKeys[i].get_q();
		Ecur -= (qp + q)*scaling;
		sortKeys[i].set_E(Ecur);
	}

	// compute field at left edge
	Scalar E_l = Ecur - q*scaling;

	if (is_E_shown()) { // interpolate E to mesh for diagnostics
		int ng = theGrid->getng();
		E[0].set_e1(E_l);
		for (int i=1; i<ng; i++) {
			int j=-1;
			while ((++j < nParticles) && (sortKeys[j].key() < i)); // in grid units!

			// minor diag error when a particle lies exactly on grid...
			// field at grid node is equal to the avg of fields at bounding particles
			if (j==0) E[i].set_e1(E_l); // node i is left of the 1st particle
			else if (j>=nParticles) E[i].set_e1(E_r); // node i is right of last particle
			else {
				E[i].set_e1(0.5*(sortKeys[j].get_E() + sortKeys[j-1].get_E()));
				j--;		// changed (previous line) and added current line to avoid "unsequenced modification and access to 'j'"; JK, 2017-09-22
			}
		}
	}
}

void DirectSolver::setdefaults(void) 
{ 
	Base_Class_Direct_Field::setdefaults();
	classname="DirectSolver";
}


// Functions to support parse and fields virtuals

void DirectSolver::init(void)
{
	printf("DS::init() called\n");

	Base_Class_Direct_Field::init();

	// set up plots ======================

	// Electric field:
	XGSet2DFlag("linlin", "x", "E1", "closed", 100, 400, 1.0, 1.0, 1, 1,
			0.0, 0.0, 0.0, 0.0, &E_shown);
	XGCurveVector(theGrid->getX(), (Scalar*) E, &ng, 1, 1, 0,
			sizeof(Vector3)/sizeof(Scalar),0);
}
