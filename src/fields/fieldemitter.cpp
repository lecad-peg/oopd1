#include "main/pd1.hpp"
#include "main/parse.hpp"
#include "utils/ovector.hpp"
#include "main/species.hpp"
#include "fields/fields.hpp"
#include "main/grid.hpp"
#include "main/particlegroup.hpp"
#include "main/boundary.hpp"
#include "main/boundary_inline.hpp"
#include "spatial_region/sptlrgn.hpp"
#include "emmiter/emitter.hpp"
#include "fields/fieldemitter.hpp"
#include "main/drive.hpp"
#include "distributions/dist_xv.hpp"
#include "distributions/dist_v.hpp"

#include "main/grid_inline.hpp"
#include "fields/fields_inline.hpp"

FieldEmitter::FieldEmitter(FILE *fp,
			   oopicList<Parameter<Scalar> > float_variables,
			   oopicList<Parameter<int> > int_variables, Fields *t,
			   Boundary *theb)  
  : Emitter(t, theb)
{
  init_parse(fp, float_variables, int_variables);
}  

FieldEmitter::~FieldEmitter()
{
}

void FieldEmitter::setdefaults()
{
  setdefaults_emit();
  Fi_w=2; //ev
  T=1/40.; //ev
}

void FieldEmitter::setparamgroup()
{
  setparamgroup_emit();
  pg.add_fparameter("Fi_w",&Fi_w);
}

void FieldEmitter::check()
{
  check_emit();
}

void FieldEmitter::init()
{
  init_emit();
}

void FieldEmitter::maintain()
{
  maintain_emit();
} 

int FieldEmitter::get_nparticles(Scalar tstart, Scalar tend)
{
  static Scalar saved = 0;
  Scalar sval;
  int retval;
  Scalar flux;
  Vector3 Enormal;
  theboundary->get_E((Direction)direction,Enormal);
      
  // mindgame: Fedora 3 (g++ 3.4.2) compilation problem
  Scalar E=fabs(Enormal.e1());
  if(E-Enormal.e1())
  {
    Scalar B=6.8308E+09;
    Scalar A=1.5414E-06;
    Scalar t=1.1;
    Scalar y=3.79E-5*sqrt(E)/Fi_w;
    Scalar v=0.95-y*y;
    flux=A*E*E/Fi_w/t*exp(-B*v*sqrt(Fi_w)*sqrt(Fi_w)*sqrt(Fi_w)/E);

    // flux /=fabs(q); // convert to # flux
  }
  else
  {
    flux=0;
  }
  sval = ((flux/(np2c0*fabs(q)))*(tend - tstart));

  if(sval < 0) { return 0; }
  sval += saved;
  retval = int(sval);
  saved = sval - retval;
  return retval;
}
