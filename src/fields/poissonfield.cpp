#include <stdio.h>

#include<vector>
using namespace std;

#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/grid.hpp"
#include "main/drive.hpp"
#include "fields/fields.hpp"
#include "main/circuit.hpp"
#include "utils/trimatrix.hpp"
#include "main/boundary.hpp"
// sknam: Why is conductor.hpp included?
#include "main/conductor.hpp"
#include "main/dielectric.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_time.hpp"
#include "diagnostics/diagnosticcontrol.hpp"
#include "spatial_region/sptlrgn.hpp"
#include "fields/poissonfield.hpp"

#include "main/grid_inline.hpp"
#include "fields/fields_inline.hpp"

PoissonSolver::PoissonSolver(SpatialRegion *sr) : Fields(sr)
{
	init_parse();
}

PoissonSolver::PoissonSolver(FILE *fp, 
		oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables,
		SpatialRegion *sr) : Fields(sr)
{
	residual = 0;
	init_parse(fp, float_variables, int_variables);
}

PoissonSolver::~PoissonSolver()
{
	if(A)    {      delete A;    }
	if(matrix_bak) { delete matrix_bak; }
	if(rhs_fixed) { delete [] rhs_fixed; }
	if(residual) { delete [] residual; }
}

void PoissonSolver::check_poisson(void)
{ 
}

void PoissonSolver::setdefaults_poisson(void)
{
	classname = "PoissonSolver";
	A = 0;
	matrix_bak = 0;
	rhs_fixed = 0;
	laplaceflag = false;
	nonlinearflag = false;
	nonlinear_residual = 0.0001; // default residual error for nonlinear solver
	itmax = ITMAX;
}

void PoissonSolver::setdefaults(void)
{
	setdefaults_fields();
	setdefaults_poisson();
}

void PoissonSolver::setparamgroup_poisson(void) 
{ 
	pg.add_bflag("laplacesplitting", &laplaceflag, true,
			"split the Poisson solve into the sum of Laplace solutions (does this work?)");

	pg.add_fparameter("nonlinear_residual", &nonlinear_residual,
			"tolerence for nonlinear solver");

	pg.add_iparameter("itmax", &itmax,
			"maximum number of iterations for nonlinear solver");
}

void PoissonSolver::setparamgroup(void)
{
	setparamgroup_fields();
	setparamgroup_poisson();
}

void PoissonSolver::check(void)
{
	check_fields();
	check_poisson();
}
void PoissonSolver::init(void)
{
	init_fields();
	init_poisson();
}

void PoissonSolver::init_poisson(void)
{
	static int iflag=true;
	int n = theGrid->getng();
	DiagnosticControl dc;

	if(iflag)
	{
		dc.add_gridarray("phi", phi);
		dc.add_gridarray("rho", rho);
		dc.add_gridarray("rhoAve", rho_ave); // for grid time average, MAL 6/13/09
		dc.add_gridarray("phiAve", phi_ave); // for grid time average, MAL 6/13/09

		dc.add_gridvectors("E", E, Edim());
	}

	A = new TriMatrix(n);
	matrix_bak = new TriMatrix(n);
	rhs_fixed = new bool[n];
}

// check to see if the nonlinearsolver should be used
bool PoissonSolver::is_nonlinear(void)
{
	oopicListIter<Species> thei(*theSpeciesList);
	for(thei.restart(); !thei.Done(); thei++)
	{
		if(thei()->is_nonlinear())
		{
			nonlinearflag = true;
			return nonlinearflag;
		}
	}
	nonlinearflag = false;
	return nonlinearflag;
}

void PoissonSolver::setmatrixcoef(void)
{
	register int i;

	// New method, Nov. 29, 2005:
	// Apply Gauss's law:
	// add flux of Dx to the right, subtract flux of Dx to the left
	Scalar Aepsleft, Aepsright;
	Scalar cell_volume;
	Scalar xleft, xright;

	for(i=0; i < ng; i++)
	{

		if(i == 0) // LHS system boundary
		{
			Aepsleft = 0.;
			xleft = theGrid->getX(i);
		}
		else
		{
			xleft = 0.5*(theGrid->getX(i) + theGrid->getX(i-1));
			Aepsleft = get_epsilon(i-1)*theGrid->getA(xleft);
			Aepsleft /= theGrid->get_dxcell(i-1);
		}

		if(i == ng-1) // RHS system boundary
		{
			Aepsright = 0.;
			xright = theGrid->getX(i);
		}
		else
		{
			xright = 0.5*(theGrid->getX(i) + theGrid->getX(i+1));
			Aepsright = get_epsilon(i)*theGrid->getA(xright);
			Aepsright /= theGrid->get_dxcell(i);
		}

		cell_volume = theGrid->getV(xleft, xright);
		cell_volume = 1./cell_volume; // convert to inverse cell volume

		matrix_bak->a(i) = -cell_volume*Aepsleft;
		matrix_bak->b(i) = cell_volume*(Aepsleft + Aepsright);
		matrix_bak->c(i) = -cell_volume*Aepsright;

	}

#ifdef DEBUG
	A->print("trimatrix.dat");
#endif
}

void PoissonSolver::update_fields(void)
{
	accumulate_rho();

	ApplyToList(update_N_tot(), *theSpeciesList, Species);

	modify_matrix(); // modify matrix coefficients and rhs

	if(is_nonlinear())
	{
		if(debug())
		{
			fprintf(stderr, "Nonlinear Solver\n");
			print_phi("boltz_init.txt");
		}

		A->nonlinearsolve(rho, phi, &PoissonSolver::electrostatic_density,
				&PoissonSolver::electrostatic_derivative,
				this,
				nonlinear_residual, itmax,
				&PoissonSolver::calculate_residual);
	}
	else
	{
		A->solve(rho, phi);
	}

	if(laplaceflag)
	{
		add_Laplace();
	}

	// restore
	restore_matrix_data();

	// compute electric field
	compute_E();

	// weight electric field to the particles
	oopicListIter<Species> thei(*theSpeciesList);
	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->Gather_E();
	}

	// compute true rho for nonlinear solve
	if(is_nonlinear())
	{
		for(int j=0; j < A->get_n(); j++)
		{
			for(thei.restart(); !thei.Done(); thei++)
			{
				rho[j] += thei()->electrostatic_density(phi[j]);
			}
		}
	}
	// do grid time averages of rho and phi, MAL 6/15/09
	grid_time_average(phi, phi_temp, phi_ave);
	grid_time_average(rho, rho_temp, rho_ave);

	// JH, 9/10/2004- prints out potential for debugging
#ifdef DEBUG
	print_phi("phi.dat");
#endif
}

void PoissonSolver::compute_E(void)
{
	register int i;

	reset_E();

	for(i=0; i < ng; i++)
	{
		E[i].set_e1(-(theGrid->gradient(phi, i)) + E[i].e1());
	}

	int j;
	oopicListIter<Dielectric> thei(*dielectriclist);
	for(thei.restart(); !thei.Done(); thei++)
	{
		if (thei()->is_usable(NEGATIVE))
		{
			j = thei()->get_j0();
			E[j].set_e1((phi[j-1] - phi[j])/theGrid->get_dxcell(j-1)
					+0.5*space_rho[j]*theGrid->get_dxcell(j-1)/eps[j-1]);
		}
		else if (thei()->is_usable(POSITIVE))
		{
			j = thei()->get_j1();
			E[j].set_e1((phi[j] - phi[j+1])/theGrid->get_dxcell(j)
					-0.5*space_rho[j]*theGrid->get_dxcell(j)/eps[j]);
		}
	}
}

// modifies the matrix and right hand side
void PoissonSolver::modify_matrix(void)
{

	// copy data from untouched data
	(*A) = matrix_bak;
	memcpy(rho,space_rho, theGrid->getng()*sizeof(Scalar));
	for(int i=0; i < theGrid->getng(); i++) { rhs_fixed[i] = false; }

	// loop over boundaries to modify matrix and rhs
	oopicListIter<Boundary> thei(*boundarylist);
	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->modify_coeffs(this);
	}
}

// restore_matrix_data:  restore data to the matrix and rhs(i.e., rho)
// - should be the equivalent of undoing modify_matrix
void PoissonSolver::restore_matrix_data(void)
{
	// restore rho from space_rho
	memcpy(rho,space_rho, theGrid->getng()*sizeof(Scalar));
}

// restore_rhs():  restores rho from master copy
// -> Scalar * new_rhs: array to restore to [assumed right size = ng]
// -> bool compute_boltz: should the boltzmann species be added to rho?
void PoissonSolver::restore_rhs(Scalar *new_rhs, bool compute_nonlin,
		Scalar *potential)
{
	// restore rho from space_rho
	memcpy(new_rhs,space_rho, theGrid->getng()*sizeof(Scalar));

	// if compute_nonlin is true, add in the contribution
	// of the nonlinear (e.g. fluid) parts of the species
	if(compute_nonlin)
	{
		if(!potential) { potential = phi; }
		add_nonlinear_density(new_rhs, potential);
	}
}

// adds coefficients to row j of the matrix
void PoissonSolver::add_coeff(int j, Scalar a_val, Scalar b_val, 
		Scalar c_val)
{

	// if matrix row j is fixed, do not do the add
	if(A->is_fixed(j))
	{
#ifdef DEBUG
		fprintf(stderr, "add_coeff:  A(%d) fixed\n", j);
#endif
		return;
	}

	A->a(j) += a_val;
	A->b(j) += b_val;
	A->c(j) += c_val;
}

// sets coefficients of row j of the matrix
void PoissonSolver::set_coeff(int j, Scalar a_val, Scalar b_val, 
		Scalar c_val)
{

	// if matrix row j is already fixed, err out with error message
	if(A->is_fixed(j))
	{
		ostring errmsg = "A->is_fixed(j),";
		ostring jstring = j;
		errmsg += jstring;

		error("PoissonSolver::set_coeff", errmsg());
	}

	A->a(j) = a_val;
	A->b(j) = b_val;
	A->c(j) = c_val;

	A->set_fixed(j);
}

// adds charge Q[coulombs] to row j of rhs
void PoissonSolver::add_charge(int j, Scalar Q)
{

	if(rhs_fixed[j])
	{
#ifdef DEBUG
		fprintf(stderr, "add_charge:  rhs[%d] fixed\n", j);
#endif
		return;
	}

	rho[j] += Q/theGrid->get_cell_volume(j);
}

// adds rhs_val to row j of rhs
void PoissonSolver::add_rhs(int j, Scalar rhs_val)
{
	if(rhs_fixed[j])
	{
#ifdef DEBUG
		fprintf(stderr, "add_rhs:  rhs[%d] fixed\n", j);
#endif
		return;
	}

	rho[j] += rhs_val;

}

// sets rhs[j] to rhs_val
void PoissonSolver::set_rhs(int j, Scalar rhs_val)
{
	if(rhs_fixed[j])
	{
		error("PoissonSolver::set_coeff", "rhs_fixed[j]");
	}

	rho[j] = rhs_val;
	rhs_fixed[j] = true;

}


Scalar PoissonSolver::electrostatic_density(Scalar potential)
{
	Scalar retval = 0.;
	oopicListIter<Species> thes(*theSpeciesList);
	for(thes.restart(); !thes.Done(); thes++)
	{
		retval += thes()->electrostatic_density(potential);
	}
	return retval;
}

Scalar PoissonSolver::electrostatic_derivative(Scalar potential)
{
	Scalar retval = 0.;
	oopicListIter<Species> thes(*theSpeciesList);
	for(thes.restart(); !thes.Done(); thes++)
	{
		retval += thes()->electrostatic_derivative(potential);
	}
	return retval;
}

// adds the contribution of nonlinear densities based on 
// a given potential to new_rhs.  Assumes arrays sizes are ng
void PoissonSolver::add_nonlinear_density(Scalar *new_rhs, Scalar *potential)
{
	for(int i=0; i < theGrid->getng(); i++)
	{
		new_rhs[i] += electrostatic_density(potential[i]);
	}
}

void PoissonSolver::update_species_fields(Scalar * _phi)
{
	ApplyToList(update_fluids_from_fields(_phi), *theSpeciesList, Species);
}


Scalar PoissonSolver::calculate_residual(Scalar *phi_check, 
		Scalar *rho_check)
{
	Scalar res = 0.;

	// allocate memory for residual
	if(!residual) { residual = new Scalar[theGrid->getng()]; }

	// residual = calculated RHS
	A->multiply(phi_check, residual);

	// print out residual for debugging, JH, October 11, 2005
#ifdef DEBUG
	printarray("phi_check.txt", theGrid->getng(), phi_check);
	printarray("residual.txt", theGrid->getng(), residual);
#endif

	restore_rhs(rho_check, true, phi_check);

	// subtract calculated rho
	for(int i=0; i < theGrid->getng(); i++)
	{
		residual[i] -= rho_check[i];
		if(rhs_fixed[i]) { residual[i] = 0.; }
	}

	Scalar numer, denom;
	numer = theGrid->integrate(residual, 0, -1, 2, true);
	denom = theGrid->integrate(rho_check, 0, -1, 2, true);

	// Add surface charge to denominator
	oopicListIter<Boundary> thei(*boundarylist);
	for(thei.restart(); !thei.Done(); thei++)
	{
		denom += ppow(thei()->get_sigma(phi_check, rho_check, true), 2);
	}

	// calculate residual (2-norm)
	res = sqrt(numer/denom);

	// update the parameters for nonlinear solvers
	update_species_fields(phi_check);

	if(pg.debug)
	{
		fprintf(stderr, "numer=%g,denom=%g, res=%g\n", numer, denom, res);
		printarrays("boltz_check.txt", theGrid->getng(), 4,
				rho, rho_check, phi_check, residual);
	}

	return res;
}


void PoissonSolver::print_phi(FILE *fp)
{
	for(int i=0; i < ng; i++)
	{
		fprintf(fp, "%g %g %g\n", theGrid->getX(i), phi[i], rho[i]);
	}
}

void PoissonSolver::print_phi(const char *name)
{
	FILE *fp;
	fp = fopen(name, "w");
	print_phi(fp);
	fclose(fp);
}

void PoissonSolver::print_matrix(FILE *fp)
{
	A->print(fp);
	fprintf(fp, "\nrho: (ng=%d)", theGrid->getng());
	for(int i=0; i < theGrid->getng(); i++)
	{
		fprintf(fp, "\n%d %g", i, rho[i]);
	}
	fprintf(fp, "\n");
}

/////////////
// Functions for Laplace splitting
/////////////

void PoissonSolver::add_Laplace(void)
{
}
