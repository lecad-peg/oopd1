#include <stdio.h>

#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/grid.hpp"
#include "fields/fields.hpp"
#include "spatial_region/sptlrgn.hpp"
#include "main/boundary.hpp"
#include "main/conductor.hpp"
#include "main/dielectric.hpp"
#include "reactions/reaction.hpp" // all includes below added for icp, MAL 1/11/11
#include "main/particlegroup.hpp"
#include "MCC/mcc.hpp"
#include "MCC/hydrocarbons_mcc.hpp"
#include "MCC/oxygen_mcc.hpp"
#include "MCC/chlorine_mcc.hpp"//20130604,Huang

// #include <complex> // for icp field calculation, MAL 1/7/11; DO COMPLEX ARITHMETIC EXPLICITLY, MAL 5/10/16
// using namespace std; // for icp field calculation, MAL 1/7/11

Fields::Fields(SpatialRegion *sr) : Parse(sr)
{
	theregion = sr;
	theGrid = sr->get_grid();
	theSpeciesList = sr->get_specieslist();
	boundarylist = sr->get_boundarylist();
	ExEzSwitch = false;		// by default no Ex field is given in input file

	// allocate memory
	ng = theGrid->getng();
	rho = new Scalar[ng];
	space_rho = new Scalar[ng];
	space_rho_neg = new Scalar[ng]; // for sheathwidth diagnostic, MAL 6/23/09
	phi = new Scalar[ng];

	// initialize the following variables, for grid time average, MAL 6/15/09
	naverage = 0;
	nsteps = -1; // number of timesteps, incremented in accumulate_rho, and first call is for initialization
	nstepsmod = 0;
	avecoef = 0.;
	rho_temp = new Scalar[ng];
	rho_ave = new Scalar[ng];
	phi_temp = new Scalar[ng];
	phi_ave = new Scalar[ng];
	fields_phi_array = 0; // HH 04/19/16
	fields_E_array = 0; // HH 05/13/16

	// for grid time average diagnostics control MAL 6/26/09
	flux1 = flux2 = flux3 = J1dotE1 = J2dotE2 = J3dotE3 =  enden1 = enden2 = enden3 = false;
	vel1 = vel2 = vel3 = T1 = T2 = T3 = Tperp = Temp = JdotE = enden = false;
	f1flag = f2flag = f3flag = e1flag = e2flag = e3flag = false;
	d0flag = true; d1flag = false; d3flag = false; // used by particlegrouparray, MAL 9/22/09
	ngdu = 1;
	nodiagupdate = true;
	// end variables for grid time average, MAL 6/15/09

	// initialize variables for sheathwidth diagnostic, MAL 6/25/09
	Lparam = 0.;
	Rparam = 0.;
	k0 = 0;
	scoef[0] = scoef[1] = 0.;
	sheathwidth[0] = sheathwidth[1] = 0.;
	previoussheathj[0] = previoussheathj[1] = 0;
	currentsheathj[0] = currentsheathj[1] = 0;

	// initialize variables for icp field solve, MAL 1/7/11
	icp2 = icp3 = false;
	K2 = K3 = f2 = f3 = phi2 = phi3 = 0.0;
	nupdateE = 5000;
	wavespeed = SPEED_OF_LIGHT;
	Er2 = new Scalar[ng];
	Ei2 = new Scalar[ng];
	Er3 = new Scalar[ng];
	Ei3 = new Scalar[ng];


	E = new Vector3[ng];
	B = new Vector3[ng];
	field_continuity = new bool[ng];
	for (int i=0; i<ng; i++)
	{
		phi[i] = 0.; // initialize phi's and rho's to zero, MAL 9/9/12
		phi_temp[i]=0.;  // for time-averages, MAL 6/13/09
		phi_ave[i] = 0.; // for time-averages, MAL 6/13/09
		rho_temp[i] = 0.; // for time-averages, MAL 6/13/09
		rho_ave[i] = 0.; // for time-averages, MAL 6/13/09
		field_continuity[i] = true;
	}

	get_Bfields();
	gridded = 1; // default to a gridded field solve
	// gridless solves only compute E on grid for diagnostic purposes
	E_shown = 0; // default to E not shown
}

Fields::Fields(Grid *_theGrid, SpeciesList *_theSpeciesList, 
		SpatialRegion *sr)  : Parse(sr)
{
	theregion = sr;
	theGrid = _theGrid;
	theSpeciesList = _theSpeciesList;
	boundarylist = sr->get_boundarylist();
	ExEzSwitch = false;		// by default no Ex field is given in input file

	// allocate memory
	ng = theGrid->getng();
	rho = new Scalar[ng];
	space_rho = new Scalar[ng];
	space_rho_neg = new Scalar[ng]; // for sheath width diagnostic, MAL 6/23/09
	phi = new Scalar[ng];

	// initialize the following variables, for grid time average, MAL 6/15/09
	naverage = 0;
	nsteps = -1; // incremented in accumulate_rho, and first call is for initialization
	nstepsmod = 0;
	avecoef = 0.;
	rho_temp = new Scalar[ng];
	rho_ave = new Scalar[ng];
	phi_temp = new Scalar[ng];
	phi_ave = new Scalar[ng];
	fields_phi_array = 0; // HH 04/19/16
	fields_E_array = 0; // HH 05/13/16
	// for diagnostics control MAL 6/26/09
	flux1 = flux2 = flux3 = J1dotE1 = J2dotE2 = J3dotE3 =  enden1 = enden2 = enden3 = false;
	vel1 = vel2 = vel3 = T1 = T2 = T3 = Tperp = Temp = JdotE = enden = false;
	f1flag = f2flag = f3flag = e1flag = e2flag = e3flag = false;
	d0flag = true; d1flag = false; d3flag = false; // used by particlegrouparray, MAL 9/22/09
	ngdu = 1;
	nodiagupdate = true;
	// end variables for grid time average, MAL 6/15/09

	// initialize variables for sheath width diagnostic, MAL 6/17/09
	Lparam = 0.;
	Rparam = 0.;
	k0 = 0;
	scoef[0] = scoef[1] = 0.;
	sheathwidth[0] = sheathwidth[1] = 0.;
	previoussheathj[0] = previoussheathj[1] = 0;
	currentsheathj[0] = currentsheathj[1] = 0;

	// initialize variables for icp field solve, MAL 1/7/11
	icp2 = icp3 = false;
	K2 = K3 = f2 = f3 = phi2 = phi3 = 0.0;
	nupdateE = 5000;
	wavespeed = SPEED_OF_LIGHT;
	Er2 = new Scalar[ng];
	Ei2 = new Scalar[ng];
	Er3 = new Scalar[ng];
	Ei3 = new Scalar[ng];

	B = new Vector3[ng];
	get_Bfields();
	E = new Vector3[ng];
	field_continuity = new bool[ng];
	for (int i=0; i<ng; i++)
	{
		phi[i] = 0.;
		phi_temp[i]=0.;  // for time-averages, MAL 6/13/09
		phi_ave[i] = 0.; // for time-averages, MAL 6/13/09
		rho_temp[i] = 0.; // for time-averages, MAL 6/13/09
		rho_ave[i] = 0.; // for time-averages, MAL 6/13/09
		field_continuity[i] = true;
	}
}

// Allocate and initialize Spatiotemporal arrays, HH 04/19/16
// Called only once in Spatialregion::update, so that everything else is already initialized.
// If we put this function in init, we get an error because size_array is not yet determined.
void Fields::init_arrays(int size_array)
{

	if (fields_phi_array) { delete [] fields_phi_array;}
	fields_phi_array = new Scalar[size_array];
	for (int m=0; m<size_array; m++)
	{
		fields_phi_array[m] = 0;
	}
        
	if (fields_E_array) { delete [] fields_E_array;}
	fields_E_array = new Scalar[size_array];
	for (int m=0; m<size_array; m++)
	{
		fields_E_array[m] = 0;
	}
}

void Fields::set_arrays(int ng, int steps_rf, int m) // set Spatiotemporal arrays, HH 04/19/16
{

    for(int n=0; n<ng; n++)
    {
    	fields_phi_array[n*steps_rf+m] += *(get_phi()+n);
    }
    
    for(int n=0; n<ng; n++)
    {
    	fields_E_array[n*steps_rf+m] += E[n].e(1);
    }
}

Fields::~Fields(void) {

	delete[] rho;
	delete[] rho_temp; // for time-averages, MAL 6/13/09
	delete[] rho_ave; // for time-averages, MAL 6/13/09
	delete[] space_rho;
	delete[] space_rho_neg; // for sheath width diagnostic, MAL 6/23/09
	delete[] phi;
	delete[] phi_temp; // for time-averages, MAL 6/13/09
	delete[] phi_ave; // for time-averages, MAL 6/13/09
	delete[] E;
	delete[] B;
	delete [] Er2; // for icp, MAL 1/9/11
	delete [] Ei2;
	delete [] Er3;
	delete [] Ei3;
	if(eps_alloc) { delete [] eps; }
	delete [] field_continuity;
	delete [] fields_phi_array; // for spatiotemporal diagnostics, HH 04/19/16
	delete [] fields_E_array; // for spatiotemporal diagnostics, HH 05/13/16
}

// accessor functions
Scalar Fields::get_dt(void) { return theregion->get_dt(); }
Scalar Fields::get_t(void) { return theregion->get_t(); }
void Fields::getE(int i, Vector3 & Eloc){ Eloc = E[i]; }

void Fields::setconductors(oopicList<Conductor> *c)
{
	conductorlist = c;
	oopicListIter<Conductor> thei(*conductorlist);
	for(thei.restart(); !thei.Done(); thei++)
	{
		if (thei()->is_usable(POSITIVE))
			field_continuity[thei()->get_j1()] = false;
		if (thei()->is_usable(NEGATIVE))
			field_continuity[thei()->get_j0()] = false;
#ifdef DEBUG
		if (thei()->is_usable(POSITIVE))
			print_shr_msg("\n%s: %d",(char *)thei()->get_name(),thei()->get_j1());
		if (thei()->is_usable(NEGATIVE))
			print_shr_msg("\n%s: %d",(char *)thei()->get_name(),thei()->get_j0());
#endif
	}
}


void Fields::setdielectrics(oopicList<Dielectric> *d)
{
	dielectriclist = d;
	if (!(d->isEmpty())) modify_epsilon();
	oopicListIter<Dielectric> thei(*dielectriclist);
	for(thei.restart(); !thei.Done(); thei++)
	{
		if (thei()->is_usable(POSITIVE))
			field_continuity[thei()->get_j1()] = false;
		if (thei()->is_usable(NEGATIVE))
			field_continuity[thei()->get_j0()] = false;
#ifdef DEBUG
		if (thei()->is_usable(POSITIVE))
			print_shr_msg("\n%s: %d",(char *)thei()->get_name(),thei()->get_j1());
		if (thei()->is_usable(NEGATIVE))
			print_shr_msg("\n%s: %d",(char *)thei()->get_name(),thei()->get_j0());
#endif
	}
}

void Fields::modify_epsilon(void)
{
	oopicListIter<Dielectric> thei(*dielectriclist);

	if(!(dielectriclist->isEmpty()))
	{
		allocate_epsilon();
	}

	for (int j=0; j<ng-1; j++)
	{
		for(thei.restart(); !thei.Done(); thei++)
		{
			if (j < thei()->get_j1() && j >= thei()->get_j0())
			{
				if (eps[j] != thei()->get_epsilon())
					eps[j] = thei()->get_epsilon();

			}
		}
	}
}

// allocate permitivity array and fill with background
// epsilon value
void Fields::allocate_epsilon(void)
{
	if(eps_alloc)
	{
		delete [] eps;
	}
	eps = new Scalar[ng-1]; // epsilon allocated on a cellular basis

	for (int i=0; i<ng-1; i++) { eps[i] = epsilon; }

	eps_alloc = true;
	const_epsilon = false;
}

void Fields::accumulate_rho(void) 
{
	register int i;
	Scalar scale;
	Scalar *dens;
	oopicListIter<Species> thei(*theSpeciesList);
	for(i=0; i < ng; i++)
	{
		// add background density
		space_rho[i] = rhoback.eval(theGrid->getX(i), get_t());
	}

	// accumulate PIC charge from species list
	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->accumulate_density();
		scale = thei()->get_q();
		dens = thei()->get_species_density();
		for(i=0; i < ng; i++)
		{
			space_rho[i] += scale*dens[i];
		}
	}
	memcpy(rho, space_rho, ng*sizeof(Scalar));

	// sheathwidth diagnostic, MAL 6/18/09
	sheathwidth[0] = find_sheath_width(0);
	sheathwidth[1] = find_sheath_width(1);
	//	if(nsteps % 1000==0) printf("\nFields::get_nsteps=%li",get_nsteps());
	nsteps++;
	nodiagupdate = !(nsteps % ngdu == 0); // true if diagnostics should NOT be updated, MAL 9/22/09
}

// find sheathwidths for sheath diagnostics; n=0 for sheath at grid 0; n=1 for sheath at grid ng-1, MAL 6/21/09
// no interpolation between neighboring grid points to determine the sheath width
Scalar Fields::find_sheath_width(int n)
// revised by MAL to use the same algorithm as in xpdp1, 5/14/11
{
	if ((n == 0 && scoef[0] == 0.) || (n == 1 && scoef[1] == 0.)) { return 0.; } // do nothing
	int i, j;
	Scalar scale; // species charge
	Scalar rhoneg, srpi, srpim1;
	Scalar *dens;
	oopicListIter<Species> thei(*theSpeciesList);
	// start at j=0 and search for condition -rhoneg > (Lsheath or Rsheath)*rhopos, ave over two grid points
	for (j=0; j < ng/2; j++)
	{ // begin accumulate space_rho_neg at grid i
		i = (n==0? j : ng-1-j); // increment from grid 0 if n=0; decrement from grid ng-1 if n=1
		// add background density if it is negative
		rhoneg = rhoback.eval(theGrid->getX(i), get_t());
		if(rhoneg > 0.) { space_rho_neg[i] = 0.; }
		else {space_rho_neg[i] = rhoneg;}

		// accumulate negative PIC charge from species list
		for(thei.restart(); !thei.Done(); thei++)
		{
			scale = thei()->get_q();
			if(scale < 0.)
			{
				dens = thei()->get_species_density(); //assumes density already updated
				space_rho_neg[i] += scale*dens[i];
			}
		} // end accumulate space_rho_neg at grid i

		if (j==0) continue; // need at least two cells; go to next grid
		srpi = space_rho[i] - space_rho_neg[i];
		srpim1 = space_rho[i-1] - space_rho_neg[i-1];
		if (srpi > 0. && srpim1 > 0. && -space_rho_neg[i]/srpi - space_rho_neg[i-1]/srpim1 > 2.*scoef[n])
			// sheath position has been found, same condition as in xpdp1
		{
			if (n==0) return 0.5*theGrid->getX(i-1) + 0.5*theGrid->getX(i) - theGrid->getx0();
			else return theGrid->getx1() - 0.5*theGrid->getX(i+1) - 0.5*theGrid->getX(i);
		}
	} // end of for loop on grid numbers
	return 0.; // sheath not found at any grid, return sheathwidth=0
}

// updates grid time averages of n, fluxes, energy densities, and J dot E's each timestep, called by species, MAL 6/14/09
// add subcycle for calls for fluxes, energy densities, and J dot E's, MAL 9/10/12
void Fields::grid_time_average(Scalar *garray, Scalar *tempgarray, Scalar *avegarray, int subcycle)
{
	register int i;
	int nsmod, nstepsdu;
	if(get_naverage() < 2) return; // do nothing, as grid time averages not enabled
	nsmod = nsteps % naverage;
	if (nsmod % ngdu !=0) return; // do nothing, since averages only displayed in xgraphics every ngdu timesteps
	if (nsmod != nstepsmod) // update time-varying parameters
	{
		nstepsmod = nsmod;
		nstepsdu = nstepsmod/ngdu;
		avecoef = 1.0/(nstepsdu+1);
	}
	// Do work here
	if(nsteps != nstepsmod && nstepsmod != 0) // 2nd and subsequent time-averaging periods, accum into temp array only
	{
		for(i=0; i<ng; i++)
		{	
			tempgarray[i] += (subcycle*garray[i] - tempgarray[i])*avecoef;
		}
		return;
	}
	if(nsteps == nstepsmod) // 1st initial time-averaging period, accum into both displayed ave and temp arrays
	{
		for(i=0; i<ng; i++)
		{	
			tempgarray[i] += (subcycle*garray[i] - tempgarray[i])*avecoef;
			avegarray[i] += (subcycle*garray[i] - avegarray[i])*avecoef;
		}
		return;
	}
	if(nsteps != nstepsmod && nstepsmod == 0) // xfer temp array to displayed ave array, and begin again accum into temp array
	{
		for(i=0; i<ng; i++)
		{	
			avegarray[i] = tempgarray[i];
			tempgarray[i] = subcycle*garray[i];
		}
		return;
	}
}

// updates grid time averages of velocities each timestep, called by species, MAL 9/11/10
void Fields::grid_time_ave(Scalar * vave, Scalar * nave, Scalar * fave)
{
	register int i;
	if(get_naverage() < 2) return; // do nothing, as grid time averages not enabled
	if(nsteps == nstepsmod || nstepsmod == 0) // 1st initial time-averaging period, and subsequent periods
	{
		for(i=0; i<ng; i++)
		{	
			if (nave[i] > 0) {
				vave[i] = fave[i]/nave[i];
			}
			else {
				vave[i]=0;
			}
		}
	}
}

// updates grid time averages of temperatures T1, T2, and T3 each timestep, called by species, MAL 9/11/10
void Fields::grid_time_ave(Scalar m, Scalar np2c, Scalar * Tave, Scalar * nave, Scalar * fave, Scalar * eave)
{
	register int i;
	if(get_naverage() < 2) return; // do nothing, as grid time averages not enabled
	if(nsteps == nstepsmod || nstepsmod == 0) // 1st initial time-averaging period, and subsequent periods
	{
		for(i=0; i<ng; i++)
		{	
			if (nave[i]*get_theGrid()->getV(i) >= 2.0*np2c) {
				Tave[i] = (2.0*eave[i] - m*fave[i]*fave[i]/nave[i])/(PROTON_CHARGE*nave[i]);
				if (Tave[i] < 0.) { Tave[i] = 0.; } // kill tiny negative T's (due to roundoff errors), MAL 1/11/11
			}
			else {
				Tave[i]=0;
			}
		}
	}
}

// updates grid time averages of temperature Tperp each timestep, called by species, MAL 9/11/10
void Fields::grid_time_ave(Scalar m, Scalar np2c, Scalar * Tperpave, Scalar * nave, Scalar * fave2, Scalar * eave2, Scalar * fave3, Scalar * eave3)
{
	register int i;
	if(get_naverage() < 2) return; // do nothing, as grid time averages not enabled
	if(nsteps == nstepsmod || nstepsmod == 0) // 1st initial time-averaging period, and subsequent periods
	{
		for(i=0; i<ng; i++)
		{	
			if (nave[i]*get_theGrid()->getV(i) >= 2.0*np2c) {
				Tperpave[i] = (eave2[i]+eave3[i] - 0.5*m*((fave2[i]*fave2[i]+fave3[i]*fave3[i]))/nave[i])/(PROTON_CHARGE*nave[i]);
				if (Tperpave[i] < 0.) { Tperpave[i] = 0.; } // kill tiny negative T's (due to roundoff errors), MAL 1/11/11
			}
			else {
				Tperpave[i]=0;
			}
		}
	}
}

 // updates grid time averages of temperature Temp each timestep, called by species, MAL 9/11/10
void Fields::grid_time_ave(Scalar m, Scalar np2c, Scalar * Tempave, Scalar * nave, Scalar * fave1, Scalar * eave1, Scalar * fave2, Scalar * eave2, Scalar * fave3, Scalar * eave3)
{
  register int i;
  if(get_naverage() < 2) return; // do nothing, as grid time averages not enabled
	if(nsteps == nstepsmod || nstepsmod == 0) // 1st initial time-averaging period, and subsequent periods
	{
		for(i=0; i<ng; i++)
		{	
			if (nave[i]*get_theGrid()->getV(i) >= 2.0*np2c) {
				Tempave[i] = (2.0*(eave1[i]+eave2[i]+eave3[i]) - m*((fave1[i]*fave1[i]+fave2[i]*fave2[i]+fave3[i]*fave3[i]))/nave[i])/(3*PROTON_CHARGE*nave[i]);
				if (Tempave[i] < 0.) { Tempave[i] = 0.; } // kill tiny negative T's (due to roundoff errors), MAL 1/11/11
			}
			else {
				Tempave[i]=0;
			}
		}
	}
}

// planar icp model of Emi Kawamura and M.A. Lieberman (2010), MAL 1/7/11
// K0 = surface current density [A/m], L = system HALF-LENGTH [m], x=0 in center of system
// complex magnetic field Hy = K0*cos(k*x)/cos(k*L)
// complex electric field Ez = j*K0*MU0*v*sin(k*x)/(sqrtKp*cos(k*L))
// complex wavenumber k = w*sqrt(Kp)/v, w = radian frequency, v = wave speed
// default is v = SPEED_OF_LIGHT, but v can be used to rescale to lower electron densities
// complex relative plasma dielectric constant Kp = 1 - wp^2/(w^2-j*num*w)
// square of radian plasma frequency wp^2 = e^2*nave/(EPS0*m), nave = average electron density
// num = electron-neutral collision frequency [Hz]
// physical electric field Ez(t) = Re(Ez)*cos(w*t+phi) - Im(Ez)*sin(w*t+phi) set in Fields::reset_E()

void Fields::update_icp_fields(Scalar * _Er, Scalar * _Ei, Scalar & _K, Scalar & _f)
{
	// find ne and wp2 from all species with name "E-"
	oopicListIter<Species> thei(*theSpeciesList);
	Scalar wp2 = 0.0;
	Scalar ne = 0.0;
	for(thei.restart(); !thei.Done(); thei++)
	{
		if (thei()->get_reactionname() == "E-")
		{
			ne += thei()->get_Ntot_particles()/theGrid->getGridVolume();
			wp2 += ((thei()->get_q())*(thei()->get_q())*thei()->get_Ntot_particles())/(EPS0*thei()->get_m()*theGrid->getGridVolume());
		}
	}
	// find num from average number of electron collisions in one timestep
	oopicList<ReactionGroup> * thergl;
	thergl = theregion->get_reactiongrouplist();
	oopicListIter<ReactionGroup> thej(*thergl);
	ostring reactant1;
	ostring reactant2;
	Species * eSpecies;
	//	long int ncollisions; // for debug icp, MAL 1/8/11
	//	Scalar aveNcollps = 0.0; // for debug icp, MAL 1/8/11
	Scalar aveNcoll_per_step = 0.0;
	Scalar num = 0.0;
	for (thej.restart(); !thej.Done(); thej++)
	{
		reactant1 = thej()->get_reactant1()->get_reactionname();
		reactant2 = thej()->get_reactant2()->get_reactionname();
//		ncollisions = thej()->get_mcc()->get_totNcoll(); // for debug
//		aveNcollps = thej()->get_mcc()->get_aveNcoll_per_step(); // for debug
//		printf("\nreactant1=%s, reactant2=%s, ncollisions=%li, aveNcoll=%g\n", reactant1.c_str(), reactant2.c_str(),
//			ncollisions, aveNcollps); // for debug
		if (reactant1 == "E-" || reactant2 == "E-")
		{
			if (reactant1 == "E-") { eSpecies = thej()->get_reactant1(); }
			else { eSpecies = thej()->get_reactant2(); }
			aveNcoll_per_step = thej()->get_mcc()->get_aveNcoll_per_step();
			num += (aveNcoll_per_step*eSpecies->get_np2c())/(get_dt() * theGrid->getGridVolume() * ne);
		}
	}
	//	printf("\nne=%g, aveNcoll_per_step=%g, num=%g", ne, aveNcoll_per_step, num); // for debug icp, MAL 1/8/11
	// calculate real part _Er and imaginary part _Ei of complex field amplitude EE
// IN WHAT FOLLOWS, DO THE COMPLEX ARITHMETIC EXPLICITLY, MAL 5/10/16
	Scalar w = TWOPI*_f;
	//	complex<Scalar> j(0.0, 1.0);
	//	complex<Scalar> denom(w*w, -w*num);
	//	complex<Scalar> Kp = 1.0 - wp2/denom;	// real(Kp) can be positive or negative; imag(Kp) is always < 0;
	Scalar KpR = 1 - wp2/(w*w+num*num);
	Scalar KpI = -wp2*num/(w*(w*w+num*num));// the sqrt function always returns the root with real(sqrt(Kp)) > 0; from the above, imag(sqrt(Kp)) < 0
	// this corresponds to a forward-traveling wave that is damped: real(k) > 0 and imag(k) < 0
	//	complex<Scalar> sqrtKp = sqrt(Kp);
    Scalar sqrtKpR = sqrt(0.5*(sqrt(KpR*KpR+KpI*KpI)+KpR));
    Scalar sqrtKpI = -sqrt(0.5*(sqrt(KpR*KpR+KpI*KpI)-KpR));
    //	complex<Scalar> k = w*sqrtKp/wavespeed; // use wavespeed, not speed of light, MAL 1/21/11
    Scalar kR = w*sqrtKpR/wavespeed;
    Scalar kI = w*sqrtKpI/wavespeed;
    //	complex<Scalar> Ecoef = j*_K*MU0*wavespeed/(sqrtKp*cos(0.5*k*theGrid->getL()));
    Scalar tR = 0.5*kR*theGrid->getL();
    Scalar tI = 0.5*kI*theGrid->getL();
    Scalar costR = cos(tR)*0.5*(exp(tI)+exp(-tI));
    Scalar costI = -sin(tR)*0.5*(exp(tI)-exp(-tI));
    Scalar denomR = sqrtKpR*costR - sqrtKpI*costI;
    Scalar denomI = sqrtKpR*costI + sqrtKpI*costR;
    Scalar EcoefR = denomI*_K*MU0*wavespeed/(denomR*denomR+denomI*denomI);
    Scalar EcoefI = denomR*_K*MU0*wavespeed/(denomR*denomR+denomI*denomI);
    /*
     Scalar ReKp = real(Kp); // for debug icp, MAL 1/8/11
     Scalar ImKp = imag(Kp);
     Scalar ResqrtKp = real(sqrtKp);
     Scalar ImsqrtKp = imag(sqrtKp);
     Scalar kr = real(k);
     Scalar ki = imag(k);
     Scalar ReEcoef = real(Ecoef);
     Scalar ImEcoef = imag(Ecoef);
     printf("\nReKp=%g, ImKp=%g, ResqrtKp=%g, ImsqrtKp=%g", ReKp, ImKp, ResqrtKp, ImsqrtKp);
     printf("\nkr=%g, ki=%g, ReEcoef=%g, ImEcoef=%g", kr, ki, ReEcoef, ImEcoef);
     */
	Scalar x, sintR, sintI;
    //	complex<Scalar> EE(0.0,0.0);
	for (int i=0; i<ng; i++)
	{
		x = theGrid->getX(i)-0.5*(theGrid->getx0()+theGrid->getx1()); // center x properly
	tR = 0.5*kR*x;
        tI = 0.5*kI*x;
        sintR = sin(tR)*0.5*(exp(tI)+exp(-tI));
        sintI = cos(tR)*0.5*(exp(tI)-exp(-tI));
        //		EE = Ecoef*sin(0.5*k*x);
        //		_Er[i] = real(EE);
        //		_Ei[i] = imag(EE);
        _Er[i] = EcoefR*sintR - EcoefI*sintI;
        _Ei[i] = EcoefR*sintI + EcoefI*sintR;
//		printf("\n  x=%g, Er=%g, Ei=%g", x, _Er[i], _Ei[i]); // for debug icp, MAL 1/9/11
	}
//	printf(", Er[0]=%g, Ei[0]=%g\n", _Er[0], _Ei[0]); // for debug icp, 1/9/11
}


// modified to incorporate icp heating, MAL 1/9/11
void Fields::reset_E(void)
{
	Scalar x, t;
	//	printf("\nFields::reset_E, nsteps=%li\n", nsteps); // for debug icp, MAL 1/8/11

	if(ExEzSwitch)        // DQW 07/23/2018 Added ExEySwitch to control Ex
	{
		for(int i=0; i < ng; i++)
		{
			x = theGrid->getX(i);
			t = get_t();
			// E[i].set_e1(Ex_amp*sin(TWOPI*f_x*t+PI*phi_z0/180.));
			E[i].set_e1(efield1.eval(x, t));
		}
	}
	else
	{
		for(int i=0; i < ng; i++)
		{
			E[i].set_e1(0.);
		}
	}

	if(transverseE)
	{
		if (icp2) // add for icp, MAL 1/9/11
		{
			if (nsteps % nupdateE == 0) { update_icp_fields(Er2, Ei2, K2, f2); }
			for(int i=0; i < ng; i++)
			{
				x = theGrid->getX(i);
				t = get_t();
				E[i].set_e2(Er2[i]*cos(TWOPI*f2*t+PI*phi2/180.) - Ei2[i]*sin(TWOPI*f2*t+PI*phi2/180.));
			}
		}
		else
		{
			for(int i=0; i < ng; i++)
			{
				x = theGrid->getX(i);
				t = get_t();
				E[i].set_e2(efield2.eval(x, t));
			}
		}
		if (icp3) // add for icp, MAL 1/9/11
		{
			if (nsteps % nupdateE == 0) { update_icp_fields(Er3, Ei3, K3, f3); }
			for(int i=0; i < ng; i++)
			{
				x = theGrid->getX(i);
				t = get_t();
				E[i].set_e3(Er3[i]*cos(TWOPI*f3*t+PI*phi3/180.) - Ei3[i]*sin(TWOPI*f3*t+PI*phi3/180.));
			}
		}
		else
		{
			for(int i=0; i < ng; i++)
			{
				x = theGrid->getX(i);
				t = get_t();
				E[i].set_e3(efield3.eval(x, t));
			}
		}
	}
}

// front ends for virtual functions used in Parse
// must be called from inherited classes
void Fields::setdefaults_fields(void)
{
	magnetized = transverseE = false;

	constB = true;
	epsilon = EPSILON0;
	epsilonr = 1.;
	const_epsilon = true;
	eps_alloc = false;
	ref_pot = 0.;
}

void Fields::setparamgroup_fields(void)
{
	pg.add_fparameter("epsilonr", &epsilonr);
	pg.add_function("rhoback", " [C/m^3] background charge density", &rhoback, 2, "x", "t");

	// mindgame: For other geometries? Is there better name for this?
	pg.add_function("efield_y", "[V/m] 2nd component of electric field", &efield2, 2, "x", "t");
	pg.add_function("efield_z", "[V/m] 3rd component of electric field", &efield3, 2, "x", "t");
	//added DQW above 08/01/2018
	pg.add_function("efield_x", "[V/m] 1rd component of electric field", &efield1, 2, "x", "t");

	// add following input parameters for icp heating, MAL 1/8/11
	pg.add_fparameter("surfacecurrent_y", &K2, "icp surface current density K_y [A/m]");
	pg.add_fparameter("surfacecurrent_z", &K3, "icp surface current density K_z [A/m]");
	pg.add_fparameter("frequency_y", &f2, "icp frequency of surface current K_y [Hz]");
	pg.add_fparameter("frequency_z", &f3, "icp frequency of surface current K_z [Hz]");
	pg.add_fparameter("phase_y", &phi2, "icp initial phase of surface current K_y [degrees]");
	pg.add_fparameter("phase_z", &phi3, "icp initial phase of surface current K_z [degrees]");
	pg.add_fparameter("wavespeed", &wavespeed, "icp nominal speed of light [m/s]");
	pg.add_iparameter("nupdateE", &nupdateE, "number of steps for update of transverse electric field");
}


void Fields::check_fields(void) // modified for icp heating calculation, MAL 1/7/11
{
	if (K2 != 0.0 && efield2.is_fparsed())
	{
		error("Can not specify both surfacecurrent_y and efield_y");
	}
	if (K2 < 0.0 || (K2 > 0.0 && f2 <= 0.0))
	{
		error("Must specify frequency_y > 0 for surfacecurrent_y > 0");
	}
	if (K3 != 0.0 && efield3.is_fparsed())
	{
		error("Can not specify both surfacecurrent_z and efield_z");
	}
	if (K3 < 0.0 || (K3 > 0.0 && f3 <= 0.0))
	{
		error("Must specify frequency_y > 0 for surfacecurrent_y > 0");
	}
	if ((K2 > 0.0 || K3 > 0.0) && nupdateE <= 0)
	{
		error("nupdateE must be > 0 for surfacecurrent_y > 0 or surfacecurrent_z > zero");
	}
	if ((K2 > 0.0 || K3 > 0.0) && wavespeed <= 0.)
	{
		error("wavespeed must be > 0 for surfacecurrent_y > 0 or surfacecurrent_z > zero");
	}
}

void Fields::init_fields(void) // modified for icp heating, MAL 1/8/11
{
	epsilon *= epsilonr;

	if (efield2.is_fparsed() || efield3.is_fparsed() || K2 > 0.0 || K3 > 0.0) // add K2 and K3 for icp, MAL 1/7/11
		transverseE = true;

	if (efield1.is_fparsed()) {
		// handle Ex field
		ExEzSwitch = true;
	}

	e_dim = 1;
	if(transverseE) { e_dim = 3; }

	if (K2 > 0.0) { icp2 = true; }
	if (K3 > 0.0) { icp3 = true; }
	//	printf("\nFields::init_fields: nsteps=%li, nupdateE=%i, K2=%g, f2=%g, phi2=%g, K3=%g, f3=%g, phi3=%g\n",
	//		nsteps, nupdateE, K2, f2, phi2, K3, f3, phi3); // for icp debug, MAL 1/7/11
}

void Fields::print_E(FILE *fp)
{
	int i;

	for(i=0; i < ng; i++)
	{
		fprintf(fp, "%g", theGrid->getX(i));
		for(int j=1; j <= e_dim; j++)
		{
			fprintf(fp, " %g", E[i].e(j));
		}
		fprintf(fp, "\n");
	}
}

// WHY IS THIS HERE? JH, 9/10/2004
void Fields::get_Bfields(void)
{
	int ng= theGrid->getng();
	Vector3 B0;
	B0.set_e1(0);
	B0.set_e2(0);
	// mindgame: why do we have nonzero magnetic field?
	// B0.set_e3(0.0337);
	B0.set_e3(0);
	for( int i=0;i< ng; i++)
	{
		B[i]=B0;
	}
}

