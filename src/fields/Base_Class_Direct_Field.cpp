//-------------------------------------------------------
//Title:   Direct Solve Base Class
//purpos:  c++ everthing fundimental to DS and TC here!
//         1) Basic Physics
//		   2) list of all elemetns (FOR DS AND TC)
//By:      Andrew J. Christlieb
//Date:    5/18/04
//Contact: Andrew J. Christlieb, Ph. D.
//         Assistant Professor
//         Department of MathematicsField
//         University of Michigan
//         2470 East Hall
//         Ann Arbor, MI 48109.
//         
//         Office: 4851 East Hall
//         E-mail: christli@umich.edu
//         Tel:    (734) 763-5725
//------------------------------------------------------
#include <stdio.h>
#include "utils/ovector.hpp"
#include "main/ostack.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/grid.hpp"
#include "fields/Base_Class_Direct_Field.hpp"

//Sssumes that the SR ahs been constucted already!
Base_Class_Direct_Field::Base_Class_Direct_Field(
                FILE *fp, oopicList<Parameter<Scalar> > float_variables,
		oopicList<Parameter<int> > int_variables, SpatialRegion *sr) 
  : Fields(sr)
{
  gridded = false;
  sortKeys = NULL;
}

Base_Class_Direct_Field::~Base_Class_Direct_Field(void)
{
  delete[] sortKeys;
}

