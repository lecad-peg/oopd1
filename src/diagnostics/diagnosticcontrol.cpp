#include "main/pd1.hpp"
#include "utils/ostring.hpp"
#include "utils/ovector.hpp"
#include "main/oopiclist.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "utils/VariableArray.hpp"
#include "main/TimeArray.hpp"
#include "main/parse.hpp"
#include "main/grid.hpp"
// add for thefield pointer access, MAL 6/17/09
#include "fields/fields.hpp"
// adding reaction diagnostics; JK 2019-11-25
#include "reactions/reaction.hpp"
//#include "MCC/mcc.hpp"

#include "main/species.hpp"
#include "main/boundary.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_time.hpp"
#include "diagnostics/diagnostic_phase.hpp"
#include "diagnostics/diagnostic_parser.hpp"
#include "diagnostics/diagnostic_eedf.hpp"
#include "diagnostics/diagnostic_reaction.hpp"
#include "diagnostics/diagnosticcontrol.hpp"

// initialize static variables
DiagList DiagnosticControl::thediagnostics(true);
oopicList<DataArray> DiagnosticControl::grid_arrays(true);
oopicList<VectorArray> DiagnosticControl::grid_vectors(true);
// mindgame: TimeDiagnostic should not be destructive.
oopicList<TimeDiagnostic> DiagnosticControl::time_diagnostics;
oopicList<std::pair<ostring, Scalar * (Species::*)()const> > DiagnosticControl::sp_grid_arrays(true);

DiagnosticControl::DiagnosticControl()
{
	gridarray = 0;
	theparser = 0;
	srname = "";
	sptr = 0; // pointer to species grid diagnostic arrays created here, MAL 10/10/09
}

DiagnosticControl::~DiagnosticControl()
{
	if(gridarray) { delete gridarray; }
	if(sptr) {delete [] sptr; } // deallocate species grid arrays, MAL 10/10/09
}

void DiagnosticControl::initialize(TIMETYPE *ttime, Grid *tg, Fields *tf, SpeciesList *spl, BoundaryList *tbl,
		oopicList<ReactionGroup> *rgl)
{
	oopicListIter<Diagnostic> thed(thediagnostics);
	oopicListIter<Species>    thes(*spl);

	bool pflag = false;
	if(!theparser) { theparser = new DiagnosticParser(true); pflag = true; }

	timepointer = ttime;
	thesplist = spl;

	ostring coordsystem;
	thegrid = tg;
	thefield = tf;

	coordsystem = thegrid->geometry_string();

	gridarray = new DataArray(coordsystem,  thegrid->getng());
	for(int i=0; i < thegrid->getng(); i++)
	{
		gridarray->setValue(thegrid->getX(i), i);
	}

	// set the correct number of grids for the gridded arrays and vectors
	oopicListIter<DataArray> thei(grid_arrays);
	oopicListIter<VectorArray> thej(grid_vectors);
	for(thei.restart(); !thei.Done(); thei++)
	{
		if(thei()->getSize() == 0)
		{
			thei()->setSize(tg->getng());
		}
	}
	for(thej.restart(); !thej.Done(); thej++)
	{
		if(thej()->getSize() == 0)
		{
			thej()->setSize(tg->getng());
		}
	}

	// plot the gridded arrays by default, as these take no CPU time
	for(thei.restart(); !thei.Done(); thei++)
	{
		if(strcmp(thei()->getName().c_str(),"phiAve") != 0 && strcmp(thei()->getName().c_str(),"rhoAve") != 0)
			add_griddiagnostic(thei()); // if not "phiAve" or "rhoAve", plot the arrays, MAL 6/14/09
		else
		{
			if(theparser->get_naverage() > 1) // plot "phiAve" and "rhoAve" if naverage > 1, MAL 6/14/09
				add_griddiagnostic(thei());
		}
	}

	// plot gridded vector arrays
	for(thej.restart(); !thej.Done(); thej++)
	{
		add_griddiagnostic(thej());
	}

	// add species diagnostics
	bool sumopt = false;
	int nsp = thesplist->nItems();
	ostring diagname;
	if(nsp >= 1)
	{
		if(nsp >= 2) { sumopt = true; }

		// plot the total number of computer particles (of all weights)
		diagname = "Number";
		add_timediagnostic(&Species::number_particles, diagname, sumopt);

		// plot the total number of physical particles, MAL 8/4/10
		diagname = "PhysicalNumber";
		add_timediagnostic(&Species::gett_Ntot_particles, diagname, sumopt);

		// plot the time-varying temperature, averaged over all physical particles, MAL 8/4/10
		if (!theparser->is_alloffenabled()) // MAL 9/11/10
		{
			if (theparser->is_T1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled()) // and below, MAL 9/11/10
			{
				diagname = "T1";
				add_timediagnostic(&Species::gett_T1tot, diagname, false); //change sumopt to false, MAL, 11/3/10
			}

			// plot the time-varying temperature, averaged over all physical particles, MAL 8/4/10
			if (theparser->is_T2enabled() || theparser->is_allonenabled())
			{
				diagname = "T2";
				add_timediagnostic(&Species::gett_T2tot, diagname, false); //change sumopt to false, T1->T2, MAL, 11/3/10
			}

			// plot the time-varying temperature, averaged over all physical particles, MAL 8/4/10
			if (theparser->is_T3enabled() || theparser->is_allonenabled())
			{
				diagname = "T3";
				add_timediagnostic(&Species::gett_T3tot, diagname, false); //change sumopt to false, T1->T3, MAL, 11/3/10
			}

			// plot the time-varying temperature, averaged over all physical particles, MAL 8/4/10
			if (theparser->is_Tperpenabled() || theparser->is_allonenabled())
			{
				diagname = "Tperp";
				add_timediagnostic(&Species::gett_Tperptot, diagname, false); //change sumopt to false, T1->Tperp MAL, 11/3/10
			}

			// plot the time-varying temperature, averaged over all physical particles, MAL 8/4/10
			if (theparser->is_Tempenabled() || theparser->is_allonenabled())
			{
				diagname = "Temp";
				add_timediagnostic(&Species::gett_Temptot, diagname, false); //change sumopt to false, T1->Temp MAL, 11/3/10
			}
		}

		// add bulk velocity diagnostic
		if(theparser->is_bulkvenabled() && !theparser->is_alloffenabled()) // added MAL 6/26/09
		{
			diagname = "Bulk_velocity";
			XGoption option;
			option.set_yscale(thes()->get_dxdt());
			add_timediagnostic(&Species::average_velocity, diagname, option);
		}

		// add sheathwidth diagnostic, MAL 6/25/09
		init_sheathwidth_diag();
		if (theparser->get_Lparam() > 0. && theparser->get_Rparam() > 0. && !theparser->is_alloffenabled())
		{
			add_timediagnostic(tf->get_sheathwidths(), 2, ostring("Sheathwidths"), false);
		}
		else if (theparser->get_Lparam() > 0. && theparser->get_Rparam() == 0. && !theparser->is_alloffenabled())
		{
			add_timediagnostic(tf->get_Lsheathwidth(), 1, ostring("Lsheathwidth"), false);
		}
		else if (theparser->get_Lparam() == 0. && theparser->get_Rparam() > 0. && !theparser->is_alloffenabled())
		{
			add_timediagnostic(tf->get_Rsheathwidth(), 1, ostring("Rsheathwidth"), false);
		}

		// Add integral of time-average J1dotE1 dV diagnostic
		if (!theparser->is_alloffenabled() && theparser->get_naverage() > 1) // and below, MAL, 9/13/10
		{
			if (theparser->is_J1dotE1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled())
			{
				diagname = "J1dotE1PwrAve";
				add_timediagnostic(&Species::get_species_ave_J1dotE1dV, diagname, sumopt);
			}

			if (theparser->is_J2dotE2enabled() || theparser->is_allonenabled())
			{
				diagname = "J2dotE2PwrAve";
				add_timediagnostic(&Species::get_species_ave_J2dotE2dV, diagname, sumopt);
			}

			if (theparser->is_J3dotE3enabled() || theparser->is_allonenabled())
			{
				diagname = "J3dotE3PwrAve";
				add_timediagnostic(&Species::get_species_ave_J3dotE3dV, diagname, sumopt);
			}

			if (theparser->is_JdotEenabled() || theparser->is_allonenabled())
			{
				diagname = "JdotEPwrAve";
				add_timediagnostic(&Species::get_species_ave_JdotEdV, diagname, sumopt);
			}
		}


		// add species grid diagnostics, MAL 9/30/09
		init_sp_grid_diag();

		// plot the instantaneous species gridded arrays, MAL 9/30/09
		add_sp_grid_array("n", &Species::get_species_density);
		if (!theparser->is_alloffenabled()) // and below, MAL, 9/13/10
		{
			if (theparser->is_flux1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled())
				add_sp_grid_array("Flux1", &Species::get_species_flux1);
			if (theparser->is_J1dotE1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled())
				add_sp_grid_array("J1dotE1", &Species::get_species_J1dotE1);
			if (theparser->is_enden1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled())
				add_sp_grid_array("Enden1", &Species::get_species_enden1);
			if (theparser->is_vel1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled())
				add_sp_grid_array("Vel1", &Species::get_species_vel1);
			if (theparser->is_T1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled())
				add_sp_grid_array("T1", &Species::get_species_T1);
			if (theparser->is_flux2enabled() || theparser->is_allonenabled())
				add_sp_grid_array("Flux2", &Species::get_species_flux2);
			if (theparser->is_J2dotE2enabled() || theparser->is_allonenabled())
				add_sp_grid_array("J2dotE2", &Species::get_species_J2dotE2);
			if (theparser->is_enden2enabled() || theparser->is_allonenabled())
				add_sp_grid_array("Enden2", &Species::get_species_enden2);
			if (theparser->is_vel2enabled() || theparser->is_allonenabled())
				add_sp_grid_array("Vel2", &Species::get_species_vel2);
			if (theparser->is_T2enabled() || theparser->is_allonenabled())
				add_sp_grid_array("T2", &Species::get_species_T2);
			if (theparser->is_flux3enabled() || theparser->is_allonenabled())
				add_sp_grid_array("Flux3", &Species::get_species_flux3);
			if (theparser->is_J3dotE3enabled() || theparser->is_allonenabled())
				add_sp_grid_array("J3dotE3", &Species::get_species_J3dotE3);
			if (theparser->is_enden3enabled() || theparser->is_allonenabled())
				add_sp_grid_array("Enden3", &Species::get_species_enden3);
			if (theparser->is_vel3enabled() || theparser->is_allonenabled())
				add_sp_grid_array("Vel3", &Species::get_species_vel3);
			if (theparser->is_T3enabled() || theparser->is_allonenabled())
				add_sp_grid_array("T3", &Species::get_species_T3);
			if (theparser->is_JdotEenabled() || theparser->is_allonenabled())
				add_sp_grid_array("JdotE", &Species::get_species_JdotE);
			if (theparser->is_endenenabled() || theparser->is_allonenabled())
				add_sp_grid_array("Enden", &Species::get_species_enden);
			if (theparser->is_Tperpenabled() || theparser->is_allonenabled())
				add_sp_grid_array("Tperp", &Species::get_species_Tperp);
			if (theparser->is_Tempenabled() || theparser->is_allonenabled())
				add_sp_grid_array("Temp", &Species::get_species_Temp);
		}

		// add the time-averaged species grid diagnostics if naverage > 1, MAL 9/30/09
		if(theparser->get_naverage() > 1)
		{
			add_sp_grid_array("nAve", &Species::get_species_ave_density);
			if (!theparser->is_alloffenabled()) // and below, MAL, 9/13/10
			{
				if (theparser->is_flux1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled())
					add_sp_grid_array("Flux1Ave", &Species::get_species_ave_flux1);
				if (theparser->is_J1dotE1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled())
					add_sp_grid_array("J1dotE1Ave", &Species::get_species_ave_J1dotE1);
				if (theparser->is_enden1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled())
					add_sp_grid_array("Enden1Ave", &Species::get_species_ave_enden1);
				if (theparser->is_vel1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled())
					add_sp_grid_array("Vel1Ave", &Species::get_species_ave_vel1);
				if (theparser->is_T1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled())
					add_sp_grid_array("T1Ave", &Species::get_species_ave_T1);
				if (theparser->is_flux2enabled() || theparser->is_allonenabled())
					add_sp_grid_array("Flux2Ave", &Species::get_species_ave_flux2);
				if (theparser->is_J2dotE2enabled() || theparser->is_allonenabled())
					add_sp_grid_array("J2dotE2Ave", &Species::get_species_ave_J2dotE2);
				if (theparser->is_enden2enabled() || theparser->is_allonenabled())
					add_sp_grid_array("Enden2Ave", &Species::get_species_ave_enden2);
				if (theparser->is_vel2enabled() || theparser->is_allonenabled())
					add_sp_grid_array("Vel2Ave", &Species::get_species_ave_vel2);
				if (theparser->is_T2enabled() || theparser->is_allonenabled())
					add_sp_grid_array("T2Ave", &Species::get_species_ave_T2);
				if (theparser->is_flux3enabled() || theparser->is_allonenabled())
					add_sp_grid_array("Flux3Ave", &Species::get_species_ave_flux3);
				if (theparser->is_J3dotE3enabled() || theparser->is_allonenabled())
					add_sp_grid_array("J3dotE3Ave", &Species::get_species_ave_J3dotE3);
				if (theparser->is_enden3enabled() || theparser->is_allonenabled())
					add_sp_grid_array("Enden3Ave", &Species::get_species_ave_enden3);
				if (theparser->is_vel3enabled() || theparser->is_allonenabled())
					add_sp_grid_array("Vel3Ave", &Species::get_species_ave_vel3);
				if (theparser->is_T3enabled() || theparser->is_allonenabled())
					add_sp_grid_array("T3Ave", &Species::get_species_ave_T3);
				if (theparser->is_JdotEenabled() || theparser->is_allonenabled())
					add_sp_grid_array("JdotEAve", &Species::get_species_ave_JdotE);
				if (theparser->is_endenenabled() || theparser->is_allonenabled())
					add_sp_grid_array("EndenAve", &Species::get_species_ave_enden);
				if (theparser->is_Tperpenabled() || theparser->is_allonenabled())
					add_sp_grid_array("TperpAve", &Species::get_species_ave_Tperp);
				if (theparser->is_Tempenabled() || theparser->is_allonenabled())
					add_sp_grid_array("TempAve", &Species::get_species_ave_Temp);
			}
		}

		oopicListIter<std::pair<ostring, Scalar * (Species::*)()const> > thespga(sp_grid_arrays);
		Diagnostic2d *ttmmpp;
		Scalar * (Species::*spga)() const;
		for(thespga.restart(); !thespga.Done(); thespga++)
		{
			ttmmpp = new Diagnostic2d(gridarray, thespga()->first);
			spga = thespga()->second;
			//
			// To put species grid diagnostics in same color order as phase space, number(t), etc, MAL 10/10/09
			//    Species * sgdarray[nsp]; //Although worked previously, the new c++ compiler does not permit declaring variable-sized arrays, MAL 4/28/16
			Species ** sgdarray = new Species *[ nsp ]; // So allocate a variable-sized array in c++ on the heap, MAL 4/28/16
			int igd = 0;
			for(thes.restart(); !thes.Done(); thes++)
			{
				sgdarray[nsp-1-igd] = thes(); igd++;
			}
			for(igd = 0; igd < nsp; igd++)
			{
				// adding species name as parameter to adding Y array; JK 2020-01-13
				ttmmpp->add_y_array( ((*sgdarray[igd]).*spga)(), (*sgdarray[igd]).get_name() );
			} // end color order correction, MAL 10/10/09
			delete [] sgdarray; // delete variable-sized array from heap, MAL 4/28/16
			sgdarray=0; // set pointer to NULL, MAL 4/28/16
			//
			// add the Diagnostic2d to the lists
			grid_diagnostics.add(ttmmpp);
			thediagnostics.add(ttmmpp);
		}

		for(thes.restart(); !thes.Done(); thes++)
		{
			// add phase space diagnostics
			if(theparser->is_phasediagnostic_enabled(thes()))
			{
				add_phasediagnostic(thes(), tbl);
			}
		}

		if( theparser->is_eedf_enabled() ) {
			// parser has EEDF diagnostics defined; create necessary EEDF diagnostic classes
			DiagnosticEEDF *tmpdD, *tmpdP;
			oopicList<DiagnosticParser> *eedf_dps = theparser->get_eedf_diag_parsers();
			oopicListIter<DiagnosticParser> thedps(*eedf_dps);

			for(thedps.restart(); !thedps.Done(); thedps++) {
				// loop over all EEDF diagnostic settings
				printf(" parsers: %s\n", thedps()->get_name().c_str());
				// define both EEDF and EEPF diagnostics
				tmpdD = new DiagnosticEEDF(thedps(), thegrid, false);
				add_diagnostic(tmpdD);
				tmpdP = new DiagnosticEEDF(thedps(), thegrid, true);
				add_diagnostic(tmpdP);

				for(thes.restart(); !thes.Done(); thes++) {
					// add all species with "electrons" in their name to be added to EEDF
					ostring tmpSpeciesName = thes()->get_name();
					printf("Species: %s\n", tmpSpeciesName.c_str());

					if( tmpSpeciesName.contains(ostring("electrons")) ) {
						// current species names contain 'electrons'; add species to both diagnostics
						tmpdD->addSpecies(thes());
						tmpdP->addSpecies(thes());
					}
				}
			}
		}
	}

	oopicListIter<Boundary> theb(*tbl);
	for(theb.restart(); !theb.Done(); theb++)
	{
		theb()->initialize_diagnostics(this);
	}


	//////////////////////////////////////
	// reaction diagnostics
	// JK, DQW, 2019-11-26
	//////////////////////////////////////
	if ( theparser->collect_reaction_diag() ) {
		// include reaction diagnostics
		oopicListIter<ReactionGroup> thergl(*rgl);

		for(thergl.restart(); !thergl.Done(); thergl++)
		{
			ReactionList *rl = thergl()->get_reaction_list();
			DiagnosticReaction *tmprd;

			for (int i=0; i < rl->num() ; i++) {
				// iterate over all reactions in current reaction group
				tmprd = new DiagnosticReaction(gridarray, thergl(), i);
				grid_diagnostics.add(tmprd);
				thediagnostics.add(tmprd);
			}
		}
	}

	//////////////////////////////////////
	// initialize diagnostics
	//////////////////////////////////////

	// initialize grid diagnostics
	oopicListIter<Diagnostic2d> thegd(grid_diagnostics);
	for(thegd.restart(); !thegd.Done(); thegd++)
	{
		thegd()->set_vecstring(thegrid->geometry_string(0), thegrid->geometry_string(1), thegrid->geometry_string(2));
	}

	// initialize time diagnostics
	oopicListIter<TimeDiagnostic> thetd(time_diagnostics);
	for(thetd.restart(); !thetd.Done(); thetd++)
	{
		thetd()->set_timepointer(timepointer);
	}

	// initialize the diagnostics
	for(thed.restart(); !thed.Done(); thed++)
	{
		// perform DiagnosticControl-level initialization
		thed()->set_srname(srname);
		thed()->set_timepointer(timepointer);

		// perform DiagnosticParser-level initialization
		theparser->init_Diagnostic(thed());

		// perform Diagnostic-level initialization
		thed()->initialize();

		// display the initialization string for the Diagnostic
		display_string(thed());
	}

	// if theparser is initialized in this function, clean up the memory
	if(pflag) { delete theparser; }
}

void DiagnosticControl::init_sp_grid_diag() // MAL 9/30/09
{
	// initialize species grid diagnostics
	int n1 = 0; // number of active species grid diagnostics flux1,2,3 and enden1,2,3 (used to allocate arrays needed)
	int n2 = 0; // number of active species grid diagnostics other than flux1,2,3 and enden1,2,3 (used to allocate arrays needed)
	thefield -> set_naverage(theparser->get_naverage());
	thefield -> set_ngdu(theparser->get_ngdu());
	// set flags used by species.cpp if diagnostics are enabled
	bool setflux1 = theparser->is_flux1enabled() || theparser->is_vel1enabled() || theparser->is_T1enabled() || theparser->is_Tempenabled() ||
					theparser->is_allon1Denabled() || theparser->is_allonenabled(); //  and below, MAL 9/13/10
	bool setflux2 = theparser->is_flux2enabled() || theparser->is_vel2enabled() || theparser->is_T2enabled() || theparser->is_Tperpenabled() ||
					theparser->is_Tempenabled() || theparser->is_allonenabled();
	bool setflux3 = theparser->is_flux3enabled() || theparser->is_vel3enabled() || theparser->is_T3enabled() || theparser->is_Tperpenabled() ||
					theparser->is_Tempenabled() || theparser->is_allonenabled();
	bool setenden1 = theparser->is_enden1enabled() || theparser->is_T1enabled() || theparser->is_Tempenabled() ||
					 theparser->is_allon1Denabled() || theparser->is_allonenabled(); //  and below, MAL 9/13/10
	bool setenden2 = theparser->is_enden2enabled() || theparser->is_T2enabled() || theparser->is_Tperpenabled() || theparser->is_Tempenabled() ||
					 theparser->is_allonenabled();
	bool setenden3 = theparser->is_enden3enabled() || theparser->is_T3enabled() || theparser->is_Tperpenabled() || theparser->is_Tempenabled() ||
					 theparser->is_allonenabled();
	bool setJ1dotE1 = theparser->is_J1dotE1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled();
	bool setJ2dotE2 = theparser->is_J2dotE2enabled() || theparser->is_allonenabled();
	bool setJ3dotE3 = theparser->is_J3dotE3enabled() || theparser->is_allonenabled();
	bool setvel1 = theparser->is_vel1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled();
	bool setvel2 = theparser->is_vel2enabled() || theparser->is_allonenabled();
	bool setvel3 = theparser->is_vel3enabled() || theparser->is_allonenabled();
	bool setJdotE = theparser->is_JdotEenabled() || theparser->is_allonenabled();
	bool setenden = theparser->is_endenenabled() || theparser->is_allonenabled();
	bool setT1 = theparser->is_T1enabled() || theparser->is_allon1Denabled() || theparser->is_allonenabled();
	bool setT2 = theparser->is_T2enabled() || theparser->is_allonenabled();
	bool setT3 = theparser->is_T3enabled() || theparser->is_allonenabled();
	bool setTperp = theparser->is_Tperpenabled() || theparser->is_allonenabled();
	bool setTemp = theparser->is_Tempenabled() || theparser->is_allonenabled();
	if (!theparser->is_alloffenabled())
	{
		if (setflux1) { thefield->set_flux1(); thefield->set_f1flag(); n1++; }
		if (setflux2) { thefield->set_flux2(); thefield->set_f2flag(); n1++; }
		if (setflux3) { thefield->set_flux3(); thefield->set_f3flag(); n1++; } // set_flux2->set_flux3, MAL 7/3/10
		if (setenden1) { thefield->set_enden1(); thefield->set_e1flag(); n1++; }
		if (setenden2) { thefield->set_enden2(); thefield->set_e2flag(); n1++; }
		if (setenden3) { thefield->set_enden3(); thefield->set_e3flag(); n1++; }
		if (setJ1dotE1) { thefield->set_J1dotE1(); thefield->set_f1flag(); n2++; }
		if (setJ2dotE2) { thefield->set_J2dotE2(); thefield->set_f2flag(); n2++; }
		if (setJ3dotE3) { thefield->set_J3dotE3(); thefield->set_f3flag(); n2++; }
		if (setvel1) { thefield->set_vel1(); thefield->set_f1flag(); n2++; }
		if (setvel2) { thefield->set_vel2(); thefield->set_f2flag(); n2++; }
		if (setvel3) { thefield->set_vel3(); thefield->set_f3flag(); n2++; }
		if (setJdotE) { thefield->set_JdotE(); thefield->set_f1flag(); thefield->set_f2flag(); thefield->set_f3flag(); n2++; }
		if (setenden) { thefield->set_enden(); thefield->set_e1flag(); thefield->set_e2flag(); thefield->set_e3flag(); n2++; }
		if (setT1) { thefield->set_T1(); thefield->set_f1flag(); thefield->set_e1flag(); n2++; }
		if (setT2) { thefield->set_T2(); thefield->set_f2flag(); thefield->set_e2flag(); n2++; }
		if (setT3) { thefield->set_T3(); thefield->set_f3flag(); thefield->set_e3flag(); n2++; }
		if (setTperp) { thefield->set_Tperp(); thefield->set_f2flag(); thefield->set_f3flag(); thefield->set_e2flag();
		thefield->set_e3flag(); n2++; }
		if (setTemp) { thefield->set_Temp(); thefield->set_f1flag(); thefield->set_f2flag(); thefield->set_f3flag();
		thefield->set_e1flag(); thefield->set_e2flag(); thefield->set_e3flag(); n2++; }
		// set flags used by particlegrouparray, one and only one flag is true
		thefield->set_d3flag(thefield->get_f2flag() || thefield->get_f3flag() || thefield->get_e2flag() || thefield->get_e3flag()); // accum density and 3D vel moments in pga
		thefield->set_d1flag((thefield->get_f1flag() || thefield->get_e1flag()) && !thefield->get_d3flag()); // accumulate density and 1D velocity moments in pga
		thefield->set_d0flag(!(thefield->get_d1flag() || thefield->get_d3flag())); // only accumulate density in pga
	}
	if (thefield->get_d0flag()) return; // additional array allocation beyond what is in species.cpp is not needed
	// find number n of species grid diagnostic arrays needed and allocate one contiguous array,
	// including new species_density, species_scratch_density, species_ave_density, and species_temp_density
	int n =6+n2; //1D, no time averages
	if (thefield->get_d3flag()) n += 8; // add number of arrays needed for 3D flux2,3 and enden2,3, no time averages
	if (theparser->get_naverage() > 1) n += 2+2*n1+2*n2; // add number of arrays needed for the time-averages
	int ng = thegrid->getng();

	oopicListIter<Species>    thes(*thesplist);
	int nsp = thesplist->nItems(); // number of species, MAL 10/10/09
	int nng = n*ng; // number of arrays needed for each species, MAL 10/10/09
	sptr = new Scalar[nsp*nng]; // allocate nsp contiguous arrays of size n*ng for each species, MAL 10/10/09
	int isp = 0;
	for(thes.restart(); !thes.Done(); thes++)
	{
		int j = 0;
		thes()->set_species_density(&sptr[isp*nng+j*ng]); j++; thes()->set_species_scratch_density(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_density(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_density(&sptr[isp*nng+j*ng]); j++;
		}

		if (thefield->get_d1flag() || thefield->get_d3flag())
		{
			thes()->set_species_flux1(&sptr[isp*nng+j*ng]); j++; thes()->set_species_scratch_flux1(&sptr[isp*nng+j*ng]); j++;
			thes()->set_species_enden1(&sptr[isp*nng+j*ng]); j++; thes()->set_species_scratch_enden1(&sptr[isp*nng+j*ng]); j++;
			if (theparser->get_naverage() > 1)
			{
				if (setflux1) { thes()->set_species_temp_flux1(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_flux1(&sptr[isp*nng+j*ng]); j++; }
				if (setenden1) { thes()->set_species_temp_enden1(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_enden1(&sptr[isp*nng+j*ng]); j++; }
			}
		}

		if (thefield->get_d3flag())
		{
			thes()->set_species_flux2(&sptr[isp*nng+j*ng]); j++; thes()->set_species_scratch_flux2(&sptr[isp*nng+j*ng]); j++;
			thes()->set_species_enden2(&sptr[isp*nng+j*ng]); j++; thes()->set_species_scratch_enden2(&sptr[isp*nng+j*ng]); j++;
			thes()->set_species_flux3(&sptr[isp*nng+j*ng]); j++; thes()->set_species_scratch_flux3(&sptr[isp*nng+j*ng]); j++;
			thes()->set_species_enden3(&sptr[isp*nng+j*ng]); j++; thes()->set_species_scratch_enden3(&sptr[isp*nng+j*ng]); j++;
			if (theparser->get_naverage() > 1)
			{
				if (setflux2) { thes()->set_species_temp_flux2(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_flux2(&sptr[isp*nng+j*ng]); j++; }
				if (setenden2) { thes()->set_species_temp_enden2(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_enden2(&sptr[isp*nng+j*ng]); j++; }
				if (setflux3) { thes()->set_species_temp_flux3(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_flux3(&sptr[isp*nng+j*ng]); j++; }
				if (setenden3) { thes()->set_species_temp_enden3(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_enden3(&sptr[isp*nng+j*ng]); j++; }
			}
		}

		if (setenden) { thes()->set_species_enden(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_enden(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_enden(&sptr[isp*nng+j*ng]); j++; }
		}

		if (setJ1dotE1) { thes()->set_species_J1dotE1(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_J1dotE1(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_J1dotE1(&sptr[isp*nng+j*ng]); j++; }
		}

		if (setJ2dotE2) { thes()->set_species_J2dotE2(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_J2dotE2(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_J2dotE2(&sptr[isp*nng+j*ng]); j++; }
		}

		if (setJ3dotE3) { thes()->set_species_J3dotE3(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_J3dotE3(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_J3dotE3(&sptr[isp*nng+j*ng]); j++; }
		}

		if (setJdotE) { thes()->set_species_JdotE(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_JdotE(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_JdotE(&sptr[isp*nng+j*ng]); j++; }
		}

		if (setvel1) { thes()->set_species_vel1(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_vel1(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_vel1(&sptr[isp*nng+j*ng]); j++; }
		}

		if (setvel2) { thes()->set_species_vel2(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_vel2(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_vel2(&sptr[isp*nng+j*ng]); j++; }
		}

		if (setvel3) { thes()->set_species_vel3(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_vel3(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_vel3(&sptr[isp*nng+j*ng]); j++; }
		}

		if (setT1) { thes()->set_species_T1(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_T1(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_T1(&sptr[isp*nng+j*ng]); j++; }
		}

		if (setT2) { thes()->set_species_T2(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_T2(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_T2(&sptr[isp*nng+j*ng]); j++; }
		}

		if (setT3) { thes()->set_species_T3(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_T3(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_T3(&sptr[isp*nng+j*ng]); j++; }
		}

		if (setTperp) { thes()->set_species_Tperp(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_Tperp(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_Tperp(&sptr[isp*nng+j*ng]); j++; }
		}

		if (setTemp) { thes()->set_species_Temp(&sptr[isp*nng+j*ng]); j++;
		if (theparser->get_naverage() > 1) {
			thes()->set_species_temp_Temp(&sptr[isp*nng+j*ng]); j++; thes()->set_species_ave_Temp(&sptr[isp*nng+j*ng]); }
		}
		isp++; // go to next species, MAL 10/10/09
	}
}

void DiagnosticControl::init_sheathwidth_diag() // MAL 9/30/09
{
	if(theparser->get_Lparam() > 0.) thefield->set_scoef0(1./theparser->get_Lparam() - 1.);
	if(theparser->get_Rparam() > 0.) thefield->set_scoef1(1./theparser->get_Rparam() - 1.);
	thefield->set_k0(3); // need 4 successive true tests for the sheath condition to be found
}

void DiagnosticControl::update()
{
	oopicListIter<Diagnostic> thei(thediagnostics);
	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->update();
	}
}

void DiagnosticControl::display_string(Diagnostic *td)
{
	ostring plname = td->get_full_name();
	fprintf(stdout, "*adding diagnostic: %s\n", plname());
}

void DiagnosticControl::parsediagnostics(FILE *fp)
{
	if(theparser)
	{
		fprintf(stderr, "Reparsing Diagnostics...\n");
		delete theparser;
	}
}

void DiagnosticControl::add_diagnostic(Diagnostic *thenewdiag)
{
	thediagnostics.add(thenewdiag);
	//  display_string(thenewdiag);
}

/*
#define LOCALCLASS DiagnosticControl
#include "diagonostics_fe_forcontrol.hpp"
#undef LOCALCLASS
*/

// diagnostic_fe.cpp -- functions for multiple classes
// the real contents are invoked when LOCALCLASS == DiagnosticControl
// Otherwise, a DiagnosticControl is instantiated and the Diagnostic
// is added there

void DiagnosticControl::add_gridarray(const char *name, Scalar *array, int n)
{
  #if DiagnosticControl == DiagnosticControl
  grid_arrays.add(new DataArray(name, n, array));
  #else
  DiagnosticControl dc;
  dc.add_gridarray(name, array, n);
  #endif
}

void DiagnosticControl::add_gridvectors(const char *name, Vector3 *array, int vdim, int n)
{
  #if DiagnosticControl == DiagnosticControl
  grid_vectors.add(new VectorArray(name, n, array, vdim));
  #else
  DiagnosticControl dc;
  dc.add_gridvectors(name, array, n);
  #endif
}

void DiagnosticControl::add_phasediagnostic(Species *s, BoundaryList * theb)
{
  #if DiagnosticControl == DiagnosticControl
  DiagnosticPhase *tpd;
  tpd = new DiagnosticPhase(s, theb);
  phase_diagnostics.add(tpd);
  add_diagnostic(tpd);

  #else
  DiagnosticControl dc;
  dc.add_phasediagnostic(s, theb);
  #endif
}

void DiagnosticControl::add_griddiagnostic(DataArray *a)
{
  #if DiagnosticControl == DiagnosticControl
  Diagnostic2d *td;

  td = new Diagnostic2d(gridarray, a);

  grid_diagnostics.add(td);
  add_diagnostic(td);

  #else
  DiagnosticControl dc;
  dc.add_griddiagnostic(a);
  #endif
}

void DiagnosticControl::add_griddiagnostic(VectorArray *a)
{
  #if DiagnosticControl == DiagnosticControl
  Diagnostic2d *td;

  td = new Diagnostic2d(gridarray, a);

  grid_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc;
  dc.add_griddiagnostic(a);
  #endif
}

void  DiagnosticControl::add_timediagnostic(Scalar *val, ostring nnn)
{
  #if DiagnosticControl == DiagnosticControl
  TimeDiagnostic *td;

  td = new TimeDiagnostic(timepointer, val, 1, nnn, false);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc;
  dc.add_time_diagnostic(val, nnn);
  #endif
}

void  DiagnosticControl::add_timediagnostic(int (Species::*spif)(), ostring nnn, bool tosum)
{
  #if DiagnosticControl == DiagnosticControl
  TimeDiagnostic *td;

  td = new TimeDiagnostic(timepointer, thesplist, spif, nnn, tosum);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc;
  dc.add_timediagnostic(spif, nnn, tosum);
  #endif
}

 // For integral of time-average J1dotE1 diagnostic, MAL 12/11/09
void  DiagnosticControl::add_timediagnostic(Scalar (Species::*spif)(), ostring nnn, bool tosum)
{
  #if DiagnosticControl == DiagnosticControl
  TimeDiagnostic *td;

  td = new TimeDiagnostic(timepointer, thesplist, spif, nnn, tosum);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc;
  dc.add_timediagnostic(spif, nnn, tosum);
  #endif
}

void  DiagnosticControl::add_timediagnostic(Vector3 (Species::*spvf)(), ostring nnn)
{
  #if DiagnosticControl == DiagnosticControl
  TimeDiagnostic *td;

  td = new TimeDiagnostic(timepointer, thesplist, spvf, nnn);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc;
  dc.add_timediagnostic(spvf);
  #endif
}

void  DiagnosticControl::add_timediagnostic(Vector3 (Species::*spvf)(), ostring nnn, XGoption xg_option)
{
  #if DiagnosticControl == DiagnosticControl
  TimeDiagnostic *td;

  td = new TimeDiagnostic(timepointer, thesplist, spvf, nnn, xg_option);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc;
  dc.add_timediagnostic(spvf);
  #endif
}

void DiagnosticControl::add_timediagnostic(Scalar *arr, int nitms, ostring nnn, bool tosum)
{
  #if DiagnosticControl == DiagnosticControl
  TimeDiagnostic *td;
  td = new TimeDiagnostic(timepointer, arr, nitms, nnn, tosum);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc; // changed "Dianostic" to "Diagnostic", MAL 6/12/09
  dc.add_timediagnostic(arr, nitms, nnn, tosum);
  #endif
}

// add species, JK 2019-01-19
void DiagnosticControl::add_timediagnostic(SpeciesList *spl, Scalar *arr, int nitms, ostring nnn, bool tosum)
{
  #if DiagnosticControl == DiagnosticControl
  TimeDiagnostic *td;
  td = new TimeDiagnostic(timepointer, spl, arr, nitms, nnn, tosum);

  time_diagnostics.add(td);
  add_diagnostic(td);
  #else
  DiagnosticControl dc; // changed "Dianostic" to "Diagnostic", MAL 6/12/09
  dc.add_timediagnostic(arr, nitms, nnn, tosum);
  #endif
}

void DiagnosticControl::add_sp_grid_array(const char *name,
				   Scalar * (Species::*spga)() const)
{
  #if DiagnosticControl == DiagnosticControl
  sp_grid_arrays.add(new std::pair<ostring, Scalar * (Species::*)()const>(ostring(name), spga));
  #else
  DiagnosticControl dc; // changed "Dianostic" to "Diagnostic", MAL 6/12/09
  dc.add_sp_grid_array(name, spga);
  #endif
}
