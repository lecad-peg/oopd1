#include "main/oopiclist.hpp"
#include "utils/ostring.hpp"
#include "main/pd1.hpp"
#include "main/parse.hpp"
#include "utils/ovector.hpp"
#include "fields/fields.hpp" // add for subcycle, MAL 1/3/10
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/particlegrouparray.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "utils/VariableArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_phase.hpp"
#include "main/grid.hpp"
#include "main/boundary.hpp"

#include "main/grid_inline.hpp"
#include "main/boundary_inline.hpp"

DiagnosticPhase::DiagnosticPhase()
{
	setdefaults();
}

DiagnosticPhase::DiagnosticPhase(Species * _thes, BoundaryList * _theb)
{
	setdefaults();
	thespecies = _thes;
	boundarylist = _theb;
}

DiagnosticPhase::DiagnosticPhase(int _n, Species * _thes, BoundaryList * _theb)
{
	setdefaults();
	nsamppart = _n;
	thespecies = _thes;
	boundarylist = _theb;
}

DiagnosticPhase::~DiagnosticPhase()
{
	if(xarray) { delete xarray; }
	yarray.deleteAll();
}

void DiagnosticPhase::set_array(int dim)
{
	// exit if dim is out of bounds
	if((dim < 0) || (dim > 2))
	{
		fprintf(stderr, "DiagnosticPhase::set_array() -- dim[=%d] out of bounds\n", dim);
		exit(1);
	}

	// exit if thespecies is null
	if(!thespecies)
	{
		fprintf(stderr, "DiagnosticPhase::set_array() -- thespecies == 0\n");
		exit(1);
	}

	ostring coordname = thespecies->get_grid()->geometry_string();
	ostring thiscoord = thespecies->get_grid()->geometry_string(dim);

	if(!xarray) { xarray = new VariableArray(coordname, nsamppart); }

	ostring yname;

	yname = "V";
	yname += thiscoord;

	yarray.add(new VariableArray(yname, nsamppart, dim));

}


void DiagnosticPhase::setdefaults()
{
	type = PHASEDIAGNOSTICTYPE;
	thespecies = 0;
	boundarylist = 0;
	nsamppart = SAMPLESIZE;
	stridesize = STRIDESIZE;
	linecolor = FILLCOLOR;
	fillcolor = FILLCOLOR;
	display_material = true;

	samplefunc = &DiagnosticPhase::FrontSample;

	// sample all components of phase space by default
	v_sample[0] = v_sample[1] = v_sample[2] = true;

	// initialize variables for XGrafix
	// (overides defaults in Diagnostic2d)
	option.set_xrescale(0);
}

void DiagnosticPhase::allocate_arrays()
{
	if(!thespecies) { delete this; }

	maxdim = 1;
	if(thespecies->is_3v())
	{
		maxdim = 3;
	}

	for(int i=0; i < maxdim; i++)
	{
		if(v_sample[i])
		{
			set_array(i);
		}
	}

}

void DiagnosticPhase::setSampleSize()
{
	int i;
	int nparttot;
	int nnum, left;

	ParticleGroupList * tpgl = thespecies->get_ParticleGroupList();
	oopicListIter<ParticleGroup> thei(*tpgl);

	nparttot = thespecies->number_particles();
	indices.resize(tpgl->nItems());

	left = 0;
	for(thei.restart(), i=0; !thei.Done(); thei++, i++)
	{
		nnum = nsamppart*thei()->get_n()/nparttot;
		indices[i].resize(nnum);
	}
}

void DiagnosticPhase::InitRandomNumbers()
{
}

// sample particles via a strided sample
// to be filled out, JH, 1/18/05
void DiagnosticPhase::StrideSample()
{
}

// sample particles from the front of phase space
void DiagnosticPhase::FrontSample()
{
	oopicListIter<DataArray> they(yarray);
	ParticleGroupList * tpgl = thespecies->get_ParticleGroupList();
	oopicListIter<ParticleGroup> thei(*tpgl);

	Scalar *tmpptr;
	int dddim;
	int i = 0;

	// DEBUG CODE -- may be deleted (JH, 1/18/2005)
	//  ostring dbname = thespecies->get_name();
	//  fprintf(stderr, "FrontSample species = %s\n", dbname());

	for(thei.restart(); !thei.Done(); thei++)
	{
		if((i + thei()->get_n()) < nsamppart)
		{
			tmpptr = xarray->get_array() + i;
			thei()->copy_x(tmpptr, 0);

			for(they.restart(); !they.Done(); they++)
			{
				dddim = get_yarr_dim(they());
				tmpptr = they()->get_array() + i;
				thei()->copy_v(tmpptr, 0, dddim);
			}

			i += thei()->get_n();
		}
		else
		{
			int rem = nsamppart - i;
			tmpptr = xarray->get_array() + i;
			thei()->copy_x(tmpptr, 0, rem);

			for(they.restart(); !they.Done(); they++)
			{
				dddim = get_yarr_dim(they());
				tmpptr = they()->get_array() + i;
				thei()->copy_v(tmpptr, 0, dddim, rem);
			}
			// mindgame: eventually, memory resizing is preferred.
			fprintf(stderr, "\nSome of %s won't be shown in DiagnosticPhase.", thespecies->get_name().c_str());
			fprintf(stderr, "\n  To see all, increase nsamppart (>= %d) in the inputfile!\n", i+thei()->get_n());

			break;
		}
	}
}

void DiagnosticPhase::SamplePhaseSpace()
{
	oopicListIter<DataArray> they(yarray);

	// call the sampling function
	((*this).*samplefunc)();

	// update the sizes
	xarray->setSize(nsamppart);
	for(they.restart(); !they.Done(); they++)
	{
		they()->setSize(nsamppart);
	}
}

void DiagnosticPhase::sample_all_particles()
{

	ParticleGroupList * tpgl = thespecies->get_ParticleGroupList();
	oopicListIter<ParticleGroup> thei(*tpgl);
	oopicListIter<DataArray> they(yarray);

	Scalar *tmpptr;
	int dddim;
	int i=0;
	for(thei.restart(); !thei.Done(); thei++)
	{
		tmpptr = xarray->get_array() + i;
		thei()->copy_x(tmpptr, 0);

		for(they.restart(); !they.Done(); they++)
		{
			//dddim = get_yarr_dim(they());
			dddim = they()->label;

			tmpptr = they()->get_array() + i;
			thei()->copy_v(tmpptr, 0, dddim);
		}

		i += thei()->get_n();
	}

	// update the sizes
	xarray->setSize(i);
	for(they.restart(); !they.Done(); they++)
	{
		they()->setSize(i);
	}
}

void DiagnosticPhase::update()
{
	if (thespecies->get_field()->get_nsteps() % thespecies->get_subcycle() !=0) return; // add for subcycle, MAL 1/3/10
	int nparttot = thespecies->number_particles();

	if(nparttot <= nsamppart)
	{
		sample_all_particles();
	}
	else
	{
		SamplePhaseSpace();
	}
}

ostring DiagnosticPhase::get_full_name() const
{
	ostring plname = "";
	plname = thespecies->get_name();
	plname += " phase space";
	return plname;
}

void DiagnosticPhase::initialize()
{
	allocate_arrays();

	// does this need to be here? JH, 1/18/05
	//  if(!yarray.nItems()) { delete this; }

	initXGrafix();
}

void DiagnosticPhase::initXGrafix()
{
	bool wininit = false;
	ostring plname;
	oopicListIter<DataArray> they(yarray);

	oopicListIter<Boundary> thej(*boundarylist);
	Scalar x1_tmp, x2_tmp, y1_tmp, y2_tmp;

	// get xmin, xmax from grid dimensions
	Grid *tg = thespecies->get_grid();
	option.set_xmin(tg->getx0());
	option.set_xmax(tg->getx1());
	option.set_xoffset(option.xmin());

	// get scaling factors
	option.set_xscale(tg->getdx());
	option.set_yscale(thespecies->get_dxdt());

	for(they.restart(); !they.Done(); they++)
	{
		if((!wininit) || v_sep_plots)
		{
			plname = get_full_name();

			if(v_sep_plots)
			{
				plname += " ";
				plname += they()->getName();
				plname += "-";
				plname += xarray->getName();
			}

			openWindow(plname);

			wininit = true;
		}

		// mindgame: Drawing the boundary regions
		if (display_material)
		{
			for(thej.restart(); !thej.Done(); thej++)
			{
				if (thej()->get_material() != EMPTY)
				{
					x1_tmp = thej()->get_r0();
					x2_tmp = thej()->get_r1();
					// very large values
					y1_tmp = -1e10;
					y2_tmp = 1e10;
					XGStructure(5, FILLED, linecolor, fillcolor,
							x1_tmp, y1_tmp, x1_tmp, y2_tmp,
							x2_tmp, y2_tmp, x2_tmp, y1_tmp,
							x1_tmp, y1_tmp);
				}
			}
		}

		XGScat2D(xarray->get_array(),
				they()->get_array(), xarray->getSizePointer(),
				thespecies->get_ident());
	}
}

int DiagnosticPhase::get_yarr_dim(DataArray *tyarr)
{
	return tyarr->label;
/*
	ostring prefix = "V";
	ostring compname;

	for(int i=0; i < 3; i++)
	{
		compname = prefix + thespecies->get_grid()->geometry_string(i);
		if(compname == tyarr->getName()) { return i; }
	}

	return -1;
*/
}
