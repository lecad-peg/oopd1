#include<stdio.h>

#include<vector>
using namespace std;

#include "utils/ostring.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/Array2d.hpp"
#include "utils/ovector.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic3d.hpp"

#include "xgrafix.h" // add, MAL 12/19/09

Diagnostic3d::Diagnostic3d(DataArray *xa, DataArray *ya, Array2D *za)
{
  setdefaults();

  xarray = xa;
  yarray = ya;
  zarray = za;
  name = zarray->getName();
}

void Diagnostic3d::setdefaults()
{
  theta = phi = 45.;
}

ostring Diagnostic3d::get_type(int i) const
{
  return ostring("lin");
}

////////////////////////////////////
// virtual functions from Diagnostic
////////////////////////////////////

ostring Diagnostic3d::get_full_name() const
{
  ostring fn;

  fn = name;
  fn += "(";
  fn += xarray->getName();
  fn += ",";
  fn += yarray->getName();
  fn += ")";

  return fn;
}

void Diagnostic3d::initialize()
{
  initXGrafix();  // sets up the XGrafix windows
}

void Diagnostic3d::openWindow()
{
  int i;
  ostring type = "";
  char state[] = "closed";
  if(openflag)
    {
      sprintf(state, "open");
    }

  for(i=0; i < 3; i++)
    {
      type += get_type(i);
    }
  
  int *theip;
  theip = new int(openflag);
  xgopenlist.add(theip);

//  XGSet3DFlag(type(), xarray->getName().c_str(), 
//	      yarray->getName().c_str(), 
//	      zarray->getName().c_str(), 
  XGSet3DFlag(type.c_str2(), xarray->getName().c_str2(), 
	      yarray->getName().c_str2(), 
	      get_full_name().c_str2(), // zarray->getName() changed to this, MAL 12/19/09
	      theta, phi,
	      state, ulx, uly,
	      option.xscale(), option.yscale(), option.zscale(),
	      option.xrescale(), option.yrescale(), option.zrescale(),
	      option.xmin(), option.xmax(),
	      option.ymin(), option.ymax(), option.zmin(),
	      option.zmax(), theip);
}

void Diagnostic3d::initXGrafix()
{
  openWindow(); // front-end to XGSet3DFlag -- opens window
	// add call to XGSurf(), MAL 12/17/09
			XGSurf(xarray->get_array(), yarray->get_array(), zarray->get_ptrptr(),
				xarray->getSizePointer(), yarray->getSizePointer(), 0);
			xginited = true;
}
