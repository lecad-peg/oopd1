#include<stdio.h>

#include<vector>
using namespace std;

#include "utils/ostring.hpp"
#include "main/oopiclist.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"

#include "xgrafix.h"

void dummy() 
{
}

Diagnostic2d::Diagnostic2d(DataArray *xrr, ostring nm)
{
	setdefaults();
	name = nm;
	xarray = xrr;
}

Diagnostic2d::Diagnostic2d(DataArray *xrr, DataArray *yrr)
{
	setdefaults();
	name = yrr->getName();

	xarray = xrr;
	yarray.add(yrr, false);
	plottype.add(new int(CURVE));
}

Diagnostic2d::Diagnostic2d(DataArray *xrr, VectorArray *yrr)
{
	setdefaults();
	name = yrr->getName();

	xarray = xrr;
	vyarray.add(yrr);
	vplottype.add(new int(VECCURVE));
}

Diagnostic2d::Diagnostic2d()
{
	xarray = 0;
	setdefaults();
}

Diagnostic2d::Diagnostic2d(void (*g)())
{
	xarray = 0;
	setdefaults();
	f = g;
}

Diagnostic2d::~Diagnostic2d()
{
	plottype.deleteAll();
	vplottype.deleteAll();
	// mindgame: to avoid memory leaks
	yarray.deleteAll();
}

void Diagnostic2d::add_y_array(Scalar *yrr, int type)
{
	if(!xarray) { diagnostic_error("Diagnostic2d::add_y_array -- xarray == 0"); }

	yarray.add(new DataArray(name, xarray->getSize(), yrr));
	plottype.add(new int(type));
}

// JK, 2019-01-13
void Diagnostic2d::add_y_array(Scalar *yrr, ostring label, int type)
{
	if(!xarray) { diagnostic_error("Diagnostic2d::add_y_array -- xarray == 0"); }

	yarray.add(new DataArray(label, xarray->getSize(), yrr));
	plottype.add(new int(type));
}


void Diagnostic2d::setdefaults()
{
	xarray = 0;

	v_sep_plots = true;

	vecstring[0] = "0";
	vecstring[1] = "1";
	vecstring[2] = "2";

	f = dummy;
	col = 0;
	sprintf(scaletype, "linlin");
	dimstr = "";

	vecdisplay[0] = true;
	vecdisplay[1] = vecdisplay[2] = true;

}

void Diagnostic2d::update()
{
	f();
	write_data();
}

void Diagnostic2d::initialize()
{
	initXGrafix();
}

void Diagnostic2d::set_func(void (*g)())
{
	f = g;
}

void Diagnostic2d::diagnostic_error(const char *str)
{
	fprintf(stderr, "\nDiagnostic:");
	if(strlen(str)) {      fprintf(stderr, "\n%s", str); }
	fprintf(stderr, "\nexiting...");
	exit(1);
}

ostring Diagnostic2d::get_full_name() const
{
	ostring plname = name + dimstr + srname + "(" + xarray->getName() + ")";

	if(xarray->getName() == "angle") {
		// remind users that distribution function includes sin(theta); JK 2012-02-03 after suggestion from MAL
		plname += ostring("*sin(angle)");
	}

	return plname;
}

void Diagnostic2d::openWindow(const ostring & yname)
{
	char state[] = "closed";
	if(openflag)
	{
		sprintf(state, "open");
	}

	int *theip;
	theip = new int(openflag);
	xgopenlist.add(theip);

	XGSet2DFlag_OS(scaletype,
			xarray->getName().c_str2(),
			//		 xarray->getName().c_str(),
			yname.c_str2(),
			//		 yname.c_str(),
			state, ulx, uly, option.xoffset(), option.yoffset(),
			option.xscale(), option.yscale(),
			option.xrescale(), option.yrescale(),
			option.xmin(), option.xmax(), option.ymin(), option.ymax(),
			theip);
}

void Diagnostic2d::initXGrafix()  // sets up the XGrafix windows
{
	bool flag = true;

	oopicListIter<DataArray> thei(yarray);
	oopicListIter<VectorArray> thev(vyarray);
	oopicListIter<int> thej(plottype);
	oopicListIter<int> thevj(vplottype);

	// set first name for vector plots
	if(vyarray.nItems() && v_sep_plots)
	{
		dimstr = vecstring[0];
	}

	ostring plname = get_full_name();
	openWindow(plname);

	if(plottype.nItems() != yarray.nItems())
	{
		fprintf(stderr, "Diagnostic2d::initXGrafix: \n");
		fprintf(stderr, "plottype.nItems() != yarray.nItems()\n");
		fprintf(stderr, "plottype.nItems()=%d,  yarray.nItems()=%d\n", plottype.nItems(), yarray.nItems());

	}

	for(thei.restart(), thej.restart(); !thei.Done(); thei++, thej++)
	{
		flag = false;
		switch(*thej())
		{
		case CURVE:
			XGCurveWithLabel(xarray->get_array(), thei()->get_array(), xarray->getSizePointer(), nextcolor(), thei()->getName().c_str2());
			break;
		case SCATTER:
			XGScat2DWithLabel(xarray->get_array(), thei()->get_array(), xarray->getSizePointer(), nextcolor(), thei()->getName().c_str2());
			break;
		default:
			fprintf(stderr, "\nUnknown plot type: %d", *thej());
			diagnostic_error("initXGrafix:");
		}
	}

	// plot Vector3 s
	for(thev.restart(), thevj.restart(); !thev.Done(); thev++, thevj++)
	{
		flag = false;
		switch(*thevj())
		{
		case VECCURVE:
			for(int tint=0; tint < thev()->get_vdim(); tint++)
			{
				XGCurveVector(xarray->get_array(), ((Scalar *)(thev()->get_array()) + tint), xarray->getSizePointer(), nextcolor(), 1, 0, sizeof(Vector3)/sizeof(Scalar), 0);
				if((tint + 1) < thev()->get_vdim())
				{
					dimstr = vecstring[tint+1];
					plname = get_full_name();
					openWindow(plname);
				}
			}
			break;
		default:
			fprintf(stderr, "\nUnknown plot type: %d", *thej());
			diagnostic_error("initXGrafix:");
		}
	}

	if(flag)
	{
		diagnostic_error("initXGrafix: no y-array found!");
	}
	dimstr = "";
	xginited = true;
}

int Diagnostic2d::nextcolor(bool increment)
{
	int tmp = col;
	if(increment) { col++; }
	return tmp;
}

int Diagnostic2d::currentcolor(void)
{
	return nextcolor(false);
}

/*
 * Write diagnostics data to file given by file handle.
 * JK, 2018-01-22
 */
void Diagnostic2d::output(FILE *fp)
{
	int i;
	oopicListIter<DataArray> thei(yarray);
	Scalar *ydata[yarray.nItems()];

	for(thei.restart(), i=0; !thei.Done(); thei++, i++) {
		// create an array of pointers to data arrays;
		ydata[i] = thei()->get_array();
	}

	// write header of the file
	fprintf(fp, "# t=%.9e; row content: i xarray[i] yarray[0][i]...\n", *time);

	for( int i=0 ; i < xarray->getSize() ; i++ ){
		// write the data: 1st col is xarr, other columns are yarrs
		fprintf(fp, "%d %g", i, xarray->getValue(i));

		for( int j=0 ; j < yarray.nItems() ; j++) {
			// write all Y-data
			fprintf(fp, " %g", ydata[j][i]);
		}

		fprintf(fp, "\n");
	}
}


void Diagnostic2d::set_vecstring(ostring a, ostring b, ostring c)
{
	vecstring[0] = a;
	vecstring[1] = b;
	vecstring[2] = c;
}

