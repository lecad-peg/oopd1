#include <stdio.h>
#include <vector>
#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "utils/ostring.hpp"
#include "main/parse.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VariableArray.hpp"
#include "utils/VectorArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_time.hpp"
#include "diagnostics/diagnostic_phase.hpp"
#include "diagnostics/diagnostic_eedf.hpp"
#include "diagnostics/diagnostic_parser.hpp"

DiagnosticParser::DiagnosticParser(bool tl)
{
	toplevel = tl;
	setdefaults();
	setparamgroup();
}

DiagnosticParser::DiagnosticParser(FILE *fp, oopicList<Parameter<Scalar> > float_variables, oopicList<Parameter<int> > int_variables, bool tl)
{
	toplevel = tl;
	init_parse(fp, float_variables, int_variables);
}

DiagnosticParser::DiagnosticParser(FILE *fp, oopicList<Parameter<Scalar> > float_variables, oopicList<Parameter<int> > int_variables, DiagList *t,
									bool tl, ostring inClassName)
{
	thediagnostics = t;
	toplevel = tl;
	set_name(inClassName);
	init_parse(fp, float_variables, int_variables);
}

DiagnosticParser::~DiagnosticParser()
{
	dps.deleteAll();
}

void DiagnosticParser::setdefaults(void)
{
	if( classname == "" ) {
		classname = "DiagnosticControl";			// default class name
	}

	initopen = false;
	bulkv = false;
	histmax = NSTEPS;
	ncomb = NCOMB;
	naverage = NAVERAGE; // changed from "1" for grid time averages, MAL 6/12/09
	Lparam = 0.; // default is no left hand sheath diagnostic, MAL 6/25/09
	Rparam = 0.; // default is no right hand sheath diagnostic, MAL 6/25/09
	// for diagnostics control MAL 6/26/09
	flux1 = flux2 = flux3 = J1dotE1 = J2dotE2 = J3dotE3 =  enden1 = enden2 = enden3 = false;
	vel1 = vel2 = vel3 = T1 = T2 = T3 = Tperp = Temp = JdotE = enden = alloff = allon1D = allon = false;
	ngdu = 1; // update grid diagnostics every ngdu timesteps; does not apply to rho, phi, and n
	name = "";
	nsamppart = SAMPLESIZE;
	display_material_flag = true;
	// EEDF settings, JK, 2017-11-01
	eedf_diag = eedf_diags_enabled = false;
	eedf_energybins = 10;
	eedf_update_interval = 15;
	eedf_j0 = eedf_j1 = 0;
	eedf_jbins = 1;
	Emin = Emax = -1;

	write_start = write_end = -1;
	write_step = 1;
	outfile = "";
//	list_j0 = list_j1 = list_write_start = "";			// JK, 2018-01-26
//	vec_j0.clear();										// JK, 2018-01-27
//	vec_j1.clear();										// JK, 2018-01-27
//	vec_write_start.clear();							// JK, 2018-01-27
	reaction_diag = false;								// JK, 2019-11-27 - include reaction diagnostics or not
}

void DiagnosticParser::setparamgroup(void)
{
	// generic parameters for all Diagnostics
	pg.add_string("name", &name, "name of diagnostic");
	pg.add_bflag("open", &initopen, true, "start diagnostic open");

	pg.add_bflag("bulkvelocity", &bulkv, true, "display bulk velocity");

	// for species gridded diagnostics control MAL 6/26/09 and 9/25/09
	pg.add_bflag("particleflux1", &flux1, true, "display particle flux 1");
	pg.add_bflag("particleflux2", &flux2, true, "display particle flux 2");
	pg.add_bflag("particleflux3", &flux3, true, "display particle flux 3");
	pg.add_bflag("J1dotE1", &J1dotE1, true, "display J1dotE1");
	pg.add_bflag("J2dotE2", &J2dotE2, true, "display J2dotE2");
	pg.add_bflag("J3dotE3", &J3dotE3, true, "display J3dotE3");
	pg.add_bflag("energydensity1", &enden1, true, "display energy density 1");
	pg.add_bflag("energydensity2", &enden2, true, "display energy density 2");
	pg.add_bflag("energydensity3", &enden3, true, "display energy density 3");
	pg.add_bflag("averagevelocity1", &vel1, true, "display average velocity 1");
	pg.add_bflag("averagevelocity2", &vel2, true, "display average velocity 2");
	pg.add_bflag("averagevelocity3", &vel3, true, "display average velocity 3");
	pg.add_bflag("temperature1", &T1, true, "display temperature 1");
	pg.add_bflag("temperature2", &T2, true, "display temperature 2");
	pg.add_bflag("temperature3", &T3, true, "display temperature 3");
	pg.add_bflag("temperatureperp", &Tperp, true, "display perpendicular temperature");
	pg.add_bflag("temperature", &Temp, true, "display temperature");
	pg.add_bflag("JdotE", &JdotE, true, "display total JdotE");
	pg.add_bflag("energydensity", &enden, true, "display total energy density");
	pg.add_bflag("alloff", &alloff, true, "turn off all named diagnostics");
	pg.add_bflag("allon1D", &allon1D, true, "turn on all 1D diagnostics"); // MAL 9/13/10
	pg.add_bflag("allon", &allon, true, "turn on all 1D and 3D diagnostics"); // MAL 9/13/10
	pg.add_iparameter("ngriddiagupdate", &ngdu, "calculate grid diagnostics every ngriddiagupdate timesteps");

	// parameters for TimeDiagnostic
	pg.add_iparameter("ncomb", &ncomb, "number of steps to comb for time diagnostics");
	pg.add_iparameter("histmax", &histmax, "maximum number of steps before combing begins"); // added explanation, MAL 6/14/09
	pg.add_iparameter("naverage", &naverage, "number of steps for grid time average diagnostics"); // added explanation, MAL 6/14/09
	pg.add_fparameter("Lsheath", &Lparam, "defines left hand sheath s(t) where n-(t)=Lsheath*n+(t)"); // for sheath diagnostic, MAL 6/25/09
	pg.add_fparameter("Rsheath", &Rparam, "defines right hand sheath s(t) where n-(t)=Rsheath*n+(t)"); // for sheath diagnostic, MAL 6/25/09

	// parameters for DiagnosticPhase
	pg.add_iparameter("nsamppart", &nsamppart, "number of particles to display in phase space");
	pg.add_bflag("no_display_material", &display_material_flag, false, "the position of material is not displayed in phase space");

	if(!toplevel) {
		// parameters for individual diagnostics blocs (e.g. EEDF), JK, 2018-01-16
		pg.add_iparameter("energybins", &eedf_energybins, "number of intervals in eV for displaying EEDF");
		pg.add_iparameter("eedf_update_interval", &eedf_update_interval, "number of steps between EEDF updates; larger number speeds up simulation");
		pg.add_iparameter("j0", &eedf_j0, "start cell for EEDF diagnostics");
		pg.add_iparameter("j1", &eedf_j1, "end cell for EEDF diagnostics");
		pg.add_string("filename", &outfile, "output file format (filename can have %d formating)");
		pg.add_iparameter("write_start", &write_start, "starting time step for writing diagnostics into file in diagnostics UPDATE (not computational) steps");
		pg.add_iparameter("write_step", &write_step, "step (frequency) of writing diagnostics into file in diagnostics UPDATE (not computational) steps");
		pg.add_iparameter("write_end", &write_end, "end time step for writing diagnostics into file in diagnostics UPDATE (not computational) steps");
		pg.add_fparameter("Emin", &Emin, "[eV] minimum energy to accumulate");
		pg.add_fparameter("Emax", &Emax, "[eV] maximum energy to accumulate");
		// added by JK, 2018-01-27
		pg.add_iparameter("jbins", &eedf_jbins, "number of equal spaced intervals in space in which define EEDF (each interval has its own EEDF)");
//		pg.add_string("list_j0", &list_j0, "list of starting cells of intervals for EEDF");
//		pg.add_string("list_j1", &list_j1, "list of ending cells of intervals for EEDF");
//		pg.add_string("list_write_start", &list_write_start, "list of starting time steps for writing diagnostics");
	}

	// add processing of reaction diagnostics flag
	pg.add_bflag("reaction_diagnostics", &reaction_diag, true, "collect and display reaction (collision) diagnostics");
}

void DiagnosticParser::check(void)
{
	ostring errstring = "Diagnostic";
	if(toplevel) { errstring += "Parser"; }

	vector<ostring> nonneg;
	nonneg.push_back(ostring("ncomb"));
	nonneg.push_back(ostring("histmax"));
	nonneg.push_back(ostring("naverage")); // naverag -> naverage, MAL 6/16/09
	nonneg.push_back(ostring("nsamppart"));
	nonneg.push_back(ostring("ngriddiagupdate")); // MAL 6/26/09

	if(!toplevel) {
		// not in toplevel parser
		nonneg.push_back(ostring("eedf_energybins"));			// JK, 2018-01-15
		nonneg.push_back(ostring("eedf_update_interval"));		// JK, 2017-11-01
		nonneg.push_back(ostring("eedf_j0"));
		nonneg.push_back(ostring("eedf_j1"));
		nonneg.push_back(ostring("eedf_jbins"));				// JK, 2018-01-27
	}

	if(ngdu < 1) { error("ngriddiagupdate must be an integer > 0"); } // for grid average diagnostics, MAL 6/27/09
	if(Lparam < 0. || Lparam >= 1.) { error("Lsheath must be a Scalar >= 0. and < 1."); } // for sheath diagnostic, MAL 6/25/09
	if(Rparam < 0. || Rparam >= 1.) { error("Rsheath must be a Scalar >= 0. and < 1."); } // for sheath diagnostic, MAL 6/25/09
	
	// add to kill diagnostics calculations when running in -nox mode, MAL 9/22/12
    // Removed in order to write results into text file, HH 02/22/16
	// if (!theRunWithXFlag) { ngdu=10000; naverage=1; Lparam=0.; Rparam=0.; alloff=true; }
	
	oopicListIter<Parameter<int> > thei(pg.iparam);

	for(thei.restart(); !thei.Done(); thei++)
	{
		for(unsigned int i=0; i < nonneg.size(); i++)
		{
			if(nonneg[i] == thei()->name)
			{
				if(thei()->val() < 0)
				{
					errstring += ": ";
					errstring += thei()->name();
					error(errstring(), "must be non-negative");
				}
			}
		}
	}
}

void DiagnosticParser::init(void)
{
}

Parse * DiagnosticParser::special(ostring s)
{
	if(s == "Diagnostic" || s == "EEDF")
	{
		DiagnosticParser *tmptr;
		if(!toplevel)
		{
			error("Diagnostic", "Can't have a Diagnostic within a Diagnostic");
		}

		tmptr = new DiagnosticParser(f, pg.float_variables, pg.int_variables, thediagnostics, false, s);

		if( s == "EEDF" ) {
			printf("== new EEDFparser; name: %s\n", tmptr->get_name().c_str());
			eedf_diags_enabled = true;
			tmptr->set_eedf(true);		// define diagnostics as EEDF diagnostics
			//
			eedf_dps.add(tmptr);
		}

		dps.add(tmptr);
		return tmptr;
	}

	return 0;
}

bool DiagnosticParser::is_phasediagnostic_enabled(Species *thesp) const
{
	return true;
}

///////////////////////////////////////
// functions to initialize diagnostics
///////////////////////////////////////

// front end function to other diagnostic initialization functions
int  DiagnosticParser::init_Diagnostic(Diagnostic *thed)
{
	TimeDiagnostic *tdptr;
	DiagnosticPhase *pdptr;
//	DiagnosticEEDF *edptr;

	if(toplevel)
	{
		oopicListIter<DiagnosticParser> thep(dps);

		for(thep.restart(); !thep.Done(); thep++)
		{
			ostring diag_name = thed->get_name();

			if(thep()->get_name() == thed->get_full_name()) {
				return thep()->init_Diagnostic(thed);

			} else if( thep()->is_eedf() && (diag_name == "EEDF" || diag_name == "EEPF") ) {
				// use second level parser to initialize EEDF/EEPF diagnostics
				return 0; // temporary ignoring this ; thep()->init_Diagnostic(thed);
			}
		}
	}

//	printf("== parser: %s eedf: %d, eedf_enabled: %d\n", get_name().c_str(), is_eedf(), is_eedf_enabled());

	// initialize generic Diagnostic parameters
	thed->set_openflag(initopen);
	// initialize parameters for writing to file
//	printf("Filename: %s (%s) toplevel: %d\n", outfile.c_str2(), thed->get_name().c_str2(), toplevel);
//	thed->set_write_params(outfile, write_start, write_step, write_end);

	// initialize Diagnostics based on type
	switch(thed->get_type())
	{
		case TIMEDIAGNOSTICTYPE:
		{
			tdptr = (TimeDiagnostic *)thed;
			init_TimeDiagnostic(tdptr);
			break;
		}
		case PHASEDIAGNOSTICTYPE:
		{
			pdptr = (DiagnosticPhase *)thed;
			init_PhaseDiagnostic(pdptr);
			break;
		}
		case EEDFDIAGNOSTICTYPE:
		{
//			edptr = (DiagnosticEEDF *)thed;
			break;
		}
	}

	return 0;
}

void DiagnosticParser::init_TimeDiagnostic(TimeDiagnostic *thetd)
{
	thetd->set_ntimesteps(histmax);
	thetd->set_ncomb(ncomb);
}

void DiagnosticParser::init_PhaseDiagnostic(DiagnosticPhase *thepd)
{
	thepd->set_nsamppart(nsamppart);
	thepd->set_display_material(display_material_flag);
}


/**
 * Convert string list of integers separated with commas into vector of integers.
 * JK, 2018-01-26
 */
vector<int> DiagnosticParser::list_to_vector(ostring in_list) {
	vector<ostring> parts = in_list.partition(',');
	vector<int> vec(parts.size());

	for( unsigned int i=0, j=0 ; i < parts.size() ; i++) {
		int pos = atoi(parts[i]);

		if( pos > -1 ) {
			vec[j] = pos;
			j++;
		}
	}
	return vec;
}

/*
 * String list of doubles into vector of doubles.
 */
vector<double> DiagnosticParser::list_to_vector_double(ostring in_list) {
	vector<ostring> parts = in_list.partition(',');
	vector<double> vec(parts.size());

	for( unsigned int i=0, j=0 ; i < parts.size() ; i++) {
		int pos = atoi(parts[i]);

		if( pos > -1 ) {
			vec[j] = pos;
			j++;
		}
	}
	return vec;
}

