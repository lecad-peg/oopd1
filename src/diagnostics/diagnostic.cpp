#include <stdio.h>

#include <vector>
using namespace std;

#include "utils/ostring.hpp"
#include "main/oopiclist.hpp"
#include "diagnostics/diagnostic.hpp"

ostring Diagnostic::get_file_name() const
{
	ostring fname;

	if( filename.find("%") > 0 ) {
		// add write sequence to file name
		char tmpname[1024];
		sprintf(tmpname, filename.c_str2(), output_num);
		fname = ostring(tmpname);
	} else {
		// just return given file name
		fname = filename;
	}

	return fname;
}

ostring Diagnostic::get_file_mode() const
{
	ostring mode = "w";
	if(increment) { mode = "a"; }
	if(binary)    { mode += "b"; }
	return mode;
}

void Diagnostic::write_data()
{
	if(noutput < 0) { return; }			// no writing
	output_index++;
//	printf("noutput=%d, output_index=%d, output_start=%d, output_num=%d, t=%g\n", noutput, output_index, output_start, output_num, *time);

	if( (output_num == -1 && (output_start == -1 || output_index < output_start)) ||		// no writing yet - we skip first "output_start" calls to update diagnostics
		(output_num > -1 && output_index < noutput) ||								// no writing yet - we have to skip "noutput" steps
		(output_end != -1 && output_num > output_end) ) {		// no writing any more - we wrote required number of steps
		return;
	}

//	printf(" ==> writing\n");

	output_index = 0;			// reset number of step between writes

	if( output_num == -1 ) { output_index --; }

	output_start = 0;			// start value is used only the first time, then not any more
	output_num++;

	FILE *fp;
	ostring fname, mode;

	fname = get_file_name();
	mode  = get_file_mode();

	if( fname.length() > 0 ) {
		// we got filename; try to save data into file
		if(!(fp=fopen(fname(), mode()))) {
			fprintf(stderr, "Diagnostic::write_data : file could not be opened\n");
			return;
		}

		output(fp);
		printf("Diagnostic %s: at t=%gs written into file %s...\n", name.c_str(), *time, fname.c_str());

		fclose(fp);
	}
}

// is_xgopen : returns true if ANY of the windows is open 
bool Diagnostic::is_xgopen()
{
	bool retval = false;

	oopicListIter<int> thej(xgopenlist);
	for(thej.restart(); !thej.Done(); thej++)
	{
		if( (retval = *thej()) ) { return retval; }
	}
	return retval;
}
