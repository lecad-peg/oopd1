/*
 * Diagnostics for displaying reactions collision frequency
 * JK, 2019-11-26
 */
#include "main/oopiclist.hpp"
#include "utils/ostring.hpp"
#include "main/pd1.hpp"
#include "main/parse.hpp"
#include "utils/ovector.hpp"
#include "fields/fields.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/particlegrouparray.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "utils/VariableArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_parser.hpp"
#include "reactions/reaction.hpp"
#include "diagnostics/diagnostic_reaction.hpp"
#include "main/grid.hpp"
#include "MCC/mcc.hpp"

/* initialize the diagnostics from given reaction */
DiagnosticReaction::DiagnosticReaction(DataArray *gridarray, ReactionGroup *group, int _reacIndex)
{
	reacIndex = _reacIndex;
	MCC * themcc = group->get_mcc();
	ReactionList *rl = group->get_reaction_list();
	// define vars for name of the diagnostics
	reactionType = rl->data(reacIndex)->get_reactiontypename();
//	chemical_equation = rl->data(reacIndex)->get_chemical_equation_short(0);
	chemical_equation = rl->data(reacIndex)->get_chemical_equation(0);
	threshold_energy = rl->data(reacIndex)->get_threshold_energy();

	// define name and x-array (grid)
	name = get_full_name();
	xarray = gridarray;
	// set diagnostics collection in the MCC
	themcc->set_collect_4_diagnostics(true);
	themcc->allocate_diagostics_arrays();
	// add given reaction data as first y array
	add_y_array(themcc->get_collision_distribution(reacIndex)->get_array(), CURVE);
}

void DiagnosticReaction::update()
{
	// update the diagnostics
	Diagnostic2d::update();
	// reset it before next loop to collect new data
	oopicListIter<DataArray> thei(yarray);
/*
	for(thei.restart(); !thei.Done(); thei++) {
		// reset all y-series in diagnostics
		thei()->zero();
	}
*/
}

ostring DiagnosticReaction::get_full_name()
{
	char buf[64];
	sprintf(buf, "K%.2f(eV) ", threshold_energy);

	for( int i=0 ; i < 64 && buf[i] != '\0' ; i++) {
		// remove the dot (.) form the diagnostics name, because xgrafix (TCL/TK) does not support it; JK 2019-12-02
		if( buf[i] == '.' ) {
			buf[i] = '_';
		}
	}

	ostring name = ostring(buf);
	name += reactionType + ostring(": ") + chemical_equation;
	return name;
}
