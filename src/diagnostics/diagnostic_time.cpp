#include<stdlib.h>

#include "main/pd1.hpp"
#include "utils/ostring.hpp"
#include "main/oopiclist.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "utils/VariableArray.hpp"
#include "main/TimeArray.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_time.hpp"

TimeDiagnostic::TimeDiagnostic()
{
	setdefaults();
}

TimeDiagnostic::TimeDiagnostic(TIMETYPE *tp)
{
	setdefaults();
	tpointer = tp;
}

TimeDiagnostic::TimeDiagnostic(TIMETYPE *tp, SpeciesList *spl, 
		int (Species::*spif)(),
		ostring nnn, bool tosum)
{
	setdefaults();
	tpointer = tp;
	splist = spl;
	spintfunc = spif;
	sum = tosum;
	name = nnn;

	nnyyarr = splist->nItems();
	td_type = SPINTFUNC;

}

TimeDiagnostic::TimeDiagnostic(TIMETYPE *tp, SpeciesList *spl, 
		Scalar (Species::*spsf)(),
		ostring nnn, bool tosum)
{
	setdefaults();
	tpointer = tp;
	splist = spl;
	spscfunc = spsf;
	sum = tosum;
	name = nnn;

	nnyyarr = splist->nItems();
	td_type = SPSCAFUNC;

}

TimeDiagnostic::TimeDiagnostic(TIMETYPE *tp, SpeciesList *spl, 
		Vector3 (Species::*spsf)(),
		ostring nnn)
{
	init(tp, spl, spsf, nnn);
}

TimeDiagnostic::TimeDiagnostic(TIMETYPE *tp, SpeciesList *spl,
		Vector3 (Species::*spsf)(),
		ostring nnn, XGoption xg_option)
{
	init(tp, spl, spsf, nnn);
	option = xg_option;
}

void TimeDiagnostic::init(TIMETYPE *tp, SpeciesList *spl,
		Vector3 (Species::*spsf)(),
		ostring nnn)
{
	setdefaults();
	tpointer = tp;
	splist = spl;
	spvecfunc = spsf;
	sum = false;
	name = nnn;

	if(!spl->nItems()) { delete this; }
	oopicListIter<Species> thes(*spl);
	thes.restart();
	if(thes()->is_3v())
	{
		nvcomp = 3;
	}
	else
	{
		nvcomp = 1;
	}

	nnyyarr = nvcomp*splist->nItems();
	td_type = SPVECFUNC;
}

TimeDiagnostic::TimeDiagnostic(TIMETYPE *tp, Scalar *thearray, int nitms,
		ostring nnn, bool tosum)
{
	setdefaults();
	tpointer = tp;
	sum = tosum;
	name = nnn;

	nnyyarr = nitms;
	td_type = SCALARRAY;
	udarray = thearray;
}

// add species list for later writing, JK 2019-01-19
TimeDiagnostic::TimeDiagnostic(TIMETYPE *tp, SpeciesList *spl, Scalar *thearray, int nitms, ostring nnn, bool tosum)
{
	setdefaults();
	tpointer = tp;
	splist = spl;
	sum = tosum;
	name = nnn;

	nnyyarr = nitms;
	td_type = SCALARRAY;
	udarray = thearray;

/*	oopicListIter<Species>   thes;
	int i;
	printf("Boundary\n");

	for(thes.restart(*spl), i=0; !thes.Done(); thes++, i++) {
		printf("%d: %s\n", i, thes()->get_name().c_str());
	}

	printf("Done!\n");*/
}

TimeDiagnostic::~TimeDiagnostic()
{
	if(xarray) { delete xarray; } // delete the TimeArray if allocated
}

void TimeDiagnostic::setdefaults()
{
	nnyyarr = 0;
	type = TIMEDIAGNOSTICTYPE;
	td_type = TD_UNDEFINED;
	nnum = NSTEPS;
	tpointer = 0;
	xarray = 0;
	splist = 0;
	spintfunc = 0;
	scalefactor = 1.;
}

void TimeDiagnostic::allocate_arrays(int number)
{
	allocate_tarray();
	int addit = 0;
	if(sum) { addit++; }
	allocate_yarrays(number+addit);
}

void TimeDiagnostic::allocate_tarray()
{
	xarray = new TimeArray(nnum);
	tarray = (TimeArray *)xarray;
}

// Added assigning species names to series if species are defined.
// The name 'total' is added if sum in selected.
// JK 2019-01-17
void TimeDiagnostic::allocate_yarrays(int number)
{
	ostring arrayname;

	// add all arrays first...
	yarray.deleteAll();

	for(int i=0; i < number; i++) {
		arrayname = i;
		yarray.add(new VariableArray(arrayname, nnum));
		plottype.add(new int(CURVE));
	}

	// then allocate their names in the same order as the series will be updated later
	// to make sure series names are correct (they are also written into TXt out files)
	oopicListIter<DataArray> thei(yarray);
	oopicListIter<Species>   thes;
	ostring coordNames[3] = { " -X (0)", " -Y (1)", " -Z (2)" };
	int i, j;

	if(splist) {
		// add species list to yarrays
		thes.restart(*splist);

		for(thei.restart(), i=0, j=0; !thei.Done(); thei++, i++) {
			// loop over y-array in the same order as later update will do
			if( sum && i == yarray.nItems()-1 ){
				// name the first series as 'total'
				arrayname = thei()->getName() + " Total";
				thei()->setName(arrayname);
				break;
			}

			switch(td_type)	{
				case SPINTFUNC:
				case SPSCAFUNC:
				case SCALARRAY:
					// add species name
					arrayname = thei()->getName() + " " + thes()->get_name();
					thei()->setName(arrayname);
					thes++;
					break;
				case SPVECFUNC:
					// vector; add species name and coordinate
					arrayname = thei()->getName() + " " + thes()->get_name() + coordNames[j];
					thei()->setName(arrayname);
					j++;

					if(j == nvcomp)	{
						// next y-array (= species) and reset coordinate
						thes++;
						j = 0;
					}
					break;
				default:
					// ignore last types to keep just numbers as series names
					break;
			}
		}
	} else if( sum ){
		// no species and summing; add total label
		for(thei.restart(), i=0; !thei.Done(); thei++, i++) {
			// loop over y-array in the same order as later update will do
			if( i == yarray.nItems()-1 ){
				// name the first series as 'total'
				arrayname = thei()->getName() + " Total";
				thei()->setName(arrayname);
				break;
			}
		}
	}
}

void TimeDiagnostic::update()
{
	int i, j;
	VariableArray *tvar;
	Scalar tmp;
	Vector3 tmpvec;
	Scalar total = 0.;

	tarray->update(*tpointer);

	// trying to figure out why the time array gets messed up, JH, Aug. 18, 2005
#ifdef DEBUG
	fprintf(stderr, "Timediagnostic, name=%s, time=%g\n",
			name(), *tpointer);
#endif

	oopicListIter<DataArray> thei(yarray);
	oopicListIter<Species>   thes;

	if(splist)
	{
		thes.restart(*splist);
	}

	for(thei.restart(), i=0, j=0; !thei.Done(); thei++, i++)
	{
		tvar = (VariableArray *)thei();

		if(sum)
		{
			if(i == yarray.nItems() -1)
			{
				tvar->update(total);
				break;
			}
		}

		switch(td_type)
		{
		case SPINTFUNC:
			tmp = ((*thes()).*spintfunc)();
			thes++;
			break;
		case SPSCAFUNC:
			tmp = ((*thes()).*spscfunc)();
			thes++;
			break;
		case SPVECFUNC:
			if(j==0) { tmpvec = ((*thes()).*spvecfunc)(); }
			j++;
			tmp = tmpvec.e(j);

			if(j == nvcomp)
			{
				thes++;
				j = 0;
			}
			break;
		case SCALARRAY:
			tmp = udarray[i];
			break;
		default:
			fprintf(stderr, "\nERROR: TimeDiagnostic::update():\n");
			fprintf(stderr, " unknown td_type: %d\n", td_type);
			exit(1);
		}

		tmp *= scalefactor;

		tvar->update(tmp);

		total += tmp;
	}

	write_data();
}

void TimeDiagnostic::initialize()
{
	allocate_arrays(nnyyarr);
	initXGrafix();
}

ostring TimeDiagnostic::get_full_name() const
{
	ostring plname = name + srname + "(t)";
	return plname;
}

void TimeDiagnostic::set_ntimesteps(int n) 
{ 
	nnum = n;

	if(xarray) { xarray->resize(nnum); }

	oopicListIter<DataArray> thei(yarray);
	for(thei.restart(); !thei.Done(); thei++)
	{
		thei()->resize(nnum);
	}
}

void TimeDiagnostic::set_ncomb(int n)
{
	VariableArray *thev;

	if(xarray)
	{
		thev = (VariableArray *)xarray;
		thev->set_ncomb(n);
	}
	oopicListIter<DataArray> thei(yarray);
	for(thei.restart(); !thei.Done(); thei++)
	{
		thev = (VariableArray *)thei();
		thev->set_ncomb(n);
	}
}
