/* diagnostics for displaying EEDF's */
#include "main/oopiclist.hpp"
#include "utils/ostring.hpp"
#include "main/pd1.hpp"
#include "main/parse.hpp"
#include "utils/ovector.hpp"
#include "fields/fields.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "main/particlegrouparray.hpp"
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "utils/VectorArray.hpp"
#include "utils/VariableArray.hpp"
#include "diagnostics/diagnostic.hpp"
#include "diagnostics/diagnostic2d.hpp"
#include "diagnostics/diagnostic_parser.hpp"
#include "diagnostics/diagnostic_eedf.hpp"
#include "main/grid.hpp"

//#define DEBUG_EEDF	1
//#include "main/grid_inline.hpp"
//#include "main/boundary_inline.hpp"

DiagnosticEEDF::DiagnosticEEDF(int _n, Species * _thes)
{
	setdefaults();
	nsamppart = _n;
	speciesList.push_back(_thes);
}

DiagnosticEEDF::DiagnosticEEDF(Species * _thes)
{
	setdefaults();
	speciesList.push_back(_thes);
}

DiagnosticEEDF::DiagnosticEEDF()
{
	setdefaults();
}

/*
 * Define and initialize EEDF diagnostics without given species.
 */
DiagnosticEEDF::DiagnosticEEDF(int _eV, int _eV_update, bool _eepf)
{
	setdefaults();
	num_energybins = _eV;
	update_interval = _eV_update;
	eepf = _eepf;
}
/*
 * Define and initialize EEDF diagnostics with diagnostics parser.
 */
DiagnosticEEDF::DiagnosticEEDF(DiagnosticParser *in_thep, Grid *thegrid, bool _eepf)
{
	setdefaults();
	num_energybins = in_thep->get_eedf_energybins();
	update_interval = in_thep->get_eedf_update_interval();
	eepf = _eepf;

	j0 = in_thep->get_eedf_j0();
	j1 = in_thep->get_eedf_j1();

	if( in_thep->get_Emin() != -1 || in_thep->get_Emax() != -1 ){
		// use defined energy limits for EEDF/EEPF
		energy_min = in_thep->get_Emin();
		energy_max = in_thep->get_Emax();
		autoEnergyLimits = false;
	}

	if( in_thep->get_eedf_jbins() > 1 ) {
		// define intervals in position JK 2018-01-27
		pos_bins = in_thep->get_eedf_jbins();
		pos_xmin = thegrid->getX(j0);
		pos_xmax = thegrid->getX(j1);
		pos_dx = (pos_xmax-pos_xmin)/pos_bins;
	}

	ostring fname = in_thep->get_file();
	printf("File: %s\n", fname.c_str());
	// setup parameters for writing into file
	set_write_params(in_thep->get_file(), in_thep->get_write_start(), in_thep->get_write_step(), in_thep->get_write_end());
	// set limits for computing EEDF
	set_x_limits(thegrid);

	printf("%s: bins: %d, update_interval: %d, j0=%d, j1=%d bins: %d\n", _eepf ? "EEPF":"EEDF", num_energybins, update_interval, j0, j1, pos_bins);
}


DiagnosticEEDF::~DiagnosticEEDF()
{
	deallocate_arrays();
}

void DiagnosticEEDF::setdefaults()
{
	type = EEDFDIAGNOSTICTYPE;
	speciesList.clear();
	speciesList.reserve(10);		// init to 10 species; should be enough for most cases
	nsamppart = SAMPLESIZE;
	stridesize = STRIDESIZE;
	linecolor = FILLCOLOR;
	fillcolor = FILLCOLOR;
	display_material = false;
	v_sep_plots = false;		// only one plot

	if( eepf ) {
		name = ostring("EEPF");
	} else {
		name = ostring("EEDF");
	}

	// define resolution in energy
	num_energybins = 100;			// always have 100 intervals
	// min and max initial energies
	energy_min = 0.0;			// EEDF from 0
	energy_max = 50.0;			// max energy: 50eV
	// update vars
	update_interval = 1;		// update EEDF every call to update()
	update_skip_count = 0;		// number of skipped updates
	countArray = NULL;			// array for counting
	evaluatedOnce = false;		// not evaluated at least once
	eepf = true;				// by default compute EEPF
	j0 = j1 = 0;				// initial grid cell interval to use for EEDF
	autoEnergyLimits = true;	// by default, automatically define min/max energy for EEDF
	pos_bins = 1;				// by default we have only one interval
	pos_xmin = pos_xmax = pos_dx = 0;
	bins_countArray.clear();
	bins_data.clear();

	// initialize variables for XGrafix
	// (overides defaults in Diagnostic2d)
	sprintf(scaletype, "linlog");			// initialize plot in linlog: x=linear, y=logarithmic scale
}

/*
 * Set number of intervals to use in sampling of EEDF in energy level.
 */
void DiagnosticEEDF::set_num_energybins(int _intervals) {
	num_energybins = _intervals;

	deallocate_arrays();
	allocate_arrays();
}

void DiagnosticEEDF::initialize()
{
	allocate_arrays();
	initXGrafix();
}


ostring DiagnosticEEDF::get_full_name() const
{
	ostring nBins = pos_bins > 1 ? ", "+ostring(pos_bins)+" intervals" : "";

	if( eepf ) {
		return ostring("EEPF ("+ostring(j0)+","+ostring(j1)+nBins+")");
	} else {
		return ostring("EEDF ("+ostring(j0)+","+ostring(j1)+nBins+")");
	}
}

/*
 * Allocate arrays for EEDF. With 1V, display one EEDF, for 3V, display 1 in each direction
 */
void DiagnosticEEDF::allocate_arrays()
{
	if(speciesList.size() == 0) {
		// exit if no species is defined
		fprintf(stderr, "DiagnosticEEDF::allocate_arrays() -- speciesList == 0\n");
		exit(1);
	}

	if( !countArray ) {
		// allocate array for counting particles
		countArray = new Scalar[num_energybins];
	}

	if(!xarray) {
		// define x-axis data
		xarray = new DataArray(ostring("energy_eV"), num_energybins);
		eV_sqrt = new DataArray(ostring("sqrt_energy_eV"), num_energybins);
		deV = (energy_max-energy_min)/num_energybins;

		for( int j=0 ; j < num_energybins ; j++) {
			// initialize the array
			xarray->setValue( energy_min + (j+0.5)*deV, j);
			eV_sqrt->setValue(sqrt(xarray->getValue(j)), j);
		}
	}

	if(yarray.isEmpty() ) {
		// add also yarray and it's name
		yarray.add(new DataArray(ostring("V"), num_energybins, 0));
	}

	if( pos_bins > 1 || bins_countArray.size() != (unsigned) pos_bins ) {
		// prepare arrays for individual intervals;
		bins_countArray.reserve(pos_bins);
		bins_data.reserve(pos_bins);

		for( int i=0 ; i < pos_bins ; i++) {
			// allocate space for all energy intervals for each interval
			bins_countArray[i] = new Scalar[num_energybins];
			bins_data[i] = new Scalar[num_energybins];
		}
	}
}

void DiagnosticEEDF::deallocate_arrays() {
	if( bins_countArray.size() > 0 ) {
		// free memory for individual array + main container
		for( int i=0 ; i < pos_bins ; i++) {
			// allocate space for all energy intervals for each interval
			delete[] bins_countArray[i];
			delete[] bins_data[i];
		}

		bins_countArray.clear();
		bins_data.clear();
	}

	if(countArray) {
		delete[] countArray;
		countArray = NULL;
	}

	if(xarray) {
		delete xarray;
		delete eV_sqrt;
	}

	yarray.deleteAll();
	speciesList.clear();
}

void DiagnosticEEDF::update()
{
	if( evaluatedOnce && (update_interval == 0 || update_skip_count < update_interval-1) ) {
		// skip update if it is evaluated at least once or we do not have to do the update yet
		update_skip_count ++;
		return;
	}

	Scalar total_part = 0;
	int ypos;
	Scalar currMaxV_eV = 0.0, cEneV;
	Scalar bins_total_part[pos_bins];
	vector<Species *>::iterator spIter;

	if( autoEnergyLimits == true ) {
		// auto defining limits for energy scale
		for( spIter = speciesList.begin() ; spIter != speciesList.end(); ++spIter ){
			// loop over all defined species to get total number of particles and max velocity
			total_part += (*spIter)->number_particles();
			ParticleGroupList * tpgl = (*spIter)->get_ParticleGroupList();
			oopicListIter<ParticleGroup> thei(*tpgl);

			for( int j=0 ; j < (*spIter)->number_particles(); j++) {
				// count+check all particles
				if( is_inside_limits( thei()->get_x(j) ) ) {
					// particle is inside limits for diagnostics
					currMaxV_eV = std::max(currMaxV_eV, thei()->get_energy_eV(j));
				}
			}
		}

		if( currMaxV_eV > energy_max || currMaxV_eV < 0.5*energy_max ) {
			// new max velocity is greater than current one or is less than half of current one (plot will be empty); resize arrays
			energy_max = 1.1*currMaxV_eV;
			deV = (energy_max-energy_min)/num_energybins;

			for( int j=0 ; j < num_energybins ; j++) {
				// initialize the array with new values for xAxis = energy
				xarray->setValue( energy_min + (j+0.5)*deV, j);
				eV_sqrt->setValue(sqrt(xarray->getValue(j)), j);
			}
		}
	}

	// initialize array for counting
	memset(countArray, 0, num_energybins*sizeof(Scalar));

	if( pos_bins > 1 ){
		// initialize array for counting
		memset(bins_total_part, 0, pos_bins*sizeof(Scalar));

		for( int i=0 ; i < pos_bins ; i++ ) {
			memset(bins_countArray[i], 0, num_energybins*sizeof(Scalar));
		}
	}

	// go back to the first data array
	oopicListIter<DataArray> they(yarray);
	they.restart();

	for( spIter = speciesList.begin() ; spIter != speciesList.end(); ++spIter ){
		// loop over all defined species to get energy distribution
		ParticleGroupList * tpgl = (*spIter)->get_ParticleGroupList();
		oopicListIter<ParticleGroup> thei(*tpgl);

		for( int j=0 ; j < (*spIter)->number_particles(); j++) {
			// count+check all particles
			if( is_inside_limits( thei()->get_x(j) ) ) {
				// particle is inside limits for diagnostics
				cEneV = thei()->get_energy_eV(j);

				if(cEneV >= energy_min && cEneV < energy_max) {
					// below given max energy
					ypos = (int) ((cEneV-energy_min)/(energy_max-energy_min)*(num_energybins-1));
					countArray[ypos] += thei()->get_w(j);
					total_part += thei()->get_w(j);

					if( pos_bins > 1 ) {
						// put particle into right position bin
						Scalar x = thei()->get_xMKS(j);
						int curr_bin = (int) ((x-pos_xmin)/pos_dx);
						bins_countArray[curr_bin][ypos] += thei()->get_w(j);
						bins_total_part[curr_bin] += thei()->get_w(j);
					}

				}
			}
		}
	}

	if ( eepf ) {
		// compute EEPF
		for( int j=0 ; j < num_energybins ; j++) {
			// normalize and save into final yarray
			they()->setValue( (Scalar)countArray[j]/((Scalar)total_part * eV_sqrt->getValue(j) * deV), j);
		}

		for( int i=0 ; i < pos_bins ; i++ ) {
			for( int j=0 ; j < num_energybins ; j++) {
				// normalize and save into final yarray
				bins_data[i][j] = (Scalar)bins_countArray[i][j]/((Scalar)total_part * eV_sqrt->getValue(j) * deV);
			}
		}

#ifdef DEBUG_EEDF
		Scalar sum=0;
		for( int j=0 ; j < num_energybins ; j++) {
			// normalize and save into final yarray
			sum += they()->getValue(j) * eV_sqrt->getValue(j) * deV;
		}
		printf("EEPF: update complete, total f_d()=%g, n_tot=%d\n", sum, total_part);
#endif

	} else {
		// compute EEDF
		for( int j=0 ; j < num_energybins ; j++) {
			// normalize and save into final yarray
			they()->setValue( (Scalar)countArray[j]/((Scalar)total_part * deV), j);
		}

		for( int i=0 ; i < pos_bins ; i++ ) {
			if( bins_total_part[i] > 0 ) {
				for( int j=0 ; j < num_energybins ; j++) {
					// normalize and save into final arrays
					bins_data[i][j] = (Scalar)bins_countArray[i][j]/((Scalar)bins_total_part[i] * deV);
				}
			} else {
				// no particles, not EEDF
				memset(bins_data[i], 0, num_energybins*sizeof(int));
			}
		}

//		printf("total parts: %d, ", total_part);
//
//		for( int i=0 ; i < pos_bins ; i++ ) {
//			printf(" %d", bins_total_part[i]);
//		}
//		printf("\n");

#ifdef DEBUG_EEDF
		Scalar sum=0;
		for( int j=0 ; j < num_energybins ; j++) {
			// normalize and save into final yarray
			sum += they()->getValue(j) * deV;
		}
		printf("EEDF: update complete, total f_d()=%g (is 1?)\n", sum);
#endif
	}

	if( !eepf ) {
		// only write; temporary fix, JK 2018-01-23
		write_data();				// call function to write EEDF to file
	}

	update_skip_count = 0;		// reset counter of skipped updates
	evaluatedOnce = true;
}


void DiagnosticEEDF::initXGrafix()
{
	bool wininit = false;
	ostring plname;
	oopicListIter<DataArray> they(yarray);

	// get xmin, xmax from class
	option.set_xmin(energy_min);
	option.set_xmax(energy_max);
	option.set_xoffset(energy_min);
	// get scaling factors
	option.set_xscale(1.0);
	option.set_yscale(1.0);

	for(they.restart(); !they.Done(); they++)
	{
		if((!wininit))
		{
			openWindow(get_full_name());
			wininit = true;
		}

		// draw curve
		vector<Species *>::iterator spIter = speciesList.begin();
		XGCurve(xarray->get_array(), they()->get_array(), xarray->getSizePointer(), (*spIter)->get_ident());
		// draw only points...
		//XGScat2D(xarray->get_array(), they()->get_array(), xarray->getSizePointer(), (*spIter)->get_ident());
	}
}


int DiagnosticEEDF::get_yarr_dim(DataArray *tyarr)
{
	return tyarr->label;
}

/*
 * Define min and max position between which we compute EEDF.
 */
void DiagnosticEEDF::set_x_limits(Grid *thegrid) {
	lim_j0 = j0;
	lim_j1 = j1;

	if( j1 == thegrid->getnc() ) {
		// whole interval; we increase end cell beyond cell index so condition pos < xmax will include also the end point of the grid
		lim_j1 ++;
	}
//	printf("x limits: %d -> %d\n", j0, j1);
}

/*
 * Write diagnostics data to file given by file handle: for main EEDF and for individual intervals, if they are defined.
 * JK, 2018-01-27
 */
void DiagnosticEEDF::output(FILE *fp)
{
	int i;
	oopicListIter<DataArray> thei(yarray);
	Scalar *ydata[yarray.nItems()];

	for(thei.restart(), i=0; !thei.Done(); thei++, i++) {
		// create an array of pointers to data arrays;
		ydata[i] = thei()->get_array();
	}

	// write header of the file

	fprintf(fp, "# t=%.9e; row content: i xarray[i] yarray[i] %d-interval-columns\n", *time, pos_bins);


	if( pos_bins > 1 ) {
		fprintf(fp, "# dx: %g ; interval x mid-positions: ", pos_dx);

		for( int j=0 ; j < pos_bins ; j++) {
			// write all data from bins
			fprintf(fp, " %g", pos_xmin + (j+0.5)*pos_dx);
		}

		fprintf(fp, "\n");
	}

	for( int i=0 ; i < xarray->getSize() ; i++ ){
		// write the data: 1st col is xarr, other columns are yarrs
		fprintf(fp, "%d %g", i, xarray->getValue(i));

		for( int j=0 ; j < yarray.nItems() ; j++) {
			// write all Y-data
			fprintf(fp, " %g", ydata[j][i]);
		}

		if( pos_bins > 1 ) {
			for( int j=0 ; j < pos_bins ; j++) {
				// write all data from bins
				fprintf(fp, " %g", bins_data[j][i]);
			}
		}

		fprintf(fp, "\n");
	}
}

