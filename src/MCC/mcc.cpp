// mindgame: restructuring, cleanup, and bug fixes (Dec. 9, 2006)
//           to replace mcc.cpp eventually
// TODO: flexible for various energy sharing in ionization event
//       flexible for various differential xsection: isotropic and anisotropic
//     (surendra or okhrimovsky)
// JTG and MAL melded file, 10/12/09
// corrected JTG miss-copied error in MCC::eExcitation, Scalar temp =; it was missing the correction, MAL 10/13/09

#include <stdio.h>
#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/grid.hpp" // add, MAL 12/30/10
#include "main/particlegroup.hpp"
#include "reactions/reaction.hpp"
// data for reaction diagnostics
#include "utils/Array.hpp"
#include "utils/DataArray.hpp"
#include "MCC/mcc.hpp"
#include "atomic_properties/fluid.hpp"
#include "atomic_properties/diffusion_fluid.hpp" // add, MAL 12/30/10
#include "xsections/xsection.hpp"
#include "fields/Base_Class_Direct_Field.hpp"
#include "main/ostack.hpp"
#include "distributions/dist_v.hpp"

//MCC::MCC(SpeciesList* slist, ReactionList _reactionlist, Scalar _dt, Fields *tf)
//{
//	specieslist = slist;
//	dt = _dt;
//	thefield = tf;
//	setdefaults();
//	reactionlist = _reactionlist;
//	collDistrib = NULL;
//}

/*
 * New initialization of MCC that includes allocating space for storing information about reactions.
 * JK, DQW, 2019-11-25
 */
MCC::MCC(SpeciesList* slist, ReactionList _reactionlist, Scalar _dt, Fields *tf)
{
	specieslist = slist;
	dt = _dt;
	thefield = tf;
	setdefaults();
	reactionlist = _reactionlist;
	collect_4_diag = false;
}

void MCC::setdefaults_mcc()
{
	extra = 0;
	maxSigmaV = 0;
	eSpecies = NULL;
	nSpecies = NULL;
	iSpecies = NULL;
	fSpecies = NULL; // initialize fluid species used in  fluid_particle, MAL200211
	is_fluidfluid = false; // to detect fluid_fluid for add_particles, MAL200215
	subcycle_ff = 100; // default value of subcycle for fluid_fluid
	sec_e = NULL;
	sec_i = NULL;
	sec_n = NULL; // add MAL 11/15/10
	// mindgame: the better way is to get nc2p from the inputfile
	useSecDefaultWeight = false; // default value is false, MAL 10/13/09
	maxSigmaV = 0; // tested and initialized to a value > 0 by a call to init_sumSigmaV(sp *) in ReactionGroup::update, MAL200117

	if( collect_4_diag ) {
		// initialize counters for collision/reaction distributions
		init_collision_distribution();
	}
}


/*
 * Allocate arrays for diagnostics.
 * JK, 2019-11-27
 */
void MCC::allocate_diagostics_arrays(void) {

	if( collect_4_diag ) {
		// allocate vector of reactions
		collDistrib.reserve(reactionlist.num());

		for( int i=0 ; i < reactionlist.num() ; i++) {
			// allocate data arrays for diagnostics of collisions for all reactions in the system
			collDistrib.push_back(new DataArray("K: "+reactionlist.data(i)->get_chemical_equation_short(0), thefield->get_ng()));
		}

		// initialize counters for collision/reaction distributions
		init_collision_distribution();
	}
}


void MCC::init_mcc(Species *_reactant1, Species *_reactant2)
{
	totNcoll = 0; // for icp, MAL 1/8/11
	totNcoll_old = 0;
	tempNcoll = 0.0;
	aveNcoll_per_step = 0.0;
	Ncoll = 0.0;
	sortflag = false;		// JK, 2020-01-07; no sorting done by default (probably reason why sometimes code did seg.fault at the begining)
	// printf("\nreac1=%s (%p), reac2=%s (%p)\n", _reactant1->get_name().c_str(), (void*)_reactant1, _reactant2->get_name().c_str(), (void*)_reactant2);

	// printf("\nreac1=%s, reac2=%s\n", _reactant1->get_name().c_str(), _reactant2->get_name().c_str()); // for debug

	if (is_electron(_reactant1))
		eSpecies = _reactant1;
	else if (is_electron(_reactant2))
		eSpecies = _reactant2;
	if (is_neutral(_reactant1))
		nSpecies = _reactant1;
	else if (is_neutral(_reactant2))
		nSpecies = _reactant2;
	if (is_ion(_reactant1)){
		iSpecies = _reactant1;
	}
	else if (is_ion(_reactant2))
		iSpecies = _reactant2;
	//for ion-ion or neutral-neutral collisions, MAL 4/17/09
	if ((is_ion(_reactant1) && is_ion(_reactant2)) || (is_neutral(_reactant1) && is_neutral(_reactant2)))
	{
		if (is_before(_reactant1, _reactant2))
		{
			iSpecies=_reactant1; //consider the former species in ReactionSpeciesType to be the ion, MAL 4/17/09
			nSpecies=_reactant2; //consider the latter species in ReactionSpeciesType to be the neutral
		}
		else if (is_after(_reactant1, _reactant2))
		{
			iSpecies=_reactant2; //consider the former species in ReactionSpeciesType to be the ion
			nSpecies=_reactant1; //consider the latter species in ReactionSpeciesType to be the neutral
		}
		else if (is_thesame(_reactant1, _reactant2)) // add if statement for like-particle collisions, MAL 11/23/09
		{
			iSpecies=_reactant1;
			nSpecies=_reactant2;
		}
	}
  
	// set subcycle_ff for the fluid_fluid collisions to be the smallest subcycle defined for all evolving
	// fluids; the default value is subcycle_ff = 100; MAL200220
	subcycle_ff = set_subcycle_ff(_reactant1, _reactant2);
  
	for (int i=reactionlist.num()-1; i>=0; i--)
	{
		switch (reactionlist.data(i)->get_reactiontype())
		{
			case E_IONIZATION: // for backward compatibility?  MAL 12/27/10
				{
//					sec_e = reactionlist.data(i)->get_products(0);
//					sec_i = reactionlist.data(i)->get_products(1);
				}
				break;
 //
			default:
				break;
		}
	}
}

// to pick the species with the largest total number of particles to be the "target" species, MAL 10/10/09
void MCC::particle_particle(Species *const _reactant1, Species *const _reactant2)
{
	Scalar Ntot_particles1, Ntot_particles2;
	// Note that init_sumSigmaV is now called with _reactant1 (really, reactant[0]) in ReactionGroup::update; MAL200117
	
	if ((Ntot_particles1=_reactant1->get_Ntot_particles())
			> (Ntot_particles2=_reactant2->get_Ntot_particles()))
	{
		particle_particle(_reactant1, _reactant2, Ntot_particles2);
	}
	else
	{
		particle_particle(_reactant2, _reactant1, Ntot_particles1);
	}
	//		printf("\tparticle_particle: R1=%s, Ntot1=%g, R2=%s, Ntot2=%g\n",_reactant1->get_name().c_str(),Ntot_particles1,
	//		_reactant2->get_name().c_str(),Ntot_particles2);
}

void MCC::particle_particle (Species *const targetspecies, Species *const projspecies, Scalar Ntot_particles)
{
	//	printf("\nMCC::particle_particle: projectile=%p (%s), target=%p (%s), Ntot_particles=%g\n",
	//		(void*)projspecies, projspecies->get_name().c_str(), (void*)targetspecies, targetspecies->get_name().c_str(), Ntot_particles);
	// REMOVE Christine Nguyen algorithm (see below), and replace with simpler (correct) algorithm, MAL 10/10/09
	// ADD like-particle neutral-neutral or ion-ion scattering, MAL 11/24/09
	// In this function, target properties are recognized by the word "target",
	// whereas projectile properties are either recognized by "proj", or no indication (index, u ...)
	// Identification of the type of the colliding particles
	// Move the statement below into particle_particle((Species *const _reactant1, Species *const _reactant2)) and always call init_sumSigmaV with _reactant1; MAL200113
	//if (maxSigmaV == 0.) init_sumSigmaV(projspecies); // initialize maxSigmaV here, as init_sumSigmaV now initializes the radiation escape factor g, MAL191219, MAL200103

	fSpecies = NULL; // there is no fluid species for particle_particle, MAL200211
	is_fluidfluid = false; // not fluid_fluid, MAL200215
	Vector3* u_target;
	Vector3* u;

	// treat both target and projectile species velocities together to accomodate ion-ion and neutral-neutral collisions, MAL 4/17/09
	set_velocity_pointers(&u_target, targetspecies, &u, projspecies); //add last two arguments and delete next line, MAL 4/17/09
	//set_velocity_pointers(&u, projspecies); MAL 4/17/09

	// Other initializations:
	ParticleGroupList *pgList_target=targetspecies->get_ParticleGroupList();
	oopicListIter<ParticleGroup> target_pgIter(*pgList_target);
	ParticleGroup * pga_target;
	ParticleGroupList* pgList = projspecies->get_ParticleGroupList();
	oopicListIter<ParticleGroup> pgIter (*pgList);
	int np = projspecies->number_particles();
	bool unlike_species = !is_thesame(targetspecies, projspecies); // for like-particle collision control, MAL 11/24/09
	// int minsubcycle = MIN(projspecies->get_subcycle(), targetspecies->get_subcycle()); // add for subcycle, MAL 12/31/09
	int minsubcycle = 1; // TO KILL SUBCYCLING IN THE MCC, MAL 8/12/12
	int nsteps = thefield -> get_nsteps(); // also used to debug too-large electron energies, MAL200112
	if (nsteps % minsubcycle != 0) return; // add for subcycle, MAL 12/31/09

	// Null Collision Probability:
	Scalar* target_density  = targetspecies->get_species_density();
	Scalar maxdensity = 0.0;
	for (int i = 0; i < thefield->get_ng(); i++)
	{
		maxdensity = MAX(maxdensity, target_density[i]);
	}
	// Scalar collprob = 1.  - exp(-maxdensity * maxSigmaV[id] * dt * projspecies.get_subcycle()); // commented out in the original code, MAL 10/10/09
	//  Scalar collprob = maxdensity * maxSigmaV * dt * projspecies->get_subcycle(); // remove, add next line for subcycle, MAL 12/31/09
	// Scalar collprob = maxdensity * maxSigmaV * dt * minsubcycle; // add for subcycle, MAL 12/31/09
	//		printf("\nMCC::particle_particle: maxdensity=%g, maxSigmaV=%g, dt=%g, minsubcycle=%i\n",maxdensity,maxSigmaV,dt,minsubcycle);
	Scalar collprob = 1.  - exp(-maxdensity * maxSigmaV * dt * minsubcycle); // TRY THIS FOR SUBCYCLE, MAL 8/14/12
	//		printf("\tparticle_particle: projsp=%s, targetsp=%s, maxSigmaV=%g, maxdensity=%g, collprob = %g\n", projspecies->get_name().c_str(),
	//			targetspecies->get_name().c_str(), maxSigmaV, maxdensity, collprob); // for debug, MAL 8/29/12
	if (collprob < 1e-20) return;
	extra += collprob*Ntot_particles;
	Scalar nCollision = extra;

	/* MAL, 10/10/09
	   This algorithm introduced by Christine Nguyen, HAS BEEN REMOVED. It was used to define a reference weight w_ref = w_proj*lambda for which to add
	   new particles (and particle groups), but it is exponentially unstable, as it generates particles (and particle groups) with smaller and smaller weights
	   when w_target(min)/w_proj(max) < 1.  Here w_proj is the weight of the projectile selected for the collision,
	   w_target(min) is the smallest target weight found, and w_proj(max) is the largest projectile
	   weight found, in the scan over all target and projectile particles (whatever their particle group). MAL 10/10/09

	// For collisions of particles of different weight, with variable weights:
	Scalar lambda = 1 ; //min(1, wt(min)/wi(max))
	Scalar proj_maxw = 0.0;
	for (pgIter.restart(); !pgIter.Done(); pgIter++)
	{
	// Note: the target is not really randomly chosen
	for (int j =0; j < pgIter()->get_n(); j++)
	{
	Scalar thew = pgIter()->get_w(j);
	if  (thew > proj_maxw)  proj_maxw = thew;
	}
	}
	Scalar target_minw = 1.1e30;
	for (target_pgIter.restart(); !target_pgIter.Done(); target_pgIter++)
	{
	// Note: the target is not really randomly chosen
	for (int j =0; j < target_pgIter()->get_n(); j++)
	{
	Scalar thew = target_pgIter()->get_w(j);
	if  (thew < target_minw)  target_minw = thew;
	}
	}
	Scalar minovermax;
	if (!proj_maxw) return;
	minovermax = target_minw/proj_maxw;
	if (minovermax<1.0) lambda = minovermax;
	END REMOVAL, MAL 10/10/09 */

	//qsort(targetspecies); // commented out in the original code, MAL 10/10/09
	//indexsort(sortKeys);

	// Collision Process
	// To avoid infinite loop when no target particle matching a projectile is found, MAL 12/28/09
	int max_tries = 0;
	if(np > 0) max_tries = (int)(np*log(1000.*np)); // Set probability of not choosing a particular projectile = 1/(1000*np)
	//	printf("\tparticle_particle: extra=%g, nCollision=%g, nproj=%i, Ntot_particles=%g, max_tries=%i\n",extra,nCollision,np,Ntot_particles,max_tries);
	//	if(np > 0) max_tries = np; // Set max_tries=np
	while(nCollision > 0 && np > 0 && max_tries > 0)
	{
		max_tries--; // to avoid infinite loop, MAL 12/28/09
		// Choice of particles colliding together
		// The projectile:
		int index = (int)(np * frand());
		for (pgIter.restart(); !pgIter.Done(); pgIter++)
		{
			if (index < pgIter()->get_n())  break;
			else index -= (int) pgIter()->get_n();
		}
		ParticleGroup *pga = pgIter();
		x = pga -> get_x(index);
		//			printf("\t\tparticle_particle: x=%g\n",x);
		*u =  pga -> get_v(index)/pga->get_gamma(index);
		Scalar w_proj = pga -> get_w(index);
		//      w_ref = w_proj*lambda; // see comment above, this calculation of w_ref is exponentially unstable, MAL 10/10/09

		//For sorting:
		int index_of_target;
		if(sortflag)
		{
			index_of_target = (int)((indexsorts[(int)x+1]-indexsorts[(int)x])*frand());
			*u_target = sortKeys[index_of_target].vel();
		}

		// The target:
		pga_target = NULL;
		int index_target;
		for (target_pgIter.restart(); !target_pgIter.Done() && !pga_target; target_pgIter++)
		{
			// Note: the target is not really randomly chosen
			bool unlike_pga = !(target_pgIter() == pga); // for like-particle collision control, MAL 11/24/09
			for (int j =0; j < target_pgIter()->get_n(); j++)
			{
				Scalar xtarget = target_pgIter()->get_x(j); // choose first target to make |x-xtarget| <= 1, MAL 9/23/12
				//	      int k = (int)target_pgIter()->get_x(j);
				bool unlike_index = !(j == index); // for like-particle collision control, MAL 11/24/09
				bool not_same_particle = unlike_species || unlike_pga || unlike_index; // for like-particle collisions, true if projectile and target are different particles, MAL 11/24/09
				//	      if (((int)x == k || (int)x -1 == k) && not_same_particle) // for like-particle collisions, make sure projectile and target are different, MAL 11/24/09
				//	      if ((int)x == k && not_same_particle) // for like-particle collisions, make sure projectile and target are different, MAL 11/24/09, REVISE FROM ABOVE, MAL 8/20/12
				if (fabs(x-xtarget) <= 1.0 && not_same_particle) // make |x-xtarget|<=1 and make sure projectile and target are different, MAL 11/24/09, 9/23/12
				{
					index_target = j;
					pga_target = target_pgIter();
					*u_target =  pga_target -> get_v(index_target)/pga_target->get_gamma(index_target);
					break;
				}
			}
		}
		//			printf("max_tries=%i, nCollision=%g, pga_target=%p\n", max_tries, nCollision, (void*)pga_target); // DEBUG, MAL 10/10/09

		if (pga_target!=NULL)
		{
			v = (*u-*u_target).magnitude()+1.e-30;
			Scalar temp = pga->get_vMKS(v);
			energy = reactant_for_sigmaV->get_energy2_eV(temp);

			// Test for possible out-of-order reactants in a reaction group, leading to unphysically large energies, MAL200117
#ifdef DEBUGmccdynamic
			if( energy > 5000 ) {
				ostring rsigmaVname = reactant_for_sigmaV->get_name();
				ostring projname = projspecies->get_name();
				ostring targetname = targetspecies->get_name();
				printf("\n\n");
				printf(" nsteps=%i\n",nsteps);
				printf("- targetname=%s\n", targetname.c_str());
				printf("  reactant_for_sigmaV=%s\n",reactant_for_sigmaV->get_name().c_str());
				printf("  target index=%d (target particle index)\n", index_target);
				printf("  target: x=%g, v=(%g, %g, %g)\n", pga_target->get_x(index_target), u_target->e1(), u_target->e2(), u_target->e3());
				printf("- projname=%s\n", projname.c_str());
				printf("  index=%d (projectile particle index)\n", index);
				printf("  projectile: x=%g, v=(%g, %g, %g)\n", x, u->e1(), u->e2(), u->e3());
				printf("- v=%g, temp=%g, energy=%g\n", v, temp, energy);
				printf("  max_tries=%i, nCollision=%g, pga_target=%p\n", max_tries, nCollision, (void*)pga_target);			}
#endif

			// mindgame: now temp is n*v
			temp *= target_density[(int)x];
			Scalar random = frand()*maxdensity*maxSigmaV;
			Scalar sumnSigmaV = 0;

			for (int i=0; i< reactionlist.num(); i++)
			{
				sumnSigmaV += get_crosssection(reactionlist.data(i), energy)*temp;
/*
				printf("\nMCC::particle_particle: sumnSigmaV=%g, random=%g, energy=%g, temp=%g\n", sumnSigmaV, random, energy, temp);
				printf("\n  reactionlist.num=%i, reactionlist.data=%p, get_xsection=%p, sigma=%g, reactiontypename=%s, is_xsection_empty=%i, is_empty=%i\n",
					i, (void*)reactionlist.data(i), (void*)reactionlist.data(i)->get_xsection(),
					get_crosssection(reactionlist.data(i), energy), reactionlist.data(i)->get_reactiontypename().c_str(), reactionlist.data()->is_xsection_empty(),
					reactionlist.data()->is_empty());
				printf("\n  reactant0=%s, reactant1=%s\n", (reactionlist.data(i)->get_reactants())[0]->get_name().c_str(),
					(reactionlist.data(i)->get_reactants())[1]->get_name().c_str());
*/
				// mindgame: comparison between sum of n*v*Sigma/(maxdensity*maxSigmaV)
				if (random <= sumnSigmaV) // A collision is realized
				{
					//		  if (!unlike_species) ("Like-particle, particle-particle collision is realized\n"); // FOR DEBUG, MAL 11/24/09
					//	BUG FIX: moved the next two lines from below to  above the call to dynamic(), as w_ref is used for some reactions called by dynamic(); MAL 12/1/10
					//	This prevents a call to add_particles() with w_ref=0, which in turn erroneously generates a new particle group
					Scalar w_target = pga_target->get_w(index_target);
					w_ref = w_proj; if (w_target < w_proj) w_ref = w_target; // w_ref is the smaller of w_proj and w_target, MAL 10/10/09
					// dynamic(*(reactionlist.data(i)->get_xsection()), reactionlist.data(i)->get_reactiontype()); // remove, MAL 1/3/10
#ifdef DEBUGmccdynamic
					if (reactionlist.data(i)->get_reactiontype() != E_ELASTIC)
					{
						printf("\n\nCalling MCC::dynamic from MCC::particle-particle..."); // E_ELASTIC works fine and so exclude this reaction from the debugging; MAL191214
						printf("\nNumber of projectiles=%i, projectile weight=%g, target weight=%g",np, w_proj, w_target);
					}
#endif
					dynamic(reactionlist.data(i)); // add, MAL 12/27/10

					if(collect_4_diag) {
						// increase the counter for current reaction and particle position; JK 2019-11-27
						// rounding the cell and counting real particles (taking into account np2c for species colliding); JK 2019-12-04
						int icell = (int) x;
						Scalar w = x - icell;
						// add weighted number of left and right grid point of current cell; JK 2019-12-06
						collDistrib[i]->setValue( collDistrib[i]->getValue(icell)+w_ref*(1-w), icell);
						collDistrib[i]->setValue( collDistrib[i]->getValue(icell+1)+w_ref*w, icell+1);
					}

					// The collision effectively occurs for the particle of smaller weight,
					// but with a probability for the other one.
					// Scalar w_target = pga_target->get_w(index_target); See above BUG FIX
					//	w_ref = w_proj; if (w_target < w_proj) w_ref = w_target; // w_ref is the smaller of w_proj and w_target, MAL 10/10/09, see above BUG FIX

					//			ostring projname = projspecies->get_name(); // FOR DEBUG, MAL 10/9/09
					//			ostring targetname = targetspecies->get_name();
					//			printf("\nMCC: part-part collision: proj=%s, target=%s, wref=%g, wproj=%g, wtarg=%g", // FOR DEBUG, MAL 10/10/09
					//				projname.c_str(), targetname.c_str(), w_ref, w_proj, w_target); //END DEBUG, MAL 10/10/09

					Scalar event = frand();
					if(event <= float( w_ref/w_proj))
					{
						// mindgame: awkward way
						if ( (*u) == DELETE_PARTICLE || // COLLIDE - DELETE; add next line to remove low energy neutral PIC particles, MAL 5/5/11
								(projspecies->get_q()==0 && projspecies->get_energy_eV(projspecies->get_dxdt()*(*u)) < projspecies->get_energyThrToAddAsPIC()))
						{
							np --;
							pga->delete_particle(index);
							//				printf("\nparticle_particle: DELETE projectile %s\n", pga->get_species()->get_name().c_str()); // for debug, MAL 12/27/10
						}
						else // COLLIDE - MOVE
						{
							*u *= pga->get_gamma2(*u);
							pga->set_v(index, *u);
						}
					}
					event = frand();
					if(event <= (float) (w_ref/w_target))
					{
						// mindgame: awkward way
						if ( (*u_target) == DELETE_PARTICLE || // COLLIDE - DELETE; add next line to remove low energy neutral PIC particles, MAL 5/5/11
								(targetspecies->get_q()==0 && targetspecies->get_energy_eV(targetspecies->get_dxdt()*(*u_target)) < targetspecies->get_energyThrToAddAsPIC()))
						{
							//				printf("\nparticle_particle: DELETE target %s\n", pga_target->get_species()->get_name().c_str()); // for debug, MAL 12/27/10
							if(sortflag)
							{
								sortKeys[index_of_target].del();
								indexsorts[(int)x]-- ;
							}
							if (!unlike_species) np --; // add if statement; for a like-particle collision, must decrement np also, MAL 11/24/09
							pga_target->delete_particle(index_target);
						}
						else // COLLIDE - MOVE
						{
							if(sortflag) sortKeys[index_of_target].set_v(*u_target);
							*u_target *= pga_target->get_gamma2(*u_target);
							pga_target->set_v(index_target, *u_target);
						}
					}
					break;
				} // end if (random <= sumnSigmaV) // A collision is realized
			} // end for (int i=0; i< reactionlist.num(); i++)
		} // end if (pga_target!=NULL)
		nCollision -= w_ref; // the "effective" number of collisions in the event is w_ref
	}
	if (np ==0) nCollision = 0.0;
	extra = nCollision;
}

//do both species together to handle ion-ion and neutral-neutral collisions, MAL 4/17/09
void MCC::set_velocity_pointers(Vector3 **uone, const Species *const spone, Vector3 **utwo, const Species *const sptwo)
{
	if (is_electron(spone))
		*uone = &ue;
	else if (is_neutral(spone))
		*uone = &un;
	else if (is_ion(spone))
		*uone =  &ui;
	if (is_electron(sptwo))
		*utwo = &ue;
	else if (is_neutral(sptwo))
		*utwo = &un;
	else if (is_ion(sptwo))
		*utwo =  &ui;
	if ((is_ion(spone) && is_ion(sptwo)) || (is_neutral(spone) && is_neutral(sptwo))) //ion-ion or neutral-neutral collisions, MAL 4/17/09
	{
		if (is_before(spone, sptwo))
		{
			*uone=&ui; //un->ui!!! consider the former species in ReactionSpeciesType to be the ion, see init_mcc(), MAL 12/27/10
			*utwo=&un; //ui->un!!! consider the latter species in ReactionSpeciesType to be the neutral, see init_mcc(), MAL 12/27/10
		}
		else if (is_after(spone, sptwo))
		{
			*uone=&un; //ui->un!!! consider the former species in ReactionSpeciesType to be the ion, see init_mcc(), MAL 12/27/10
			*utwo=&ui; //un->ui!!! consider the latter species in ReactionSpeciesType to be the neutral, see init_mcc(), MAL 12/27/10
		}
		else if (is_thesame(spone, sptwo)) // Add this else if statement for like-particle collisions, MAL 11/23/09
		{
			*uone=&ui;
			*utwo=&un;
		}
	}
}

// mindgame: interaction between fluid part of targetspecies
//          and particle part of projspecies.
void MCC::fluid_particle(Species *const fluid_species, Species *const particle_species)
{

//	if (maxSigmaV == 0.) init_sumSigmaV(particle_species); // initialize maxSigmaV here, if needed, as init_sumSigmaV now initializes the radiation escape factor g, MAL191219
	// always call init_sumSigmaV with reactant[0], which is 'fluid_species' in this case; JK 2020-01-13
//	if (maxSigmaV == 0.) init_sumSigmaV(fluid_species); // initialize maxSigmaV here, if needed, as init_sumSigmaV now initializes the radiation escape factor g, MAL191219
	fSpecies = fluid_species; // identify the fluid species, MAL200211
	is_fluidfluid = false; // not fluid_fluid, MAL200215
	// Recognition of the species types
	Vector3 *u_fluid;
	Vector3 *u_particle;
	// treat both target and projectile species velocities together to accomodate ion-ion and neutral-neutral collisions, MAL 4/17/09
	set_velocity_pointers(&u_fluid, fluid_species, &u_particle, particle_species); //add last two args and delete next line, MAL 4/17/09
	//set_velocity_pointers(&u_particle, particle_species); MAL 4/17/09


#ifdef DEBUGmcc
	fprintf(stderr, "\n\nMCC::fluid_particle");
	fprintf(stderr, ": with target->");
	fprintf(stderr, "%s",fluid_species->get_name().c_str());
	fprintf(stderr, ": And projectile->");
	fprintf(stderr, "%s",particle_species->get_name().c_str());
	fprintf(stderr, " \n u = &ue: %d; ", u_particle == &ue);
	fprintf(stderr, " u = &ui: %d; ", u_particle == &ui);
	fprintf(stderr, " u = &un: %d; ", u_particle == &un);
	fprintf(stderr, " u_target = &ue: %d; ", u_fluid == &ue);
	fprintf(stderr, " u_target = &ui: %d; ", u_fluid == &ui);
	fprintf(stderr, " u_target = &un: %d; ", u_fluid == &un);
#endif
//  printf("\nparticle_species=%s, fluid_species=%s, fSpecies==fluid_species=%i\n",particle_species->get_name().c_str(), fluid_species->get_name().c_str(), fluid_species==fSpecies); // for debug, MAL200211
	// Other initializations:
	ParticleGroupList* pgList =  particle_species->get_ParticleGroupList();
	oopicListIter<ParticleGroup> pgIter (*pgList);
	int np = particle_species->number_particles();
	// int minsubcycle = MIN(fluid_species->get_subcycle(), particle_species->get_subcycle()); // add for subcycle, MAL 12/31/09
	int minsubcycle = 1; // TO KILL SUBCYCLING IN THE MCC, MAL 8/12/12
	//	int minsubcycle = particle_species->get_subcycle(); //TRY THIS FOR FLUID-PARTICLE COLLISIONS, MAL 8/14/12
	if (thefield->get_nsteps() % minsubcycle != 0) return; // add for subcycle, MAL 12/31/09
	ostring particlespecies = particle_species->get_name(); // for DEBUG, MAL 9/2/12
	ostring fluidspecies = fluid_species->get_name(); // for DEBUG, MAL 9/7/12
#ifdef DEBUGmcc
	fprintf(stderr, "\n Nsteps=%li, particlespecies=%s, fluidspecies=%s, ng=%i*****\n",thefield->get_nsteps(), particlespecies.c_str(),
			fluidspecies.c_str(),thefield->get_ng());
#endif
	// Null Collision Probability:
	oopicList<Fluid> *target_fList = fluid_species->get_fluidList();
	oopicListIter<Fluid> fIter(*target_fList);
	Scalar maxdensity = 0.0;
	for (int i = 0; i < thefield->get_ng(); i++)
	{
		Scalar f_density = 0.0;
		for (fIter.restart(); !fIter.Done(); fIter ++)
		{
			f_density += fIter()->get_n(i);
			//		printf("\nmcc.cpp::fluid_particle: f_density=%g",f_density);
		}
		maxdensity = MAX(maxdensity, f_density);
	}
	//		printf("\nmcc.cpp::fluid_particle: maxdensity=%g, maxSigmaV=%g, dt=%g, minsubcycle=%i\n",maxdensity,maxSigmaV,dt,minsubcycle);
	// Scalar collprob = 1.  - exp(-maxdensity * maxSigmaV * dt * projspecies->get_subcycle());
	//  Scalar collprob = maxdensity * maxSigmaV * dt * particle_species->get_subcycle(); // remove, add next line for subcycle, MAL 12/31/09
	//Scalar collprob = maxdensity * maxSigmaV * dt * minsubcycle; // add for subcycling, MAL 12/31/09
	Scalar collprob = 1.  - exp(-maxdensity * maxSigmaV * dt * minsubcycle); // TRY THIS FOR SUBCYCLE, MAL 8/14/12
	//	if (thefield->get_nsteps() <=1) // for debug, MAL 8/29/12
	//	{
	//		printf("\nMCC::fluid_particle: fluidsp=%s, particlesp=%s, maxSigmaV=%g, maxdensity=%g, collprob = %g\n", fluid_species->get_name().c_str(),
	//			particle_species->get_name().c_str(), maxSigmaV, maxdensity, collprob); // for debug, MAL 8/29/12
	//	}
	// printf("mcc.cpp::fluid_particle: collprob=%g, extra=%g\n",collprob,extra);

	if (collprob < 1e-20) return;
	extra += collprob*particle_species->get_Ntot_particles();
	Scalar nCollision = extra;
	//	printf("mcc.cpp::fluid_particle: extra=%g, nCollision=%g, np=%i\n",extra,nCollision,np);

	while (nCollision > 0.0 && np > 0 )
	{
		// Choice of projectile
		int index = (int)(np * frand());
		for (pgIter.restart(); !pgIter.Done(); pgIter++)
		{
			if (index < pgIter()->get_n()) break;
			else index -= (int)pgIter()->get_n();
		}

		ParticleGroup* pga = pgIter();
		x = pga -> get_x(index);
		*u_particle = pga -> get_v(index)/pga->get_gamma(index);
		w_ref = pga -> get_w(index);

		Scalar random = frand()*maxdensity*maxSigmaV;
		Scalar sumnSigmaV = 0;

		//  Contrary to the particle method, we don't randomly chose one fluid as a target,
		//  but we assess the probability of each fluid for better statistics.
		//  mindgame: why NGP rather than CIC ?
		//  --> C.N.: Yes it would be better, and it shouldn't be hard for fluid-particle collisions.

		for (fIter.restart(); !fIter.Done(); fIter++)
		{
			// mindgame: no need when density zero.
			if (fIter()->get_n((int)x) > 0)
			{
				// mindgame: To avoid critical memory leaks,
				//          use get_v_from_distribution
				*u_fluid = pga->get_vnorm(fIter()->get_v_from_distribution((int)x));

#ifdef DEBUGmcc
				fprintf(stderr, "\n u %g", (*u_particle).magnitude());
				fprintf(stderr, "\n u_target %g", (*u_fluid).magnitude());
#endif

				// mindgame: in general, v is relative speed.
				v = (*u_particle-*u_fluid).magnitude()+1.e-30;
				//            if (particlespecies == "E-") v = (*u_particle).magnitude()+1.e-30; // to compare to xpdp1, electron vel not transformed into target rest frame MAL 9/25/12
				//            else v = (*u_particle-*u_fluid).magnitude()+1.e-30; // to compare to xpdp1, MAL 9/25/12
				Scalar temp = pga->get_vMKS(v);
				energy = reactant_for_sigmaV->get_energy2_eV(temp);
				// mindgame: now temp is n*v
				temp *= fIter()->get_n((int)x);

#ifdef DEBUGmcc
				fprintf (stderr, "\n random: %g ---  FROM energy: %g;   ", random, energy);
				fprintf (stderr, " (*u).e1() %g, (*u_target).e1() %g", (*u_particle).e1(), (*u_fluid).e1());
				fprintf (stderr, "\n\t\t target_density: %g, and n_target*v_mks: %g, ForXsection: ",  fIter()->get_n((int)x), temp);
#endif

				int i;
				for (i=0; i< reactionlist.num(); i++)
				{
					sumnSigmaV += get_crosssection(reactionlist.data(i), energy)*temp;

#ifdef DEBUGmcc
					fprintf (stderr, "\n \t sumSigma: %g --- FROM %s WITH csx: %g; ", sumnSigmaV,
							(reactionlist.data(i)->get_reactiontypename()).c_str(),  get_crosssection(reactionlist.data(i), energy));
#endif

					// mindgame: comparison between sum of n*v*Sigma/(maxdensity*maxSigmaV)
					if (random <= sumnSigmaV)
					{
#ifdef DEBUGmccdynamic
						if (reactionlist.data(i)->get_reactiontype() != E_ELASTIC)
						{
							printf("\n\nCalling MCC::dynamic from MCC::fluid-particle..."); // E_ELASTIC works fine and so exclude this reaction from the debugging; MAL191214
							printf("\nNumber of projectiles=%i, projectile weight=%g",np, w_ref);
						}
#endif
						dynamic(reactionlist.data(i)); // add, MAL 12/27/10
/*
						fprintf(stderr, "Reaction Type: %d - %s; position: %d\n", reactionlist.data(i)->get_reactiontype(),
								ReactionTypeName(reactionlist.data(i)->get_reactiontype()).c_str(), (int)x);
						reactionlist.data(i)->print_chemical_equation(0);
						printf("\n--> %s\n", (reactionlist.data(i)->get_chemical_equation_short(0)).c_str());
*/
						if(collect_4_diag) {
							// increase the counter for current reaction and particle position; JK 2019-11-27
							// rounding the cell and counting real particles (taking into account np2c for species colliding); JK 2019-12-04
							int icell = (int) x;
							Scalar w = x - icell;
							// add weighted number of left and right grid point of current cell; JK 2019-12-06
							collDistrib[i]->setValue( collDistrib[i]->getValue(icell)+w_ref*(1-w), icell);
							collDistrib[i]->setValue( collDistrib[i]->getValue(icell+1)+w_ref*w, icell+1);
						}

//						fprintf(stderr, "\n Reaction Type: %s", ReactionTypeName(reactionlist.data(i)->get_reactiontype()));

#ifdef DEBUGmcc
						fprintf(stderr, "\n Reaction Type: %s", (reactionlist.data(i)->get_reactiontypename()).c_str());
#endif
						// MAY NOT NEED THE NEXT OR STATEMENT ABOUT LOW ENERGY PIC PARTICLES, MAL200211

						// mindgame: awkward way
						if ( *u_particle == DELETE_PARTICLE ||  // add next line to remove low energy neutral PIC particles, MAL 5/5/11
								(particle_species->get_q()==0 && particle_species->get_energy_eV(particle_species->get_dxdt()*(*u_particle)) < particle_species->get_energyThrToAddAsPIC()))
						{
							//				printf("\nfluid_particle: DELETE %s\n", pga->get_species()->get_name().c_str()); //for debug, MAL 12/27/10
#ifdef DEBUGmcc
							fprintf(stderr, "  --------> COLLIDE - DELETE");
#endif
							pga->delete_particle(index);
							np--;
						}
						else
						{
#ifdef DEBUGmcc
							fprintf(stderr, "  --------> COLLIDE - MOVE");
							fprintf(stderr, "\n pre R velocity %g", (pga -> get_v(index)).magnitude());
#endif
							(*u_particle) *= pga->get_gamma2((*u_particle));
#ifdef DEBUGmcc
							fprintf(stderr, " --- post R velocity  %g", (*u_particle).magnitude());
#endif
							pga->set_v(index,(*u_particle) );
						}
						if ( *u_fluid == DELETE_PARTICLE ) // add this, it really means delete the fluid, MAL200211
						{
			//              printf("\nfluid_species=%s, evolve_fluid=%i",fluid_species->get_name().c_str(), fIter()->get_evolvefluid()); // for debug, MAL200211
						  if (fIter() -> get_evolvefluid()) fIter() -> add_or_subtract_density(-w_ref,x);
						}
						break;
					} // completes: if (random <= sumnSigmaV)  // A collision with this fluid species is realized
				} // completes: for (i=0; i< reactionlist.num(); i++)
				if (i < reactionlist.num() ) break;
			} // completes: if (fIter()->get_n((int)x) > 0)
		}
		nCollision -= w_ref;
	} // completes: while (nCollision > 0.0 && np > 0 )
	if (np == 0) nCollision = 0.0;
	extra = nCollision;
}

// Add fluid_fluid collisions for neutral fluids; set subcycle = 100, MAL200218
void MCC::fluid_fluid(Species *const fspecies1, Species *const fspecies2) // add, MAL200214
{
	if (thefield->get_nsteps() % subcycle_ff != 0) return; // subcycle_ff set in set_subcycle_ff below, MAL200220
	if (fspecies1 -> get_q() != 0 || fspecies2 -> get_q() !=0) return; // both species must be neutrals
	oopicList<Fluid> *flist1 = fspecies1 -> get_fluidList();
	int nfluids1 = flist1 -> nItems();
	if ( ! nfluids1 ) return;
	oopicListIter<Fluid> fIter1(*flist1);
	oopicList<Fluid> *flist2 = fspecies2 -> get_fluidList();
	int nfluids2 = flist2 -> nItems();
	if ( ! nfluids2 ) return;
	oopicListIter<Fluid> fIter2(*flist2);
	// initializations
	fSpecies = NULL; // not needed, as only used in add_or_subtract_neutrals, when there are no product species
	is_fluidfluid = true; // always add particles with default weight in add_particles
	Grid * thegrid = thefield->get_theGrid();
	int ng = thefield->get_ng();
	Vector3 *u1;
	Vector3 *u2;
	set_velocity_pointers(&u1, fspecies1, &u2, fspecies2);
	for (fIter1.restart(); !fIter1.Done(); fIter1 ++) // copy n_fluid into n_fluid_old for evolving fluids
	{
		if (fIter1()->get_evolvefluid())
			for (int i=0; i<ng; i++) fIter1()->set_n_old(fIter1()->get_n(i),i);
	}
	for (fIter2.restart(); !fIter2.Done(); fIter2 ++) // copy n_fluid into n_fluid_old for evolving fluids
	{
		if (fIter2()->get_evolvefluid())
			for (int i=0; i<ng; i++) fIter2()->set_n_old(fIter2()->get_n(i),i);
	}
  
	for (fIter1.restart(); !fIter1.Done(); fIter1 ++)
	{
		for (fIter2.restart(); !fIter2.Done(); fIter2 ++)
		{
			for (int i=0; i< reactionlist.num(); i++)
			{
				Reaction * reaction = reactionlist.data(i);
				if (reaction -> is_product_empty2()) continue; // reactions (elastic, charge transfer, etc) with no products do nothing
		//        printf("\nf1=%s(%s), f2=%s(%s), reactiontypename=%s, subcycle_ff=%i",fspecies1->get_name().c_str(), fIter1()->get_fluidname().c_str(), fspecies2->get_name().c_str(), fIter2()->get_fluidname().c_str(), reaction->get_reactiontypename().c_str(), subcycle_ff); // debug
				for (int j=0; j<ng; j++)
				{
					*u1 = fIter1()->get_v_from_distribution(j)/fspecies1->get_dxdt(); //sets un or ui in grid units
					*u2 = fIter2()->get_v_from_distribution(j)/fspecies2->get_dxdt(); // sets un or ui in grid units
					v = (*u1 - *u2).magnitude()+1.e-30; // relative speed in grid units
					energy = reactant_for_sigmaV->get_energy2_eV(v*reactant_for_sigmaV->get_dxdt());
					Scalar crosssec = get_crosssection(reaction, energy);
					Scalar ratecoef = crosssec*v*reactant_for_sigmaV->get_dxdt();
					if(fIter1() == fIter2()) ratecoef *= 0.5; // like-fluid reaction
					w_ref = ratecoef * (fIter1()->get_n_old(j)) * (fIter2()->get_n_old(j)) * (thegrid->getV(j))*subcycle_ff*dt; // number of physical particles in grid j to add or subtract; add subcycle factor = 100
					x = float(j); // position in grid units to add or subtract
					dynamic(reactionlist.data(i)); // sets ui and un to DELETE_PARTICLE and adds product species
					if(collect_4_diag) collDistrib[i]->setValue( collDistrib[i]->getValue(j)+w_ref, j);
					if (fIter1() -> get_evolvefluid()) fIter1()->add_or_subtract_density(-w_ref,x);
					if (fIter2() -> get_evolvefluid()) fIter2()->add_or_subtract_density(-w_ref,x);
				}
		//        printf("\nf1=%s(%s), f2=%s(%s), reactiontypename=%s, maxwt=%g, smallestdefaultwt=%g",fspecies1->get_name().c_str(), fIter1()->get_fluidname().c_str(), fspecies2->get_name().c_str(), fIter2()->get_fluidname().c_str(), reaction->get_reactiontypename().c_str(), maxwt, smallestdefaultwt); // debug fluid_fluid subcycle
			}
		}
	}
}

int MCC::set_subcycle_ff(Species *_reactant1, Species *_reactant2)
{
	if (_reactant1 -> get_q() != 0 || _reactant2 -> get_q() !=0) return subcycle_ff; // both species must be neutrals
	oopicList<Fluid> *flist1 = _reactant1 -> get_fluidList();
	int nfluids1 = flist1 -> nItems();
	if ( ! nfluids1 ) return subcycle_ff;
	oopicListIter<Fluid> fIter1(*flist1);
	oopicList<Fluid> *flist2 = _reactant2 -> get_fluidList();
	int nfluids2 = flist2 -> nItems();
	if ( ! nfluids2 ) return subcycle_ff;
	oopicListIter<Fluid> fIter2(*flist2);
	int minsubcycle = 10000;
	for (fIter1.restart(); !fIter1.Done(); fIter1 ++)
	{
		if (fIter1()->get_evolvefluid()) minsubcycle = MIN(minsubcycle,fIter1()->get_subcycle());
	}
	for (fIter2.restart(); !fIter2.Done(); fIter2 ++)
	{
		if (fIter2()->get_evolvefluid()) minsubcycle = MIN(minsubcycle,fIter2()->get_subcycle());
	}
	return minsubcycle;
}

// for reactiontype = PHOTON_EMISSION, gets product of cross section and radiation escape factor, MAL 1/1/11
// returns cross section for all other reactiontypes
inline Scalar MCC::get_crosssection(Reaction * _reaction, Scalar _energy)
{
	if (_reaction->get_reactiontype() == PHOTON_EMISSION && _reaction->get_characteristic_values().size() == 5)
	{
		return ((_reaction->get_xsection()->sigma(_energy)) * (_reaction->get_characteristic_values(4)));
	}
	else
	{
		return (_reaction->get_xsection()->sigma(_energy));
	}
}

void MCC::init_sumSigmaV(Species *sp)
	// It is "potentially" relativistic both for ions and electrons
{
//	maxSigmaV = 0; // add, MAL 1/2/11; NOW SET TO ZERO IN set_defaults, and tested in particle_particle and fluid_particle, MAL191219
	reactant_for_sigmaV = sp;
	Reaction * r1 = reactionlist.data(0);
	if (r1 -> get_reactiontype() == PHOTON_EMISSION && r1 -> get_characteristic_values().size() == 4) setRadiationEscapeFactor(r1);
	Scalar enmax=reactant_for_sigmaV->get_maxEnergyForSigmaV(); //add to make as in xpdp1, MAL 8/29/12
	for (Scalar en = 0.; en<enmax; en+=0.1) //add to make as in xpdp1, MAL 8/29/12
	{
		Scalar temp =0.;
		for (int i=reactionlist.num()-1; i >= 0; i--)
		{
			temp += get_crosssection(reactionlist.data(i), en); // reactionlist.data(i)->get_xsection() replaced by get_crosssection, MAL 1/2/11
		}
		maxSigmaV = MAX(temp*reactant_for_sigmaV->get_speed2(en), maxSigmaV);
	}
	//printf("\nMCC::init_sumSigmaV: reactant_for_sigmaV=%s, enmax=%g, maxSigmaV=%g\n",sp->get_name().c_str(), enmax, maxSigmaV);
}

///////////////////////
//sorting            //
///////////////////////
int MCC::partition(Sort *data, int l, int r)
{
	int i(l-1);
	int j(r);
	float v = data[r].key();
	Sort tmp;
	while(true){
		while (data[++i].key() < v) if (i==r) break;
		while (v < data[--j].key()) if (j==l) break;
		if (i >= j) break;
		tmp = data[i]; // swap the elements i and j
		data[i] = data[j];
		data[j] = tmp;
	}
	tmp = data[i]; // swap the elements i and r
	data[i] = data[r];
	data[r] = tmp;
	return i;
}

void MCC::indexsort(Sort *data)
{
	if(indexsorts) delete [] indexsorts;
	indexsorts = new int[thefield -> get_ng()];

	for (int i=0; i < thefield->get_ng();i++)
	{
		indexsorts[i]=0;
	}

	for (int i=0; i < nParticles;i++)
	{
		indexsorts[int(data[i].key())]++;
	}

	for (int i=0; i < thefield->get_ng()-1;i++)
	{
		indexsorts[i+1] += indexsorts[i];
	}

	FILE *fp;
	ostring ffnname ="position.dat";
	fp = fopen(ffnname(), "a");
	for (int i=1;i < nParticles;i++)
	{
		fprintf(fp, "position[%d]=%g\n",i,data[i].key());
	}
	fclose(fp);

	ffnname ="indexsorts.dat";
	fp = fopen(ffnname(), "a");
	for (int i=1;i < thefield->get_ng();i++)
	{
		fprintf(fp, "indexsorts[%d]=%d\n",i,indexsorts[i]);
	}
	fclose(fp);

}

// provides a non-recursive quicksort of keys based on ParticleGroups

void MCC::qsort(Species & targetspecies)
{
	// calculate number of particles
	sortflag = true;
	nParticles = 0;
	ParticleGroupList *pgList_target = targetspecies.get_ParticleGroupList();
	oopicListIter<ParticleGroup> pgIter(*pgList_target);
	for (pgIter.restart(); !pgIter.Done(); pgIter++){
		nParticles += pgIter() -> get_n();
	}

	// create key index array structure
	if (sortKeys) delete[] sortKeys; // deallocate old array
	sortKeys = new Sort[nParticles];
	int j=0;
	Scalar xMin, xMax;
	bool init = true;
	// for (spIter.restart(); !spIter.Done(); spIter++) {
	//  oopicListIter<ParticleGroup> pgIter(*spIter()->get_ParticleGroupList());
	// must initialize xmin and xmax
	if (init) {
		pgIter.restart();
		xMin = pgIter()->get_x(0);
		xMax = xMin;
		init = false;
	}
	// set up sortKeys and also get xmin and xmax
	for (pgIter.restart(); !pgIter.Done(); pgIter++) {
		ParticleGroup *pg = pgIter();
		for (int i=pg->get_n(); i;) { // loop over particles
			i--; // decrement done here to reduce for loop calcs
			sortKeys[j].set(pg, i); // store index and ParticleGroup*
			float x = pg->get_x(i); // compare xmin and xmax to x
			xMin = MIN(xMin,x);
			xMax = MAX(xMax,x);
			j++;
		}
	}
	// }
	// sort the data
	int l = 0;
	int r = nParticles-1;
	int i;
	oopicStack<int> s;
	s.push(l);
	s.push(r);
	while (!s.isEmpty()){
		r = s.pop();
		l = s.pop();
		if (r <= l) continue;
		i = partition(sortKeys, l, r);
		if (i-1 > r-i){
			s.push(l); s.push(i-1); s.push(i+1); s.push(r);
		} else {
			s.push(i+1); s.push(r); s.push(l); s.push(i-1);
		}
	}
	FILE *fp;
	ostring ffnname ="position_inside_qsort.dat";
	fp = fopen(ffnname(), "a");
	for (int i=1;i < nParticles;i++)
	{
		fprintf(fp, "position[%d]=%g\n",i,sortKeys[i].key());
	}
	fclose(fp);
}

////////////////////////////////
//  Kinematics                //
////////////////////////////////

// modify calling arguments to give access to get_products() and get_characteristic_values(), MAL 1/3/11
void MCC::dynamic(Reaction * _reaction)
{
	Reaction * reaction = _reaction;
	const Xsection  & cx  = *(reaction->get_xsection());
	ReactionType reactiontype = reaction->get_reactiontype();
	totNcoll ++; // add for icp heating, MAL 1/9/11
#ifdef DEBUGmcc
	totalnumberofcollisions ++;
	printf("\nReactionType: %s\n", ReactionTypeName(reactiontype).c_str());
#endif
#ifdef DEBUGmccdynamic
	if (reactiontype != E_ELASTIC) // E_ELASTIC works fine and so exclude this reaction from the debugging; MAL191214
	{
		reaction->print_chemical_equation(0);
		Scalar crosssec = get_crosssection(reaction,energy);
		printf("\n   energy = 1/2*M_projectile*(v_projectile - v_target)^2 = %g eV, cross section=%g m2",energy,crosssec);
		ostring eSpeciesname = "NULL";
		ostring iSpeciesname = "NULL";
		ostring nSpeciesname = "NULL";
		if (eSpecies) eSpeciesname = eSpecies->get_name();
		if (iSpecies) iSpeciesname = iSpecies->get_name();
		if (nSpecies) nSpeciesname = nSpecies->get_name();
		printf("\neSpecies=%s, iSpecies=%s, nSpecies=%s, ReactionType: %s", eSpeciesname.c_str(), iSpeciesname.c_str(), nSpeciesname.c_str(), ReactionTypeName(reactiontype).c_str());
		printf("\nue=(%g, %g, %g)",ue.e1(),ue.e2(),ue.e3());
		printf("\nui=(%g, %g, %g)",ui.e1(),ui.e2(),ui.e3());
		printf("\nun=(%g, %g, %g)",un.e1(),un.e2(),un.e3());
		int cvsize = reaction->get_characteristic_values().size();
		printf("\ncharacteristic_values size = %i",cvsize);
		if (cvsize)
		{
			for (int j=0; j<cvsize; j++)
				printf("\ncharacteristic_value[%i]=%g",j,reaction->get_characteristic_values(j));
		}
	}
#endif
	switch(reactiontype)
	{
	// --- Scattering processes:
	case E_ELASTIC:
    {
		eElastic(cx);
#ifdef DEBUGmccdynamic
		if (reactiontype != E_ELASTIC) printf("\nExiting E_ELASTIC"); // E_ELASTIC works fine and so exclude this reaction from the debugging; MAL191214
#endif
    }
		break;
	case EO_ELASTIC: // added by JTG
		eElastic(cx);
		break;
	case E_EXCITATION:
    {
		sec_n = reaction->get_products(0); // add MAL 12/27/10
#ifdef DEBUGmccdynamic
		ostring secn_name = "NULL";
		if (sec_n) secn_name = sec_n->get_reactionname();
		printf("\nsec_n=%s",secn_name.c_str());
#endif
		eExcitation(cx);
#ifdef DEBUGmccdynamic
		printf("\nExiting E_EXCITATION");
#endif
    }
		break;
	case PHOTON_EMISSION: // add MAL 12/27/10
    {
		sec_n = reaction->get_products(0);
#ifdef DEBUGmccdynamic
		ostring secn_name = "NULL";
		if (sec_n) secn_name = sec_n->get_reactionname();
		printf("\nsec_n=%s",secn_name.c_str());
#endif
		photonEmission(cx);
		//			printf("\n%s -> %s PHOTON_EMISSION called",reaction->get_reactants()[0]->get_reactionname().c_str(), sec_n->get_reactionname().c_str());
//            printf("\n1eV cross section=%g\n",get_crosssection(reaction,1.0));
#ifdef DEBUGmccdynamic
		printf("\nExiting PHOTON_EMISSION");
#endif
    }
		break;
	case PENNING_IONIZATION: // add MAL 12/27/10
    {
		sec_e = reaction->get_products(0);
		sec_i = reaction->get_products(1);
		sec_n = reaction->get_products(2);
#ifdef DEBUGmccdynamic
		ostring sece_name = "NULL"; ostring seci_name = "NULL"; ostring secn_name = "Null";
		if (sec_e) sece_name = sec_e->get_reactionname();
		if (sec_i) seci_name = sec_i->get_reactionname();
		if (sec_n) secn_name = sec_n->get_reactionname();
		printf("\nsec_e=%s, sec_i=%s, sec_n=%s",sece_name.c_str(), seci_name.c_str(), secn_name.c_str());
#endif
		samemassPenningIz(cx, reaction->get_characteristic_values(0)); // ReleasedEnergy = characteristic_values(0)
		//			printf("\n%s + %s -> %s + %s + %s PENNING_IONIZATION called", reaction->get_reactants()[0]->get_reactionname().c_str(),
		//				reaction->get_reactants()[1]->get_reactionname().c_str(), sec_e->get_reactionname().c_str(), sec_i->get_reactionname().c_str(),
		//				sec_n->get_reactionname().c_str());
#ifdef DEBUGmccdynamic
		printf("\nExiting PENNING_IONIZATION");
#endif
    }
		break;
	case AM_PENNING_IONIZATION: // add MAL 12/27/10 (SAS July 7 2014)
		sec_e = reaction->get_products(0);
		sec_i = reaction->get_products(1);
		sec_n = reaction->get_products(2);
		anymassPenningIz(cx, iSpecies, nSpecies, reaction->get_characteristic_values(0)); // ReleasedEnergy = characteristic_values(0)
		//			printf("\n%s + %s -> %s + %s + %s PENNING_IONIZATION called", reaction->get_reactants()[0]->get_reactionname().c_str(),
		//				reaction->get_reactants()[1]->get_reactionname().c_str(), sec_e->get_reactionname().c_str(), sec_i->get_reactionname().c_str(),
		//				sec_n->get_reactionname().c_str());
		break;
	case SAMEMASS_EXCITATION: // add MAL 12/27/10
		{
			sec_n = reaction->get_products(0);
 #ifdef DEBUGmccdynamic
			ostring secn_name = "NULL";
			if (sec_n) secn_name = sec_n->get_reactionname();
			printf("\nsec_n=%s, Nsamemassexcproducts=%i",secn_name.c_str(),Nsamemassexcproducts);
 #endif
			samemassExcitation(cx);
		//			printf("\n%s + %s -> %s + %s SAMEMASS_EXCITATION called", reaction->get_reactants()[0]->get_reactionname().c_str(),
		//				reaction->get_reactants()[1]->get_reactionname().c_str(), sec_n->get_reactionname().c_str(),
		//				reaction->get_reactants()[1]->get_reactionname().c_str());
#ifdef DEBUGmccdynamic
			printf("\nExiting SAMEMASS_EXCITATION");
#endif
		}
		break;
	case SAMEMASS_ELASTIC:
		{
		samemassElastic(cx);
//		printf("\n%s + %s -> %s + %s SAMEMASS_ELASTIC called", reaction->get_reactants()[0]->get_reactionname().c_str(),
//		reaction->get_reactants()[1]->get_reactionname().c_str(), reaction->get_reactants()[0]->get_reactionname().c_str(),
//		reaction->get_reactants()[1]->get_reactionname().c_str());
#ifdef DEBUGmccdynamic
		printf("\nExiting SAMEMASS_ELASTIC");
#endif
		}
		break;
	case HALFMASS_ELASTIC:  // Added by JTG May 20 2009
		halfmassElastic(cx); //
		break;  //
	case ANYMASS_ELASTIC:  // Added by JTG November 23 2009
		anymassElastic(cx,iSpecies,nSpecies); //
		break;  //
	case B_ANYMASS_ELASTIC:  // Added by SAS August 2014
		BackwardsAnymassElastic(cx,iSpecies,nSpecies); //
		break;  //
	case B_SAMEMASS_ELASTIC: // Added by SAS August 2014
		{
			BackwardsSamemassElastic(cx); //
#ifdef DEBUGmccdynamic
			printf("\nExiting B_SAMEMASS_ELASTIC");
#endif
		}
		break;  //
		// --- Ionizing processes:
	case E_IONIZATION:
		{
			sec_e = reaction->get_products(0);
			sec_i = reaction->get_products(1);
#ifdef DEBUGmccdynamic
			ostring sece_name = "NULL"; ostring seci_name = "NULL";
			if (sec_e) sece_name = sec_e->get_reactionname();
			if (sec_i) seci_name = sec_i->get_reactionname();
			printf("\nsec_e=%s, sec_i=%s",sece_name.c_str(), seci_name.c_str());
#endif
			//      printf("\nIonization sec_e = %s, sec_i = %s\n", sec_e->get_name().c_str(), sec_i->get_name().c_str());
			eIonization(cx);
#ifdef DEBUGmccdynamic
			printf("\nExiting E_IONIZATION");
#endif
		}
		break;
		// --- Charge exchange processes:
	case Ch_EXCHANGE:
		ElectronExchange(cx);
		break;
	case Ch_EXCHANGEO: // Added by JTG
		ElectronExchange(cx);
		break;
	case NONRES_Ch_EXCHANGE: // Added by MAL, 11/20/11
		{
			sec_i=reaction->get_products(0);
			sec_n=reaction->get_products(1);
			Scalar ReleasedEnergy = 0;
			if (!(reaction -> is_characteristic_value_empty())) ReleasedEnergy = reaction -> get_characteristic_values(0);
			nonresElectronExchange(sec_i, sec_n, ReleasedEnergy);
		}
		break;
	default:
		fprintf(stderr,"Unrecognized collision type in MCC::dynamic()\n");
	}
}

// NOT USED; USE dynamic instead, MAL200303
// For fluid_fluid collision dynamics; MAL200214
void MCC::dynamic_fluids(Reaction * _reaction)
{
	Reaction * reaction = _reaction;
	const Xsection  & cx  = *(reaction->get_xsection());
	ReactionType reactiontype = reaction->get_reactiontype();
	totNcoll ++; // add for icp heating, MAL 1/9/11

#ifdef DEBUGmcc
	totalnumberofcollisions ++;
	printf("\nReactionType: %s\n", ReactionTypeName(reactiontype).c_str());
#endif
#ifdef DEBUGmccdynamic
	if (reactiontype != E_ELASTIC) // E_ELASTIC works fine and so exclude this reaction from the debugging; MAL191214
	{
		reaction->print_chemical_equation(0);
		Scalar crosssec = get_crosssection(reaction,energy);
		printf("\n   energy = 1/2*M_projectile*(v_projectile - v_target)^2 = %g eV, cross section=%g m2",energy,crosssec);
		ostring eSpeciesname = "NULL";
		ostring iSpeciesname = "NULL";
		ostring nSpeciesname = "NULL";
		if (eSpecies) eSpeciesname = eSpecies->get_name();
		if (iSpecies) iSpeciesname = iSpecies->get_name();
		if (nSpecies) nSpeciesname = nSpecies->get_name();
		printf("\neSpecies=%s, iSpecies=%s, nSpecies=%s, ReactionType: %s", eSpeciesname.c_str(), iSpeciesname.c_str(), nSpeciesname.c_str(), ReactionTypeName(reactiontype).c_str());
		printf("\nue=(%g, %g, %g)",ue.e1(),ue.e2(),ue.e3());
		printf("\nui=(%g, %g, %g)",ui.e1(),ui.e2(),ui.e3());
		printf("\nun=(%g, %g, %g)",un.e1(),un.e2(),un.e3());
		int cvsize = reaction->get_characteristic_values().size();
		printf("\ncharacteristic_values size = %i",cvsize);
		if (cvsize)
		{
			for (int j=0; j<cvsize; j++)
				printf("\ncharacteristic_value[%i]=%g",j,reaction->get_characteristic_values(j));
		}
	}
#endif

	switch(reactiontype)
	{
	// --- Scattering processes:
	//   case E_ELASTIC:
	//     break;
	//   case EO_ELASTIC: // added by JTG
	//     break;
	//   case E_EXCITATION:
	//     break;
    case PHOTON_EMISSION: // add MAL 12/27/10
    {
		sec_n = reaction->get_products(0);
#ifdef DEBUGmccdynamic
		ostring secn_name = "NULL";
		if (sec_n) secn_name = sec_n->get_reactionname();
		printf("\nPHOTON_EMISSION: sec_n=%s",secn_name.c_str());
#endif
		photonEmission(cx);
//      if(sec_n) {}// add_product(sec_n,ui);
//      ui = DELETE_PARTICLE; // deletes particle before Unit in the SpeciesList
    }
      break;
    case PENNING_IONIZATION: // add MAL 12/27/10
    {
      sec_e = reaction->get_products(0);
      sec_i = reaction->get_products(1);
      sec_n = reaction->get_products(2);
#ifdef DEBUGmccdynamic
      ostring sece_name = "NULL"; ostring seci_name = "NULL"; ostring secn_name = "Null";
      if (sec_e) sece_name = sec_e->get_reactionname();
      if (sec_i) seci_name = sec_i->get_reactionname();
      if (sec_n) secn_name = sec_n->get_reactionname();
      Scalar releasedenergy = reaction->get_characteristic_values(0);
      printf("\nPENNING_IONIZATION: sec_e=%s, sec_i=%s, sec_n=%s",sece_name.c_str(), seci_name.c_str(), secn_name.c_str());
#endif
      samemassPenningIz(cx, reaction->get_characteristic_values(0)); // ReleasedEnergy = characteristic_values(0)
//      ui = DELETE_PARTICLE;
//      un = DELETE_PARTICLE;
    }
      break;
    case AM_PENNING_IONIZATION: // add MAL 12/27/10 (SAS July 7 2014)
    {
		sec_e = reaction->get_products(0);
		sec_i = reaction->get_products(1);
		sec_n = reaction->get_products(2);
		anymassPenningIz(cx, iSpecies, nSpecies, reaction->get_characteristic_values(0)); // ReleasedEnergy = characteristic_values(0)
//      Scalar releasedenergy = reaction->get_characteristic_values(0);
//      ui = DELETE_PARTICLE;
//      un = DELETE_PARTICLE;
      //      printf("\n%s + %s -> %s + %s + %s PENNING_IONIZATION called", reaction->get_reactants()[0]->get_reactionname().c_str(),
      //        reaction->get_reactants()[1]->get_reactionname().c_str(), sec_e->get_reactionname().c_str(), sec_i->get_reactionname().c_str(),
      //        sec_n->get_reactionname().c_str());
    }
      break;
    case SAMEMASS_EXCITATION: // add MAL 12/27/10
    {
    	sec_n = reaction->get_products(0);
 #ifdef DEBUGmccdynamic
		ostring secn_name = "NULL";
		if (sec_n) secn_name = sec_n->get_reactionname();
		printf("\nsec_n=%s, Nsamemassexcproducts=%i",secn_name.c_str(),Nsamemassexcproducts);
 #endif
		samemassExcitation(cx);
//      if(sec_n) {} // add_product(sec_n,un);
//      un = DELETE_PARTICLE; // deletes is_after particle in the SpeciesList
    }
      break;
    case SAMEMASS_ELASTIC:
      break;
    case HALFMASS_ELASTIC:  // Added by JTG May 20 2009
      break;
    case ANYMASS_ELASTIC:  // Added by JTG November 23 2009
      break;
    case B_ANYMASS_ELASTIC:  // Added by SAS August 2014
      break;
    case B_SAMEMASS_ELASTIC: // Added by SAS August 2014
      break;
      // --- Ionizing processes:
//    case E_IONIZATION:
//      break;
      // --- Charge exchange processes:
    case Ch_EXCHANGE:
      break;
    case Ch_EXCHANGEO: // Added by JTG
      break;
    case NONRES_Ch_EXCHANGE: // Added by MAL, 11/20/11
      break;
    default:
    	fprintf(stderr,"Unrecognized collision type in MCC::dynamic_fluid()\n");
	}
}

// Returns average number of collisions per timestep, needed for icp heating, called in reaction.cpp, MAL 1/8/11
// the averaging time is nupdateE, a parameter in the PoissonSolver section of the input file; default is nupdateE=5000
void MCC::time_average()
{
	Ncoll = (Scalar)(totNcoll - totNcoll_old);
	int nsmod = thefield->get_nsteps() % thefield->get_nupdateE();
	Scalar avecoef = 1.0/(nsmod+1);
	if(nsmod != 0) // accum into tempNcoll only
	{
		tempNcoll += (Ncoll - tempNcoll)*avecoef;
	}
	else if(nsmod == 0 && thefield->get_nsteps() > 1) // xfer tempNcoll to aveNcoll, and begin again accum into tempNcoll
	{
		aveNcoll_per_step = tempNcoll/thefield->get_nupdateE(); // average number of collisions in one timestep
		tempNcoll = Ncoll;
		totNcoll_old = totNcoll;
	}
}

// --- Elastic and non-elastic scattering processes:
void MCC::eElastic(const Xsection& cx)  // Only works for electrons (currently)
{
	/*
	//This is the nonrelativistic algorithm
	if(relativisticMCC == false)
	{
	new_eVelocity_NR();
	}
	//This is the relativistic algorithm
	else
	{
	Scalar thetaS, phiS;
	elasticScatteringAngles(energy,thetaS, phiS);
	Vector3 uScatter;
	Scalar afterElastic = elasticEnergy(energy, thetaS);
	energy = energy - afterElastic;
	uScatter = newMomentum(u, energy, thetaS, phiS);
	u = uScatter;
	}
	 */
	// We move to the frame of the neutral target.
	//But the electron velocity being much higher than the neutral velocity, energy and v are not modified.

	Scalar temp = eSpecies->get_nr_u_limit()/eSpecies->get_dxdt();  // Incorrect normalization, changed * to /, MAL 8/31/09
	temp /= eSpecies->gamma(temp);
	if (v < temp)
	{
		//			v = ue.magnitude(); //USE xpdp1 algorithm, which ignores transformation to CM system
		eScattering_NR();
	}
	else
	{
		eScattering_R();
	}
}

void MCC::eExcitation(const Xsection& cx)
	// Excitation is treated like elastic collisions with a reduced energy.
	// This reduced energy is used both for the choice of the scattering angle angle and to determine the post collision velocity.
	// This might need to be reviewed (C.H. Lim 25th May 2005)
	// Superelastic collisions are treated as an elastic collision with an increased energy, MAL 12/27/10
{
	//  if (sec_n->get_name() == "Argon") { // for debug excitation and superelastic collisions
	//  printf("\neExcitation: eSpecies=%p (%s), nSpecies=%p (%s), sec_n=%p (%s)", (void*)eSpecies, eSpecies->get_name().c_str(),
	//	(void*)nSpecies, nSpecies->get_name().c_str(), (void*)sec_n, sec_n->get_name().c_str());
	//	printf("\n  ue1=%g, ue2=%g, ue3=%g, un1=%g, un2=%g, un3=%g, electronenergy=%g", ue.e1(), ue.e2(), ue.e3(), un.e1(), un.e2(), un.e3(), energy);
	//	}
	energy -= cx.get_threshold();
	Scalar temp = eSpecies->get_nr_u_limit()/eSpecies->get_dxdt(); // Incorrect normalization, added /eSpecies->get_dxdt(), MAL 8/31/09 and 10/12/09 here
	temp /= eSpecies->gamma(temp); // Incorrect, added eSpecies->, MAL 8/31/09
	if (v < temp)
	{
		v = eSpecies->get_speed2(energy);
		v /= eSpecies->get_dxdt();
		eScattering_NR();
	}
	else
	{
		eScattering_R();
	}
//	if(sec_n && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_n->get_energyThrToAddAsPIC()) add_particles(sec_n,un); // MAL 11/16/10
	if(sec_n) add_product(sec_n, un);
	un = DELETE_PARTICLE;
	//	if (sec_n->get_name() == "Argon") {
	//  printf("\n  ue1=%g, ue2=%g, ue3=%g, un1=%g, un2=%g, un3=%g, electronenergy=%g", ue.e1(), ue.e2(), ue.e3(), un.e1(), un.e2(), un.e3(),
	//		eSpecies->get_energy_eV(eSpecies->get_dxdt()*ue));
	//	}
}


void MCC::photonEmission(const Xsection& cx)  // add MAL 11/17/10
{
	// Here iSpecies is the emitting neutral species, nSpecies is Unit, and sec_n is the final state species, MAL 12/27/10
	//  printf("\nphotonEmission: nSpecies=%p (%s), iSpecies=%p (%s), sec_n=%p (%s)", (void*)nSpecies, nSpecies->get_name().c_str(),
	//		(void*)iSpecies, iSpecies->get_name().c_str(), (void*)sec_n, sec_n->get_name().c_str());
	//  printf("\n  un1=%g, un2=%g, un3=%g, ui1=%g, ui2=%g, ui3=%g", un.e1(), un.e2(), un.e3(), ui.e1(), ui.e2(), ui.e3());

	// if(sec_n && iSpecies->get_energy_eV(iSpecies->get_dxdt()*ui) > sec_n->get_energyThrToAddAsPIC()) add_particles(sec_n,ui); // MAL 12/27/10
#ifdef DEBUGmccdynamic
	printf("\nEntering photonEmission ...");
#endif
//  if(sec_n && (iSpecies->get_energy_eV(iSpecies->get_dxdt()*ui) > sec_n->get_energyThrToAddAsPIC())) add_particles(sec_n,ui); // try to remove seg fault, MAL191217
	if(sec_n) add_product(sec_n,ui);
	ui = DELETE_PARTICLE; // deletes particle before Unit in the SpeciesList
	//  printf("\n  un1=%g, un2=%g, un3=%g, ui1=%g, ui2=%g, ui3=%g\n", un.e1(), un.e2(), un.e3(), ui.e1(), ui.e2(), ui.e3());
}

void MCC::samemassExcitation(const Xsection& cx)  // add MAL 12/27/10
{
	//   printf("\nsamemassExcitation: nSpecies=%p (%s), iSpecies=%p (%s), sec_n=%p (%s)", (void*)nSpecies, nSpecies->get_name().c_str(),
	// 	(void*)iSpecies, iSpecies->get_name().c_str(), (void*)sec_n, sec_n->get_name().c_str());
	//  printf("\n  un1=%g, un2=%g, un3=%g, ui1=%g, ui2=%g, ui3=%g", un.e1(), un.e2(), un.e3(), ui.e1(), ui.e2(), ui.e3());
	// We move to the frame of the neutral here.
	Vector3 ui_init = ui;
	ui -= un;
	// Scalar coschi = sqrt(frand());
	Scalar coschi = sqrt(abs(frand())); // to prevent seg faults, MAL191217
	Scalar chi = acos(coschi);
	Scalar phi = TWOPI * frand();
	v = ui.magnitude() * coschi;
	ui = v * newcoordiante(ui, chi, phi) + un;
	un +=  ui_init - ui;
	// if (sec_n && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_n->get_energyThrToAddAsPIC())
	//	add_particles(sec_n,un); // MAL 12/27/10
	if(sec_n) add_product(sec_n,un);
	un = DELETE_PARTICLE; // deletes is_after particle in the SpeciesList
	//  printf("\n  un1=%g, un2=%g, un3=%g, ui1=%g, ui2=%g, ui3=%g\n", un.e1(), un.e2(), un.e3(), ui.e1(), ui.e2(), ui.e3());
}

// Calculate effective decay rate nu10eff = g*nu10 for photon emission, g = radiation escape factor, MAL 12/30/10
// Use formulation of P.J. Walsh, Phys. Rev. vol 116, p. 511, Eq. (2.9), (1959); see also T. Holstein, Phys. Rev. vol. 83, p. 1159 (1951)
// Also see C.M. Ferreira, J. Loureiro, and A. Ricard, J. Appl. Phys. vol 57, p. 82 (1985)
// radiation wavelength lambda10 in m, natural decay rate nu10 in s^{-1}, lower-level resonantatom-groundneutral cross section sigma00 in m^2,
// ratio of upper-to-lower level statistical weights g1overg0
void MCC::setRadiationEscapeFactor(Reaction * _reaction)
{
	Species * sp = _reaction->get_products()[0];
	Scalar lambda10 = _reaction->get_characteristic_values(0);
	Scalar nu10 = _reaction->get_characteristic_values(1);
	Scalar sigma00 = _reaction->get_characteristic_values(2);
	Scalar g1overg0 = _reaction->get_characteristic_values(3);
	if (!sp || !sp->includes_fluids()) // ADD PARENS, AS !(sp->includes_fluids()) ???
	{
		_reaction->add_characteristic_value(1.0);
	}
	else
	{
		oopicList<Fluid> * fluidlist = sp ->get_fluidList();
		oopicListIter<Fluid> thei(*fluidlist);
		Scalar L = sp->get_grid()->getL();
		Scalar V = sp->get_grid()->getGridVolume();
		printf("\nMCC::setRadiationEscapeFactor(): lambda10=%g, nu10=%g, sigma00=%g, g1overg0=%g, secspecies=%s, L=%g, V=%g", lambda10, nu10, sigma00,
				g1overg0, sp->get_reactionname().c_str(), L, V);
		Scalar g = 0; // escape factor including both Doppler and collisional broadening
		for (thei.restart(); !thei.Done(); thei++)
		{
			Scalar n = thei()->get_Ntot()/V;
			Scalar T = thei()->get_T();
			Scalar v0 = sqrt(2.0*PROTON_CHARGE*T/sp->get_m());
			Scalar v00 = sqrt(2.0*PROTON_CHARGE*0.026/sp->get_m()); // added, since sigma00 is for Tg = 0.026 eV, and sigma scales as 1/sqrt(Tg), MAL 5/13/16
// cross section sigma00=sigma_t/2, as given from B.M. Smirnov, Theory of Gas Discharge Plasma, pp. 71-74 (Springer, 2015), MAL 5/13/16
			Scalar k0 = lambda10*lambda10*lambda10*nu10*g1overg0*n/(8.0*PI*sqrt(PI)*v0);
			Scalar a = (nu10 + n*sigma00*v00*sqrt(4.0/PI))*lambda10/(4.0*PI*v0); // v0 -> v00, MAL 5/13/16
			Scalar gd = 2.0/(k0*L*sqrt(PI*log(0.5*k0*L))); // escape factor for pure Doppler broadening
			Scalar gc = sqrt(sqrt(PI)*a/(0.5*k0*L))/sqrt(PI); // escape factor for pure collisional broadening
			Scalar gcd = 2.0*a/(PI*sqrt(log(0.5*k0*L))); // escape factor for collisional-type emission and Doppler-type absorption
			Scalar gi = gd*exp(-gcd*gcd/(gc*gc)) + gc*oopd1::erf(gcd/gc);
			g += gi;
			printf("\n  T=%g, n=%g, v0=%g, k0=%g, a=%g, gd=%g, gc=%g, gcd=%g, gi=%g", T, n, v0, k0, a, gd, gc, gcd, gi);
		}
		printf("\n  overall escape factor g = %g, nu10eff=g*nu10=%g\n", g, g*nu10);
		_reaction->add_characteristic_value(g);
	}
}

// Non-relativistic case:
void MCC::eScattering_NR()
{
	Scalar coschi; // chi is the electron scattering angle in the target rest frame

	// RESTORE SURENDRA'S FORMULA FOR COMPARISON TO XPDP1, MAL 8/17/12
	// Surendra's formula: Surendra, Graves, Jellum. Phys.Rev. A41. 1112 (1990).
	//  if (energy < 1e-30) coschi = 1.0;
	//  else coschi = 1 + 2.0 * (1.0 - pow(energy + 1.0, frand())) /  energy;



	// Okhrimovskyy, Bogaerts, Gijbels. Phys. Rev. Vol 65, 037402 (2002).
	Scalar R = frand();
	coschi = 1 - (2*R)/(1+8*(energy/27.21)*(1-R));
	if (coschi < -1) coschi = -1; // add to prevent seg faults, MAL191217
	if (coschi > 1) coschi = 1; // add to prevent seg faults, MAL191217
	Scalar chi = acos(coschi);
	Scalar phi = TWOPI * frand();
	Scalar m1 = eSpecies -> get_m();
	Scalar m2 = nSpecies -> get_m();
	// v in grid units and energy in eV are in the frame where v_neutral = 0; ie, v is the relative velocity in grid units
	//	 printf("\nm1=%g, m2=%g, vinit=%g, coschi=%g",m1,m2,v,coschi);
	// assumes chi is electron scattering angle in CM frame; for electron-heavy particle scattering, it is correct
	// v *= sqrt(1. - 2. * (m1*m2/(m1+m2)/(m1+m2))  * (1. - coschi));
	// v *= sqrt(1. - 2. * (m1*m2/(m1+m2)/(m1+m2))  * (1. - coschi)); // remove to prevent seg faults, MAL191217
	v *= sqrt(1. - 2. * (m1*m2/((m1+m2)*(m1+m2)))  * (1. - coschi));
	// now v is the new relative velocity, accounting for the elastic scattering energy loss
	//	 printf("\nvfinal=%g, |un|=%g, |ue|init=%g",v,un.magnitude(),ue.magnitude());
	ue = v*  newcoordiante(ue-un , chi, phi) + un; //add un here to xform from target rest frame back to lab system!  MAL, 8/29/12
	//  ue = v*  newcoordiante(ue, chi, phi); //to test for no lab-CM xformation for electrons, MAL 9/2/12
	//	 printf("\n|ue|final=%g\n",ue.magnitude());
}

void MCC::eScattering_R()
{
	Scalar thetaS, phiS;
	elasticScatteringAngles(energy,thetaS, phiS);
	energy -= elasticEnergy(energy, thetaS);
	ue = newMomentum(ue, energy, thetaS, phiS);
}

// Method for calculating the scattering angles, relativistic case

void MCC::elasticScatteringAngles(const Scalar& eImpact, Scalar& thetaScatter, Scalar& phiScatter)
{
	//First, we generate a Monte Carlo value for thetaScatter
	int atomicNum = nSpecies->get_atomicnumber();

	//the beta from the get_beta is the normalized beta so I use the old calculation, to be discussed
	// mindgame: OK to delete?
	/*
	   Scalar gMinus1 = eImpact / ELECTRON_MASS_EV;
	   Scalar gamma = gMinus1 + 1.;
	   Scalar beta = sqrt((gamma + 1.) * gMinus1) / gamma;
	 */

	// Scalar beta = pga->get_beta2(eImpact);
	Scalar beta = eSpecies->get_beta2(eImpact);
	Scalar a_bohr = 0.529E-10;      // mindgame: these move to pd1.hpp?
	Scalar alpha = 1. / 137.;
	Scalar h = 1.055e-34 * 6241457005723417000.;
	Scalar p = beta * (eImpact + ELECTRON_MASS_EV) / SPEED_OF_LIGHT;

	// unused variable according to g++. should be deleted.  JH, Aug. 22, 2005.
	// Scalar p_2 = 1. / SPEED_OF_LIGHT * sqrt(2. * eImpact * ELECTRON_MASS_EV + eImpact * eImpact);

	Scalar A_1 = 0.25 * pow((h / p),2.) / pow((0.885 * pow( atomicNum, -1./3.) * a_bohr),2.);
	Scalar A_2 = 1.13 + 3.76 * pow((alpha * atomicNum / beta),2); //2nd arg of pow should be 2, not 1, from JTG 12/1/12
	Scalar A = A_1 * A_2;
	Scalar fTheta = frand();
	Scalar cosine = 2. * A + 1 +( 2 * A * (A + 1) / (fTheta - A - 1));

	/////////////////////////////////////////////////////////////////////
	//Sometimes, calculation of A is incorrect in the region of low energy
	//I added this criteria C.H Lim 6/1/2005 'need to be discussed'
	if (cosine > -1. && cosine <1)  thetaScatter = acos(cosine);
	else if (cosine > 1.)  thetaScatter = acos(1.)+1E-30;
	else if (cosine < -1.) thetaScatter = acos(-1.)+1E-30;
	else thetaScatter = 0.;
	//////////////////////////////////////////////////////////////////////

	phiScatter = TWOPI * frand();

#ifdef DEBUG
	FILE *fp;
	ostring ffnname = "relelastic_angle.dat";
	fp = fopen(ffnname, "a");
	fprintf(fp, "%g \t %g \t %g \n", (thetaScatter), phiScatter, fTheta );
	fclose(fp);
#endif
	return;
}

// Method for calculating the change of the energy in case of elastic collision, relativistic case

Scalar MCC::elasticEnergy( const Scalar& eImpact, Scalar& thetaScatter)
{
	// mindgame: What are these for?
	/*
	   Scalar gMinus1 = eImpact / ELECTRON_MASS_EV;
	   Scalar gamma_elec = gMinus1 + 1;
	   Scalar beta_elec = eSpecies->get_beta2(eImpact); ////instead of pga->get_beta2
	   Scalar W = sqrt(pow(ELECTRON_MASS+PROTON_MASS,2.)*SPEED_OF_LIGHT_SQ*SPEED_OF_LIGHT_SQ+2.*(eImpact-ELECTRON_MASS*SPEED_OF_LIGHT_SQ)*PROTON_MASS*SPEED_OF_LIGHT_SQ);
	   Scalar newEnergy = 1/W*(pow(ELECTRON_MASS*SPEED_OF_LIGHT_SQ,2.)+eImpact*PROTON_MASS*SPEED_OF_LIGHT_SQ);
	 */

	Scalar M = nSpecies -> get_m()/PROTON_CHARGE;
	Scalar newEnergy = ((eImpact+ELECTRON_MASS_EV)*sin(thetaScatter)*sin(thetaScatter)+M*SPEED_OF_LIGHT_SQ*(1-cos(thetaScatter)))*eImpact*(eImpact+2*ELECTRON_MASS_EV);
	newEnergy *= 1/((eImpact+M*SPEED_OF_LIGHT_SQ)*(eImpact+M*SPEED_OF_LIGHT_SQ)-eImpact*(eImpact+2*ELECTRON_MASS_EV)*cos(thetaScatter)*cos(thetaScatter));

	return newEnergy;
}

void MCC::samemassPenningIz(const Xsection& cx, Scalar ReleasedEnergy)
// Non-relativistic, isotropic neutral-neutral elastic scattering for equal masses mi = mn, followed by emission of a fast electron
{
	//   printf("samemassPenningIz: ion=%s, neutral=%s, init_ion_en=%g eV, init_neut_en=%g eV", iSpecies->get_name().c_str(), nSpecies->get_name().c_str(),
	//  	iSpecies->get_energy_eV(nSpecies->get_dxdt()*ui), nSpecies->get_energy_eV(nSpecies->get_dxdt()*un)); // FOR DEBUG, MAL 12/27/10

	// We move to the frame of the neutral here.
	Vector3 ui_init = ui;
	ui -= un;
	//  Scalar coschi = sqrt(frand()); // delete to try to eliminate seg fault, MAL191217
	Scalar coschi = sqrt(abs(frand())); // add, MAL191217
	Scalar chi = acos(coschi);
	Scalar phi = TWOPI * frand();
	v = ui.magnitude() * coschi;
	ui = v * newcoordiante(ui, chi, phi) + un;
	un +=  ui_init - ui;
	if (nSpecies->get_q() == 0 && sec_n) // should always be true, MAL 11/26/10
	{
//		if (sec_n->get_energy_eV(sec_n->get_dxdt()*un) > sec_n->get_energyThrToAddAsPIC())
//			add_particles(sec_n, un); // Create PIC argon neutral
		if(sec_n) add_product(sec_n, un);
		un = DELETE_PARTICLE; // Delete original neutral (if a particle)
	}
	if (iSpecies->get_q() == 0 && sec_i) 	// iSpecies is also a neutral species, MAL 11/26/10
	{
//		add_particles(sec_i, ui); // Create PIC argon ion
		if(sec_i) add_product(sec_i, ui);
		ui = DELETE_PARTICLE; // Delete original incident neutral (if a particle)
		//		printf("\n  ui1=%g, ui2=%g, ui3=%g", ui.e1(), ui.e2(), ui.e3()); // for debug, MAL 12/27/10
	}
	if (sec_e)
	{
		// 	Scalar ReleasedEnergy = cx.get_threshold();
		Scalar espeed = sqrt(2.0*ReleasedEnergy*PROTON_CHARGE/ELECTRON_MASS)/sec_e->get_dxdt();
		phi = TWOPI * frand();
		coschi = 1.0 - 2.0 * frand();
		//    Scalar sinchi = sqrt(1-pow(coschi,2)); // delete to try to eliminate seg fault, MAL191217
		Scalar sinchi = sqrt(abs(1-coschi*coschi)); // add to prevent seg faults?, MAL191217
		Vector3 uee = Vector3(sinchi*cos(phi), sinchi*sin(phi), coschi);
		uee *= espeed;
//		add_particles(sec_e,uee);
		add_product(sec_e,uee);
		//  	printf("\n  sec_i=%s, sec_n=%s, sec_e=%s, ReleasedEn=%g eV, Ee=%g eV\n", sec_i->get_name().c_str(), sec_n->get_name().c_str(),
		//   sec_e->get_name().c_str(), ReleasedEnergy, sec_e->get_energy_eV(sec_e->get_dxdt()*uee)); // FOR DEBUG, MAL 12/27/10
	}
}


void MCC::anymassPenningIz(const Xsection& cx, Species* si, Species* sn, Scalar ReleasedEnergy)
	// Non-relativistic, isotropic ion-neutral or neutral-neutral elastic scattering followed by an emission of a fast electron. Works for particles of any mass ratio mi/mn
	// added by MAL 11/20/09 (SAS July 4 2014)
{
	Scalar mi = 0;
	Scalar mn = 0;
	Scalar mu;
	Scalar mratio;
	if(si) mi = si->get_m();
	if(sn) mn = sn->get_m();
	if(mi>0 && mn>0)
	{
		mu = mi*mn/(mi+mn); // reduced mass
		mratio = mi/mn;
	}
	else error("mi and mn must be nonzero in anymassElastic");
	Vector3 ui_init = ui; // Save initial ion velocity
	// We move to the frame ("lab frame") of the neutral here.
	// NOTE: here the "lab frame" really means the target rest frame; MAL 9/25/12
	Vector3 ui_lab = ui-un;
	Scalar vi_lab = ui_lab.magnitude();
	Scalar Vcm = mu*vi_lab/mn; // Center of mass speed in the lab system
	Scalar vilabVcm = vi_lab - Vcm; // Initial (and final) speed in CM system

	// Find the scattering angle chi in the lab frame
	Scalar chi = 0.0;
	Scalar costheta=1.0-2.0*frand(); // theta is the CM scattering angle
	if (costheta <= -1.0) // Special case of theta = pi
	{
		if (mratio < 1.0) chi = M_PI;
		else if (mratio == 1.0) chi = M_PI/2.0;
	}
	else if (mratio + costheta == 0.0) // Special case of chi = pi/2
		chi = M_PI/2.0;
	else
	{
		Scalar temp1 = 1.0-costheta*costheta;
		Scalar sintheta = 0.0;
		if (temp1 > 0.0) sintheta=sqrt(temp1);
		chi = atan(sintheta/(mratio+costheta)); if(chi < 0.0) chi += M_PI;
	}
	Scalar phi = TWOPI*frand();
	// Find the magnitude viprime_lab of the scattered ion (or neutral) velocity in the lab frame
	Scalar viprime_lab = sqrt(vilabVcm*vilabVcm + Vcm*Vcm + 2*vilabVcm*Vcm*costheta);
	ui = viprime_lab*newcoordiante(ui_lab, chi, phi) + un; // new ion (or neutral) velocity
	un += (mi/mn)*(ui_init - ui); // From conservation of momentum, new neutral velocity

	if (nSpecies->get_q() == 0 && sec_n) // should always be true, MAL 11/26/10
	{
//		if (sec_n->get_energy_eV(sec_n->get_dxdt()*un) > sec_n->get_energyThrToAddAsPIC())
//			add_particles(sec_n, un); // Create PIC argon neutral
		add_product(sec_n,un);
		un = DELETE_PARTICLE; // Delete original neutral (if a particle)
	}
	if (iSpecies->get_q() == 0 && sec_i) 	// for neutral-neutral scattering, iSpecies is also a neutral species, MAL 11/26/10
	{
//		add_particles(sec_i, ui); // Create PIC argon ion
		add_product(sec_i, ui);
		ui = DELETE_PARTICLE; // Delete original incident neutral (if a particle)
		//		printf("\n  ui1=%g, ui2=%g, ui3=%g", ui.e1(), ui.e2(), ui.e3()); // for debug, MAL 12/27/10
	}
	if (sec_e)
	{
		// 	Scalar ReleasedEnergy = cx.get_threshold();
		Scalar espeed = sqrt(2.0*ReleasedEnergy*PROTON_CHARGE/ELECTRON_MASS)/sec_e->get_dxdt();
		phi = TWOPI * frand();
		Scalar coschi = 1.0 - 2.0 * frand();
		//   Scalar sinchi = sqrt(1-pow(coschi,2));
		Scalar sinchi = sqrt(abs(1-coschi*coschi)); // prevent seg faults? MAL191217
		Vector3 uee = Vector3(sinchi*cos(phi), sinchi*sin(phi), coschi);
		uee *= espeed;
//		add_particles(sec_e,uee);
		add_product(sec_e, uee);
		//  	printf("\n  sec_i=%s, sec_n=%s, sec_e=%s, ReleasedEn=%g eV, Ee=%g eV\n", sec_i->get_name().c_str(), sec_n->get_name().c_str(),
		//   sec_e->get_name().c_str(), ReleasedEnergy, sec_e->get_energy_eV(sec_e->get_dxdt()*uee)); // FOR DEBUG, MAL 12/27/10
	}
}


void MCC::samemassElastic(const Xsection& cx)
	// Non-relativistic, isotropic ion-neutral or neutral-neutral elastic scattering for equal masses mi = mn
{
	//	if (is_thesame(iSpecies,nSpecies)) printf("samemassElastic: ion=%s, neutral=%s, new_neut_en=%g eV, en_thr=%g eV, q=%g\n", iSpecies->get_name().c_str(), nSpecies->get_name().c_str(),
	//		nSpecies->get_energy_eV(nSpecies->get_dxdt()*un), nSpecies->get_energyThrToAddAsPIC(),nSpecies->get_q()); // FOR DEBUG, MAL 11/23/09
	//	printf("\nsamemassElastic: nSpecies=%p (%s), iSpecies=%p (%s)", (void*)nSpecies, nSpecies->get_name().c_str(),
	//		(void*)iSpecies, iSpecies->get_name().c_str());
	//  printf("\n  un1=%g, un2=%g, un3=%g, ui1=%g, ui2=%g, ui3=%g", un.e1(), un.e2(), un.e3(), ui.e1(), ui.e2(), ui.e3());

	// We move to the frame of the neutral here.
	Vector3 ui_init = ui;
	ui -= un;
	//  Scalar coschi = sqrt(frand()); // to try to remove seg fault, MAL191217
	Scalar coschi = sqrt(abs(frand())); // add to try to remove seg fault, MAL191217
	Scalar chi = acos(coschi);
	Scalar phi = TWOPI * frand();
	v = ui.magnitude() * coschi;
	ui = v * newcoordiante(ui, chi, phi) + un;
	un +=  ui_init - ui;
	un = add_or_delete_neutral(*nSpecies,un); // add, MAL 11/20/11
	ui = add_or_delete_neutral(*iSpecies,ui); // iSpecies could be a neutral; add, MAL 11/20/11
}

void MCC::halfmassElastic(const Xsection& cx)
	// Non-relativistic, isotropic ion-neutral or neutral-neutral elastic scattering for mi = 0.5*mn
	// added by JTG May 19 2009
{
	// We move to the frame of the neutral here.
	Vector3 ui_init = ui;
	ui -= un;
	Scalar random = frand();
	Scalar chi = atan(sqrt(random*(1-random))/(.75 -random)); if(chi < 0.0) chi += M_PI;
	if (random >= 1.0) chi = M_PI; // Special case of CM scat angle theta = pi, MAL 11/21/09
	Scalar phi = TWOPI * frand();
	Scalar coschi = cos(chi);
	v = ui.magnitude() * (coschi +sqrt(coschi*coschi+3))/3.0;
	ui = v * newcoordiante(ui, chi, phi) + un;
	un +=  0.5 * (ui_init - ui);
	un = add_or_delete_neutral(*nSpecies,un); // add, MAL 11/20/11
	ui = add_or_delete_neutral(*iSpecies,ui); // iSpecies could be a neutral; add, MAL 11/20/11
}

void MCC::anymassElastic(const Xsection& cx, Species* si, Species* sn)
	// Non-relativistic, isotropic ion-neutral or neutral-neutral elastic scattering. Works for particles of any mass ratio mi/mn
	// added by MAL 11/20/09
{
	Scalar mi = 0;
	Scalar mn = 0;
	Scalar mu;
	Scalar mratio;
	if(si) mi = si->get_m();
	if(sn) mn = sn->get_m();
	if(mi>0 && mn>0)
	{
		mu = mi*mn/(mi+mn); // reduced mass
		mratio = mi/mn;
	}
	else error("mi and mn must be nonzero in anymassElastic");
	Vector3 ui_init = ui; // Save initial ion velocity
	// We move to the frame ("lab frame") of the neutral here.
	// NOTE: here the "lab frame" really means the target rest frame; MAL 9/25/12
	Vector3 ui_lab = ui-un;
	Scalar vi_lab = ui_lab.magnitude();
	Scalar Vcm = mu*vi_lab/mn; // Center of mass speed in the lab system
	Scalar vilabVcm = vi_lab - Vcm; // Initial (and final) speed in CM system

	// Find the scattering angle chi in the lab frame
	Scalar chi = 0.0;
	Scalar costheta=1.0-2.0*frand(); // theta is the CM scattering angle
	if (costheta <= -1.0) // Special case of theta = pi
	{
		if (mratio < 1.0) chi = M_PI;
		else if (mratio == 1.0) chi = M_PI/2.0;
	}
	else if (mratio + costheta == 0.0) // Special case of chi = pi/2
		chi = M_PI/2.0;
	else
	{
		Scalar temp1 = 1.0-costheta*costheta;
		Scalar sintheta = 0.0;
		if (temp1 > 0.0) sintheta=sqrt(temp1);
		chi = atan(sintheta/(mratio+costheta)); if(chi < 0.0) chi += M_PI;
	}
	Scalar phi = TWOPI*frand();
	// Find the magnitude viprime_lab of the scattered ion velocity in the lab frame
	Scalar viprime_lab = sqrt(vilabVcm*vilabVcm + Vcm*Vcm + 2*vilabVcm*Vcm*costheta);
	ui = viprime_lab*newcoordiante(ui_lab, chi, phi) + un; // new ion velocity
	un += (mi/mn)*(ui_init - ui); // From conservation of momentum, new neutral velocity
	un = add_or_delete_neutral(*nSpecies,un); // add, MAL 11/20/11
	ui = add_or_delete_neutral(*iSpecies,ui); // iSpecies could be a neutral; add, MAL 11/20/11
}

// For reactions with no secondary species (elastic scattering, charge transfer, etc) MAL200211
// Modified from previous version to deal with fluid, as well as particle species
// Adds or deletes a neutral if neutral energy is above or below energyThrToAddAsPIC after the collision, MAL 11/20/11
// Changed to return Vector 3 so that we delete the particle, HH 03/17/16
Vector3 MCC::add_or_delete_neutral(Species& speciesNeutral, Vector3 uNeutral)
{
	if (speciesNeutral.get_q()) { return uNeutral; } // a charged species; no additions or mcc subtractions
	else // a neutral species
	{
		if(speciesNeutral.get_energy_eV(speciesNeutral.get_dxdt()*uNeutral) > speciesNeutral.get_energyThrToAddAsPIC())
		{
			if (fSpecies != &speciesNeutral)
			{
				return uNeutral;
			} // a high energy particle species; no additions
			else // a high energy fluid species, so add particles
			{
				add_particles(&speciesNeutral, uNeutral); uNeutral = DELETE_PARTICLE; return uNeutral;
			}
		}
		else // a low energy species
		{
			if (fSpecies != &speciesNeutral) // a low energy particle species; try to add to first evolving fluid encountered
			{
				uNeutral = DELETE_PARTICLE; // mark species for deletion in mcc
				oopicList<Fluid> *flist = speciesNeutral.get_fluidList();
				int nFluids = flist->nItems();
				if (!nFluids) return uNeutral;
				oopicListIter<Fluid> fIter(*flist);
				for (fIter.restart(); !fIter.Done(); fIter ++)
				{
					if (fIter()->get_evolvefluid()) {
						fIter()->add_or_subtract_density(w_ref, x);
						break;
					} // add to first evolving fluid
				}
				return uNeutral; // species marked for deletion in the mcc
			}
			else // a low energy fluid species
				return uNeutral; // no additions or mcc subtractions
		}
	}
}

/*
// Adds or deletes a neutral if neutral energy is above or below energyThrToAddAsPIC after the collision, MAL 11/20/11
// Changed to return Vector 3 so that we delete the particle, HH 03/17/16
Vector3 MCC::add_or_delete_neutral(Species& speciesNeutral, Vector3 uNeutral){
	if(!speciesNeutral.get_q()){ // it is a neutral species
		if(speciesNeutral.get_energy_eV(speciesNeutral.get_dxdt()*uNeutral) > speciesNeutral.get_energyThrToAddAsPIC()){
			add_particles(&speciesNeutral, uNeutral);
		}
		uNeutral = DELETE_PARTICLE;
	}
	return uNeutral;
}
*/

void MCC::BackwardsAnymassElastic(const Xsection& cx, Species* si, Species* sn)
	// Non-relativistic, anisotropic ion-neutral or neutral-neutral elastic scattering. Works for particles of any mass ratio mi/mn
	// added by SAS July 8 2014
{
	Scalar mi = 0;
	Scalar mn = 0;
	Scalar mu;
	Scalar mratio;
	if(si) mi = si->get_m();
	if(sn) mn = sn->get_m();
	if(mi>0 && mn>0)
	{
		mu = mi*mn/(mi+mn); // reduced mass
		mratio = mi/mn;
	}
	else error("mi and mn must be nonzero in anymassElastic");

	Vector3 ui_init = ui; // Save initial ion velocity
	// We move to the frame ("lab frame") of the neutral here.
	// NOTE: here the "lab frame" really means the target rest frame; MAL 9/25/12
	Vector3 ui_lab = ui-un;
	Scalar vi_lab = ui_lab.magnitude();
	Scalar Vcm = mu*vi_lab/mn; // Center of mass speed in the lab system
	Scalar vilabVcm = vi_lab - Vcm; // Initial (and final) speed in CM system

	// Find the scattering angle chi in the lab frame
	Scalar chi = 0.0;

	if (mratio < 1.0) chi = M_PI;
	else if (mratio == 1.0) chi = M_PI/2.0;

	Scalar phi = TWOPI*frand();
	// Find the magnitude viprime_lab of the scattered ion velocity in the lab frame
	Scalar viprime_lab = sqrt(vilabVcm*vilabVcm + Vcm*Vcm - 2*vilabVcm*Vcm);
	ui = viprime_lab*newcoordiante(ui_lab, chi, phi) + un; // new ion velocity
	un += (mi/mn)*(ui_init - ui); // From conservation of momentum, new neutral velocity

	un = add_or_delete_neutral(*nSpecies,un); // add, MAL 11/20/11
	ui = add_or_delete_neutral(*iSpecies,ui); // iSpecies could be a neutral; add, MAL 11/20/11
}


void MCC::BackwardsSamemassElastic(const Xsection& cx)
	// Non-relativistic, isotropic ion-neutral or neutral-neutral elastic scattering. Works for particles of same mass
	// added by SAS July 8 2014
{

	Vector3 ui_init = ui;
	ui -= un;
	Scalar coschi = 0;
	Scalar chi = M_PI/2.0;
	Scalar phi = TWOPI * frand();
	v = ui.magnitude() * coschi;
	ui = v * newcoordiante(ui, chi, phi) + un;
	un +=  ui_init - ui;

	un = add_or_delete_neutral(*nSpecies,un); // add, MAL 11/20/11
	ui = add_or_delete_neutral(*iSpecies,ui); // iSpecies could be a neutral; add, MAL 11/20/11
}


// --- Ionization processes:
void MCC::eIonization(const Xsection& cx)
{
	// We don't subtract the velocity of the neutral: assumed the electron has a much higher velocity: energy, ue are valid.
	//  printf("\neIonization: eSpecies=%p (%s), nSpecies=%p (%s), sec_i=%p (%s)", (void*)eSpecies, eSpecies->get_name().c_str(),
	//		(void*)nSpecies, nSpecies->get_name().c_str(), (void*)sec_i, sec_i->get_name().c_str());
	//	printf("\n  ue1=%g, ue2=%g, ue3=%g, un1=%g, un2=%g, un3=%g, electronenergy=%g", ue.e1(), ue.e2(), ue.e3(), un.e1(), un.e2(), un.e3(), energy);
	Scalar thres = cx.get_threshold();
	ionization(thres);
	// Creation of ionized species
	if (sec_i)
	{
//		add_particles(sec_i, un);
		add_product(sec_i, un);
	}
	//Delete the neutral:
	// mindgame: awkward way
	un = DELETE_PARTICLE;
	//  printf("\n  ue1=%g, ue2=%g, ue3=%g, un1=%g, un2=%g, un3=%g, electronenergy=%g", ue.e1(), ue.e2(), ue.e3(), un.e1(), un.e2(), un.e3(),
	//		eSpecies->get_energy_eV(eSpecies->get_dxdt()*ue));
}
/*
// USE xpdp1 method for ionization, MAL 8/24/12
void MCC::ionization(const Scalar threshold)
{
Scalar tengy, tvel, coschi, chi, phi;
Vector3 uEjected;
// First, calculate the energy and scattering angles of the ejected electron
tengy = 10.0*tan(frand()*atan((energy-threshold)/20.0)); //ejected electron energy in eV, in system with v_neutral=0, fromxpdp1 oxygenmcc.c
tvel = eSpecies->get_speed2(tengy)/eSpecies->get_dxdt(); //relative velocity in grid units
//	printf("\nmcc.cpp::ionization entered, energy=%g, tengy=%g, tvel=%g",energy,tengy,tvel);
if (tengy < 1E-30) coschi = 1;
else coschi = (tengy + 2 - 2*pow(tengy+1,frand()))/tengy; // from oxygenmcc.c::onewvel in xpdp1, MAL, 8/24/12
chi = acos(coschi);
phi = TWOPI*frand();
//  uEjected = tvel*newcoordiante(ue-un,chi,phi) + un; // add un to transform back to lab system, MAL 8/30/12
uEjected = tvel*newcoordiante(ue,chi,phi); // to test for no lab-CM xform for electrons, MAL 9/2/12
//  printf(", |ue|=%g, |uEjected|=%g",ue.magnitude(),uEjected.magnitude());
if (sec_e)  //Creations necessarily as particles and with the default weight.
{
//		printf("\nmcc::ionization, adding secondary electron sec_e=%p (%s)\n", (void*)sec_e, sec_e->get_name().c_str());
add_particles(sec_e, uEjected);
}

// Next, calculate the energy and scattering angles of the primary electron
tengy = energy - threshold - tengy; // primary electron energy in eV, in system with v_neutral=0
tvel = eSpecies->get_speed2(tengy)/eSpecies->get_dxdt(); // relative velocity in grid units
if (tengy < 1E-30) coschi = 1;
else coschi = (tengy + 2 - 2*pow(tengy+1,frand()))/tengy; // from oxygenmcc.c::onewvel in xpdp1, MAL, 8/24/12
chi = acos(coschi);
phi = TWOPI*frand();
//  ue = tvel*newcoordiante(ue-un,chi,phi) + un; // add un to transform back to lab system, MAL 8/30/12
ue = tvel*newcoordiante(ue,chi,phi); // to test for no lab-CM xform for electrons, MAL 9/2/12
//	printf("\nmcc.cpp::ionization: tengy=%g, tvel=%g, new |ue|=%g\n",tengy,tvel,ue.magnitude());
}
 */

// /* KILL oopd1 IONIZATION METHOD, MAL 8/24/12
void MCC::ionization(const Scalar threshold)
{
	// First, calculate the energy of the ejected electron
	Scalar eEjected = ejectedEnergy(threshold, energy);

	// Next, calculate the scattering angles of both the primary electron and the ejected electron
	Scalar thetaP, phiP, thetaS, phiS;
	primarySecondaryAngles(threshold, energy, eEjected, thetaP, phiP, thetaS, phiS);
	Vector3 uEjected = newMomentum(ue, eEjected, thetaS, phiS);

	if (sec_e)  //Creations necessarily as particles and with the default weight.
	{
//				printf("\nmcc::ionization, adding secondary electron sec_e=%p (%s)\n", (void*)sec_e, sec_e->get_name().c_str());
//		add_particles(sec_e, uEjected);
		add_product(sec_e, uEjected); // test add_product; ionization called by oxygen_mcc, MAL200208
	}

	// Scatters the primary electron
	Scalar newEnergy = energy - threshold - eEjected;
	ue = newMomentum(ue, newEnergy, thetaP, phiP);
}
// */

//Method for calculating energy imparted to a secondary electron
Scalar MCC::ejectedEnergy(const Scalar& I, const Scalar& impactEnergy)
{
	// threshold in eV
	if (impactEnergy <= I) return 0.;
	Scalar tPlusMC = impactEnergy + ELECTRON_MASS_EV;
	Scalar twoTplusMC = impactEnergy + tPlusMC;
	Scalar tPlusI = impactEnergy + I;
	Scalar tMinusI = impactEnergy -I;
	Scalar invTplusMCsq = 1. / (tPlusMC * tPlusMC);
	Scalar tPlusIsq = tPlusI * tPlusI;
	Scalar iOverT = I / impactEnergy;
	Scalar funcT1 = 14. / 3. + .25 * tPlusIsq * invTplusMCsq
			- ELECTRON_MASS_EV * twoTplusMC * iOverT * invTplusMCsq;

	Scalar funcT2 = 5. / 3. - iOverT - 2. * iOverT * iOverT / 3.
			+ .5 * I * tMinusI * invTplusMCsq
			+ ELECTRON_MASS_EV * twoTplusMC * I * invTplusMCsq * log(iOverT) / tPlusI;
	Scalar aGreaterThan = funcT1 * tMinusI / funcT2 / tPlusI;

	// We may have to try several times before the "rejection method"
	// let us keep our ejected energy test value, wTest

	int needToTryAgain = 1;
	Scalar randomFW, wTest, wPlusI, wPlusIsq;
	Scalar invTminusW, invTminusWsq, invTminusW3;
	Scalar probabilityRatio;

	while (needToTryAgain == 1)
	{
		randomFW = frand() * aGreaterThan;
		wTest = I * funcT2 * randomFW / (funcT1 - funcT2 * randomFW);
		wPlusI = wTest + I;
		wPlusIsq = wPlusI * wPlusI;
		invTminusW = 1. / (impactEnergy - wTest);
		invTminusWsq = invTminusW * invTminusW;
		invTminusW3 = invTminusW * invTminusWsq;
		probabilityRatio = ( 1. + 4. * I / wPlusI / 3. + wPlusIsq
				* invTminusWsq + 4. * wPlusIsq * invTminusW3 / 3.
				-ELECTRON_MASS_EV * twoTplusMC * wPlusI * invTminusW * invTplusMCsq + wPlusIsq * invTplusMCsq) / funcT1;
		if (probabilityRatio >= (Scalar)frand()) needToTryAgain = 0;
	}

#ifdef DEBUG
	FILE *fp;
	ostring ffnname ="secondary.dat";
	fp = fopen(ffnname(), "a");
	fprintf(fp, "%g \t %g \t %g \n",wTest,impactEnergy,wTest/impactEnergy );
	fclose(fp);
#endif
	return wTest;
}

// Method for calculating the scattering angles of the impacting and ejected electrons
void MCC::primarySecondaryAngles( const Scalar& I, const Scalar& eImpact, const Scalar& eSecondary,
		Scalar& thetaPrimary, Scalar& phiPrimary, Scalar& thetaSecondary,
		Scalar& phiSecondary)
{
	// threshold in eV
	if (eImpact <= I)
	{
		thetaPrimary = 0.;
		thetaSecondary = 0.;
		phiPrimary = 0.;
		phiSecondary = 0.;
		return;
	}

	// First, we generate a Monte Carlo value for thetaPrimary
	Scalar ePrimary = eImpact - eSecondary - I;
	Scalar w = ePrimary;
	Scalar wPlusI = w + I;
	Scalar wPlusI2MC = wPlusI + 2. * ELECTRON_MASS_EV;
	Scalar tPlus2MC = eImpact + 2. * ELECTRON_MASS_EV;

	Scalar alpha;
	alpha = ELECTRON_MASS_EV / (eImpact + ELECTRON_MASS_EV);
	alpha = 0.6 * alpha;

	Scalar g2_T_W = sqrt( wPlusI * tPlus2MC / wPlusI2MC / eImpact );
	Scalar g3_T_W = alpha * sqrt( I * ( 1. - wPlusI / eImpact ) / w );

	Scalar fTheta = frand();
	thetaPrimary = acos( g2_T_W + g3_T_W * tan( ( 1. - fTheta )
			* atan( ( 1. - g2_T_W ) / g3_T_W ) - fTheta

			* atan( ( 1. + g2_T_W ) / g3_T_W)));
	if (g3_T_W == 0)
	{
		thetaPrimary = acos(0.);
	}
	phiPrimary = TWOPI * frand();

	// Third, we generate a Monte Carlo value for thetaSecondary
	w = eSecondary;
	wPlusI = w + I;
	wPlusI2MC = wPlusI + 2. * ELECTRON_MASS_EV;
	g2_T_W = sqrt( wPlusI * tPlus2MC / wPlusI2MC / eImpact );
	g3_T_W = alpha * sqrt( I * ( 1. - wPlusI / eImpact ) / w );
	fTheta = frand();

	// have to figure out the difference of atan and atan2
	thetaSecondary =  acos( g2_T_W + g3_T_W * tan( ( 1. - fTheta )
			* atan( ( 1. - g2_T_W ) / g3_T_W ) - fTheta
			* atan( ( 1. + g2_T_W ) / g3_T_W)));

	// Fourth, generate a monte Carlo value for phiSecondary
	Scalar eCritical = 10. * I;
	Scalar a_T_w = 0;
	if ( eSecondary>eCritical && ePrimary>eCritical )
	{
		a_T_w = sqrt((eSecondary - eCritical) * (ePrimary - eCritical)) / eCritical;
	}
	Scalar delta = TWOPI / (1. + a_T_w);
	phiSecondary = phiPrimary + M_PI + (frand() - .5) * delta;

#ifdef DEBUG
	FILE *fp;
	ostring ffnname = "ion_angle.dat";
	fp = fopen(ffnname(), "a");
	fprintf(fp, "%g \t %g \t %g \t %g \n", thetaPrimary, phiPrimary, thetaSecondary, phiSecondary);
	fclose(fp);
#endif
	return;
}

// --- Charge exchange between neutral and daughter ion (resonant charge exchange)

void MCC::ElectronExchange(const Xsection& cx)
{
	Vector3 temp = ui;
	ui = un; // new ion velocity
	un = temp; // new neutral velocity
	un = add_or_delete_neutral(*nSpecies,un); // add, MAL 11/20/11

}

// --- nonresonant charge exchange, add MAL 11/20/11
/* KILL this old one, as it does not do the energetics correctly
   void MCC::nonresElectronExchange(const Xsection & cx)
   {
	Vector3 temp = ui;
	ui = un; // new ion velocity
	un = temp; // new neutral velocity
	if (sec_n)
		add_or_delete_neutral(*sec_n,un); // add, MAL 11/20/11
	if (sec_i)
	{
		add_particles(sec_i, ui);
	}
	ui = DELETE_PARTICLE;
   }
 */
void MCC::nonresElectronExchange(Species * ProductIon, Species * ProductNeutral, Scalar ReleasedEnergy)
{
	// Added by MAL, 10/7/12
	// ReleasedEnergy = additional potential energy released in the CM system during the charge transfer (positive or negative)
	Scalar Mi = iSpecies -> get_m();
	Scalar Mn = nSpecies -> get_m();
	Scalar mu = 1./(1./Mi+1./Mn);
	Vector3 Vcm = (Mi*ui*iSpecies->get_dxdt()+Mn*un*nSpecies->get_dxdt())/(Mi+Mn);
	Vector3 vrel = ui - un;
	Vector3 unit_vrel = vrel.unit();
	Scalar Energycm = (0.5*Mi*(ui*iSpecies->get_dxdt()-Vcm).magnitude_squared() + 0.5*Mn*(un*nSpecies->get_dxdt()-Vcm).magnitude_squared())/PROTON_CHARGE; 
	Scalar Energycm_new = Energycm + ReleasedEnergy;
	Scalar MomentumChange = 0;
	if (Energycm_new > 0) MomentumChange = sqrt(2.0*PROTON_CHARGE*mu*Energycm_new);
	Vector3 vel_newn;
	Vector3 vel_newi;
	//		printf("\nMCC::nonresElectronExchange: Ecm=%g, ReleasedEnergy=%g, projectile=%s, Energy_p=%g eV\n", Energycm, ReleasedEnergy, iSpecies->get_name().c_str(), iSpecies->get_energy_eV(iSpecies->get_dxdt()*ui)); // FOR DEBUG, MAL 10/7/12
	//		printf("MCC::nonresElectronExchange: target=%s, Energy_t=%g eV\n", nSpecies->get_name().c_str(), nSpecies->get_energy_eV(nSpecies->get_dxdt()*un));
	if (ProductNeutral) 
	{ 
		vel_newn = (Vcm + (MomentumChange/Mi)*unit_vrel)/ProductNeutral->get_dxdt(); 
		// add particles if ProductNeutral meets the energy threshold condition, MAL 10/7/12
//		if ((ProductNeutral->get_q() == 0 && ProductNeutral->get_energy_eV(ProductNeutral->get_dxdt()*vel_newn) >
//					ProductNeutral->get_energyThrToAddAsPIC())) add_particles(ProductNeutral, vel_newn); // lab frame
		if(ProductNeutral->get_q() == 0) add_product(ProductNeutral, vel_newn);
		//  	printf("MCC::nonresElectronExchange: ProductNeutral=%s, Energyn=%g eV\n", ProductNeutral->get_name().c_str(), ProductNeutral->get_energy_eV(ProductNeutral->get_dxdt()*vel_newn));
	}
	if (ProductIon)
	{ 
		vel_newi = (Vcm - (MomentumChange/Mn)*unit_vrel)/ProductIon->get_dxdt();
		// add particles, MAL 10/7/12
//		add_particles(ProductIon, vel_newi);
		add_product(ProductIon, vel_newi);
	}   
	//  	printf("MCC::nonresElectronExchange: ProductIon=%s, Energyi=%g eV\n", ProductIon->get_name().c_str(), ProductIon->get_energy_eV(ProductIon->get_dxdt()*vel_newi));
	//		printf("MCC::nonresElectronExchange: KEproducts-Kereactants=%g eV\n", ProductNeutral->get_energy_eV(ProductNeutral->get_dxdt()*vel_newn) +
	//			ProductIon->get_energy_eV(ProductIon->get_dxdt()*vel_newi) - iSpecies->get_energy_eV(iSpecies->get_dxdt()*ui) - nSpecies->get_energy_eV(nSpecies->get_dxdt()*un));
	ui = DELETE_PARTICLE; // delete projectile ion
	un = DELETE_PARTICLE; // delete target neutral
}

// --- Subfunctions of general use (for most of the processes): 


// Calculate the  new momentum
Vector3 MCC::newMomentum(const Vector3& U_initial, 
		const Scalar& newEnergy, const Scalar& theta, const Scalar& phi)
	// Comment (07/06): Is it not the velocity, not the momentum which is returned?

{
	/*
	   Scalar gMin1 = newEnergy / ELECTRON_MASS_EV;
	   Scalar u0new = SPEED_OF_LIGHT * sqrt(1-1/(gMin1+1)*1/(gMin1+1));
	 */

	Scalar u0new = eSpecies->get_speed2(newEnergy);
	u0new /= eSpecies->get_dxdt();

#ifdef DEBUG
	ostring ffnname = "kinetic_energy.dat";
	FILE *fp;
	Vector3 uuu = U_initial; // define uuu, MAL 11/16/10
	fp = fopen(ffnname(), "a");
	fprintf(fp,"before vel =%g \t after vel =%g \n ",uuu.magnitude(), u0new);
	fclose(fp);
#endif
	return u0new *  newcoordiante(U_initial , theta, phi);
}

// NEW VERSION,  cosPhi0 and sinPhi0 WERE NOT CORRECTLY CALCULATED IN  THE OLD VERSION IN oopd1
// USE THIS VERSION!!! MAL 9/3/09
// Return the Coordiante normalized to 1, from emission energy and  angles (purely geometric function)
Vector3 MCC::newcoordiante(Vector3 vel , Scalar theta, Scalar phi)  
	//corrected MAL 9/3/09
{
	Vector3 u0norm = vel.unit();
	Scalar cosT0 = u0norm.e3();
	Scalar sinT0 = sqrt(1. - cosT0 * cosT0);
	Scalar cosPhi0 = 0;
	Scalar sinPhi0 = 0;
	if(sinT0 != 0)
	{
		cosPhi0 = u0norm.e1()/sinT0;
		sinPhi0 = u0norm.e2()/sinT0;
	}
	Scalar cosT = cos(theta);
	Scalar sinT = sin(theta);
	Scalar cosPhi = cos(phi);
	Scalar sinPhi = sin(phi);
	Scalar e1new = (cosPhi * sinT * cosPhi0 * cosT0 - sinPhi * sinT *
			sinPhi0 + cosT * cosPhi0 * sinT0);
	Scalar e2new = (cosPhi * sinT * sinPhi0 * cosT0 + sinPhi * sinT *
			cosPhi0 + cosT * sinPhi0 * sinT0);
	Scalar e3new = (-cosPhi * sinT * sinT0 + cosT * cosT0);
	//fprintf(stderr,"x enwe =%g\t ynew =%g\t znew =%g\n", e1new, e2new, e3new);
	//  printf("\ne1new =%g\t e2new =%g\t e3new =%g\t sumofsq=%g\n", e1new, e2new, e3new, sumofsquares);

#ifdef DEBUG
	Scalar e1 = (cosPhi * sinT * cosPhi0 * cosT0 - sinPhi * sinT *
			sinPhi0 + cosT * cosPhi0 * sinT0)*(cosPhi * sinT * cosPhi0 * cosT0 -
					sinPhi * sinT * sinPhi0 + cosT * cosPhi0 * sinT0);
	Scalar e2 =  (cosPhi * sinT * sinPhi0 * cosT0 + sinPhi * sinT *
			cosPhi0 + cosT * sinPhi0 * sinT0)* (cosPhi * sinT * sinPhi0 * cosT0 +
					sinPhi * sinT * cosPhi0 + cosT * sinPhi0 * sinT0);
	Scalar e3 = (-cosPhi * sinT * sinT0 + cosT * cosT0)*(-cosPhi * sinT
			* sinT0 + cosT * cosT0);

	fprintf(stderr,"sum in newmomentum = %g \n",sqrt(e1+e2+e3));
#endif
	Vector3 eNew(e1new, e2new, e3new);
	return eNew;
}

void MCC::add_particles (Species* sp,  const Vector3 & vel)
{
#ifdef DEBUGmccdynamic
  printf("\nEntering MCC::add_particles");
#endif
	// mindgame: the better way is to get nc2p from the inputfile
	if(is_fluidfluid || sp->use_SecDefaultWeight() || useSecDefaultWeight) // local variable set in Species, global variable set in MCC, MAL 10/15/09; ADD is_fluidfluid, must always use np2c, MAL200215
	{
		Scalar defaultweight = sp->get_np2c();
		Scalar weight_ratio = w_ref/defaultweight;
		int creations = (int) weight_ratio;
#ifdef DEBUGmccdynamic
    printf("\nMCC::add_particles: sp->useSecDefaultWeight=%i, useSecDefaultWeight=%i, w_ref=%g, defaultweight (sp np2c)=%g, weight_ratio=%g, creations=%i", sp->use_SecDefaultWeight(), useSecDefaultWeight, w_ref, defaultweight, weight_ratio, creations);
#endif
		weight_ratio -= creations;
		if (frand()< weight_ratio) creations ++;
#ifdef DEBUGmccdynamic
    printf("\nMCC::add_particles: (final values) weight_ratio=%g, creations=%i",weight_ratio,creations);
#endif
		//      while (creations --) sp->add_particle(x, vel.e1(), vel.e2(), vel.e3(),false, false, defaultweight);
		while (creations --) sp->add_particle(x, vel.e1(), vel.e2(), vel.e3(),false, false, defaultweight);
	}
	else
		sp->add_particle(x, vel.e1(), vel.e2(), vel.e3(),false, false, w_ref);
	//    sp->add_particle(x, vel.e1(), vel.e2(), vel.e3(),false, true, w_ref); //set E[i] when created, MAL 89/1/12
/*
	if( strcmp("el", sp->get_name().c_str()) == 0 ) {
		printf("sp name: %s; number of groups: %d\n", sp->get_name().c_str(), sp->get_ParticleGroupList()->nItems() );

		oopicListIter<ParticleGroup > thei(*(sp->get_ParticleGroupList()));
		int jj = 0;

		for(thei.restart(); !thei.Done(); thei++, jj++) {
			bool allOK = true;

			for( int i=0 ; i < thei()->get_n() && allOK ; i++) {
				allOK = (thei()->get_w(i) == thei()->get_w0() );
			}

			printf("  - %d: %d, w0=%g, allOK = %s\n", jj, thei()->get_n(), thei()->get_w0(), allOK ? "yes" : "NO!");
		}

	}
*/

#if 0
	printf("sp name: %s; number of groups: %d\n", sp->get_name().c_str(), sp->get_ParticleGroupList()->nItems() );

	oopicListIter<ParticleGroup > thei(*(sp->get_ParticleGroupList()));
	int jj = 0;

	for(thei.restart(); !thei.Done(); thei++, jj++) {
		bool allOK = true;

		for( int i=0 ; i < thei()->get_n() && allOK ; i++) {
			allOK = (thei()->get_w(i) == thei()->get_w0() );
		}

		printf("  - %d: %d, w0=%g, allOK = %s\n", jj, thei()->get_n(), thei()->get_w0(), allOK ? "yes" : "NO!");
	}
#endif
}

void MCC::add_product(Species* sp, const Vector3 & vel)  // adds evolving fluids as well as particles, MAL200208
{
	if (sp->get_q() != 0) { add_particles(sp, vel); return;} // charged particles always added as PIC species
	if(sp->get_energy_eV(sp->get_dxdt()*vel) > sp->get_energyThrToAddAsPIC()){
	add_particles(sp, vel); return; } // add high energy neutrals as PIC species
	oopicList<Fluid> *flist = sp->get_fluidList();
	int nFluids = flist->nItems();
	if (!nFluids) return; // there are no fluids
	oopicListIter<Fluid> fIter(*flist);
	for (fIter.restart(); !fIter.Done(); fIter ++) {
		// A work in progress; add density to the first evolving fluid encountered
		// There will usually be only one such fluid
		// To be done: if more than one evolving fluid, then choose the one best matching vel?
		if (fIter()->get_evolvefluid()) { fIter()->add_or_subtract_density(w_ref, x); break; }
	}
}

