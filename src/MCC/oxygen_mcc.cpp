// JTG and MAL melded file, 10/12/09
// Final file approved by JTG and MAL, 12/2/09
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "xsections/xsection.hpp"
#include "reactions/reaction.hpp"
#include "MCC/mcc.hpp"
#include "MCC/oxygen_mcc.hpp"
#include "fields/fields.hpp" //add for debug, MAL 9/6/12

OXYGEN_MCC::OXYGEN_MCC(oopicList<Species>* slist, ReactionList _reactionlist, Scalar _dt, Fields *tf) : MCC(slist, _reactionlist, _dt, tf)
{
  setdefaults();
}

void OXYGEN_MCC::setdefaults()
{
  sec_O2_sing_delta = sec_O2_sing_sigma = sec_O_1D = sec_O_1S = NULL;
  sec_O2_rot= sec_O2_vib1 = sec_O2_vib2 = sec_O2_vib3 = sec_O2_vib4 = sec_O2_lexc = NULL;
  sec_O = sec_O2 = sec_O_neg = sec_O2_ion = sec_O_ion = NULL;
}

void OXYGEN_MCC::init(Species *_reactant1, Species *_reactant2)
{
  init_mcc(_reactant1, _reactant2);
   for (int i=reactionlist.num()-1; i>=0; i--)
  {
    switch (reactionlist.data(i)->get_reactiontype())
  {
		case E_ELASTIC: // add for DEBUG, MAL 9/6/12
		Nelastic=0;
//		printf("\noxygen_mcc.cpp::init: Initialize Nelastic=%i",Nelastic);
		break;
		case Ch_EXCHANGE:
		Nchargetransfer=0;
//		printf("\noxygen_mcc.cpp::init: Initialize Nchargetransfer=%i",Nchargetransfer);
		break;
    case E_DETACHMENT:
//			sec_e = reactionlist.data(i)->get_products(2); //no products(2) for this reaction!  MAL 8/11/12
      sec_e = reactionlist.data(i)->get_products(1); //Invert order as in oxygen_gas.hpp MAL 4/12/09
      sec_O = reactionlist.data(i)->get_products(0);
      break;   
   case EDISSIZ:
       sec_e = reactionlist.data(i)->get_products(2);
       sec_O_ion = reactionlist.data(i)->get_products(1);
       sec_O = reactionlist.data(i)->get_products(0);
      break;
      case E_O2IONIZATION:
      Nionization = 0;
//      sec_e = reactionlist.data(i)->get_products(2); // Added by JTG September 3 2009; no products(2) for this reaction!  MAL 8/11/12
      sec_e = reactionlist.data(i)->get_products(1);
      sec_O2_ion = reactionlist.data(i)->get_products(0);
//		  printf("\noxygen_mcc.cpp::init: Initialize Nionization=%i\n",Nionization);
      break;
      case E_OIONIZATION:
//      sec_e = reactionlist.data(i)->get_products(2); // Added by JTG August 18 2009; no products(2) for this reaction!  MAL 8/11/12
      sec_e = reactionlist.data(i)->get_products(1);
      sec_O_ion = reactionlist.data(i)->get_products(0);
      break;
    case E_ROTATIONAL:
      sec_O2_rot = reactionlist.data(i)->get_products(0);
      break;
     case E_LEXCITATION:
     sec_O_1D = reactionlist.data(i)->get_products(0);
//     sec_O_1D = reactionlist.data(i)->get_products(1); // not needed, since set in line above, MAL 8/11/12
     break;
   case E_LOSS1:
     sec_O2 = reactionlist.data(i)->get_products(0); // not needed, since ELOSS1 has no secondary species, MAL 8/11/12
     break;
   case E_LOSS2:
     sec_O = reactionlist.data(i)->get_products(0);
//     sec_O = reactionlist.data(i)->get_products(1); // not needed, since set in line above, MAL 8/11/12
     break;
    case E_LOSS3:
     sec_O = reactionlist.data(i)->get_products(0);
     sec_O_1D = reactionlist.data(i)->get_products(1);
     break;
     case E_LOSS4:
     sec_O_1D = reactionlist.data(i)->get_products(0);
//     sec_O_1D = reactionlist.data(i)->get_products(1); // not needed, since set in line above, MAL 8/11/12
     break;
    case E_EXCITATION1:
      sec_O2_vib1 = reactionlist.data(i)->get_products(0);
      break;
    case E_EXCITATION2:
      sec_O2_vib2 = reactionlist.data(i)->get_products(0);
      break;
    case E_EXCITATION3:
      sec_O2_vib3 = reactionlist.data(i)->get_products(0);
      break;
    case E_EXCITATION4:
      sec_O2_vib4 = reactionlist.data(i)->get_products(0);
      break;
    case E_SINGLET_DELTA:
      sec_O2_sing_delta = reactionlist.data(i)->get_products(0);
      break;
    case E_SINGLET_DELTAdex:  
      sec_O2 = reactionlist.data(i)->get_products(0);  // JTG January 9 2013
      break;
    case E_SINGLET_SIGMAdex:  
      sec_O2 = reactionlist.data(i)->get_products(0);  // HH February 9 2016
      break;
    case E_SINGLET_SIGMA:
      sec_O2_sing_sigma = reactionlist.data(i)->get_products(0);
      break;
    case E_EXC1D:
      sec_O_1D = reactionlist.data(i)->get_products(0);
      break;
     case E_EXC1Ddex:
      sec_O = reactionlist.data(i)->get_products(0);
      break;
     case E_EXC1S:
      sec_O_1S = reactionlist.data(i)->get_products(0);
      break;
    case NEG_ELASTIC:
      break;
    case POSO_ELASTIC:
     break;
    case POS_ELASTIC:
      break;
    case NEUO_ELASTIC:  // Added by JTG May 26 2009
      break;
    case NEG_DETCH:
      sec_e = reactionlist.data(i)->get_products(2);
      sec_O2 = reactionlist.data(i)->get_products(0);
      sec_O = reactionlist.data(i)->get_products(1);
      break;
    case O2FRAG:   // Added by JTG August 17 2009
      sec_O2 = reactionlist.data(i)->get_products(0);
      sec_O = reactionlist.data(i)->get_products(1);
      sec_O_ion = reactionlist.data(i)->get_products(2);
      break;
    case POLARDISS:   // Added by JTG July 28 2009
      sec_e = reactionlist.data(i)->get_products(2);
      sec_O_neg = reactionlist.data(i)->get_products(0);
      sec_O_ion = reactionlist.data(i)->get_products(1);
      break;
    case NEGO_DETACHMENT:   // Added by JTG July 17 2009
      sec_e = reactionlist.data(i)->get_products(1);
      sec_O2 = reactionlist.data(i)->get_products(0);
      break;
    case MUTUAL_NEUTRALIZATION:
      sec_O = reactionlist.data(i)->get_products(1);
      sec_O2 = reactionlist.data(i)->get_products(0);
      break;
      case MUTUAL_NEUTRALIZATIONO:
          Nmutneut = 0;
          sec_O = reactionlist.data(i)->get_products(0);
//      sec_O = reactionlist.data(i)->get_products(1); // not needed, as set in previous line, MAL 8/11/12
    break;
    case O1DTOO2b:
      sec_O = reactionlist.data(i)->get_products(1);
      sec_O2_sing_sigma = reactionlist.data(i)->get_products(0);
      break;
     case O2bO2quench:
      sec_O2 = reactionlist.data(i)->get_products(1);
      sec_O2 = reactionlist.data(i)->get_products(0);
      break;
    case E_DISS_ATTACHMENT:
          Ndissattach = 0;
          sec_O = reactionlist.data(i)->get_products(1); //Invert order as in oxygen_gas.hpp MAL 4/12/09
      sec_O_neg = reactionlist.data(i)->get_products(0);
      break;
    case E_DISS_RECOMBINATION:
      sec_O = reactionlist.data(i)->get_products(0);
      sec_O_1D = reactionlist.data(i)->get_products(1);
      break;
    case P_eEXCHANGE:  // Added by JTG June 25 2009
        sec_O = reactionlist.data(i)->get_products(1);
        sec_O2_ion = reactionlist.data(i)->get_products(0);
				break;
      case P_e2EXCHANGE:  // Added by JTG October 15 2014
        sec_O2 = reactionlist.data(i)->get_products(1);
        sec_O2_ion = reactionlist.data(i)->get_products(0);
				break;
    case P2_eEXCHANGE:  // Added by JTG July 26 2009
        sec_O2 = reactionlist.data(i)->get_products(0);
        sec_O_ion = reactionlist.data(i)->get_products(1);
				break;
     case O2O2_ELASTIC:
      break;
    case OO_ELASTIC:
      break;
    default:
      break;	
    }
  }
}

void OXYGEN_MCC::dynamic(Reaction * _reaction)
{
Reaction * reaction = _reaction;
const Xsection  & cx  = *(reaction->get_xsection());
ReactionType reactiontype = reaction->get_reactiontype();
//  printf("\noxygen_mcc::ReactionType: %s\n", ReactionTypeName(reactiontype).c_str());
#ifdef DEBUGmcc
  totalnumberofcollisions ++;
  printf("\nReactionType: %s\n", ReactionTypeName(reactiontype).c_str());
#endif  
if (energy > cx.get_threshold()) {
  switch(reactiontype)
    {
      // --- Scattering processes:
    case E_ELASTIC:
//			Nelastic ++; // for debug, also for other important oxygen reactions below, MAL 12/25/12
//			Nsteps=(int)(thefield->get_nsteps());
//			printf("Nsteps=%i, Nelastic=%i\n",Nsteps,Nelastic);
      eElastic(cx);
      break;
     case EO_ELASTIC:  
//             fprintf(stderr,"elastic\n");
      eElastic(cx);
      break;
    case E_EXCITATION1:
//             fprintf(stderr,"excitation1\n");
      eExcitation1(cx);
      break;
    case E_EXCITATION2:
//            fprintf(stderr,"excitation2\n");
      eExcitation2(cx);
      break;
    case E_EXCITATION3:
//             fprintf(stderr,"excitation3\n");
      eExcitation3(cx);
      break;
    case E_EXCITATION4:
//             fprintf(stderr,"excitation4\n");
      eExcitation4(cx);
      break;
    case E_LEXCITATION:
//             fprintf(stderr,"lexcitation\n");
      elExcitation(cx);
      break;
    case E_SINGLET_DELTA:
//           fprintf(stderr,"delta\n");
      esingdelta(cx);
      break;
      case E_SINGLET_DELTAdex:
//           fprintf(stderr,"delta\n");
      esingdeltadex(cx);
      break;
      case E_SINGLET_SIGMAdex:
//           fprintf(stderr,"sigma\n");
      esingsigmadex(cx);
      break;
    case E_SINGLET_SIGMA:
//             fprintf(stderr,"sigma\n");
      esingsigma(cx);
      break;
    case E_LOSS1:
 //            fprintf(stderr,"loss1\n");
      eloss(cx);
      break;
    case E_LOSS2:
 //            fprintf(stderr,"loss2\n");
      eloss2(cx);  // Changed JTG July 26 2009
      break;
    case E_LOSS3:
 //            fprintf(stderr,"loss3\n");
      eloss3(cx);
      break;
    case E_LOSS4:
 //            fprintf(stderr,"loss4\n");
      eloss4(cx);
      break;
    case E_ROTATIONAL: //processes are same
//            fprintf(stderr,"rotation\n");
      erotational(cx);
      break;
    case E_EXC1D:
//          fprintf(stderr,"delta\n");
      eexc1d(cx);
      break;
     case E_EXC1Ddex:
//          fprintf(stderr,"delta\n");
      eexc1ddex(cx);
      break;
     case E_EXC1S:
//          fprintf(stderr,"delta\n");
      eexc1s(cx);
      break;
     case E_EXC3P0:
//          fprintf(stderr,"delta\n");
      eloss(cx);
      break;
      case E_EXC5S0:
//          fprintf(stderr,"delta\n");
      eloss(cx);
      break;
     case E_EXC3S0:
//          fprintf(stderr,"delta\n");
      eloss(cx);
      break;
      //--- Ionizing processes:
         case E_IONIZATION:
//       fprintf(stderr,"ionizationo\n");
       eIonizationo(cx);
      break;
      // --- Ionizing processes:
       case E_O2IONIZATION:
//			Nionization ++;
//			Nsteps=(int)(thefield->get_nsteps());
//			printf("Nsteps=%i, Nionization=%i\n",Nsteps,Nionization);
//	    fprintf(stderr,"\n************oxygen_mcc.cpp::dynamic: eIonizationo2 called\n");
        eIonizationo2(cx);
       break;
        case E_OIONIZATION:
//	    fprintf(stderr,"\nionizationo\n");
        eIonizationo(cx);
       break;
    case EDISSIZ:
//        fprintf(stderr,"dissociative ionization\n");
      dissionization(cx);
      break;

     // --- Recombinations:
    case E_DETACHMENT:
//       	    fprintf(stderr,"\nE_detachment\n");
      edetachment(cx);
      break;
    case E_DISS_RECOMBINATION:
    //       fprintf(stderr,"\nEDissRecomb\n");
      edissrecombination(cx);
      break;
    case E_DISS_ATTACHMENT:
//			Ndissattach ++;
//			Nsteps=(int)(thefield->get_nsteps());
//			if(Nsteps>500000) printf("Nsteps=%i, Ndissattach=%i\n",Nsteps,Ndissattach);
       //    fprintf(stderr,"Diss_attach\n");
      edissattach(cx);
      break;
    case NEG_ELASTIC:
        //   fprintf(stderr,"neg_elastic\n");
      halfmassElastic(cx);      //changed by JTG May 20 2009
      break;
     case POSO_ELASTIC:
//           fprintf(stderr,"poso_elastic\n");
      halfmassElastic(cx);      //changed by JTG May 20 2009
      break;
    case NEUO_ELASTIC:
//           fprintf(stderr,"neuo_elastic\n");
      halfmassElastic(cx);      //changed by JTG May 26 2009
      break;
    case NEG_DETCH:
//         fprintf(stderr,"neg_detch\n");
      negdetachment(cx);
      break;
    case NEGO_DETACHMENT:   // Added by JTG July 17 2009
//           fprintf(stderr,"nego_detch\n");
      negodetachment(cx);
      break;
    case O2FRAG:   // Added by JTG August 17 2009
//           fprintf(stderr,"\nfragment\n");
      fragmentation(cx);
      break;
    case MUTUAL_NEUTRALIZATION:
//			Nmutneut ++;
//			Nsteps=(int)(thefield->get_nsteps());
//			if (Nsteps>500000) printf("Nsteps=%i, Nmutneut=%i\n",Nsteps,Nmutneut);
//           fprintf(stderr,"\nO2+/O- Mutual_neut\n");
      mutualneu(cx);
      break;
    case MUTUAL_NEUTRALIZATIONO:
//           fprintf(stderr,"Mutual_neut\n");
      mutualneuo(cx);
      break;
    case O1DTOO2b:
      o1dtoo2b(cx);
      break;
     case O2bO2quench:
      o2bo2quench(cx);
      break;
    case POLARDISS:
//           fprintf(stderr,"Polar_diss\n");
      polardisso(cx);
      break;
    case Ch_EXCHANGE:
//			Nchargetransfer ++;
//			Nsteps=(int)(thefield->get_nsteps());
//			printf("Nsteps=%i, Nchargetransfer=%i\n",Nsteps,Nchargetransfer);
      //  fprintf(stderr,"exchange\n");
      ElectronExchange(cx);
      break;
      case Ch_EXCHANGEO:  // Added by JTG July 31 2009
//	     fprintf(stderr,"Oexchange\n");
      ElectronExchange(cx);
      break;
     case POS_ELASTIC:    // Added by JTG July 31 2009
//          fprintf(stderr,"pos_elastic\n");
      samemassElastic(cx);
      break;
     case P_eEXCHANGE:
//          fprintf(stderr,"P_eexchange\n");
		{
      sec_O2_ion=reaction->get_products(0);
      sec_O=reaction->get_products(1);
			Scalar ReleasedEnergy = 0;
			if (!(reaction -> is_characteristic_value_empty())) ReleasedEnergy = reaction -> get_characteristic_values(0);
      nonresElectronExchange(sec_O2_ion, sec_O, ReleasedEnergy);
			}
//      pElectronExchange(cx); // kill this and use nonresElectronExchange, MAL 12/10/12
      break; 
      case P_e2EXCHANGE:
//          fprintf(stderr,"P_eexchange\n");
		{
      sec_O2_ion=reaction->get_products(0);
      sec_O2=reaction->get_products(1);
			Scalar ReleasedEnergy = 0;
			if (!(reaction -> is_characteristic_value_empty())) ReleasedEnergy = reaction -> get_characteristic_values(0);
      nonresElectronExchange(sec_O2_ion, sec_O2, ReleasedEnergy);
			}
//      pElectronExchange(cx); // kill this and use nonresElectronExchange, MAL 12/10/12
      break; 
		 case P2_eEXCHANGE:
//          fprintf(stderr,"P2eexchange\n");
		{
      sec_O2=reaction->get_products(0);
      sec_O_ion=reaction->get_products(1);
			Scalar ReleasedEnergy = 0;
			if (!(reaction -> is_characteristic_value_empty())) ReleasedEnergy = reaction -> get_characteristic_values(0);
      nonresElectronExchange(sec_O_ion, sec_O2, ReleasedEnergy);
			}
//      p2ElectronExchange(cx); // replace with call to nonresElectronExchange, MAL 10/12/12
      break;
		 case O2O2_ELASTIC: // Added MAL 1/5/10
//          fprintf(stderr,"O2O2_elastic\n");
		  samemassElastic(cx);
			break;
		 case OO_ELASTIC: // Added MAL 1/5/10
//          fprintf(stderr,"OO_elastic\n");
			samemassElastic(cx);
			break;
    default: 
      fprintf(stderr,"Unrecognized collision type!\n"); //Except keep this one MAL 4/1/09
    }
	} //end if (energy > cx.get_threshold())
}

/////////////////////////////
// Reaction dynamics       // 
/////////////////////////////
// Simple models are used: 
// non-relativistic processes, isotropic behaviors, ion/neutral velocity not modified by a single electron.

void OXYGEN_MCC::negdetachment (const Xsection &cx) 
{
  Any_DissociationInThree(ui, un, iSpecies->get_m(), nSpecies->get_m(), sec_e, sec_O2, sec_O, -1.43); //cx.get_threshold()->-1.43, MAL 10/4/12
  ui = DELETE_PARTICLE;
}

void OXYGEN_MCC::fragmentation (const Xsection &cx)  // Added by JTG August 17 2009
{
//  Any_DissociationInThree(ui, un, iSpecies->get_m(), nSpecies->get_m(), sec_O2, sec_O, sec_O_ion, cx.get_threshold());
// Must subtract 6.9 V (dissociation energy of O2+ into O+ and O) from the CM energy, MAL 10/2/12
  Any_DissociationInThree(ui, un, iSpecies->get_m(), nSpecies->get_m(), sec_O2, sec_O, sec_O_ion, -6.9);
  ui = DELETE_PARTICLE;
}

void OXYGEN_MCC::negodetachment (const Xsection &cx)
{
//  Any_DissociationInTwo(ui, un, iSpecies->get_m(), nSpecies->get_m(), sec_e, sec_O2, cx.get_threshold());
// NOTE: The  unstable (repulsive) O2- complex dissociates into O2 and an electron; in the CM system,
// the electron gets virtually all of the 4.2 eV energy difference between O2 and O2-, so pass a "released energy" of 4.2 V, MAL 10/2/12
  Any_DissociationInTwo(ui, un, iSpecies->get_m(), nSpecies->get_m(), sec_e, sec_O2, 3.63); // 4.2->3.63, MAL 10/4/12
  ui = DELETE_PARTICLE;
}

void OXYGEN_MCC::polardisso (const Xsection &cx) 
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_O_neg, sec_O_ion, 0.00);  //Lost energy is 13.62 - 1.46 = 11.16 eV and add 5.09 eV, the threshold is 15 V
}

void OXYGEN_MCC::dissionization (const Xsection &cx) 
{
	// The target is nSpecies = O2, MAL 9/25/09
//	printf("dissionization: eSpecies=%s, nSpecies=%s, e_en=%g, neut_en=%g\n", eSpecies->get_name().c_str(), nSpecies->get_name().c_str(),
//		 eSpecies->get_energy_eV(eSpecies->get_dxdt()*ue), nSpecies->get_energy_eV(nSpecies->get_dxdt()*un)); // FOR DEBUG, MAL 11/23/09
  Any_eIonization2(cx, sec_O2_ion); // IT DOES NOT MATTER what particle species address (here it is sec_O2_ion) is called, as it is never used, MAL 11/25/09
// I suspect the threshold is somewhat higher and/or the lost energy is somewhat lower, so you get hot O's and O+'s from this reaction, MAL 11/25/09
  Any_DissociationInTwo(un, sec_O, sec_O_ion, 0.0); // Threshold is 18.73 eV.  Lost energy is 18.9 eV  JTG August 27 2009
    un = DELETE_PARTICLE; // DELETE THE TARGET NEUTRAL O2, MAL 11/25/09
}

void OXYGEN_MCC::eloss2 (const Xsection &cx)  //Changed to include dissociation JTG August 2009
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_O, sec_O, 1.03);
 }

void OXYGEN_MCC::eloss3 (const Xsection &cx) //Changed to include dissociation JTG August 2009
{
	Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_O, sec_O_1D, 1.27);
}

void OXYGEN_MCC::eloss4 (const Xsection &cx) //Changed to include dissociation JTG August 2009
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un,sec_O_1D, sec_O_1D, 0.88);
}

void OXYGEN_MCC::elExcitation (const Xsection &cx) //Changed to include dissociation JTG August 2009
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_O_1D, sec_O_1D, 3.25);
}

void OXYGEN_MCC::eExcitation1 (const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
//  if(sec_O2_vib1 && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O2_vib1->get_energyThrToAddAsPIC()) add_particles(sec_O2_vib1,un); // MAL 11/25/09
  if(sec_O2_vib1) add_product(sec_O2_vib1, un);
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::eExcitation2 (const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
//  if(sec_O2_vib2 && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O2_vib2->get_energyThrToAddAsPIC()) add_particles(sec_O2_vib2,un); // MAL 11/25/09
  if(sec_O2_vib2) add_product(sec_O2_vib2, un);
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::eExcitation3 (const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
//  if(sec_O2_vib3 && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O2_vib3->get_energyThrToAddAsPIC()) add_particles(sec_O2_vib3,un); // MAL 11/25/09
  if(sec_O2_vib3) add_product(sec_O2_vib3, un);
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::eExcitation4 (const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
//  if(sec_O2_vib4 && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O2_vib4->get_energyThrToAddAsPIC()) add_particles(sec_O2_vib4,un); // MAL 11/25/09
  if(sec_O2_vib4) add_product(sec_O2_vib4, un);
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::esingdelta (const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
//  if(sec_O2_sing_delta && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O2_sing_delta->get_energyThrToAddAsPIC()) add_particles(sec_O2_sing_delta,un); // MAL 11/25/09
  if(sec_O2_sing_delta) add_product(sec_O2_sing_delta, un);
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::esingdeltadex (const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
//  if(sec_O2 && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O2->get_energyThrToAddAsPIC()) add_particles(sec_O2,un); // JTG January 9 2013
  if(sec_O2) add_product(sec_O2, un);
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::esingsigmadex (const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
//  if(sec_O2 && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O2->get_energyThrToAddAsPIC()) add_particles(sec_O2,un); // HH February 9 2016
  if(sec_O2) add_product(sec_O2, un);
  un = DELETE_PARTICLE;
}


void OXYGEN_MCC::esingsigma (const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
//  if(sec_O2_sing_sigma && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O2_sing_sigma->get_energyThrToAddAsPIC()) add_particles(sec_O2_sing_sigma,un); // MAL 11/25/09
  if(sec_O2_sing_sigma) add_product(sec_O2_sing_sigma, un);
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::erotational (const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
//  if(sec_O2_rot && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O2_rot->get_energyThrToAddAsPIC()) add_particles(sec_O2_rot,un); // MAL 11/25/09
  if(sec_O2_rot) add_product(sec_O2_rot, un);
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::eloss(const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
}


void OXYGEN_MCC::eexc1d (const Xsection& cx)   // Added by JTG July 20 2009
{
  Any_eExcitation(cx, nSpecies->get_m());
//  if(sec_O_1D && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O_1D->get_energyThrToAddAsPIC()) add_particles(sec_O_1D,un); // MAL 11/25/09
  if(sec_O_1D) add_product(sec_O_1D, un);
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::eexc1ddex (const Xsection& cx)   // JTG January 10 2013
{
  Any_eExcitation(cx, nSpecies->get_m());
//  if(sec_O && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O->get_energyThrToAddAsPIC()) add_particles(sec_O,un);
  if(sec_O) add_product(sec_O, un);
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::eexc1s (const Xsection& cx)   // Added by JTG July 20 2009
{
  Any_eExcitation(cx, nSpecies->get_m());
//  if(sec_O_1S && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O_1S->get_energyThrToAddAsPIC()) add_particles(sec_O_1S,un); // MAL 11/25/09
  if(sec_O_1S) add_product(sec_O_1S, un);
  un = DELETE_PARTICLE;
}


void OXYGEN_MCC::edissattach(const Xsection& cx)
{
  Any_DissociationInTwo(un, sec_O, sec_O_neg, energy-3.63); // change ui to un; MAL 4/12/09
//  Any_DissociationInTwoNoXform(un, sec_O, sec_O_neg, energy-4.2); // change ui to un; MAL 4/12/09; Change 3.63 -> 4.2 to agree with xpdp1, MAL 8/16/12
  ue = DELETE_PARTICLE; // change un to ue; MAL 4/12/09
}

void OXYGEN_MCC::edetachment(const Xsection& cx)
{
	// The target is iSpecies = O-, MAL 9/25/09
	Any_eDetachment(cx, sec_O);
	ui = DELETE_PARTICLE; // MUST DELETE THE ION, see Any_eDetachment below, MAL 12/28/09
}

void OXYGEN_MCC::eIonizationo(const Xsection& cx)
{
	// The target is nSpecies = O, MAL 9/25/09
  Any_eIonization(cx, sec_O_ion);
	un = DELETE_PARTICLE; // CORRECTION, see Any_eIonization below, MAL 9/25/09
}

void OXYGEN_MCC::eIonizationo2(const Xsection& cx)
{
	// The target is nSpecies = O2, MAL 9/25/09
//	 printf("\noxygen_mcc::eIonizationo2: entered\n");
  Any_eIonization(cx, sec_O2_ion);
	un = DELETE_PARTICLE;  // CORRECTION, see Any_eIonization below, MAL 9/25/09
}

void OXYGEN_MCC::edissrecombination(const Xsection& cx) 
{
//  Any_DissociationInTwo(ui, sec_O, sec_O_1D, cx.get_threshold());
  Any_DissociationInTwo(ui, sec_O, sec_O_1D, 4.87); // Must pass the released energy, not the threshold energy, which is zero!  MAL 10/2/12
  ue = DELETE_PARTICLE;
}

void OXYGEN_MCC::negelastic (const Xsection &) 
{
}


void OXYGEN_MCC::pElectronExchange(const Xsection & cx)
{ // Pb: exothermicities not taken into account...error?
//  if (sec_O && iSpecies->get_energy_eV(iSpecies->get_dxdt()*ui) > sec_O->get_energyThrToAddAsPIC()) // MAL 11/25/09
  if(sec_O)
    { 
//      add_particles(sec_O, ui);
      add_product(sec_O, ui);
    }
  if (sec_O2_ion)
    { 
//      add_particles(sec_O2_ion, un);
      add_product(sec_O2_ion, un);
    }
  ui = DELETE_PARTICLE;
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::p2ElectronExchange(const Xsection & cx)
{ // Pb: exothermicities not taken into account...error?
//  if (sec_O2 && iSpecies->get_energy_eV(iSpecies->get_dxdt()*ui) > sec_O2->get_energyThrToAddAsPIC()) // MAL 11/25/09
  if(sec_O2)
    { 
//      add_particles(sec_O2, ui);
      add_product(sec_O2, ui);
    }
  if (sec_O_ion)
    { 
//      add_particles(sec_O_ion, un);
      add_product(sec_O_ion, un);
    } 
  ui = DELETE_PARTICLE;
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::mutualneu (const Xsection &)
{
	// The iSpecies is O-, the nSpecies is O2+, MAL 11/25/09
//  if (sec_O && iSpecies->get_energy_eV(iSpecies->get_dxdt()*ui) > sec_O->get_energyThrToAddAsPIC()) add_particles(sec_O, ui); // test add_product, MAL200208
  if(sec_O) add_product(sec_O, ui); // test add_product, MAL200208
//  if (sec_O2 && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O2->get_energyThrToAddAsPIC()) add_particles(sec_O2, un);
  if(sec_O2) add_product(sec_O2, un); // test add_product, MAL200208
  ui = DELETE_PARTICLE;
  un = DELETE_PARTICLE;
}


void OXYGEN_MCC::mutualneuo (const Xsection &) 
{
	// The iSpecies is O-, the nSpecies is O+, MAL 11/25/09
//  if (sec_O && iSpecies->get_energy_eV(iSpecies->get_dxdt()*ui) > sec_O->get_energyThrToAddAsPIC()) add_particles(sec_O, ui);
  if(sec_O) add_product(sec_O, ui);
//  if (sec_O && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O->get_energyThrToAddAsPIC()) add_particles(sec_O, un);
  if(sec_O) add_product(sec_O, un);
  ui = DELETE_PARTICLE;
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::o1dtoo2b (const Xsection &) 
{
	// The iSpecies is O(1D), the nSpecies is O2, JTG February 10 2015
//  if (sec_O && iSpecies->get_energy_eV(iSpecies->get_dxdt()*ui) > sec_O->get_energyThrToAddAsPIC()) add_particles(sec_O, ui);
  if(sec_O) add_product(sec_O, ui);
//  if (sec_O2_sing_sigma && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O2_sing_sigma->get_energyThrToAddAsPIC()) add_particles(sec_O2_sing_sigma, un);
  if(sec_O2_sing_sigma) add_product(sec_O2_sing_sigma, un);
  ui = DELETE_PARTICLE;
  un = DELETE_PARTICLE;
}

void OXYGEN_MCC::o2bo2quench (const Xsection &) 
{
	// The iSpecies is O2(b), the nSpecies is O2, JTG February 13 2015
//  if (sec_O2 && iSpecies->get_energy_eV(iSpecies->get_dxdt()*ui) > sec_O2->get_energyThrToAddAsPIC()) add_particles(sec_O2, ui);
  if(sec_O2) add_product(sec_O2, ui);
//  if (sec_O2 && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_O2->get_energyThrToAddAsPIC()) add_particles(sec_O2, un);
  if(sec_O2) add_product(sec_O2, un);
  ui = DELETE_PARTICLE;
  un = DELETE_PARTICLE;
}


void OXYGEN_MCC::Any_eExcitation (const Xsection& cx, Scalar targetmass) 
{
  energy -= cx.get_threshold();
//	if (energy < 0.) printf("\nOXYGEN_MCC::Any_eExcitation, energy=%g\n",energy); //FOR DEBUG, MAL 8/25/12
  v = eSpecies->get_speed2(energy); 
  v /= eSpecies->get_dxdt();
	
  // Isotropic scattering in target rest frame (also CM system to a very good approximation) with the reduced energy.
  Scalar coschi = 1 - 2*frand();
  Scalar chi = acos(coschi);
  Scalar phi = TWOPI*frand();
  Scalar me = eSpecies -> get_m();
  v *= sqrt(1. - 2. * me * targetmass / (me + targetmass) / (me + targetmass) * (1. - coschi)); 
  ue = v*  newcoordiante(ue-un , chi, phi) + un;

/*
	// RESTORE SURENDRA'S FORMULA FOR COMPARISON TO XPDP1, MAL 8/25/12
  // Surendra's formula: Surendra, Graves, Jellum. Phys.Rev. A41. 1112 (1990).
	Scalar coschi;
  if (energy < 1e-30) coschi = 1.0;
  else coschi = 1 + 2.0 * (1.0 - pow(energy + 1.0, frand())) /  energy;  
  Scalar chi = acos(coschi);
  Scalar phi = TWOPI*frand();
  ue = v*  newcoordiante(ue , chi, phi);  // note me << mneutral is assumed for excitations in xpdp1, MAL 8/25/12
 */
}


void OXYGEN_MCC::Any_DissociationInTwo(Vector3 & target_velocity,
					     Species* s1, Species* s2,
					     Scalar ReleasedEnergy) 
{
  if (s1||s2)
    { 
      Scalar mu;
      Scalar m1, m2;
      if (s1) m1 = s1->get_m();
      if (s2) m2 = s2->get_m();
      if (s1 && s2) 
        mu = 1./(1/m1+1/m2);
      else
      {
	// mindgame: in case that only one of two is known, m1 << m2 is assumed.
	if (s1) 
          mu = m1;
        else
	  mu = 0;
      }
      Scalar temp = 0;
      if (ReleasedEnergy >= 0)
      temp  = sqrt(2*ReleasedEnergy*mu*PROTON_CHARGE);
      Scalar phi = frand()*TWOPI; // isotropic in the frame of the excited species
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi); 

      if (s1) 
	{ 
	  Vector3 newvel = target_velocity + ((temp/m1)*uProducts)/s1->get_dxdt(); 
//	  Vector3 newvel = ((temp/m1)*uProducts)/s1->get_dxdt(); // as done in xpdp1, MAL 9/17/12
		// add particles if s1 is an electron or ion, or if s1 is a neutral and meets the energy threshold condition, MAL 11/25/09
//		if (s1->get_q() !=0 || (s1->get_q() == 0 && s1->get_energy_eV(s1->get_dxdt()*newvel) > s1->get_energyThrToAddAsPIC())) add_particles(s1, newvel); // lab frame
    add_product(s1, newvel);
//		printf("AnyDissociationInTwo: s1=%s, en1=%g eV\n", s1->get_name().c_str(), s1->get_energy_eV(s1->get_dxdt()*newvel)); // FOR DEBUG, MAL 11/23/09

	}
      if (s2)
	{ 
	  Vector3 newvel = target_velocity - ((temp/m2)*uProducts)/s2->get_dxdt();
//	  Vector3 newvel = ((-temp/m2)*uProducts)/s2->get_dxdt(); // as done in xpdp1, MAL 9/17/12
		// add particles if s2 is an electron or ion, or if s2 is a neutral and meets the energy threshold condition, MAL 11/25/09
//	  if (s2->get_q() !=0 || (s2->get_q() == 0 && s2->get_energy_eV(s2->get_dxdt()*newvel) > s2->get_energyThrToAddAsPIC())) add_particles(s2, newvel);
    add_product(s2, newvel);
//		printf("AnyDissociationInTwo: s2=%s, en2=%g eV\n", s2->get_name().c_str(), s2->get_energy_eV(s2->get_dxdt()*newvel)); // FOR DEBUG, MAL 11/23/09
	}
    }
  target_velocity = DELETE_PARTICLE;
}

void OXYGEN_MCC::Any_DissociationInThree(Vector3 & target_velocity,
				 	       Species* s1, Species* s2, 
					       Species* s3,
					       Scalar ReleasedEnergy)
{
  if(s1||s2||s3)
    {
      Scalar mu;
      Scalar m1, m2, m3;
      if (s1) m1 = s1->get_m();
      if (s2) m2 = s2->get_m();
      if (s3) m3 = s3->get_m();
      if (s1 && s2 && s3) 
      {
        mu = 1./(1/m1+ 1/m2 + 1/m3); // mu is the reduced mass.
      }
      else
      {
	if (s1 && s2)
	{
	   mu = 1./(1/m1+1/m2); 
	}
	// mindgame: m1 , m2 << m3 is assumed.
	// mindgame: m1 = m2 is additionally assumed.
	else if (s3 && s1) mu = 1./(1/m3+2/m1);
        else if (s3 && s2) mu = 1./(1/m3+2/m2);
	else if (s3) mu = 0;
	else if (s2) mu = 0.5*m2;
	else mu = 0.5*m1;
      }
      Scalar temp = 0;
      if (ReleasedEnergy >= 0)
      temp  = sqrt(2*ReleasedEnergy*mu*PROTON_CHARGE);
      Scalar phi = frand()*TWOPI; // isotropic
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi);   
      phi = frand()*TWOPI; 
   // The algorithm below is not general; in the CM system, the sum (head to tail) of the three product momentum vectors forms an equilateral triangle, MAL 12/2/09
	 // the products come off with 120 degree angles from each other. In general, any three product momentum vectors that form a triangle are possible; fix later?  
      if (s1)
	{ 
	  Vector3 newvel = target_velocity + ((temp/m1)*uProducts)/s1->get_dxdt();
		// add particles if s1 is an electron or ion, or if s1 is a neutral and meets the energy threshold condition, MAL 11/25/09
//		if (s1->get_q() !=0 || (s1->get_q() == 0 && s1->get_energy_eV(s1->get_dxdt()*newvel) > s1->get_energyThrToAddAsPIC())) add_particles(s1, newvel); // lab frame
    add_product(s1, newvel);
//		printf("AnyDissociationInThree: s1=%s, en1=%g eV\n", s1->get_name().c_str(), s1->get_energy_eV(s1->get_dxdt()*newvel)); // FOR DEBUG, MAL 11/23/09
#ifdef DEBUGmcc  
	  fprintf(stderr, "\n New particle velocity: %g, uimagnitude: %g", newvel.magnitude(), ui.magnitude());
#endif
	}
      if (s2)
	{ 
	  Vector3 newvel = target_velocity + ((temp/m2)*newcoordiante(uProducts, TWOTHIRDS*PI, phi))/s2->get_dxdt(); // NOTE (2/3) (int) -> TWOTHIRDS (floating), MAL 12/2/09
		// add particles if s2 is an electron or ion, or if s2 is a neutral and meets the energy threshold condition, MAL 11/25/09
//	  if (s2->get_q() !=0 || (s2->get_q() == 0 && s2->get_energy_eV(s2->get_dxdt()*newvel) > s2->get_energyThrToAddAsPIC())) add_particles(s2, newvel);
    add_product(s2, newvel);
//		printf("AnyDissociationInThree: s2=%s, en2=%g eV\n", s2->get_name().c_str(), s2->get_energy_eV(s2->get_dxdt()*newvel)); // FOR DEBUG, MAL 11/23/09
	  
	}  
      if (s3)
	{ 
	  Vector3 newvel = target_velocity + (temp/m3)*newcoordiante(uProducts, TWOTHIRDS*PI, phi + PI)/s3->get_dxdt(); // See above, MAL 12/2/09
		// add particles if s3 is an electron or ion, or if s3 is a neutral and meets the energy threshold condition, MAL 11/25/09
//	  if (s3->get_q() !=0 || (s3->get_q() == 0 && s3->get_energy_eV(s3->get_dxdt()*newvel) > s3->get_energyThrToAddAsPIC())) add_particles(s3, newvel);
    add_product(s3, newvel);
//		printf("AnyDissociationInThree: s3=%s, en3=%g eV\n", s3->get_name().c_str(), s3->get_energy_eV(s3->get_dxdt()*newvel)); // FOR DEBUG, MAL 11/23/09
	  
	}
    }
  target_velocity = DELETE_PARTICLE;
}

void OXYGEN_MCC::Any_DissociationInTwo(Vector3 & Vr1, Vector3 & Vr2,  
Scalar Mr1, Scalar Mr2, Species* s1, Species* s2, Scalar Ereleased)
{

// Revised by MAL and JTG, 12/2/09
// Vr1,2 = projectile,target velocity, Mr1,2 = projectile,target mass,  
// Ereleased = additional potential energy released during the dissociation
	Vector3 Vcm = (Mr1*Vr1*iSpecies->get_dxdt()+Mr2*Vr2*nSpecies->get_dxdt())/(Mr1+Mr2);
	Scalar energy1 = (0.5*Mr1* (Vr1*iSpecies->get_dxdt()-Vcm).magnitude_squared() +  0.5*Mr2* (Vr2*nSpecies->get_dxdt()-Vcm).magnitude_squared())/PROTON_CHARGE; 
	Vector3 newvel1;
	Vector3 newvel2;
  if (s1||s2)
    { 
      Scalar mu;
      Scalar m1, m2;
      Scalar CMEnergy = energy1 + Ereleased; 
      if (s1) m1 = s1->get_m();
      if (s2) m2 = s2->get_m();
      if (s1 && s2) 
        mu = 1./(1/m1+1/m2);
      else
      {
	// mindgame: in case that only one of two is known, m1 << m2 is assumed.
	if (s1) 
          mu = m1;
        else
	  mu = 0;
      }
      Scalar temp = 0;
      if (CMEnergy >= 0)
      temp  = sqrt(2*CMEnergy*mu*PROTON_CHARGE);
      Scalar phi = frand()*TWOPI; // isotropic in the frame of the excited species
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi) ; 
//		printf("AnyDissociationInTwo: projectile=%s, en_p=%g eV\n", iSpecies->get_name().c_str(), iSpecies->get_energy_eV(iSpecies->get_dxdt()*Vr1)); // FOR DEBUG, MAL 11/23/09
//		printf("AnyDissociationInTwo: target=%s, en_t=%g eV\n", nSpecies->get_name().c_str(), nSpecies->get_energy_eV(nSpecies->get_dxdt()*Vr2)); // FOR DEBUG, MAL 11/23/09
      if (s1) 
	{ 
	  newvel1 = (Vcm + (temp/m1)*uProducts)/s1->get_dxdt(); 
		// add particles if s1 is an electron or ion, or if s1 is a neutral and meets the energy threshold condition, MAL 11/25/09
//		if (s1->get_q() !=0 || (s1->get_q() == 0 && s1->get_energy_eV(s1->get_dxdt()*newvel1) > s1->get_energyThrToAddAsPIC())) add_particles(s1, newvel1); // lab frame
    add_product(s1, newvel1);
//  	printf("AnyDissociationInTwo: s1=%s, en1=%g eV\n", s1->get_name().c_str(), s1->get_energy_eV(s1->get_dxdt()*newvel1)); // FOR DEBUG, MAL 11/23/09
	}
      if (s2)
	{ 
	  newvel2 = (Vcm - (temp/m2)*uProducts)/s2->get_dxdt();
		// add particles if s2 is an electron or ion, or if s2 is a neutral and meets the energy threshold condition, MAL 11/25/09
//	  if (s2->get_q() !=0 || (s2->get_q() == 0 && s2->get_energy_eV(s2->get_dxdt()*newvel2) > s2->get_energyThrToAddAsPIC())) add_particles(s2, newvel2);
    add_product(s2, newvel2);
//    printf("AnyDissociationInTwo: s2=%s, en2=%g eV\n", s2->get_name().c_str(), s2->get_energy_eV(s2->get_dxdt()*newvel2)); // FOR DEBUG, MAL 11/23/09
//		printf("AnyDissociationInTwo: KEproducts-Kereactants=%g eV\n", s1->get_energy_eV(s1->get_dxdt()*newvel1) +
//			s2->get_energy_eV(s2->get_dxdt()*newvel2) - iSpecies->get_energy_eV(iSpecies->get_dxdt()*Vr1) - nSpecies->get_energy_eV(nSpecies->get_dxdt()*Vr2)); // FOR DEBUG, MAL 11/23/09
	}
    }
  Vr2 = DELETE_PARTICLE; // delete target particle, MAL 11/26/09
}



void OXYGEN_MCC::Any_DissociationInThree(Vector3 & Vr1, Vector3 & Vr2, 
                                               Scalar Mr1, Scalar Mr2,
				 	       Species* s1, Species* s2, 
					       Species* s3,
					       Scalar Ereleased)
{
// for dissociations involving the collision of two heavy particle reactants, MAL 10/2/12
// Ereleased is the additional potential energy released during the dissociation
// Revised by MAL, 11/26/09
// Vr1,2 = projectile,target velocity, Mr1,2 = projectile,target mass
	Vector3 Vcm = (Mr1*Vr1*iSpecies->get_dxdt()+Mr2*Vr2*nSpecies->get_dxdt())/(Mr1+Mr2);
	Scalar energy1 = (0.5*Mr1* (Vr1*iSpecies->get_dxdt()-Vcm).magnitude_squared() +  0.5*Mr2* (Vr2*nSpecies->get_dxdt()-Vcm).magnitude_squared())/PROTON_CHARGE; 
  if(s1||s2||s3)
    {
      Scalar mu;
      Scalar m1, m2, m3;
      Scalar CMEnergy = energy1 + Ereleased; //energy in CM system plus released energy, MAL 10/2/12
			Vector3 newvel1;
			Vector3 newvel2;
			Vector3 newvel3;
      if (s1) m1 = s1->get_m();
      if (s2) m2 = s2->get_m();
      if (s3) m3 = s3->get_m();
      if (s1 && s2 && s3) 
      {
        mu = 1./(1/m1+ 1/m2 + 1/m3); // mu is the reduced mass.
      }
      else
      {
	if (s1 && s2)
	{
	   mu = 1./(1/m1+1/m2); 
	}
	// mindgame: m1 , m2 << m3 is assumed.
	// mindgame: m1 = m2 is additionally assumed.
	else if (s3 && s1) mu = 1./(1/m3+2/m1);
        else if (s3 && s2) mu = 1./(1/m3+2/m2);
	else if (s3) mu = 0;
	else if (s2) mu = 0.5*m2;
	else mu = 0.5*m1;
      }
      Scalar temp = 0;
      if (CMEnergy >= 0)
      temp  = sqrt(2*CMEnergy*mu*PROTON_CHARGE);
     
      Scalar phi = frand()*TWOPI; // isotropic
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi);   
      phi = frand()*TWOPI; 
//		printf("AnyDissociationInThree: projectile=%s, en_p=%g eV\n", iSpecies->get_name().c_str(), iSpecies->get_energy_eV(iSpecies->get_dxdt()*Vr1)); // FOR DEBUG, MAL 11/23/09
//		printf("AnyDissociationInThree: target=%s, en_t=%g eV\n", nSpecies->get_name().c_str(), nSpecies->get_energy_eV(nSpecies->get_dxdt()*Vr2)); // FOR DEBUG, MAL 11/23/09
   // The algorithm below is not general; in the CM system, the sum (head to tail) of the three product momentum vectors forms an equilateral triangle, MAL 12/2/09
	 // the products come off with 120 degree angles from each other. In general, any three product momentum vectors that form a triangle are possible; fix later?  
      if (s1)
	{ 
	  newvel1 = (Vcm + (temp/m1)*uProducts)/s1->get_dxdt();
//	  printf("AnyDissociationInThree: temp = %g, Vcmag = %g, newvel1 = %g, ReleasedEnergy = %g \n",temp,Vcm.magnitude(),newvel1.magnitude(),ReleasedEnergy); 
		// add particles if s1 is an electron or ion, or if s1 is a neutral and meets the energy threshold condition, MAL 11/25/09
//		if (s1->get_q() !=0 || (s1->get_q() == 0 && s1->get_energy_eV(s1->get_dxdt()*newvel1) > s1->get_energyThrToAddAsPIC())) add_particles(s1, newvel1); // lab frame
    add_product(s1, newvel1);
//		printf("AnyDissociationInThree: s1=%s, en1=%g eV\n", s1->get_name().c_str(), s1->get_energy_eV(s1->get_dxdt()*newvel1)); // FOR DEBUG, MAL 11/23/09

#ifdef DEBUGmcc  
	  fprintf(stderr, "\n New particle velocity: %g, uimagnitude: %g", newvel1.magnitude(), ui.magnitude());
#endif
	}
      if (s2)
	{ 
	  newvel2 = (Vcm + (temp/m2)*newcoordiante(uProducts, TWOTHIRDS*PI, phi))/s2->get_dxdt();  
		// add particles if s2 is an electron or ion, or if s2 is a neutral and meets the energy threshold condition, MAL 11/25/09
//	  if (s2->get_q() !=0 || (s2->get_q() == 0 && s2->get_energy_eV(s2->get_dxdt()*newvel2) > s2->get_energyThrToAddAsPIC())) add_particles(s2, newvel2);
    add_product(s2, newvel2);
//		printf("AnyDissociationInThree: s2=%s, en2=%g eV\n", s2->get_name().c_str(), s2->get_energy_eV(s2->get_dxdt()*newvel2)); // FOR DEBUG, MAL 11/23/09
	  
	}  
      if (s3)
	{ 
	  newvel3 = (Vcm + (temp/m3)*newcoordiante(uProducts, TWOTHIRDS*PI, phi + PI))/s3->get_dxdt();  
		// add particles if s3 is an electron or ion, or if s3 is a neutral and meets the energy threshold condition, MAL 11/25/09
//	  if (s3->get_q() !=0 || (s3->get_q() == 0 && s3->get_energy_eV(s3->get_dxdt()*newvel3) > s3->get_energyThrToAddAsPIC())) add_particles(s3, newvel3);
    add_product(s3, newvel3);
//		printf("AnyDissociationInThree: s3=%s, en3=%g eV\n", s3->get_name().c_str(), s3->get_energy_eV(s3->get_dxdt()*newvel3)); // FOR DEBUG, MAL 11/23/09
//		printf("AnyDissociationInThree: KEproducts-Kereactants=%g eV\n", s1->get_energy_eV(s1->get_dxdt()*newvel1) + s3->get_energy_eV(s3->get_dxdt()*newvel3) +
//			s2->get_energy_eV(s2->get_dxdt()*newvel2) - iSpecies->get_energy_eV(iSpecies->get_dxdt()*Vr1) - nSpecies->get_energy_eV(nSpecies->get_dxdt()*Vr2)); // FOR DEBUG, MAL 11/23/09
	  
	}
    }
  Vr2 = DELETE_PARTICLE; // delete target particle, MAL 11/26/09
}

// --- Detachment processes:
void OXYGEN_MCC::Any_eDetachment(const Xsection& cx, Species* s1) 
{
// Used for edetachment MAL 12/27/09
// We don't subtract the velocity of the heavy target ion: assuming the electron has a much higher velocity: energy, ue are valid. 
  Scalar thres = cx.get_threshold();
  ionization(thres);
  // Creation of ionized species
  if (s1)
    { 
   // add particles if s1 is an electron or ion, or if s1 is a neutral and meets the energy threshold condition, MAL 11/25/09
 //  if (s1->get_q() !=0 || (s1->get_q() == 0 && s1->get_energy_eV(s1->get_dxdt()*ui) > s1->get_energyThrToAddAsPIC())) add_particles(s1, ui); // lab frame
      add_product(s1, ui);
    }
// Must add proper u_target = DELETE_PARTICLE to individual cross section calls, MAL 11/25/09
}

// --- Ionization processes:
void OXYGEN_MCC::Any_eIonization(const Xsection& cx, Species* s1) 
{
// Revised by MAL, 11/25/09
// Used for edetachment, eionizationo, and eionizationo2, MAL 11/25/09
// We don't subtract the velocity of the heavy target (ion or neutral): assuming the electron has a much higher velocity: energy, ue are valid. 
  Scalar thres = cx.get_threshold();
//	printf("\noxygen_mcc::Any_eIonization: entered, threshold=%g\n",thres);
  ionization(thres);
  // Creation of ionized species
  if (s1)
    { 
//		printf("\noxygen_mcc::Any_eIonization, adding ion s1=%p (%s)\n\n", (void*)s1, s1->get_name().c_str());

   // add particles if s1 is an electron or ion, or if s1 is a neutral and meets the energy threshold condition, MAL 11/25/09
//   if (s1->get_q() !=0 || (s1->get_q() == 0 && s1->get_energy_eV(s1->get_dxdt()*un) > s1->get_energyThrToAddAsPIC())) add_particles(s1, un); // lab frame
      add_product(s1, un);
    }
// Must add proper u_target = DELETE_PARTICLE to individual cross section calls, MAL 11/25/09
}

void OXYGEN_MCC::Any_eIonization2(const Xsection& cx, Species* s1) 
{
	// Used for dissociative ionization of O2, revised by MAL, 11/26/09
  // We don't subtract the velocity of the neutral: assumed the electron has a much higher velocity: energy, ue are valid. 
  Scalar thres = cx.get_threshold();
  ionization(thres);
// Add u_n = DELETE_PARTICLE to dissociative ionization cross section call, MAL 11/25/09
}
