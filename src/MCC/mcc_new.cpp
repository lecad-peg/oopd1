// mindgame: restructuring, cleanup, and bug fixes (Dec. 9, 2006)
//           to replace mcc.cpp eventually
// TODO: flexible for various energy sharing in ionization event
//       flexible for various differential xsection: isotropic and anisotropic
//     (surendra or okhrimovsky)
#include <stdio.h>
#include "main/pd1.hpp"
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
//#include "collisionpkg.hpp"
#include "reactions/reaction.hpp"
#include "MCC/mcc_new.hpp"
//#include "MCC/mcc.hpp"
#include "atomic_properties/fluid.hpp"
#include "xsections/xsection.hpp"
#include "fields/Base_Class_Direct_Field.hpp"
#include "main/ostack.hpp"
#include "distributions/dist_v.hpp"

MCC_new::MCC_new(oopicList<Species>* slist, ReactionList _reactionlist, Scalar _dt, Fields *tf)
{
  specieslist = slist;
  dt = _dt;
  thefield = tf;
  setdefaults();
  reactionlist = _reactionlist;
}

void MCC_new::setdefaults_mcc()
{
  extra = 0;
  maxSigmaV = 0;
  eSpecies = NULL;
  nSpecies = NULL;
  iSpecies = NULL;
  sec_e = NULL;
  sec_i = NULL;
  // mindgame: the better way is to get nc2p from the inputfile
  useSecDefaultWeight = false;
}

void MCC_new::init_mcc(Species *_reactant1, Species *_reactant2)
{

  init_sumSigmaV(_reactant1);

  if (is_electron(_reactant1))
    eSpecies = _reactant1;
  else if (is_electron(_reactant2))
    eSpecies = _reactant2;
  if (is_neutral(_reactant1))
    nSpecies = _reactant1;
  else if (is_neutral(_reactant2))
    nSpecies = _reactant2;
  if (is_ion(_reactant1))
    iSpecies = _reactant1;
  else if (is_ion(_reactant2))
    iSpecies = _reactant2;

  for (int i=reactionlist.num()-1; i>=0; i--)
  {
    if (reactionlist.data(i)->get_reactiontype() == E_IONIZATION)
    {
      sec_e = reactionlist.data(i)->get_products(0);
      sec_i = reactionlist.data(i)->get_products(1);
    }
  }
}

void MCC_new::particle_particle(Species *const _reactant1, Species *const _reactant2)
{
  Scalar Ntot_particles1, Ntot_particles2; 
  if ((Ntot_particles1=_reactant1->get_Ntot_particles())
       > (Ntot_particles2=_reactant2->get_Ntot_particles()))
  {
    particle_particle(_reactant1, _reactant2, Ntot_particles2);
  }
  else
  {
    particle_particle(_reactant2, _reactant1, Ntot_particles1);
  }
}

void MCC_new::particle_particle (Species *const targetspecies, Species *const projspecies, Scalar Ntot_particles)
{ 
  // In this function, target properties are recognized by the word "target", 
  // whereas projectile properties are either recognized by "proj", or no indication (index, u ...)
 
  // Identification of the type of the colliding particles 
  Vector3* u_target;
  Vector3* u;
  
  set_velocity_pointers(&u_target, targetspecies);
  set_velocity_pointers(&u, projspecies);
 
  // Other initializations:
  ParticleGroupList *pgList_target=targetspecies->get_ParticleGroupList();
  oopicListIter<ParticleGroup> target_pgIter(*pgList_target);  
  ParticleGroup * pga_target;
  ParticleGroupList* pgList = projspecies->get_ParticleGroupList();
  oopicListIter<ParticleGroup> pgIter (*pgList);
  int np = projspecies->number_particles(); 

  // Null Collision Probability:
  Scalar* target_density  = targetspecies->get_species_density();
  Scalar maxdensity = 0.0;  
  for (int i = 0; i < thefield->get_ng(); i++)
    {
      maxdensity = MAX(maxdensity, target_density[i]);
    }
  // Scalar collprob = 1.  - exp(-maxdensity * maxSigmaV[id] * dt * projspecies.get_subcycle());
  Scalar collprob = maxdensity * maxSigmaV * dt * projspecies->get_subcycle(); 
  if (collprob < 1e-20) return; 
  extra += collprob*Ntot_particles; 
  Scalar nCollision = extra; 

  // For collisions of particles of different weight, with variable weights:
  Scalar lambda = 1 ; //min(1, wt(min)/wi(max))
  Scalar proj_maxw = 0.0;
  for (pgIter.restart(); !pgIter.Done(); pgIter++) 
    { 
      // Note: the target is not really randomly chosen
      for (int j =0; j < pgIter()->get_n(); j++)
	{
	  Scalar thew = pgIter()->get_w(j); 
	  if  (thew > proj_maxw)  proj_maxw = thew; 
	}
    }
  Scalar target_minw = 1.1e30;
  for (target_pgIter.restart(); !target_pgIter.Done(); target_pgIter++) 
    { 
      // Note: the target is not really randomly chosen
      for (int j =0; j < target_pgIter()->get_n(); j++)
	{
	  Scalar thew = target_pgIter()->get_w(j); 
	  if  (thew < target_minw)  target_minw = thew;
	}
    }
  Scalar minovermax;
  if (!proj_maxw) return;
  minovermax = target_minw/proj_maxw;
  if (minovermax<1.0) lambda = minovermax; 
  
  //  qsort(targetspecies);
  // indexsort(sortKeys);

  // Collision Process 
  while(nCollision > 0 && np > 0) 
    {
      // Choice of particles colliding together
      // The projectile:
      int index = (int)(np * frand());
      for (pgIter.restart(); !pgIter.Done(); pgIter++)
	{
	  if (index < pgIter()->get_n())  break;
	  else index -= (int) pgIter()->get_n();
	}
      ParticleGroup *pga = pgIter(); 
      x = pga -> get_x(index);       
      *u =  pga -> get_v(index)/pga->get_gamma(index);	
      Scalar w_proj = pga -> get_w(index); 
      w_ref = w_proj*lambda;

      //For sorting: 
      int index_of_target;
      if(sortflag)
	{
	  index_of_target = (int)((indexsorts[(int)x+1]-indexsorts[(int)x])*frand());
	  *u_target = sortKeys[index_of_target].vel();
	}  

      // The target:
      pga_target = NULL;	
      int index_target;	
      for (target_pgIter.restart(); !target_pgIter.Done() && !pga_target; target_pgIter++) 
	{ 
	  // Note: the target is not really randomly chosen
	  for (int j =0; j < target_pgIter()->get_n(); j++)
	    {
	      int k = (int)target_pgIter()->get_x(j);
	      if ((int)x == k || (int)x -1 == k)
		{
		  index_target = j;
		  pga_target = target_pgIter();
		  *u_target =  pga_target -> get_v(index_target)/pga_target->get_gamma(index_target); 
		  break;
		}
	    }
	}

      if (pga_target!=NULL)
	{
	  v = (*u-*u_target).magnitude()+1.e-30; 
	  Scalar temp = pga->get_vMKS(v); 
	  energy = reactant_for_sigmaV->get_energy2_eV(temp);
	  // mindgame: now temp is n*v
	  temp *= target_density[(int)x];
	  Scalar random = frand()*maxdensity*maxSigmaV;
	  Scalar sumnSigmaV = 0;
	  
	  for (int i=0; i< reactionlist.num(); i++)
	    {
	      sumnSigmaV += reactionlist.data(i)->get_xsection()->sigma(energy)*temp;
        // mindgame: comparison between sum of n*v*Sigma/(maxdensity*maxSigmaV)
	      if (random <= sumnSigmaV) // A collision is realized
		{
		  dynamic(*(reactionlist.data(i)->get_xsection()), reactionlist.data(i)->get_reactiontype());

		  // The collision effectively occurs for the particle of smaller weight,
		  // but with a probability for the other one.
		  Scalar w_target = pga_target->get_w(index_target);
		  Scalar event = frand();
		  if(event <= float( w_ref/w_proj)) 
		    {
		  // mindgame: awkward way
		      if((*u) == DELETE_PARTICLE)  
			{
			  np --; 
			  pga->delete_particle(index);
			}
		      else
			{
			  *u *= pga->get_gamma2(*u); 
			  pga->set_v(index, *u);
			}
		    }
		  event = frand();
		  if(event <= (float) (w_ref/w_target))
		    {
		  // mindgame: awkward way
		      if((*u_target) == DELETE_PARTICLE)
			{
			  if(sortflag)
			    {			  
			      sortKeys[index_of_target].del();
			      indexsorts[(int)x]-- ;
			    }
			  pga_target->delete_particle(index_target);     
			} 
		      else
			{
			  if(sortflag) sortKeys[index_of_target].set_v(*u_target);
			  *u_target *= pga_target->get_gamma2(*u_target); 
			  pga_target->set_v(index_target, *u_target);
			}
		    }
		  break;
		}
	    }
	}
      nCollision -= w_ref; // the "effective" number of collisions in the event is w_ref 
    }
  if (np ==0) nCollision = 0.0;
  extra = nCollision; 
}

void MCC_new::set_velocity_pointers(Vector3 **u, const Species *const sp)
{
  if (is_electron(sp))
  {
    *u = &ue;
  }
  else if (is_neutral(sp)) 
  {
    *u = &un;	
  }
  else if (is_ion(sp))
  {
    *u =  &ui;
  }
}


// mindgame: interaction between fluid part of targetspecies
//          and particle part of projspecies.
void MCC_new::fluid_particle(Species *const fluid_species, Species *const particle_species)
{
  // Recognition of the species types
  Vector3 *u_fluid;
  Vector3 *u_particle;
  set_velocity_pointers(&u_fluid, fluid_species);
  set_velocity_pointers(&u_particle, particle_species);
 
#ifdef DEBUGmcc
  fprintf(stderr, "\n");
  fprintf(stderr, ": with target->"); fprintf(stderr, targetspecies->get_name());
  fprintf(stderr, ": And projectile->"); fprintf(stderr, projspecies->get_name());
  fprintf(stderr, " \n u = &ue: %d; ", u == &ue);    
  fprintf(stderr, " u = &ui: %d; ", u == &ui);
  fprintf(stderr, " u = &un: %d; ", u == &un);
  fprintf(stderr, " u_target = &ue: %d; ", u_target == &ue);
  fprintf(stderr, " u_target = &ui: %d; ", u_target == &ui);
  fprintf(stderr, " u_target = &un: %d; ", u_target == &un);
#endif

  // Other initializations:
  ParticleGroupList* pgList =  particle_species->get_ParticleGroupList();
  oopicListIter<ParticleGroup> pgIter (*pgList);
  int np = particle_species->number_particles();

  // Null Collision Probability:
  oopicList<Fluid> *target_fList = fluid_species->get_fluidList(); 	  
  oopicListIter<Fluid> fIter(*target_fList);  
  Scalar maxdensity = 0.0;
  for (int i = 0; i < thefield->get_ng(); i++)
    {
      Scalar f_density = 0.0;
      for (fIter.restart(); !fIter.Done(); fIter ++)
	{
	  f_density += fIter()->get_n(i); 
	} 
      maxdensity = MAX(maxdensity, f_density); 
    }
  // Scalar collprob = 1.  - exp(-maxdensity * maxSigmaV * dt * projspecies->get_subcycle());
  Scalar collprob = maxdensity * maxSigmaV * dt * particle_species->get_subcycle();
  if (collprob < 1e-20) return;
  extra += collprob*particle_species->get_Ntot_particles(); 
  Scalar nCollision = extra;

  while (nCollision > 0.0 && np > 0 ) 
    {
      // Choice of projectile
      int index = (int)(np * frand()); 
      for (pgIter.restart(); !pgIter.Done(); pgIter++)
	{
	  if (index < pgIter()->get_n()) break;
	  else index -= (int)pgIter()->get_n();
	}      
      
      ParticleGroup* pga = pgIter();
      x = pga -> get_x(index);  
      *u_particle = pga -> get_v(index)/pga->get_gamma(index);
      w_ref = pga -> get_w(index);

      Scalar random = frand()*maxdensity*maxSigmaV;
      Scalar sumnSigmaV = 0;
      
      //  Contrary to the particle method, we don't randomly chose one fluid as a target, 
      //  but we assess the probability of each fluid for better statistics.
      //  mindgame: why NGP rather than CIC ?
      //  --> C.N.: Yes it would be better, and it shouldn't be hard for fluid-particle collisions. 

      for (fIter.restart(); !fIter.Done(); fIter++)
      {
	// mindgame: no need when density zero.
        if (fIter()->get_n((int)x) > 0)
        {
	  // mindgame: To avoid critical memory leaks,
          //          use get_v_from_distribution
	  *u_fluid = pga->get_vnorm(fIter()->get_v_from_distribution((int)x));	

#ifdef DEBUGmcc
	  fprintf(stderr, "\n u %g", (*u).magnitude());
	  fprintf(stderr, "\n u_target %g", (*u_target).magnitude());
#endif

	   // mindgame: in general, v is relative speed.
	  v = (*u_particle-*u_fluid).magnitude()+1.e-30; 
	  Scalar temp = pga->get_vMKS(v); 
	  energy = reactant_for_sigmaV->get_energy2_eV(temp); 
	  // mindgame: now temp is n*v
	  temp *= fIter()->get_n((int)x);
	  
#ifdef DEBUGmcc	     
	  fprintf (stderr, "\n random: %g ---  FROM energy: %g;   ", random, energy);	      
	  fprintf (stderr, " (*u).e1() %g, (*u_target).e1() %g", (*u).e1(), (*u_target).e1());
	  fprintf (stderr, "\n\t\t target_density: %g, and v_mks: %g, ForXsection: ",  fIter()->get_n((int)x), temp);   
#endif		

	  int i;
	  for (i=0; i< reactionlist.num(); i++)
	    { 
	      sumnSigmaV += reactionlist.data(i)->get_xsection()->sigma(energy)*temp;
#ifdef DEBUGmcc
	      fprintf (stderr, "\n \t sumSigma: %g --- FROM csx: %g; ", sumSigma,  reactionlist.data(i)->get_xsection()->sigma(energy));	      
#endif

     // mindgame: comparison between sum of n*v*Sigma/(maxdensity*maxSigmaV)
	      if (random <= sumnSigmaV)
		{
		  dynamic(*(reactionlist.data(i)->get_xsection()), reactionlist.data(i)->get_reactiontype());
#ifdef DEBUGmcc
       		  fprintf(stderr, "\n Reaction Type: %s", (reactionlist.data(i)->get_reactiontype()).c_str());
#endif

		  // mindgame: awkward way
		    if(*u_particle == DELETE_PARTICLE)  
		    {
#ifdef DEBUGmcc			
		      fprintf(stderr, "  --------> COLLIDE - DELETE");
#endif
		      pga->delete_particle(index);
		      np--;
		    }
		  else
		    { 
#ifdef DEBUGmcc		      
		      fprintf(stderr, "  --------> COLLIDE - MOVE");
		      fprintf(stderr, "\n pre R velocity %g", (pga -> get_v(index)).magnitude());
#endif
		      (*u_particle) *= pga->get_gamma2((*u_particle));
#ifdef DEBUGmcc
		      fprintf(stderr, " --- post R velocity  %g", (*u).magnitude());		      
#endif
		      pga->set_v(index,(*u_particle) );
		    } 
		    break;
		}
	    }
	  if (i < reactionlist.num() ) break;
 	}
      }
      nCollision -= w_ref;
    }
  if (np == 0) nCollision = 0.0;
  extra = nCollision;
}

void MCC_new::init_sumSigmaV(Species *sp)
  // It is "potentially" relativistic both for ions and electrons 
{ 
  reactant_for_sigmaV = sp;
  for (Scalar en = 0.; en<MAX_ENERGY; en+=0.5)
    {
      Scalar temp =0.; 
      for (int i=reactionlist.num()-1; i >= 0; i--)
	{
	  temp += reactionlist.data(i)->get_xsection()->sigma(en);
	}
      maxSigmaV = MAX(temp*reactant_for_sigmaV->get_speed2(en), maxSigmaV);
    }
}

///////////////////////
//sorting            //                                                                                                                         
///////////////////////
int MCC_new::partition(Sort *data, int l, int r)
{
  int i(l-1);
  int j(r);
  float v = data[r].key();
  Sort tmp;
  while(true){
    while (data[++i].key() < v) if (i==r) break;
    while (v < data[--j].key()) if (j==l) break;
    if (i >= j) break;
    tmp = data[i]; // swap the elements i and j                                                                                               
    data[i] = data[j];
    data[j] = tmp;
  }
  tmp = data[i]; // swap the elements i and r                                                                                                 
  data[i] = data[r];
  data[r] = tmp;
  return i;
}

void MCC_new::indexsort(Sort *data)
{
  if(indexsorts) delete [] indexsorts;
  indexsorts = new int[thefield -> get_ng()];
  
  for (int i=0; i < thefield->get_ng();i++)
    {
      indexsorts[i]=0;
    }

  for (int i=0; i < nParticles;i++)
    {
      indexsorts[int(data[i].key())]++;
    }

  for (int i=0; i < thefield->get_ng()-1;i++)
    {
      indexsorts[i+1] += indexsorts[i];
    }

  FILE *fp;
  ostring ffnname ="position.dat";
  fp = fopen(ffnname(), "a");
  for (int i=1;i < nParticles;i++)
    {
      fprintf(fp, "position[%d]=%g\n",i,data[i].key());
    }
  fclose(fp);

  ffnname ="indexsorts.dat";
  fp = fopen(ffnname(), "a");
  for (int i=1;i < thefield->get_ng();i++)
    {
      fprintf(fp, "indexsorts[%d]=%d\n",i,indexsorts[i]);
    }
  fclose(fp);

}

// provides a non-recursive quicksort of keys based on ParticleGroups                                                                         

void MCC_new::qsort(Species & targetspecies)
{
  // calculate number of particles                                                                                            
  sortflag = true;
  nParticles = 0;
  ParticleGroupList *pgList_target = targetspecies.get_ParticleGroupList();
  oopicListIter<ParticleGroup> pgIter(*pgList_target);
  for (pgIter.restart(); !pgIter.Done(); pgIter++){
    nParticles += pgIter() -> get_n();
  }

  // create key index array structure                                                                                                         
  if (sortKeys) delete[] sortKeys; // deallocate old array                                                                                    
  sortKeys = new Sort[nParticles];
  int j=0;
  Scalar xMin, xMax;
  bool init = true;
  // for (spIter.restart(); !spIter.Done(); spIter++) {                                                                                       
  //  oopicListIter<ParticleGroup> pgIter(*spIter()->get_ParticleGroupList());                                                                
  // must initialize xmin and xmax       
  if (init) {
    pgIter.restart();
    xMin = pgIter()->get_x(0);
    xMax = xMin;
    init = false;
  }
  // set up sortKeys and also get xmin and xmax                                                                                             
  for (pgIter.restart(); !pgIter.Done(); pgIter++) {
    ParticleGroup *pg = pgIter();
    for (int i=pg->get_n(); i;) { // loop over particles                                                                                    
      i--; // decrement done here to reduce for loop calcs                                                                                  
      sortKeys[j].set(pg, i); // store index and ParticleGroup*                                                                             
      float x = pg->get_x(i); // compare xmin and xmax to x                                                                                 
      xMin = MIN(xMin,x);
      xMax = MAX(xMax,x);
      j++;
    }
  }
  // }                                                                                                                                      
  // sort the data                                                                                                                            
  int l = 0;
  int r = nParticles-1;
  int i;
  oopicStack<int> s;
  s.push(l);
  s.push(r);
  while (!s.isEmpty()){
    r = s.pop();
    l = s.pop();
    if (r <= l) continue;
    i = partition(sortKeys, l, r);
    if (i-1 > r-i){
      s.push(l); s.push(i-1); s.push(i+1); s.push(r);
    } else {
      s.push(i+1); s.push(r); s.push(l); s.push(i-1);
    }
  }
  FILE *fp;
  ostring ffnname ="position_inside_qsort.dat";
  fp = fopen(ffnname(), "a");
  for (int i=1;i < nParticles;i++)
    {
      fprintf(fp, "position[%d]=%g\n",i,sortKeys[i].key());
    }
  fclose(fp);
}

////////////////////////////////     
//  Kinematics                //
////////////////////////////////

void MCC_new::dynamic(const Xsection& cx, ReactionType reactiontype)
{
#ifdef DEBUGmcc
  totalnumberofcollisions ++;
  printf("\nReactionType: %s\n", ReactionTypeName(reactiontype).c_str());
#endif  

  switch(reactiontype)
    {
      // --- Scattering processes:
    case E_ELASTIC:                
      eElastic(cx);
      break;
    case E_EXCITATION:
      eExcitation(cx);
      break;
    case SAMEMASS_ELASTIC: 
      samemassElastic(cx);
      break;

      // --- Ionizing processes:
    case E_IONIZATION:
      eIonization(cx);
      break;

      // --- Charge exchange processes:    
    case Ch_EXCHANGE:
      ElectronExchange(cx);
      break;   

    default: 
      fprintf(stderr,"Unrecognized collision type!\n");
    }
}

// --- Elastic and non-elastic scattering processes:
void MCC_new::eElastic(const Xsection& cx)  // Only works for electrons (currently)
{
  /*
  //This is the nonrelativistic algorithm
  if(relativisticMCC_new == false)
  {
  new_eVelocity_NR();
  }
  //This is the relativistic algorithm
  else
  {
  Scalar thetaS, phiS;
  elasticScatteringAngles(energy,thetaS, phiS);
  Vector3 uScatter;
  Scalar afterElastic = elasticEnergy(energy, thetaS);
  energy = energy - afterElastic;
  uScatter = newMomentum(u, energy, thetaS, phiS);
  u = uScatter;
  }
  */
  // We move to the frame of the neutral target. 
  //But the electron velocity being much higher than the neutral velocity, energy and v are not modified.
  
  Scalar temp = eSpecies->get_nr_u_limit()*eSpecies->get_dxdt();  
  temp /= eSpecies->gamma(temp);
  if (v < temp)
    {
      eScattering_NR(); 
    }
  else
    {
      eScattering_R(); 
    }
}

void MCC_new::eExcitation(const Xsection& cx)
  // Excitation is treated like elastic collisions with a reduced energy. 
  // This reduced energy is used both for the choice of the scattering angle angle and to determine the post collision velocity. 
  // This might need to be reviewed (C.H. Lim 25th May 2005)
{
  energy -= cx.get_threshold(); 
  Scalar temp = eSpecies->get_nr_u_limit();
  temp /= gamma(temp);
  if (v < temp) 
    {
      v = eSpecies->get_speed2(energy); 
      v /= eSpecies->get_dxdt();  
      eScattering_NR(); 
    }
  else
    {
      eScattering_R(); 
    }
}

// Non-relativistic case:
void MCC_new::eScattering_NR()
{
  Scalar coschi;
  
  /*
  // Surendra's formula: Surendra, Graves, Jellum. Phys.Rev. A41. 1112 (1990).
  if (energy < 1e-30) costheta = 1.0;
  else coschi = 1 + 2.0 * (1.0 - pow(energy + 1.0, frand())) /  energy;  
  */ 
  
  // Okhrimovskyy, Bogaerts, Gijbels. Phys. Rev. Vol 65, 037402 (2002).  
  Scalar R = frand();
  coschi = 1 - (2*R)/(1+8*(energy/27.21)*(1-R));  
   
  Scalar chi = acos(coschi);
  Scalar phi = TWOPI * frand(); 
  Scalar m1 = eSpecies -> get_m();
  Scalar m2 = nSpecies -> get_m(); 
  v *= sqrt(1. - 2. * (m1*m2/(m1+m2)/(m1+m2))  * (1. - coschi)); 
  ue = v*  newcoordiante(ue , chi, phi);
}

void MCC_new::eScattering_R()
{
  Scalar thetaS, phiS;
  elasticScatteringAngles(energy,thetaS, phiS);
  energy -= elasticEnergy(energy, thetaS);
  ue = newMomentum(ue, energy, thetaS, phiS); 
}

// Method for calculating the scattering angles, relativistic case

void MCC_new::elasticScatteringAngles(const Scalar& eImpact, Scalar& thetaScatter, Scalar& phiScatter)
{
  //First, we generate a Monte Carlo value for thetaScatter
  int atomicNum = nSpecies->get_atomicnumber();

  //the beta from the get_beta is the normalized beta so I use the old calculation, to be discussed
  // mindgame: OK to delete?
  /*
    Scalar gMinus1 = eImpact / ELECTRON_MASS_EV;
    Scalar gamma = gMinus1 + 1.;
    Scalar beta = sqrt((gamma + 1.) * gMinus1) / gamma;
  */

  // Scalar beta = pga->get_beta2(eImpact);
  Scalar beta = eSpecies->get_beta2(eImpact);
  Scalar a_bohr = 0.529E-10;      // mindgame: these move to pd1.hpp?
  Scalar alpha = 1. / 137.;
  Scalar h = 1.055e-34 * 6241457005723417000.;                        
  Scalar p = beta * (eImpact + ELECTRON_MASS_EV) / SPEED_OF_LIGHT;

  // unused variable according to g++. should be deleted.  JH, Aug. 22, 2005.
  // Scalar p_2 = 1. / SPEED_OF_LIGHT * sqrt(2. * eImpact * ELECTRON_MASS_EV + eImpact * eImpact);

  Scalar A_1 = 0.25 * pow((h / p),2.) / pow((0.885 * pow( atomicNum, -1./3.) * a_bohr),2.);
  Scalar A_2 = 1.13 + 3.76 * pow((alpha * atomicNum / beta),1);
  Scalar A = A_1 * A_2;
  Scalar fTheta = frand();
  Scalar cosine = 2. * A + 1 +( 2 * A * (A + 1) / (fTheta - A - 1));

  /////////////////////////////////////////////////////////////////////
  //Sometimes, calculation of A is incorrect in the region of low energy
  //I added this criteria C.H Lim 6/1/2005 'need to be discussed'
  if (cosine > -1. && cosine <1)  thetaScatter = acos(cosine);    
  else if (cosine > 1.)  thetaScatter = acos(1.)+1E-30;
  else if (cosine < -1.) thetaScatter = acos(-1.)+1E-30;
  else thetaScatter = 0.;
  //////////////////////////////////////////////////////////////////////

  phiScatter = TWOPI * frand();

#ifdef DEBUG
  FILE *fp;
  ostring ffnname = "relelastic_angle.dat";
  fp = fopen(ffnname, "a");
  fprintf(fp, "%g \t %g \t %g \n", (thetaScatter), phiScatter, fTheta );
  fclose(fp);
#endif
  return;
}

// Method for calculating the change of the energy in case of elastic collision, relativistic case

Scalar MCC_new::elasticEnergy( const Scalar& eImpact, Scalar& thetaScatter)
{
  // mindgame: What are these for?
  /*
    Scalar gMinus1 = eImpact / ELECTRON_MASS_EV;
    Scalar gamma_elec = gMinus1 + 1;
    Scalar beta_elec = eSpecies->get_beta2(eImpact); ////instead of pga->get_beta2
    Scalar W = sqrt(pow(ELECTRON_MASS+PROTON_MASS,2.)*SPEED_OF_LIGHT_SQ*SPEED_OF_LIGHT_SQ+2.*(eImpact-ELECTRON_MASS*SPEED_OF_LIGHT_SQ)*PROTON_MASS*SPEED_OF_LIGHT_SQ); 
    Scalar newEnergy = 1/W*(pow(ELECTRON_MASS*SPEED_OF_LIGHT_SQ,2.)+eImpact*PROTON_MASS*SPEED_OF_LIGHT_SQ);
  */

  Scalar M = nSpecies -> get_m()/PROTON_CHARGE;
  Scalar newEnergy = ((eImpact+ELECTRON_MASS_EV)*sin(thetaScatter)*sin(thetaScatter)+M*SPEED_OF_LIGHT_SQ*(1-cos(thetaScatter)))*eImpact*(eImpact+2*ELECTRON_MASS_EV);
  newEnergy *= 1/((eImpact+M*SPEED_OF_LIGHT_SQ)*(eImpact+M*SPEED_OF_LIGHT_SQ)-eImpact*(eImpact+2*ELECTRON_MASS_EV)*cos(thetaScatter)*cos(thetaScatter));

  return newEnergy;
}

void MCC_new::samemassElastic(const Xsection& cx) 
  // It is assumed to be non- relativistic. Only works for particles of equivalent masses.
{
  // We move to the frame of the neutral here.
  Vector3 ui_init = ui;
  ui -= un; 
  Scalar coschi = sqrt(frand());
  Scalar chi = acos(coschi);
  Scalar phi = TWOPI * frand();
  v = ui.magnitude() * coschi; 
  ui = v * newcoordiante(ui, chi, phi) + un;
  un +=  ui_init - ui;
}

// --- Ionization processes:
void MCC_new::eIonization(const Xsection& cx) 
{
  // We don't substract the velocity of the neutral: assumed the electron has a much higher velocity: energy, ue are valid. 
  Scalar thres = cx.get_threshold();
  ionization(thres);
  // Creation of ionized species
  if (sec_i)
    { 
      add_particles(sec_i, un);
    }
  //Delete the neutral:
  // mindgame: awkward way
  un = DELETE_PARTICLE;
}

void MCC_new::ionization(const Scalar threshold)
{
  // First, calculate the energy of the ejected electron
  Scalar eEjected = ejectedEnergy(threshold, energy);
 
  // Next, calculate the scattering angles of both the primary electron and the ejected electron
  Scalar thetaP, phiP, thetaS, phiS;
  primarySecondaryAngles(threshold, energy, eEjected, thetaP, phiP, thetaS, phiS);
  Vector3 uEjected = newMomentum(ue, eEjected, thetaS, phiS);
  
  if (sec_e)  //Creations necessarily as particles and with the default weight.
    {
      add_particles(sec_e, uEjected);
    } 
  
  // Scatters the primary electron
  Scalar newEnergy = energy - threshold - eEjected;
  ue = newMomentum(ue, newEnergy, thetaP, phiP); 
}

//Method for calculating energy imparted to a secondary electron
Scalar MCC_new::ejectedEnergy(const Scalar& I, const Scalar& impactEnergy)
{
  // threshold in eV
  if (impactEnergy <= I) return 0.;
  Scalar tPlusMC = impactEnergy + ELECTRON_MASS_EV;
  Scalar twoTplusMC = impactEnergy + tPlusMC;  
  Scalar tPlusI = impactEnergy + I;
  Scalar tMinusI = impactEnergy -I;
  Scalar invTplusMCsq = 1. / (tPlusMC * tPlusMC);
  Scalar tPlusIsq = tPlusI * tPlusI;
  Scalar iOverT = I / impactEnergy;
  Scalar funcT1 = 14. / 3. + .25 * tPlusIsq * invTplusMCsq 
    - ELECTRON_MASS_EV * twoTplusMC * iOverT * invTplusMCsq;
  
  Scalar funcT2 = 5. / 3. - iOverT - 2. * iOverT * iOverT / 3. 
    + .5 * I * tMinusI * invTplusMCsq
    + ELECTRON_MASS_EV * twoTplusMC * I * invTplusMCsq * log(iOverT) / tPlusI;
  Scalar aGreaterThan = funcT1 * tMinusI / funcT2 / tPlusI;

  // We may have to try several times before the "rejection method"
  // let us keep our ejected energy test value, wTest

  int needToTryAgain = 1;
  Scalar randomFW, wTest, wPlusI, wPlusIsq;
  Scalar invTminusW, invTminusWsq, invTminusW3;
  Scalar probabilityRatio;

  while (needToTryAgain == 1)
    {
      randomFW = frand() * aGreaterThan;
      wTest = I * funcT2 * randomFW / (funcT1 - funcT2 * randomFW);
      wPlusI = wTest + I;
      wPlusIsq = wPlusI * wPlusI;
      invTminusW = 1. / (impactEnergy - wTest);
      invTminusWsq = invTminusW * invTminusW;
      invTminusW3 = invTminusW * invTminusWsq;
      probabilityRatio = ( 1. + 4. * I / wPlusI / 3. + wPlusIsq 
			   * invTminusWsq + 4. * wPlusIsq * invTminusW3 / 3. 
			   -ELECTRON_MASS_EV * twoTplusMC * wPlusI * invTminusW * invTplusMCsq + wPlusIsq * invTplusMCsq) / funcT1;
      if (probabilityRatio >= (Scalar)frand()) needToTryAgain = 0;
    }
  
#ifdef DEBUG
  FILE *fp;
  ostring ffnname ="secondary.dat";
  fp = fopen(ffnname(), "a");
  fprintf(fp, "%g \t %g \t %g \n",wTest,impactEnergy,wTest/impactEnergy );
  fclose(fp);
#endif  
  return wTest;
}

// Method for calculating the scattering angles of the impacting and ejected electrons 
void MCC_new::primarySecondaryAngles( const Scalar& I, const Scalar& eImpact, const Scalar& eSecondary, 
				  Scalar& thetaPrimary, Scalar& phiPrimary, Scalar& thetaSecondary, 
				  Scalar& phiSecondary)
{
  // threshold in eV
  if (eImpact <= I)
    {
      thetaPrimary = 0.;
      thetaSecondary = 0.;
      phiPrimary = 0.;
      phiSecondary = 0.;
      return;
    }

  // First, we generate a Monte Carlo value for thetaPrimary
  Scalar ePrimary = eImpact - eSecondary - I;
  Scalar w = ePrimary;
  Scalar wPlusI = w + I;
  Scalar wPlusI2MC = wPlusI + 2. * ELECTRON_MASS_EV;
  Scalar tPlus2MC = eImpact + 2. * ELECTRON_MASS_EV;

  Scalar alpha; 
  alpha = ELECTRON_MASS_EV / (eImpact + ELECTRON_MASS_EV);
  alpha = 0.6 * alpha;

  Scalar g2_T_W = sqrt( wPlusI * tPlus2MC / wPlusI2MC / eImpact );
  Scalar g3_T_W = alpha * sqrt( I * ( 1. - wPlusI / eImpact ) / w );

  Scalar fTheta = frand();
  thetaPrimary = acos( g2_T_W + g3_T_W * tan( ( 1. - fTheta ) 
					      * atan( ( 1. - g2_T_W ) / g3_T_W ) - fTheta 

					      * atan( ( 1. + g2_T_W ) / g3_T_W)));
  if (g3_T_W == 0)
    {
      thetaPrimary = acos(0.);
    }
  phiPrimary = TWOPI * frand();

  // Third, we generate a Monte Carlo value for thetaSecondary
  w = eSecondary;
  wPlusI = w + I;
  wPlusI2MC = wPlusI + 2. * ELECTRON_MASS_EV;
  g2_T_W = sqrt( wPlusI * tPlus2MC / wPlusI2MC / eImpact );
  g3_T_W = alpha * sqrt( I * ( 1. - wPlusI / eImpact ) / w );
  fTheta = frand();

  // have to figure out the difference of atan and atan2
  thetaSecondary =  acos( g2_T_W + g3_T_W * tan( ( 1. - fTheta ) 
						 * atan( ( 1. - g2_T_W ) / g3_T_W ) - fTheta 
						 * atan( ( 1. + g2_T_W ) / g3_T_W)));

  // Fourth, generate a monte Carlo value for phiSecondary
  Scalar eCritical = 10. * I;
  Scalar a_T_w = 0;
  if ( eSecondary>eCritical && ePrimary>eCritical )
    {
      a_T_w = sqrt((eSecondary - eCritical) * (ePrimary - eCritical)) / eCritical;
    }
  Scalar delta = TWOPI / (1. + a_T_w);
  phiSecondary = phiPrimary + M_PI + (frand() - .5) * delta;

#ifdef DEBUG
  FILE *fp;
  ostring ffnname = "ion_angle.dat";
  fp = fopen(ffnname(), "a");
  fprintf(fp, "%g \t %g \t %g \t %g \n", thetaPrimary, phiPrimary, thetaSecondary, phiSecondary);
  fclose(fp);
#endif
  return;
}

// --- Charge exchange:

void MCC_new::ElectronExchange(const Xsection& cx)
{
  Vector3 temp = ui;
  ui = un;
  un = temp; // neutral velocity encountered. 
}

// --- Subfunctions of general use (for most of the processes): 


// Calculate the  new momentum
Vector3 MCC_new::newMomentum(const Vector3& U_initial, 
 			 const Scalar& newEnergy, const Scalar& theta, const Scalar& phi) 
  // Comment (07/06): Is it not the velocity, not the momentum which is returned?

{
  /*
    Scalar gMin1 = newEnergy / ELECTRON_MASS_EV;
    Scalar u0new = SPEED_OF_LIGHT * sqrt(1-1/(gMin1+1)*1/(gMin1+1));
  */
  
  Scalar u0new = eSpecies->get_speed2(newEnergy);
  u0new /= eSpecies->get_dxdt(); 

#ifdef DEBUG
  ostring ffnname = "kinetic_energy.dat";
  FILE *fp;
  fp = fopen(ffnname(), "a");
  fprintf(fp,"before vel =%g \t after vel =%g \n ",u.magnitude(), u0new);
  fclose(fp);
#endif
  return u0new *  newcoordiante(U_initial , theta, phi);
}

// Return the Coordiante normalized to 1, from emission energy and angles (purely geometric function)
Vector3 MCC_new::newcoordiante(Vector3 vel , Scalar theta, Scalar phi) 
{
  Scalar u10 = vel.e1();
  Scalar u20 = vel.e2();
  Scalar u30 = vel.e3();
  Scalar u0 = sqrt(u10 * u10 + u20 * u20 + u30 * u30);
  Scalar cosT0 = u30 / u0;
  Scalar sinT0 = sqrt(1. - cosT0 * cosT0);
  Scalar cosPhi0;
  Scalar root = sqrt(u0 * u0 - u30 * u30);
  if (root <= fabs(u10)) cosPhi0 = 1;
  else cosPhi0 = u10 / root;
  Scalar sinPhi0 = sin(acos(cosPhi0));
  Scalar cosT = cos(theta);
  Scalar sinT = sin(theta);
  Scalar cosPhi = cos(phi);
  Scalar sinPhi = sin(phi);
  Scalar e1new = (cosPhi * sinT * cosPhi0 * cosT0 - sinPhi * sinT * sinPhi0 + cosT * cosPhi0 * sinT0);
  Scalar e2new = (cosPhi * sinT * sinPhi0 * cosT0 + sinPhi * sinT * cosPhi0 + cosT * sinPhi0 * sinT0);
  Scalar e3new = (-cosPhi * sinT * sinT0 + cosT * cosT0);
  //fprintf(stderr,"x enwe =%g\t ynew =%g\t znew =%g\n", e1new, e2new, e3new);

#ifdef DEBUG  
  Scalar e1 = (cosPhi * sinT * cosPhi0 * cosT0 - sinPhi * sinT * sinPhi0 + cosT * cosPhi0 * sinT0)*(cosPhi * sinT * cosPhi0 * cosT0 - sinPhi * sinT * sinPhi0 + cosT * cosPhi0 * sinT0);
  Scalar e2 =  (cosPhi * sinT * sinPhi0 * cosT0 + sinPhi * sinT * cosPhi0 + cosT * sinPhi0 * sinT0)* (cosPhi * sinT * sinPhi0 * cosT0 + sinPhi * sinT * cosPhi0 + cosT * sinPhi0 * sinT0);
  Scalar e3 = (-cosPhi * sinT * sinT0 + cosT * cosT0)*(-cosPhi * sinT * sinT0 + cosT * cosT0);
  
  fprintf(stderr,"sum in newmomentum = %g \n",sqrt(e1+e2+e3));
#endif
  Vector3 eNew(e1new, e2new, e3new);
  return eNew;
}

void MCC_new::add_particles (Species* sp,  const Vector3 & vel)
{  
 // mindgame: the better way is to get nc2p from the inputfile
  if(useSecDefaultWeight)
    {
      Scalar defaultweight = sp->get_np2c();
      Scalar weight_ratio = w_ref/defaultweight;
      int creations = (int) weight_ratio;
      weight_ratio -= creations;      
      if (frand()< weight_ratio) creations ++;
      while (creations --) sp->add_particle(x, vel.e1(), vel.e2(), vel.e3(),false, false, defaultweight);
    }
  else 
    sp->add_particle(x, vel.e1(), vel.e2(), vel.e3(),false, false, w_ref);
}


