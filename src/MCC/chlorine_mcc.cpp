// Aug, 10, 2012
// Modified for chlorine by Jon T. Gudmundsson and Shao Huang, spring 2013
// Final corrections by JTG and SH, 7/16/13; minor correction by MAL, 7/17/13
//
// Update for fluid-fluid interactions; replace add_particles with add_product, MAL200303
#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "xsections/xsection.hpp"
#include "reactions/reaction.hpp"
#include "MCC/mcc.hpp"
#include "MCC/chlorine_mcc.hpp"
#include "fields/fields.hpp" //add for debug, MAL 9/6/12

CHLORINE_MCC::CHLORINE_MCC(oopicList<Species>* slist, ReactionList _reactionlist, Scalar _dt, Fields *tf) : MCC(slist, _reactionlist, _dt, tf)
{
  setdefaults();
}

void CHLORINE_MCC::setdefaults()
{
sec_Cl = sec_Cl_neg_ion = sec_Cl2 = sec_Cl2_ion = sec_Cl_ion= NULL;
}

//// The followings designate the products for each reaction type. 
void CHLORINE_MCC::init(Species *_reactant1, Species *_reactant2)
{
  init_mcc(_reactant1, _reactant2);
   for (int i=reactionlist.num()-1; i>=0; i--)
  {
    switch (reactionlist.data(i)->get_reactiontype())
  {
		case E_ELASTIC: // add for DEBUG, MAL 9/6/12
		Nelastic=0;
                break;

      	case E_Cl2IONIZATION: 		// ionization: e + Cl2 -> Cl2+ + 2e
     //         Nionization = 0;
      	
      		sec_e = reactionlist.data(i)->get_products(1);
      		sec_Cl2_ion = reactionlist.data(i)->get_products(0);
      	break;

case EDISSSingleIzCl2:
       sec_e = reactionlist.data(i)->get_products(2);
       sec_Cl_ion = reactionlist.data(i)->get_products(1);
       sec_Cl = reactionlist.data(i)->get_products(0);
      break;

	case EDISSDoubleIzCl2:
       	sec_e = reactionlist.data(i)->get_products(3);
       	sec_e = reactionlist.data(i)->get_products(2);
       	sec_Cl_ion = reactionlist.data(i)->get_products(1);
       	sec_Cl_ion = reactionlist.data(i)->get_products(0);
      	break;

    case E_DISS_ATTACHMENT_Cl2:
          Ndissattach = 0;
          sec_Cl = reactionlist.data(i)->get_products(1); 
      sec_Cl_neg_ion = reactionlist.data(i)->get_products(0);
      break;


case POLARDISSCl2:
      sec_e = reactionlist.data(i)->get_products(2);
      sec_Cl_ion = reactionlist.data(i)->get_products(1);
      sec_Cl_neg_ion = reactionlist.data(i)->get_products(0);
break;

      case E_ClIONIZATION:
      sec_e = reactionlist.data(i)->get_products(1);
      sec_Cl_ion = reactionlist.data(i)->get_products(0);
      break;

    case E_DETACHMENT_negCl:		
      sec_e = reactionlist.data(i)->get_products(1); 
      sec_Cl = reactionlist.data(i)->get_products(0);
      break;   

    	// case E_Double_DET_negCl:	
      	// sec_e = reactionlist.data(i)->get_products(2); 	
      	// sec_e = reactionlist.data(i)->get_products(1); 
      	// sec_Cl_ion = reactionlist.data(i)->get_products(0);
      	// break;   

	case E_Double_DET_negCl:
//        sec_e = reactionlist.data(i)->get_products(3);  //add reaction in chlorine_gas.hpp with only 2 sec electrons, MAL 7/17/13
        sec_e = reactionlist.data(i)->get_products(2);
        sec_e = reactionlist.data(i)->get_products(1);
        sec_Cl_ion = reactionlist.data(i)->get_products(0);
        break;

    case NEGCl2_DETCH:
      sec_e = reactionlist.data(i)->get_products(2);
      sec_Cl = reactionlist.data(i)->get_products(1);
      sec_Cl2 = reactionlist.data(i)->get_products(0);
      break;
    case NEGCl_DETACHMENT:  
      sec_e = reactionlist.data(i)->get_products(1);
      sec_Cl2 = reactionlist.data(i)->get_products(0);
      break;


    case E_DISS_REC_posCl2:
      sec_Cl = reactionlist.data(i)->get_products(1);
      sec_Cl = reactionlist.data(i)->get_products(0);
      break;

case MN_posCl2negCl:
sec_Cl = reactionlist.data(i)->get_products(2);
sec_Cl = reactionlist.data(i)->get_products(1);
      sec_Cl = reactionlist.data(i)->get_products(0);
break;

      case MN_posClnegCl:
          Nmutneut = 0;
          sec_Cl = reactionlist.data(i)->get_products(0);
//      sec_Cl = reactionlist.data(i)->get_products(1); // not needed, as set in previous line, MAL 8/11/12
    break;

		case CEposCl2Cl2:
		Nchargetransfer=0;
		break;

case CEposClCl2:
        sec_Cl = reactionlist.data(i)->get_products(1);
        sec_Cl2_ion = reactionlist.data(i)->get_products(0);
				break;
	case CEposCl2Cl:
        sec_Cl_ion = reactionlist.data(i)->get_products(1);
        sec_Cl2 = reactionlist.data(i)->get_products(0);
	break;


case E_LOSS_Cl2:
break;


   case E_LOSS_Cl2_3Pu:
     sec_Cl = reactionlist.data(i)->get_products(0);
break;
   case E_LOSS_Cl2_1Pu:
     sec_Cl = reactionlist.data(i)->get_products(0);
break;
   case E_LOSS_Cl2_3Pg:
     sec_Cl = reactionlist.data(i)->get_products(0);
break;
   case E_LOSS_Cl2_1Pg:
     sec_Cl = reactionlist.data(i)->get_products(0);
break;
   case E_LOSS_Cl2_3Su:
     sec_Cl = reactionlist.data(i)->get_products(0);
break;


   case E_LOSS_Cl2_R1Pu:
     sec_Cl = reactionlist.data(i)->get_products(0);
break;
   case E_LOSS_Cl2_R1Su:
     sec_Cl = reactionlist.data(i)->get_products(0);
break;

case Cl2Cl2_ELASTIC:
break;
case ClCl_ELASTIC:
break;
case El_ClCl2:
break;
case El_posCl2Cl2:
break;
case El_posClCl2:
break;
case El_negClCl2:
break;

    case Cl2FRAG:  
      sec_Cl_ion = reactionlist.data(i)->get_products(2);
      sec_Cl = reactionlist.data(i)->get_products(1);
      sec_Cl2 = reactionlist.data(i)->get_products(0);
      break;


/////////////////////////////////////////////////////////////////////////////






    default:
      break;	
    }
  }
}


//// the followings designate the function being used to calculate the dynamics of the reaction.
void CHLORINE_MCC::dynamic(Reaction * _reaction)
{
Reaction * reaction = _reaction;
const Xsection  & cx  = *(reaction->get_xsection());
ReactionType reactiontype = reaction->get_reactiontype();
//  printf("\nCHLORINE_MCC::ReactionType: %s\n", ReactionTypeName(reactiontype).c_str());
#ifdef DEBUGmcc
  totalnumberofcollisions ++;
  printf("\nReactionType: %s\n", ReactionTypeName(reactiontype).c_str());
#endif  
if (energy > cx.get_threshold()) {
  switch(reactiontype)
    {

	case E_ELASTIC:  
	       
	      	eElastic(cx); 	
	break;

        case E_Cl2IONIZATION: 	// ionization: e + Cl2 -> Cl2+ + 2e
       		eIonizationcl2(cx);
      	break;

case EDISSSingleIzCl2:
edisssingleizcl2(cx);
break;

	case EDISSDoubleIzCl2:
	edissdoubleizcl2(cx);
	break;
    case E_DISS_ATTACHMENT_Cl2:
      edissattachcl2(cx);
      break;

case POLARDISSCl2:
polardisscl2(cx);
break;

case E_ClIONIZATION:
eIonizationcl(cx);
break;

    case E_DETACHMENT_negCl:
      edetachmentnegcl(cx);
      break;

    	case E_Double_DET_negCl:
      	edoubledetnegcl(cx);
      	break;

 case NEGCl2_DETCH:
negcl2detch(cx);
break;

    case NEGCl_DETACHMENT:  
      negcldetachment(cx);
      break;


    case E_DISS_REC_posCl2:
  edissrecposcl2(cx);
      break;

    case MN_posCl2negCl:
      mnposcl2negcl(cx);
      break;

    case MN_posClnegCl:
      mnposclnegcl(cx);
      break;

      case CEposClCl: 
      ElectronExchange(cx);
      break;

      case CEposCl2Cl2: 
      ElectronExchange(cx);
      break;

case CEposClCl2:
	{
      sec_Cl2_ion=reaction->get_products(0);
      sec_Cl=reaction->get_products(1);
	Scalar ReleasedEnergy = 0;
	if (!(reaction -> is_characteristic_value_empty())) ReleasedEnergy = reaction -> get_characteristic_values(0);
      nonresElectronExchange(sec_Cl2_ion, sec_Cl, ReleasedEnergy);
			}
      break; 

      case CEnegClCl: 
      ElectronExchange(cx);
      break;

    case E_LOSS_Cl2:
      eloss_cl2(cx);
      break;
    case E_LOSS_Cl:
      eloss_cl(cx);
      break;

case E_LOSS_Cl2_3Pu:
eloss_cl2_3pu(cx);
break;
case E_LOSS_Cl2_1Pu:
eloss_cl2_1pu(cx);
break;
case E_LOSS_Cl2_3Pg:
eloss_cl2_3pg(cx);
break;
case E_LOSS_Cl2_1Pg:
eloss_cl2_1pg(cx);
break;
case E_LOSS_Cl2_3Su:
eloss_cl2_3su(cx);
break;

case E_LOSS_Cl2_R1Pu:
eloss_cl2_r1pu(cx);
break;
case E_LOSS_Cl2_R1Su:
eloss_cl2_r1su(cx);
break;

case Cl2Cl2_ELASTIC:
samemassElastic(cx);
break;
case ClCl_ELASTIC:
samemassElastic(cx);
break;

case El_ClCl2:
halfmassElastic(cx);
break;

case El_posCl2Cl2:
samemassElastic(cx);
break;

case El_posClCl2:
halfmassElastic(cx);
break;
case El_negClCl2:
halfmassElastic(cx);
break;

case Cl2FRAG:
fragmentation_cl2(cx);
break;


		 case CEposCl2Cl:
		{
      sec_Cl2=reaction->get_products(0);
      sec_Cl_ion=reaction->get_products(1);
			Scalar ReleasedEnergy = 0;
			if (!(reaction -> is_characteristic_value_empty())) ReleasedEnergy = reaction -> get_characteristic_values(0);
      nonresElectronExchange(sec_Cl_ion, sec_Cl2, ReleasedEnergy);
			}

      break;


///////////////////////////////////////////////////////////////////


    default: 
      fprintf(stderr,"Unrecognized collision type!\n"); 
    }
    	} //end if (energy > cx.get_threshold())
}

/////////////////////////////
// Reaction dynamics       // 
/////////////////////////////
// Simple models are used: 
// non-relativistic processes, 2 electronisotropic behaviors, ion/neutral velocity not modified by a single electron.


void CHLORINE_MCC::eIonizationcl2(const Xsection& cx)
{	
  	Any_eIonization(cx, sec_Cl2_ion);
	un = DELETE_PARTICLE;  // CORRECTION, see Any_eIonization below, MAL 9/25/09
}

void CHLORINE_MCC::edisssingleizcl2 (const Xsection &cx) // analogy to dissionization(cx) in CHLORINE_MCC.cpp
{
  Any_eIonization2(cx, sec_Cl2_ion); 
  Any_DissociationInTwo(un, sec_Cl, sec_Cl_ion, 0.7); //Revised July 8 2013
// Threshold is 15.7 eV.  Lost energy is 15 eV  (according to the energy diagram (figure 1) 
// in christophorou99_131, 15 eV is total energy of Cl+(3Pg) + Cl(2Pu), lost energy here means 
// the energy lost from electrons, and this energy will go to heavy particles.)
    un = DELETE_PARTICLE; // DELETE THE TARGET NEUTRAL O2, MAL 11/25/09
}

void CHLORINE_MCC::edissdoubleizcl2 (const Xsection &cx) // analogy to dissionization(cx) in CHLORINE_MCC.cpp
{
  Any_eIonization2(cx, sec_Cl2_ion); // electron energy after collision = electron energy before collision - 31.13(threshold). Then 31.13 gose into Cl2+.
  Any_DissociationInThree(un, sec_e, sec_Cl_ion, sec_Cl_ion, 3.61); // Then 31.13-2*13.76(potential energy of Cl+)=3.61 is split between Cl+ + Cl+ + e

// This reaction is split into two processes. 1. e + Cl2 -> Cl2+ + e
// 2. Cl2+ -> Cl+ + Cl+ + e

    un = DELETE_PARTICLE; // DELETE THE TARGET NEUTRAL O2, MAL 11/25/09
}



void CHLORINE_MCC::edissattachcl2(const Xsection& cx)
{
  Any_DissociationInTwo(un, sec_Cl, sec_Cl_neg_ion, energy+1.13); 
// -0.93 is the energy of Cl + Cl-, "energy" denotes the energy of incident electron. 
//-0.93  is the potential energy of Cl(2Pu) + Cl-(1Sg), which is shown in Fig. 1 in Christophorou and Olthoff,
//  Physical and Chemical Reference Data 28(1), 131-169.
// see also Peyerimhoff and Buenker, Chemical Physics 57 (1981) 279-296
  ue = DELETE_PARTICLE; // change un to ue; MAL 4/12/09
}

void CHLORINE_MCC::polardisscl2 (const Xsection &cx) 
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_Cl_neg_ion, sec_Cl_ion, 0.0); 
// The dissociation limit of  Cl+ + Cl- is 9.44 eV and dissociation energy is 2.48 eV so
// 9.44 + 2.48 = 11.92 eV, threshold of this reaction is 11.9, so 11.9-11.92=0.0 eV
// Peyerimhoff and Buenker, Chemical Physics 57 (1981) 279-296 see discussion on page 295
}

void CHLORINE_MCC::eIonizationcl(const Xsection& cx)
{
	
  Any_eIonization(cx, sec_Cl_ion);
	un = DELETE_PARTICLE; // CORRECTION, see Any_eIonization below, MAL 9/25/09
}

void CHLORINE_MCC::edetachmentnegcl(const Xsection& cx)
{
	// The target is iSpecies = Cl-
	Any_eDetachment(cx, sec_Cl);
	ui = DELETE_PARTICLE; // MUST DELETE THE ION, see Any_eDetachment below, MAL 12/28/09
}

void CHLORINE_MCC::edoubledetnegcl(const Xsection& cx)
{
	// // The target is iSpecies = Cl-
	// Any_eDetachment(cx, sec_Cl);// e + Cl- -> Cl + 2e 
	// Any_DissociationInTwo(ui, sec_e, sec_Cl_ion, 0); // Cl -> Cl+ + e
	// ui = DELETE_PARTICLE;

	// The target is iSpecies = Cl-, MAL, July 11 2013
        // fprintf(stderr,"doubledetachmentnegativeion\n");
//  add_particles(sec_Cl_ion, ui); //add a Cl+ with velocity of ui
  if(sec_Cl_ion) add_product(sec_Cl_ion, ui); //add a Cl+ with velocity of ui
        Scalar Renergy = energy - 28.6; //released energy should always be positive for a reaction to occur
        if (Renergy < 0) Renergy = 0; //but let's make sure it is never negative
	

        Any_DissociationInThree(ui, sec_e, sec_e, sec_e, Renergy); // add three electrons at 120 degrees, each having Renergy/3, and then delete the Cl-

	ue = DELETE_PARTICLE; //delete the incident electron
        

}


void CHLORINE_MCC::negcl2detch (const Xsection &cx) 
{
  Any_DissociationInThree(ui, un, iSpecies->get_m(), nSpecies->get_m(), sec_e, sec_Cl2, sec_Cl, -3.61);
  ui = DELETE_PARTICLE;
}


void CHLORINE_MCC::negcldetachment (const Xsection &cx)
{

// NOTE: The  unstable (repulsive) Cl2- complex dissociates into Cl2 and an electron; in the CM system,
// -0.93 is potential energy of Cl- + Cl, 0 is the potential energy of Cl2. -0.93-0=-0.93
// Peyerimhoff and Buenker, Chemical Physics 57 (1981) 279-296
// also  2.48 - 3.61 = - 1.13 eV  which is what we use  
  Any_DissociationInTwo(ui, un, iSpecies->get_m(), nSpecies->get_m(), sec_e, sec_Cl2, -1.13); // 
  ui = DELETE_PARTICLE;
}

void CHLORINE_MCC::edissrecposcl2(const Xsection& cx) 
{
  Any_DissociationInTwo(ui, sec_Cl, sec_Cl, 11.28); // Must pass the released energy, not the threshold energy, 
  // which is zero! 11.28 eV =13.76 eV - 2.48 eV, where 13.76 eV is the energy of Cl2+ and 2.48 eV is the energy of Cl+Cl. 
  ue = DELETE_PARTICLE;
}


void CHLORINE_MCC::mnposclnegcl (const Xsection &) 
{
	// The iSpecies is Cl-, the nSpecies is Cl+  
//  if (sec_Cl && iSpecies->get_energy_eV(iSpecies->get_dxdt()*ui) > sec_Cl->get_energyThrToAddAsPIC()) add_particles(sec_Cl, ui); //add Cl with the velocity of Cl-
  if(sec_Cl) add_product(sec_Cl, ui);
//  if (sec_Cl && nSpecies->get_energy_eV(nSpecies->get_dxdt()*un) > sec_Cl->get_energyThrToAddAsPIC()) add_particles(sec_Cl, un);//add Cl with the velocity of Cl+
  if(sec_Cl) add_product(sec_Cl, un);
  ui = DELETE_PARTICLE;
  un = DELETE_PARTICLE;
}

void CHLORINE_MCC::mnposcl2negcl (const Xsection &) 
{
  Any_DissociationInThree(ui, un, iSpecies->get_m(), nSpecies->get_m(), sec_Cl, sec_Cl, sec_Cl,5.59);
//5.59 eV = 11.48 eV - 0.93 eV -2*2.48 eV Ionization potential Cl2+ is 11.48,  and potential energy of Cl + Cl- is -0.93 eV, 
// potential energy of Cl + Cl is 2.48 eV 
  ui = DELETE_PARTICLE;
  un = DELETE_PARTICLE;

}


void CHLORINE_MCC::eloss_cl2(const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());


}
void CHLORINE_MCC::eloss_cl(const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
}


void CHLORINE_MCC::eloss_cl2_3pu (const Xsection &cx) 
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_Cl, sec_Cl, 0.76);
 }
void CHLORINE_MCC::eloss_cl2_1pu (const Xsection &cx) 
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_Cl, sec_Cl, 1.56);
 }
void CHLORINE_MCC::eloss_cl2_3pg (const Xsection &cx) 
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_Cl, sec_Cl, 3.75);
 }
void CHLORINE_MCC::eloss_cl2_1pg (const Xsection &cx) 
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_Cl, sec_Cl, 4.38);
 }
void CHLORINE_MCC::eloss_cl2_3su (const Xsection &cx) 
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_Cl, sec_Cl, 4.32);
 }

void CHLORINE_MCC::eloss_cl2_r1pu (const Xsection &cx) 
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_Cl, sec_Cl, 6.74);
 }
void CHLORINE_MCC::eloss_cl2_r1su (const Xsection &cx) 
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_Cl, sec_Cl, 6.84);
 }

void CHLORINE_MCC::fragmentation_cl2 (const Xsection &cx)  // Added by JTG August 17 2009
{

// Must subtract 3.52 V (dissociation energy of Cl2+ into Cl+ and Cl from the CM energy, MAL 10/2/12
  Any_DissociationInThree(ui, un, iSpecies->get_m(), nSpecies->get_m(), sec_Cl2, sec_Cl, sec_Cl_ion, -3.52);
  ui = DELETE_PARTICLE;
}


/////////////////////////////////////////////////////////////////
//Functions used in common:

void CHLORINE_MCC::Any_eExcitation (const Xsection& cx, Scalar targetmass) 
{
  energy -= cx.get_threshold();
//	if (energy < 0.) printf("\nCHLORINE_MCC::Any_eExcitation, energy=%g\n",energy); //FOR DEBUG, MAL 8/25/12
  v = eSpecies->get_speed2(energy); 
  v /= eSpecies->get_dxdt();
	
  // Isotropic scattering in target rest frame (also CM system to a very good approximation) with the reduced energy.
  Scalar coschi = 1 - 2*frand();
  Scalar chi = acos(coschi);
  Scalar phi = TWOPI*frand();
  Scalar me = eSpecies -> get_m();
  v *= sqrt(1. - 2. * me * targetmass / (me + targetmass) / (me + targetmass) * (1. - coschi)); 
  ue = v*  newcoordiante(ue-un , chi, phi) + un;

/*
	// RESTORE SURENDRA'S FORMULA FOR COMPARISON TO XPDP1, MAL 8/25/12
  // Surendra's formula: Surendra, Graves, Jellum. Phys.Rev. A41. 1112 (1990).
	Scalar coschi;
  if (energy < 1e-30) coschi = 1.0;
  else coschi = 1 + 2.0 * (1.0 - pow(energy + 1.0, frand())) /  energy;  
  Scalar chi = acos(coschi);
  Scalar phi = TWOPI*frand();
  ue = v*  newcoordiante(ue , chi, phi);  // note me << mneutral is assumed for excitations in xpdp1, MAL 8/25/12
 */
}


void CHLORINE_MCC::Any_DissociationInTwo(Vector3 & target_velocity,
					     Species* s1, Species* s2,
					     Scalar ReleasedEnergy) 
{
  if (s1||s2)
    { 
      Scalar mu;
      Scalar m1, m2;
      if (s1) m1 = s1->get_m();
      if (s2) m2 = s2->get_m();
      if (s1 && s2) 
        mu = 1./(1/m1+1/m2);
      else
      {
	// mindgame: in case that only one of two is known, m1 << m2 is assumed.
	if (s1) 
          mu = m1;
        else
	  mu = 0;
      }
      Scalar temp = 0;
      if (ReleasedEnergy >= 0)
      temp  = sqrt(2*ReleasedEnergy*mu*PROTON_CHARGE);
      Scalar phi = frand()*TWOPI; // isotropic in the frame of the excited species
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi); 

      if (s1) 
	{ 
	  Vector3 newvel = target_velocity + ((temp/m1)*uProducts)/s1->get_dxdt(); 
//	  Vector3 newvel = ((temp/m1)*uProducts)/s1->get_dxdt(); // as done in xpdp1, MAL 9/17/12
		// add particles if s1 is an electron or ion, or if s1 is a neutral and meets the energy threshold condition, MAL 11/25/09
//		if (s1->get_q() !=0 || (s1->get_q() == 0 && s1->get_energy_eV(s1->get_dxdt()*newvel) > s1->get_energyThrToAddAsPIC())) add_particles(s1, newvel); // lab frame
    add_product(s1, newvel);
//		printf("AnyDissociationInTwo: s1=%s, en1=%g eV\n", s1->get_name().c_str(), s1->get_energy_eV(s1->get_dxdt()*newvel)); // FOR DEBUG, MAL 11/23/09

	}
      if (s2)
	{ 
	  Vector3 newvel = target_velocity - ((temp/m2)*uProducts)/s2->get_dxdt();
//	  Vector3 newvel = ((-temp/m2)*uProducts)/s2->get_dxdt(); // as done in xpdp1, MAL 9/17/12
		// add particles if s2 is an electron or ion, or if s2 is a neutral and meets the energy threshold condition, MAL 11/25/09
//	  if (s2->get_q() !=0 || (s2->get_q() == 0 && s2->get_energy_eV(s2->get_dxdt()*newvel) > s2->get_energyThrToAddAsPIC())) add_particles(s2, newvel);
    add_product(s2, newvel);
//		printf("AnyDissociationInTwo: s2=%s, en2=%g eV\n", s2->get_name().c_str(), s2->get_energy_eV(s2->get_dxdt()*newvel)); // FOR DEBUG, MAL 11/23/09
	}
    }
  target_velocity = DELETE_PARTICLE;
}

void CHLORINE_MCC::Any_DissociationInThree(Vector3 & target_velocity,
				 	       Species* s1, Species* s2, 
					       Species* s3,
					       Scalar ReleasedEnergy)
{
  if(s1||s2||s3)
    {
      Scalar mu;
      Scalar m1, m2, m3;
      if (s1) m1 = s1->get_m();
      if (s2) m2 = s2->get_m();
      if (s3) m3 = s3->get_m();
      if (s1 && s2 && s3) 
      {
        mu = 1./(1/m1+ 1/m2 + 1/m3); // mu is the reduced mass.
      }
      else
      {
	if (s1 && s2)
	{
	   mu = 1./(1/m1+1/m2); 
	}
	// mindgame: m1 , m2 << m3 is assumed.
	// mindgame: m1 = m2 is additionally assumed.
	else if (s3 && s1) mu = 1./(1/m3+2/m1);
        else if (s3 && s2) mu = 1./(1/m3+2/m2);
	else if (s3) mu = 0;
	else if (s2) mu = 0.5*m2;
	else mu = 0.5*m1;
      }
      Scalar temp = 0;
      if (ReleasedEnergy >= 0)
      temp  = sqrt(2*ReleasedEnergy*mu*PROTON_CHARGE);
      Scalar phi = frand()*TWOPI; // isotropic
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi);   
      phi = frand()*TWOPI; 
   // The algorithm below is not general; in the CM system, the sum (head to tail) of the three product momentum vectors forms an equilateral triangle, MAL 12/2/09
	 // the products come off with 120 degree angles from each other. In general, any three product momentum vectors that form a triangle are possible; fix later?  
      if (s1)
	{ 
	  Vector3 newvel = target_velocity + ((temp/m1)*uProducts)/s1->get_dxdt();
		// add particles if s1 is an electron or ion, or if s1 is a neutral and meets the energy threshold condition, MAL 11/25/09
//		if (s1->get_q() !=0 || (s1->get_q() == 0 && s1->get_energy_eV(s1->get_dxdt()*newvel) > s1->get_energyThrToAddAsPIC())) add_particles(s1, newvel); // lab frame
    add_product(s1, newvel);
	//	printf("AnyDissociationInThree: s1=%s, en1=%g eV\n", s1->get_name().c_str(), s1->get_energy_eV(s1->get_dxdt()*newvel)); // FOR DEBUG, MAL 11/23/09
#ifdef DEBUGmcc  
	  fprintf(stderr, "\n New particle velocity: %g, uimagnitude: %g", newvel.magnitude(), ui.magnitude());
#endif
	}
      if (s2)
	{ 
	  Vector3 newvel = target_velocity + ((temp/m2)*newcoordiante(uProducts, TWOTHIRDS*PI, phi))/s2->get_dxdt(); // NOTE (2/3) (int) -> TWOTHIRDS (floating), MAL 12/2/09
		// add particles if s2 is an electron or ion, or if s2 is a neutral and meets the energy threshold condition, MAL 11/25/09
//	  if (s2->get_q() !=0 || (s2->get_q() == 0 && s2->get_energy_eV(s2->get_dxdt()*newvel) > s2->get_energyThrToAddAsPIC())) add_particles(s2, newvel);
    add_product(s2, newvel);
	//	printf("AnyDissociationInThree: s2=%s, en2=%g eV\n", s2->get_name().c_str(), s2->get_energy_eV(s2->get_dxdt()*newvel)); // FOR DEBUG, MAL 11/23/09
	  
	}  
      if (s3)
	{ 
	  Vector3 newvel = target_velocity + (temp/m3)*newcoordiante(uProducts, TWOTHIRDS*PI, phi + PI)/s3->get_dxdt(); // See above, MAL 12/2/09
		// add particles if s3 is an electron or ion, or if s3 is a neutral and meets the energy threshold condition, MAL 11/25/09
//	  if (s3->get_q() !=0 || (s3->get_q() == 0 && s3->get_energy_eV(s3->get_dxdt()*newvel) > s3->get_energyThrToAddAsPIC())) add_particles(s3, newvel);
    add_product(s3, newvel);
	//	printf("AnyDissociationInThree: s3=%s, en3=%g eV\n", s3->get_name().c_str(), s3->get_energy_eV(s3->get_dxdt()*newvel)); // FOR DEBUG, MAL 11/23/09
	  
	}
    }
  target_velocity = DELETE_PARTICLE;
}

void CHLORINE_MCC::Any_DissociationInTwo(Vector3 & Vr1, Vector3 & Vr2,  
Scalar Mr1, Scalar Mr2, Species* s1, Species* s2, Scalar Ereleased)
{

// Revised by MAL and JTG, 12/2/09
// Vr1,2 = projectile,target velocity, Mr1,2 = projectile,target mass,  
// Ereleased = additional potential energy released during the dissociation
	Vector3 Vcm = (Mr1*Vr1*iSpecies->get_dxdt()+Mr2*Vr2*nSpecies->get_dxdt())/(Mr1+Mr2);
	Scalar energy1 = (0.5*Mr1* (Vr1*iSpecies->get_dxdt()-Vcm).magnitude_squared() +  0.5*Mr2* (Vr2*nSpecies->get_dxdt()-Vcm).magnitude_squared())/PROTON_CHARGE; 
	Vector3 newvel1;
	Vector3 newvel2;
  if (s1||s2)
    { 
      Scalar mu;
      Scalar m1, m2;
      Scalar CMEnergy = energy1 + Ereleased; 
      if (s1) m1 = s1->get_m();
      if (s2) m2 = s2->get_m();
      if (s1 && s2) 
        mu = 1./(1/m1+1/m2);
      else
      {
	// mindgame: in case that only one of two is known, m1 << m2 is assumed.
	if (s1) 
          mu = m1;
        else
	  mu = 0;
      }
      Scalar temp = 0;
      if (CMEnergy >= 0)
      temp  = sqrt(2*CMEnergy*mu*PROTON_CHARGE);
      Scalar phi = frand()*TWOPI; // isotropic in the frame of the excited species
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi) ; 
//		printf("AnyDissociationInTwo: projectile=%s, en_p=%g eV\n", iSpecies->get_name().c_str(), iSpecies->get_energy_eV(iSpecies->get_dxdt()*Vr1)); // FOR DEBUG, MAL 11/23/09
//		printf("AnyDissociationInTwo: target=%s, en_t=%g eV\n", nSpecies->get_name().c_str(), nSpecies->get_energy_eV(nSpecies->get_dxdt()*Vr2)); // FOR DEBUG, MAL 11/23/09
      if (s1) 
	{ 
	  newvel1 = (Vcm + (temp/m1)*uProducts)/s1->get_dxdt(); 
		// add particles if s1 is an electron or ion, or if s1 is a neutral and meets the energy threshold condition, MAL 11/25/09
//		if (s1->get_q() !=0 || (s1->get_q() == 0 && s1->get_energy_eV(s1->get_dxdt()*newvel1) > s1->get_energyThrToAddAsPIC())) add_particles(s1, newvel1); // lab frame
    add_product(s1, newvel1);
//  	printf("AnyDissociationInTwo: s1=%s, en1=%g eV\n", s1->get_name().c_str(), s1->get_energy_eV(s1->get_dxdt()*newvel1)); // FOR DEBUG, MAL 11/23/09
	}
      if (s2)
	{ 
	  newvel2 = (Vcm - (temp/m2)*uProducts)/s2->get_dxdt();
		// add particles if s2 is an electron or ion, or if s2 is a neutral and meets the energy threshold condition, MAL 11/25/09
//	  if (s2->get_q() !=0 || (s2->get_q() == 0 && s2->get_energy_eV(s2->get_dxdt()*newvel2) > s2->get_energyThrToAddAsPIC())) add_particles(s2, newvel2);
    add_product(s2, newvel2);
//    printf("AnyDissociationInTwo: s2=%s, en2=%g eV\n", s2->get_name().c_str(), s2->get_energy_eV(s2->get_dxdt()*newvel2)); // FOR DEBUG, MAL 11/23/09
//		printf("AnyDissociationInTwo: KEproducts-Kereactants=%g eV\n", s1->get_energy_eV(s1->get_dxdt()*newvel1) +
//			s2->get_energy_eV(s2->get_dxdt()*newvel2) - iSpecies->get_energy_eV(iSpecies->get_dxdt()*Vr1) - nSpecies->get_energy_eV(nSpecies->get_dxdt()*Vr2)); // FOR DEBUG, MAL 11/23/09
	}
    }
  Vr2 = DELETE_PARTICLE; // delete target particle, MAL 11/26/09
}



void CHLORINE_MCC::Any_DissociationInThree(Vector3 & Vr1, Vector3 & Vr2, 
                                               Scalar Mr1, Scalar Mr2,
				 	       Species* s1, Species* s2, 
					       Species* s3,
					       Scalar Ereleased)
{
// for dissociations involving the collision of two heavy particle reactants, MAL 10/2/12
// Ereleased is the additional potential energy released during the dissociation
// Revised by MAL, 11/26/09
// Vr1,2 = projectile,target velocity, Mr1,2 = projectile,target mass
	Vector3 Vcm = (Mr1*Vr1*iSpecies->get_dxdt()+Mr2*Vr2*nSpecies->get_dxdt())/(Mr1+Mr2);
	Scalar energy1 = (0.5*Mr1* (Vr1*iSpecies->get_dxdt()-Vcm).magnitude_squared() +  0.5*Mr2* (Vr2*nSpecies->get_dxdt()-Vcm).magnitude_squared())/PROTON_CHARGE; 
  if(s1||s2||s3)
    {
      Scalar mu;
      Scalar m1, m2, m3;
      Scalar CMEnergy = energy1 + Ereleased; //energy in CM system plus released energy, MAL 10/2/12
			Vector3 newvel1;
			Vector3 newvel2;
			Vector3 newvel3;
      if (s1) m1 = s1->get_m();
      if (s2) m2 = s2->get_m();
      if (s3) m3 = s3->get_m();
      if (s1 && s2 && s3) 
      {
        mu = 1./(1/m1+ 1/m2 + 1/m3); // mu is the reduced mass.
      }
      else
      {
	if (s1 && s2)
	{
	   mu = 1./(1/m1+1/m2); 
	}
	// mindgame: m1 , m2 << m3 is assumed.
	// mindgame: m1 = m2 is additionally assumed.
	else if (s3 && s1) mu = 1./(1/m3+2/m1);
        else if (s3 && s2) mu = 1./(1/m3+2/m2);
	else if (s3) mu = 0;
	else if (s2) mu = 0.5*m2;
	else mu = 0.5*m1;
      }
      Scalar temp = 0;
      if (CMEnergy >= 0)
      temp  = sqrt(2*CMEnergy*mu*PROTON_CHARGE);
     
      Scalar phi = frand()*TWOPI; // isotropic
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi);   
      phi = frand()*TWOPI; 
//		printf("AnyDissociationInThree: projectile=%s, en_p=%g eV\n", iSpecies->get_name().c_str(), iSpecies->get_energy_eV(iSpecies->get_dxdt()*Vr1)); // FOR DEBUG, MAL 11/23/09
//		printf("AnyDissociationInThree: target=%s, en_t=%g eV\n", nSpecies->get_name().c_str(), nSpecies->get_energy_eV(nSpecies->get_dxdt()*Vr2)); // FOR DEBUG, MAL 11/23/09
   // The algorithm below is not general; in the CM system, the sum (head to tail) of the three product momentum vectors forms an equilateral triangle, MAL 12/2/09
	 // the products come off with 120 degree angles from each other. In general, any three product momentum vectors that form a triangle are possible; fix later?  
      if (s1)
	{ 
	  newvel1 = (Vcm + (temp/m1)*uProducts)/s1->get_dxdt();
//	  printf("AnyDissociationInThree: temp = %g, Vcmag = %g, newvel1 = %g, ReleasedEnergy = %g \n",temp,Vcm.magnitude(),newvel1.magnitude(),ReleasedEnergy); 
		// add particles if s1 is an electron or ion, or if s1 is a neutral and meets the energy threshold condition, MAL 11/25/09
//		if (s1->get_q() !=0 || (s1->get_q() == 0 && s1->get_energy_eV(s1->get_dxdt()*newvel1) > s1->get_energyThrToAddAsPIC())) add_particles(s1, newvel1); // lab frame
    add_product(s1, newvel1);
//		printf("AnyDissociationInThree: s1=%s, en1=%g eV\n", s1->get_name().c_str(), s1->get_energy_eV(s1->get_dxdt()*newvel1)); // FOR DEBUG, MAL 11/23/09

#ifdef DEBUGmcc  
	  fprintf(stderr, "\n New particle velocity: %g, uimagnitude: %g", newvel1.magnitude(), ui.magnitude());
#endif
	}
      if (s2)
	{ 
	  newvel2 = (Vcm + (temp/m2)*newcoordiante(uProducts, TWOTHIRDS*PI, phi))/s2->get_dxdt();  
		// add particles if s2 is an electron or ion, or if s2 is a neutral and meets the energy threshold condition, MAL 11/25/09
//	  if (s2->get_q() !=0 || (s2->get_q() == 0 && s2->get_energy_eV(s2->get_dxdt()*newvel2) > s2->get_energyThrToAddAsPIC())) add_particles(s2, newvel2);
    add_product(s2, newvel2);
//		printf("AnyDissociationInThree: s2=%s, en2=%g eV\n", s2->get_name().c_str(), s2->get_energy_eV(s2->get_dxdt()*newvel2)); // FOR DEBUG, MAL 11/23/09
	  
	}  
      if (s3)
	{ 
	  newvel3 = (Vcm + (temp/m3)*newcoordiante(uProducts, TWOTHIRDS*PI, phi + PI))/s3->get_dxdt();  
		// add particles if s3 is an electron or ion, or if s3 is a neutral and meets the energy threshold condition, MAL 11/25/09
//	  if (s3->get_q() !=0 || (s3->get_q() == 0 && s3->get_energy_eV(s3->get_dxdt()*newvel3) > s3->get_energyThrToAddAsPIC())) add_particles(s3, newvel3);
    add_product(s3, newvel3);
//		printf("AnyDissociationInThree: s3=%s, en3=%g eV\n", s3->get_name().c_str(), s3->get_energy_eV(s3->get_dxdt()*newvel3)); // FOR DEBUG, MAL 11/23/09
//		printf("AnyDissociationInThree: KEproducts-Kereactants=%g eV\n", s1->get_energy_eV(s1->get_dxdt()*newvel1) + s3->get_energy_eV(s3->get_dxdt()*newvel3) +
//			s2->get_energy_eV(s2->get_dxdt()*newvel2) - iSpecies->get_energy_eV(iSpecies->get_dxdt()*Vr1) - nSpecies->get_energy_eV(nSpecies->get_dxdt()*Vr2)); // FOR DEBUG, MAL 11/23/09
	  
	}
    }
  Vr2 = DELETE_PARTICLE; // delete target particle, MAL 11/26/09
}

// --- Detachment processes:
void CHLORINE_MCC::Any_eDetachment(const Xsection& cx, Species* s1) 
{
// Used for edetachment MAL 12/27/09
// We don't subtract the velocity of the heavy target ion: assuming the electron has a much higher velocity: energy, ue are valid. 
  Scalar thres = cx.get_threshold();
  ionization(thres);
  // Creation of ionized species
  if (s1)
    { 
   // add particles if s1 is an electron or ion, or if s1 is a neutral and meets the energy threshold condition, MAL 11/25/09
//   if (s1->get_q() !=0 || (s1->get_q() == 0 && s1->get_energy_eV(s1->get_dxdt()*ui) > s1->get_energyThrToAddAsPIC())) add_particles(s1, ui); // lab frame
      add_product(s1, ui);
    }
// Must add proper u_target = DELETE_PARTICLE to individual cross section calls, MAL 11/25/09
}

// --- Ionization processes:
void CHLORINE_MCC::Any_eIonization(const Xsection& cx, Species* s1) 
{
// Revised by MAL, 11/25/09
// Used for edetachment, eionizationo, and eionizationo2, MAL 11/25/09
// We don't subtract the velocity of the heavy target (ion or neutral): assuming the electron has a much higher velocity: energy, ue are valid. 
  Scalar thres = cx.get_threshold();
//	printf("\nCHLORINE_MCC::Any_eIonization: entered, threshold=%g\n",thres);
  ionization(thres);
  // Creation of ionized species
  if (s1)
    { 
//		printf("\nCHLORINE_MCC::Any_eIonization, adding ion s1=%p (%s)\n\n", (void*)s1, s1->get_name().c_str());

   // add particles if s1 is an electron or ion, or if s1 is a neutral and meets the energy threshold condition, MAL 11/25/09
//   if (s1->get_q() !=0 || (s1->get_q() == 0 && s1->get_energy_eV(s1->get_dxdt()*un) > s1->get_energyThrToAddAsPIC())) add_particles(s1, un); // lab frame
      add_product(s1, un);
    }
// Must add proper u_target = DELETE_PARTICLE to individual cross section calls, MAL 11/25/09
}

void CHLORINE_MCC::Any_eIonization2(const Xsection& cx, Species* s1) 
{
	// Used for dissociative ionization of O2, revised by MAL, 11/26/09
  // We don't subtract the velocity of the neutral: assumed the electron has a much higher velocity: energy, ue are valid. 
  Scalar thres = cx.get_threshold();
  ionization(thres);
// Add u_n = DELETE_PARTICLE to dissociative ionization cross section call, MAL 11/25/09
}
