#include "utils/ovector.hpp"
#include "main/parse.hpp"
#include "main/species.hpp"
#include "main/particlegroup.hpp"
#include "xsections/xsection.hpp"
#include "reactions/reaction.hpp"
#include "MCC/mcc.hpp"
#include "MCC/hydrocarbons_mcc.hpp"

Hydrocarbons_MCC::Hydrocarbons_MCC(oopicList<Species>* slist, ReactionList _reactionlist, Scalar _dt, Fields *tf) : MCC(slist, _reactionlist, _dt, tf)
{
  setdefaults();
}

void Hydrocarbons_MCC::setdefaults()
{
  sec_iminusH = sec_iminusH2 = NULL;
  sec_nminusH = sec_nminusH2 = sec_nminusH3 = NULL;
  sec_H = sec_H2 = sec_p = NULL;
  DIeEnergyloss = DIenergy = CXhenergy = 0;
  DE1energy = DE2energy = DE3energy = DE4energy = DE5energy = 0;
  DR1energy = DR2energy = DR3energy = DR4energy = DR5energy = 0;
}

void Hydrocarbons_MCC::init(Species *_reactant1, Species *_reactant2)
{
  init_mcc(_reactant1, _reactant2);

  // mindgame: compare with hydrocarbon_gas.hpp
  for (int i=reactionlist.num()-1; i>=0; i--)
  {
    switch (reactionlist.data(i)->get_reactiontype())
    {
      case E_DISS_IONIZATION:
        sec_e = reactionlist.data(i)->get_products(0);
        sec_H = reactionlist.data(i)->get_products(1);
        sec_iminusH = reactionlist.data(i)->get_products(2);
	DIeEnergyloss = reactionlist.data(i)->get_characteristic_values(0);
	DIenergy = reactionlist.data(i)->get_characteristic_values(1);
	break;
      case E_DISS_EXCITATION1:
        sec_H = reactionlist.data(i)->get_products(0);
        sec_nminusH = reactionlist.data(i)->get_products(1);
	DE1energy = reactionlist.data(i)->get_characteristic_values(0);
	break;
      case E_DISS_EXCITATION2:
        sec_H2 = reactionlist.data(i)->get_products(0);
        sec_nminusH2 = reactionlist.data(i)->get_products(1);
	DE2energy = reactionlist.data(i)->get_characteristic_values(0);
	break;
      case P_eEXCHANGE:
        sec_H = reactionlist.data(i)->get_products(0);
        sec_i = reactionlist.data(i)->get_products(1);
	break;
      case P_hEXCHANGE:
        sec_H2 = reactionlist.data(i)->get_products(0);
        sec_iminusH = reactionlist.data(i)->get_products(1);
	CXhenergy = reactionlist.data(i)->get_characteristic_values(0);
	break;
      case E_DISS_EXCITATION3:
        sec_H = reactionlist.data(i)->get_products(0);
        sec_iminusH = reactionlist.data(i)->get_products(1);
	DE3energy = reactionlist.data(i)->get_characteristic_values(0);
	break;
      case E_DISS_EXCITATION4:
        sec_p = reactionlist.data(i)->get_products(0);
        sec_nminusH = reactionlist.data(i)->get_products(1);
	DE4energy = reactionlist.data(i)->get_characteristic_values(0);
	break;
      case E_DISS_EXCITATION5:
        sec_H2 = reactionlist.data(i)->get_products(0);
        sec_iminusH2 = reactionlist.data(i)->get_products(1);
	DE5energy = reactionlist.data(i)->get_characteristic_values(0);
	break;
      case E_DISS_RECOMBINATION1:
        sec_H = reactionlist.data(i)->get_products(0);
        sec_nminusH = reactionlist.data(i)->get_products(1);
	DR1energy = reactionlist.data(i)->get_characteristic_values(0);
	break;
      case E_DISS_RECOMBINATION2:
        sec_H2 = reactionlist.data(i)->get_products(0);
        sec_nminusH2 = reactionlist.data(i)->get_products(1);
	DR2energy = reactionlist.data(i)->get_characteristic_values(0);
	break;
      case E_DISS_RECOMBINATION3:
        sec_H = reactionlist.data(i)->get_products(0);
        sec_H2 = reactionlist.data(i)->get_products(1);
        sec_nminusH3 = reactionlist.data(i)->get_products(2);
	DR3energy = reactionlist.data(i)->get_characteristic_values(0);
	break;
      default:
	break;	
    }
  }
}

void Hydrocarbons_MCC::dynamic(Reaction * _reaction)
{
Reaction * reaction = _reaction;
const Xsection  & cx  = *(reaction->get_xsection());
ReactionType reactiontype = reaction->get_reactiontype();
//  printf("\nmcc::ReactionType: %s\n", ReactionTypeName(reactiontype).c_str());
#ifdef DEBUGmcc
  totalnumberofcollisions ++;
  printf("\nReactionType: %s\n", ReactionTypeName(reactiontype).c_str());
#endif  

  switch(reactiontype)
    {
      // --- Scattering processes:
    case E_ELASTIC:                 
      eElastic(cx);
      break;
    case P_ELASTIC: 
      pElastic(cx);
      break;
    case E_EXCITATION:
      eExcitation(cx);
      break;
    case E_DISS_EXCITATION1:
      eDissExcitation1(cx);
      break;
    case E_DISS_EXCITATION2:
      eDissExcitation2(cx);
      break;
    case E_DISS_EXCITATION3:
      eDissExcitation3(cx);
      break;
    case E_DISS_EXCITATION4:
      eDissExcitation4(cx);
      break;
    case E_DISS_EXCITATION5:
      eDissExcitation5(cx);
      break;

      // --- Ionizing processes:
    case E_IONIZATION:
      eIonization(cx);
      break;
    case E_DISS_IONIZATION:
      eDissIonization(cx);
      break;
   
      // --- Charge exchange processes:    
    case P_eEXCHANGE:
      pElectronExchange(cx);
      break;   
    case P_hEXCHANGE:
      pHydrogenExchange(cx);
      break;
    
      // --- Recombinations:
    case E_DISS_RECOMBINATION1:
      eDissRecombination1(cx);
      break;
    case E_DISS_RECOMBINATION2:
      eDissRecombination2(cx);
      break;
    case E_DISS_RECOMBINATION3:
      eDissRecombination3(cx);
      break;
    default: 
      fprintf(stderr,"Unrecognized collision type!\n");
    }
}

/////////////////////////////
// Reaction dynamics       // 
/////////////////////////////
// Simple models are used: 
// non-relativistic processes, isotropic behaviors, ion/neutral velocity not modified by a single electron.

void Hydrocarbons_MCC::eElastic (const Xsection &) 
{
}

void Hydrocarbons_MCC::pElastic (const Xsection &) 
{
  
}

void Hydrocarbons_MCC::eExcitation (const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
}

void Hydrocarbons_MCC::Any_eExcitation (const Xsection& cx, Scalar targetmass) 
{
  //
  energy -= cx.get_threshold();
  v = eSpecies->get_speed2(energy); 
  v /= eSpecies->get_dxdt();
  
  // Isotropic scattering with the reduced energy.
  Scalar coschi = 1 - 2*frand();
  Scalar chi = acos(coschi);
  Scalar phi = TWOPI*frand();
  Scalar me = eSpecies -> get_m();
  v *= sqrt(1. - 2. * me * targetmass / (me + targetmass) / (me + targetmass) * (1. - coschi)); 
  ue = v*  newcoordiante(ue , chi, phi);  
}
void Hydrocarbons_MCC::Any_DissociationInTwo(Vector3 & target_velocity,
					     Species* s1, Species* s2,
					     Scalar ReleasedEnergy) 
{
  if (s1||s2)
    { 
      Scalar mu;
      Scalar m1, m2;
      if (s1) m1 = s1->get_m();
      if (s2) m2 = s2->get_m();
      if (s1 && s2) 
        mu = 1./(1/m1+1/m2);
      else
      {
	// mindgame: in case that only one of two is known, m1 << m2 is assumed.
	if (s1) 
          mu = m1;
        else
	  mu = 0;
      }
      Scalar temp  = sqrt(2*ReleasedEnergy*mu*PROTON_CHARGE); 
      Scalar phi = frand()*TWOPI; // isotropic in the frame of the excited species
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi) ; 
      
      if (s1) 
	{ 
	  Vector3 newvel = target_velocity + ((temp/m1)*uProducts)/s1->get_dxdt(); 
	  add_particles(s1, newvel); // lab frame	  
	}
      if (s2)
	{ 
	  Vector3 newvel = target_velocity - ((temp/m2)*uProducts)/s2->get_dxdt();
	  add_particles(s2, newvel);	    
	}
    }
  target_velocity = DELETE_PARTICLE;
}

void Hydrocarbons_MCC::Any_DissociationInThree(Vector3 & target_velocity,
				 	       Species* s1, Species* s2, 
					       Species* s3,
					       Scalar ReleasedEnergy)
{
  if(s1||s2||s3)
    {
      Scalar mu;
      Scalar m1, m2, m3;
      if (s1) m1 = s1->get_m();
      if (s2) m2 = s2->get_m();
      if (s3) m3 = s3->get_m();
      if (s1 && s2 && s3) 
      {
        mu = 1./(1/m1+ 1/m2 + 1/m3); // mu is the reduced mass.
      }
      else
      {
	if (s1 && s2)
	{
	   mu = 1./(1/m1+1/m2); 
	}
	// mindgame: m1 , m2 << m3 is assumed.
	// mindgame: m1 = m2 is additionally assumed.
	else if (s3 && s1) mu = 1./(1/m3+2/m1);
        else if (s3 && s2) mu = 1./(1/m3+2/m2);
	else if (s3) mu = 0;
	else if (s2) mu = 0.5*m2;
	else mu = 0.5*m1;
      }
      Scalar temp  = sqrt(2*(ReleasedEnergy)*mu*PROTON_CHARGE);  
      Scalar phi = frand()*TWOPI; // isotropic
      Scalar coschi = 1 - 2*frand();
      Scalar sinchi =  sqrt(1 - pow(coschi,2));  
      Vector3 uProducts = Vector3 (sinchi*cos(phi), sinchi*sin(phi), coschi);   
      phi = frand()*TWOPI; 
      
      if (s1)
	{ 
	  Vector3 newvel = target_velocity + ((temp/m1)*uProducts)/s1->get_dxdt();
	  add_particles(s1, newvel);	  
#ifdef DEBUGmcc  
	  fprintf(stderr, "\n New particle velocity: %g, uimagnitude: %g", newvel.magnitude(), ui.magnitude());
#endif
	}
      if (s2)
	{ 
	  Vector3 newvel = target_velocity + ((temp/m2)*newcoordiante(uProducts, (2/3)*PI, phi))
	    / s2->get_dxdt();  
	  add_particles(s2, newvel);	  
	}  
      if (s3)
	{ 
	  Vector3 newvel = target_velocity + (temp/m3)*newcoordiante(uProducts, (2/3)*PI, phi + PI)
	    /s3->get_dxdt();  
	  add_particles(s3, newvel);	  
	}
    }
  target_velocity = DELETE_PARTICLE;
}


void Hydrocarbons_MCC::eDissExcitation1(const Xsection& cx)  
{
  // Calculations valid in the  neutral frame (ue >> un => negligible modifications).   
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_H, sec_nminusH, DE1energy);  
}
void Hydrocarbons_MCC::eDissExcitation2(const Xsection& cx)
{
  Any_eExcitation(cx, nSpecies->get_m());
  Any_DissociationInTwo(un, sec_H2, sec_nminusH2, DE2energy);
}
void Hydrocarbons_MCC::eDissExcitation3(const Xsection& cx)
{
  Any_eExcitation(cx, iSpecies->get_m());
  Any_DissociationInTwo(ui, sec_H, sec_iminusH, DE3energy);
}
void Hydrocarbons_MCC::eDissExcitation4(const Xsection& cx)
{
  Any_eExcitation(cx, iSpecies->get_m());
  Any_DissociationInTwo(ui, sec_p, sec_nminusH, DE4energy);
}
void Hydrocarbons_MCC::eDissExcitation5(const Xsection& cx)
{
  Any_eExcitation(cx, iSpecies->get_m());
  Any_DissociationInTwo(ui, sec_H2, sec_iminusH2, DE5energy);
}

void Hydrocarbons_MCC::eDissIonization(const Xsection& cx)
{
  // Calculations valid in the  neutral frame (ue >> un => negligible modifications).   
  
  // Approximate solution to allow collisions with energies <= average_energyloss 
  Scalar average_energyloss = DIeEnergyloss;
  if (average_energyloss >= energy) 
    { 
      ionization (energy);
      Any_DissociationInTwo(un, sec_H, sec_iminusH, DIenergy- (average_energyloss-energy));
      }
  else
    {
      ionization (average_energyloss);
      Any_DissociationInTwo(un, sec_H, sec_iminusH, DIenergy);
    }
}

void Hydrocarbons_MCC::eDissRecombination1(const Xsection& cx) 
  // Calculations valid in center of center of mass frame ~ ion frame (ue >> ui => negligible modifications)
{
  Any_DissociationInTwo(ui, sec_H, sec_nminusH, DR1energy);
  ue = DELETE_PARTICLE;
}

void Hydrocarbons_MCC::eDissRecombination2(const Xsection& cx) 
{
  Any_DissociationInThree(ui, sec_H, sec_H, sec_nminusH2, DR2energy+energy);  
  ue = DELETE_PARTICLE;
}

void Hydrocarbons_MCC::eDissRecombination3(const Xsection& cx)
{
  Any_DissociationInThree(ui, sec_H, sec_H2, sec_nminusH3, DR3energy+energy);  
  ue = DELETE_PARTICLE;
}

void Hydrocarbons_MCC::pElectronExchange(const Xsection & cx)
{ // Pb: exothermicities not taken into account...error?
  if (sec_H)
    { 
      add_particles(sec_H, ui);
    }
  if (sec_i)
    { 
      add_particles(sec_i, un);
    } 
  ui = DELETE_PARTICLE;
  un = DELETE_PARTICLE;
}

void Hydrocarbons_MCC::pHydrogenExchange(const Xsection& cx) 
  // Data could be found: isotropy here is very questionable.
{
  // Move to the center of mass frame, cmf. 
  Scalar mp = iSpecies->get_m(); 
  Scalar mn = nSpecies->get_m();
  Scalar mass_sum = mn+mp;
  un = (mn*un+mp*ui)/mass_sum; // cmf velocity
  energy = energy*mn/mass_sum; 
  Any_DissociationInTwo(un, sec_H2, sec_iminusH, CXhenergy+energy);  
  ui = DELETE_PARTICLE;
}
