// Secondary Emitter Class
//
// Original by HyunChul Kim, 2006
//
#include "main/pd1.hpp"
#include "main/parse.hpp"
#include "utils/ovector.hpp"
#include "main/species.hpp"
#include "fields/fields.hpp"
#include "main/particlegroup.hpp"
#include "main/grid.hpp"
#include "main/boundary.hpp"
#include "main/boundary_inline.hpp"
#include "spatial_region/sptlrgn.hpp"
#include "emmiter/emitter.hpp"
#include "emmiter/secondaryemitter.hpp"
#include "main/particlegrouparray.hpp"

#include "fields/fields_inline.hpp"
#include "main/grid_inline.hpp"

SecondaryEmitter::SecondaryEmitter(Fields *t, Boundary *theb,
				   Species *sp, int _method, int _nmax,
				   Scalar _np2c0, int _vw)
  : Emitter(t, theb, _method)
{
  spemit = sp;
  nmax = _nmax;
  variable_weight = _vw;
  np2c0 = _np2c0;
  setdefaults();
  init();
}

SecondaryEmitter::~SecondaryEmitter()
{
}

void SecondaryEmitter::setdefaults()
{
  classname = "SecondaryEmitter";
  qm=0;
  direction=NONE;
  iter=0;
  get_vandx = NULL;
}

void SecondaryEmitter::init()
{
  qm = spemit->get_qm();
  emit_pg = new ParticleGroupArray(nmax, spemit, thefield, np2c0,
				   variable_weight);

  choose_method_function(method);
}

