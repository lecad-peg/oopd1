#include "main/pd1.hpp"
#include "main/parse.hpp"
#include "utils/ovector.hpp"
#include "main/species.hpp"
#include "fields/fields.hpp"
#include "main/particlegroup.hpp"
#include "main/grid.hpp"
#include "main/boundary.hpp"
#include "main/boundary_inline.hpp"
#include "spatial_region/sptlrgn.hpp"
#include "emmiter/emitter.hpp"
#include "main/drive.hpp"
#include "distributions/dist_xv.hpp"
#include "distributions/dist_v.hpp"
#include "emmiter/beamemitter.hpp"
#include "fields/fields_inline.hpp"
#include "main/grid_inline.hpp"

BeamEmitter::BeamEmitter(FILE *fp, 
			 oopicList<Parameter<Scalar> > float_variables,
			 oopicList<Parameter<int> > int_variables, Fields *t,
			 Boundary *theb)
  : Emitter(t, theb)
{
  init_parse(fp, float_variables, int_variables);
}  

BeamEmitter::~BeamEmitter()
{
}

void BeamEmitter::setdefaults()
{
  classname = "BeamEmitter";
  
  setdefaults_emit();
}

void BeamEmitter::setparamgroup()
{
  setparamgroup_emit();
  pg.add_function("I", "[A] emitted current as a function of time", 
		  &I, 1, "t");
  pg.add_function("F", "[s^-1] number of injected particles per unit time", 
		  &F, 1, "t");
}

void BeamEmitter::check()
{
  if (I.is_fparsed() && F.is_fparsed()) 
    {
      error ("F or I cannot be set at the same time. Choose one of them.");  
    }
  check_emit();
}

void BeamEmitter::maintain()
{
  maintain_emit();
}

void BeamEmitter::init()
{
  init_emit();
}

int BeamEmitter::get_nparticles(Scalar tstart, Scalar tend)
{
  static Scalar saved = 0;
  Scalar sval;
  int retval;
  Scalar flux;
  time = tstart;

  // calculate value of J (current density)

   if (I.is_fparsed()) // Flux has been defined from current
    {
      flux = I.eval(tstart)/theg->getA(theg->gridXtoX(j0));
      if (q!=0.0) flux /= q; 
      else if (flux != 0.0) error("non zero current, with particle of charge 0!"); 
    }
  else
    {
      flux = F.eval(tstart)/theg->getA(theg->gridXtoX(j0));
    }
  sval = ((flux/np2c0)*(tend - tstart));
  if(sval < 0) { return 0; }
  sval += saved;
  retval = int(sval);
  saved = sval - retval;
  return retval;
}
