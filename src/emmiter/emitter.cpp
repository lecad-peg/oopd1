#include "main/pd1.hpp"
#include "main/parse.hpp"
#include "utils/ovector.hpp"
#include "main/species.hpp"
#include "fields/fields.hpp"
#include "main/grid.hpp"
#include "main/particlegroup.hpp"
#include "main/particlegrouparray.hpp"
#include "main/boundary.hpp"
#include "main/boundary_inline.hpp"
#include "spatial_region/sptlrgn.hpp"
#include "emmiter/emitter.hpp"
#include "main/drive.hpp"
#include "distributions/dist_xv.hpp"
#include "distributions/dist_v.hpp"

#include "main/grid_inline.hpp"
#include "fields/fields_inline.hpp"

Emitter::Emitter(Fields *t, Boundary *theb, int _method)
{
  method = _method;
  distri = 0;
  thefield = t;
  theboundary = theb;
  specieslist = thefield->get_specieslist();
  theg = thefield->get_theGrid();
  emit_pg = 0;
}

Emitter::~Emitter()
{ 
  if (distri) delete distri;
  if (emit_pg) delete emit_pg;
}

void Emitter::setdefaults_emit()
{ 
  qm=0;
  iter=0;
  np2c0 = -1.;
  var_energy = false;
  energy_x = energy_y = energy_z = 0.;
  v0_x = v0_y = v0_z = 0.;
  vt_x = vt_y = vt_z = 0.;
  T = 0.;
  method = 1;
  direction = POSITIVE;
  speciesname = "";
  nmax = NMAX;
  get_vandx = NULL;
}

void Emitter::setparamgroup_emit()
{
  pg.add_fparameter("np2c", &np2c0);
  pg.add_fparameter("energy_x", &energy_x);
  pg.add_fparameter("energy_y", &energy_y);
  pg.add_fparameter("energy_z", &energy_z);
  pg.add_fparameter("v0_x", &v0_x);
  pg.add_fparameter("v0_y", &v0_y);
  pg.add_fparameter("v0_z", &v0_z);
  pg.add_fparameter("vt_x", &vt_x);
  pg.add_fparameter("vt_y", &vt_y);
  pg.add_fparameter("vt_z", &vt_z);
  pg.add_fparameter("T",&T);

  pg.add_iparameter("methods",&method);
  pg.add_string("species", &speciesname);
  pg.add_flag("inject_to_right", &direction, POSITIVE);
  pg.add_flag("inject_to_left", &direction, NEGATIVE);
  pg.add_bflag("var_energy", &var_energy, true);
  pg.add_function("vx_equation", "[m/s]",
		  &vx_equation, 1, "t");
  pg.add_iparameter("nmax", &nmax);
}

void Emitter::check_emit()
{
  if (nmax <= 0) { error("nmax <= 0"); }
  if(spemit == NULL) { error("species not found"); }
  // mindgame: in case that np2c0 is not given, use np2c0 of spemit.
  if (np2c0 <= 0.)
  {
    np2c0 = spemit->get_np2c();
  }
  if (np2c0 <= 0.)
  {
    error("np2c must be specified in either Emitter or Species!");
  }
  if (T && vt.magnitude())
  {
    error("T and VT can not be set at the same time. Choose one of them only!!");
  }
} 

void Emitter::init_emit() 
{ 
  if (T)
  {
    vt_x=vt_y=vt_z=spemit->vt(T);
  }
  if (energy_x)
  {
    v0_x = spemit->get_speed2(energy_x);
  }
  if (energy_y)
  {
    v0_y = spemit->get_speed2(energy_y);
  }
  if (energy_z)
  {
    v0_z = spemit->get_speed2(energy_z);
  }
  Vector3 vt(Vector3(vt_x,vt_y,vt_z));
  Vector3 v0(Vector3(v0_x,v0_y,v0_z));
  distri=new VDistribution(spemit,vt,v0,true);

  q = spemit->get_q();
  qm = spemit->get_qm();

  emit_pg = new ParticleGroupArray(nmax, spemit, thefield, np2c0,
				   spemit->is_variableWeight());

  choose_method_function(method);
}

// mindgame: choose appropriate get_vandx() depending on method.
void Emitter::choose_method_function(int _method)
{
  switch (_method)
  {
    case 0:
      get_vandx = &Emitter::get_vandx0;
      break;
    case 1:
      get_vandx = &Emitter::get_vandx1;
      break;
    case 2:
      get_vandx = &Emitter::get_vandx2;
      break;
    case 3:
      get_vandx = &Emitter::get_vandx3;
      break;
    case 4:
      get_vandx = &Emitter::get_vandx4;
      break;
    case 5:
      get_vandx = &Emitter::get_vandx5;
      break;
  }
}

void Emitter::maintain_emit()
{
  char err_string[STR_SIZE];
  oopicListIter<Species> thei(*specieslist);

  if(speciesname != "")
    {
      for(thei.restart(); !thei.Done(); thei++)
        {
          if(thei()->get_name() == speciesname)
            {
              spemit = thei();
              break;
            }
        }
      if(thei.Done())
        {
          sprintf(err_string,
                  "species '%s' not declared before Emitter in input file",
                  speciesname());
          error(err_string);
        }

      speciesname = "";
    }
}

void Emitter::emit(Scalar tstart, Scalar tend)
{
  int nn;
  iter++;
  
  nn = get_nparticles(tstart, tend);
   
  if(nn)
  {
    Vector3 *Vnf;
    Scalar *ftmp; //store the f[i]
    
    Vnf = new Vector3[nn];
    ftmp = new Scalar[nn];
    get_times(nn,ftmp,tstart,tend); //get f
    get_velocities(nn, Vnf, ftmp); //get Vn-f
    emit_particles(nn, j0, Vnf, ftmp, tend-tstart,(Direction)direction, np2c0);
    delete [] ftmp;
    delete [] Vnf;   
  }
}

// mindgame: the arguments of following two functions are assumed
//          that their input velocity is in units of MKS
//          while their position is in units of grid.

// mindgame: for the emission of a single particle
void Emitter::emit(Scalar & xp, Vector3 & vtmp, Scalar timefrac, Scalar ddtt,
	           Direction dir, Scalar weight)
{
  emit_particles(1, xp, &vtmp, &timefrac, ddtt, dir, weight);
}

void Emitter::emit_particles(int nn, Scalar xstart,
			     Vector3 *vstart,
			     Scalar *ftmp, Scalar ddtt,
			     Direction dir, Scalar weight)
{
  int i;
  Scalar *xinit, *dX;
  Vector3 *vfinal;

  dX = new Scalar[nn];
  xinit = new Scalar[nn];
  vfinal = new Vector3[nn];

  for(i=0; i < nn; i++)
  {
    (this->*get_vandx)(vfinal[i],dX[i],
	     ftmp[i],vstart[i],xstart,ddtt, dir);
  }
	
//  theg->XtogridX(nn,dX); // this function is not correct here!!  MAL 9/17/09
	theg->dXtogriddX(nn,dX); // added this function to grid.cpp, MAL 9/17/09
//	printf("Emitter.cpp:237, nsteps=%li, xstart=%g, dX[0]=%g, x+dX[0]=%g\n",theboundary->get_thefield()->get_nsteps(), xstart,dX[0],xstart+dX[0]);	
//	if(dir == POSITIVE && dX[0] < 0 || dir == NEGATIVE && dX[0] > 0) printf("\tXXXXXX ERROR XXXXXXX\n");	

  //give the offset to the xstart, in order to avoid seg fault 
	//seg fault is due to bug in getvandx5 mover (see below); guarded against below to squash bug forever, MAL 12/26/09
	//getvandx5 occasionally generates dX of wrong sign -> xstart+dX does not lie within the grid
	//to see bug, run Oxygen_Test3D.inp  choosing meth = 5 in input file, and uncomment above two print statements
  if (sizeof(Scalar) == sizeof(double))
	{ 
      if (dir==POSITIVE)
	{
//	  xstart += theg->getng() * 1e-10 + 1e-20; // change 1e-15 and 1e-50 to 1e-10 and 1e-20, MAL 9/17/09
	  xstart += theg->getng() * 1e-15 + 1e-15; // change 1e-15 and 1e-50 to 1e-10 and 1e-20, MAL 9/17/09
	}
      else
	{
//	  xstart = xstart - (theg->getng() * 1e-10 + 1e-20); // change 1e-15 and 1e-50 to 1e-10 and 1e-20, MAL 9/17/09
	  xstart = xstart - (theg->getng() * 1e-15 + 1e-15); // change 1e-15 and 1e-50 to 1e-10 and 1e-20, MAL 9/17/09
	} 
    }  
  else
    {
      if (dir==POSITIVE)
	{
	  xstart += theg->getng() * 1e-6 + 1e-20; // change = to +=, MAL 9/16/09
	}
      else
	{
	  xstart = xstart  - (theg->getng() * 1e-6 + 1e-20);
	}
    }

		for(i=0;i<nn;i++)
		{
			xinit[i]=xstart;
			Scalar xend = xstart + dX[i]; // squash seg fault due to any erroneous dX[i] generated by particle mover, MAL 12/26/09
			// New code follows to squash seg fault permanently, MAL 3/19/10
			if(xend <= 0.0)
			{
				printf("\nnsteps = %li, Emitter::emit_particles: xend<0 is not within grid range; set xend=1e-15; xstart=%f, dx=%f, mover method meth = %i\n",
						theboundary->get_thefield()->get_nsteps(), xstart, dX[i], method);
				xend=1e-15;
				dX[i] = 0.0;		// added by JK & DQW; 2019-01-11
				printf("Uncomment printf statements in emit_particles to debug mover used to generate out-of-range xend (M.A. Lieberman 3/19/10), now Dx[i]=0 (JK & DQW, 2019-01-11)\n");
			  
			}
			if(xend >= theg->getng())
			{
				printf("\nnsteps = %li, Emitter::emit_particles: xend<0 is not within grid range; set xend=1e-15; xstart=%f, dx=%f, mover method meth = %i\n",
						theboundary->get_thefield()->get_nsteps(), xstart, dX[i], method);
				xend=theg->getng() - 1e-15;
				dX[i] = 0.0;		// added by JK & DQW; 2019-01-11
				printf("Uncomment printf statements in emit_particles to debug mover used to generate out-of-range xend (M.A. Lieberman 3/19/10), now Dx[i]=0 (JK & DQW, 2019-01-11)\n");

			}
		}

  add_particles(nn, weight, xinit, dX, vfinal, true, dir);

  delete [] dX;
  delete [] vfinal;
  delete [] xinit;
}

// mindgame: the following functions assume
//          that all parameters are in units of MKS
//          except that xnf is in units of grid.

void Emitter::get_vandx0(Vector3 &vnhalf, Scalar &xn, Scalar f,
			 Vector3 vnf, Scalar xnf, Scalar dt, Direction dir)
{
  vnhalf=vnf;
  xn=0.;
}

void Emitter::get_vandx1(Vector3 &vnhalf, Scalar &xn, Scalar f, 
			 Vector3 vnf, Scalar xnf, Scalar dt, Direction dir)
{
  Vector3 E;
  theboundary->get_E(dir, E);
  vnhalf=vnf+qm*E*(f-0.5)*dt;
  xn=vnf.e1()/spemit->get_gamma(vnf)*f*dt;
}

void Emitter::get_vandx2(Vector3 &vnhalf, Scalar &xn, Scalar f,
                         Vector3 vnf, Scalar xnf, Scalar dt, Direction dir)
{
  Vector3 en, Vn2f;
  Vector3 bn,bn1;
  theboundary->get_EB(dir, en, bn1);
  en=en*qm;
  bn=bn1.unit();
  Scalar On=qm*bn1.magnitude();
  Boris_push_integrator(vnf,vnhalf,en,bn,On,f-0.5,f-0.5, dt);
  
  Boris_push_integrator(vnf,Vn2f,en,bn,On,0.5*f,0.5*f, dt);
  
  xn=f*dt*Vn2f.e1()/spemit->get_gamma(Vn2f);
}


void Emitter::get_vandx3(Vector3 &vnhalf, Scalar &xn, Scalar f, 
			 Vector3 vnf, Scalar xnf, Scalar dt, Direction dir)
{
  Vector3 en,V1,V2,Vn1f,Vn2f;
  Vector3 bn,bn1;
  theboundary->get_EB(dir, en, bn1);
  en=en*qm;
  bn=bn1.unit();
  Scalar On=qm*bn1.magnitude();
  
  Boris_push_integrator(vnf,V1,en,bn,On,f-0.5, f-0.5, dt);
 
  Boris_push_integrator(vnf,Vn1f,en,bn,On,1,1, dt);
  
  Boris_push_integrator(vnf,V2,en,bn,On,-1,-1, dt);
  
  Scalar gammaV2= spemit->get_gamma(V2);
  Scalar gammaVn1f = spemit->get_gamma(Vn1f);
  Scalar gammavnf = spemit->get_gamma(vnf);
  vnhalf=V1+ 1/24.*(Vn1f/gammaVn1f-vnf/gammavnf)%bn*On*dt-1/12.*(gammaVn1f-gammavnf)*(Vn1f/gammaVn1f-vnf/gammavnf)-1/24.*(gammaVn1f-2*gammavnf+gammaV2)*vnf/gammavnf;
  
   Boris_push_integrator(vnf,Vn2f,en,bn,On,0.5*f,0.5*f, dt);
   Vector3 Xn=f*dt*(Vn2f/spemit->get_gamma(Vn2f)+(vnhalf-V1)/gammavnf);
   xn=Xn.e1();
}

void Emitter::get_vandx4(Vector3 &vnhalf, Scalar &xn, Scalar f,
			 Vector3 vnf, Scalar xnf, Scalar dt, Direction dir)
{
  Vector3 en,V1,V2,Vn1f,Vn2f;
  Vector3 bn,bn1;
  theboundary->get_EB(dir, en, bn1);
  en=en*qm;
  bn=bn1.unit();
  Scalar On=qm*bn1.magnitude();
  Vector3 graden;
  Vector3 gradbn1,gradbn; 
  theboundary->get_EBgradient(dir, graden, gradbn1);
  graden=qm*graden;
  Scalar gradOn;
  Vector3 b;
  theboundary->get_B(dir, b);
  if (gradbn1.isNonZero())
    {  gradbn=gradbn1/b.magnitude();
    gradOn=qm*b.unit()*gradbn1;
    }
  else
    {
      gradbn=0.*gradbn1;
      gradOn=0;
    }
  
  Scalar V_nf = vnf.e1()/spemit->get_gamma(vnf);  
 
  Scalar Ov=On+(f-0.5)/2.*V_nf*gradOn*dt;
  
  Vector3 bv=bn+(f-0.5)/2.*V_nf*gradbn*dt;

  Vector3 ev=en+(f-0.5)/2.*V_nf*graden*dt;  
  
  Scalar  Ot=On+0.5*V_nf*gradOn*dt;
  
  Vector3 bt=bn+0.5*V_nf*gradbn*dt;

  Vector3 et=en+0.5*V_nf*graden*dt;

  Scalar Ot1=On-0.5*V_nf*gradOn*dt;

  Vector3 bt1=bn-0.5*V_nf*gradbn*dt;
 
  Vector3 et1=en-0.5*V_nf*graden*dt;
  
  Scalar Ox=On+f/4.*V_nf*gradOn*dt;

  Vector3 bx=bn+f/4.*V_nf*gradbn*dt;

  Vector3 ex=en+f/4.*V_nf*graden*dt;//get field revised parameters
  
  Boris_push_integrator(vnf,V1,ev,bv,Ov,f-0.5, f-0.5, dt);

  Boris_push_integrator(vnf,Vn1f,et,bt,Ot,1,1, dt);
  
  Boris_push_integrator(vnf,V2,et1,bt1,Ot1,-1,-1, dt);
  
  Boris_push_integrator(vnf,Vn2f,ex,bx,Ox,0.5*f,0.5*f, dt);

  Scalar gammaV2= spemit->get_gamma(V2);
  Scalar gammaVn1f = spemit->get_gamma(Vn1f);
  Scalar gammavnf = spemit->get_gamma(vnf);
  
  Vector3 vn_add=1/24.*(Vn1f/gammaVn1f-vnf/gammavnf)%bn*On*dt-1/12.*(gammaVn1f-gammavnf)*(Vn1f/gammaVn1f-vnf/gammavnf)-1/24.*(gammaVn1f-2*gammavnf+gammaV2)*vnf/gammavnf+1/24.*V_nf*(graden+gradOn*vnf%(bn)/gammavnf+On*vnf%(gradbn)/gammavnf)*dt*dt;
 
  vnhalf=V1-vn_add;
   
  Vector3 Xn=f*dt*(Vn2f/spemit->get_gamma(Vn2f));//+vn_add/gammavnf);

  xn=Xn.e1();//get Xn
}

// WARNING, DO NOT USE, MAL 12/26/09
// WARNING!  THIS MOVER OCCASIONALLY GENERATES A dX[i] (AN xn) OF THE WRONG SIGN, SUCH THAT xstart+dX[i] DOES NOT LIE WITHIN THE GRID 0 < x < ng
void Emitter::get_vandx5(Vector3 &vnhalf,
			Scalar &xn, Scalar f, Vector3 vnf, Scalar xnf,
			Scalar dt, Direction dir)
{
  Vector3 en,en1,bn,bn1,graden,gradbn, V1, Vn1f, Vn2f, V2;
  Scalar On,On1,gradOn;
  get_en(xnf,en,en1, dir);

  get_bn(xnf,bn,bn1,On,On1, dir);
  
  get_Gradeb(xnf,graden,gradbn,gradOn, dir);//get field parameters
  
  Scalar V_nf = vnf.e1()/spemit->get_gamma(vnf);

  Scalar Ov=(1-(2*f-3)/4)*On+(2*f-3)/4.*On1+(f-0.5)/2.*V_nf*gradOn*dt;

  Vector3 bv=(1-(2*f-3)/4.)*bn+(2*f-3)/4.*bn1+(f-0.5)/2.*V_nf*gradbn*dt;

  Vector3 ev=(1-(2*f-3)/4.)*en+(2*f-3)/4.*en1+(f-0.5)/2.*V_nf*graden*dt;

  Scalar Ot=(1-(2*f-3)/4)*On+(2*f-3)/4.*On1+0.5*V_nf*gradOn*dt;

  Vector3 bt=(1-(2*f-3)/4.)*bn+(2*f-3)/4.*bn1+0.5*V_nf*gradbn*dt;

  Vector3 et=(1-(2*f-3)/4.)*en+(2*f-3)/4.*en1+0.5*V_nf*graden*dt;

  Scalar Ot1=(1-(2*f-3)/4)*On+(2*f-3)/4.*On1-0.5*V_nf*gradOn*dt;

  Vector3 bt1=(1-(2*f-3)/4.)*bn+(2*f-3)/4.*bn1-0.5*V_nf*gradbn*dt;

  Vector3 et1=(1-(2*f-3)/4.)*en+(2*f-3)/4.*en1-0.5*V_nf*graden*dt;

  Scalar Ox=(1-(2*f/3.-1))*On+(2*f/3.-1)*On1+f/4.*V_nf*gradOn*dt;

  Vector3 bx=(1-(2*f/3.-1))*bn+(2*f/3.-1)*bn1+f/4.*V_nf*gradbn*dt;

  Vector3 ex=(1-(2*f/3.-1))*en+(2*f/3.-1)*en1+f/4.*V_nf*graden*dt;
 
  Boris_push_integrator(vnf,V1,ev,bv,Ov,f-0.5, f-0.5, dt);

  Boris_push_integrator(vnf,Vn1f,et,bt,Ot,1,1, dt);

  Boris_push_integrator(vnf,V2,et1,bt1,Ot1,-1,-1, dt);
  
  Boris_push_integrator(vnf,Vn2f,ex,bx,Ox,0.5*f,0.5*f, dt);

  Scalar gammaV2= spemit->get_gamma(V2);
  Scalar gammaVn1f = spemit->get_gamma(Vn1f);
  Scalar gammavnf = spemit->get_gamma(vnf);

  Vector3 vn_add=1/24.*(Vn1f/gammaVn1f-vnf/gammavnf)%bn*On*dt-1/12.*(gammaVn1f-gammavnf)*(Vn1f/gammaVn1f-vnf/gammavnf)-1/24.*(gammaVn1f-2*gammavnf+gammaV2)*vnf/gammavnf+1/24.*V_nf*(graden+gradOn*vnf%(bn)/gammavnf+On*vnf%(gradbn)/gammavnf)*dt*dt+1/24.*(en-en1+On*vnf%(bn-bn1)/gammavnf+(On-On1)*vnf%(bn)/gammavnf)*dt;

  vnhalf=V1;

  Vector3 Xn=f*dt*(Vn2f/spemit->get_gamma(Vn2f)+vn_add/gammavnf);

  xn=Xn.e1();//get Xn
  }

void Emitter::get_en(Scalar xnf1, Vector3 &_en,
		     Vector3 &_en1, Direction dir)
{
   if(iter==1)
     {
       theboundary->get_E(dir, a[0]);
       a[1]=a[0];
       _en=qm*a[0],_en1=qm*a[1];
     }
   else 
     {
       int locate;
       locate=iter%2;
       switch(locate)
	{
        case 0: theboundary->get_E(dir, a[1]);
	         _en=qm*a[1],_en1=qm*a[0];
                 break; 
        case 1: theboundary->get_E(dir, a[0]);
                 _en=qm*a[0],_en1=qm*a[1];
	         break;  
        }
     }

}
void Emitter::get_bn(Scalar xnf1, Vector3 &_bn,
		     Vector3 &_bn1, Scalar &_On, Scalar &_On1, Direction dir)
{ 
   if(iter==1)
     {
       theboundary->get_B(dir, b[0]);
       b[1]=b[0];
       _bn=b[0].unit();
       _bn1=_bn;

       _On=qm*b[0].magnitude(),_On1=_On;
     }
   else 
     {
       int locat;
       locat=iter%2;
       switch(locat)
	{
        case 0: theboundary->get_B(dir, b[1]);
		_bn=b[1].unit();
		_bn1=b[0].unit();
                _On=qm*b[1].magnitude(),_On1=qm*b[0].magnitude();
                 break; 

        case 1: theboundary->get_B(dir, b[0]);
                _bn=b[0].unit();
                _bn1=b[1].unit();
		_On=qm*b[0].magnitude(),_On1=qm*b[1].magnitude();
                 break;  
        }   
     } 
}

void Emitter::get_Gradeb(Scalar xnf1,Vector3 &_graden, 
			 Vector3 &_gradbn,Scalar &_gradOn, Direction dir)
{
   Vector3 _gradbn1;
   theboundary->get_EBgradient(dir, _graden, _gradbn1);
   Vector3 Bn;
   theboundary->get_B(dir, Bn);
   _graden=qm*_graden;
   if (_gradbn1.isNonZero())
   {  
     _gradbn=_gradbn1/Bn.magnitude();
     _gradOn=qm*Bn.unit()*_gradbn1;
   }
   else
   {
     _gradbn=_gradbn1;
     _gradOn=0;
   }
}  

//------- to return the final velocity without solving matrix-------------
// the standard equation solved below is : V_final-V_init= c1*e*dt+tan(O * c2 *dt/2)(V_final+V_init)/gamma*b --------------

void Emitter::Boris_push_integrator(Vector3 V_init,Vector3 &V_final,
                                    Vector3 e, Vector3 b,
                                    Scalar O,
                                    Scalar c1, Scalar c2, Scalar dt)
{
  Vector3 V_minus, V_plus, T, S, V_pie;
  e = 0.5*c1*dt*e;
  V_minus = V_init + e;                  //half acc
  Scalar rotate_gamma = spemit->get_gamma(V_minus);
  T = tan(O*c2*dt/2)/rotate_gamma*b;
  S = 2*T/(1+T.magnitude()*T.magnitude());
  V_pie = V_minus + V_minus%T;
  V_plus= V_minus + V_pie%S;              // rotate
  V_final = V_plus + e;                  // another half acc
}


void Emitter::get_times(int npart, Scalar *t, Scalar ts, Scalar te)
{
  int i; 
  Scalar tttime;
  bool uniformintime;
  
  uniformintime=true;
  
  if(uniformintime)
    {
      Scalar delt = (te - ts)/Scalar(npart);
      for(i=0, tttime=ts; i < npart; i++, tttime += delt)
	{
	  t[i] = 1-(tttime-ts)/(te-ts); //to get the "f"
	}
    }
  else
    {
    }
}

void Emitter::get_velocities(int npart, Vector3 *v, Scalar *f)
{
  if(var_energy)
  {
    if (direction == POSITIVE) v -> set_e1(vx_equation.eval(time));
    else if (direction == NEGATIVE) v ->set_e1(-vx_equation.eval(time));
  }
  else
  {
    distri->get_U(npart,v, (Direction)direction);
  }
}

void Emitter::add_particles(int nn, Scalar weight, Scalar *xinit, Scalar *dX,
			    Vector3 *vfinal, bool vnorm, Direction dir)
{
  theboundary->eject(nn, spemit, weight, dir);
  emit_pg->add_particles(nn, weight, xinit, dX, vfinal, vnorm, false);
}

void Emitter::add_particles_to_list()
{
  if (emit_pg->get_n())
  {
    // mindgame: false, true
    spemit->add_particles(emit_pg, false, true);
    emit_pg->delete_all();
  }
}
