##
##	oopd1 Makefile
##
#------------------------------------------------------------------------
#---------ATRIBUTES THAT MIGHT BE CHANGED BY THE USER--------------------
#Verbosity of the makefile
VERBOSE = TRUE

# compile w/o MPI (no) or w/ MPI (yes)
MPI=no

# compile mode: optimized; if not set, it is debug mode
#MODE=optimized

#------------------LIBRARIES SETUP---------------------------------------
# Tcl/Tk version and directory, use if only tkX.x.a libs exist:
# TCLTK=8.4
TCLTK=8.6
#TKDIR=-L/usr/local/lib
TKDIR=-L/usr/lib/tk8.6

#Xgraphics path
#XGPATH = /usr/local/xgrafix
XGPATH = ${HOME}/opt/xgrafix

#Extra libraries directories
EXTRA_LIBDIR=

#------------------------------------------------------------------------
CFLAGS=-I$(XGPATH)/include
CUDAFLAGS = -std=c++11 -I$(XGPATH)/include

#------------------------------------------------------------------------
COPT= -O3
CDEBUG= -Wall -g

#-----NOTHING SHOULD BE EDITED FROM BELOW BY THE STANDARD USER------------------
#------------------------------------------------------------------------
# Declare the folders inside /src and /include to search for files
# The .c, .cpp must be placed in a folder inside /src and the header
# files inside /include
DIRS = main spatial_region diagnostics reactions distributions fields \
			 utils emmiter MCC

#------------------------------------------------------------------------
## Flags used for compiling.  -O for optimization, which is optional.
## If the X11 include files are not located in /usr/include/X11, you
## must specify the directory as a flag prefixed by -I.  For instance,
## if the include files for X11 are located in /usr/local/include, the
## flag -I/usr/local/include must also be passed.

#------------------------------------------------------------------------
#Libraries for compilation
LIBS  = -L/usr/local/lib $(EXTRA_LIBDIR) -L$(XGPATH)/lib -lXGC250 \
				-ltk$(TCLTK) -ltcl$(TCLTK) -lXpm -lX11 -lm -ldl

#------------------------------------------------------------------------
## Libraries and their directories used in loading.
## Normal is -lm and -lX11 for the Math and X11 libraries, which is
## NOT optional.  On Unicos, -lnet is also used.  If the X11
## libraries are not located in /usr/lib/X11, you
## must also specify the directory as a flag prefixed by -L.
## For instance, if the include files for X11 are located in
## /usr/local/lib, the flag -L/usr/local/lib must also be passed.
## On Sun, use -Bstatic to avoid problems with differing versions of OS.
## Man cc for more information on flags.


UNAME_S := $(shell uname -s)
UNAME_N := $(shell uname -n)


#------------------------------------------------------------------------
#Compilation selection for MPI or just CPP
ifeq ($(MPI),no)
   CC = g++
   EXEC = pd1
   ifeq ($(UNAME_S),Darwin)
      TCLTK=
      EXTRA_LIBDIR=-L/opt/local/lib -I/opt/local/include -L/opt/X11/lib
   endif
else
   EXEC = mpipd1
   ifeq ($(UNAME_S),Darwin)
      CC = mpic++-mpich-devel-mp
   else
      CC = mpiCC
      ifeq ($(UNAME_N),dev-intel16)
         # HPCC @ MSU
         XGPATH = $(HOME)/xgrafix
      endif
   endif
endif

#------------------------------------------------------------------------
# OS specific part
ifeq ($(OS),Windows_NT)
    RM = del /F /Q
    RMDIR = -RMDIR /S /Q
    MKDIR = -mkdir
    ERRIGNORE = 2>NUL || true
    SEP=\\
else
    RM = rm -rf
    RMDIR = rm -rf
    MKDIR = mkdir -p
    ERRIGNORE = 2>/dev/null
    SEP=/
endif

PSEP = $(strip $(SEP))

ifeq ($(VERBOSE),TRUE)
    HIDE =
else
    HIDE = @
endif

#------------------------------------------------------------------------
ifeq ($(MODE),optimized)
	override CFLAGS += $(COPT)
else
	override CFLAGS += $(CDEBUG)
endif


#------------------------------------------------------------------------
#------------------------------------------------------------------------
#Set project directories and target executable
PROJDIR := $(realpath $(CURDIR))
SOURCEDIR := $(PROJDIR)/src
BUILDDIR := $(PROJDIR)/build
INCLUDEDIR := $(PROJDIR)/include
DEPFILE = .depend
TARGET = pd1.exe


SOURCEDIRS = $(foreach dir, $(DIRS), $(addprefix $(SOURCEDIR)/, $(dir)))
TARGETDIRS = $(foreach dir, $(DIRS), $(addprefix $(BUILDDIR)/, $(dir)))

# Generate the GCC includes parameters by adding -I before each source folder
INCLUDES = -I$(INCLUDEDIR)#$(foreach dir, $(DIRS), $(addprefix -I$(INCLUDEDIR)/, $(dir)))

# Add this list to VPATH, the place make will look for the source files
VPATH = $(SOURCEDIRS)

# Create a list of *.c and *.cpp sources in DIRS
SOURCESCPP = $(foreach dir, $(DIRS), $(shell find $(SOURCEDIR)/$(dir) -type f -name *.cpp))
SOURCESC = $(foreach dir, $(DIRS), $(shell find $(SOURCEDIR)/$(dir) -type f -name *.c))
SOURCESCUDA = $(foreach dir, $(DIRS), $(shell find $(SOURCEDIR)/$(dir) -type f -name *.cu))
ALLSOURCES := $(foreach source, $(SOURCESCPP), $(source))
ALLSOURCES += $(foreach source, $(SOURCESC), $(source))
ALLSOURCES += $(foreach source, $(SOURCESCUDA), $(source))

# Define objects for all sources
OBJS := $(subst $(SOURCEDIR),$(BUILDDIR),$(SOURCESCPP:.cpp=.o))
OBJS += $(subst $(SOURCEDIR),$(BUILDDIR),$(SOURCESC:.c=.o))
OBJS += $(subst $(SOURCEDIR),$(BUILDDIR),$(SOURCESCUDA:.cu=.o))

# Define dependencies files for all objects
DEPS := $(foreach dir, $(DIRS), $($(addprefix $(INCLUDEDIR)/, $(dir)):.hpp=.d))
DEPS += $(foreach dir, $(DIRS), $($(addprefix $(INCLUDEDIR)/, $(dir)):.hh=.d))
DEPS += $(foreach dir, $(DIRS), $($(addprefix $(INCLUDEDIR)/, $(dir)):.h=.d))

#------------------------------------------------------------------------
#Define the compilation rules for .cpp, .c and .cu files
define generateRules
$(1)/%.o: %.cpp
	@echo Building $$@
	$(HIDE)$(CC) $$(INCLUDES) -c $(CFLAGS) -o $$(subst /,$$(PSEP),$$@) $$(subst /,$$(PSEP),$$<) $$(LIBS)
$(1)/%.o: %.c
	@echo Building $$@
	$(HIDE)$(CC) $$(INCLUDES) -c $(CFLAGS) -o $$(subst /,$$(PSEP),$$@) $$(subst /,$$(PSEP),$$<) $$(LIBS)
$(1)/%.o: %.cu
	@echo Building $$@
	$(HIDE)nvcc $$(INCLUDES) -c $(CUDALAGS) -o $$(subst /,$$(PSEP),$$@) $$(subst /,$$(PSEP),$$<) $$(LIBS)
endef

.PHONY: all clean directories dep

all:	$(DEPFILE) directories $(TARGET)


$(TARGET): $(OBJS)
	echo Linking $@
	$(HIDE)$(CC) $(OBJS) -o $(TARGET) $(LIBS)

-include $(DEPFILE)


# Include dependencies
#-include $(DEPS)


# Generate rules
$(foreach targetdir, $(TARGETDIRS), $(eval $(call generateRules, $(targetdir))))
#$(info $(INCLUDES))

directories:
	$(MKDIR) -p $(subst /,$(PSEP),$(TARGETDIRS)) $(ERRIGNORE)

#------------------------------------------------------------------------
# mindgame: for case that the file $(DEPFILE) is wanted to be updated.
dep:
	$(MAKE) $(DEPFILE)

#------------------------------------------------------------------------


$(DEPFILE):
	echo "# Do not edit. Dependencies generated by Makefile" > $(DEPFILE)
	for sourcefile in $(ALLSOURCES); do \
	  objtarget=$$(echo $${sourcefile} | sed -e 's/src/build/;s/\.[a-z]\{1,3\}$$/.o/');\
	  $(CC) -MM $(CFLAGS) $(INCLUDES) -MT $${objtarget} $${sourcefile} >> $(DEPFILE); \
	done;
# mindgame: makedepend is a legacy util.
  #$(info $(foreach source, $(ALLSOURCES), $(CC) -MM $(CFLAGS) $(INCLUDES)  $(source) ))
#	touch $(DEPFILE)
#	makedepend -f$(DEPFILE) *.cpp 2> /dev/null

#------------------------------------------------------------------------
clean:
	$(HIDE)$(RMDIR) $(subst /,$(PSEP),$(TARGETDIRS)) $(ERRIGNORE)
	$(HIDE)$(RM) $(TARGET) $(ERRIGNORE)
	$(HIDE)$(RM) $(DEPFILE)
	@echo Cleaning done !

#------------------------------------------------------------------------
distclean:
	@make clean
	@rm -f $(EXEC)

#------------------------------------------------------------------------
# DO NOT DELETE
# mindgame: check whether it exists
#ifneq ($(wildcard $(DEPFILE)),)
#include $(DEPFILE)
#endif

#---------------TODO-----------------------------------------------------
#THIS COMPILATIONS HAVE NOT BEEN ADAPTED YET
#------------------------------------------------------------------------
# fcalc -- function calculator (tests Equation class)
fcalc:		fparser.o function.o equation.o
	$(CC) -o fcalc function.o fparser.o equation.o $(LIBS)

#------------------------------------------------------------------------
# ndarray -- demonstrates ndarray and binned template
ndarray: $(DEPFILE) $(PD1OBJ) ndarray.cpp
	$(CC) $(CFLAGS) $(DEFINES) -o ndarray ndarray.cpp uniformmesh.o DataArray.o equation.o fparser.o

#------------------------------------------------------------------------
# load -- demonstrates distribution loading
LOAD_SOURCE= dist_load.cpp dist_xv.cpp dist_x.cpp dist_v.cpp dist_function.cpp \
	dist_uniform.cpp dist_gaussian.cpp dist_arbitrary.cpp message.cpp
dist_load: $(LOAD_SOURCE)
	$(CC) $(CFLAGS) -D_STANDALONE_LOADLIB -o dist_load $(LOAD_SOURCE)


# Example: make query-ALLSOURCES
query-%:
	@echo $($(*))

#Info about makefiles extracted, mainly, from:
## https://riptutorial.com/makefile/example/21376/building-from-different-source-folders-to-different-target-folders
## https://www.partow.net/programming/makefile/index.html
