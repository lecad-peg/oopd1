% check to see if phi and rho are consistent

% global variables
phi_file='phi.txt'
rho_file='rho.txt'
EPSILON0=8.85418781761E-12;


% read the files
[x,phi]=textread(phi_file, '%n%n','commentstyle','shell');
[x,rho]=textread(rho_file, '%n%n','commentstyle','shell');

ng = size(x,1)
dx = x(2) - x(1)  % assumes constant mesh spacing

% check rho
rho_check(1) = rho(1);
rho_check(ng) = rho(ng);
for i=1+1:ng-1
rho_check(i) = phi(i+1)- 2*phi(i) + phi(i-1);
rho_check(i) = -rho_check(i)*(EPSILON0/(dx*dx));
end

% check phi
rhs = rho;
rhs(1) = phi(1);
rhs(ng) = phi(ng);
M = 2*diag(ones(ng,1)) - diag(ones(ng-1,1),1) - diag(ones(ng-1,1),-1);
M = M*EPSILON0/(dx*dx);
M(1,1) = 1;
M(ng,ng) = 1;
phi_check = M\rhs;

% plot the resulting conclusions
plot(x, rho, x, rho_check);
plot(x, phi, x, phi_check);
