% global data
n0 = 5.48e+15;
T = 200
phi_file='phi.txt'
rho_file = 'nboltz.txt';
e = 1.609E-19;

% read the files
[x,phi]=textread(phi_file, '%n%n','commentstyle','shell');
[x,rho]=textread(rho_file, '%n%n','commentstyle','shell');

ng = size(x,1)

for i=1:ng
rho_check(i) = n0*exp(phi(i)/T);
end
plot(x, rho, x, rho_check);
